@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
def RELEASE_VERSION
def RELEASE_BUILD
def PR_BRANCH
def ENV
def BRANCH_NAME
def date

pipeline {
    agent {label 'python-3.6'}
    options {
        skipDefaultCheckout()
    }
    environment {
        FOLDER = "."
        // repository name
        REPOSITORY = "artemis"
        // repo user
        OWNER = "mathildetech"

        TAG_CREDENTIALS = "alaudabot-bitbucket"

        DEPLOYMENT_NAME = "artemis"

        DINGTALK_ROBOT = "artemis-robot"

        TEST_IMAGE = "index.alauda.cn/alaudak8s/artemis"
        SONOBUOY_IMAGE = "index.alauda.cn/alaudaorg/qaimages:sonobuoy"
		IMAGE_CREDENTIALS = "alaudak8s"
        CHARTS_PIPELINE = "/common/common-charts-pipeline"
        CHART_NAME = "artemis-e2e"
        CHART_COMPONENT = "artemis"
        
    }
    stages {
        stage('Prepare') {
            steps {
                script {
                    container('tools') {
                        // checkout code
                        def scmVars
                        retry(2) { scmVars = checkout scm }
                        release = deploy.release(scmVars)

                        RELEASE_BUILD = release.version
                        RELEASE_VERSION = release.majorVersion
                        PR_BRANCH = release.version.minus('.').minus('.').toLowerCase()
                        // extract information
                        GIT_COMMIT = scmVars.GIT_COMMIT
                        GIT_BRANCH = scmVars.GIT_BRANCH

                        ENV = release.environment

                        BRANCH_NAME = "master"
                        if (release.change["branch"]){
                            BRANCH_NAME = release.change["branch"].replace("/", "-")
                            CASE_TYPE = release.change["title"].replace(" ", "")
                        } else {
                            CASE_TYPE = "BAT"
                        }

                        echo """
                            release ${RELEASE_VERSION}
                            version ${release.version}
                            is_release ${release.is_release}
                            is_build ${release.is_build}
                            is_master ${release.is_master}
                            deploy_env ${release.environment}
                            auto_test ${release.auto_test}
                            environment ${release.environment}
                            majorVersion ${release.majorVersion}
                            git_branch ${GIT_BRANCH}
                            git_commit ${GIT_COMMIT}
                            pr_branch ${PR_BRANCH}
                            case_type ${CASE_TYPE}
                            BRANCH_NAME ${BRANCH_NAME}
                            BROWSER_PROXY ${env.BROWSER_PROXY}
                            BUILD_URL ${release.BUILD_URL}
                        """
                    }
                }
            }
        }
        
        stage('Build') {
            steps {
                script {
					container('tools') {
                        IMAGE = deploy.dockerBuildWithRegister(
                            dockerfile: "Dockerfile", //Dockerfile
                            address: "alaudak8s/artemis",
                            tag: "${BRANCH_NAME}", // tag
                            armBuild: false // default true
                        )
                        if("${release.is_release}" == "false") {
                            IMAGE.start().push()
                            IMAGE.push("${release.version}")
                        } else {
                            IMAGE.start().push("${release.version}")
                        } 
                        
                        
                        
                        if ("${release.is_master}" == "true"){
                            IMAGE.push("latest")
                        }
					}
                }
            }
        }

        stage('set-cluster') {
            steps {
                script {
                    container('tools') {
                        // 准备kubectl
                        sh """
                        kubectl config set-cluster automation --server=https://94.191.89.53:6443 --insecure-skip-tls-verify=true
                        kubectl config set-credentials automation --token=eyJhbGciOiJSUzI1NiIsImtpZCI6IlpVNzBRMkFTZmxENDVyYnR0WEZzaXpYQzVxMnF0Y3hFUTlpaUlBVE82SEUifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLXpwdnh2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI0NTJkMTI4Ny03MGNjLTRkZGYtODNkYi1jNGJhODExYzFlNWIiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.taGR1f4dU65HgQjaIByvj4A58yQwghA1ZHoNEfHMvYr_zezL8DHmplzNCIJXOuc3EAL07raARSjiR3Kask17NbybzXTk8TAHb66dCRivGlTnS3xeiDi4PhPVvcUeNJ4rThawFyQEiwcqNlOr4axWIP4q9APf-5lZeGTDRrzCs8mYF58fHxLAl2xgWlI2H5se-1TAWbte0n16gqUv7LtArSAxtFecJakHuG4KwhmzclU8sOaQGdhcrega-kPp7CXsgkYuSUvapP5AEKwySNNIIvZ8V_EckGK5edtS_egYatNU3jenJYxMVO0I1zrTq72fm7VR_v7tgkgz9_yOSehQQw
                        kubectl config set-context automation --cluster=automation --user=automation
                        kubectl config use-context automation
                        """
                    }
                }
            }
        }

        stage('Test') {
            steps {
                timeout(120) {
                    script {
                        container('tools') {
                            image = ""
                            if (BRANCH_NAME == "master"){
                                image = "${TEST_IMAGE}:latest"
                            } else {
                                image = "${TEST_IMAGE}:${release.version}"
                            }
                            echo "IMAGE is : ${image}"

                            // 获取时间戳 当做测试的ns
                            date = sh "date +%Y%m%d%H%M%S"
                            sh "date +%Y%m%d%H%M%S > current_date"
                            date = readFile "current_date"
                            date = date.replace("\n", "")

                             if("${release.is_release}" == "false") {
                                if (CASE_TYPE.toLowerCase() == "bat") {
                                    sh """
                                    scripts/sonobuoy_gen.sh ${image} acp ${BRANCH_NAME} ${release.BUILD_URL} artemis-acp.yaml
                                    scripts/sonobuoy_gen.sh ${image} devops_ui ${BRANCH_NAME} ${release.BUILD_URL} artemis-devops-ui.yaml
                                    scripts/sonobuoy_gen.sh ${image} asm_ui ${BRANCH_NAME} ${release.BUILD_URL} artemis-asm-ui.yaml
                                    scripts/sonobuoy_gen.sh ${image} ait_ui ${BRANCH_NAME} ${release.BUILD_URL} ait-ui.yaml
                                    sonobuoy run --sonobuoy-image ${SONOBUOY_IMAGE} --plugin artemis-acp.yaml --plugin artemis-devops-ui.yaml --plugin artemis-asm-ui.yaml --plugin ait-ui.yaml --context=automation -n artemis-${date} --wait
                                    """
                                } else{
                                    sh """
                                    scripts/sonobuoy_gen.sh ${image} ${CASE_TYPE} ${BRANCH_NAME} ${release.BUILD_URL} artemis-pr.yaml
                                    sonobuoy run --sonobuoy-image ${SONOBUOY_IMAGE} --plugin artemis-pr.yaml --context=automation -n artemis-${date} --wait
                                    """
                                }
                             }



                            // try {
                               
                            //     // 生成测试yaml
                            //     sh """
                            //     sonobuoy gen plugin --name plugin-${date} --image ${image} -e ENV=staging -e CASE_TYPE=${CASE_TYPE} -e BRANCH_NAME=${BRANCH_NAME} -e BUILD_URL=${release.BUILD_URL} > artemis-pr.yaml
                            //     scripts/add_volumemonut.sh
                            //     cat artemis-pr.yaml
                            //     sonobuoy run --image-pull-policy Always --sonobuoy-image ${SONOBUOY_IMAGE} --plugin artemis-pr.yaml -n artemis-${date} --wait
                            //     """
                            // }
                            // catch (Exception exc) {
                            //     echo "${exc}"
                            //     throw(exc)
                            // }
                        }
                    }
                }
            }
        }

        stage('retrieve-result') {
            steps {
                script {
                    container('tools') {
                        if("${release.is_release}" == "false") {
                            // 拷贝测试数据
                            result_tar = sh script: "sonobuoy retrieve -n artemis-${date}", returnStdout: true
                        
                            // 清理
                            sh "sonobuoy delete -n artemis-${date}"

                            // 检查测试结果
                            sh "python scripts/check_result.py ui_report ${result_tar}"
                        }
                    }
                }
            }
        }

        stage('restore-cluster') {
            steps {
                script {
                    container('tools') {
                        sh "rm ~/.kube/config"
                    }
                }
            }
        }

        stage('Tag git') {
			when {
				expression {
					release.shouldTag()
				}
                anyOf {
				    branch "master";
				    branch "release*"
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							deploy.gitTag(
								TAG_CREDENTIALS,
								RELEASE_BUILD,
								OWNER,
								REPOSITORY
							)
						}
					}
				}
			}
		}

        stage('Chart Update') {
			when {
				expression {
					release.shouldUpdateChart()
				}
                anyOf {
				    branch "master";
				    branch "release*"
				}
			}
			steps {
				script {
					// echo "will trigger charts-pipeline using branch ${release.chartBranch}"
					deploy.triggerChart([
						chart: CHART_NAME,
						component: CHART_COMPONENT,
						version: RELEASE_VERSION,
						imageTag: RELEASE_BUILD,
						branch: release.chartBranch,
						prBranch: release.change["branch"],
						env: release.environment
					]).start()
				}
			}
		}
    }

    post {
        //success {
        //    script {
        //        deploy.notificationSuccess(DEPLOYMENT_NAME, DINGTALK_ROBOT, "测试通过了!", RELEASE_BUILD)
        //    }
        //}
        //failure {
        //    script {
        //        deploy.notificationFailed(DEPLOYMENT_NAME, DINGTALK_ROBOT, "测试失败了!", RELEASE_BUILD)
        //    }
        //}
        //always {
        //    script {
        //        archiveArtifacts allowEmptyArchive: true, artifacts: "ui_report/**", fingerprint: true
        //        junit allowEmptyResults: true, testResults: "ui_report/*.xml"
        //    }
		//}
		always {
         script {
             container('tools') {
                 deploy.alaudaNotification notification: [name: 'uitest-notification', namespace: 'cpaas-system']
             }
             junit allowEmptyResults: true, testResults: "ui_report/*.xml"
             archiveArtifacts allowEmptyArchive: true, artifacts: "ui_report/**", fingerprint: true
         }
     }

    }
}
