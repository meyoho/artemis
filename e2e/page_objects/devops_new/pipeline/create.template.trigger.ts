import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { PipelinePageBase } from './pipeline.page.base';
import { $, $$ } from 'protractor';
import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';

/**
 * Created by liuwei on 2020/2/05.
 */

export class TemplateTrigger extends PipelinePageBase {
  constructor() {
    super();
  }

  get timingScanCheckBox(): AlaudaAuiCheckbox {
    return new AlaudaAuiCheckbox(
      $('alo-pipeline-code-change-trigger-form aui-checkbox'),
    );
  }

  get cornTriggerCheckBox(): AlaudaAuiCheckbox {
    return new AlaudaAuiCheckbox(
      $('alo-pipeline-cron-trigger-form .checkbox aui-checkbox'),
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByLeftText(text: string): AuiSelect {
    switch (text) {
      case '定时扫描':
        return new AuiSelect(
          $('alo-pipeline-code-change-trigger-form aui-select'),
        );
    }
  }

  setCronTrigger(data) {
    this.getButtonByText('自定义')
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          //遍历所有星期的checkout box
          $$('alo-cron-trigger-selector aui-checkbox').each(elem => {
            elem.getText().then(text => {
              const checkBox = new AlaudaAuiCheckbox(elem);
              if (data.includes(text)) {
                checkBox.check();
              } else {
                checkBox.uncheck();
              }
            });
          });
        }
      });
  }

  enterValue(name, value) {
    switch (name) {
      case '定时扫描':
        this.timingScanCheckBox.check();
        this.getElementByLeftText(name).select(value);
        break;

      case '定时触发器':
        this.cornTriggerCheckBox.check();
        this.setCronTrigger(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
