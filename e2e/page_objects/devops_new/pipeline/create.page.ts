/**
 * Created by zhangjiao on 2019/6/13.
 */

import { DevopsPageBase } from '../devops.page.base';
import { $ } from 'protractor';

import { CreatePypeDialog } from './create.type.dailog';
import { CreateTemplateSelectorPage } from './create.template.selector';
import { CreateBasicInfo } from './create.basic.page';
import { TemplateParamaterConfigPage } from './create.template.paramterconfig';
import { TemplateTrigger } from './create.template.trigger';

export class PipelineCreatePage extends DevopsPageBase {
  get dialogCreateType(): CreatePypeDialog {
    return new CreatePypeDialog();
  }
  get createTemplateSelectorPage(): CreateTemplateSelectorPage {
    return new CreateTemplateSelectorPage(
      $('alo-pipeline-template-select-from .cards-container'),
    );
  }

  get basicInfo(): CreateBasicInfo {
    return new CreateBasicInfo();
  }

  get templateParamaterConfig(): TemplateParamaterConfigPage {
    return new TemplateParamaterConfigPage();
  }

  get templateTrigger(): TemplateTrigger {
    return new TemplateTrigger();
  }

  isReady(feature: string): boolean {
    return this.globalfeatureIsEnabled(feature);
  }
  create(data) {
    this.clickLeftNavByText('流水线');
    this.getButtonByText('创建流水线').click();
    this.dialogCreateType.select(data['创建类型']);
    this.createTemplateSelectorPage.select(data['选择模版']);
    this.basicInfo.fill(data['基本信息']);
    this.templateParamaterConfig.fill(data['模版参数设置']);
    this.templateTrigger.input(data['触发器']);
    this.templateTrigger.click('创建');
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  create_script(data) {
    this.clickLeftNavByText('流水线');
    this.getButtonByText('创建流水线').click();
    this.dialogCreateType.select(data['创建类型']);
    this.basicInfo.fill(data['基本信息']);
    this.templateParamaterConfig.fill(data['Jenkinsfile']);
    this.templateTrigger.input(data['触发器']);
    this.templateTrigger.click('创建');
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  create_multibranch(data) {
    this.clickLeftNavByText('流水线');
    this.getButtonByText('创建流水线').click();
    this.dialogCreateType.select(data['创建类型']);
    this.basicInfo.fill(data['基本信息']);
    this.templateParamaterConfig.fill(data['分支设置']);
    this.templateTrigger.input(data['触发器']);
    this.templateTrigger.click('创建');
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
