import { $, $$, ElementFinder, element, by, browser } from 'protractor';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { CodeRepositoryDialog } from './code.repository.dailog';
import { RepositoryDialog } from './repository.dailog';
import { PipelinePageBase } from './pipeline.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AuiSwitch } from '../../../element_objects/alauda.auiSwitch';
import { AlaudaAuiCheckbox } from '../../../element_objects/alauda.auicheckbox';

export class TemplateParamaterConfigPage extends PipelinePageBase {
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ):
    | CodeRepositoryDialog
    | AlaudaDropdown
    | AuiSelect
    | RepositoryDialog
    | AlaudaAuiCodeEditor
    | AuiSwitch
    | AlaudaAuiCheckbox
    | AlaudaInputbox {
    switch (text) {
      case '代码仓库':
        return new CodeRepositoryDialog();
      case '分支':
        return new AlaudaDropdown(
          super.getElementByText(text, 'aui-select'),
          $$('.aui-option'),
        );
      case '镜像仓库':
        return new RepositoryDialog();
      case '集群':
      case '命名空间':
      case '应用':
      case '容器':
      case '分发仓库':
      case '源镜像凭据': // 同步镜像
      case '目标镜像凭据':
        return new AuiSelect(super.getElementByText(text, 'aui-select'));
      case '依赖仓库':
        return new AlaudaDropdown(
          super.getElementByText(text, 'aui-multi-select'),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        );
      case '文本输入 jmx':
        return new AuiSwitch($('div[class="aui-switch"]'));
      case '构建命令':
      case 'jmx 脚本':
      case 'JMeter 命令':
        return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
      case '部署配置目录':
      case '产出物路径':
      case '相对目录':
      case '构建上下文':
      case '构建参数':
      case 'Dockerfile':
      case '重试次数':
      case '参数':
      case '超时时间（秒）':
      case '源镜像仓库地址': // 同步镜像
      case '目标镜像仓库地址':
      case '源镜像标签':
      case '目标镜像标签':
        return new AlaudaInputbox(super.getElementByText(text, 'input'));
      case '部署容器':
        return new AlaudaInputbox(super.getElementByText('容器', 'input'));
      case '开启通知':
      case '创建应用':
        return new AuiSwitch(super.getElementByText(text, 'aui-switch'));
      case '通知':
        return new AlaudaDropdown(
          super.getElementByText(text, 'aui-multi-select'),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        );
      case '失败后回滚':
        return new AuiSwitch(super.getElementByText(text, 'aui-switch'));
      case '发现代码分支':
      case '发现代码仓库的 Pull Requests':
        const xpath1 = `//div[normalize-space(text())="${text}"]//ancestor::div/div/aui-checkbox`;
        const ele1: ElementFinder = element(by.xpath(xpath1));
        return new AlaudaAuiCheckbox(ele1);
      case '忽略已开启过 Pull Request 的分支':
      case '执行流水线时合并到目标分支':
      case '执行 Pull Request 中的分支':
        const xpath2 = `//div[normalize-space(text())="${text}"]//ancestor::div/div/aui-checkbox`;
        const ele2: ElementFinder = element(by.xpath(xpath2));
        return new AlaudaAuiCheckbox(ele2);
      case '浅克隆':
      case '获取 tag':
      case '删除流水线':
        return new AuiSwitch(super.getElementByText(text, 'aui-switch'));
      case '分支列表':
        return new AlaudaDropdown(
          super.getElementByText(text, 'aui-multi-select'),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '代码仓库':
        const existed_repo: ElementFinder = super.getElementByText(
          name,
          'aui-icon[@class="existed-repo__edit"]',
        );

        existed_repo.isPresent().then(isPresent => {
          if (isPresent) {
            new AlaudaButton(existed_repo).click();
          } else {
            new AlaudaButton(
              super.getElementByText(name, 'alo-repository-selector'),
            ).click();
          }
        });

        const elem = this.getElementByText(name) as CodeRepositoryDialog;
        elem.fill(value);
        break;

      case '镜像仓库':
        const existed_rep: ElementFinder = super.getElementByText(
          name,
          'aui-icon[@icon="basic:pencil_edit"]',
        );

        existed_rep.isPresent().then(isPresent => {
          if (isPresent) {
            new AlaudaButton(existed_rep).click();
          } else {
            new AlaudaButton(
              super.getElementByText(name, 'aui-icon[@icon="basic:plus"]'),
            ).click();
          }
        });

        (this.getElementByText(name) as RepositoryDialog).fill(value);
        break;
      case '分支':
        (this.getElementByText(name) as AlaudaDropdown).input(value);
        break;
      case '集群':
      case '命名空间':
      case '应用':
      case '容器':
      case '分发仓库':
      case '源镜像凭据': // 同步镜像
      case '目标镜像凭据':
        (this.getElementByText(name) as AuiSelect).select(value);
        break;
      case '依赖仓库':
        (this.getElementByText(name) as AlaudaDropdown).select(value);
        break;

      case '代码检出':
      case '代码扫描':
      case 'Docker 构建':
      case '部署应用':
      case '源镜像信息': // 同步镜像
      case '目标镜像信息':
        this.advanceOption(name, value);
        break;

      case '文本输入 jmx':
        (this.getElementByText(name) as AuiSwitch).open();
        break;
      case 'jmx 脚本':
      case '构建命令':
      case 'JMeter 命令':
        (this.getElementByText(name) as AlaudaAuiCodeEditor).setYamlValue(
          value,
        );
        break;
      /**
       * 创建应用打开时
       */
      case '部署配置目录':
      case '部署容器':
      case '产出物路径':
      case '相对目录':
      case '构建上下文':
      case '构建参数':
      case 'Dockerfile':
      case '重试次数':
      case '参数':
      case '超时时间（秒）':
      case '源镜像仓库地址': // 同步镜像
      case '目标镜像仓库地址':
      case '源镜像标签':
      case '目标镜像标签':
        (this.getElementByText(name) as AlaudaInputbox).input(value);
        break;
      case '开启通知':
      case '创建应用':
        if (String(value).toLowerCase() === 'true') {
          (this.getElementByText(name) as AuiSwitch).open();
        } else {
          (this.getElementByText(name) as AuiSwitch).close();
        }
        break;
      case '通知':
        (this.getElementByText(name) as AlaudaDropdown).select(value);
        break;
      case '失败后回滚':
        if (String(value).toLowerCase() === 'true') {
          (this.getElementByText(name) as AuiSwitch).open();
        } else {
          (this.getElementByText(name) as AuiSwitch).close();
        }
        break;
      case '多分支发现策略': // 多分支升级beta对应重构
        this.input(value);
        break;
      case '发现代码分支':
      case '发现代码仓库的 Pull Requests':
      case '忽略已开启过 Pull Request 的分支':
      case '执行流水线时合并到目标分支':
      case '执行 Pull Request 中的分支':
        if (String(value).toLowerCase() === 'true') {
          (this.getElementByText(name) as AlaudaAuiCheckbox).check();
        } else {
          (this.getElementByText(name) as AlaudaAuiCheckbox).uncheck();
        }
        break;
      case '选择分支列表':
        for (const item in value) {
          const branch: AlaudaDropdown = this.getElementByText(
            '分支列表',
          ) as AlaudaDropdown;
          browser.sleep(2000);
          branch.select(value[item]);
          browser.sleep(1000);
        }
        break;
      case '克隆配置、分支流水线回收':
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  fill(data) {
    this.input(data);
    this.click('下一步');
  }

  addSecret(data) {
    (this.getElementByText('代码仓库') as CodeRepositoryDialog).addSecret(data);
  }

  private _getXpathAdvancedFieldsButton(name, item) {
    // return `//aui-form-item//label[@class="aui-form-item__label" and normalize-space(text())="${name}" ]/ancestor::div[contains(@class, "dynamic-form-group")]//div[normalize-space(text())="${item}"]`;
    return `//div[normalize-space(text())="${name}"]/parent::div//div[normalize-space(text())="${item}"]`;
  }

  /**
   * 展开或者收起高级选项
   * @param name 名称：代码检出，代码扫描，Docker 构建，部署应用
   * @param item 类型：展开高级选项，收起高级选项
   */
  advanceOption(name: string, item: string) {
    let xpath = '';
    xpath = this._getXpathAdvancedFieldsButton(name, item);
    const advanceButton: ElementFinder = element(by.xpath(xpath));
    advanceButton.isPresent().then(isPresent => {
      if (isPresent) {
        new AlaudaButton(advanceButton).click();
      }
    });
  }
}
