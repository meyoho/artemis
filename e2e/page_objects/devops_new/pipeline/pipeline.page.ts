/**
 * Created by zhangjiao on 2019/6/13.
 */

import { DevopsPageBase } from '../devops.page.base';
import { PipelineCreatePage } from './create.page';
import { PipelineDetailVerify } from '@e2e/page_verify/devops/pipeline/pipeline_detail.page';
import { PipelineDetailPage } from './detail.page';
import { PipelineUpdatePage } from './update.page';
import { DevopsPreparePage } from '../devops.prepare.page';
import { PipelineListPage } from './list.page';

export class PipelinePage extends DevopsPageBase {
  get createPage() {
    return new PipelineCreatePage();
  }

  get updatePage() {
    return new PipelineUpdatePage();
  }

  get listPage() {
    return new PipelineListPage();
  }

  get detailPage() {
    return new PipelineDetailPage();
  }

  get preparePage(): DevopsPreparePage {
    return new DevopsPreparePage();
  }

  get detailVerifyPage() {
    return new PipelineDetailVerify();
  }
}
