/**
 * Created by zhangjiao on 2019/6/13.
 */

import { $, $$, element, by, browser } from 'protractor';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { PipelineUpdatePage } from './update.page';
import { PipelinePageBase } from './pipeline.page.base';
import { CommonPage } from '@e2e/utility/common.page';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class PipelineDetailPage extends PipelinePageBase {
  private get updatePage(): PipelineUpdatePage {
    return new PipelineUpdatePage();
  }
  get action(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('.page-header aui-icon[icon="angle_down"]'),
      $$('aui-tooltip aui-menu-item button'),
    );
  }

  get codeEditor() {
    return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
  }

  get historyTable() {
    return new AuiTableComponent($('aui-table'));
  }

  /**
   * 根据左侧文字获得右面元素,
   * @param text 左侧文字
   */
  getElementByText(text: string) {
    switch (text) {
      case 'imageTag': // 同步镜像或者更新应用中imagetag参数
        return new AlaudaInputbox(super.getElementByText(text));
      case '名称':
        return new AlaudaLabel($('.aui-card__header .pipeline-name'));
      case '执行方式':
        const xpath1 =
          '//div[contains(@class,"field")]//label[normalize-space(text())="执行方式" ]/ancestor::div[contains(@class,"field")]';
        return new AlaudaLabel(element(by.xpath(xpath1)));
      default:
        const xpath = `//div[@class="field"]//label[normalize-space(text())="${text}" ]/ancestor::div[@class="field"]//span`;
        return new AlaudaLabel(element(by.xpath(xpath)));
    }
  }

  clickTab(name: string) {
    const xpath = `//aui-tab-header//span[normalize-space(text())="${name}" ]`;
    return new AlaudaButton(element(by.xpath(xpath))).click();
  }

  async executePipeline() {
    await this.getButtonByText('操作').click();
    await browser.sleep(5000);
    await this.getButtonByText('执行').click();
    const message = await this.toast.getMessage();
    expect(message).toBe('流水线开始执行成功');
    console.log(message);
  }

  async executeParamPipeline(imageTag: string) {
    await this.getButtonByText('操作').click();
    await browser.sleep(5000);
    await this.getButtonByText('执行').click();
    (this.getElementByText('imageTag') as AlaudaInputbox).input(imageTag);
    await this.getButtonByText('确定').click();
    const message = await this.toast.getMessage();
    expect(message).toBe('流水线开始执行成功');
    console.log(message);
  }

  async updatedeployPipeline() {
    await this.getButtonByText('操作').click();
    await browser.sleep(5000);
    await this.getButtonByText('执行').click();
    await this.getButtonByText('确定').click();
    const message = await this.toast.getMessage();
    expect(message).toBe('流水线开始执行成功');
    console.log(message);
  }

  async scanPipeline() {
    await this.getButtonByText('操作').click();
    await browser.sleep(5000);
    await this.getButtonByText('扫描').click();
    const message = await this.toast.getMessage();
    expect(message).toBe('流水线开始扫描成功!');
    console.log(message);
  }

  async listPage_pipelineExecuteResult() {
    const result_element = element(by.xpath('//span[text()=" 执行成功 "]'));
    await CommonPage.waitElementPresent(result_element, 300000);
    return result_element.getText();
  }

  update(data) {
    this.action.select('更新');
    this.updatePage.update(data);
  }

  delete() {
    this.action.select('删除');
    this.click('删除');
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
