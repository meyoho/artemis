import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { PipelinePageBase } from './pipeline.page.base';

/**
 * Created by liuwei on 2020/2/05.
 */

export class CreateBasicInfo extends PipelinePageBase {
  constructor() {
    super();
  }

  /**
   * 根据左侧文字获得右面元素,
   * @param text 左侧文字
   */
  getElementByLeftText(
    text: string,
  ): AlaudaRadioButton | AuiSelect | AlaudaInputbox {
    switch (text) {
      case '名称':
      case '显示名称':
        return new AlaudaInputbox(super.getElementByText(text, 'input'));
      case 'Jenkins':
        return new AuiSelect(super.getElementByText(text, 'aui-select'));
      case 'Jenkinsfile 位置':
      case '执行方式':
        return new AlaudaRadioButton(
          super.getElementByText(text, 'aui-radio-group'),
        );
    }
  }
  enterValue(name, value) {
    switch (name) {
      case '名称':
      case '显示名称':
        (this.getElementByLeftText(name) as AlaudaInputbox).input(value);
        break;
      case 'Jenkinsfile 位置':
      case '执行方式':
        (this.getElementByLeftText(name) as AlaudaRadioButton).clickByName(
          value,
        );
        break;
      case 'Jenkins':
        (this.getElementByLeftText(name) as AuiSelect).select(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  fill(data) {
    this.input(data);
    this.click('下一步');
  }
}
