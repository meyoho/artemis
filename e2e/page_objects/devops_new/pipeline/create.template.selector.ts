import { ElementFinder, element, by } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { PipelinePageBase } from './pipeline.page.base';

/**
 * Created by liuwei on 2020/02/05.
 */

export class CreateTemplateSelectorPage extends PipelinePageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  /**
   * 模版创建流水线页，选择模版
   * @param name 模版名称
   */
  select(name: string) {
    this.waitElementPresent(this._root, '选择模版页面没有打开');
    const xpath = `//span[normalize-space(text())="${name}"]/ancestor::alo-pipeline-template-list-card//button[@aui-button="primary"]`;
    const templateElem: ElementFinder = element(by.xpath(xpath));
    this.waitElementPresent(
      templateElem,
      `模版 ${name} 元素没找到(${templateElem.locator()})`,
    );
    new AlaudaButton(templateElem).click();
  }
}
