import { element, by } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { DevopsPageBase } from '../devops.page.base';

/**
 * Created by liuwei on 2020/2/05.
 */

export class PipelinePageBase extends DevopsPageBase {
  constructor() {
    super();
  }

  /**
   * 单击上一步，下一步，取消按钮
   * @param name
   */
  click(name: string) {
    const xpath = `//button//span[normalize-space(text())="${name}"]/parent::button`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }
}
