import { ElementFinder, element, by, $, $$ } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { DevopsPageBase } from '../devops.page.base';
import { SecretPage } from '../secret/secret.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

/**
 * Created by liuwei on 2020/2/05.
 */

export class CodeRepositoryDialog extends DevopsPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder = $('aui-dialog')) {
    super();
    this._root = root;
  }
  get root(): ElementFinder {
    this.waitElementPresent(
      this._root,
      '选择[模板创建，脚本创建，多分支创建] 的[创建流水线 Dialog]页没出现',
    );
    return this._root;
  }

  /**
   * 根据左侧文字获得右面元素,
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ): AlaudaRadioButton | AuiSelect | AlaudaInputbox | AlaudaDropdown {
    switch (text) {
      case '代码仓库地址':
        return new AlaudaInputbox(super.getElementByText(text, 'input'));
      case '代码仓库':
        return new AlaudaDropdown(
          $('aui-dialog-content aui-select'),
          $$('.repo-name'),
        );
      case '凭据':
        return new AuiSelect(super.getElementByText(text, 'aui-select'));
      case '方式':
      case '仓库类型':
        return new AlaudaRadioButton(
          super.getElementByText(text, 'aui-radio-group'),
        );
    }
  }
  enterValue(name, value) {
    switch (name) {
      case '代码仓库地址':
        (this.getElementByText(name) as AlaudaInputbox).input(value);
        break;
      case '方式':
      case '仓库类型':
        (this.getElementByText(name) as AlaudaRadioButton).clickByName(value);
        break;
      case '代码仓库':
        (this.getElementByText(name) as AlaudaDropdown).select(value);
        break;
      case '凭据':
        (this.getElementByText(name) as AuiSelect).select(value);
        break;
      case '添加凭据':
        this.addSecret(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 单击确定，取消按钮
   * @param name
   */
  click(name: string) {
    const xpath = `//div[@class="aui-dialog__footer"]//button//span[normalize-space(text())="${name}"]/parent::button`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  /**
   * 填写代码库信息，单击确定
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fill(data) {
    this.input(data);
    this.click('确定');
  }

  addSecret(data) {
    const secretPage = new SecretPage();
    secretPage.addSecrets(data);
  }
}
