/**
 * Created by zhangjiao on 2019/6/13.
 */

import { PipelineCreatePage } from './create.page';

export class PipelineUpdatePage extends PipelineCreatePage {
  update(data) {
    console.log('step1');
    this.basicInfo.input(data['基本信息']);
    console.log('step2');
    this.templateParamaterConfig.input(data['模版参数设置']);
    console.log('step3');
    this.templateTrigger.input(data['触发器']);
    console.log('step4');
    this.templateTrigger.click('更新');
    console.log('step5');
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
