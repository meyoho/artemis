/**
 * Created by zhangjiao on 2019/6/13.
 */

import { DevopsPageBase } from '../devops.page.base';
import { $, ElementFinder } from 'protractor';

import { AuiTableComponent } from '@e2e/component/aui_table.component';

export class PipelineListPage extends DevopsPageBase {
  get pipeLineTable(): AuiTableComponent {
    return new AuiTableComponent($('.aui-card__content'));
  }

  /**
   * 表格为空时的显示, 子类不一样时需要重载
   */
  get noResult(): ElementFinder {
    return $('.aui-card__content alo-no-data');
  }

  enterDetail(name: string) {
    this.clickLeftNavByText('流水线');
    this.pipeLineTable.clickResourceNameByRow([name], '名称');
  }

  search(name) {
    this.pipeLineTable.aloSearch.search(name);
  }
}
