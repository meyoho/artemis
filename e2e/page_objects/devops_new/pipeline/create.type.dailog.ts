import { DevopsPageBase } from '@e2e/page_objects/devops/devops.page.base';
import { $, ElementFinder } from 'protractor';

/**
 * Created by liuwei on 2020/2/05.
 */

export class CreatePypeDialog extends DevopsPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder = $('aui-dialog')) {
    super();
    this._root = root;
  }
  get root(): ElementFinder {
    this.waitElementPresent(
      this._root,
      '选择[模板创建，脚本创建，多分支创建] 的[创建流水线 Dialog]页没出现',
    );
    return this._root;
  }

  /**
   * 从 【模板创建，脚本创建，多分支创建】 选择一个单击
   * @param item string, [模板创建，脚本创建，多分支创建
   * @example select('脚本创建')
   */
  select(item: string) {
    // 根据 item 找到单击的选择元素
    const itemElem: ElementFinder = this.root
      .$$('aui-dialog-content a span')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === item;
        });
      })
      .first();
    // 单击元素
    itemElem.click();
    // 等待Dialog]页 消失
    this.waitElementNotPresent(this._root, '');
    this.waitProgressBarNotPresent();
  }
}
