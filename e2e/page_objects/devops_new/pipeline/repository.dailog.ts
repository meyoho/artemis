import {
  $,
  ElementFinder,
  element,
  by,
  protractor,
  browser,
  $$,
} from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AloParamsInput } from '@e2e/element_objects/alauda.alo_params_input';
import { DevopsPageBase } from '../devops.page.base';
import { SecretPage } from '../secret/secret.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

/**
 * Created by liuwei on 2020/2/05.
 */

export class RepositoryDialog extends DevopsPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder = $('aui-dialog')) {
    super();
    this._root = root;
  }
  get root(): ElementFinder {
    this.waitElementPresent(
      this._root,
      '选择[模板创建，脚本创建，多分支创建] 的[创建流水线 Dialog]页没出现',
    );
    return this._root;
  }

  /**
   * 根据左侧文字获得右面元素,
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ):
    | AlaudaRadioButton
    | AuiSelect
    | AlaudaInputbox
    | AloParamsInput
    | AlaudaDropdown {
    switch (text) {
      case '镜像仓库-选择':
        return new AlaudaDropdown(
          super.getElementByText('镜像仓库', 'aui-select'),
          $$('.cdk-overlay-pane .aui-option .image-name'),
        );
      case '镜像仓库-输入':
        return new AlaudaInputbox(super.getElementByText('镜像仓库', 'input'));
      case '方式':
        return new AlaudaRadioButton(
          super.getElementByText(text, 'aui-radio-group'),
        );
      case '版本':
        return new AloParamsInput(
          super.getElementByText(
            text,
            'div[contains(@class,"tag-form-style")]',
          ),
        );
      case '凭据':
        return new AuiSelect(super.getElementByText(text, 'aui-select'));
      case '镜像地址':
        return new AlaudaInputbox(super.getElementByText('镜像地址', 'input'));
    }
  }
  enterValue(name, value) {
    console.log('key:' + name);
    console.log('value:' + value);
    switch (name) {
      case '版本':
        (this.getElementByText(name) as AloParamsInput).input(value);
        break;
      case '方式':
        (this.getElementByText(name) as AlaudaRadioButton).clickByName(value);
        break;
      case '镜像仓库':
        this._root
          .$('aui-radio-button *[class*="isChecked"]')
          .getText()
          .then(text => {
            this.waitProgressBarNotPresent();
            switch (text) {
              case '输入':
                (this.getElementByText(
                  '镜像仓库-输入',
                ) as AlaudaInputbox).input(value);
                browser.sleep(1000);
                const item1: ElementFinder = $$('aui-tooltip .aui-suggestion')
                  .filter(elem => {
                    return elem.getText().then(text => {
                      return text.trim() === value;
                    });
                  })
                  .first();
                item1.isPresent().then(isPressent => {
                  if (isPressent) {
                    item1.click();
                  }
                });
                break;
              case '新建':
                (this.getElementByText(
                  '镜像仓库-选择',
                ) as AlaudaDropdown).input(value[0]);
                const tooltip1 = $('aui-tooltip aui-suggestion-group');
                tooltip1.isPresent().then(isPresent => {
                  if (isPresent) {
                    tooltip.sendKeys(protractor.Key.ENTER);
                    browser.sleep(50);
                  }
                });

                new AlaudaInputbox(
                  super.getElementByText(
                    '镜像仓库',
                    'div[@class="image-input"]//input',
                  ),
                ).input(value[1]);
                browser.sleep(1000);

                const item: ElementFinder = $$('aui-tooltip .aui-suggestion')
                  .filter(elem => {
                    return elem.getText().then(text => {
                      return text.trim() === value[1];
                    });
                  })
                  .first();
                item.isPresent().then(isPressent => {
                  if (isPressent) {
                    item.click();
                  }
                });

                break;
              case '选择':
                (this.getElementByText(
                  '镜像仓库-选择',
                ) as AlaudaDropdown).select(value);
                const tooltip = $('aui-tooltip aui-suggestion-group');

                tooltip.isPresent().then(isPresent => {
                  if (isPresent) {
                    tooltip.sendKeys(protractor.Key.ENTER);
                    browser.sleep(50);
                  }
                });
                break;
            }
          });
        break;
      case '凭据':
        (this.getElementByText(name) as AuiSelect).select(value);
        break;
      case '添加凭据':
        this.addSecret(value);
        break;
      case '镜像仓库-输入':
        (this.getElementByText(name) as AlaudaInputbox).input(value);
        break;
      case '镜像地址':
        (this.getElementByText(name) as AlaudaInputbox).input(value);
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 单击确定，取消按钮
   * @param name
   */
  click(name: string) {
    const xpath = `//div[@class="aui-dialog__footer"]//button//span[normalize-space(text())="${name}"]/parent::button`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  fill(data) {
    this.input(data);
    this.click('确定');
  }

  addSecret(data) {
    const secretPage = new SecretPage();
    secretPage.addSecrets(data);
  }
}
