/**
 * Created by zhangjiao on 2019/6/14.
 */
import { by, ElementFinder } from 'protractor';
import { AlaudaElement } from '../../../element_objects/alauda.element';
//import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '@e2e/element_objects/alauda.confirmdialog';

import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { DevopsPageBase } from '../devops.page.base';

export class SecretUpdatePage extends DevopsPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form aui-form-item',
      'aui-form-item label[class*=aui-form-item__label]',
    );
  }

  get updateDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('.aui-dialog'),
      '.aui-dialog__header div span',
      'button[aui-button=primary]',
      `button[aui-button='']`,
    );
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case 'SSH 私钥':
        new AlaudaInputbox(this.getElementByText(
          name,
          'textarea',
        ) as ElementFinder).input(value);
        break;
      default:
        new AlaudaInputbox(super.getElementByText(
          name,
          tagname,
        ) as ElementFinder).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
