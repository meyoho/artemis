/**
 * Created by zhangjiao on 2019/6/14.
 */
import { $, $$, by, element, browser } from 'protractor';

import { SecretCreatePage } from './create.page';
import { SecretUpdatePage } from './update.page';
import { DevopsPageBase } from '../devops.page.base';
import { AlaudaList } from '@e2e/element_objects/alauda.list';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaBasicInfo } from '@e2e/element_objects/alauda.basicinfo';
import { CommonPage } from '@e2e/utility/common.page';
import { SecretPageDetailVerify } from '@e2e/page_verify/devops/secret/detail.page';

export class SecretPage extends DevopsPageBase {
  /**
   * 创建凭据类
   */
  get createPage() {
    return new SecretCreatePage();
  }

  /**
   * 更新凭据类
   */
  get secretUpdatePage() {
    return new SecretUpdatePage();
  }

  /**
   * 详情页验证类
   */
  get detailVerifyPage() {
    return new SecretPageDetailVerify();
  }

  addSecrets(testData) {
    this.getButtonByText('添加凭据').click();
    const createButton = element(
      by.xpath(
        '//span[text()="创建凭据"]/../../../..//button[@aui-button="primary"]',
      ),
    );
    CommonPage.waitElementPresent(createButton);

    this.createPage.fillForm(testData);
    browser.sleep(500);
    createButton.click();
    // 等待创建成功的toast 提示消失
    this.createPage.toast.getMessage().then(message => {
      console.log(message);
    });
    browser.sleep(1000);
    CommonPage.waitElementNotPresent(createButton);
  }

  /**
   * @param data 创建凭据
   */
  create(data) {
    this.fillForm(data);
    this.createPage.clickCreateButton(); //点击创建按钮
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  /**
   * @param data 创建凭据
   */
  fillForm(data) {
    this.clickLeftNavByText('凭据');
    this.waitProgressBarNotPresent();
    this.getButtonByText('创建凭据').click();
    this.createPage.fillForm(data);
  }

  /**
   * 凭据列表页更新凭据的显示名称
   */
  secretlistUpdateDisplay(name, data) {
    this.updatefillForm(name);
    this.resourceTable.clickOperationButtonByRow([name], '更新显示名称'); // 点击列表页更新显示名称
    this.secretUpdatePage.fillForm(data);
    this.secretUpdatePage.updateDialog.clickConfirm(); //点击更新按钮
  }

  /**
   * @param data 更新凭据
   */
  updatefillForm(name) {
    this.clickLeftNavByText('凭据');
    this.waitProgressBarNotPresent();
    this.resourceTable.searchByResourceName(name, 1);
  }

  /**
   * 凭据列表页更新凭据的数据信息
   */
  secretlistUpdateData(name, data) {
    this.updatefillForm(name);
    this.resourceTable.clickOperationButtonByRow([name], '更新数据信息'); // 点击列表页更新显示名称
    this.secretUpdatePage.fillForm(data);
    this.secretUpdatePage.updateDialog.clickConfirm(); //点击更新按钮
  }

  /**
   * @param name 凭据详情页更新凭据的显示名称
   */
  secretdetailUpdateDisplay(name, data) {
    this.updateDetailForm(name);
    this.getButtonByText('更新显示名称').click();
    this.secretUpdatePage.fillForm(data);
    this.secretUpdatePage.updateDialog.clickConfirm(); //点击更新按钮
  }

  /**
   * 凭据详情页更新凭据
   * @param name
   */
  updateDetailForm(name) {
    this.clickLeftNavByText('凭据');
    this.waitProgressBarNotPresent();
    this.resourceTable.searchByResourceName(name, 1);
    this.resourceTable.clickResourceNameByRow([name]);
    this.detailPage_triggerButton.click();
  }

  /**
   * 凭据详情页更新凭据的数据信息
   * @param name
   */
  secretdetailUpdateData(name, data) {
    this.updateDetailForm(name);
    this.getButtonByText('更新数据信息').click();
    this.secretUpdatePage.fillForm(data);
    this.secretUpdatePage.updateDialog.clickConfirm(); //点击更新按钮
  }

  /**
   * 凭据列表页删除凭据
   */
  secretlistDelete(name) {
    this.updatefillForm(name);
    this.resourceTable.clickOperationButtonByRow([name], '删除');
    this.confirmDialog_okButton.click(); //点击删除按钮
    this.resourceTable.searchByResourceName(name, 0);
  }

  /**
   * 凭据详情页删除凭据
   */
  secretdetaillDelete(name) {
    this.updateDetailForm(name);
    this.getButtonByText('删除').click();
    this.confirmDialog_okButton.click();
    this.resourceTable.searchByResourceName(name, 0);
  }

  // get secretTable(): AuiTableComponent {
  //     return new AuiTableComponent(
  //         $('.aui-layout__page .layout-page-content'),
  //         'aui-table',
  //         '.alo-search',
  //         'aui-table-cell',
  //         'aui-table-row'
  //     ); // 检索框的父元素;
  // }
  // -----------begin-----------secret list page
  navigationButton() {
    // return this.clickLeftNavByText('凭据', false);
    return this.clickLeftNavByText('凭据');
  }

  get listPage_createButton() {
    return this.getButtonByText('创建凭据');
  }

  listPage_SecretName(project_name, secretName) {
    return new AlaudaList(
      by.xpath(
        '//*[@href="/console-devops/admin/secrets/' +
          project_name +
          '/' +
          secretName +
          '"]',
      ),
    );
  }

  get confirmDialog_okButton() {
    return new AlaudaButton(
      $('aui-confirm-dialog .aui-confirm-dialog__confirm-button'),
    );
  }
  get listPage_getSecretTotal() {
    return new AlaudaText(by.css('.aui-paginator__total'));
  }

  get listPage_getSecretName() {
    return new AlaudaText(by.css('.name-cell a span'));
  }
  get listPage_nextPage_button() {
    return new AlaudaButton($('.aui-icon-angle_right'));
  }

  listPage_SecretOperate(SecretName, ns, operateName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="/console-devops/admin/secrets/' +
          ns +
          '/aaasdfsf' +
          SecretName +
          '"]',
      ),
      by.xpath(
        '//a[@href="/console-devops/admin/secrets/' +
          ns +
          '/aaasdfsf' +
          SecretName +
          '"]/../../../..//alo-menu-trigger',
      ),
      by.cssContainingText('.aui-menu-item', operateName),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  // -----------end-----------secret list page

  // -----------begin-----------Create secret page
  get createPage_headerTitle_Text() {
    return new AlaudaText(by.css('.aui-card__header span'));
  }

  get createPage_secretName_inputbox() {
    return new AlaudaInputbox(element(by.name('name')));
  }

  get createPage_project_dropdown() {
    return new AlaudaDropdown(element(by.name('namespace')), $$('.aui-option'));
  }

  get createPage_typeItem() {
    return element.all(by.css('.aui-radio-group .aui-radio-button'));
  }

  get createPage_createButton() {
    return this.getButtonByText('创建');
  }

  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }

  get createpage_input_content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      'input',
      '.aui-form-item__error',
    );
  }
  // -----------end-----------Create secret page

  // -----------begin-----------secret detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.title span'),
      'label',
      'span',
    );
  }
  get detailPage_triggerButton() {
    return new AlaudaButton($('.alo-menu-trigger'));
  }
  // -----------end-----------secret detail page

  // -----------begin-----------secret update page
  updateDialog_SecretName(secretname) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-form-item__content"]/span[contains(text(), "' +
          secretname +
          '")]',
      ),
    );
  }
  get updatePage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '',
      'label',
      '.aui-form-item__content .aui-form-item__control',
    );
  }
  get updatePage_updateButton() {
    return new AlaudaButton(
      element(by.xpath('//button[@aui-button="primary"]')),
    );
  }
  // -----------end-----------secret update page
}
