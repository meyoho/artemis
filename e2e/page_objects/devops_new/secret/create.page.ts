/**
 * Created by zhangjiao on 2019/6/14.
 */
import { $, browser, by, element, ElementFinder } from 'protractor';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { CommonPage } from '../../../utility/common.page';

import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { DevopsPageBase } from '../devops.page.base';

export class SecretCreatePage extends DevopsPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form aui-form-item',
      'aui-form-item label[class*=aui-form-item__label]',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
    tagname = 'input',
  ): AlaudaRadioButton | ElementFinder {
    switch (text) {
      case '使用域':
        return new AlaudaRadioButton(
          element(by.xpath('//aui-radio-group[@name="private"]')),
        );
      case '类型':
        return new AlaudaRadioButton(
          element(by.xpath('//aui-radio-group[@name="type"]')),
        );
      default:
        return this.alaudaElement.getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case '使用域':
        this.getElementByText(name, tagname).clickByName(value);
        break;
      case '类型':
        this.getElementByText(name, tagname).clickByName(value);
        break;
      case 'SSH 私钥':
        new AlaudaInputbox(this.getElementByText(
          name,
          'textarea',
        ) as ElementFinder).input(value);

        break;
      case '所属项目':
        const project = new AuiSelect(this.getElementByText(
          name,
          'aui-select',
        ) as ElementFinder);
        project.select(value);
        break;
      case 'Opaque':
        let index = 1;
        for (const key in value) {
          const keyInputbox = $(
            `alo-opaque-secret >div:nth-child(${index}) input:nth-child(1)`,
          );
          CommonPage.waitElementPresent(keyInputbox);
          keyInputbox.clear();
          keyInputbox.sendKeys(key);
          browser.sleep(100);
          const valueInputbox = $(
            `alo-opaque-secret >div:nth-child(${index}) input:nth-child(2)`,
          );
          CommonPage.waitElementPresent(valueInputbox);
          valueInputbox.clear();
          valueInputbox.sendKeys(value[key]);
          browser.sleep(100);
          $('alo-opaque-secret svg[class*=basic-plus]').click();
          index++;
        }
        break;
      case '工具类型':
        const tooltype = new AuiSelect(this.getElementByText(
          name,
          'aui-select',
        ) as ElementFinder);
        tooltype.select(value);
        break;
      case '工具集成名称':
        const tool_service_name = new AuiSelect(this.getElementByText(
          name,
          'aui-select',
        ) as ElementFinder);
        tool_service_name.select(value);
        break;
      default:
        new AlaudaInputbox(super.getElementByText(
          name,
          tagname,
        ) as ElementFinder).input(value);

        // browser.sleep(10);
        // this.getElementByText(name, tagname);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  clickCreateButton() {
    new AlaudaButton($('.aui-card__footer button[class*="primary"]')).click();
  }
}
