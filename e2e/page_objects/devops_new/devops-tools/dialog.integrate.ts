/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, by, element } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { DevopsPageBase } from '../devops.page.base';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class IntergrateDialog extends DevopsPageBase {
  clickByTypeBarName(name) {
    const xpath = `//alo-tool-type-bar//button//span[text()="${name}"]`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  clickToolByName(name) {
    const xpath = `//aui-dialog//div[normalize-space(text())="${name}" ]`;
    return new AlaudaButton(element(by.xpath(xpath))).click();
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '认证方式':
      case '类型':
        return new AlaudaRadioButton(
          this.getElementByText_xpath(text, 'aui-radio-button'),
        );
      case '凭据':
        return new AlaudaDropdown(
          this.getElementByText_xpath(text, 'aui-select'),
          $$('.aui-option'),
        );
      default:
        return this.getElementByText_xpath(text, 'input');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '集成':
        this.clickToolByName(value);
        break;
      case '类型':
      case '认证方式':
        this.getElementByText(name).clickByName(value);
        break;
      case '凭据':
        this.getElementByText(name).input(value);
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 集成一个工具
   * @param data 测试数据  {
                            集成: Jenkins
                            名称: ''
                            访问地址: ''
                            API 地址: ''
                          }
   */
  integrate(data) {
    this.fillForm(data);
    this.clickIntegrateButton();
  }

  /**
   * 单击集成按钮
   */
  clickIntegrateButton() {
    new AlaudaButton($('aui-dialog-footer button[class*="primary"]')).click();
  }

  /**
   * 单击取消按钮
   */
  clickCancelButton() {
    new AlaudaButton($('aui-dialog-footer button[class*="default"]')).click();
  }
}
