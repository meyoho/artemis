//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $ } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class DeleteDialogPage extends DevopsPageBase {
  get inputBox() {
    return new AlaudaInputbox($('aui-dialog input'));
  }
  /**
   * 删除按钮
   */
  get buttonDelete() {
    return new AlaudaButton($('aui-dialog-footer button[aui-button="danger"]'));
  }

  /**
   * 取消按钮
   */
  get buttonCancel() {
    return new AlaudaButton($('aui-dialog-footer button[auidialogclose]'));
  }

  /**
   * 删除未分配仓库的工具链
   */
  get dialog_delete() {
    return new AlaudaButton(
      $(
        '.aui-confirm-dialog__button-wrapper .aui-confirm-dialog__confirm-button',
      ),
    );
  }

  /**
   *取消删除未分配仓库的工具链
   */
  get dialog_Cancel() {
    return new AlaudaButton(
      $(
        '.aui-confirm-dialog__button-wrapper .aui-confirm-dialog__cancel-button',
      ),
    );
  }

  /**
   * 删除工具链
   * @param name 名称
   */
  delete(name) {
    this.inputBox.input(name);
    this.buttonDelete.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  isPresent() {
    return $('aui-dialog input').isPresent();
  }
}
