//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, element, by, browser } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { BindIntegrationToolPage } from './bind.integration.tool.page';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class UpdateDialogPage extends DevopsPageBase {
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case 'API 地址':
      case '访问地址':
      case '凭据名称':
      case '显示名称':
      case '用户名':
      case 'Token':
        return this.getElementByText_xpath(text, 'input');
      case '描述':
        return this.getElementByText_xpath(text, 'textarea');
      case '凭据':
        return new AlaudaDropdown(
          this.getElementByText_xpath(text, 'aui-select'),
          $$('.aui-option'),
        );
      case '类型':
      case '认证方式':
        return new AlaudaRadioButton(element(by.xpath('//aui-radio-group')));
    }
  }

  /**
   * 绑定页
   */
  get bindIntegrationToolPage() {
    return new BindIntegrationToolPage();
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case 'API 地址':
      case '访问地址':
      case '描述':
      case '凭据名称':
      case '显示名称':
      case '用户名':
      case 'Token':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '凭据':
        (this.getElementByText(name) as AlaudaDropdown).input(value);
        break;
      case '添加凭据':
        this.getButtonByText('添加凭据').click();
        break;
      case '类型':
      case '认证方式':
        (this.getElementByText(name) as AlaudaRadioButton).clickByName(value);
        break;
      case '创建':
        this.getButtonByText('创建').click();
        browser.sleep(2000);
        break;
      case '选择镜像repo':
        const imagerepo = new AlaudaDropdown(
          $('.select-line input'),
          $$('.aui-tree-node .aui-tree-node__title'),
        );
        imagerepo.select(value);
        break;
      case '点击添加地址':
        const okclick = new AlaudaButton(
          $('.select-line .aui-button__content'),
        );
        okclick.click();
        break;
      case '自动同步全部仓库':
        const checkBox = new AlaudaAuiCheckbox($('div aui-checkbox'));
        if (String(value).toLowerCase() === 'true') {
          checkBox.check();
        } else {
          checkBox.uncheck();
        }
        break;
      case '请选择仓库':
        const multi = new AuiMultiSelect(
          $('div aui-multi-select'),
          $('div aui-tooltip'),
        );
        value.forEach(codeRegistry => {
          multi.input(codeRegistry);
        });
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 更新一个工具链
   * @param name 工具链的名称
   * @param data 更新的内容
   */
  update(data) {
    this.fillForm(data);
    this.clickUpdateButton();
  }

  clickUpdateButton() {
    new AlaudaButton($('aui-dialog button[class*="primary"]')).click();
  }

  clickCancelButton() {
    new AlaudaButton($('aui-dialog button[class*="default"]')).click();
  }

  /**
   * 绑定详情页，分配仓库|分配项目按钮
   *
   */
  clickAssignButton() {
    new AlaudaButton(
      $('.acl-disabled-container span[class*="aui-button__content"]'),
    ).click();
  }

  /**
   * 绑定详情页，分配仓库|分配项目按钮后，更新按钮
   */
  clickAssignUpdateButton() {
    new AlaudaButton($('.aui-dialog__footer button[class*="primary"]')).click();
  }

  /**
   * 项目下，绑定列表页和绑定详情页过滤框
   */
  get input_search() {
    return new AlaudaInputbox($('.aui-search__input'));
  }

  /**
   * 绑定列表页，过滤框输入存在的过滤项
   */
  bindListPage_Exist(binName) {
    return new AlaudaList(by.xpath('//div[@title="' + binName + '"]/..'));
  }

  /**
   * 绑定列表页，过滤框输入不存在的过滤项
   */
  get bindListPage_empty() {
    return new AlaudaText(by.css('div.empty-list-hint'));
  }
}
