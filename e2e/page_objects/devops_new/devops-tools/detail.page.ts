//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, by, element } from 'protractor';

import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { DevopsPageBase } from '../devops.page.base';
import { UpdateDialogPage } from './dialog.update';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { DevopsToolListPage } from './list.page';
import { DeleteDialogPage } from './dialog.delete';
import { ConfirmDialogPage } from './dialog.confirm';
import { ErrorDialogPage } from './dialog.error';
import { BindIntegrationToolPage } from './bind.integration.tool.page';
import { SelectProjectDialogPage } from './dialog.select.project';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class DevopsToolDetailPage extends DevopsPageBase {
  /**
   * 页面右上角 3个点 下拉框
   */
  get menuTrigger() {
    return new AlaudaDropdown(
      $('alo-menu-trigger button[class*="trigger"]'),
      $$('aui-menu-item button'),
    );
  }

  get bindTable() {
    return new AlaudaAuiTable($('div aui-table'));
  }

  /**
   * 列表页
   */
  get listPage() {
    return new DevopsToolListPage();
  }

  /**
   * 删除 dialog, 包含输入框确认（应该适用解绑）
   */
  get deleteDialog() {
    return new DeleteDialogPage();
  }

  /**
   * 删除确认框
   */
  get confirmDialog() {
    return new ConfirmDialogPage();
  }

  /**
   * 页面右上角，错误提示框
   */
  get errorDialog() {
    return new ErrorDialogPage();
  }

  /**
   * 绑定工具链页面，包含绑定账号，分配代码库
   */
  get bindIntergratePage() {
    return new BindIntegrationToolPage();
  }

  /**
   * 选择项目 dialog 页
   */
  get selectProjectDialogPage() {
    return new SelectProjectDialogPage();
  }

  /**
   * 更新基本信息弹出的修改dialog
   */
  get updateDialog() {
    return new UpdateDialogPage();
  }
  _getElementByText(text, tag) {
    const xpath = `//div[contains(@class,"field")]//label[normalize-space(text())="${text}" ]/ancestor::div[contains(@class,"field")]/${tag}`;
    return new AlaudaLabel(element(by.xpath(xpath)));
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '类型':
        return this._getElementByText(text, 'span/parent::div');
      case 'API 地址':
        return this._getElementByText(text, 'span');
      case '访问地址':
        return this._getElementByText(text, 'a/span');

      case '集成名称':
      case '凭据':
        return this._getElementByText(text, '/a');
      case '仓库类型':
        return this._getElementByText(text, 'label/parent::div');
      case '描述':
      case '认证方式':
        return this._getElementByText(text, 'span');
    }
  }

  //harbor绑定页面
  _getElementByText_harbor(text, tag = "*[@class='value']") {
    const xpath = `//li[contains(@class,"info-item")]//span[normalize-space(text())="${text}"]/ancestor::li[contains(@class,"info-item")]//${tag}`;
    return new AlaudaLabel(element(by.xpath(xpath)));
  }

  /**
   *
   * @param harbor绑定详情页面获取数据
   */
  getElementByText_harbor(text: string): any {
    switch (text) {
      case '集成名称：':
      case '凭据：':
      case '绑定时间：':
      case '描述：':
        return this._getElementByText_harbor(text);
      case '仓库类型：':
        return this._getElementByText_harbor(
          text,
          'span[contains(@class,"label")]/parent::li',
        );
    }
  }

  /**
   * 更新基本信息
   * 更新基本信息
   * @param data
   */
  updateBasicInfo(data) {
    this.fillUpdateBasicInfo(data);
    this.updateDialog.clickUpdateButton();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  fillUpdateBasicInfo(data) {
    this.listPage.enterDetail(data['工具链名称']);
    this.menuTrigger.select('更新基本信息');
    this.updateDialog.fillForm(data);
  }

  /**
   * 删除一个工具链
   * @param name 名称
   */
  delete(name) {
    this.listPage.enterDetail(name);
    this.menuTrigger.select('删除');
    this.deleteDialog.isPresent().then(isPresent => {
      if (isPresent) {
        this.deleteDialog.delete(name);
      } else {
        this.confirmDialog.delete();
      }
    });
  }

  bind(data) {
    this.listPage.enterDetail(data['工具链名称']);
    this.getButtonByText('绑定').click();

    this.selectProjectDialogPage.select(data['项目名称']);
    this.bindIntergratePage.bind(data);
  }

  unbind(name, data) {
    this.listPage.enterDetail(name);
    this.menuTrigger.scrollToBoWindowBottom();
    const rowElem = this.bindTable.getRow([data['名称'], data['项目名称']]);
    new AlaudaDropdown(
      rowElem.$('alo-menu-trigger aui-icon'),
      $$('aui-menu-item button'),
    ).select('解除绑定');
    this.confirmDialog.buttonConfirm.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   *
   * @param name 集成详情页解除已分配仓库或项目的绑定
   * @param data
   */
  unAssignbind(name, data) {
    this.listPage.enterDetail(name);
    this.menuTrigger.scrollToBoWindowBottom();
    const rowElem = this.bindTable.getRow([data['名称'], data['项目名称']]);
    new AlaudaDropdown(
      rowElem.$('alo-menu-trigger aui-icon'),
      $$('aui-menu-item button'),
    ).select('解除绑定');
    this.deleteDialog.delete(data['名称']);
  }

  //解除绑定确认框点击取消按钮
  unbind_cancel(name, data) {
    this.listPage.enterDetail(name);
    this.menuTrigger.scrollToBoWindowBottom();
    const rowElem = this.bindTable.getRow([data['名称'], data['项目名称']]);
    new AlaudaDropdown(
      rowElem.$('alo-menu-trigger aui-icon'),
      $$('aui-menu-item button'),
    ).select('解除绑定');
    this.confirmDialog.buttonCancel.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  isPresent(titleName) {
    return element(
      by.xpath(
        `//div[@class="header-title"]//span[normalize-space(text())="${titleName}" ]`,
      ),
    ).isPresent();
  }
}
