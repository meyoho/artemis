//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, by, element } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class SelectProjectDialogPage extends DevopsPageBase {
  get searchBox() {
    return new AuiSearch($('aui-dialog aui-search'));
  }

  click(name) {
    const xpath = `//aui-dialog-content//div[@class="name" and normalize-space(text())="${name}"]`;
    const elem = element(by.xpath(xpath));
    this.waitElementPresent(
      elem,
      `项目列表没有定位到 项目 ${name}, ${elem.locator()}`,
    );
    new AlaudaButton(elem).click();
  }

  select(name) {
    //this.searchBox.search(name);
    this.click(name);
  }
}
