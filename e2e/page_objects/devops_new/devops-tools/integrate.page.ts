/**
 * Created by zhangjiao on 2018/9/4.
 */
import { by } from 'protractor';
import { IntergrateDialog } from './dialog.integrate';
import { DevopsPageBase } from '../devops.page.base';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class IntergratePage extends DevopsPageBase {
  /**
   * 集成devops 工具链的dialog 页面
   */
  get intergrateDialog() {
    return new IntergrateDialog();
  }

  /**
   * 集成一个Devops工具
   * @param data
   */
  integrate(data) {
    this.fillForm(data);
    this.intergrateDialog.clickIntegrateButton();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 集成一个Devops工具
   * @param data
   */
  fillForm(data) {
    this.clickLeftNavByText('DevOps 工具链');
    this.waitProgressBarNotPresent();
    this.getButtonByText('集成').click();
    this.intergrateDialog.fillForm(data);
  }

  /**
   * 集成页面必填项校验
   */
  get integrate_input_content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      'input',
      '.aui-form-item__error',
    );
  }
}
