/**
 * Created by zhangjiao on 2018/9/4.
 */
import { IntergratePage } from './integrate.page';
import { DevopsToolDetailVerify } from '@e2e/page_verify/devops/devops_tool/detail.page';
import { DevopsToolPreparePage } from './prepare.page';
import { DevopsPageBase } from '../devops.page.base';
import { DevopsToolListPage } from './list.page';
import { DevopsToolDetailPage } from './detail.page';
import { UpdateDialogPage } from './dialog.update';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class DevopsToolPage extends DevopsPageBase {
  /**
   * 集成devops 工具链的页面
   */
  get intergratePage() {
    return new IntergratePage();
  }

  /**
   * devops 工具链的列表页
   */
  get listPage() {
    return new DevopsToolListPage();
  }

  /**
   * 详情页
   */
  get detailPage() {
    return new DevopsToolDetailPage();
  }

  /**
   * 详情页验证类
   */
  get detailVerifyPage() {
    return new DevopsToolDetailVerify();
  }

  get preparePage() {
    return new DevopsToolPreparePage();
  }

  get updateDialogPage() {
    return new UpdateDialogPage();
  }
}
