/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, element, by } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { PageBase } from '../../page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AddSecretDialog } from './dialog.add.secret';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class BindIntegrationToolPage extends PageBase {
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ):
    | AlaudaRadioButton
    | AlaudaDropdown
    | AlaudaInputbox
    | AlaudaAuiCheckbox
    | AuiMultiSelect
    | AlaudaButton {
    switch (text) {
      case '描述':
        return new AlaudaInputbox(
          this.getElementByText_xpath(text, 'textarea'),
        );
      case '认证方式':
        return new AlaudaRadioButton(element(by.xpath('//aui-radio-group')));
      case '凭据':
        return new AlaudaDropdown(
          this.getElementByText_xpath(text, 'aui-select'),
          $$('.aui-option'),
        );
      case '名称':
        return new AlaudaInputbox(this.getElementByText_xpath(text, 'input'));
      // case '自动同步全部仓库':
      //   return new AlaudaAuiCheckbox($('div aui-checkbox'));
      // case '请选择仓库':
      //   return new AuiMultiSelect(
      //     $('div aui-multi-select'),
      //     $('div aui-tooltip'),
      //   );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '认证方式':
        (this.getElementByText(name) as AlaudaRadioButton).clickByName(value);
        break;
      case '凭据':
        (this.getElementByText(name) as AlaudaDropdown).input(value);
        break;
      case '名称':
      case '描述':
        (this.getElementByText(name) as AlaudaInputbox).input(value);
        break;
      case '分配代码仓库':
        this.primaryButton.click();
        for (const codeKey in value) {
          switch (codeKey) {
            case '自动同步全部仓库':
              const checkBox = new AlaudaAuiCheckbox($('div aui-checkbox'));
              if (String(value[codeKey]).toLowerCase() === 'true') {
                checkBox.check();
              } else {
                checkBox.uncheck();
              }
              break;
            case '请选择仓库':
              const multi = new AuiMultiSelect(
                $('div aui-multi-select'),
                $('div aui-tooltip'),
              );
              value[codeKey].forEach(codeRegistry => {
                multi.input(codeRegistry);
              });

              break;
          }
        }
        break;
      case '选择镜像repo':
        this.primaryButton.click();
        const imagerepo = new AlaudaDropdown(
          $('.select-line input'),
          $$('.aui-tree-node .aui-tree-node__title'),
        );
        imagerepo.select(value);
        break;
      case '点击添加地址':
        const okclick = new AlaudaButton(
          $('.select-line .aui-button__content'),
        );
        okclick.click();
        break;
      case '删除已添加的地址':
        const repo_list_table = new AlaudaAuiTable(
          $('.aui-card__content aui-table'),
        );
        repo_list_table
          .getRow(value)
          .element(by.css('.remove-button .aui-icon'))
          .click();
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 凭据名称: '', 显示名称: '', 类型: '',用户名: '', 密码: '' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  get primaryButton() {
    return new AlaudaButton($(`.actions button[class*="primary"]`));
  }

  get cancelButton() {
    return new AlaudaButton($(`.actions button:not([class*="primary"])`));
  }

  /**
   * 绑定工具
   * @param data 测试数据
   */
  bind(data) {
    this.fillForm(data);
    this.primaryButton.click();
    // 等待状态消失
    this.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
    );
  }

  get dialogAddSecret() {
    return new AddSecretDialog();
  }

  /**
   * 添加凭据
   * @param data 测试数据
   */
  addSecret(data) {
    this.getButtonByText('添加凭据').click();
    this.dialogAddSecret.create(data);
  }
}
