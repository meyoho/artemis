//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { by, element } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

/**
 * DevOps 工具链， 列表页
 */
export class DevopsToolListPage extends DevopsPageBase {
  /**
   * 单击名称，进入工具的详情页
   * @param name
   */
  enterDetail(name) {
    this.clickLeftNavByText('DevOps 工具链');
    this.waitProgressBarNotPresent();
    const xpath = `//div[@title and normalize-space(text())="${name}" ]`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  clickByTypeBarName(name) {
    const xpath = `//alo-tool-type-bar//button//span[text()="${name}"]`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  /**
   * 判断列表页是否存在
   */
  isPresent() {
    return element(by.xpath('//alo-tool-type-bar')).isPresent();
  }

  /**
   * 工具链是否存在
   * @param name 名称
   */
  isExist(name) {
    return element(
      by.xpath(`//div[@title and normalize-space(text())="${name}" ]`),
    ).isPresent();
  }
}
