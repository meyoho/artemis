/**
 * Created by zhangjiao on 2018/9/4.
 */
import { PageBase } from '../../page.base';
import { CommonApi } from '@e2e/utility/common.api';
import {
  alauda_type_jenkins,
  alauda_coderepo_service,
  alauda_image_registry,
} from '@e2e/utility/resource.type.k8s';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class DevopsToolPreparePage extends PageBase {
  /**
   * 删除jenkins 资源
   * @param name
   */
  deleteJenkins(name) {
    CommonApi.deleteResource(name, alauda_type_jenkins, null);
  }

  deleteCodeRepoService(name) {
    CommonApi.deleteResource(name, alauda_coderepo_service, null);
  }

  deleteharbor(name) {
    CommonApi.deleteResource(name, alauda_image_registry, null);
  }

  deleteCodeRepoBinding(name: string, namespace: string) {
    try {
      CommonApi.deleteResource(name, 'CodeRepoBinding', namespace);
    } catch (e) {
      console.log(e.message);
    }
  }

  deleteJenkinsBinding(name: string, namespace: string) {
    try {
      CommonApi.deleteResource(name, 'jenkinsbinding', namespace);
    } catch (e) {
      console.log(e.message);
    }
  }

  deleteimageRepoBinding(name: string, namespace: string) {
    try {
      CommonKubectl.execKubectlCommand(
        `kubectl delete irb -n ${namespace} ${name}`,
      );
      //CommonApi.deleteResource(name, 'irb', namespace);
    } catch (e) {
      console.log(e.message);
    }
  }
}
