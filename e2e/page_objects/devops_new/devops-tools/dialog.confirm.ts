//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $ } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class ConfirmDialogPage extends DevopsPageBase {
  /**
   * 删除按钮
   */
  get buttonConfirm() {
    return new AlaudaButton($('aui-confirm-dialog button[class *="danger"]'));
  }

  /**
   * 取消按钮
   */
  get buttonCancel() {
    return new AlaudaButton($('aui-confirm-dialog button[class *="cancel"]'));
  }

  /**
   * 删除工具链
   * @param name 名称
   */
  delete() {
    this.buttonConfirm.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
