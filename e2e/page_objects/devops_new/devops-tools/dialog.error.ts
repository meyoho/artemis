//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $ } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class ErrorDialogPage extends DevopsPageBase {
  get title() {
    return new AlaudaLabel($('aui-notification div[class*="title"]'));
  }

  get message() {
    return new AlaudaLabel($('aui-notification div[class*="content"]'));
  }

  isPresent() {
    return this.title.isPresent();
  }
}
