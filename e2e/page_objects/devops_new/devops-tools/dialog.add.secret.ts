/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $$ } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { PageBase } from '../../page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

/**
 * DevOps 工具链， 单击集成后弹出的dialog 页面
 */
export class AddSecretDialog extends PageBase {
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ): AlaudaRadioButton | AlaudaDropdown | AlaudaInputbox {
    switch (text) {
      case '类型':
        return new AlaudaRadioButton(
          this.getElementByText_xpath(text, 'aui-radio-button'),
        );
      case '凭据':
        return new AlaudaDropdown(
          this.getElementByText_xpath(text, 'aui-select'),
          $$('.aui-option'),
        );
      default:
        return new AlaudaInputbox(this.getElementByText_xpath(text, 'input'));
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '类型':
        (this.getElementByText(name) as AlaudaRadioButton).clickByName(value);
        break;
      case '凭据':
        (this.getElementByText(name) as AlaudaDropdown).input(value);
        break;
      default:
        (this.getElementByText(name) as AlaudaInputbox).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 凭据名称: '', 显示名称: '', 类型: '',用户名: '', 密码: '' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 创建保密字典
   * @param data 测试数据 { 凭据名称: '', 显示名称: '', 类型: '',用户名: '', 密码: '' }
   */
  create(data) {
    this.fillForm(data);
    this.getButtonByText('创建').click();
  }
}
