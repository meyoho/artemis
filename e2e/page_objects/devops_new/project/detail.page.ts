//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, by, element, browser } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { ProjectListPage } from './list.page';
import { BindIntegrationToolPage } from '../devops-tools/bind.integration.tool.page';
import { IntergrateDialog } from '../devops-tools/dialog.integrate';
import { ErrorDialogPage } from '../devops-tools/dialog.error';
import { DevopsToolDetailPage } from '../devops-tools/detail.page';
import { DeleteDialogPage } from '../devops-tools/dialog.delete';
import { UpdateDialogPage } from '../devops-tools/dialog.update';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

/**
 * 项目, 列表页
 */
export class ProjectDetailPage extends DevopsPageBase {
  getElementByText(name) {
    return element(
      by.xpath(`//aui-tab-group//*[normalize-space(text())="${name}" ]`),
    );
  }
  clickByName(name) {
    return new AlaudaButton(this.getElementByText(name)).click();
  }

  get errorDialog() {
    return new ErrorDialogPage();
  }

  get listPage() {
    return new ProjectListPage();
  }

  get bindIntergratePage() {
    return new BindIntegrationToolPage();
  }

  get devopsToolDetailPage() {
    return new DevopsToolDetailPage();
  }

  get intergrateDialog() {
    return new IntergrateDialog();
  }

  get deleteDialog() {
    return new DeleteDialogPage();
  }

  get updateDialogPage() {
    return new UpdateDialogPage();
  }

  /**
   * 页面右上角 3个点 下拉框
   */
  get menuTrigger() {
    return new AlaudaDropdown(
      $('alo-menu-trigger button[class*="trigger"]'),
      $$('aui-menu-item button'),
    );
  }

  fillBindInfo(data) {
    // 从列表页，单击名称进入详情页
    this.listPage.enterDetail(data['项目名称']);

    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );
    // 单击绑定按钮
    this.getButtonByText('绑定').click();
    // 在绑定的 dialog 页， 单击工具链的名称， 打开绑定页面
    this.intergrateDialog.clickToolByName(data['工具链名称']);
    // 在绑定的页面上，填写绑定信息
    this.bindIntergratePage.fillForm(data);
  }

  /**
   * 项目详情页，绑定一个项目
   * @param data
   */
  bind(data) {
    this.fillBindInfo(data);
    // 单击绑定 按钮，绑定工具链
    this.bindIntergratePage.primaryButton.click();

    // 分配仓库: { 自动同步全部仓库: 'true' },
    for (const key in data) {
      if (key === '分配代码仓库') {
        {
          // 等待状态消失
          this.waitElementNotPresent(
            $('div .status aui-icon'),
            '【代码仓库获取中...】的提示没有消失',
          );

          this.toast.getMessage().then(message => {
            console.log(message);
          });
          //验证 同步全部仓库
          console.log('等待同步全部仓库');
          browser.wait(
            () => {
              return this.devopsToolDetailPage.bindTable
                .getRowCount()
                .then(rowCount => {
                  return rowCount > 0;
                });
            },
            30 * 1000,
            '同步代码仓库失败',
          );
        }
      }
    }
  }

  // /**
  //  * 项目详情页，绑定harbor
  //  * @param data
  //  */
  // bind2(data) {
  //   this.fillBindInfo(data);
  //   // 单击绑定 按钮，绑定工具链
  //   console.log('this.bindIntergratePage.primaryButton2.click();');
  //   this.bindIntergratePage.primaryButton2.click();
  // }

  /**
   * 项目详情页，解绑一个工具链
   * @param data
   */
  unbind(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 单击工具链的名称，进入详情页
    this.clickByName(data['名称']);
    this.devopsToolDetailPage.menuTrigger.select('解除绑定');
    this.deleteDialog.delete(data['名称']);
  }

  /**
   * 项目详情页，解绑一个未分配仓库的工具链
   * @param data
   */
  unbind2(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 单击工具链的名称，进入详情页
    this.clickByName(data['名称']);
    this.devopsToolDetailPage.menuTrigger.select('解除绑定');
    this.deleteDialog.dialog_delete.click();
  }

  unbind2_cancel(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 单击工具链的名称，进入详情页
    this.clickByName(data['名称']);
    this.devopsToolDetailPage.menuTrigger.select('解除绑定');
    this.deleteDialog.dialog_Cancel.click();
  }

  /**
   * 项目详情页，判断项目是否绑定了，工具链
   * @param data
   */
  isBind(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 找到工具链
    return this.getElementByText(data['名称']).isPresent();
  }

  /**
   * 项目详情页，绑定详情页更新基本信息
   * @param data
   */
  updateBind(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 单击工具链的名称，进入详情页
    this.clickByName(data['名称']);
    this.menuTrigger.select('更新');
    this.updateDialogPage.update(data);
  }

  /**
   * 项目详情页，绑定详情页，分配项目|分配仓库
   * @param data
   */
  update_assignBind(data) {
    this.listPage.enterDetail(data['项目名称']);
    // 单击 "DevOps 工具链 " Tab
    this.clickByName('DevOps 工具链');
    // 等待状态消失
    this.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );

    // 单击工具链的名称，进入详情页
    this.clickByName(data['名称']);
    this.updateDialogPage.clickAssignButton(); //绑定详情页，点击分配仓库|分配项目按钮
    this.updateDialogPage.fillForm(data);
    this.updateDialogPage.clickAssignUpdateButton(); //点击更新按钮
    // 分配仓库: { 自动同步全部仓库: 'true' },
    for (const key in data) {
      if (key === '分配代码仓库' || key === '请选择仓库') {
        {
          // 等待状态消失
          this.waitElementNotPresent(
            $('div .status aui-icon'),
            '【代码仓库获取中...】的提示没有消失',
          );

          this.toast.getMessage().then(message => {
            console.log(message);
          });
          //验证 同步全部仓库
          console.log('等待同步全部仓库');
          browser.wait(
            () => {
              return this.devopsToolDetailPage.bindTable
                .getRowCount()
                .then(rowCount => {
                  return rowCount > 0;
                });
            },
            30 * 1000,
            '同步代码仓库失败',
          );
        }
      }
    }
  }
}
