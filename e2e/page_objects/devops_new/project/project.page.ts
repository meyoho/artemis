//

/**
 * Created by zhangjiao on 2018/9/4.
 */

import { DevopsPageBase } from '../devops.page.base';
import { ProjectListPage } from './list.page';
import { ProjectDetailPage } from './detail.page';
import { DevopsToolPage } from '../devops-tools/devops.tools.page';

/**
 * 项目, 列表页
 */
export class ProjectPage extends DevopsPageBase {
  get detailPage() {
    return new ProjectDetailPage();
  }

  get listPage() {
    return new ProjectListPage();
  }

  get devopsToolPage() {
    return new DevopsToolPage();
  }
}
