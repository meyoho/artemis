//

/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, by, element } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';

/**
 * 项目, 列表页
 */
export class ProjectListPage extends DevopsPageBase {
  /**
   * 单击名称，进入项目的详情页
   * @param name
   */
  enterDetail(name) {
    this.clickLeftNavByText('项目');
    this.waitProgressBarNotPresent();
    this.searchBox.search(name);
    const xpath = `//a//span[normalize-space(text())="${name}"]`;
    new AlaudaButton(element(by.xpath(xpath))).click();
  }

  get searchBox() {
    return new AuiSearch($('.alo-search aui-search'));
  }

  /**
   * 判断列表页是否存在
   */
  isPresent() {
    return element($('.alo-search aui-search')).isPresent();
  }

  /**
   * 工具链是否存在
   * @param name 名称
   */
  isExist(name) {
    return element(
      by.xpath(`//a//span[normalize-space(text())="${name}"]`),
    ).isPresent();
  }
}
