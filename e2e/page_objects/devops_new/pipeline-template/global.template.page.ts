/**
 * Created by liuzongyao on 2020/4/13.
 */

import { DevopsPageBase } from '../devops.page.base';
import { DetailGlobalTemplate } from './detail.global.template';
import { ConfigTemplateRepo } from './config.template.repo';
import { DevopsPreparePage } from '../devops.prepare.page';
import { OfficialTemplate } from '../../../page_objects/devops_new/pipeline-template/official.template.page';

/**
 * 项目, 列表页
 */
export class GlobalTemplatePage extends DevopsPageBase {
  get configTemplateRepo() {
    return new ConfigTemplateRepo();
  }

  get detailPage() {
    return new DetailGlobalTemplate();
  }

  get officialTemplate() {
    return new OfficialTemplate();
  }

  get preparePage(): DevopsPreparePage {
    return new DevopsPreparePage();
  }
}
