//

/**
 * Created by liuzongyao on 2020/4/24.
 */
import { by, element } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';

/**
 * 全局自定义模版主界面
 */
export class DetailGlobalTemplate extends DevopsPageBase {
  clickconfigicon() {
    element(
      by.xpath("//span[text()=' 自定义模版配置 ']/parent::div//button"),
    ).click();
  }

  getButton(text) {
    return element(by.xpath(`//button[text()=" ${text} "]`));
  }

  enterConfig(text) {
    this.clickconfigicon();
    this.getButtonByText(text).click();
  }
}
