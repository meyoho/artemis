/**
 * Created by liuzongyao on 2020/4/13.
 */

import { $, by, element, promise } from 'protractor';
import { DevopsPageBase } from '../devops.page.base';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';

/**
 * 项目, 列表页
 */
export class OfficialTemplate extends DevopsPageBase {
  get searchBox() {
    return new AuiSearch(
      $('.aui-search.aui-search--medium'),
      '.aui-search input',
      '.aui-icon.aui-icon-close_small',
      '.aui-icon.aui-icon-search_s',
    );
  }

  /**
   * 进入模版详情
   * @param templateName 模版名称
   */
  clickOfficalTemplate(templateName: string) {
    const xpath = `//a[text()=' ${templateName} ']`;
    const template = element(by.xpath(xpath));
    template.click();
  }
  /**
   * 搜素模版
   * @param projectName 模版名称
   */
  // searchTemplate(templateName: string) {
  //   const xpath = "//input[@placeholder='按显示名称搜索']";
  //   element(by.xpath(xpath)).sendKeys(templateName);
  //   const searchbutton = "//div[contains(@class,'aui-search__button-icon')]/aui-icon"
  //   element(by.xpath(searchbutton)).click();
  // }

  searchTemplate(templateName: string) {
    this.searchBox.search(templateName);
  }

  getTemplatecount(): promise.Promise<number> {
    const rowlist = element.all(by.xpath('//div[contains(@class,"name")]'));
    return rowlist.count().then(function(rowCount) {
      return rowCount;
    });
  }

  clickNextPage(pageName: number) {
    const page = element(by.xpath(`//button/span[text()=${pageName}]`));
    page.click();
  }
}
