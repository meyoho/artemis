import { PageBase } from './page.base';
import { $, $$, browser, promise } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';

/**
 * Created by liuwei on 2019/12/19.
 */

export class LoginPage {
  private _loginSuccessTimeout: number;
  private _waitLoadingDisappearTimeout: number;
  private _inputTimeout: number;
  private _enterUserViewTimeout: number;

  constructor() {
    this._loginSuccessTimeout = 0;
    this._waitLoadingDisappearTimeout = 0;
    this._inputTimeout = 0;
    this._enterUserViewTimeout = 0;
  }
  private get _page() {
    return new PageBase();
  }
  /**
   * 管理员登录按钮
   */
  get adminButton(): AlaudaButton {
    return new AlaudaButton($('div .dex-subtle-text'));
  }

  /**
   * 用户名输入框
   */
  get emailInput(): AlaudaInputbox {
    return new AlaudaInputbox($('form input[name=login]'));
  }

  /**
   * 密码输入框
   */
  get passwordInput(): AlaudaInputbox {
    return new AlaudaInputbox($('form input[id=password]'));
  }

  /**
   * 登录按钮
   */
  get loginButton(): AlaudaButton {
    return new AlaudaButton($('form button'));
  }

  /**
   * 登录成功了么
   */
  get isloginSuccess(): promise.Promise<boolean> {
    return this._page.waitElementPresent(
      this._page.productAction.root,
      '登陆后，切换产品的控件没出现',
      100,
    );
  }

  /**
   * 错误提示
   */
  private get _isInputError(): promise.Promise<boolean> {
    return $$('div .dex-error-box[style*="block"]')
      .get(0)
      .isPresent();
  }

  /**
   * 递归：登录页，输入用名，密码，单击登录按钮
   * @param isAdmin
   * @param name
   * @param password
   */
  private _inputForm(isAdmin: boolean, name: string, password: string) {
    if (isAdmin) {
      // 输入用户名，密码，单击登陆
      this.emailInput.input(name);
      this.passwordInput.input(password);
      this.loginButton
        .click()
        .then(() => {
          this._isInputError.then(isPresent => {
            if (isPresent) {
              // 如果有错误（例如，用户名没输入就单击登陆按钮了），重试一次
              console.log(
                `管理员登录页面，输入用户名，密码错误，重试${this
                  ._inputTimeout++}次`,
              );
              this._inputForm(isAdmin, name, password);
              if (this._inputTimeout > 3) {
                throw new Error('管理员登录页面，输入用户名，密码失败');
              }
            }
          });
        })
        .catch(reason => {
          console.log(`登录输入用户名，密码异常：${reason}`);
        });
    } else {
      // 普通用户登陆
    }
    return browser.sleep(1);
  }

  /**
   * 从浏览器中获取token
   */
  private async _getToke(): Promise<string> {
    return await browser
      .executeScript('return localStorage.getItem("token");')
      .then(token => {
        this._page.token = String(token);
        return this._page.token;
      });
  }

  /**
   * 将toen 写到临时文件中保存
   */
  private _writeTokeToFile(): Promise<void> {
    return this._getToke().then(token => {
      console.log('登录后的token:' + token);
      if (token !== 'null') {
        console.log(
          '将token 保存到文件:' + CommonMethod.writeyamlfile('token', token),
        );
      }
    });
  }

  /**
   * 本地保存Token 了么
   */
  hasToken(): boolean {
    return (
      CommonMethod.isExistFile('token') &&
      CommonMethod.readyamlfile('temp/token') !== 'null' &&
      CommonMethod.readyamlfile('temp/token') !== '' &&
      CommonMethod.readyamlfile('temp/token') !== 'undefined'
    );
  }

  /**
   * 递归：等待加载图标消失
   */
  async waitLoadingDisappear() {
    return await this._page
      .waitElementNotPresent(
        $('.index-loading-spinner'),
        'loading bar not disappear',
      )
      .then(isDisappear => {
        if (!isDisappear) {
          browser.refresh();
          browser.sleep(1000);
          console.log(
            `Loading 图标没有消失，刷新${this
              ._waitLoadingDisappearTimeout++}次页面`,
          );
          if (this._waitLoadingDisappearTimeout > 5) {
            throw new Error('等待Loading 图标消失超时了');
          }
          this.waitLoadingDisappear();
        }
      });
  }

  /**
   * 递归：等待切换产品图标出现
   */
  waitLoginSuccess(timeout = 5) {
    this.isloginSuccess.then(success => {
      if (!success) {
        console.log(`刷新 ${this._loginSuccessTimeout++} 次页面`);
        browser.refresh();
        browser.sleep(1000);
        this.waitLoadingDisappear();
        if (this._loginSuccessTimeout > timeout) {
          throw new Error('等待登录成功超时了');
        }
        this.waitLoginSuccess();
      }
    });
  }

  /**
   * 登录业务视图打开
   * @param url 业务视图的URL
   * @param keyUrl 判断业务视图打开的URL 中的关键字
   */
  waitEnterUserView(url, keyUrl) {
    browser.getCurrentUrl().then(currentUrl => {
      if (!currentUrl.includes(keyUrl)) {
        browser.get(url);
        browser.sleep(1000);
        if (this._enterUserViewTimeout === 0) {
          console.log(`打开业务视图 ${url}`);
        } else {
          console.log(`重试打开业务视图 ${this._enterUserViewTimeout} 次`);
        }
        this.waitLoadingDisappear();
        this.waitLoginSuccess();
        if (this._enterUserViewTimeout > 5) {
          throw new Error(`打开业务视图 ${url} 超时了`);
        }
        this.waitEnterUserView(url, keyUrl);
      }
    });
  }

  /**
   * 打开网页
   */
  start() {
    console.log('首次登录');
    console.log(`baseUrl is ${browser.baseUrl}`);
    browser.get(browser.baseUrl).then(() => {
      browser.getCurrentUrl().then(url => {
        console.log('current URL : ' + url);
      });
    });
    this.waitLoadingDisappear();
  }

  /**
   * 使用token 打开网页
   */
  startWithToken() {
    const token: string = CommonMethod.readyamlfile('temp/token');
    console.log('token 已经存在了');
    console.log(`打开 ${browser.baseUrl}?id_token=${token}`);
    browser.get(`${browser.baseUrl}?id_token=${token}`);
    this.waitLoadingDisappear();
    this.waitLoginSuccess();
    browser.getCurrentUrl().then(url => {
      console.log('当前URL: ' + url);
    });
  }

  /**
   * 登陆
   * @param name 用户名
   * @param password 密码
   * @param isAdmin 是否是管理员登陆，默认是
   * @example login()
   */
  login(
    name: string = ServerConf.ADMIN_USER,
    password: string = ServerConf.ADMIN_PASSWORD,
    isAdmin = true,
  ) {
    if (!this.hasToken()) {
      this.start();
      if (isAdmin) {
        // 单击管理员登陆按钮
        this.adminButton.isPresent().then(isPresent => {
          if (isPresent) {
            this.adminButton.click();
          }
        });
      }

      this._inputForm(isAdmin, name, password);
      this.waitLoadingDisappear();
      this.waitLoginSuccess();
      this.isloginSuccess
        .then(success => {
          if (success) {
            this._writeTokeToFile();
          }
        })
        .catch(ex => {
          console.log(`登录成功后,保存token 异常：${ex}`);
        });
    } else {
      this.startWithToken();
    }
  }

  logout() {
    //单击用户名，弹出下拉框
    this._page.accountAction.account.click();
    // 在下拉框中，退出登录
    const item = this._page.accountAction.itemList
      .filter(elem => {
        return elem.getText().then(text => {
          return text === '退出登录';
        });
      })
      .first();

    new AlaudaButton(item).click();
    const confirmDialog = new AuiConfirmDialog(
      $('aui-dialog aui-confirm-dialog'),
    );
    confirmDialog.isPresent().then(isPresent => {
      if (isPresent) {
        confirmDialog.clickConfirm();
      }
    });
  }
}
