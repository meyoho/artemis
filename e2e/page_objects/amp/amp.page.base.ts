/**
 * Created by liuwei on 2019/6/13.
 */

import { ServerConf } from '../../config/serverConf';
import { PageBase } from '../page.base';

export class AmpPageBase extends PageBase {
    /**
     * 生成测试数据
     * @param prefix 前缀
     * @example getTestData('asm');
     */
    getTestData(prefix: string = ''): string {
        return `amp-${ServerConf.BRANCH_NAME}-${prefix}`;
    }
}
