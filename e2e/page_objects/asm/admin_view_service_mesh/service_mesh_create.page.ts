import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AsmPageBase } from '../asm.page.base';

export class ServiceMeshCreatePage extends AsmPageBase {
  constructor() {
    super();
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  getElementByText(name, tagname = 'input'): AlaudaInputbox {
    return new AlaudaInputbox(this.getElementByText_xpath(name, tagname));
  }

  enterValue(name: string, value, tagname = 'input') {
    this.getElementByText(name, tagname).input(value);
  }

  fillForm(testData) {
    for (const key in testData) {
      this.enterValue(key, testData[key]);
    }
  }
}
