import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$ } from 'protractor';
import { AsmPageBase } from '../asm.page.base';
import { ServiceMeshCreatePage } from './service_mesh_create.page';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';

export class ServiceMeshDetailPage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('.alo-detail__field', '.alo-detail__field label');
  }

  /**
   * 操作按钮
   */
  get actionDropDown(): AlaudaDropdown {
    const dropdown = new AlaudaDropdown(
      $('alo-service-mesh-info button'),
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }
  /**
   * 确认框
   */
  get dialog(): AuiDialog {
    return new AuiDialog(
      $('aui-confirm-dialog'),
      '.aui-dialog__content',
      '.aui-confirm-dialog__confirm-button',
    );
  }

  /**
   * 更新服务网格
   * @param testData
   */
  updateServiceMesh(testData) {
    const createPage = new ServiceMeshCreatePage();
    this.actionDropDown.select('更新');
    createPage.fillForm(testData);
    createPage.getButtonByText('更新').click();
    this.dialog.clickConfirm();
  }
}
