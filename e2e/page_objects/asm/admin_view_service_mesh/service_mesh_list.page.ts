import { AsmPageBase } from '../asm.page.base';
import { $ } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';

export class ServiceMeshListPage extends AsmPageBase {
  get serviceMeshTable(): AuiTableComponent {
    return new AuiTableComponent($('.aui-card'), 'aui-table', '.list-header');
  }
}
