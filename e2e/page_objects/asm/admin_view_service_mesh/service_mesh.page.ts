import { AsmPageBase } from '../asm.page.base';
import { ServiceMeshListPage } from './service_mesh_list.page';
import { ServiceMeshDetailPage } from './service_mesh_detail.page';
import { ServiceMeshCreatePage } from './service_mesh_create.page';

export class ServiceMeshPage extends AsmPageBase {
  get listPage() {
    return new ServiceMeshListPage();
  }

  get detailPage() {
    return new ServiceMeshDetailPage();
  }

  get createPage() {
    return new ServiceMeshCreatePage();
  }
}
