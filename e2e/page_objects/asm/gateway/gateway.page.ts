import { AsmPageBase } from '../asm.page.base';

import { GatewayCreatePage } from './gataway_create.page';
import { GatawayDetailPage } from './gataway_detail.page';
import { GatewayListPage } from './gataway_list.page';

export class GatewayPage extends AsmPageBase {
    get createPage(): GatewayCreatePage {
        return new GatewayCreatePage();
    }

    get detailPage(): GatawayDetailPage {
        return new GatawayDetailPage();
    }

    get listPage(): GatewayListPage {
        return new GatewayListPage();
    }
}
