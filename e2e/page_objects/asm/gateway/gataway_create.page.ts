/**
 * Created by lrz on 2019/06/18.
 * 创建Istio网关页面
 */
import { ElementFinder, browser } from 'protractor';

import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AsmPageBase } from '../asm.page.base';
import { AlaudaAuiSelectDouble } from '@e2e/element_objects/alauda.aui_select_double';

export class GatewayCreatePage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  /**
   * 根据左侧文字获取右侧页面元素
   * @param text 左侧文字
   * @param tagname
   */
  getElementByText(
    text: string,
    tagname = 'input',
  ): AlaudaAuiSelectDouble | AlaudaInputbox {
    switch (text) {
      case '微服务':
        const microService: ElementFinder = super.getElementByText(
          text,
          'div[class *= multi-input]',
        );
        return new AlaudaAuiSelectDouble(microService);
      case '名称':
      case '显示名称':
      case '网关入口':
        return new AlaudaInputbox(this.alaudaElement.getElementByText(text));
      //   case '条件规则':
      //     const conditonRuleElement: ElementFinder = super.getElementByText(
      //       text,
      //       'alo-condition-rule',
      //     );
      //     return new ConDitionRule(conditonRuleElement);
      //   case '权重规则':
      //     const weightElement = super.getElementByText(
      //       text,
      //       'alo-weight-rule alo-array-form-table',
      //     );
      //     return new ArrayFormTable(weightElement);
      default:
        return new AlaudaInputbox(super.getElementByText(text, tagname));
    }
  }

  /**
   * 在文本框中输入值
   * @param name 文本框左侧文字
   * @param value 输入文本框中的值
   * @param tagname
   */
  enterValue(name: string, value, tagname = 'input') {
    switch (name) {
      case '微服务':
        const microService = this.getElementByText(
          name,
        ) as AlaudaAuiSelectDouble;
        microService.select(value);
        break;
      case '名称':
      case '显示名称':
        const inputbox = this.getElementByText(name) as AlaudaInputbox;
        inputbox.input(value);
        break;
      case '网关入口':
        const tagsInput = this.getElementByText(
          name,
          tagname,
        ) as AlaudaInputbox;
        tagsInput.input(value);
        break;
      //   case '条件规则':
      //     const conditionRule: ConDitionRule = this.getElementByText(name);
      //     conditionRule.input(value);
      //     break;
      //   case '权重规则':
      //     const arrayFormTable: ArrayFormTable = this.getElementByText(name);
      //     arrayFormTable.fill(value);
      //     break;
      default:
        (this.getElementByText(name, tagname) as AlaudaInputbox).input(value);
    }
  }

  /**
   * 填写表格
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 创建Istio网关
   * @param testData 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  createGateway(testData) {
    this.getButtonByText('创建网关').click();
    // browser.refresh();
    this.fillForm(testData);
    this.getButtonByText('创建').click();
    browser.sleep(100);
  }
}
