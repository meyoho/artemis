/**
 * Created by lrz on 2019/06/17.
 * gataway详情页
 */

import {
  $,
  $$,
  ElementArrayFinder,
  ElementFinder,
  browser,
  promise,
} from 'protractor';

import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaToast } from '../../../element_objects/alauda.toast';
import { Dialog } from '../../asm/dialog.page';
import { AsmPageBase } from '../asm.page.base';
import { GatewayCreatePage } from './gataway_create.page';
import { RouteCreatePage } from '../route/route_create.page';
import { RouteDetailPage } from '../route/route_detail.page';

class ConditionsRule {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    this._root = root;
  }

  get conditionRule() {
    const tmp = {
      规则: new AlaudaAuiTable(
        this._root.$('alo-route-conditions-table>aui-table'),
      ),

      权重: new AlaudaAuiTable(
        this._root.$('alo-route-weights-table>aui-table'),
      ),
    };
    return tmp;
  }
}

export class GatawayDetailPage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('.alo-detail__field', '.alo-detail__field label');
  }

  private _getConditionRule(headerText: string): AlaudaAuiTable | object {
    const list: ElementArrayFinder = $$('alo-route-detail .weights');
    const item: ElementFinder = list
      .filter(elem => {
        const headerElem: ElementFinder = elem.$('.header');
        return headerElem.isPresent().then(isPresent => {
          if (isPresent) {
            return headerElem.getText().then(text => {
              return text.trim().includes(headerText);
            });
          } else {
            return false;
          }
        });
      })
      .first();

    this.waitElementPresent(
      item,
      `路由管理的详情页[${headerText}]部分没有正常加载完成`,
    );

    if (headerText === '权重规则') {
      return new AlaudaAuiTable(item.$('alo-route-weights-table>aui-table'));
    } else {
      return new ConditionsRule(item.$('alo-route-conditions-table'))
        .conditionRule;
    }
  }

  getElementByText(text: string, tagname = 'span'): AlaudaAuiTable | object {
    switch (text) {
      case '权重规则':
      case '条件规则(优先级1)':
      case '条件规则(优先级2)':
      case '条件规则(优先级3)':
      case '条件规则(优先级4)':
      case '条件规则(优先级5)':
      case '条件规则(优先级6)':
        return this._getConditionRule(text);
      default:
        return super.getElementByText(text, tagname);
    }
  }

  get actionDropdown(): AlaudaDropdown {
    const dropdown = new AlaudaDropdown(
      $('.aui-card__header button'),
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  /**
   * toast 控件的message信息
   */
  get toast(): AlaudaToast {
    return new AlaudaToast();
  }

  update(testData) {
    this.actionDropdown.select('更新');
    const createPage = new GatewayCreatePage();
    createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  delete() {
    this.actionDropdown.select('删除');
    this.getButtonByText('删除').click();
  }

  get _dialog(): Dialog {
    const dialog: Dialog = new Dialog($('.cdk-overlay-pane aui-dialog'));
    return dialog;
  }

  stop() {
    this.getButtonByText('停用').click();
    this._dialog.button_OK.getText().then(text => {
      expect(text).toBe('停用');
    });

    this._dialog.confirm();
    this.toast.getMessage().then(message => console.log(message));
  }

  start() {
    this.getButtonByText('启用').click();
    // const buttonStart = new AlaudaButton($('.aui-layout__page-header button[aui-button="primary"]'));
    // buttonStart.click();
    this._dialog.button_OK.getText().then(text => {
      expect(text).toBe('启用');
    });

    this._dialog.confirm();
    this.toast.getMessage().then(message => console.log(message));
  }

  /**
   * 创建网关路由
   * @param testData
   */
  createGatewayRoute(testData) {
    const routeCreatePage = new RouteCreatePage();
    routeCreatePage.createRoute(testData);
  }

  /**
   * 更新网关路由
   */
  updateGatewayRoute(testData) {
    const routeUpdatePage = new RouteDetailPage();
    routeUpdatePage.update(testData);
  }

  /**
   * 删除网关路由
   */
  deleteDatewayRoute(): promise.Promise<void> {
    const deleteUpdatePage = new RouteDetailPage();
    deleteUpdatePage.delete();
    return browser.sleep(1);
  }

  verify(expectData) {
    for (const key in expectData) {
      switch (key) {
        case '权重规则':
          const table: AlaudaAuiTable = this.getElementByText(
            key,
          ) as AlaudaAuiTable;

          table.getHeaderText().then(text => {
            expect(text).toEqual(['微服务', '版本', '端口', '权重']);
          });

          expect(table.getRowByIndex(0).getText()).toEqual(expectData[key]);
          break;
        default:
          if (key.includes('条件规则')) {
            const ruleTable: AlaudaAuiTable = this.getElementByText(key)[
              '规则'
            ];
            const weightTable: AlaudaAuiTable = this.getElementByText(key)[
              '权重'
            ];

            // 验证条件规则
            ruleTable.getHeaderText().then(text => {
              expect(text).toEqual(['条件类别', '匹配方式', 'Key', 'Value']);
            });

            expect(ruleTable.getRowByIndex(0).getText()).toEqual(
              expectData[key][0],
            );
            // 验证权重

            weightTable.getHeaderText().then(text => {
              expect(text).toEqual(['微服务', '版本', '端口', '权重']);
            });

            expect(weightTable.getRowByIndex(0).getText()).toEqual(
              expectData[key][1],
            );
          } else {
            expect(
              (this.getElementByText(key) as AlaudaAuiTable).getText(),
            ).toBe(expectData[key]);
          }
      }
      this.getElementByText(key);
    }
    return browser.sleep(1);
  }
}
