/**
 * Created by lrz on 2019/06/14.
 * 创建路由页面条件规则元素
 */

import { $, ElementFinder } from 'protractor';

import { ArrayFormTable } from '../../element_objects/alauda.arrayFormTable';
import { AlaudaElementBase } from '../../element_objects/element.base';

import { AsmPageBase } from './asm.page.base';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';

export class ConDitionRule extends AlaudaElementBase {
  private _root: ElementFinder;
  private asm_base = new AsmPageBase();

  /**
   * 构造函数
   * @param conditionRule alo-condition-rule 控件
   */
  constructor(conditionRule: ElementFinder = $('.rule alo-condition-rule')) {
    super();
    this._root = conditionRule.$$('alo-array-form-table').first();
  }

  /**
   * 创建路由时条件规则输入
   * @param parameters
   */
  input(parameters: Array<Map<string, Array<ArrayFormTable | AuiSelect>>>) {
    for (let i = 0; i < parameters.length - 1; i++) {
      this.asm_base.getButtonByText('添加条件规则').click();
    }

    this.waitElementPresent(this._root.$$('tr alo-array-form-table').first());

    // 选择工作负载
    const wokeload = new AuiSelect(this._root.$$('aui-select').first());
    wokeload.select(parameters[0]['工作负载']);
    // 选择条件规则
    this._root.$$('tr alo-array-form-table').each((arrayFormTable, index) => {
      const tmp = new ArrayFormTable(arrayFormTable);

      const _index: number = Math.floor(index / 2);

      if (index % 2 === 0) {
        tmp.fill(parameters[_index]['规则']);
      }
    });
  }
}
