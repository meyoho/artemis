import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $ } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { PageBase } from '../../page.base';

export class RouteListPage extends PageBase {
    get routeTable(): AuiTableComponent {
        return new AuiTableComponent(
            $('.aui-card'),
            'aui-table',
            '.list-header'
        );
    }

    get loadMoreButton() {
        return new AlaudaButton($('alo-resource-list-footer button'));
    }
}
