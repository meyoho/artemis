/**
 * Created by lrz on 2019/06/14.
 * 创建路由页面
 */

import { ElementFinder } from 'protractor';

import { ArrayFormTable } from '../../../element_objects/alauda.arrayFormTable';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AsmPageBase } from '../asm.page.base';
import { ConDitionRule } from '../condition_rule.element';

export class RouteCreatePage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  /**
   * 根据左侧文字获取右侧页面元素
   * @param text 左侧文字
   * @param tagname
   */
  getElementByText(
    text: string,
    tagname = 'input',
  ): AlaudaInputbox | ConDitionRule | ArrayFormTable {
    switch (text) {
      case '名称':
      case '显示名称':
        return new AlaudaInputbox(this.alaudaElement.getElementByText(text));

      case '条件规则':
        const conditonRuleElement: ElementFinder = super.getElementByText(
          text,
          'alo-condition-rule',
        );
        return new ConDitionRule(conditonRuleElement);
      case '权重规则':
        const weightElement = super.getElementByText(
          text,
          'alo-weight-rule alo-array-form-table',
        );
        return new ArrayFormTable(weightElement);
      default:
        return new AlaudaInputbox(super.getElementByText(text, tagname));
    }
  }

  /**
   * 在文本框中输入值
   * @param name 文本框左侧文字
   * @param value 输入文本框中的值
   * @param tagname
   */
  enterValue(name: string, value, tagname = 'input') {
    switch (name) {
      case '名称':
      case '显示名称':
        const inputbox = this.getElementByText(name) as AlaudaInputbox;
        inputbox.input(value);
        break;

      case '条件规则':
        const conditionRule: ConDitionRule = this.getElementByText(
          name,
        ) as ConDitionRule;
        conditionRule.input(value);
        break;
      case '权重规则':
        const arrayFormTable: ArrayFormTable = this.getElementByText(
          name,
        ) as ArrayFormTable;
        arrayFormTable.fill(value);
        break;
      default:
        (this.getElementByText(name, tagname) as AlaudaInputbox).input(value);
    }
  }

  /**
   * 填写表格
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 创建路由
   * @param testData 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  createRoute(testData) {
    this.getButtonByText('创建路由').click();
    this.fillForm(testData);
    this.getButtonByText('创建').click();
  }

  createRouteRule(testData) {
    this.getButtonByText('创建路由').click();
    this.getButtonByText('添加条件规则组').click();
    this.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
