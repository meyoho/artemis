import { $, $$, ElementFinder, browser } from 'protractor';

import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';

import { MicroServiceDetailPage } from '../servicelist/microservice_detail.page';

export class RoutePolicyPage extends MicroServiceDetailPage {
  /**
   * 创建路由策略按钮所在的card
   * @param typename 权重规则/条件规则1
   */

  private _getAuiCard(typename: string): ElementFinder {
    let tempElem: ElementFinder;
    if (typename.includes('权重规则')) {
      tempElem = $('div .weights');
      return tempElem;
    } else if (typename.includes('条件规则')) {
      tempElem = $('div .conditions-form');
      return tempElem;
    }

    this.waitElementPresent(
      tempElem,
      `应用的详情页没有找到组件[${typename}]所在的card`,
      30000,
    );
    return tempElem;
  }

  /**
   * 获取路由详情页路由策略的card
   * @param type 权重规则/条件规则
   */
  policyCard(type): AlaudaAuiTable {
    const policyCard = this._getAuiCard(type).$('alo-route-traffic-table');
    return new AlaudaAuiTable(policyCard);
  }

  /**
   * 创建路由策略按钮
   */
  policyDropdown(type): AlaudaDropdown {
    const dropdownEle = this._getAuiCard(type).$('.aui-button');
    const dropdown = new AlaudaDropdown(
      dropdownEle,
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  /**
   * 路由策略操作按钮，操作下有更新、删除
   */
  policyActionDropdown(type): AlaudaDropdown {
    const dropdownEle = this._getAuiCard(type)
      .$('alo-route-traffic-table')
      .$('.aui-table__row .aui-icon');
    const dropdown = new AlaudaDropdown(
      dropdownEle,
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  /**
   * 创建路由策略
   * @param type 条件规则/权重规则的路由策略
   * @param policyType 路由策略类型，如错误注入等
   * @param testData 测试数据
   */
  createPolicy(type, policyType, testData) {
    if (type === '条件规则') {
      this.clickRuleDetaiBtn();
    }
    this.policyDropdown(type).select(policyType);
    this._dialog.fillForm(testData);
    this._dialog.confirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
    // if (type === '条件规则') {
    //   this.clickRuleDetaiBtn();
    // }
  }

  /**
   * 更新路由策略
   * @param type 条件规则/权重规则的路由策略
   * @param testData 测试数据
   */
  updatePolicy(type, testData) {
    this.policyActionDropdown(type).select('更新');
    this._dialog.fillForm(testData);
    this._dialog.confirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 删除路由策略
   * @param type 条件规则/权重规则的路由策略
   */
  deletePolicy(type) {
    this.policyActionDropdown(type).select('删除');
    this._dialog.getButtonByText('删除').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 策略card是否存在
   */
  policyCardIsPresent(type) {
    const policyTable: AlaudaAuiTable = this.policyCard(type);
    return policyTable.getRowCount();
  }

  verifyPolicy(expectData) {
    for (const key in expectData) {
      switch (key) {
        case '权重规则':
          const policyTable1: AlaudaAuiTable = this.policyCard(key);
          policyTable1.scrollToBoWindowBottom();
          // expect(policyTable.getHeaderText()).toEqual(['策略类型', '策略详情']);
          expect(policyTable1.getRowByIndex(0).getText()).toEqual(
            expectData[key],
          );
          break;
        case '条件规则':
          const policyTable2: AlaudaAuiTable = this.policyCard(key);
          policyTable2.scrollToBoWindowBottom();
          // expect(policyTable.getHeaderText()).toEqual(['策略类型', '策略详情']);
          expect(policyTable2.getRowByIndex(0).getText()).toEqual(
            expectData[key],
          );
          break;
      }
    }
    return browser.sleep(1);
  }
}
