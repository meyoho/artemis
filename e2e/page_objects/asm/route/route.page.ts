/**
 * Created by lrz on 2019/06/14.
 * 路由管理页面
 */

import { RoutePolicyPage } from './route.policy.page';
import { RouteCreatePage } from './route_create.page';
import { RouteDetailPage } from './route_detail.page';
import { RouteListPage } from './route_list.page';
import { MicroServicePage } from '../servicelist/microservice.page';

export class RoutePage extends MicroServicePage {
  get createPage(): RouteCreatePage {
    return new RouteCreatePage();
  }

  get detailPage(): RouteDetailPage {
    return new RouteDetailPage();
  }

  get listPage(): RouteListPage {
    return new RouteListPage();
  }

  get policyPage(): RoutePolicyPage {
    return new RoutePolicyPage();
  }
}
