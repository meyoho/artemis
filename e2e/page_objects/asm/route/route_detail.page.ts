/**
 * Created by lrz on 2019/06/17.
 * 路由详情页
 */

import { $, $$, promise, browser } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaToast } from '../../../element_objects/alauda.toast';
import { Dialog } from '../../asm/dialog.page';
import { AsmPageBase } from '../asm.page.base';

import { RouteCreatePage } from './route_create.page';

export class RouteDetailPage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'alo-route-base-info-detail .alo-detail__field',
      '.alo-detail__field label',
    );
  }

  /**
   * 操作按钮
   */
  get actionDropdown(): AlaudaDropdown {
    const dropdown = new AlaudaDropdown(
      $('alo-route-detail alo-route-base-info-detail .aui-button--default'),
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  /**
   * toast 控件的message信息
   */
  get toast(): AlaudaToast {
    return new AlaudaToast();
  }

  /**
   * 更新路由
   * @param testData
   */
  update(testData) {
    this.actionDropdown.select('更新');
    const createPage = new RouteCreatePage();
    createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  delete(): promise.Promise<void> {
    this.actionDropdown.select('删除');
    this.getButtonByText('删除').click();
    return browser.sleep(1);
  }

  /**
   * 弹出框
   */
  get _dialog(): Dialog {
    const dialog: Dialog = new Dialog($('.cdk-overlay-pane aui-dialog'));
    return dialog;
  }

  stop() {
    this.getButtonByText('停用').click();
    this._dialog.button_OK.getText().then(text => {
      expect(text).toBe('停用');
    });

    this._dialog.confirm();
    this.toast.getMessage().then(message => console.log(message));
  }

  start() {
    this.getButtonByText('启用').click();
    this._dialog.button_OK.getText().then(text => {
      expect(text).toBe('启用');
    });

    this._dialog.confirm();
    this.toast.getMessage().then(message => console.log(message));
  }
}
