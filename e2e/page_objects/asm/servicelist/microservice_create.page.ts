import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, ElementFinder } from 'protractor';

import { AsmPageBase } from '../asm.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class MicroServiceCreatePage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  getElementByText(name): AlaudaInputbox | ArrayFormTable {
    switch (name) {
      case '微服务名称':
      case '显示名称':
        return new AlaudaInputbox(super.getElementByText(name));
      case '服务版本':
        const serviceVersion: ElementFinder = super.getElementByText(
          name,
          'alo-array-form-table',
        );
        return new ArrayFormTable(serviceVersion);
    }
  }

  enterValue(name, value) {
    switch (name) {
      case '微服务名称':
      case '显示名称':
        const inputbox = this.getElementByText(name) as AlaudaInputbox;
        inputbox.input(value);
        break;
      case '服务版本':
        const condition: ArrayFormTable = this.getElementByText(
          name,
        ) as ArrayFormTable;
        condition.fill(value);
        break;
    }
  }

  fillForm(testData) {
    for (const key in testData) {
      this.enterValue(key, testData[key]);
    }
  }

  get createButton() {
    return new AlaudaButton(
      $('.alo-mutate-page-bottom-buttons .aui-button--primary'),
    );
  }
  createMicroService(testData) {
    this.getButtonByText('创建微服务').click();
    this.fillForm(testData);
    this.createButton.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
