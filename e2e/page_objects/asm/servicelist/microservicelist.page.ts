import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $ } from 'protractor';

import { AsmPageBase } from '../asm.page.base';

export class MicroServiceListPage extends AsmPageBase {
    get serviceListTable(): AuiTableComponent {
        return new AuiTableComponent(
            $('aui-card .aui-card'),
            'aui-table',
            '.aui-card__header'
        );
    }

    get loadMoreButton() {
        return new AlaudaButton($('alo-resource-list-footer button'));
    }
}
