import { AsmPageBase } from '../asm.page.base';

import { MicroServiceListPage } from './microservicelist.page';
import { MicroServiceCreatePage } from './microservice_create.page';
import { MicroServiceDetailPage } from './microservice_detail.page';

export class MicroServicePage extends AsmPageBase {
    get createMicroService(): MicroServiceCreatePage {
        return new MicroServiceCreatePage();
    }

    get microServiceListPage(): MicroServiceListPage {
        return new MicroServiceListPage();
    }

    get microServiceDetailPage(): MicroServiceDetailPage {
        return new MicroServiceDetailPage();
    }
}
