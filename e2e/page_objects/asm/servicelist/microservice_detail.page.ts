import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$, ElementFinder, browser, promise } from 'protractor';

import { AsmPageBase } from '../asm.page.base';

import { MicroServiceCreatePage } from './microservice_create.page';
import { MicroServiceDetailDialog } from './microservice_detail_dialog.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class MicroServiceDetailPage extends AsmPageBase {
  /**
   * 操作按钮
   */
  get actionDropdown(): AlaudaDropdown {
    const dropdown = new AlaudaDropdown(
      $('.aui-card__header .aui-button--default'),
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  /**
   * 规则详情按钮
   */
  clickRuleDetaiBtn() {
    const ruleDetailBtn = new AlaudaButton(
      $('alo-route-detail .condition .switchBtn'),
    );
    ruleDetailBtn.click();
  }

  /**
   * 更新微服务
   * @param testData
   */
  update(testData) {
    this.actionDropdown.select('更新');
    const createPage = new MicroServiceCreatePage();
    createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    browser.sleep(1000);
  }

  /**
   * 弹出框
   */
  get _dialog(): MicroServiceDetailDialog {
    const elem = $('.cdk-overlay-pane aui-dialog');
    const dialog: MicroServiceDetailDialog = new MicroServiceDetailDialog(elem);
    return dialog;
  }

  /**
   * 删除微服务
   */
  delete() {
    this.actionDropdown.select('删除');
    this.getButtonByText('删除').click();
  }

  /**
   * 创建服务入口 按钮
   */
  get createServiceEntryButton() {
    return this.getButtonByText('创建服务入口');
  }

  /**
   * 服务入口列表的操作按钮
   */
  get serviceEntryAction(): AlaudaDropdown {
    const dropdown = new AlaudaDropdown(
      $('.container .aui-icon'),
      $$('button[class*="aui-menu-item"]'),
    );
    return dropdown;
  }

  get strategyAction(): AlaudaDropdown {
    const strategyAction = new AlaudaDropdown(
      $('alo-strategy-table aui-table .aui-icon'),
      $$('button[class*="aui-menu-item"]'),
    );

    return strategyAction;
  }

  /**
   * 服务入口列表
   */
  get serviceEntryTable(): AuiTableComponent {
    return new AuiTableComponent($('.container'), 'aui-table');
  }

  /**
   * 策略列表
   */
  get strategyTable() {
    return new AuiTableComponent($('alo-strategy-table'), 'aui-table');
  }

  /**
   * 访问控制列表
   */
  get whiteListTable() {
    return new AuiTableComponent($('alo-whitelist'), 'aui-table');
  }

  /**
   * 创建服务入口
   */
  createServiceEntry(testData) {
    this.createServiceEntryButton.click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }

  /**
   * 更新服务入口
   */
  updateServiceEntry(testData) {
    this.serviceEntryAction.select('更新');
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }

  /**
   * 删除服务入口
   */
  deleteServiceEntry(): promise.Promise<void> {
    this.serviceEntryAction.select('删除');
    this.getButtonByText('删除').click();
    return browser.sleep(1);
  }

  /**
   * 左侧拓扑图服务入口节点
   */
  get _serviceNode(): ElementFinder {
    const serviceNode = $('alo-service-graph .node_service path');
    this.waitElementPresent(serviceNode, '左侧拓扑图的service节点未出现');
    this.waitElementClickable(serviceNode);
    return serviceNode;
  }

  /**
   * tab页-策略
   */
  get _strategyTab(): ElementFinder {
    const tabMenu = $('.aui-tab-header__labels .aui-tab-label:nth-child(2)');
    this.waitElementPresent(tabMenu, 'tab策略未出现');
    return tabMenu;
  }

  /**
   * tab页-访问控制
   */
  get _whitelistTab(): ElementFinder {
    const tabMenu = $('.aui-tab-header__labels .aui-tab-label:nth-child(4)');
    this.waitElementPresent(tabMenu, 'tab访问控制未出现');
    return tabMenu;
  }

  /**
   * tab页-路由
   */
  get _route(): ElementFinder {
    const tabMenu = $('.aui-tab-header__labels .aui-tab-label:nth-child(3)');
    this.waitElementPresent(tabMenu, 'tab路由未出现');
    this.waitElementClickable(tabMenu).then(isClickable => {
      if (isClickable !== true) {
        throw new Error('tab路由不可单击');
      }
    });

    return tabMenu;
  }

  /**
   * 创建熔断策略
   */
  createOutlierDetection(testData) {
    this.getButtonByText('创建策略').click();
    this.getButtonByText('熔断').click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }
  /**
   * 创建连接池设置策略
   * @param testData
   */
  createConnectionPoolSettings(testData) {
    this.getButtonByText('创建策略').click();
    this.getButtonByText('连接池设置').click();
    this.getButtonByText('HTTP/HTTP2').click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }

  /**
   * 创建负载均衡策略
   * @param testData
   */
  createLoadBalancer(testData) {
    this.getButtonByText('创建策略').click();
    this.getButtonByText('负载均衡').click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }

  /**
   * 创建安全策略
   * @param testData
   */
  createSecurityStrategy(testData) {
    this.getButtonByText('创建策略').click();
    this.getButtonByText('安全').click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
  }

  /**
   * 更新策略
   */
  updateStrategy(testData) {
    this.strategyAction.select('更新');
    this._dialog.fillForm(testData);
    this._dialog.confirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 删除策略
   */
  deleteStrategy() {
    this.strategyAction.select('删除');
    this.getButtonByText('确定').click();
  }

  /**
   * 创建白名单
   */
  createWhiteList(testData): promise.Promise<void> {
    this.getButtonByText('添加白名单').click();
    this._dialog.fillForm(testData);
    this._dialog.confirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
    return browser.sleep(1);
  }

  /**
   * 删除指定白名单
   */
  deleteWhiteList(microservice_name) {
    this.whiteListTable.clickOperationButtonByRow([microservice_name], '删除');
    this._dialog.getButtonByText('删除').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
    this.waitProgressBarNotPresent();
  }
}
