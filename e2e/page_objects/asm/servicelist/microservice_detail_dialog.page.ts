import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, ElementFinder, promise, browser } from 'protractor';

import { AsmPageBase } from '../asm.page.base';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';

export class MicroServiceDetailDialog extends AsmPageBase {
  private _timeout: number;
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  private _root: ElementFinder;

  constructor(root: ElementFinder) {
    super();
    this._root = root;
    this._timeout = 0;
  }
  get title(): ElementFinder {
    return this._root.$('.aui-confirm-dialog__title span:nth-child(2)');
  }

  get content(): ElementFinder {
    return this._root.$('.aui-confirm-dialog__content');
  }

  /**
   * 确认键
   */
  get button_OK(): ElementFinder {
    return this._root.$('.aui-dialog__footer .aui-button--primary');
  }

  /**
   * 取消键
   */
  get button_cancel(): ElementFinder {
    return this._root.$('.aui-dialog__footer .aui-button--default');
  }

  /**
   * 点击确认
   */
  confirm() {
    const button = new AlaudaButton(this.button_OK);
    button.click();
    this.waitElementNotPresent(button.button, '添加白名单，diablog 页没关闭');
    button.button.isPresent().then(isPresent => {
      if (isPresent) {
        this._timeout++;
        if (this._timeout > 5) {
          console.log(`重试 ${this._timeout} 次, 关闭添加白名单 dialog`);
          throw new Error('关闭添加白名单 dialog 超时了');
        }
        this.confirm();
      }
    });
  }

  /**
   * 点击 取消
   */
  cancel(): promise.Promise<void> {
    const button = new AlaudaButton(this.button_cancel);
    button.click();
    return browser.sleep(1);
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ):
    | ArrayFormTable
    | AuiMultiSelect
    | AlaudaInputGroup
    | AuiSelect
    | AlaudaRadioButton
    | AlaudaInputbox {
    switch (text) {
      case '端口':
        const portInfo = super.getElementByText(text, 'alo-array-form-table');
        return new ArrayFormTable(portInfo);
      case '服务版本':
        const workload = super.getElementByText(text, '.aui-multi-select');
        return new AuiMultiSelect(workload, $('.cdk-overlay-pane aui-tooltip'));
      case '检查周期':
      case '实例最短隔离时间':
      case '实例最大隔离比例':
        const checkCycle = super.getElementByText(
          text,
          '.aui-form-item__content>aui-input-group',
        );
        return new AlaudaInputGroup(checkCycle);
      case '负载均衡策略':
      case '目标服务':
      case '目标服务版本':
      case '重写 host':
        const auiSelect: ElementFinder = super.getElementByText(
          text,
          'aui-select',
        );
        return new AuiSelect(auiSelect, $('.cdk-overlay-pane aui-tooltip'));
      case '规则':
        return new AlaudaRadioButton(
          super.getElementByText(text, 'aui-radio-group'),
        );

      default:
        return new AlaudaInputbox(super.getElementByText(text, 'input'));
    }
  }

  enterValue(key: string, value) {
    switch (key) {
      case '白名单':
        const serviceVersion = new ArrayFormTable(
          this._root.$('alo-array-form-table'),
        );

        serviceVersion.fill(value);
        break;
      case '端口':
        const arrayFormTable: ArrayFormTable = this.getElementByText(
          key,
        ) as ArrayFormTable;
        arrayFormTable.fill(value);
        break;
      case '服务版本':
        const auiMultiSelect: AuiMultiSelect = this.getElementByText(
          key,
        ) as AuiMultiSelect;
        auiMultiSelect.select(value);
        break;
      case '检查周期':
      case '实例最短隔离时间':
        const outlierDetection: AlaudaInputGroup = this.getElementByText(
          key,
        ) as AlaudaInputGroup;
        outlierDetection.input(value[0], value[1]);
        break;
      case '实例最大隔离比例':
        const ratio: AlaudaInputGroup = this.getElementByText(
          key,
        ) as AlaudaInputGroup;
        ratio.input(value);
        break;
      case '负载均衡策略':
      case '目标服务':
      case '目标服务版本':
      case '重写 host':
        (this.getElementByText(key) as AuiSelect).select(value);
        break;
      case '规则':
        (this.getElementByText(key) as AlaudaRadioButton).clickByName(value);
        break;
      default:
        (this.getElementByText(key) as AlaudaInputbox).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 服务入口复选框
   */
  get serviceEntryCheckbox(): AlaudaAuiCheckbox {
    return new AlaudaAuiCheckbox(this._root.$('.aui-checkbox'));
  }
}
