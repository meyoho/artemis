/**
 * Created by liuwei on 2019/6/13.
 */
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaDropdownWithSearch } from '@e2e/element_objects/alauda.dropdown_search';
import { AlaudaTabItem } from '@e2e/element_objects/alauda.tabitem';
import { CommonMethod } from '@e2e/utility/common.method';
import { $, $$, ElementFinder, browser, by, promise } from 'protractor';

import { AuiTableComponent } from '../../component/aui_table.component';
import { ServerConf } from '../../config/serverConf';
// import { CommonMethod } from '../../utility/common.method';
// import { alauda_type_project } from '../../utility/resource.type.k8s';
import { AloSearch } from '../../element_objects/alauda.aloSearch';
import { AuiDialog } from '../../element_objects/alauda.aui_dialog';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonPage } from '../../utility/common.page';
import { PageBase } from '../page.base';

export class AsmPageBase extends PageBase {
  constructor() {
    super();
    browser.baseUrl = `${ServerConf.BASE_URL}/console-asm/`;
  }

  get clusterName() {
    return ServerConf.REGIONNAME;
  }

  get globalClusterName() {
    return 'global';
  }

  get projectName(): string {
    return 'uiauto-asm';
  }

  get paginationProJectName(): string {
    return 'uiauto-asm-pagination';
  }

  get paginationNs(): string {
    return 'uiauto-asm-pagination-ns';
  }

  get namespace1Name(): string {
    return 'uiauto-asm-ns1';
  }
  get namespace2Name(): string {
    return 'uiauto-asm-ns2';
  }

  get asmApp1Name(): string {
    return 'asm-app1';
  }

  get asmApp2Name(): string {
    return 'asm-app2';
  }

  get serviceMeshName(): string {
    return ServerConf.SERVICE_MESH_NAME;
  }
  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    const branchName = ServerConf.BRANCH_NAME.replace('_', '-')
      .substring(0, 10)
      .replace('_', '-')
      .replace('/', '')
      .toLowerCase();
    return `asm-${branchName}-${prefix}`;
  }

  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }

  /**
   * progressbar 出现后，等待消失
   * @example waitProgressBarNotPresent()
   */
  waitProgressBarNotPresent(timeout = 20000): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(
          this.progressbar,
          'progressbar 没消失',
          timeout,
        );
      }
    });
  }

  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  /**
   * 创建开启了serviceMesh的namespace
   * @param projectName
   * @param namespaceName
   */
  createNamespace(projectName: string, namespaceName: string) {
    CommonKubectl.createResource(
      'alauda.asm.namespace.yaml',
      {
        '${CLUSTER_NAME}': ServerConf.REGIONNAME,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${PROJECT_NAME}': projectName,
        '${NAMESPACE_NAME}': namespaceName,
        '${NAMESPACE_DISPLAYNAME}': namespaceName.toLocaleUpperCase(),
      },
      'qa-namespace' + String(new Date().getMilliseconds()),
      ServerConf.REGIONNAME,
    );
  }

  private _searchProject(projectName: string, timeout = 20000) {
    const nodata = $('alo-no-data');
    const searchBox = new AloSearch($('.list-header .alo-search'));
    return browser.driver
      .wait(() => {
        searchBox.search(projectName);
        return nodata.isPresent().then(isPresent => !isPresent);
      }, timeout)
      .then(
        () => true,
        () => {
          throw new Error(`项目列表页没有查询到[${projectName}]`);
        },
      );
  }

  /**
   * 项目列表
   */
  get projectTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('main'),
      'alo-project-list',
      '.alo-search',
      '.project-card>div span',
      '.project-card',
    ); // 检索框的父元素);
  }

  /**
   * 进入项目中
   * @param projectName 项目名称
   * @example enterProject('asm-demo')
   */
  enterProject(projectName: string) {
    // this.isProjectReady(projectName);
    this._searchProject(projectName);
    this.waitElementPresent(
      this.projectTable.getRow([projectName]),
      `在项目列表页没有找到项目【${projectName}】, 无法进入到详情页`,
    );

    this.projectTable.clickResourceNameByRow([projectName]);
    this.waitProgressBarNotPresent();
  }

  private _goToChinese() {
    const icon = $('.account-menu__icon');
    CommonPage.waitElementPresent(icon);
    icon.click();
    browser.sleep(100);
    // 找到中文下拉选项
    const items = $$('.aui-tooltip--bottom aui-menu-item button');
    const item = items
      .filter(elem => {
        return elem.getText().then(text => {
          return text === '简体中文';
        });
      })
      .first();
    // 如果找到，单击
    item.isPresent().then(isPresent => {
      if (isPresent) {
        item.click();
        this.waitProgressBarNotPresent();
      } else {
        icon.click();
      }
    });
  }

  /**
   * 页面左上角项目下拉框
   */
  get projectMenu(): AlaudaDropdownWithSearch {
    return new AlaudaDropdownWithSearch(
      $('.acl-project-select aui-icon'),
      $$('.acl-project-select__options .aui-menu-item span'),
      $('.aui-menu aui-search .hasIcon'),
    );
  }
  /**
   * 页面集群导航条
   * @param region_name 集群名称
   */
  regionMenu(region_name): AlaudaTabItem {
    return new AlaudaTabItem(
      by.xpath('//aui-tag'),
      by.xpath("//aui-tag/div[contains(@class,'aui-tag--primary')]"),
      by.xpath('//aui-tag/div/span[contains(text(),"' + region_name + '")]'),
    );
  }

  /**
   * 页面命名空间导航条
   */
  namespaceMenu(): AlaudaAuiTable {
    return new AlaudaAuiTable($('aui-card aui-table'));
  }

  // 搜索命名空间
  searchNamespace(namespace) {
    // const nodata = $('main aui-card');
    const searchBox = new AuiSearch($('aui-search .hasIcon'));
    searchBox.onlySearch(namespace);
    // return browser.driver
    //     .wait(() => {
    //         searchBox.onlySearch(namespace);
    //         return nodata.isPresent().then(isPresent => !isPresent);
    //     }, 2000)
    //     .then(
    //         () => true,
    //         () => {
    //             throw new Error(`命名空间列表页没有查询到[${namespace}]`);
    //         }
    //     );
  }

  waitDeployReady(
    deployname,
    namespace = this.namespace1Name,
    clusterName = this.clusterName,
  ) {
    return browser.sleep(1000).then(() => {
      let times = 0;
      while (times < 60) {
        const ret1 = CommonKubectl.execKubectlCommand(
          `kubectl get deploy -n ${namespace} ${deployname}`,
          clusterName,
        ).includes('1/1');
        const ret2 = CommonKubectl.execKubectlCommand(
          `kubectl get po -n ${namespace}|grep ${deployname}|wc -l`,
          clusterName,
        ).includes('1');

        if (ret1 && ret2) {
          return true;
        } else {
          CommonMethod.sleep(3000);
          times += 1;
        }
      }
      return false;
    });
  }
  /**
   * 进入管理视图
   */
  enterAdminView() {
    this._goToChinese();
    $('acl-project-select a span')
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          // 如果项目存在，说明在用户视图
          // 用户视图页面， 切换视图的按钮
          const button = $('.view-switch');
          this.waitElementPresent(
            button,
            '等待 10s后，切换用户视图按钮未出现',
            10000,
          );
          button.click();
        }
      });
  }

  /**
   * 进入用户视图页面
   * @param project_name 项目名称
   * @param region_name 集群名称
   * @param namespace_name 命名空间
   */
  enterUserView(project_name, region_name, namespace_name = null) {
    this._goToChinese();
    const pro_name = project_name;
    let isNotFound = CommonKubectl.execKubectlCommand(
      `kubectl get project ${pro_name}`,
    ).includes('NotFound');
    let timeout = 0;
    while (isNotFound) {
      CommonMethod.sleep(1000);
      isNotFound = CommonKubectl.execKubectlCommand(
        `kubectl get project ${pro_name}`,
      ).includes('NotFound');
      if (timeout++ > 120) {
        throw new Error(`创建项目${pro_name}超时`);
      }
    }
    $('acl-project-select a span')
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          // 如果项目不存在，说明在管理视图
          // 管理视图页面， 切换视图的按钮
          const button = $('.view-switch');
          this.waitElementPresent(
            button,
            '等待 10s后，切换用户视图按钮未出现',
            10000,
          );
          button.click();
          this.waitElementPresent(
            $('acl-project-select a span'),
            '等待 10s后，项目未出现',
            10000,
          );
        }
      });
    browser.driver.wait(
      function() {
        return $('acl-project-select a span')
          .getText()
          .then(t => {
            if (t.length) {
              return true;
            } else {
              return false;
            }
          });
      },
      30000,
      'the project drop down button not visit',
    );
    this.projectMenu.select(pro_name);
    browser.refresh();
    if (this.clusterDisplayName(region_name) === undefined) {
      this.regionMenu(region_name).click();
    } else {
      this.regionMenu(this.clusterDisplayName(region_name)).click();
    }

    this.searchNamespace(namespace_name);
    const enterIcon = new AlaudaButton($('aui-table-cell button'));
    enterIcon.click();
    // if (namespace_name) {
    //     console.log(namespace_name);
    //     this.namespaceMenu()
    //         .getCell('', [namespace_name])
    //         .then(ele => {
    //             this.waitElementPresent(ele, '找不到对应的进入按钮');
    //             ele.click();
    //         });
    // }
  }

  // 分页选项，10.20.50.100
  get paginationOptopn() {
    const pagination_option = new AuiSelect(
      $('.aui-paginator__sizes .aui-input-group'),
      $('.cdk-overlay-pane aui-tooltip'),
    );

    return pagination_option;
  }

  /**
   * 删除资源
   */
  deleteResource(resource, resourcename, namespace) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete ${resource} ${resourcename} -n ${namespace}`,
      this.clusterName,
    );
  }

  isNotExist(clusterName, cmd) {
    return CommonKubectl.execKubectlCommand(cmd, clusterName).includes(
      'NotFound',
    );
  }
}
