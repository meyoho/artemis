import { $, ElementFinder } from 'protractor';

import { AlaudaInputGroup } from '../../element_objects/alauda.auiInput.group';
import { AuiSelect } from '../../element_objects/alauda.auiSelect';
import { AlaudaButton } from '../../element_objects/alauda.button';
import { AlaudaElement } from '../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../element_objects/alauda.radioButton';

import { AsmPageBase } from './asm.page.base';

export class Dialog extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'aui-form-item .aui-form-item label[class=aui-form-item__label]',
    );
  }

  private _root: ElementFinder;

  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }
  get title(): ElementFinder {
    return this._root.$('.aui-confirm-dialog__title span:nth-child(2)');
  }

  get content(): ElementFinder {
    return this._root.$('.aui-confirm-dialog__content');
  }

  /**
   * 确认键
   */
  get button_OK(): ElementFinder {
    return this._root.$('button[class*="primary"]');
  }

  /**
   * 取消键
   */
  get button_cancel(): ElementFinder {
    return this._root.$('button[class*="cancel"]');
  }

  /**
   * 点击确认
   */
  confirm() {
    const button = new AlaudaButton(this.button_OK);
    button.click();
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
  ):
    | ElementFinder
    | AuiSelect
    | AlaudaInputGroup
    | AlaudaRadioButton
    | AlaudaInputbox {
    switch (text) {
      case '重写 host':
      case '目标服务':
      case '目标服务版本':
      case '服务':
        const auiSelect: ElementFinder = super.getElementByText(
          text,
          'aui-select',
        );
        return new AuiSelect(auiSelect, $('.cdk-overlay-pane aui-tooltip'));
      case '错误比例':
      case '延迟时间':
      case '全局超时时间':
      case '重试超时时间':
      case '延迟比例':
        const auiInputGroup: ElementFinder = super.getElementByText(
          text,
          'aui-input-group',
        );
        return new AlaudaInputGroup(auiInputGroup);
      case '规则':
        return new AlaudaRadioButton(
          super.getElementByText(text, 'aui-radio-group'),
        );
      default:
        return new AlaudaInputbox(super.getElementByText(text, 'input'));
    }
  }

  enterValue(key: string, value) {
    switch (key) {
      case '重写 host':
      case '目标服务':
      case '目标服务版本':
        // case '服务':之前安全策略用
        (this.getElementByText(key) as AuiSelect).select(value);
        break;
      case '错误比例':
      case '延迟时间':
      case '延迟比例':
      case '全局超时时间':
      case '重试超时时间':
        (this.getElementByText(key) as AlaudaInputGroup).input(value);
        break;
      case '规则':
        (this.getElementByText(key) as AlaudaRadioButton).clickByName(value);
        break;
      default:
        (this.getElementByText(key) as AlaudaInputbox).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
