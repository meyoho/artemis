import { AsmPageBase } from '../asm.page.base';
import { $ } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class AsmProjectListPage extends AsmPageBase {
  get projectTable(): AuiTableComponent {
    return new AuiTableComponent($('.aui-card'), 'aui-table', '.list-header');
  }
  get loadMoreButton() {
    return new AlaudaButton($('alo-resource-list-footer button'));
  }
}
