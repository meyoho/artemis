import { AsmPageBase } from '../asm.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $ } from 'protractor';

export class AsmProjectDetailPage extends AsmPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('.alo-detail__field', '.alo-detail__field label');
  }

  /**
   * 命名空间列表
   */
  get namespaceTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('alo-project-detail aui-card:nth-child(2) .aui-card'),
      'aui-table',
      '.list-header',
    );
  }
}
