import { AsmPageBase } from '../asm.page.base';
import { AsmProjectListPage } from './asm_project_list.page';
import { AsmProjectDetailPage } from './asm_project_detail.page';

export class AsmProjectPage extends AsmPageBase {
  get listPage() {
    return new AsmProjectListPage();
  }

  get detailPage() {
    return new AsmProjectDetailPage();
  }
}
