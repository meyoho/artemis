import { SecurityRuleCreatePage } from './securityrule_create.page';
import { SecurityRuleListPage } from './securityrule_list.page';
import { MicroServicePage } from '../servicelist/microservice.page';

export class SecurityStrategyPage extends MicroServicePage {
  get createPage(): SecurityRuleCreatePage {
    return new SecurityRuleCreatePage();
  }

  get listPage(): SecurityRuleListPage {
    return new SecurityRuleListPage();
  }
}
