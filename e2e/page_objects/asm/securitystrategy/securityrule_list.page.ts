import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $, $$ } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaToast } from '../../../element_objects/alauda.toast';
import { AsmPageBase } from '../../asm/asm.page.base';
import { Dialog } from '../../asm/dialog.page';

export class SecurityRuleListPage extends AsmPageBase {
    get securityRuleTable(): AuiTableComponent {
        return new AuiTableComponent(
            $('.aui-card'),
            'aui-table',
            '.list-header'
        );
    }

    /**
     * 加载更多 按钮
     */
    get loadMoreButton() {
        return new AlaudaButton($('alo-resource-list-footer button'));
    }

    /**
     * 创建、更新、删除弹出框
     */
    get _dialog(): Dialog {
        const dialog: Dialog = new Dialog($('.cdk-overlay-pane aui-dialog'));
        return dialog;
    }

    /**
     * toast 控件的message信息
     */
    get toast(): AlaudaToast {
        return new AlaudaToast();
    }

    /**
     * 操作按钮
     */
    get actionDropdown(): AlaudaDropdown {
        const dropdown = new AlaudaDropdown(
            $('aui-table .aui-button'),
            $$('button[class*="aui-menu-item"]')
        );
        return dropdown;
    }

    /**
     * 更新安全规则
     * @param testData
     */
    updateSecirityRule(testData) {
        this.actionDropdown.select('更新');
        this._dialog.fillForm(testData);
        this._dialog.confirm();
        this.toast.getMessage().then(message => {
            console.log(message);
        });
    }

    deleteSecurityRule() {
        this.actionDropdown.select('删除');
        this._dialog.confirm();
        this.toast.getMessage().then(message => {
            console.log(message);
        });
    }
}
