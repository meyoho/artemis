import { $ } from 'protractor';

import { AlaudaToast } from '../../../element_objects/alauda.toast';
import { AsmPageBase } from '../../asm/asm.page.base';
import { Dialog } from '../../asm/dialog.page';

export class SecurityRuleCreatePage extends AsmPageBase {
  /**
   * 创建、更新、删除弹出框
   */
  get _dialog(): Dialog {
    const dialog: Dialog = new Dialog($('.cdk-overlay-pane aui-dialog'));
    return dialog;
  }

  /**
   * toast 控件的message信息
   */
  get toast(): AlaudaToast {
    return new AlaudaToast();
  }

  /**
   * 创建安全规则
   * @param testData
   */
  createSecurityRule(testData) {
    this._dialog.fillForm(testData);
    this._dialog.confirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
