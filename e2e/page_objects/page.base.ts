/**
 * Created by zhangjiao on 2018/4/16.
 */

import { AccountMenu } from '@e2e/element_objects/acp/alauda.account_menu';
import { AlaudaClusterSelect } from '@e2e/element_objects/acp/alauda.cluster_select';
import { AlaudaNamespaceSelect } from '@e2e/element_objects/acp/alauda.namespace_select';
import { AlaudaAclProjectSelect } from '@e2e/element_objects/acp/alauda.project_select';
import { ViewSwitch } from '@e2e/element_objects/acp/alauda.view_switch';
import { AlaudaAuiBreadCrumb } from '@e2e/element_objects/alauda.auiBreadcrumb';
import {
  $,
  $$,
  ElementFinder,
  browser,
  by,
  element,
  promise,
  protractor,
} from 'protractor';

import { ServerConf } from '../config/serverConf';
import { AlaudaButton } from '../element_objects/alauda.button';
import { AlaudaDropdown } from '../element_objects/alauda.dropdown';
import { AlaudaElement } from '../element_objects/alauda.element';
import { AlaudaText } from '../element_objects/alauda.text';
import { AlaudaToast } from '../element_objects/alauda.toast';
import { CommonKubectl } from '../utility/common.kubectl';
import { CommonMethod } from '../utility/common.method';
import { CommonPage } from '../utility/common.page';
import { LoginPage } from './login.page';

const { prepareTestData } = require('../utility/prepare.testdata');

export class PageBase {
  private _token: string;
  private _productTimeout: number;
  constructor() {
    this._productTimeout = 0;
  }
  /**
   * 获得新窗口的
   * @param index 窗口的index, 从 0 开始
   * @example switchWindow(1)
   */
  switchWindow(index: number) {
    browser.getAllWindowHandles().then(handles => {
      const newWindowHandle = handles[index];
      browser
        .switchTo()
        .window(newWindowHandle)
        .then(() => {
          browser.getCurrentUrl().then(url => {
            console.log('switch to new Window: ' + url);
          });
        });
    });
  }

  /**
	 * 等待页面元素出现
	 * @param elem 页面元素
	 * @param errorMessage 错误提示信息
	 * @param timeout 超时时间
	 * @example waitElementPresent($('button'), '等待5秒后，按钮没有出现'， 5000).then(isPresent => {
		if(isPresent) {
			console.log('元素出现了')
		} else {
			console.log('元素没出现')
		}
	})
	 */
  waitElementPresent(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 10000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementPresent(elem, timeout).then(isPresent => {
      if (!isPresent) {
        console.log(errorMessage);
      }
      return isPresent;
    });
  }

  /**
   * 等待页面元素不出现
   * @param elem 页面元素
   * @param errorMessage 错误提示
   * @param timeout 超时时间
   * @example waitElementNotPresent($('button'), '等待5秒后，按钮没有消失', 5000)
   */
  waitElementNotPresent(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 10000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementNotPresent(elem, timeout).then(
      isNotPresent => {
        if (!isNotPresent) {
          console.log(errorMessage);
        }
        return isNotPresent;
      },
    );
  }
  /**
   * 等待页面元素显示
   * @param elem 页面元素
   * @param errorMessage 错误提示
   * @param timeout 超时时间
   * @example waitElementDisplay($('button'), '等待5秒后，按钮没有显示', 5000)
   */
  waitElementDisplay(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 20000,
  ) {
    return CommonPage.waitElementDisplay(elem, timeout).then(isDisplay => {
      if (!isDisplay) {
        console.log(errorMessage);
      }
      return isDisplay;
    });
  }

  /**
   * 等待页面元素不显示
   * @param elem 页面元素
   * @param errorMessage 错误提示
   * @param timeout 超时时间
   * @example waitElementNotDisplay($('button'), '等待5秒后，按钮仍然显示', 5000)
   */
  waitElementNotDisplay(
    elem: ElementFinder,
    errorMessage: string,
    timeout = 20000,
  ) {
    return CommonPage.waitElementNotDisplay(elem, timeout).then(
      isNotDisplay => {
        if (!isNotDisplay) {
          console.log(errorMessage);
        }
        return isNotDisplay;
      },
    );
  }

  /**
   * 等待元素的文字变成 期望值
   * @param elem 页面元素
   * @param expectText 期望值
   * @param timeout 超时时间
   */
  waitElementTextChangeTo(elem: ElementFinder, expectText, timeout = 20000) {
    CommonPage.waitElementTextChangeTo(elem, expectText, timeout);
  }

  /**
   * 等待元素能单击
   * @param elem 页面元素
   * @param timeout 等待多久超时
   */
  waitElementClickable(elem: ElementFinder, timeout = 10000) {
    this.waitElementPresent(elem, `${elem.locator()} not found`);
    const EC = protractor.ExpectedConditions;
    return browser.driver.wait(EC.elementToBeClickable(elem), timeout).then(
      () => true,
      err => {
        elem.getTagName().then(tagName => {
          console.error(err);
          throw new Error(`元素${tagName}, 不可单击`);
        });
        return false;
      },
    );
  }

  /**
   * 页面左上角的产品信息
   */
  async _getPoduct(): Promise<ElementFinder> {
    let viewElem: ElementFinder;
    return browser.getCurrentUrl().then(async url => {
      if (url.includes('console-acp')) {
        viewElem = $('rc-page-header .project-title');
        await this.waitElementPresent(
          viewElem,
          '容器平台页面左上角，产品名称没加载出来',
        );
        return viewElem;
      }
      if (url.includes('console-devops') || url.includes('console-asm')) {
        viewElem = $('.aui-page__header alo-logo span');
        await this.waitElementPresent(
          viewElem,
          '平台页面左上角，产品名称没加载出来',
        );
        return viewElem;
      }

      if (url.includes('console-platform')) {
        viewElem = $('.aui-page__header alu-logo span');
        await this.waitElementPresent(
          viewElem,
          '平台页面左上角，产品名称没加载出来',
        );
        return viewElem;
      }
    });
  }

  /**
   * 切换项目控件
   */
  get productAction() {
    return new AlaudaDropdown(
      $('.aui-page__header acl-product-select'),
      $$('aui-menu-item button'),
    );
  }

  /**
   * 切换产品
   * @param name 产品名称
   */
  switchProduct(name) {
    return this.productAction.select(name);
  }

  /**
   * 切换用户视角控件
   */
  async viewAction(): Promise<ViewSwitch> {
    const elem: ElementFinder = await this._getPoduct();
    return elem.getText().then(text => {
      switch (text.trim()) {
        case 'Container Platform':
          return new ViewSwitch($('.aui-page__header rc-view-switch'));
        case 'DevOps':
        case 'Service Mesh':
          return new ViewSwitch(
            $('.aui-page__header .user-action .view-switch'),
          );
      }
    });
  }

  /**
   * 切换到用户视图
   */
  switchUserView() {
    this.waitElementNotPresent(
      element(by.css('aui-notification .aui-notification__title')),
      '等待 10s后，错误提示框未关闭',
      10000,
    );
    this.viewAction().then(action => {
      action.switchUserView();
    });
  }

  /**
   * 切换到管理视图
   */
  switchAdminView() {
    return this.viewAction().then(action => {
      action.switchAdminView();
    });
  }

  /**
   * 页面右上角用户文字显示
   */
  get accountAction(): AccountMenu {
    const account = $('.aui-page__header acl-account-menu');
    this.waitElementPresent(account, '页面右上角用户菜单没出现');
    return new AccountMenu(account);
  }

  /**
   * 页面右上角平台中心
   */
  get platformCenter(): AlaudaDropdown {
    const account = $('.aui-page__header acl-platform-center-nav');
    this.waitElementPresent(account, '页面右上角用户菜单没出现');
    return new AlaudaDropdown(account, $$('aui-tooltip aui-menu-item'));
  }

  get loginPage(): LoginPage {
    return new LoginPage();
  }

  /**
   * 页面右上角切换设置项
   * @param name 名称，项目管理，平台管理，个人中心
   */
  switchSetting(name) {
    switch (name) {
      case '项目管理':
      case '平台管理':
      case '运维中心':
        this.platformCenter.select(name);
        const loginPage = new LoginPage();
        loginPage.waitLoadingDisappear();
        this._getPoduct().then(newProduct => {
          this.waitElementPresent(
            newProduct,
            `切换后，${name}页面没有正常加载`,
          );
          newProduct.getText().then(text => {
            if (text !== name) {
              if (this._productTimeout++ < 5) {
                this.switchSetting(name);
              } else {
                throw new Error(`切换【${name}】失败`);
              }
            }
          });
          browser.sleep(1000);
        });

        break;
      default:
        this.accountAction.select(name);
        browser.sleep(1000);
        break;
    }
  }

  /**
   * 管理视图上切换集群的控件
   */
  get clusterAction(): promise.Promise<AlaudaClusterSelect> {
    return this._getPoduct().then(product => {
      return product.getText().then(text => {
        switch (text.trim()) {
          case 'Container Platform':
          case '运维中心':
          case '平台管理':
            return new AlaudaClusterSelect(
              $('.cluster-badge .display-name span'),
            );
          case 'Service Mesh':
          case 'DevOps':
            return new AlaudaClusterSelect(
              $('.aui-page__header .switch-cluster span'),
            );
        }
      });
    });
  }

  switchCluster_onAdminView(name: string) {
    console.log(`选择集群 ${name}`);
    this.clusterAction
      .then(cluster => {
        cluster.select(`${name} (${this.clusterDisplayName(name)})`);
      })
      .catch(ex => {
        console.log('===================');
        console.log('ERROR:' + ex);
        console.log('===================');
      });
    return browser.sleep(1);
  }

  /**
   * 业务视图上切换的控件
   */
  get namespaceAction(): promise.Promise<AlaudaNamespaceSelect> {
    return this._getPoduct().then(product => {
      return product.getText().then(text => {
        switch (text.trim()) {
          case 'Container Platform':
            return new AlaudaNamespaceSelect(
              $('rc-cluster-badge .display-name span'),
            );
          case 'Service Mesh':
            return new AlaudaNamespaceSelect(
              $('.aui-page__header .switch-cluster span'),
            );
        }
      });
    });
  }

  switchNameSpace(clusterName, nsName) {
    this.namespaceAction.then(nsAction => {
      nsAction.select(clusterName, nsName);
    });
  }

  /**
   * 切换成中文格式
   */
  switchChinese() {
    // 找到中文下拉选项
    this.accountAction.select('简体中文');
  }

  /**
   * 切换项目控件
   */
  get projectAction(): AlaudaAclProjectSelect {
    return new AlaudaAclProjectSelect(
      $('.aui-page__header acl-project-select'),
    );
  }

  /**
   * 切换项目
   * @param name 项目名称
   */
  switchProject(name) {
    this.projectAction.select(name);
  }

  /**
   * 面包屑
   */
  get breadcrumb(): AlaudaAuiBreadCrumb {
    return new AlaudaAuiBreadCrumb();
  }

  /**
   * 根据 Button 上的文字获得此按钮，
   * 注：如果页面（包括弹出页面）上有重名的按钮，此方法不适用
   * @example 单击创建配置字典按钮 ：getButtonByText('创建配置字典').click()
   */
  getButtonByText(buttonText): AlaudaButton {
    return new AlaudaButton(
      element(
        by.xpath(
          `//button/descendant-or-self::*[normalize-space(text()) ='${buttonText}']`,
        ),
      ),
    );
  }

  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-field[class]',
      'aui-form-field[class] label[class*=aui-form-field]',
    );
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string, tagname = 'input'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value, tagname = 'input') {
    this.getElementByText(name, tagname).clear();
    this.getElementByText(name, tagname).sendKeys(value);
    browser.sleep(1);
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   * @example fillForm({ 应用名称: 'qq', 镜像源证书: '不使用' })
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 单击左导航
   * @example clickLeftNavByText('配置字典')
   */
  async clickLeftNavByText(text) {
    await CommonPage.clickLeftNavByText(text);
    await this.waitElementNotPresent($('.loading span'), '');
  }

  /**
   * 根据左侧文字，查找右侧元素
   * @param left 左侧文字
   * @param tagname 要查找的右侧元素
   * @extends getElementByText('名称', 'input')
   */
  getElementByText_xpath(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and normalize-space(text())="${left}" ]/ancestor::aui-form-item//${tagname}`;
    const temp = element(by.xpath(xpath));
    temp.isPresent().then(isPresent => {
      if (!isPresent) {
        console.log(
          `根据左侧文字[${left}], 没有找到右侧控件 ${tagname}\n${temp.locator()}`,
        );
      }
    });
    return temp;
  }

  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    return CommonMethod.random_generate_testData(prefix);
  }
  /**
   * 获取提示框标题
   */
  get erroralert_title() {
    this.waitElementPresent(
      element(by.css('.aui-notification__title')),
      '等待 1s后，提示框未出现',
      1000,
    );
    return new AlaudaText(by.css('.aui-notification__title'));
  }
  /**
   * 获取提示框内容
   */
  get erroralert_content() {
    return new AlaudaText(by.css('.aui-notification__content'));
  }
  /**
   * 关闭提示框按钮
   */
  close_erroralert() {
    new AlaudaButton($('.aui-notification__close')).click();
    this.waitElementNotDisplay(
      $('.aui-notification__close'),
      '等待5秒后，提示框仍然显示',
      5000,
    );
  }
  /**
   * 获取toast
   */
  get toast() {
    return new AlaudaToast($('.aui-message .aui-message__content'));
  }

  /**
   * 登陆
   * @param name 用户名
   * @param password 密码
   * @param isAdmin 是否是管理员登陆，默认是
   * @example login()
   */
  login(
    name: string = ServerConf.ADMIN_USER,
    password: string = ServerConf.ADMIN_PASSWORD,
    isAdmin = true,
  ) {
    CommonMethod.writeyamlfile(
      'token',
      CommonMethod.loginGetToken(name, password, isAdmin ? 'local' : 'ldap'),
    );
    const loginPage = new LoginPage();
    loginPage.login(name, password, isAdmin);
  }

  logout() {
    const loginPage = new LoginPage();
    loginPage.logout();
  }

  /**
   * 页面登陆后，读取token
   */
  get token() {
    return this._token;
  }

  /**
   * 页面登陆后，设置token 值
   */
  set token(value) {
    this._token = value;
  }

  /**
   * 创建项目
   * @param projectName 项目名称
   * @param cluster 项目所属集群
   * @param projectDisplayName 项目显示名称
   * @param projectDescription 项目描述
   */
  createProject(
    projectName: string,
    cluster = ServerConf.REGIONNAME,
    projectDisplayName = '',
    projectDescription = 'UI TEST Project',
  ) {
    const data = {
      PROJECT_NAME: projectName,
      PROJECT_DESCRIPTION: projectDescription,
      PROJECT_DISPLAYNAME: projectDisplayName,
      CLUSTERS: [],
    };
    if (Array.isArray(cluster)) {
      cluster.forEach(v => {
        data.CLUSTERS.push({
          CLUSTER_NAME: v,
        });
      });
    } else {
      data.CLUSTERS.push({
        CLUSTER_NAME: cluster,
      });
    }
    CommonKubectl.createResourceByTemplate(
      'alauda.project.template.yaml',
      data,
      'qa-project' + String(new Date().getMilliseconds()),
    );
    let isNotFound = CommonKubectl.execKubectlCommand(
      `kubectl get project ${projectName}`,
    ).includes('NotFound');
    let timeout = 0;
    while (isNotFound) {
      CommonMethod.sleep(1000);
      isNotFound = CommonKubectl.execKubectlCommand(
        `kubectl get project ${projectName}`,
      ).includes('NotFound');
      if (timeout++ > 120) {
        throw new Error(`创建项目${projectName}超时`);
      }
    }
  }

  /**
   * 在项目下，创建一个命名空间
   * @param projectName 项目名称
   * @param namespaceName 命名空间名称
   */
  createNamespace(
    projectName: string,
    namespaceName: string,
    cluster = ServerConf.REGIONNAME,
  ): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.namespace.template.yaml',
      {
        cluster: cluster,
        project: projectName,
        ns_name: namespaceName,
        display_name: 'UI TEST NS',
      },
      'qa-namespace' + String(new Date().getMilliseconds()),
      cluster,
    );
  }

  private _deleteNamespace(
    namespaceName: string,
    clusterName = 'global',
  ): void {
    const cmd_get = `kubectl get ns ${namespaceName}`;
    const cmd_delete = `kubectl delete ns ${namespaceName}`;
    const message = `删除namespace ${namespaceName} 超时`;
    this._waitDelete(cmd_get, cmd_delete, message, clusterName);
  }

  private _deleteProject(projectName: string, clusterName = 'global'): void {
    const cmd_get = `kubectl get project ${projectName}`;
    const cmd_delete = `kubectl delete project ${projectName}`;
    const message = `删除 project ${projectName} 超时`;
    this._waitDelete(cmd_get, cmd_delete, message, clusterName);
  }

  public waitDelete(
    getCmd: string,
    deleteCmd: string,
    message: string,
    clusterName = 'global',
  ) {
    this._waitDelete(getCmd, deleteCmd, message, clusterName);
  }

  private _waitDelete(
    getCmd: string,
    deleteCmd: string,
    message: string,
    clusterName = 'global',
  ) {
    try {
      let isDeleted = CommonKubectl.execKubectlCommand(
        getCmd,
        clusterName,
      ).includes('Error');
      let timeout = 0;
      while (!isDeleted) {
        console.log(CommonKubectl.execKubectlCommand(getCmd, clusterName));
        console.log('clusterName:' + clusterName);
        console.log('isDeleted:' + isDeleted + ':' + deleteCmd);
        console.log(
          'DeletedCmd:' +
            CommonKubectl.execKubectlCommand(deleteCmd, clusterName),
        );
        CommonKubectl.execKubectlCommand(deleteCmd, clusterName);
        CommonMethod.sleep(1000);
        isDeleted = CommonKubectl.execKubectlCommand(getCmd).includes('Error');

        if (timeout++ > 60) {
          throw new Error(message);
        }
      }
    } catch (ex) {
      console.log(ex);
    }
  }

  /**
   * 删除Namespace
   * @param namespaceName 命名空间名称
   * @param clusterName 集群名称
   * @example deleteNamespace('asm-demo', 'high')
   */
  deleteNamespace(namespaceName: string, clusterName = 'global'): void {
    const cmd_get = `kubectl get ns ${namespaceName}`;
    const cmd_delete = `kubectl delete ns ${namespaceName}`;
    const message = `删除namespace ${namespaceName} 超时`;
    this._waitDelete(cmd_get, cmd_delete, message, clusterName);
  }

  /**
   * 删除project
   * @param project_name project 名称
   * @example deleteProject('asm-demo')
   */
  deleteProject(project_name: string): void {
    try {
      // 获得项目绑定的所有集群
      const clusters = CommonMethod.parseYaml(
        CommonKubectl.execKubectlCommand(
          `kubectl get project ${project_name} -o yaml`,
        ),
      );

      if (clusters.spec !== undefined) {
        clusters.spec.clusters.forEach(cluster => {
          // 遍历所有绑定集群
          const nsList = CommonMethod.parseYaml(
            CommonKubectl.execKubectlCommand(
              'kubectl get ns  -o yaml',
              cluster.name,
            ),
          ).items;
          // 查询到项目下的命名空间
          nsList.forEach(item => {
            for (const k in item.metadata.labels) {
              if (k === `${ServerConf.LABELBASEDOMAIN}/project`) {
                if (item.metadata.labels[k] === project_name) {
                  // 删除命名空间
                  console.log(
                    `在集群 ${cluster.name} 里， 删除命名空间 ${item.metadata.name}`,
                  );
                  this._deleteNamespace(item.metadata.name, cluster.name);
                }
              }
            }
          });
        });
        console.log(`删除项目 ${project_name}`);
        this._deleteProject(project_name);
      }
    } catch (e) {
      console.log(e.message);
    }
  }
  /**
   * 校验yaml 内容
   * @param src 期望值 json数据类型
   * @param dist 实际值 yaml内容经过CommonMethod.parseYaml(yml)解析后的 json数据类型
   * @param deep 是否对Array内容进行深度比较 boolean数据类型
   * @example
   * const src = {
   *           metadata: {
   *               name: cronjob_name
   *           },
   *           spec: {
   *               concurrencyPolicy: 'Allow',
   *               failedJobsHistoryLimit: 20,
   *               successfulJobsHistoryLimit: 20,
   *               schedule: '0-59 * * * *',
   *               suspend: false,
   *               jobTemplate: {
   *                   metadata: {
   *                       labels: {
   *                           'cronjob.alauda.io/name': cronjob_name
   *                       }
   *                   },
   *                   spec: {
   *                       activeDeadlineSeconds: 7200,
   *                       backoffLimit: 6,
   *                       template: {
   *                           spec: {
   *                               restartPolicy: 'Never'
   *                           }
   *                       }
   *                   }
   *               }
   *           }
   *       };
   * // 切换到YAML tab 页
   * this.clictTab('YAML');
   * // 从YAML页面元素中获取YAML内容
   * this.yamlContent.getYamlValue().then(yml => {
   *     // 将yaml内容转换成Json数据类型
   *     const dist = CommonMethod.parseYaml(yml);
   *     // 校验数据
   *     this.jsonContain(src,dist);
   *     });
   */
  jsonContain(src: object, dist: object, deep = false) {
    for (const key in src) {
      // 检查 key 是否包含在目标数据中
      this._checkKeyExist(dist, key);
      if (typeof src[key] === typeof dist[key]) {
        if (src[key] instanceof Object && !Array.isArray(src[key])) {
          this.jsonContain(src[key], dist[key], deep);
        } else if (src[key] instanceof Array) {
          if (deep) {
            this.deepArrayContain(src[key], dist[key]);
          } else {
            this.arrayContain(src[key], dist[key]);
          }
        } else {
          expect(src[key]).toBe(dist[key]);
        }
      } else {
        console.log(`匹配目标与预期数据类型不一致${key}`);
        console.log('src:\n' + src.toString());
        console.log('dist:\n' + dist.toString());
        throw new Error(`匹配目标与预期数据类型不一致${key}`);
      }
    }
  }

  private _checkKeyExist(dist: object, key: string) {
    if (dist[key] === undefined) {
      let temp = '目标类型中所有的关键字是:\n';
      for (const dist_key in dist) {
        temp += dist_key + '\n';
      }
      throw new Error(`目标类型中不包含期望关键字${key}\n${temp}`);
    }
  }

  /**
   * 校验 Array: dist 是否包含 Array: src 里的所有元素
   * @param src 期望值 Array数据类型
   * @param dist 实际值 Array数据类型
   * @example
   * const src = ['a','b','c']
   * const dist = ['a','b','c','d']
   * this.arrayContain(src,dist)
   */
  arrayContain(src: Array<any>, dist: Array<any>) {
    for (let i = 0; i < src.length; i++) {
      expect(dist.toString()).toContain(src[i].toString());
    }
  }
  /**
   * 深度校验 Array: dist 是否包含 Array: src 里的所有元素，元素类型是Array或Object数据类型，需要元素在src 和dist的index相同
   * @param src 期望值 Array数据类型
   * @param dist 实际值 Array数据类型
   * @example
   * const src = ['a',['b',1],{c: 1}]
   * const dist = ['a',['b',1,2],{c: 1,m: 2},'d']
   * this.arrayContain(src,dist)
   */
  deepArrayContain(src: Array<any>, dist: Array<any>) {
    for (let key = 0; key < src.length; key++) {
      if (key < dist.length) {
        if (typeof src[key] === typeof dist[key]) {
          if (src[key] instanceof Object && !Array.isArray(src[key])) {
            this.jsonContain(src[key], dist[key], true);
          } else if (src[key] instanceof Array) {
            this.deepArrayContain(src[key], dist[key]);
          } else {
            expect(src[key]).toBe(dist[key]);
          }
        } else {
          console.log(`匹配目标与预期数据类型不一致,index: ${key}`);
          console.log('src:\n' + src.toString());
          console.log('dist:\n' + dist.toString());
          throw new Error(`匹配目标与预期数据类型不一致,index: ${key}`);
        }
      } else {
        throw new Error(`匹配目标length比预期数据大: `);
      }
    }
  }

  /**
   * 集群名称
   */
  get clusterName() {
    return ServerConf.REGIONNAME;
  }

  /**
   * 集群显示名称
   */
  clusterDisplayName(name: string = this.clusterName) {
    const cmd = `kubectl get clusters.clusterregistry.k8s.io -n ${ServerConf.GLOBAL_NAMESPCE} ${name} -o yaml`;
    const yamlValue = CommonMethod.parseYaml(
      CommonKubectl.execKubectlCommand(cmd),
    );
    let displayname;
    try {
      displayname =
        yamlValue.metadata.annotations[
          `${ServerConf.LABELBASEDOMAIN}/display-name`
        ];
    } catch {
      displayname = undefined;
    }
    return displayname;
  }
  /**
   * 获取功能开关是否开启
   * @param feature_name 功能开关名称。如 tapp
   * @param cluster 集群名称
   */
  _featureIsEnabled(feature_name, cluster = ServerConf.REGIONNAME) {
    const reg = new RegExp('^(E|e)rror.?');
    let feature_enabled = false;
    let feature_stage = '';
    const cluster_feature = CommonKubectl.execKubectlCommand(
      `kubectl get clusteralaudafeaturegates.alauda.io ${feature_name} -o yaml`,
      cluster,
    );
    if (!reg.test(cluster_feature)) {
      feature_enabled = CommonMethod.parseYaml(cluster_feature).spec.enabled;
      feature_stage = CommonMethod.parseYaml(
        cluster_feature,
      ).spec.stage.toLowerCase();
    } else {
      console.log(
        `获取集群: ${cluster} ${feature_name}功能开关失败,开始获取全局${feature_name}功能开关`,
      );
      const global_feature = CommonKubectl.execKubectlCommand(
        `kubectl get alaudafeaturegates.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} ${feature_name} -o yaml`,
      );
      if (!reg.test(global_feature)) {
        feature_enabled = CommonMethod.parseYaml(global_feature).spec.enabled;
        feature_stage = CommonMethod.parseYaml(
          global_feature,
        ).spec.stage.toLowerCase();
      } else {
        console.log(`获取全局${feature_name}功能开关失败`);
        return false;
      }
    }
    if (!(feature_stage.length === 0)) {
      const stage_yaml = CommonKubectl.execKubectlCommand(
        `kubectl get alaudafeaturegates.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} ${feature_stage} -o yaml`,
      );
      if (!reg.test(stage_yaml)) {
        const stage_enable = CommonMethod.parseYaml(stage_yaml).spec.enabled;
        if (stage_enable) {
          return feature_enabled;
        } else {
          console.log(`${feature_stage}功能开关未打开`);
          return stage_enable;
        }
      } else if (!(feature_stage === 'Alpha')) {
        console.log(`获取${feature_stage}功能开关失败`);
        return feature_enabled;
      } else {
        console.warn(`获取${feature_stage}功能开关失败`);
        return false;
      }
    } else {
      console.warn(`获取${feature_name}功能成熟度开关失败`);
      return false;
    }
  }
  /**
   * 通过api获取功能开关状态
   * @param feature_name 功能开关名称。如 tapp
   * @param cluster 集群名称
   */
  featureIsEnabled(feature_name, cluster = ServerConf.REGIONNAME) {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}' -k ${
      ServerConf.BASE_URL
    }/fg/v1/${cluster}/featuregates/${feature_name}`;
    console.log(cmd);
    const rsp: string = CommonMethod.execCommand(cmd);
    console.log(rsp);
    try {
      return JSON.parse(rsp)['status']['enabled'];
    } catch {
      return false;
    }
  }
  /**
   * 获取全局功能开关是否开启
   * @param feature_name 功能开关名称。如 tapp
   */
  globalfeatureIsEnabled(feature_name) {
    const reg = new RegExp('^(E|e)rror.?');
    let feature_enabled = false;
    let feature_stage = '';
    const global_feature = CommonKubectl.execKubectlCommand(
      `kubectl get alaudafeaturegates.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} ${feature_name} -o yaml`,
    );
    if (!reg.test(global_feature)) {
      feature_enabled = CommonMethod.parseYaml(global_feature).spec.enabled;
      feature_stage = CommonMethod.parseYaml(
        global_feature,
      ).spec.stage.toLowerCase();
    } else {
      console.log(`获取全局${feature_name}功能开关失败`);
      return false;
    }
    if (!(feature_stage.length === 0)) {
      const stage_yaml = CommonKubectl.execKubectlCommand(
        `kubectl get alaudafeaturegates.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} ${feature_stage} -o yaml`,
      );
      if (!reg.test(stage_yaml)) {
        const stage_enable = CommonMethod.parseYaml(stage_yaml).spec.enabled;
        if (stage_enable) {
          return feature_enabled;
        } else {
          console.log(`${feature_stage}功能开关未打开`);
          return stage_enable;
        }
      } else if (!(feature_stage === 'Alpha')) {
        console.log(`获取${feature_stage}功能开关失败`);
        return feature_enabled;
      } else {
        console.warn(`获取${feature_stage}功能开关失败`);
        return false;
      }
    } else {
      console.warn(`获取${feature_name}功能成熟度开关失败`);
      return false;
    }
  }
  /**
   * 根据key和value转换成字典类型
   * @param keys_values Arrray<Array<string>>
   */
  createLabels(keys_values: Array<Record<string, string>>) {
    const labels = {};
    keys_values.forEach(key_value => {
      labels[key_value.key] = key_value.value;
    });
    return labels;
  }
  checkRegionNotExist(region_name, kind = 'cluster'): boolean {
    return CommonKubectl.execKubectlCommand(
      `kubectl get ${kind} -n ${ServerConf.GLOBAL_NAMESPCE} ${region_name}`,
      ServerConf.SECONDREGIONNAME,
    ).includes('NotFound');
  }
  waitResourceExist(
    resource_kind,
    resource_name,
    resource_namespace = '',
    flag = true,
    region_name = ServerConf.REGIONNAME,
  ) {
    const cmd =
      resource_namespace === ''
        ? `kubectl get  ${resource_kind} ${resource_name}`
        : `kubectl get  ${resource_kind} ${resource_name} -n ${resource_namespace}`;
    let timeout = 0;
    while (
      flag
        ? prepareTestData.isNotExist(region_name, cmd)
        : !prepareTestData.isNotExist(region_name, cmd)
    ) {
      prepareTestData.sleep(5000);
      if (timeout++ > 10) {
        return false;
      }
    }
    return true;
  }
}
