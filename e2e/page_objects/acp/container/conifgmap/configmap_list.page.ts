import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $, by, element } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';

export class ConfigMapListPage extends AcpPageBase {
  /*
    configmap 列表页点击创建按钮
    */
  get createConfigMapButton() {
    return new AlaudaButton(element(by.buttonText('创建配置字典')));
  }
  get configmapTable() {
    return new AuiTableComponent(
      $('aui-card>.aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-row',
    );
  }
  get configmapPageTitle() {
    return new AlaudaText(by.css('aui-breadcrumb>.aui-breadcrumb'));
  }
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog .aui-dialog'),
      'aui-dialog-content',
      '.aui-button--danger',
      'button[auidialogclose]',
      'aui-dialog-header',
    );
  }
  /**
   * 检索configmap
   * @param name 名称
   * @param resultCount 期望检索的个数
   */
  search(name: string, resultCount = 1) {
    this.clickLeftNavByText('配置字典');
    this.configmapTable.searchByResourceName(name, resultCount);
  }

  /**
   * 单击操作下的选项
   * @param name 名称
   * @param actionName 更新，或删除
   */
  operation(name: string, actionName: string) {
    this.search(name, 1);
    this.configmapTable.clickOperationButtonByRow(
      [name],
      actionName,
      'button aui-icon',
      'aui-menu-item>button',
    );
  }

  delete(name: string) {
    this.operation(name, '删除');
    this.auiDialog.clickConfirm();
  }
}
