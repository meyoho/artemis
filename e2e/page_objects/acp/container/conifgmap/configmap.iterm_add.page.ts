import { AcpPageBase } from '../../acp.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $ } from 'protractor';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class ItermAddPage extends AcpPageBase {
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog'),
      'aui-dialog-content',
      '.aui-button--primary',
      '.aui-button--default',
      'aui-dialog-header',
    );
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label', $('aui-dialog'));
  }
  getElementByText(text: string) {
    switch (text) {
      case '键':
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'input'),
        );
      case '值':
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'textarea'),
        );
    }
  }
  enterValue(name: string, value: string) {
    this.getElementByText(name).input(value);
  }
}
