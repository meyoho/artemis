import { ConfigmapDetailVerify } from '@e2e/page_verify/container/configmap/configmap_detail.page';
import { ConfigMapListVerify } from '@e2e/page_verify/container/configmap/configmap_list.page';

import { AcpPageBase } from '../../acp.page.base';

import { ConfigmapCreatePage } from './configmap_create.page';
import { ConfigmapDetailPage } from './configmap_detail.page';
import { ConfigMapListPage } from './configmap_list.page';
import { ConfigmapUpdatePage } from './configmap_update.page';
import { PrepareData } from './prepare.page';

export class ConfigPage extends AcpPageBase {
    get createPage(): ConfigmapCreatePage {
        return new ConfigmapCreatePage();
    }

    get updatePage(): ConfigmapUpdatePage {
        return new ConfigmapUpdatePage();
    }

    get detailPage(): ConfigmapDetailPage {
        return new ConfigmapDetailPage();
    }

    get detailPageVerify(): ConfigmapDetailVerify {
        return new ConfigmapDetailVerify();
    }

    get listPage(): ConfigMapListPage {
        return new ConfigMapListPage();
    }

    get listPageVerify(): ConfigMapListVerify {
        return new ConfigMapListVerify();
    }

    get prepareData(): PrepareData {
        return new PrepareData();
    }
}
