import { ConfigmapCreatePage } from './configmap_create.page';
import { ConfigMapListPage } from './configmap_list.page';

export class ConfigmapUpdatePage extends ConfigmapCreatePage {
  get listPage(): ConfigMapListPage {
    return new ConfigMapListPage();
  }
  update(name: string, testData) {
    this.listPage.operation(name, '更新');
    this.fillForm(testData);
    this.clickConfirm();
  }
}
