import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { ConfigmapDataViewer } from '@e2e/page_objects/acp/container/element_objects/alauda.configmap_secret_data';
import { $, ElementFinder, by, element, $$, promise } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { ItermAddPage } from './configmap.iterm_add.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class ConfigmapDetailPage extends AcpPageBase {
  /**
   * 返回定时任务记录详情页title: 任务/任务记录/<名称>
   */
  get title(): ElementFinder {
    this.waitElementPresent(
      $('aui-breadcrumb .aui-breadcrumb'),
      'configmap详情页title',
    );
    return $('aui-breadcrumb .aui-breadcrumb');
  }
  /**
   * 返回任务记录名称
   */
  get name(): ElementFinder {
    this.waitElementPresent(
      $('.aui-card__header div'),
      '.aui-card__header div 没有定位到 comfigmap名称',
    );
    return $('.aui-card__header div');
  }
  get workloads() {
    return new AuiTableComponent(
      $('rc-configmap-workloads'),
      'aui-table[class*="aui-table"]',
      'aui-table[class*="aui-table"]',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  /**
   *
   * @param tab_name tab页名称如：详细信息、YAML
   */
  clickTab(tab_name: string) {
    element(
      by.xpath(
        '//div[@class="aui-tab-label__content" and contains(text(),"' +
          tab_name +
          '")]',
      ),
    ).click();
    if (tab_name === '工作负载') {
      this.workloads.waitRowCountChangeto(1);
    }
  }
  /**
   * 获取configmap 数据父元素
   */
  get data(): ConfigmapDataViewer {
    return new ConfigmapDataViewer($('rc-k8s-config-items'));
  }

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-field-set-item',
      'label',
      $('rc-field-set-group'),
    );
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(
    text: string,
    tagname = '.field-set-item__value div',
  ): ElementFinder {
    return this.alaudaElement.getElementByText(text, tagname);
  }
  /**
   * 获取yaml元素
   */
  get getYamlElement(): AlaudaYamlEditor {
    return new AlaudaYamlEditor();
  }
  get yamlContent(): promise.Promise<string> {
    this.clickTab('YAML');
    return this.getYamlElement.getYamlValue();
  }
  get itermAddPage() {
    return new ItermAddPage();
  }
  clickOperation(name: string) {
    const op = new AlaudaDropdown(
      $('.aui-card__header>button'),
      $$('aui-tooltip acl-disabled-container button'),
    );
    return op.select(name);
  }
  AddIterms(data: Record<string, any>) {
    new AlaudaButton(
      element(by.xpath('//button/span[contains(text(),"添加配置项")]')),
    ).click();
    // this.clickOperation('添加配置项').then();
    this.itermAddPage.fillForm(data);
    return this.itermAddPage.auiDialog.clickConfirm();
  }
  AddItermsCancel(data: Record<string, any>) {
    new AlaudaButton(
      element(by.xpath('//button/span[contains(text(),"添加配置项")]')),
    ).click();
    this.itermAddPage.fillForm(data);
    return this.itermAddPage.auiDialog.clickCancel();
  }
  AddItermsClose(data: Record<string, any>) {
    new AlaudaButton(
      element(by.xpath('//button/span[contains(text(),"添加配置项")]')),
    ).click();
    this.itermAddPage.fillForm(data);
    return this.itermAddPage.auiDialog.clickClose();
  }
  deleteIterm(name: string) {
    this.data.clickDelete(name);
    return this.itermAddPage.auiDialog.clickConfirm();
  }
  deleteItermCancel(name: string) {
    this.data.clickDelete(name);
    return this.itermAddPage.auiDialog.clickCancel();
  }
  updateIterm(name: string, value: string) {
    this.data.clickPencil(name);
    this.itermAddPage.fillForm({ 值: value });
    return this.itermAddPage.auiDialog.clickConfirm();
  }
  updateItermCancel(name: string, value: string) {
    this.data.clickPencil(name);
    this.itermAddPage.fillForm({ 值: value });
    return this.itermAddPage.auiDialog.clickCancel();
  }
}
