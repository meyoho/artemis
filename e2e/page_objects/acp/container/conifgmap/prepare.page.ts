import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { k8s_type_configmaps } from '@e2e/utility/resource.type.k8s';

import { AcpPageBase } from '../../acp.page.base';

export class PrepareData extends AcpPageBase {
  delete(
    name: string,
    ns_name = this.namespace1Name,
    cluster = this.clusterName,
  ) {
    CommonKubectl.deleteResource(k8s_type_configmaps, name, cluster, ns_name);
  }
  create(values: Record<string, any>, cluster = this.clusterName) {
    CommonKubectl.createResourceByTemplate(
      'alauda.configmap.yaml',
      values,
      `alauda.configmap.tmp-${this.getRandomNumber()}`,
      cluster,
    );
  }
}
