import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, ElementFinder, browser } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { ItemFieldset } from '../element_objects/items_fieldset';

import { ConfigmapDetailPage } from './configmap_detail.page';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';

export class ConfigmapCreatePage extends AcpPageBase {
  get detailPage() {
    return new ConfigmapDetailPage();
  }
  // private _inputGroup: ElementFinder;
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }
  get yamlEditor() {
    return new AlaudaYamlEditor();
  }
  auiSelect(ele: ElementFinder): AuiSelect {
    return new AuiSelect(
      ele.$('form>aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      case '配置项':
        return this.alaudaElement.getElementByText(
          text,
          'rc-config-items-fieldset',
        );
      case '描述':
        return this.alaudaElement.getElementByText(text, 'textarea');
      default:
        return this.alaudaElement.getElementByText(text, 'input');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value: string) {
    switch (name) {
      case '配置项':
        new ItemFieldset(this.getElementByText(name)).add(value);
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
  }
  select_mode(mode: string) {
    const modes = new AlaudaRadioButton($('aui-radio-group'));
    modes.clickByName(mode);
  }

  create_by_yaml(data: string) {
    this.clickLeftNavByText('配置字典');
    this.getButtonByText('创建配置字典').click();
    this.select_mode('YAML');
    this.yamlEditor.setYamlValue(data);
    this.clickConfirm();
    browser.sleep(100);
    this.waitElementNotPresent($('rc-configmap-form'), '创建配置字典失败');
    return this.waitElementPresent(
      this.detailPage.name,
      '详情页 comfigmap名称没出现',
    );
  }

  create(testData) {
    this.clickLeftNavByText('配置字典');
    this.getButtonByText('创建配置字典').click();
    this.fillForm(testData);
    this.clickConfirm();
    browser.sleep(100);
    this.waitElementNotPresent($('rc-configmap-form'), '创建配置字典失败');
    return this.waitElementPresent(
      this.detailPage.name,
      '详情页 comfigmap名称没出现',
    );
  }
  create_cancel(testData) {
    this.clickLeftNavByText('配置字典');
    this.getButtonByText('创建配置字典').click();
    this.fillForm(testData);
    this.getButtonByText('取消').click();
    browser.sleep(100);
    this.waitElementNotPresent(
      $('rc-configmap-form'),
      '创建配置字典点击取消按钮失败',
    );
    return this.waitElementPresent(
      this.getButtonByText('创建配置字典').button,
      '返回配置字典列表失败',
    );
  }
}
