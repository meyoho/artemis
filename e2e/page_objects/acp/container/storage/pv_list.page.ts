import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, by, element } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

import { PvDetailPage } from './pv_detail.page';

export class PVList extends AcpPageBase {
  /**
   * pv列表 title: 存储/持久卷
   */
  get title() {
    this.waitElementPresent(
      $('aui-breadcrumb .aui-breadcrumb'),
      'pv列表页title',
    );
    return $('aui-breadcrumb .aui-breadcrumb').getText();
  }
  /**
   * 点击页面按钮
   * @param name 按钮名称
   */
  clickButon(name: string) {
    element(by.xpath('//button/span[contains(text(),"' + name + '")]')).click();
  }
  /**
   * 存储列表
   */
  get pvTable() {
    return new AuiTableComponent(
      $('aui-card>.aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-row',
    );
  }

  /**
   * 检索一个持久卷
   * @param name 持久卷名称
   * @param resultCount 期望检索结果的个数
   */
  search(name: string, resultCount = 1) {
    this.clickLeftNavByText('持久卷');
    this.pvTable.searchByResourceName(name, resultCount);
  }

  /**
   * 检索持久卷
   * @param name 持久卷名称
   */
  viewDatail(name: string): PvDetailPage {
    this.search(name);
    this.pvTable.clickResourceNameByRow([name]);
    const detailPage = new PvDetailPage();
    this.waitElementPresent(
      detailPage.name,
      '持久卷列表页单击名称后，详情页没有打开',
    );
    return detailPage;
  }

  /**
   * 删除持久卷
   * @param name 持久卷名称
   */
  delete(name: string) {
    this.search(name);
    this.pvTable.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button',
      'aui-menu-item button',
    );
    this.getButtonByText('确定').click();
  }
}
