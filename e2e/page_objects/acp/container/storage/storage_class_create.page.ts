import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { $, $$, browser } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { PreparePage } from './prepare.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class ScCreatePage extends AcpPageBase {
  // private _inputGroup: ElementFinder;
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      '.aui-form-item__label',
    );
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '集群':
        return this.alaudaElement.getElementByText(
          text,
          '.aui-form-item__content span',
        );
      case 'YAML':
        return new AlaudaYamlEditor();
    }

    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case '集群':
        break;
      case 'YAML':
        this.getElementByText(name, tagname).setYamlValue(value);
    }
  }
  /**
   * 选择创建存储类类型
   * @param name 存储类类型名称如NFS
   */
  selectScType(name) {
    const sc_types = $$(
      'aui-dialog>.aui-dialog--big .create-type__item',
    ).filter(ele => {
      return ele
        .$('.create-type__title')
        .getText()
        .then(text => {
          return text.trim() == name;
        });
    });
    return sc_types.count().then(ct => {
      if (ct > 0) {
        return new AlaudaButton(
          sc_types.first().$("button[class*='aui-button--primary']>span"),
        ).click();
      }
    });
  }
  /**
   * 创建storage class
   * @param testData 测试数据
   */
  create(testData) {
    this.getButtonByText('创建存储类')
      .click()
      .then(() => {
        const prepare_page = new PreparePage();
        if (prepare_page.storageclassGraphicalIsEnabled()) {
          this.selectScType('YAML');
        } else {
          browser.sleep(100);
        }
      });
    for (const key in testData) {
      this.enterValue(key, testData[key]);
    }
    this.getButtonByText('创建').click();
    // this.confirmDialog.clickConfirm();
    this.waitElementPresent($('h3>span'), '页面没有跳转到存储类详情页');
  }
}
