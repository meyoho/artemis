import { ServerConf } from '@e2e/config/serverConf';
import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';

export class PreparePage extends AcpPageBase {
  /**
   * 获取图形化创建存储类功能开关状态
   */
  storageclassGraphicalIsEnabled() {
    return this.featureIsEnabled(
      'storageclass-graphical',
      ServerConf.REGIONNAME,
    );
  }
  /**
   * 获取集群默认存储类列表
   * @param cluster
   */
  defaultScs(cluster = ServerConf.REGIONNAME) {
    const defaultscs = CommonKubectl.execKubectlCommand(
      'kubectl get sc -o=jsonpath=\'{.items[?(@.metadata.annotations.storageclass\\.kubernetes\\.io/is-default-class=="true")].metadata.name}\'',
      cluster,
    );
    console.log(defaultscs);
    if (defaultscs.length > 0) {
      return defaultscs.split(' ');
    } else {
      return [];
    }
  }
  /**
   * 取消设置默认存储类
   * @param sc_name
   * @param cluster
   */
  unsetDefaultClass(sc_name, cluster = ServerConf.REGIONNAME) {
    const cmd = `kubectl annotate storageclasses ${sc_name} 'storageclass.kubernetes.io/is-default-class'="false" --overwrite`;
    CommonKubectl.execKubectlCommand(cmd, cluster);
  }
  /**
   * 设置默认存储类
   * @param sc_name
   * @param cluster
   */
  setAssignDefaultClass(sc_name, cluster = ServerConf.REGIONNAME) {
    const cmd = `kubectl annotate storageclasses ${sc_name} 'storageclass.kubernetes.io/is-default-class'="true" --overwrite`;
    CommonKubectl.execKubectlCommand(cmd, cluster);
  }
  /**
   * 确保业务集群上只有一个默认的存储类(cephfs)
   */
  setDefaultClass() {
    const storageclasslist = CommonMethod.parseYaml(
      CommonKubectl.execKubectlCommand(
        'kubectl get storageclass -o yaml',
        ServerConf.REGIONNAME,
      ),
    );
    storageclasslist.items.forEach(item => {
      const name = item.metadata.name;
      const cmd = `kubectl annotate storageclasses ${name} 'storageclass.kubernetes.io/is-default-class'="false" --overwrite`;
      if (name !== 'cephfs') {
        if (
          item.metadata.annotations[
            'storageclass.kubernetes.io/is-default-class'
          ] !== 'false'
        ) {
          CommonKubectl.execKubectlCommand(cmd, ServerConf.REGIONNAME);
        }
      }
    });
  }

  /**
   * 删除PV
   * @param pvName pv 的名称
   */
  deletePv(pvName: string) {
    CommonKubectl.deleteResource(
      'PersistentVolume',
      pvName,
      ServerConf.REGIONNAME,
    );
  }

  /**
   * 删除PVC
   * @param pvcName PVC 名称
   * @param nsName 命名空间名称
   */
  deletePvc(
    pvcName: string,
    nsName: string,
    regionName = ServerConf.REGIONNAME,
  ) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete PersistentVolumeClaim ${pvcName} -n ${nsName}`,
      regionName,
    );
  }

  /**
   * 创建一个持久卷
   * @param pvName 持久卷名称
   */
  createPv(pvName: string) {
    CommonKubectl.createResourceByTemplate(
      'alauda.pv.template.yaml',
      { pv_name: pvName, path: '/tmp/pv' },
      'hostpath_pv.yaml',
      ServerConf.REGIONNAME,
    );
  }
  /**
   * 通过模板创建pvc
   * @param data 数据
   */
  createPvcByYaml(data, regionName = ServerConf.REGIONNAME) {
    return CommonKubectl.createResourceByTemplate(
      'alauda.pvc.template.yaml',
      data,
      'alauda.pvc',
      regionName,
    );
  }
  /**
   * 创建一个nsf 持久卷
   * @param name 持久卷名称
   */
  createnfsPv(name: string) {
    CommonKubectl.createResourceByTemplate(
      'alauda.pv.template.yaml',
      {
        pv_name: name,
        nfs_server: ServerConf.NFS_SERVER,
        path: '/exported/rwo',
      },
      'nfs_pv.yaml',
      ServerConf.REGIONNAME,
    );
  }

  /**
   * 删除存储类
   * @param name 存储类名称
   */
  deleteStorageClass(name: string) {
    CommonKubectl.deleteResource('StorageClass', name, ServerConf.REGIONNAME);
  }
}
