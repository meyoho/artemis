import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { PvcDetailVerify } from '@e2e/page_verify/container/storage/pvc_detail.page';
import { PvcListVerify } from '@e2e/page_verify/container/storage/pvc_listVerify.page';
import { PvDetailVerify } from '@e2e/page_verify/container/storage/pv_detail.page';
import { PvListVerify } from '@e2e/page_verify/container/storage/pv_list.page';
import { ScCreatePageVerify } from '@e2e/page_verify/container/storage/storage_class_detail.page';
import { SCListVerify } from '@e2e/page_verify/container/storage/storage_class_list.page';

import { PreparePage } from './prepare.page';
import { PvcCreatePage } from './pvc_create.page';
import { PvcDetailPage } from './pvc_detail.page';
import { PvcList } from './pvc_list.page';
import { PvcUpdatePage } from './pvc_update.page';
import { PvCreatePage } from './pv_create.page';
import { PvDetailPage } from './pv_detail.page';
import { PVList } from './pv_list.page';
import { PvUpdatePage } from './pv_update.page';
import { ScCreatePage } from './storage_class_create.page';
import { ScDetailPage } from './storage_class_detail.page';
import { SCList } from './storage_class_list.page';

export class StoragePage extends AcpPageBase {
    get pv_create(): PvCreatePage {
        return new PvCreatePage();
    }

    get pv_update(): PvUpdatePage {
        return new PvUpdatePage();
    }

    get pv_detail(): PvDetailPage {
        return new PvDetailPage();
    }

    get pv_detailVerify(): PvDetailVerify {
        return new PvDetailVerify();
    }

    get pv_list(): PVList {
        return new PVList();
    }

    get pv_listVerify(): PvListVerify {
        return new PvListVerify();
    }

    get pvc_create(): PvcCreatePage {
        return new PvcCreatePage();
    }

    get pvc_update(): PvcUpdatePage {
        return new PvcUpdatePage();
    }

    get pvc_detail(): PvcDetailPage {
        return new PvcDetailPage();
    }

    get pvc_detailVerify(): PvcDetailVerify {
        return new PvcDetailVerify();
    }

    get pvc_list(): PvcList {
        return new PvcList();
    }

    get pvc_listVerify(): PvcListVerify {
        return new PvcListVerify();
    }
    get sc_create(): ScCreatePage {
        return new ScCreatePage();
    }

    get sc_detail(): ScDetailPage {
        return new ScDetailPage();
    }

    get sc_detailVerify(): ScCreatePageVerify {
        return new ScCreatePageVerify();
    }

    get sc_list(): SCList {
        return new SCList();
    }

    get sc_listVerify(): SCListVerify {
        return new SCListVerify();
    }

    get prePareData() {
        return new PreparePage();
    }
}
