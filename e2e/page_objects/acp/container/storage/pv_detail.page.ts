import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { $, $$, ElementFinder, by, element, promise } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

export class PvDetailPage extends AcpPageBase {
    /**
     * 返回pv详情页title: 存储/持久卷/<name>/
     */
    get title(): ElementFinder {
        this.waitElementPresent(
            $('.aui-page__toolbar rc-default-breadcrumb'),
            'pv详情页title'
        );
        return $('.aui-page__toolbar rc-default-breadcrumb');
    }
    /**
     * 返回pv名称
     */
    get name(): ElementFinder {
        this.waitElementPresent(
            $('.aui-card .aui-card__header'),
            'pv 详情页名称（.aui-card .aui-card__header）没有出现'
        );
        return $('.aui-card .aui-card__header');
    }
    /**
     * 点击切换tab页
     * @param tab_name tab页名称如 YAML
     */
    clickTab(tab_name: string) {
        element(
            by.xpath(
                '//div[@class="aui-tab-label__content" and contains(text(),"' +
                    tab_name +
                    '")]'
            )
        ).click();
    }
    /**
     * 点击操作选项
     * @param option 操作选项名称
     */
    clickOption(option: string): promise.Promise<void> {
        const select = new AlaudaDropdown(
            $('.aui-card__header>button'),
            $$('aui-menu-item>button')
        );
        return select.select(option);
    }

    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'rc-field-set-item',
            '.field-set-item__label label',
            $('rc-field-set-group')
        );
    }

    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(
        text: string,
        tagname: string = '.field-set-item__value'
    ): any {
        switch (text) {
            case '状态':
                return this.alaudaElement
                    .getElementByText(text, tagname)
                    .$('.rc-status-text');
            default:
                return this.alaudaElement.getElementByText(text, tagname);
        }
    }

    /**
     * 返回yaml元素
     */
    yamlElement() {
        return new AlaudaYamlEditor();
    }
}
