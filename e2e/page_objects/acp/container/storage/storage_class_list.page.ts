import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { $, by, element } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class SCList extends AcpPageBase {
  /**
   * 存储类列表 title: 存储/存储类
   */
  get title() {
    this.waitElementPresent(
      $('aui-breadcrumb .aui-breadcrumb'),
      '存储类列表页title',
    );
    return $('aui-breadcrumb .aui-breadcrumb').getText();
  }

  /**
   * 点击页面按钮
   * @param name 按钮名称
   */

  clickButon(name: string) {
    const button = element
      .all(by.xpath('//button//span[contains(text(),"' + name + '")]'))
      .filter(ele => {
        return ele.isEnabled();
      })
      .first();
    this.waitElementPresent(button, `没有找到按钮${name}`);
    return button.click();
  }
  /**
   * 存储类列表
   */
  get scTable() {
    return new AuiTableComponent($('aui-card>.aui-card'));
  }

  /**
   * 单击按钮后，出现的确认框
   */
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
  }

  /**
   * 设置存储类为默认
   * @param sc_name 存储类的名称
   */
  setDefault(sc_name: string) {
    this.scTable.clickOperationButtonByRow(
      [sc_name],
      '设为默认',
      '.aui-button',
      'aui-menu-item button',
    );
  }

  /**
   * 取消设置存储类为默认
   * @param sc_name 存储类的名称
   */
  unSetDefault(sc_name: string) {
    this.scTable.clickOperationButtonByRow(
      [sc_name],
      '取消默认',
      '.aui-button',
      'aui-menu-item button',
    );
  }
  clickRefreshButton() {
    return new AlaudaButton($('aui-search+button')).click();
  }
  /**
   * 删除存储类
   * @param sc_name 存储类的名称
   */
  delete(sc_name: string) {
    this.scTable.clickOperationButtonByRow(
      [sc_name],
      '删除',
      '.aui-button',
      'aui-menu-item button',
    );
    this.confirmDialog.clickConfirm();
    return this.clickRefreshButton();
  }
}
