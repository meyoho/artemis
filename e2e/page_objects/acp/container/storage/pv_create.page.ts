import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { PageBase } from '@e2e/page_objects/page.base';
import { $, browser } from 'protractor';

import { AuiInputGroup } from '../element_objects/aui_input_group';

import { PvDetailPage } from './pv_detail.page';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';

export class PvCreatePage extends PageBase {
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '名称':
      case '服务地址':
        return new AlaudaInputbox(this.getElementByText_xpath(text));
      case '路径':
        return new AlaudaInputbox(
          this.getElementByText_xpath(text, 'input[not(@disabled)]'),
        );
      case '大小':
        return new AuiInputGroup(
          this.getElementByText_xpath(text, 'aui-input-group'),
        );
      case '类型':
      case '访问模式':
      case '回收策略':
        return new AlaudaRadioButton(
          this.getElementByText_xpath(text, 'aui-radio-group'),
        );
      case '标签':
      case '注解':
        return new ArrayFormTable(
          this.getElementByText_xpath(text, 'rc-array-form-table'),
          '.rc-array-form-table__bottom-control-buttons button',
        );
      case '高级':
        return $('.aui-form-item__content>button aui-icon>svg');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value: string) {
    switch (name) {
      case '名称':
      case '路径':
      case '服务地址':
        const test: AlaudaInputbox = this.getElementByText(name);
        test.input(value);
        break;
      case '大小':
        this.getElementByText(name).inputbox.input(value);
        break;
      case '类型':
      case '访问模式':
      case '回收策略':
        this.getElementByText(name).clickByName(value);
        break;
      case '标签':
      case '注解':
        this.getElementByText(name).fill(value);
        break;
      case '高级':
        if (value === 'true') {
          this.expandAdvence();
        } else {
          this.foldAdvence();
        }
        break;
    }
  }

  /**
   * 创建PV
   * @param value 测试数据
   */
  create(value) {
    this.clickLeftNavByText('持久卷');
    this.waitElementPresent(
      this.getButtonByText('创建持久卷').button,
      '创建持久卷 按钮没有出现',
    );
    this.getButtonByText('创建持久卷').click();
    this.fillForm(value);
    this.getButtonByText('创建').click();
    const detailpage = new PvDetailPage();
    this.waitElementPresent(
      detailpage.name,
      '单击创建按钮后，页面没有跳转到详情页',
    );

    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  get foldableBlock() {
    return new FoldableBlock($('.aui-form-item__content>button[size="small"]'));
  }
  /**
   * 展开高级
   * @param elem
   */
  expandAdvence() {
    this.foldableBlock.expand();
    return browser.sleep(1);
  }

  /**
   * 折叠高级
   * @param elem
   */
  foldAdvence() {
    this.foldableBlock.fold();
    return browser.sleep(1);
  }
}
