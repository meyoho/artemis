import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$ } from 'protractor';

import { PvcDetailPage } from './pvc_detail.page';
import { PVList } from './pv_list.page';

export class PvcList extends PVList {
    /**
     * 检索一个持久卷
     * @param name 持久卷名称
     * @param resultCount 期望检索结果的个数
     */
    search(name: string, resultCount: number = 1) {
        this.clickLeftNavByText('存储');
        this.pvTable.searchByResourceName(name, resultCount);
    }

    /**
     * 检索持久卷
     * @param name 持久卷名称
     */
    viewDatail(name: string): any {
        this.search(name);
        this.pvTable.clickResourceNameByRow([name]);
        const detailPage = new PvcDetailPage();
        this.waitElementPresent(
            detailPage.name,
            '持久卷列表页单击名称后，详情页没有打开'
        );
        return detailPage;
    }

    /**
     * 删除持久卷
     * @param name 持久卷名称
     */
    delete(name: string) {
        this.search(name);
        this.pvTable.clickResourceNameByRow([name]);
        const option = new AlaudaDropdown(
            $('.aui-card__header>button'),
            $$('aui-menu-item>button')
        );
        option.select('删除');
        this.confirmDialog.clickConfirm();
    }
}
