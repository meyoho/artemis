import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, promise, browser } from 'protractor';

import { AuiInputGroup } from '../element_objects/aui_input_group';

//import { PvcDetailPage } from './pvc_detail.page';
import { PvCreatePage } from './pv_create.page';

export class PvcCreatePage extends PvCreatePage {
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '名称':
        return new AlaudaInputbox(this.getElementByText_xpath(text, tagname));
      case '大小':
        return new AuiInputGroup(
          this.getElementByText_xpath(text, 'aui-input-group'),
        );
      case '访问模式':
        return new AlaudaRadioButton(
          this.getElementByText_xpath(text, 'aui-radio-group'),
        );
      case '标签':
      case '注解':
      case '选择器':
        return new ArrayFormTable(
          this.getElementByText_xpath(text, 'rc-array-form-table'),
          '.rc-array-form-table__bottom-control-buttons button',
        );
      case '存储类':
        return new AuiSelect(
          this.getElementByText_xpath(text, 'aui-select'),
          $('.cdk-overlay-pane aui-tooltip'),
        );
      case '高级':
        return $('.aui-form-item__content>button aui-icon>svg');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value: string, tagname = 'input') {
    switch (name) {
      case '名称':
        this.getElementByText(name, tagname).input(value);
        break;
      case '大小':
        this.getElementByText(name, tagname).inputbox.input(value);
        break;
      case '访问模式':
        this.getElementByText(name).clickByName(value);
        break;
      case '标签':
      case '注解':
      case '选择器':
        this.expandAdvence().then(() => {
          const tmp: ArrayFormTable = this.getElementByText(name);
          tmp.scrollToBoWindowBottom();
          this.getElementByText(name).fill(value);
        });
        break;
      case '存储类':
        this.getElementByText(name).select(value);
        break;
      case '高级':
        if (value === 'true') {
          this.expandAdvence();
        } else {
          this.foldAdvence();
        }
        break;
    }
  }

  /**
   * 创建PVC
   * @param value 测试数据
   */
  create(value): promise.Promise<void> {
    this.clickLeftNavByText('存储');
    this.waitElementPresent(
      this.getButtonByText('创建持久卷声明').button,
      '创建持久卷声明 按钮没有出现',
    );
    this.getButtonByText('创建持久卷声明').click();
    this.fillForm(value);
    browser.sleep(1000);
    this.getButtonByText('创建').click();

    // 创建成功后，跳到列表页面

    return this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
