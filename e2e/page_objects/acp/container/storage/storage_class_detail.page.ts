import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { $, $$, by, element, promise } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

export class ScDetailPage extends AcpPageBase {
    /**
     * 返回storageclass详情页title: 存储/存储类/<name>/
     */
    get title(): promise.Promise<String> {
        this.waitElementPresent(
            $('aui-breadcrumb>.aui-breadcrumb'),
            'sc详情页title'
        );
        return $('aui-breadcrumb>.aui-breadcrumb').getText();
    }
    /**
     * 返回pv名称
     */
    get name(): promise.Promise<String> {
        this.waitElementPresent(
            $('.aui-card__header h3'),
            'storage class 详情页，名称加载超时'
        );
        return $('.aui-card__header h3').getText();
    }
    /**
     * 点击切换tab页
     * @param tab_name tab页名称如 YAML
     */
    clickTab(tab_name: string) {
        element(
            by.xpath(
                '//div[@class="aui-tab-label__content" and contains(text(),"' +
                    tab_name +
                    '")]'
            )
        ).click();
    }
    /**
     * 点击操作选项
     * @param option 操作选项名称
     */
    clickOption(option: string): promise.Promise<void> {
        const select = new AlaudaDropdown(
            $('.aui-card__header>button'),
            $$('aui-menu-item>button')
        );
        return select.select(option);
    }
    /**
     * 返回yaml元素
     */
    yamlElement() {
        return new AlaudaYamlEditor();
    }
    /**
     * 获取详情页的信息
     * @param key 属性的key
     * 如getDetail("创建时间")
     */
    getDetail(key: string): promise.Promise<string> {
        return this.getElementByText(key).getText();
    }

    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'rc-field-set-item',
            '.field-set-item__label label'
        );
    }

    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(
        text: string,
        tagname: string = '.field-set-item__value'
    ): any {
        return this.alaudaElement.getElementByText(text, tagname);
    }
}
