import { PvCreatePage } from './pv_create.page';
import { PvDetailPage } from './pv_detail.page';
import { PVList } from './pv_list.page';

export class PvUpdatePage extends PvCreatePage {
    /**
     * 更新PV
     * @param value 测试数据
     */
    update(name: string, value) {
        this.clickLeftNavByText('持久卷');
        const listPage = new PVList();
        const detailPage: PvDetailPage = listPage.viewDatail(name);

        detailPage.clickOption('更新');
        this.waitElementPresent(
            this.getButtonByText('更新').button,
            '单击详情页右上角操作--> 更新后，更新页面没有打开'
        );

        this.fillForm(value);
        this.getButtonByText('更新').click();
        // this.toast.getMessage().then(message => {
        //     console.log(message);
        // });

        this.waitElementPresent(
            detailPage.name,
            '单击更新按钮后，页面没有跳转到详情页'
        );
    }
}
