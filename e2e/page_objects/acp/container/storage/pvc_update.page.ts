import { AlaudaElement } from '@e2e/element_objects/alauda.element';

import { PvcCreatePage } from './pvc_create.page';
import { PvcList } from './pvc_list.page';

export class PvcUpdatePage extends PvcCreatePage {
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-form-item>.aui-form-item',
            '.aui-form-item__label-wrapper--right>label'
        );
    }

    /**
     * 创建PV
     * @param value 测试数据
     */
    update(name: string, value) {
        this.clickLeftNavByText('存储');
        const listPage = new PvcList();
        listPage.viewDatail(name).clickOption('更新');

        this.fillForm(value);
        this.getButtonByText('更新').click();
        // this.toast.getMessage().then(message => {
        //     console.log(message);
        // });

        // this.waitElementPresent(
        //     this.getButtonByText('更新').button,
        //     '单击更新按钮后，页面没有跳转到详情页'
        // );
    }
}
