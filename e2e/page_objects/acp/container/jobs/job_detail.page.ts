import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, by, element, $$, browser } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class JobDetailPage extends AcpPageBase {
  /**
   * 返回定时任务记录详情页title: 任务/任务记录/<名称>
   */
  get title() {
    return this.breadcrumb;
  }
  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog .aui-dialog'),
      '.aui-confirm-dialog__content',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
      '.aui-confirm-dialog__title',
    );
  }
  /**
   * 返回任务记录名称
   */
  get name() {
    this.waitElementPresent(
      $('rc-job-detail-info>aui-card .aui-card__header'),
      '定时任务记录名称',
    );
    return $('rc-job-detail-info>aui-card .aui-card__header');
  }
  /**
   * 返回定时任务记录容器列表
   */
  get containers_table() {
    return new AuiTableComponent($('rc-job-detail-pod-list'));
  }
  /**
   * 返回定时任务记录k8s时间列表
   */
  get k8s_eventstable() {
    return new AuiTableComponent($('rc-k8s-event-list'));
  }
  /**
   * 点击tab页
   * @param tab_name  tab页名称 yaml 详情
   */
  clickTab(tab_name: string) {
    element(
      by.xpath(
        '//div[@class="aui-tab-label__content" and contains(text(),"' +
          tab_name +
          '")]',
      ),
    ).click();
  }
  /**
   * 根据key: value结构的页面元素名称找到页面元素
   * @param text 名称
   */
  getElementByText(text: string) {
    const ele = element(
      by.xpath(
        `//div[contains(@class,'item__label')]/label[contains(text(),'${text}')]/../../div[contains(@class,'item__value')]`,
      ),
    );
    this.waitElementPresent(ele, `${text} 未出现`);
    return ele;
  }
  /** */
  podsLogElement() {
    this.waitElementPresent($('rc-pods-log'), '日志控件未出现').then(
      ispresent => {
        if (!ispresent) {
          throw new Error('日志控件未出现');
        }
      },
    );
    return $('rc-pods-log');
  }
  clickOperation(operation) {
    return new AlaudaDropdown(
      $('rc-job-detail-info .aui-card__header>button'),
      $$('aui-tooltip aui-menu-item'),
    ).select(operation);
  }
  delete(flag = '确定') {
    return this.clickOperation('删除').then(() => {
      if (flag === '确定') {
        this.auiDialog.clickConfirm();
        this.waitElementNotPresent(this.name, '等待页面跳转到job列表页');
        return browser.sleep(1);
      } else if (flag === '取消') {
        return this.auiDialog.clickCancel();
      }
    });
  }
}
