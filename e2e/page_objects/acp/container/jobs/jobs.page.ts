import { CronjobDetailVerify } from '@e2e/page_verify/container/jobs/cronjob_detail.page';
import { CronjobListVerify } from '@e2e/page_verify/container/jobs/cronjob_list.page';
import { JobDetailVerify } from '@e2e/page_verify/container/jobs/job_detail.page';
import { JobListVerify } from '@e2e/page_verify/container/jobs/job_list.page';

import { AcpPageBase } from '../../acp.page.base';

import { CronjobCreatePage } from './cronjob_create.page';
import { CronJobDetail } from './cronjob_detail.page';
import { CronjobList } from './cronjob_list.page';
import { JobDetailPage } from './job_detail.page';
import { JobList } from './job_list.page';
import { PrepareData } from './prepare.page';

export class JobPage extends AcpPageBase {
    get createCronjobPage() {
        return new CronjobCreatePage();
    }
    get detailCronjonPage() {
        return new CronJobDetail();
    }
    get cronjobListPage() {
        return new CronjobList();
    }
    get cronjobDetailVerify() {
        return new CronjobDetailVerify();
    }
    get cronjobListVerify() {
        return new CronjobListVerify();
    }
    get jobDetailPage() {
        return new JobDetailPage();
    }
    get jobListPage() {
        return new JobList();
    }
    get jobDetailVerifyPage() {
        return new JobDetailVerify();
    }
    get jobListVerifyPage() {
        return new JobListVerify();
    }
    get preparePage() {
        return new PrepareData();
    }
}
