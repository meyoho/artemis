import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $, by, element, promise } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

export class JobList extends AcpPageBase {
  refreshButton() {
    return new AlaudaButton(element(by.css('aui-search+.aui-button')));
  }
  get jobtable() {
    return new AuiTableComponent(
      $('rc-job-record-list'),
      'aui-table',
      'aui-card>div>.aui-card__header',
    );
  }
  get jobPageTitle() {
    return new AlaudaText(by.css('.aui-breadcrumb'));
  }
  /**
   * 单击操作下的选项
   * @param name 名称
   * @param actionName 更新，或删除
   */
  operation(name: string, actionName: string) {
    // this.search(name, 1);
    this.jobtable.clickOperationButtonByRow(
      [name],
      actionName,
      'button',
      'aui-menu-item>button',
    );
  }
  getJobNameFromCronjob(cronjob_name) {
    return this.jobtable.getCell('名称', [cronjob_name]).then(ele => {
      return ele.getText();
    });
  }
  /**
   * 搜索定时任务历史
   * @param name 定时任务名称
   * return 任务历史名称
   */
  search(name: string, resultCount = 1): promise.Promise<string> {
    this.clickLeftNavByText('任务记录');
    return this.jobtable.getCell('名称', [name]).then(jobname_ele => {
      const jobname = jobname_ele.getText();
      return jobname.then(_jobname => {
        this.jobtable.searchByResourceName(_jobname, resultCount);
        return _jobname;
      });
    });
  }
  /**
   * 进入任务历史详情
   * @param name 定时任务名称
   * return 任务历史名称
   */
  toDetail(name: string): promise.Promise<string> {
    this.clickLeftNavByText('任务记录');
    let jobname: promise.Promise<string>;
    return this.jobtable.getCell('名称', [name]).then(jobname_ele => {
      jobname = jobname_ele.getText();
      return jobname.then(_jobname => {
        this.jobtable.clickResourceNameByRow([_jobname]);
        return _jobname;
      });
    });
  }
  /**
   * 删除任务历史
   * @param name 定时任务名称
   * return 任务历史名称
   */
  delete(name: string): promise.Promise<string> {
    this.clickLeftNavByText('任务记录');
    return this.jobtable.getCell('名称', [name]).then(jobname_ele => {
      const jobname = jobname_ele.getText();
      return jobname.then(_jobname => {
        this.operation(_jobname, '删除');
        this.confirmDialog.clickConfirm();
        return _jobname;
      });
    });
  }
}
