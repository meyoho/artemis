import { ImageSelect } from '@e2e/element_objects/acp/alauda.image_select';
import { AlaudaAuiNumberInput } from '@e2e/element_objects/alauda.aui_number_input';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { TableInput } from '@e2e/element_objects/alauda.table_input';
import {
  $,
  /*$, by, element,*/ ElementFinder,
  browser,
  element,
  by,
} from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

import { CronjobList } from './cronjob_list.page';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';

export class CronjobCreatePage extends AcpPageBase {
  // private _inputGroup: ElementFinder;
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  private _number_re = /^\d+/g;
  private _unit_re = /\D$/g;

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }
  _getElementByText(
    left: string,
    tagname = 'input',
    root_xpath = '/',
  ): ElementFinder {
    const xpath = `${root_xpath}/aui-form-item//label[@class="aui-form-item__label" and normalize-space(text())="${left}"]/ancestor::aui-form-item//${tagname}`;
    return element(by.xpath(xpath));
  }
  get podElement() {
    return new AlaudaElement(
      'aui-form-item>div',
      'label',
      $('rc-pod-template-form'),
    );
  }
  get listPage(): CronjobList {
    return new CronjobList();
  }
  switchCreateMode(mode: string) {
    return new AlaudaRadioButton(
      $('.aui-card__header>aui-radio-group'),
      '.aui-radio-button__content',
    ).clickByName(mode);
  }
  auiSelect(ele: ElementFinder): AuiSelect {
    return new AuiSelect(
      ele.$('form>aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '镜像仓库':
        return new ImageSelect(
          this._getElementByText('镜像仓库', 'aui-form-item__content'),
          $('aui-tooltip'),
        );
      case '名称':
        return new AlaudaInputbox(
          this._getElementByText('名称', 'input', '//rc-cron-job-form/form'),
        );
      case '方式':
      case '执行方式':
      case '任务类型':
        return new AlaudaRadioButton(
          this._getElementByText(text, 'aui-radio-group'),
          '.aui-radio-button__content',
        );
      case '定时并发策略':
        return new AlaudaRadioButton(
          this._getElementByText(text, 'aui-radio-group'),
          '.aui-radio__content',
        );
      case '计划成功次数':
      case '最大并行次数':
      case '失败重试次数':
        return new AlaudaAuiNumberInput(
          this._getElementByText(text, 'aui-number-input'),
        );
      case '任务超时时间':
        return new AlaudaInputGroup(
          this._getElementByText(text, 'rc-cron-job-config-time-form'),
          '',
          'form>input',
        );
      case '容器名称':
        return new AlaudaInputbox(
          this._getElementByText('名称', 'input', '//rc-pod-template-form/'),
        );
      case 'cpu':
        return new AlaudaInputGroup(
          $('.input-groups-wrapper:nth-child(1)'),
          'addon-label',
        );
      case '内存':
        return new AlaudaInputGroup(
          $('.input-groups-wrapper:nth-child(2)'),
          'addon-label',
        );
      case '环境变量':
        return new TableInput(
          this._getElementByText(text, 'table', '//rc-pod-template-form/'),
        );
      case '配置引用':
        return new AuiMultiSelect(
          this._getElementByText(
            text,
            'aui-multi-select',
            '//rc-pod-template-form/',
          ),
          $('aui-tooltip'),
        );
      case '启动命令':
      case '参数':
      case '日志文件':
      case '排除日志文件':
        return new ArrayFormTable(
          this._getElementByText(
            text,
            'rc-string-array-form-table',
            '//rc-pod-template-form/',
          ),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)>span',
        );
      case '端口':
        return new ArrayFormTable(
          this._getElementByText(
            text,
            'rc-array-form-table',
            '//rc-pod-template-form/',
          ),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)>span',
        );
      case '凭据':
        return new AuiMultiSelect(
          this._getElementByText(
            text,
            'aui-multi-select',
            '//rc-pod-template-form/',
          ),
          $('.cdk-overlay-pane aui-tooltip'),
        );
      default:
        return new AlaudaInputbox(this._getElementByText(text, 'input'));
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value: any) {
    browser.sleep(1).then(() => {
      switch (name) {
        case '镜像':
          // {方式: '输入', 镜像地址: 'index.docker.io/nginx:latest'}
          if ('方式' in value && value['方式'] === '输入') {
            this.getElementByText('方式').clickByName(value['方式']);
            this.getElementByText('镜像地址').input(value['镜像地址']);
            browser.sleep(1000);
          } else {
            // {repository: 'index.docker.io/nginx', tag: 'latest'}
            this.getElementByText('镜像仓库').select(
              value['repository'],
              value['tag'],
            );
          }
          this.auiDialog.buttonConfirm.click();
          break;
        case '执行方式':
        case '任务类型':
        case '定时并发策略':
          this.getElementByText(name).clickByName(value);
          break;
        case '保留任务历史':
          for (const key in value) {
            //const element = value[key];
            const input_ele = new AlaudaInputbox(
              new AlaudaElement(
                'aui-input-group',
                '.aui-input-group__addon--before',
                this._getElementByText(
                  name,
                  'div[@class="aui-form-item__content"]',
                ),
              ).getElementByText(key),
            );
            input_ele.input(value[key]);
          }
          break;
        case '容器大小':
          this.alaudaElement.scrollToBoWindowBottom();
          // cpu 1核/1m 内存 1Mi/1Gi
          for (const key in value) {
            this.getElementByText(key).input(
              this._number_re.exec(value[key])[0],
              this._unit_re.exec(value[key])[0],
            );
          }
          break;
        case '环境变量':
          //value {"键":["key1","key2","key3"...],"值":["value1","value2","value3"...]}
          this.foldableBlock.expand();
          let line = 0;
          for (const _key in value) {
            if (value[_key].length > line) {
              line = value[_key].length;
            }
          }
          for (let i = 0; i < line; i++) {
            this.getElementByText(name).add_row();
          }
          this.getElementByText(name).inputTableValues(value);
          break;
        case '计划成功次数':
        case '最大并行次数':
        case '失败重试次数':
          this.getElementByText(name).input(value);
          break;
        case '任务超时时间':
          // 1秒/1分钟/1小时/1天
          this.alaudaElement.scrollToBottom();
          const input_value = this._number_re.exec(value)[0];
          const input_unit = this._unit_re.exec(value)[0];
          this.getElementByText(name).input(input_value, input_unit);
          break;
        case '配置引用':
          this.foldableBlock.expand();
          value.forEach(v => {
            this.getElementByText(name).select(v[0], v[1]);
          });
          this.foldableBlock.fold();
          break;
        case '启动命令':
        case '参数':
        case '日志文件':
        case '排除日志文件':
        case '端口':
          this.foldableBlock.expand();
          this.getElementByText(name).fill(value);
          break;
        case '凭据':
          // this.podFoldableBlock.expand();
          this.getElementByText(name).select(value);
          break;
        case 'YAML':
          this.switchCreateMode('YAML');
          new AlaudaYamlEditor().checkEditorIsPresent().then(ispresent => {
            if (ispresent) {
              new AlaudaYamlEditor().setYamlValue(value);
            }
          });
          break;
        default:
          this.getElementByText(name).input(value);
          break;
      }
    });
  }

  /**
   * 创建定时任务
   * @param testData 数据
   */
  create(testData) {
    this.listPage.clickLeftNavByText('定时任务');
    this.listPage.createCronjobButton().click();
    this.fillForm(testData);
    this.clickConfirm();
    this.waitElementNotPresent(
      $('rc-cron-job-form-container'),
      '创建定时任务页面跳转失败',
    );
  }
  /**
   * 更新定时任务
   * @param name 定时任务名称
   * @param testData 数据
   */
  update(name: string, testData) {
    this.listPage.clickLeftNavByText('定时任务');
    this.listPage.operation(name, '更新');
    this.fillForm(testData);
    this.clickConfirm();
    this.waitElementNotPresent(
      $('rc-cron-job-form-container'),
      '创建定时任务页面跳转失败',
    );
  }
  /**
   * 点击容器组高级选项，展开更多输入框
   */
  clickContainerHighOption() {
    $('.pod-spec-container-tabs  .foldable-wrapper aui-icon>svg')
      .getAttribute('class')
      .then(class_value => {
        if (class_value.endsWith('down')) {
          $('.pod-spec-container-tabs  .foldable-wrapper aui-icon>svg').click();
        }
      });
  }
  get foldableBlock() {
    return new FoldableBlock(
      $('rc-container-form .aui-form-item__content>button[size="small"]'),
    );
  }
  get podFoldableBlock() {
    return new FoldableBlock(
      $('aui-tab-group+rc-foldable-block .foldable-trigger__button'),
    );
  }
}
