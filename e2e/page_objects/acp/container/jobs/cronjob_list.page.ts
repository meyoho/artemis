import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $, ElementFinder, browser, by, element } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

export class CronjobList extends AcpPageBase {
  /*
    定时任务列表页点击创建按钮
    */
  createCronjobButton() {
    return new AlaudaButton(element(by.buttonText('创建定时任务')));
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string, tagname = 'input'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value, tagname = 'input') {
    this.getElementByText(name, tagname).clear();
    this.getElementByText(name, tagname).sendKeys(value);
    // browser.sleep(100);
  }
  /*
    在创建定时任务选择镜像弹窗中输入镜像
    */
  inputImage(image: string): void {
    const ele: ElementFinder = $('input[name="image"]');
    ele.clear();
    ele.sendKeys(image);
  }
  /*
    在创建定时任务选择镜像弹窗中点击确认
    */
  ensureImageButton(): void {
    $('aui-dialog-footer button[aui-button="primary"]').click();
  }
  /*
    定时任务列表页点击刷新按钮
    */
  get refreshButton() {
    return new AlaudaButton(element(by.css('aui-search+.aui-button')));
  }
  /*
    定时任务列表页table元素 
    */
  get cronjobtable() {
    return new AuiTableComponent(
      $('aui-card>.aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
    );
  }
  /* 
    定时任务列表页title元素
    */
  get cronjobPageTitle() {
    return new AlaudaText(by.css('aui-breadcrumb>.aui-breadcrumb'));
  }
  assertColumExists(cronjob_name: string) {
    this.cronjobtable.getCell('名称', cronjob_name).catch(reason => {
      console.log(reason);
    });
  }
  /**
   * 单击操作下的选项
   * @param name 名称
   * @param actionName 更新，或删除
   */
  operation(name: string, actionName: string) {
    // this.search(name, 1);
    this.cronjobtable.clickOperationButtonByRow(
      [name],
      actionName,
      'button',
      'aui-menu-item>button',
    );
  }
  /**
   * 检索configmap
   * @param name 名称
   * @param resultCount 期望检索的个数
   */
  search(name: string, resultCount = 1) {
    this.clickLeftNavByText('定时任务');
    // browser.sleep(1000);
    this.cronjobtable.searchByResourceName(name, resultCount);
  }
  exec_menual(name: string) {
    this.clickLeftNavByText('定时任务');
    // browser.sleep(1000);
    this.operation(name, '立即执行');
    this.confirmDialog.clickConfirm();
    browser.sleep(1000);
  }
  toCronjobDetailPage(name: string) {
    this.clickLeftNavByText('定时任务');
    // browser.sleep(1000);
    return this.cronjobtable.clickResourceNameByRow([name]);
  }
  delete(name: string) {
    this.clickLeftNavByText('定时任务');
    // browser.sleep(1000);
    this.operation(name, '删除');
    this.confirmDialog.clickConfirm();
  }
}
