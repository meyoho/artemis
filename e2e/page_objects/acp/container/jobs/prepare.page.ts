import { CommonKubectl } from '@e2e/utility/common.kubectl';
import {
  k8s_type_cronjobs,
  k8s_type_jobs,
} from '@e2e/utility/resource.type.k8s';

import { AcpPageBase } from '../../acp.page.base';
import { ServerConf } from '@e2e/config/serverConf';

export class PrepareData extends AcpPageBase {
  public delete(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_cronjobs,
      name,
      this.clusterName,
      this.namespace1Name,
    );
    this.delete_jobs(name);
  }
  public createCronjob(data, cluster = this.clusterName) {
    CommonKubectl.createResourceByTemplate(
      'alauda.cronjob.tpl.yaml',
      data,
      `cronjob-${this.getRandomNumber()}`,
      cluster,
    );
  }
  private delete_jobs(cronjob_name: string) {
    try {
      CommonKubectl.execKubectlCommand(
        `kubectl delete ${k8s_type_jobs} -n ${this.namespace1Name} -l cronjob.${ServerConf.LABELBASEDOMAIN}/name=${cronjob_name}`,
        this.clusterName,
      );
    } catch (ex) {
      console.log(ex.message);
      return ex.message;
    }
  }
  deleteJob(
    jobname,
    ns_name = this.namespace1Name,
    cluster = this.clusterName,
  ) {
    try {
      CommonKubectl.execKubectlCommand(
        `kubectl delete ${k8s_type_jobs} -n ${ns_name} ${jobname}`,
        cluster,
      );
    } catch (ex) {
      console.log(ex.message);
      return ex.message;
    }
  }
}
