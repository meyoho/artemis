import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { $, ElementFinder, by, element, promise, $$ } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';

export class CronJobDetail extends AcpPageBase {
  /**
   * 返回定时任务详情页title: 任务/定时任务/<名称>
   */
  get title() {
    return this.breadcrumb;
  }
  /**
   * 返回定时任务的名称
   */
  get name(): ElementFinder {
    this.waitElementPresent(
      $('rc-cron-job-detail-info>aui-card .aui-card__header'),
      '定时任务名称',
    );
    return $('rc-cron-job-detail-info>aui-card .aui-card__header');
  }
  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog .aui-dialog'),
      '.aui-confirm-dialog__content',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
      '.aui-confirm-dialog__title',
    );
  }
  /**
   * 获取详情页的信息
   * @param key 属性的key
   * 如getDetail("创建时间")
   */
  getDetail(key: string): promise.Promise<string> {
    const ele = new AlaudaElement('.detail-item', 'label');
    return ele.getElementByText(key, 'div').getText();
  }
  /**
   * 根据key: value结构的页面元素名称找到页面元素
   * @param text 名称
   */
  getElementByText(text: string) {
    const ele = element(
      by.xpath(
        `//div[contains(@class,'item__label')]/label[contains(text(),'${text}')]/../../div[contains(@class,'item__value')]`,
      ),
    );
    this.waitElementPresent(ele, `${text} 未出现`);
    return ele;
  }
  /**
   * 点击切换tab页
   * @param name tab 名称如YAML
   */
  clickTab(name: string) {
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(text(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }
  get yamlContent() {
    return new AlaudaYamlEditor();
  }
  /**
   * 点击操作
   * @param operation 操作按钮文字
   */
  clickOperation(operation) {
    return new AlaudaDropdown(
      $('rc-cron-job-detail-info .aui-card__header>button'),
      $$('aui-tooltip aui-menu-item'),
    ).select(operation);
  }
  delete(flag = '确定') {
    this.clickOperation('删除').then(() => {
      if (flag === '确定') {
        this.auiDialog.clickConfirm();
        this.waitElementNotPresent(this.name, '页面跳转失败');
      } else if (flag === '取消') {
        this.auiDialog.clickCancel();
      }
    });
  }
  execManual(flag = '确定') {
    this.clickOperation('立即执行').then(() => {
      if (flag === '确定') {
        this.auiDialog.clickConfirm();
        this.waitElementNotPresent(this.name, '页面跳转失败');
      } else if (flag === '取消') {
        this.auiDialog.clickCancel();
      }
    });
  }
  updateResourceLimit(data) {
    return this.getElementByText('资源限制')
      .$('acl-disabled-container')
      .click()
      .then(() => {
        const resourceform = new AuiDialog(
          $('aui-dialog>.aui-dialog--big'),
          'aui-dialog-content',
          'button[aui-button="primary"]',
          'button+button',
          'aui-dialog-header',
        );
        // {资源限制: [{CPU: 10, 单位: 'm' }, {内存: 10, 单位: 'Mi'}]
        const ct = resourceform.content as ElementFinder;
        ct.element(
          by.xpath(
            "//aui-form-item//div/label[normalize-space(text())='资源限制']/parent::div/following-sibling::div",
          ),
        )
          .$$('div[class*="input-groups"]')
          .each((elem, index) => {
            const inputBoxGroup: AlaudaInputGroup = new AlaudaInputGroup(
              elem,
              '.addon-label',
              'input[formcontrolname="value"]',
            );
            inputBoxGroup.scrollToBoWindowBottom();
            const item = data[index];

            for (const keyItem in item) {
              if (keyItem !== '单位') {
                inputBoxGroup.input(item[keyItem], item['单位']);
              }
            }
          });
        return resourceform.clickConfirm();
      });
  }
}
