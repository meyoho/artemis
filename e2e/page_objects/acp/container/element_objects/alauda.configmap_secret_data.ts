/**
 * rc-k8s-config-items 控件，
 * 配置字典详情页，数据部分有使用
 * Created by 李俊磊 on 2018/11/7.
 */

import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { $$, ElementFinder, promise } from 'protractor';

interface ConfigKey {
  index: number;
  value: string;
}
export class ConfigmapDataViewer extends AlaudaElementBase {
  private _ConfigmapDataView: ElementFinder;
  private _eleLocator: string;

  /**
   * 控件 rc-k8s-config-items
   */
  constructor(
    configmapDataViewer: ElementFinder,
    elementLocator = 'rc-k8s-config-items',
  ) {
    super();
    this._ConfigmapDataView = configmapDataViewer;
    this._eleLocator = elementLocator;
  }

  /**
   * 获得 rc-k8s-config-items 控件
   */
  get ConfigmapDataViewer(): ElementFinder {
    this.waitElementPresent(this._ConfigmapDataView).then(isPresent => {
      if (!isPresent) {
        throw new Error('控件 ' + this._eleLocator + ' 不存在');
      }
    });
    return this._ConfigmapDataView;
  }

  /**
   * 控件包含左右两个部分，左侧是键，右侧包含键和值
   * 这个函数主要用来判断左侧是键是否存在
   */
  hasData(): promise.Promise<boolean> {
    return this._ConfigmapDataView
      .$('div[class*="-keys__label-key"]')
      .isPresent();
  }

  /**
   * 判断 控件是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this._ConfigmapDataView.isPresent();
  }

  /**
   * 单击左侧键的名称，获得右侧键对应的值
   * @param data_key 左侧键的名称
   * @example click('dbname').then((value) => { console.log(value)})
   */
  get_value(data_key: string): promise.Promise<string> {
    return this.ConfigmapDataViewer.$('.secret-data-keys')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          this.click_key(data_key);
          // 等待active 的切换
          // browser.sleep(2000);
          return this._getVaueElementIndexByKey(data_key).then(index => {
            if (index != null) {
              return this._ConfigmapDataView
                .$$("div[class*='-values']>div[class*='-section']")
                .get(index)
                .$('pre')
                .getText();
            } else {
              return null;
            }
          });
        } else {
          return this.getVaueElementByKey(data_key).then(ele => {
            if (ele != null) {
              return ele.$('pre').getText();
            } else {
              return null;
            }
          });
        }
      });
  }
  /**
   * 单击左边的键名称
   * @param data_key 左侧键的名称
   */
  click_key(data_key: string) {
    const item = $$('div[class$="-keys__label-key"]')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === data_key;
        });
      })
      .first();

    this.waitElementPresent(item).then(isPresent => {
      if (!isPresent) {
        throw new Error(
          `在控件 ` + this._eleLocator + ` 中，没有找到[${data_key}]`,
        );
      }
    });
    item.click().then(() => {
      this._getVaueElementIndexByKey(data_key).then(index => {
        if (index != null) {
          this.waitElementPresent(
            this._ConfigmapDataView
              .$$("div[class*='-values']>div[class*='-section']")
              .get(index)
              .$('pre'),
          );
        }
      });
    });
  }
  /**
   * 判断是否存在key
   * @param key configmap 的key
   */
  hasKey(key: string): promise.Promise<boolean> {
    const keys = this.ConfigmapDataViewer.$$(
      'div[class*="-keys__label"]>div[class$="-keys__label-key"]',
    ).filter(ele => {
      return ele.getText().then(text => {
        return text.trim() === key;
      });
    });
    return keys.count().then(ct => {
      return ct > 0;
    });
  }
  private _getVaueElementIndexByKey(key: string) {
    return this.hasKey(key).then(haskey => {
      if (haskey) {
        const keys = this.ConfigmapDataViewer.$$(
          'div[class*="-keys__label"]>div[class$="-keys__label-key"]',
        ).map((ele, index) => {
          return { index: index, value: ele.getText() };
        });
        return keys.then(key_lsit => {
          let lineIndex = 0;
          key_lsit.forEach((keyIndex: ConfigKey) => {
            if (keyIndex.value === key) {
              lineIndex = keyIndex.index;
              return;
            }
          });
          return lineIndex;
        });
      } else {
        return null;
      }
    });
  }
  getVaueElementByKey(key: string): promise.Promise<ElementFinder> {
    return this._getVaueElementIndexByKey(key).then(index => {
      if (index != null) {
        return this.ConfigmapDataViewer.$$(
          'div[class*="-values"]>div[class*="-section"]',
        ).get(index);
      } else {
        return null;
      }
    });
  }
  clickPencil(data_key: string) {
    this.getVaueElementByKey(data_key).then(ele => {
      if (ele != null) {
        ele.$('aui-icon[icon="basic:pencil_s"]').click();
      } else {
        throw new Error(`${data_key}没有找到`);
      }
    });
  }
  clickDelete(data_key: string) {
    this.getVaueElementByKey(data_key).then(ele => {
      if (ele != null) {
        ele.$('aui-icon[icon="basic:trash"]').click();
      } else {
        throw new Error(`${data_key}没有找到`);
      }
    });
  }
}
