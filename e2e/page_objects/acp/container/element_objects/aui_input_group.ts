/**
 * aui-input-group 控件，左边一个input,右边一个单位
 * Created by liuwei on 2019/7/18.
 */
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class AuiInputGroup extends AlaudaElementBase {
    private _root: ElementFinder;

    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get auiInputGoup(): ElementFinder {
        return this._root;
    }

    /**
     * 文本框
     */
    get inputbox(): AlaudaInputbox {
        this.waitElementPresent(this.auiInputGoup);
        return new AlaudaInputbox(this.auiInputGoup.$('input'));
    }

    /**
     * 文本框右侧的单位
     */
    get unit(): ElementFinder {
        this.waitElementPresent(this.auiInputGoup);
        return this.auiInputGoup.$('span');
    }
}
