import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { k8s_type_secrets } from '@e2e/utility/resource.type.k8s';

import { AcpPageBase } from '../../acp.page.base';

export class PrepareData extends AcpPageBase {
  delete(
    name: string,
    namespace = this.namespace1Name,
    cluster = this.clusterName,
  ) {
    CommonKubectl.deleteResource(k8s_type_secrets, name, cluster, namespace);
  }
  create(values: Record<string, any>, cluster = this.clusterName) {
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      values,
      `alauda.secret_tpl.tmp-${this.getRandomNumber()}`,
      cluster,
    );
  }
}
