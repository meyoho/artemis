import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';

import { AcpPageBase } from '../../acp.page.base';
import { ItemFieldset } from '../element_objects/items_fieldset';

import { SecretListPage } from './secret_list.page';

export class SecretCreatePage extends AcpPageBase {
  // private _inputGroup: ElementFinder;
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }
  get listPage() {
    return new SecretListPage();
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '数据':
        return new ItemFieldset(
          this.alaudaElement.getElementByText(text, 'rc-secret-items-fieldset'),
        );
      case '类型':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
        );
      case '证书':
      case '私钥':
      case 'SSH 私钥':
      case '描述':
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'textarea'),
        );
      default:
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'input'),
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value: string) {
    switch (name) {
      case '数据':
        this.getElementByText(name).add(value);
        break;
      case '类型':
        this.getElementByText(name).clickByName(value);
        break;
      default:
        this.getElementByText(name).input(value);
        break;
    }
  }

  create(testData) {
    this.clickLeftNavByText('保密字典');
    this.getButtonByText('创建保密字典').click();
    this.fillForm(testData);
    this.clickConfirm();
    this.waitElementNotPresent(this.buttonConfirm.button, '', 5000);
  }
  update(name, testData) {
    this.listPage.secretTable.searchByResourceName(name, 1);
    this.listPage.secretTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-menu-item>button',
    );
    this.fillForm(testData);
    this.clickConfirm();
    this.waitElementNotPresent(this.buttonConfirm.button, '', 5000);
  }
  create_cancel(testData) {
    this.clickLeftNavByText('保密字典');
    this.getButtonByText('创建保密字典').click();
    this.fillForm(testData);
    this.getButtonByText('取消').click();
  }
  update_cancel(name, testData) {
    this.listPage.secretTable.searchByResourceName(name, 1);
    this.listPage.secretTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-menu-item>button',
    );
    this.fillForm(testData);
    this.getButtonByText('取消').click();
  }
}
