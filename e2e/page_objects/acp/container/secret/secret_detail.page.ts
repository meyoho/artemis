import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { ConfigmapDataViewer } from '@e2e/page_objects/acp/container/element_objects/alauda.configmap_secret_data';
import { $, $$, ElementFinder, by, element } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

export class SecretDetailPage extends AcpPageBase {
    /**
     * 返回定时任务记录详情页title: 任务/任务记录/<名称>
     */
    get title(): ElementFinder {
        this.waitElementPresent(
            $('aui-breadcrumb .aui-breadcrumb'),
            'Secret详情页title'
        );
        return $('aui-breadcrumb .aui-breadcrumb');
    }
    /**
     * 返回任务记录名称
     */
    get name(): ElementFinder {
        this.waitElementPresent(
            $('.aui-card__header div'),
            '.aui-card__header div 没有定位到 secret名称'
        );
        return $('.aui-card__header div');
    }

    get action(): AlaudaDropdown {
        return new AlaudaDropdown(
            $('.aui-card__header button'),
            $$('aui-menu-item button')
        );
    }

    /**
     *
     * @param tab_name tab页名称如：详细信息、YAML
     */
    clickTab(tab_name: string) {
        element(
            by.xpath(
                '//div[@class="aui-tab-label__content" and contains(text(),"' +
                    tab_name +
                    '")]'
            )
        ).click();
    }
    /**
     * 获取configmap 数据父元素
     */
    get data(): ConfigmapDataViewer {
        return new ConfigmapDataViewer(
            $('rc-secret-data-viewer'),
            'rc-secret-data-viewer'
        );
    }

    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'rc-field-set-item',
            'label',
            $('rc-field-set-group')
        );
    }

    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(
        text: string,
        tagname: string = '.field-set-item__value div'
    ): ElementFinder {
        return this.alaudaElement.getElementByText(text, tagname);
    }
}
