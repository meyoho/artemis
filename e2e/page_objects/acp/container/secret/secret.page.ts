import { SecretDetailVerify } from '@e2e/page_verify/container/secret/secret_detail.page';
import { SecretListVerify } from '@e2e/page_verify/container/secret/secret_list.page';

import { AcpPageBase } from '../../acp.page.base';

import { PrepareData } from './prepare.page';
import { SecretCreatePage } from './secret_create.page';
import { SecretDetailPage } from './secret_detail.page';
import { SecretListPage } from './secret_list.page';

export class SecretPage extends AcpPageBase {
    get createPage(): SecretCreatePage {
        return new SecretCreatePage();
    }

    get detailPage(): SecretDetailPage {
        return new SecretDetailPage();
    }

    get detailPageVerify(): SecretDetailVerify {
        return new SecretDetailVerify();
    }

    get listPage(): SecretListPage {
        return new SecretListPage();
    }

    get listPageVerify(): SecretListVerify {
        return new SecretListVerify();
    }

    get prepareData(): PrepareData {
        return new PrepareData();
    }
}
