import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $, by } from 'protractor';

import { AcpPageBase } from '../../acp.page.base';

import { SecretDetailPage } from './secret_detail.page';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';

export class SecretListPage extends AcpPageBase {
  get secretTable() {
    return new AuiTableComponent(
      $('ng-component aui-card>.aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-row',
    );
  }
  get configmapPageTitle() {
    return new AlaudaText(by.css('aui-breadcrumb>.aui-breadcrumb'));
  }
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog .aui-dialog'),
      'aui-dialog-content',
      '.aui-button--danger',
      'button[auidialogclose]',
      'aui-dialog-header',
    );
  }
  /**
   * 检索configmap
   * @param name 名称
   * @param resultCount 期望检索的个数
   */
  search(name: string, resultCount = 1) {
    this.clickLeftNavByText('保密字典');
    this.secretTable.searchByResourceName(name, resultCount);
  }

  /**
   * 单击操作下的选项
   * @param name 名称
   * @param actionName 更新，或删除
   */
  operation(name: string, actionName: string) {
    this.search(name, 1);
    this.secretTable.clickOperationButtonByRow(
      [name],
      actionName,
      'aui-table-cell button',
      'aui-menu-item>button',
    );
  }

  delete(name: string) {
    this.operation(name, '删除');
    this.auiDialog.clickConfirm();
  }

  /**
   * 检索持久卷
   * @param name 持久卷名称
   */
  viewDatail(name: string): SecretDetailPage {
    this.search(name);
    this.secretTable.clickResourceNameByRow([name]);
    const detailPage = new SecretDetailPage();
    this.waitElementPresent(
      detailPage.data.ConfigmapDataViewer.$('div[class*="-values"]>div'),
      '等待数据加载成功',
      3000,
    );
    return detailPage;
  }
}
