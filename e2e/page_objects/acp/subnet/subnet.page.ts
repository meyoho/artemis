import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { DetailPageVerify } from '@e2e/page_verify/acp/subnet/detail.page';
import { ListPageVerify } from '@e2e/page_verify/acp/subnet/list.page';

import { CreatePage } from './create.page';
import { DeletePage } from './delete.page';
import { DetailPage } from './detail.page';
import { ListPage } from './list.page';
import { PreparePage } from './prepare.page';
import { UpdatePage } from './update.page';

export class SubnetPage extends AcpPageBase {
    /**
     * 准备测试数据
     */
    get preparePage() {
        return new PreparePage();
    }
    /**
     * 创建页
     */
    get createPage() {
        return new CreatePage();
    }

    /**
     * 列表页
     */
    get listPage() {
        return new ListPage();
    }

    get listPageVerify() {
        return new ListPageVerify();
    }

    /**
     * 删除页
     */
    get deletePage() {
        return new DeletePage();
    }

    /**
     * 详情页
     */
    get detailPage() {
        return new DetailPage();
    }

    get detailPageVerify() {
        return new DetailPageVerify();
    }

    /**
     * 更新页
     */
    get updatePage() {
        return new UpdatePage();
    }
}
