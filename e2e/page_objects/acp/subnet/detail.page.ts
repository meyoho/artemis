import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, $$, by, element, browser, ElementFinder } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';

import { DeletePage } from './delete.page';
import { ListPage } from './list.page';
import { UpdatePage } from './update.page';

export class DetailPage extends AcpPageBase {
  /**
   * 点击切换tab页
   * @param name tab 名称如已用 IP
   */
  clickTab(name: string) {
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(text(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }
  /**
   * IP列表
   */
  get IpTable() {
    return new AuiTableComponent(
      $('rc-subnet-ip-used aui-card'),
      'aui-table',
      '.aui-card__header',
    );
  }
  searchIp(region_name, name, ip, row = 1) {
    this.listPage.enterDetail(region_name, name);
    this.clickTab('已用 IP');
    this.IpTable.searchByResourceName(ip, row);
  }
  get detailElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-card__content rc-field-set-item',
      ' .field-set-item__label',
      $('rc-subnet-detail-info aui-card'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      default:
        return this.detailElement.getElementByText(
          text,
          '.field-set-item__value',
        );
    }
  }
  get listPage() {
    return new ListPage();
  }
  get updatePage() {
    return new UpdatePage();
  }
  /**
   * 详情页的操作按钮
   */
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header .aui-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  updateGateWay(region_name, name, testData, flag = 'update') {
    this.listPage.enterDetail(region_name, name);
    this.action.select('更新网关');
    this.updatePage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
      case 'close':
        this.auiDialog.clickClose();
        break;
      default:
        this.auiDialog.clickConfirm();
        break;
    }
  }
  async updateNamespace(region_name, name, testData, flag = 'update') {
    await this.listPage.enterDetail(region_name, name);
    await this.action.select('更新命名空间');
    this.updatePage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        await this.auiDialog.clickCancel();
        break;
      case 'close':
        await this.auiDialog.clickClose();
        break;
      default:
        await this.auiDialog.clickConfirm();
        break;
    }
  }
  updateWhitelist(region_name, name, testData, flag = 'update') {
    this.listPage.enterDetail(region_name, name);
    this.action.select('更新白名单');
    this.updatePage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
      case 'close':
        this.auiDialog.clickClose();
        break;
      default:
        this.auiDialog.clickConfirm();
        break;
    }
  }
  /**
   * 删除页
   */
  get deletePage() {
    return new DeletePage();
  }

  delete(region_name, name, flag = 'delete') {
    this.listPage.enterDetail(region_name, name);
    this.action.select('删除');
    switch (flag) {
      case 'cancel':
        this.deletePage.deleteDialog.clickCancel();
        break;
      default:
        this.deletePage.deleteDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  async waitStatus() {
    this.waitElementPresent(
      element(
        by.xpath(
          `//label[contains(text(),"已用 IP 数")]/ancestor::rc-field-set-item/div[@class="field-set-item__value" and contains(text(),"0")]`,
        ),
      ),
      `等待子网状态`,
      15000,
    );
    await browser.refresh();
    await this.loginPage.waitLoadingDisappear();
    await this.waitElementPresent($('rc-page-header .project-title'), '');
    return browser.sleep(1000);
  }
}
