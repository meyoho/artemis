import { $ } from 'protractor';

import { AuiConfirmDialog } from '../../../element_objects/alauda.aui_confirm_dialog';
import { AcpPageBase } from '../acp.page.base';

export class DeletePage extends AcpPageBase {
    /**
     *  删除对话框
     */
    get deleteDialog() {
        return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
    }
}
