import { ListPage } from './list.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';
import { browser } from 'protractor';

export class PreparePage extends ListPage {
  /**
   * 删除子网
   * @param region_name 集群名称
   * @param value 子网
   * @param key 关键字
   * @example delete('calico','test','名称')
   * @example delete('ovn','2.2.0.0/16','网段')
   */
  async delete(region_name, value, key = '网段') {
    await this.before(region_name);
    browser.sleep(1000);
    await this.SubnetTable.getColumeTextByName(key).then(text => {
      if (text.includes(value)) {
        this.SubnetTable.clickOperationButtonByRow(
          [value],
          '删除',
          'aui-table-row aui-icon',
        );
        this.deletePage.deleteDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
      }
    });
    return browser.sleep(1000);
  }
  waitStatus(subnet_name, region_name) {
    let isNotFound = CommonKubectl.execKubectlCommand(
      `kubectl get subnet -n ${subnet_name} -o json`,
      region_name,
    ).includes('status');
    let timeout = 0;
    while (!isNotFound) {
      CommonMethod.sleep(1000);
      isNotFound = CommonKubectl.execKubectlCommand(
        `kubectl get subnet -n ${subnet_name} -o json`,
        region_name,
      ).includes('status');
      if (timeout++ > 20) {
        console.log('获取子网状态超时');
        return;
      }
    }
  }
}
