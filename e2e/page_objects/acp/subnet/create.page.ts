import { AlaudaAuiTagsInput } from '@e2e/element_objects/alauda.aui_tags_input';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, $$, ElementFinder } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

export class CreatePage extends AcpPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form-item',
      '.aui-form-item__label',
      $('.cdk-overlay-pane aui-dialog  form'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '名称':
      case '网段':
      case '网关':
        return this.alaudaElement.getElementByText(text);
      case '保留 IP':
      case '白名单':
        return this.alaudaElement.getElementByText(text, 'aui-tags-input');
      case '网关类型':
        return super.getElementByText(text, 'aui-radio-group');
      case '命名空间':
        return this.alaudaElement.getElementByText(text, 'aui-multi-select');
      case '网关节点名称':
        return this.alaudaElement.getElementByText(text, 'aui-select');
      case '子网隔离':
      case '外出流量 NAT':
        return this.alaudaElement.getElementByText(text, 'aui-switch');
    }
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '名称':
      case '网段':
      case '网关':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '保留 IP':
      case '白名单':
        const taginput = new AlaudaAuiTagsInput(this.getElementByText(name));
        taginput.clearTags();
        value.forEach(ip => {
          taginput.input(ip);
        });
        break;
      case '网关类型':
        new AlaudaRadioButton(this.getElementByText(name)).clickByName(value);
        break;
      case '网关节点名称':
        new AlaudaDropdown(
          this.getElementByText(name),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        ).select(value);
        break;
      case '命名空间':
        new AuiMultiSelect(
          this.getElementByText(name),
          $('.cdk-overlay-pane aui-tooltip'),
        ).select(value);
        break;
      case '子网隔离':
      case '外出流量 NAT':
        if (value === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        } else {
          new AuiSwitch(this.getElementByText(name)).close();
        }
    }
  }
}
