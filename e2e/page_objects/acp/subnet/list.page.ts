import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, browser } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

import { CreatePage } from './create.page';
import { DeletePage } from './delete.page';
import { DetailPage } from './detail.page';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';

export class ListPage extends AcpPageBase {
  /**
   * 详情页
   */
  get detailPage() {
    return new DetailPage();
  }

  /**
   * 子网列表
   */
  get SubnetTable() {
    return new AuiTableComponent(
      $('.layout-page-content aui-card'),
      'aui-table',
      '.aui-card__header',
    );
  }
  async before(region_name) {
    await this.clickLeftNavByText('子网');
    await browser.sleep(1000);
    await this.switchCluster_onAdminView(region_name);
    await this.waitElementPresent(
      $('.aui-card__content aui-table'),
      '等待 10s后，列表页未显示',
      10000,
    );
    await this.waitElementNotPresent($('.aui-icon-spinner'), '', 20000);
    return browser.sleep(100);
  }
  /**
   * 搜索子网
   * @param name 名称
   */
  async search(region_name, name, row = 1) {
    await this.before(region_name);
    await new AuiSearch($('acl-page-guard aui-search')).search(name);
    console.log(row);
    return browser.sleep(1);
    // this.SubnetTable.searchByResourceName(name, row);
  }
  /**
   * 点击名称进入详情页
   * @param name
   */
  async enterDetail(region_name, name) {
    await this.search(region_name, name);
    await this.SubnetTable.clickResourceNameByRow([name], '名称');
    // 详情页，左上角子网名称控件
    const nameElem = $('rc-subnet-detail-info .aui-card__header');
    await this.waitElementPresent(nameElem, '详情页没有打开');
  }
  /**
   * 删除页
   */
  get deletePage() {
    return new DeletePage();
  }
  async deleteSubnet(region_name, name, row = 1, flag = 'delete') {
    await this.search(region_name, name, row);
    await this.SubnetTable.getRowCount().then(count => {
      if (count > 0) {
        this.SubnetTable.clickOperationButtonByRow(
          [name],
          '删除',
          'aui-table-row aui-icon',
        );
        switch (flag) {
          case 'cancel':
            this.deletePage.deleteDialog.clickCancel();
            break;
          default:
            this.deletePage.deleteDialog.clickConfirm();
            this.toast.getMessage(5000).then(message => {
              console.log(message);
            });
            break;
        }
      }
    });
    return browser.sleep(1);
  }

  get createPage() {
    return new CreatePage();
  }
  async createSubnet(region_name, testData, flag = 'add') {
    await this.before(region_name);
    await browser.sleep(1000).then(async () => {
      this.getButtonByText('创建子网').click();
      this.createPage.fillForm(testData);
      switch (flag) {
        case 'cancel':
          this.auiDialog.clickCancel();
          break;
        case 'close':
          this.auiDialog.clickClose();
          break;
        default:
          this.auiDialog.clickConfirm();
          // this.toast.getMessage(5000).then(message => {
          //   console.log(message);
          // });
          break;
      }
      await this.detailPage.waitStatus();
    });
  }
}
