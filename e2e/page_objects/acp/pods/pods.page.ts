import { AcpPageBase } from '../acp.page.base';
import { PodDetailPage } from './pods.detail.page';
import { PodDetailVerifyPage } from '@e2e/page_verify/acp/pods/pods.detail.verify.page';
import { PodListPage } from './pods.list.page';
import { PodListVerifyPage } from '@e2e/page_verify/acp/pods/pods.list.verify.page';
import { PodPreparePage } from './pods.prepare.page';

export class PodsPage extends AcpPageBase {
  get podPreparePage() {
    return new PodPreparePage();
  }
  get podDetailPage() {
    return new PodDetailPage();
  }
  get podDetailVerifyPage() {
    return new PodDetailVerifyPage();
  }
  get podListPage() {
    return new PodListPage();
  }
  get podListVerifyPage() {
    return new PodListVerifyPage();
  }
}
