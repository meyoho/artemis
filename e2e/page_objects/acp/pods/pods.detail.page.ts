import { AcpPageBase } from '../acp.page.base';
import { browser, element, by, $, $$, promise } from 'protractor';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { PodContainersPage } from './pods.detail.containers.page';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';

export class PodDetailPage extends AcpPageBase {
  private _root = new AlaudaElement(
    'rc-field-set-item',
    '.field-set-item__label',
    $('rc-pod-detail-info>aui-card:first-child'),
  );
  get podName() {
    return this.root.$('.aui-card__header').getText();
  }
  get log() {
    this.waitElementPresent($('rc-pods-log'), '等待日志元素加载成功');
    return $('rc-pods-log').getText();
  }
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-confirm-dialog'));
  }
  /**
   * 展示pod的label
   * @param header 卡片名
   * @param itemName 标签名
   * @param index 索引值
   */
  showLabels(label): promise.Promise<string> {
    const tagLabels = new AlaudaTagsLabel(
      element(
        by.xpath(
          (function() {
            if (label === '凭据') {
              return `//label[contains(text(),"${label}")]/parent::div/..//rc-tags-label`;
            } else if (label === '容器组标签') {
              return `//div[contains(text(),"${label}")]/parent::div/..//rc-tags-label`;
            }
          })(),
        ),
      ),
    );
    return tagLabels.getText();
  }
  get nodeSelector() {
    return element(
      by.xpath(
        `//div[contains(text(),"主机选择器")]/parent::div/..//div[@class="rc-pod-node-selector"]`,
      ),
    );
  }
  /**
   * 点击切换tab页
   * @param name tab 名称如YAML
   */
  clickTab(name: string) {
    browser.executeScript('window.scrollTo(0,0)');
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(string(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    return ele.click();
  }
  /**
   * 根据标题获取配置tab页card
   * @param text 标题
   * @param container_name 容器名称， 如果有多个容器必须传
   */
  getConfigCard(text, container_name = '') {
    return element(
      by.xpath(
        `//${(function() {
          if (container_name === '') {
            return '';
          } else {
            return `div[starts-with(@class,'rc-secondary-title') and normalize-space(text())='${container_name}']/following-sibling::`;
          }
        })()}aui-card//div[@class="aui-card__header" and contains(text(),"${text}")]/parent::div/parent::aui-card`,
      ),
    );
  }
  /**
   * 根据标题获取配置tab页配置信息列表
   * @param text 标题
   * @param container_name 容器名称， 如果有多个容器必须传
   */
  getConfigTable(text, container_name = '') {
    return new AuiTableComponent(
      this.getConfigCard(text, container_name),
      'aui-table',
      '',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  detail_info() {
    return this._root;
  }
  get root() {
    this.waitElementPresent(
      $('rc-pod-detail-info>aui-card:first-child'),
      'Pod详情页加载失败',
    );
    return $('rc-pod-detail-info>aui-card:first-child');
  }
  isNotInPage() {
    return this.waitElementNotPresent(
      $('rc-pod-detail-info>aui-card:first-child'),
      '等待页面跳转失败',
    );
  }
  status_info_table() {
    return new AuiTableComponent(
      $('rc-pod-status-list'),
      'aui-table',
      '',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  get containers() {
    return new PodContainersPage();
  }
  toWorkloadDetailPage() {
    return this.clickTab('详细信息').then(() => {
      this.detail_info()
        .getElementByText('来源', 'a')
        .click();
    });
  }
  get Yaml() {
    this.clickTab('YAML');
    return new AlaudaYamlEditor().getYamlValue();
  }
  getElementByText(text) {
    switch (text) {
      case '存储卷':
      case 'Pod 亲和':
        this.foldableBlock(text).expand();

        return new AuiTableComponent(
          this.root
            .$$('rc-foldable-block')
            .filter(fold => {
              return fold
                .$('.toggle-bar__text')
                .getText()
                .then(fold_text => {
                  return fold_text.trim().includes(text);
                });
            })
            .first()
            .$('.foldable-content'),
          'rc-array-form-table',
          '',
          'td',
          'tbody>tr',
          'th',
        );
      default:
        return this._root.getElementByText(text, '.field-set-item__value');
    }
  }
  foldableBlock(kind: string) {
    return new FoldableBlock(
      this.root.element(
        by.xpath(
          `//rc-foldable-block/div/div[normalize-space(text())="${kind}"]/../..`,
        ),
      ),
    );
  }
  clickOperation(operation: string, child_operation = null) {
    return new AlaudaDropdown(
      this.root.$('.aui-card__header button'),
      $$('aui-menu-item'),
    ).select(operation, child_operation);
  }
  deletePod(flag = '确定') {
    return this.clickOperation('删除').then(() => {
      const podname = this.podName;
      if (flag === '确定') {
        this.confirmDialog.clickConfirm();
        this.waitElementPresent(
          $('.aui-inline-alert__content'),
          '等待页面跳转到列表页',
        );
      } else if (flag === '取消') {
        this.confirmDialog.clickCancel();
      }
      return podname;
    });
  }
  switch_pod_in_log_tab(key, value = '') {
    return new AuiSelect(
      $('.log-source__toolbar>aui-select'),
      $('aui-tooltip'),
    ).select(key, value);
  }
}
