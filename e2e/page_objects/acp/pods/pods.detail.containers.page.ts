import { AcpPageBase } from '../acp.page.base';
import { $, element, by } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class PodContainersPage extends AcpPageBase {
  private _root = new AlaudaElement(
    'rc-field-set-item',
    '.field-set-item__label>label',
    $('rc-pod-controller-containers'),
  );
  get root() {
    this.waitElementPresent(
      $('rc-pod-controller-containers'),
      '等待容器列表加载成功失败',
    );
    return $('rc-pod-controller-containers');
  }
  clickTab(containerName) {
    const tabs = this.root
      .$$('div[role="tablist"] div[role="tab"]')
      .filter(ele => {
        return ele
          .$('.init_container_tag')
          .isPresent()
          .then(ispresent => {
            if (ispresent) {
              return ele.getText().then(text => {
                return ele
                  .$('.init_container_tag')
                  .getText()
                  .then(inittagtext => {
                    return (
                      text.replace(inittagtext, '').trim() === containerName
                    );
                  });
              });
            } else {
              return ele.getText().then(text => {
                return text.trim() === containerName;
              });
            }
          });
      });
    return tabs.count().then(ct => {
      if (ct === 0) {
        throw new Error(`容器名称（${containerName}）未在tab中找到`);
      } else {
        return new AlaudaButton(tabs.first()).click();
      }
    });
  }
  getElementByText(text) {
    switch (text) {
      case '已挂载存储卷':
      case '端口':
        this.foldableBlock(text).expand();

        return new AuiTableComponent(
          this.root
            .$$('rc-foldable-block')
            .filter(fold => {
              return fold
                .$('.toggle-bar__text')
                .getText()
                .then(fold_text => {
                  return fold_text.trim().includes(text);
                });
            })
            .first()
            .$('.foldable-content'),
          'rc-array-form-table',
          '',
          'td',
          'tbody>tr',
          'th',
        );
      //   case '健康检查':
      //     this.foldableBlock(text).expand();
      //     return new AuiTableComponent(
      //       $('rc-container-healthcheck-display'),
      //       'rc-array-form-table',
      //       '',
      //       'td',
      //       'tbody>tr',
      //       'th',
      //     );
      //   case '端口':
      //     this.foldableBlock(text).expand();
      //     return new AuiTableComponent(
      //       $('rc-container-ports-preview'),
      //       'rc-array-form-table',
      //       '',
      //       'td',
      //       'tbody>tr',
      //       'th',
      //     );
      case '存活性健康检查':
      case '可用性健康检查':
        this.foldableBlock('健康检查').expand();
        return new AlaudaElement(
          '.healthcheck-table-cell',
          '.healthcheck-table-cell-label',
          element(
            by.xpath(
              `//div[@class="healthcheck-table-cell-label" and normalize-space(text())="健康检查类型"]/../div[@class="healthcheck-table-cell-content" and normalize-space(text())="${text.slice(
                0,
                3,
              )}"]/../../..`,
            ),
          ),
        );
      case 'EXEC':
      case '日志':
        return this.root
          .$$('button')
          .filter(btn => {
            return btn.getText().then(btn_text => {
              return btn_text.trim() === text;
            });
          })
          .first();
      default:
        return this._root.getElementByText(text, '.field-set-item__value');
    }
  }
  foldableBlock(kind: string) {
    return new FoldableBlock(
      element(
        by.xpath(
          `//rc-foldable-block/div/div[contains(text(),"${kind}")]/../..`,
        ),
      ),
    );
  }
}
