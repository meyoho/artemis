import { AcpPageBase } from '../acp.page.base';
import { browser, $, element, by } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class PodListPage extends AcpPageBase {
  get alertInfo() {
    return $('.aui-inline-alert__content');
  }
  get podListTable() {
    browser.sleep(1000);
    return new AuiTableComponent(
      $('rc-pod-list .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-confirm-dialog'));
  }
  searchPod(key: string, rowcount) {
    return this.podListTable.searchByResourceName(key, rowcount);
  }
  searchPodByKey(key: string) {
    return this.podListTable.searchResource(key);
  }
  refreshPodList() {
    return new AlaudaButton($('rc-pod-list aui-icon[icon="basic:refresh"]'))
      .click()
      .then(() => {
        this.waitElementNotPresent(
          element(
            by.xpath(
              "//acl-k8s-resource-list-footer/div/span[contains(text(),'加载中')]",
            ),
          ),
          '等待列表刷新完成失败',
        );
      });
  }
  waitPodNotExist(podName, time_out = 30) {
    this.refreshPodList().then(() => {
      browser.sleep(2000);
    });
    browser.sleep(1).then(() => {
      for (let i = 0; i < time_out; i++) {
        element(by.xpath(`//aui-table-cell/a[contains(text(),'${podName}')]`))
          .isPresent()
          .then(ispresent => {
            if (ispresent === true) {
              browser.sleep(2000);
              this.refreshPodList().then(() => {
                browser.sleep(1000);
              });
            }
          });
      }
    });
  }
  deletePod(pod_line: number, flag = '确定', isstatefulset = false) {
    return this.podListTable
      .getRowByIndex(pod_line)
      .$('aui-table-cell:first-child')
      .getText()
      .then(podName => {
        this.podListTable.clickOperationButtonByRow(
          [podName],
          '删除',
          'button aui-icon[icon="basic:ellipsis_v_s"]',
          '.aui-menu>div>*',
        );
        if (flag === '确定') {
          this.confirmDialog.clickConfirm();
          if (!isstatefulset) {
            this.waitPodNotExist(podName);
          }
        } else if (flag === '取消') {
          this.confirmDialog.clickCancel();
        }
        return podName;
      });
  }
  getDeleteConfirmInfo() {
    return this.confirmDialog.content.getText();
  }
  toDetail(name) {
    return this.podListTable.clickResourceNameByRow([name]).then(() => {
      this.waitElementPresent(
        $('rc-pod-detail-info>aui-card:first-child'),
        '等待页面跳转到详情页失败',
      );
    });
  }
}
