import { AcpPageBase } from '../acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { PreparePage as TappPreparePage } from '../tapp/prepare';
import { AppPrepareData } from '../appcore/prepare.page';
import { browser } from 'protractor';
import { CommonMethod } from '@e2e/utility/common.method';

export class PodPreparePage extends AcpPageBase {
  private tapp_client = new TappPreparePage();
  private app_client = new AppPrepareData();
  createPod(data, cluster = this.clusterName) {
    return CommonKubectl.createResourceByTemplate(
      'alauda.pod.tpl.yaml',
      data,
      `pod-tpl-${this.getRandomNumber()}`,
      cluster,
    );
  }
  deletePod(name, namespace = this.namespace1Name, cluster = this.clusterName) {
    return CommonKubectl.execKubectlCommand(
      `kubectl delete pod -n ${namespace} ${name}`,
      cluster,
    );
  }
  createJob(data, cluster = this.clusterName) {
    return CommonKubectl.createResourceByTemplate(
      'alauda.job.tpl.yaml',
      data,
      `job-tpl-${this.getRandomNumber()}`,
      cluster,
    );
  }
  deleteJob(name, namespace = this.namespace1Name, cluster = this.clusterName) {
    return CommonKubectl.execKubectlCommand(
      `kubectl delete jobs -n ${namespace} ${name}`,
      cluster,
    );
  }
  createTapp(data, cluster = this.clusterName) {
    this.tapp_client.createTapp(data, cluster);
  }
  deleteTapp(
    name,
    namespace = this.namespace1Name,
    cluster = this.clusterName,
  ) {
    this.tapp_client.deleteTapp(name, namespace, cluster);
  }
  createApp(data, cluster = this.clusterName) {
    this.app_client.createAppByTemplate(
      'alauda.application.tpl.yaml',
      data,
      cluster,
    );
  }
  deleteApp(data, cluster = this.clusterName) {
    this.app_client.deleteAppByTemplate(
      'alauda.application.tpl.yaml',
      data,
      cluster,
    );
  }
  waitPodRunning(
    podName,
    namespace = this.namespace1Name,
    cluster_name = this.clusterName,
  ) {
    return browser.sleep(1000).then(() => {
      let times = 0;
      while (times < 20) {
        const ret = CommonKubectl.execKubectlCommand(
          `kubectl get pod -n ${namespace} ${podName}`,
          cluster_name,
        ).includes('Running');
        if (ret) {
          return ret;
        } else {
          CommonMethod.sleep(3000);
          times += 1;
        }
      }
      return false;
    });
  }
}
