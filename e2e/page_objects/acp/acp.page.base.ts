/**
 * Created by shijingnan on 2019/6/13.
 */
import { AlaudaTabItem } from '@ele/alauda.tabitem';
import { $, ElementFinder, browser, by, promise, element } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AuiConfirmDialog } from '../../element_objects/alauda.aui_confirm_dialog';
import { AuiDialog } from '../../element_objects/alauda.aui_dialog';
import { AlaudaAuiTable } from '../../element_objects/alauda.aui_table';
import { AlaudaAuiBreadCrumb } from '../../element_objects/alauda.auiBreadcrumb';
import { AlaudaButton } from '../../element_objects/alauda.button';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { PageBase } from '../page.base';
import { LoginPage } from '../login.page';

export class AcpPageBase extends PageBase {
  constructor() {
    super();
    browser.baseUrl = `${ServerConf.BASE_URL}/console-acp/`;
  }
  getRandomNumber() {
    return Math.floor(Math.random() * 1000 + 1);
  }
  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    const branchName = ServerConf.BRANCH_NAME.substring(0, 10)
      .replace('_', '-')
      .replace('/', '')
      .toLowerCase();
    return `acp-${branchName}-${prefix}`;
  }

  async switchSetting(name) {
    await super.switchSetting(name);
    await this.loginPage.waitLoadingDisappear();
  }

  get clusterName() {
    return ServerConf.REGIONNAME;
  }
  get ovnClusterName() {
    return ServerConf.OVNREGIONNAME;
  }
  get calicoClusterName() {
    return ServerConf.CALICOREGIONNAME;
  }
  get projectName(): string {
    return 'uiauto-acp';
  }

  get namespace1Name(): string {
    return 'uiauto-acp-ns1';
  }
  get namespace2Name(): string {
    return 'uiauto-acp-ns2';
  }
  get ovnNamespaceName(): string {
    return 'uiauto-acp-ns4';
  }
  get calicoNamespaceName(): string {
    return 'uiauto-acp-ns3';
  }
  get project2Name(): string {
    return 'uiauto-acp2';
  }

  get project2ns1(): string {
    return 'uiauto-acp2-ns1';
  }
  get project2ns2(): string {
    return 'uiauto-acp2-ns2';
  }
  /**
   * 命名空间绑定子网
   * @param namespace 命名空间名称
   * @param subnet 子网名称
   */
  bindingOvnSubnetCidr(
    namespace = this.ovnNamespaceName,
    subnet = 'ovn-default',
  ) {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl -X PATCH ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}' -H 'content-type:application/merge-patch+json' --data '{"spec":{"namespaces":["${namespace}"]}}' -k ${
      ServerConf.BASE_URL
    }/kubernetes/${this.ovnClusterName}/apis/kubeovn.io/v1/subnets/${subnet}`;
    console.log(cmd);
    const rsp: string = CommonMethod.execCommand(cmd);
    console.log(rsp);
  }
  /**
   * 获取命名空间绑定的子网
   * @param namespace 命名空间名称
   * @param cluster 集群名称
   */
  getOnvSubnetCidr(
    namespace = this.ovnNamespaceName,
    cluster = this.ovnClusterName,
  ) {
    const annos = JSON.parse(
      CommonKubectl.execKubectlCommand(
        `kubectl get ns ${namespace} -o json`,
        cluster,
      ),
    )['metadata']['annotations'];
    console.log(annos);
    return [
      annos['ovn.kubernetes.io/cidr'],
      annos['ovn.kubernetes.io/logical_switch'],
    ];
  }
  /**
   * 获取命名空间绑定的子网
   * @param namespace 命名空间名称
   * @param cluster 集群名称
   */
  getCalicoSubnetCidr(
    namespace = this.ovnNamespaceName,
    cluster = this.ovnClusterName,
  ) {
    const annos = JSON.parse(
      CommonKubectl.execKubectlCommand(
        `kubectl get ns ${namespace} -o json`,
        cluster,
      ),
    )['metadata']['annotations'];
    console.log(annos);
    return [
      annos['ovn.kubernetes.io/cidr'],
      annos['ovn.kubernetes.io/logical_switch'],
    ];
  }
  /**
   * 随机生成一个ip
   * @param cidr
   */
  randomCreateIP(cidr: string) {
    const ip2int = require('ip-to-int');
    console.log(cidr);
    const int_ip_s = ip2int(cidr.split('/')[0]).toInt();
    const ip =
      int_ip_s +
      Math.floor(
        Math.random() *
          Math.pow(
            2,
            32 - Number.parseInt(cidr.split('/')[cidr.split('/').length - 1]),
          ) +
          1,
      );
    return ip2int(`${ip}`).toIP();
  }
  /**
   * 判断ip地址是否已经被使用
   * @param ipaddr IP地址
   * @param subnet 子网名称
   * @param cluster 集群
   */
  ipIsUsed(ipaddr, subnet, cluster = this.ovnClusterName) {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}' -k ${
      ServerConf.BASE_URL
    }/acp/v1/resources/search/kubernetes/${cluster}/apis/kubeovn.io/v1/ips?keyword=${ipaddr}\\&field=spec.ipAddress\\&labelSelector=ovn.kubernetes.io/subnet=${subnet}`;
    console.log(cmd);
    const rsp: string = CommonMethod.execCommand(cmd);
    console.log(rsp);
    const items = JSON.parse(rsp)['items'];
    return (
      items.filter(item => {
        return item['spec']['ipAddress'] === ipaddr;
      }).length === 0
    );
  }
  /**
   * 从console-acp进入项目管理页面
   */
  enterManageProjectFromAcp() {
    this.switchSetting('项目管理');
  }
  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }
  /**
   * progressbar 出现后，等待消失
   * @example waitProgressBarNotPresent()
   */
  waitProgressBarNotPresent(timeout = 20000): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(
          this.progressbar,
          'progressbar 没消失',
          timeout,
        );
      }
    });
  }
  /**
   * 获取面包屑
   * @param timeout
   */
  get breadcrumb(): AlaudaAuiBreadCrumb {
    return new AlaudaAuiBreadCrumb(
      $('aui-breadcrumb'),
      'aui-breadcrumb-item span',
    );
  }
  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  /**
   * 进入管理视图页面
   */
  async enterOperationView() {
    // browser.sleep(1000);
    // 管理视图页面，切换视图的按钮

    return await this.switchAdminView();
  }

  /**
   * 页面集群导航条
   * @param region_name 集群名称
   */
  regionMenu(region_name): AlaudaTabItem {
    return new AlaudaTabItem(
      by.xpath('//aui-tag'),
      by.xpath("//aui-tag/div[contains(@class,'aui-tag--primary')]"),
      by.xpath('//aui-tag/div/span[contains(text(),"' + region_name + '")]'),
    );
  }

  /**
   * 页面命名空间导航条
   */
  namespaceMenu(): AlaudaAuiTable {
    return new AlaudaAuiTable($('aui-card aui-table'));
  }
  /**
   * 进入用户视图页面
   * @param namespace_name 命名空间
   */
  async enterUserView(
    namespace_name = null,
    projectName = this.projectName,
    region_name = this.clusterName,
  ) {
    if (namespace_name) {
      const url = `${ServerConf.BASE_URL}/console-acp/workspace;project=${projectName};cluster=${region_name};namespace=${namespace_name}/app/list`;
      const loginPage = new LoginPage();
      loginPage.waitEnterUserView(url, '/app/list');

      await this.waitElementPresent(
        $(`acl-namespace-select a[title~=${namespace_name}]`),
        '切换用户视图失败',
        30000,
      ).then(isPresent => {
        if (!isPresent) {
          throw new Error('切换用户视图失败, 测试无法继续');
        }
      });
    } else {
      // 切换到用户视图
      this.switchUserView();
      // 选择一个项目
      this.switchProject(projectName);
      // 单击一个集群名称
      if (this.clusterDisplayName(region_name) === undefined) {
        this.regionMenu(region_name).click();
      } else {
        this.regionMenu(this.clusterDisplayName(region_name)).click();
      }
    }
    browser.sleep(100);
  }
  /**
   * 用户视图返回项目列表
   */
  backtoproject() {
    $('.aui-page__header .project-icon').click();
  }
  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(
      $('.resource-form__footer button[aui-button="primary"]'),
    );
  }
  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton($('.resource-form__footer button[aui-button=""]'));
  }

  /**
   * 单击确定按钮
   */
  clickConfirm(): promise.Promise<void> {
    this.buttonConfirm.click();
    // this.waitProgressBarNotPresent();
    return browser.sleep(100);
  }

  /**
   * 单击取消按钮
   */
  clickCancel(): promise.Promise<void> {
    this.buttonCancel.click();
    return browser.sleep(100);
  }
  /**
   * 创建service
   * @param serviceName
   * @param namespaceName
   */
  createService(serviceName: string, namespaceName: string): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.service.yaml',
      {
        service_name: serviceName,
        ns_name: namespaceName,
      },
      'qa-service' + String(new Date().getMilliseconds()),
      ServerConf.REGIONNAME,
    );
  }
  /**
   * 创建secret
   * @param secretName
   * @param namespaceName
   */
  createSecret(secretName: string, namespaceName: string): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      {
        name: secretName,
        namespace: namespaceName,
        datas: {
          'tls.crt': `-----BEGIN CERTIFICATE-----
MIIFxzCCA6+gAwIBAgIJAJWuu0/x8yZ3MA0GCSqGSIb3DQEBCwUAMHoxCzAJBgNV
BAYTAmJqMQswCQYDVQQIDAJiajELMAkGA1UEBwwCYmoxDzANBgNVBAoMBmFsYXVk
YTEPMA0GA1UECwwGYWxhdWRhMQ8wDQYDVQQDDAZhbGF1ZGExHjAcBgkqhkiG9w0B
CQEWD2puc2hpQGFsYXVkYS5pbzAeFw0xOTA2MjgxMTA5MzRaFw0yMDA2MjcxMTA5
MzRaMHoxCzAJBgNVBAYTAmJqMQswCQYDVQQIDAJiajELMAkGA1UEBwwCYmoxDzAN
BgNVBAoMBmFsYXVkYTEPMA0GA1UECwwGYWxhdWRhMQ8wDQYDVQQDDAZhbGF1ZGEx
HjAcBgkqhkiG9w0BCQEWD2puc2hpQGFsYXVkYS5pbzCCAiIwDQYJKoZIhvcNAQEB
BQADggIPADCCAgoCggIBALCQV0y095q5nfN9qckWPFJ08nMaV1v1eveGon26nPfB
vI/f+Z3pMax/Y/8+TIcYb1cvCoOdBL408IsFEK26Io7u+ahLsyp7lm0nQc+erJ59
lUTkFf6Vkbf/90e6AIb4F2efb3mGAuT9W382vPMdgqjcUo96PX3SaBHukOuvDZE5
L2BWP+6fL02clCj3aVJubHpG5T7D5ktm46K/TPFWvgcBlI0zAN70EDhAH5jIIXkr
Tf05254YcBUX1O9VQx0mDQ2XqIiDuA04t9MniYhvyHwuha5j9DkujAR4J1dknPr4
PUGYC9ZWy8zxQrT9SGZeDS2+z0FS9ccIUh6R81vBJ6E484eZ+LcR85Bh8nvoph/4
AkaKVg+cOXFH6n4P6vw0lD58cttLAEVOz2R+aCoEEF/x7fNbX1bwWgnsXxH6d/jJ
SnvxIC6G1nun5WEUrvmBJTwAxD2dBumBql86jQ8zM3POwbbUSangvwpy0PEONDUx
WpkD6TZuPHE5FTQkeIlcf3DcMvZ/BDEm1QOD0U2Vzoe/hp5S4GrbUzcoi+HSKDer
SlqEWptN6nYSV9n9unaJ0o41q2QJeEPVaWObtjtFPISJW/VcE1shRMtJeCsCRAqA
Vi1PU2WqYLVgI1qLpZv252kG1i2I7BNtPWWTPc0IaMDDivJlLrVtXwE/au97BegV
AgMBAAGjUDBOMB0GA1UdDgQWBBQXdBBvekj678mwvtpYt0bI/+1U4DAfBgNVHSME
GDAWgBQXdBBvekj678mwvtpYt0bI/+1U4DAMBgNVHRMEBTADAQH/MA0GCSqGSIb3
DQEBCwUAA4ICAQBDWQvcxgRJWWk2dYuVVY5PlP17Ad55huXe5e5ghuGDDzXqVHJG
k8kfG03shXx6cirE8cVjWIEWyBWEc+PGfLrImqW/ghnL7bIs0593naG2cbs3alyJ
J+k9wJMWMupf2D+d1H8GHlPy9ab0ynKqQTGbZOfj0qnliYmsfULQX66WKvFT3Fae
ZqK0fPGr6iuZC6OQxal9GszXOAiiBL/oZ0+bqZd7S0tbbdT1Ots2M3xa5yet+GPb
/I3s15IkJyc5dXoHXdABfYUmmWI/u9lT9uoe5bl88ML1FLw75992Be6rMr1v017I
gcrKW33TzaemSWqXCODvEUczh9fvOH1/sV5ZjgMmVOcHJ0iqMLunVyM71A/b/FPC
A7Pst/jjJLN31d2qLy1xd5O15d9qYUprWzQdewmrdjJWvVMVkocd3ruIqElvYIWh
HcfQ93JCxQaHogyA+6GgRXwK4GvK7LqJpGLEDWM9xXCAAsz/5awXKuJWTb7eDSui
utx0aPqNNFMYnnxMcCJXE6FF5+/Xo2BYe4CFf0FPBRihIsHAkQxlhdlYhDBdXOzb
054+DDKOIxMkRj6NHSxsZAksoiOOPMvCOOGURn7tGKnxn1UJT2pJuNX39/eKKDOz
PHFesLYw4gbmIfKoGcEUIdbPGS4HHATgI4TsMvoEAuxu6mIDGBPDc5O9VQ==
-----END CERTIFICATE-----`,
          'tls.key': `-----BEGIN PRIVATE KEY-----
MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQCwkFdMtPeauZ3z
fanJFjxSdPJzGldb9Xr3hqJ9upz3wbyP3/md6TGsf2P/PkyHGG9XLwqDnQS+NPCL
BRCtuiKO7vmoS7Mqe5ZtJ0HPnqyefZVE5BX+lZG3//dHugCG+Bdnn295hgLk/Vt/
NrzzHYKo3FKPej190mgR7pDrrw2ROS9gVj/uny9NnJQo92lSbmx6RuU+w+ZLZuOi
v0zxVr4HAZSNMwDe9BA4QB+YyCF5K039OdueGHAVF9TvVUMdJg0Nl6iIg7gNOLfT
J4mIb8h8LoWuY/Q5LowEeCdXZJz6+D1BmAvWVsvM8UK0/UhmXg0tvs9BUvXHCFIe
kfNbwSehOPOHmfi3EfOQYfJ76KYf+AJGilYPnDlxR+p+D+r8NJQ+fHLbSwBFTs9k
fmgqBBBf8e3zW19W8FoJ7F8R+nf4yUp78SAuhtZ7p+VhFK75gSU8AMQ9nQbpgapf
Oo0PMzNzzsG21Emp4L8KctDxDjQ1MVqZA+k2bjxxORU0JHiJXH9w3DL2fwQxJtUD
g9FNlc6Hv4aeUuBq21M3KIvh0ig3q0pahFqbTep2ElfZ/bp2idKONatkCXhD1Wlj
m7Y7RTyEiVv1XBNbIUTLSXgrAkQKgFYtT1NlqmC1YCNai6Wb9udpBtYtiOwTbT1l
kz3NCGjAw4ryZS61bV8BP2rvewXoFQIDAQABAoICAQCfR0cJ0sL/WG1OB9HPrnhy
Zj+FaIDgWz4358PUKjGKK8KgBAwZ0RFXrIAX9SpyQK/IdY5FlrN9pyJMNyCJqD2e
taSQluGk9GKKS3zKADl2rJGo2R2iUWloFMgNc5UgLNRoTWLxvNwFmS0eQNIJe1iw
Xjk4Z6zrQZFSg+VGR9+0X2MGxCfeasnweM0Le84OZ8ECOcx6Z5zr7oByA6M0n+zD
Q0CwdA9YEdJD/EzEtd3EgjVTuAsPvrn1vU8BLOhjdj0WQvbtV+4EN2NVX/hpFXZF
2G4OGOtrXjs/mOjcpM4cdS5FaxVaoWgulIJ55XeXqUa4OsabBmzjxPTX0RA5eLD2
91HPrOXkadjG4uccJG7xwGYs4wmXZ0K47xfyEX75FGzfuiboKx6295udwXe/XW+z
7Ws3UQSeLJ2fsuomXwVkTBF7dXoDe7XvyhFYsFUWFlWFW6ZRy+B1tMUjl304PhKu
mhp8exW3TLcxeQI3sDcvQJoBkE8wjaGvgOja3VteBUunBHOuKyKiFfvTZ4z3T93q
ia90M0YyFDLNVls0flN308Ng/CFCQdwayHFkM5iwJ8Ssr5wshf9l2F1hE5kUcEeI
BnCx0kjCVMwmgvVN79fdJJlNlLzPehdSPtVZYc6Ywfj1mDSA4XgZxwii4MV0Lu9a
pHN5YLsoXKgiROCGe6upTQKCAQEA2MkbUbTmdc+N44ZCbimHkobbx6qakOiTuJKc
lyR1as1vb1RUxERw6hAWXl5ejb2Rj8qjNGS4I0jmPFZ4aKX+lwPqA/H881et2+O+
0/h4DDh+xTvgQx8JHo/H3qlqMpbb2DGEqyLb/rj6Ag4q+1Dxd/Uopy3+Fkh8m1YX
2tqbc6bA9JFQLsM+ueBRrWsrsf4TXlR5YJMlQOPNCIgkeROdPw0yyCu6UnMyui0e
10Yo6cN9KfYlOKMcTF68CnpwddybU10sAseDbyAF/cPSfoy3NLJ35971pxIAkRnK
1xiD0U48nQXoRvvKpdcVjObZxi6pJut6Q8OobNHbNDsXtXdthwKCAQEA0ICl+Tdn
SR+EKP+d37YM9hcshd9/sUv9KojhJkGtLVoSEoz/fe95c4pspLKLPvix9uBYSZpP
MOuEouRzcTufLhJ0eA7Ku6sPSQztgcy2hTQMdJMTTR9IL/Yx9mjmeOb7WliEVtv6
d9WeQ3F9h5oVcwbRGYVCSmPZYJUP1VFpE5KuVjyPi+6Zt2mO4h5yuHx84r5xyQwX
+ars120/YO9564jLZvCN0niiB0RQmXQiqyuuqQVvGHzNv1GIN6xiJmEpO0rM4vgo
BW0/IaAV7g0ilG5QNrn7fp740RDKrARAgVq8O48ZCMtY8mnBYpUXe486bB72EhiP
2rVc0uNFp+dEgwKCAQBMtx5L09CacFCt650mjtvDJqYwCleYYDWmY3u2oNhMa1a7
9wPoULAEpJzF2G9tiWAxmmwW5m2jTvDq+U6y+csWdfQiYUg6dRS5c+bb7FnPOU7j
TUUXe6wmfIsmaJ2EOY6i+yMPzM49XghbIdhkB0jsQn2Ya9Y23H/8GINol2PbkW9H
HQVbBwzzikZdMEfOPTNtdkddvbS5OeQc2NDPZ/mFjo4h+Bd/hfS1nbIETHe+AcgR
SWHyoQYqoHu62YlirCbrYVVpxmDkulhkbR/YWMOcimuUSnp0HRLGRnJZm/tJ3n3k
s2p3dDNKbMrbifYXA/woIbyqrlqz56/C1AaX2SORAoIBAGMuf0VQpJI1KsHx7mCH
khzSIzy/d1eYZSKBoFB5d26WZPhsF/EJJIXguBOUVwaqeMWOLJE+lSMZNavi7kNT
8EGTR9IAf1HMamX+f1B6V+x/ONmPSOQr2EPwYg3sEo46miB88N78n2W3uSQt7V++
3Sstd/Qi3sjSPIpj/fckVc6YvNOK1WHkKz0DOpO3lizKEwItQ6U2WoVKWIWVk0uC
P0XaeZ9LGLug5Lp+NbT5eLBiBCCk1Bca7yhuGVUabEoDeACbcWEaWLdUI27UXvox
R1zCKOgPimHOL2AME8zF7QVbayUNygPT70jJI9PIRjZhiJH8HGO5SVzpXvF2aSEY
sw8CggEBAJwmfWFFtWe2NR0rZGwALi//B+HOcHq+cjXQqTZzAanGnDgo9f7X7j6p
YrtJykojBxVvc67SdwqjKTJkM5ly8yN+yXMA1+dE128GdGSFh46F17BzQ142exyI
nHJlfNCbPXccBlOag4mrguLPGJI290mbc5xqhayYGr415krJcJwgUOT03gQWqqUN
kEAhY/FG7HgHvIZEZz3/wtwbbmHK3imxLyCTd/315HfwBswuR6PG3MDKsDNvbrcz
X02rx0jjQ3JXp8HJAJRkj2DZECVLasl8VgQbfalAf3TAHb02e4TtlgsdQM13IAfJ
8gUQOo/CXbpNRtCBalsed6jvC0whNuo=
-----END PRIVATE KEY-----`,
        },
      },
      'qa-secret' + String(new Date().getMilliseconds()),
      ServerConf.REGIONNAME,
    );
  }
  deleteSecret(name: string, ns_name: string = this.namespace1Name) {
    CommonKubectl.deleteResource('secret', name, this.clusterName, ns_name);
  }
  /**
   * 创建domain
   * @param domainName 域名名称
   * @param kind 域名类型 full extensive
   * @param clusterName 分配集群
   * @param projectName 分配项目
   */
  createDomain(
    domainName: string,
    kind = 'extensive',
    clusterName: string,
    projectName: string,
  ): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.domain.template.yaml',
      {
        domain_name: domainName,
        cluster_name: clusterName,
        project_name: projectName,
        kind: kind,
      },
      'qa-domain' + String(new Date().getMilliseconds()),
    );
  }
  waitDomainInProject(
    domain,
    domain_kind,
    project = this.projectName,
    cluster = this.clusterName,
  ) {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}' -k ${
      ServerConf.BASE_URL
    }/apis/crd.alauda.io/v2/domains?labelSelector=project.alauda.io/name%20in%20%28${project}%2CALL_ALL%29,cluster.alauda.io/name%20in%20%28${cluster}%2CALL_ALL%29`;

    for (let i = 0; i < 30; i++) {
      console.log(cmd);
      const rsp: string = CommonMethod.execCommand(cmd);
      console.log(rsp);
      const items = JSON.parse(rsp)['items'];
      if (
        items.filter(item => {
          if (domain_kind === 'full') {
            return item['spec']['name'] === domain;
          } else {
            return item['spec']['name'] === `*.${domain}`;
          }
        }).length === 0
      ) {
        CommonMethod.sleep(2000);
      } else {
        return true;
      }
    }
    return false;
  }
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
  }

  /**
   * 删除应用
   * @param appName 应用名称
   * @param namespace 命名空间
   * @param tempFile 临时文件
   */
  deleteApp(appName: string, namespace: string, tempFile: string) {
    const command = `kubectl get pod ${appName} -n ${namespace}`;
    let isDeleted = CommonKubectl.execKubectlCommand(
      command,
      ServerConf.REGIONNAME,
    ).includes('NotFound');
    let timeout = 0;
    while (!isDeleted) {
      console.log(
        CommonKubectl.deleteResourceByYmal(tempFile, ServerConf.REGIONNAME),
      );
      CommonMethod.sleep(1000);
      isDeleted = CommonKubectl.execKubectlCommand(
        command,
        ServerConf.REGIONNAME,
      ).includes('NotFound');

      if (timeout++ > 120) {
        throw new Error(`删除应用${appName}超时`);
      }
    }
  }
  refreshList() {
    return new AlaudaButton(
      $('.aui-card__header aui-icon[icon="basic:refresh"]'),
    )
      .click()
      .then(() => {
        this.waitElementNotPresent(
          element(
            by.xpath(
              "//acl-k8s-resource-list-footer/div/span[contains(text(),'加载中')]",
            ),
          ),
          '等待列表刷新完成失败',
        );
      });
  }
  conflictRetry() {
    this.confirmDialog.isPresent().then(ispresent => {
      if (ispresent) {
        this.confirmDialog.title.getText().then(text => {
          console.log(text);
        });
        this.confirmDialog.clickConfirm();
      }
    });
  }
}
