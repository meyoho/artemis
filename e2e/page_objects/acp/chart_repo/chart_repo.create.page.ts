import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
// import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { $, promise, $$ } from 'protractor';

import { ListPage } from './chart_repo.list.page';
// import { ProjectSelect } from './project_select.element';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';

export class CreatePage extends AcpPageBase {
  private get list_page() {
    return new ListPage();
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'label',
      $('aui-dialog>.aui-dialog'),
    );
  }
  assert_error(assert: boolean, message) {
    if (!assert) {
      throw new Error(message);
    }
  }
  get _auiDialog() {
    return new AuiDialog(
      $('aui-dialog>.aui-dialog'),
      'aui-dialog-content',
      '.aui-button--danger',
      '.aui-button--default',
      '.aui-dialog__header-title',
    );
  }
  get title(): promise.Promise<string> {
    this.waitElementPresent(
      this.alaudaElement.root.$('aui-dialog-header'),
      '创建模板仓库标题未出现',
    );
    return this.alaudaElement.root.$('aui-dialog-header').getText();
  }
  analysisRepo(repo_uri) {
    const user_password_repr = /http[s]?:\/\/[^\s]{0,}:[^\s]{0,}@[^\s]{1,}/;
    let user = '';
    let password = '';
    let uri = repo_uri;
    if (user_password_repr.test(repo_uri)) {
      const user_password = repo_uri
        .split(/(?:http[s]?:\/\/|@[^\s]{1,})/g)
        .join('')
        .split(/:/g);
      user = user_password[0];
      password = user_password[1];
      uri = repo_uri.split(/:\/\/[^\s]{0,}:[^\s]{0,}@/g).join('://');
    }
    return { user: user, password: password, uri: uri };
  }
  getElementByText(text: string): any {
    switch (text) {
      case '分配项目':
        return new AlaudaDropdown(
          this.alaudaElement.getElementByText(text, 'aui-select'),
          $$('aui-tooltip aui-option'),
        );
      // return new ProjectSelect(
      //     this.alaudaElement.getElementByText(text, 'aui-select'),
      //     $('aui-tooltip'),
      //     'aui-option'
      // );
      case '类型':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
          '.aui-radio-button__content',
        );
      default:
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'input'),
        );
    }
  }
  enterValue(name: string, value: any) {
    switch (name) {
      case '分配项目':
        this.getElementByText(name).input(value);
        break;
      case '类型':
        this.getElementByText(name).clickByName(value);
        break;
      default:
        this.getElementByText(name).input(value);
        break;
    }
  }
  create(data) {
    this.list_page.getButtonByText('添加模板仓库').click();
    this.fillForm(data);
    this.auiDialog.clickConfirm();
    this.waitElementNotPresent(
      $('aui-dialog .aui-dialog'),
      '添加模板仓库失败',
      5000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '添加模板仓库失败');
    });
  }
  create_cancle(data) {
    this.list_page.getButtonByText('添加模板仓库').click();
    this.fillForm(data);
    this.auiDialog.clickCancel();
    this.waitElementNotPresent(
      $('aui-dialog .aui-dialog'),
      '添加模板仓库失败',
      3000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '添加模板仓库失败');
    });
  }
}
