// import { AuiToolTip } from '@e2e/element_objects/alauda.auiToolTip';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { $, promise, browser } from 'protractor';

import { CreatePage } from './chart_repo.create.page';
import { ListPage } from './chart_repo.list.page';

export class DetailPage extends AcpPageBase {
  private get list_page() {
    return new ListPage();
  }
  private get create_page() {
    return new CreatePage();
  }
  get detail_title(): promise.Promise<string> {
    this.waitElementPresent(
      $('aui-card:nth-child(1) .aui-card__headers'),
      '模板仓库详情页标题没有出现',
    );
    return $('aui-card:nth-child(1) .aui-card__headers').getText();
  }
  get detail_card(): AlaudaElement {
    return new AlaudaElement(
      'rc-field-set-item',
      'label',
      $('aui-card:nth-child(1)'),
    );
  }
  get repos_card() {
    return new AlaudaElement();
  }
  get repos_title() {
    this.waitElementPresent(
      $('aui-card:nth-child(2) .aui-card__headers'),
      '模板仓库应用模板标题没有出现',
    );
    return $('aui-card:nth-child(2) .aui-card__headers').getText();
  }
  get sysc_status() {
    this.waitElementPresent(
      $('rc-chartrepo-status span'),
      '同步状态页面元素没有出现',
    );
    return $('rc-chartrepo-status span').getText();
  }
  get _auiDialog() {
    return new AuiDialog(
      $('aui-dialog>.aui-dialog'),
      'aui-dialog-content',
      '.aui-button--danger',
      '.aui-button--default',
      '.aui-dialog__header-title',
    );
  }
  clickOperation(operation: string) {
    this.getButtonByText('操作').click();
    this.getButtonByText(operation).click();
    // const tooltip = new AuiToolTip($('aui-tooltip'));
    // tooltip.selectAui_option(operation);
  }
  assert_error(assert: boolean, message) {
    if (!assert) {
      throw new Error(message);
    }
  }
  waitSyncEnd(timeout = 30) {
    let present = true;
    $('rc-chartrepo-status')
      .isPresent()
      .then(ispresent => {
        present = ispresent;
        if (ispresent) {
          let s = 0;
          while (s < timeout && present) {
            s += 1;
            this.getButtonByText('刷新')
              .button.isPresent()
              .then(button_ispresent => {
                if (button_ispresent) {
                  this.getButtonByText('刷新').click();
                  browser.sleep(2000);
                } else {
                  present = false;
                }
              });
          }
        }
      });
  }
  update(data: any, name: string) {
    this.list_page.chartRepoTable.clickResourceNameByRow([name]);
    this.waitSyncEnd();
    this.clickOperation('更新');
    this.create_page.fillForm(data);
    this.auiDialog.clickConfirm();
    this.waitElementNotPresent(
      $('aui-dialog>.aui-dialog'),
      '更新模板仓库失败',
      5000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '更新模板仓库失败');
    });
  }
  update_cancel(data: any, name: string) {
    this.list_page.chartRepoTable.clickResourceNameByRow([name]);
    this.waitSyncEnd();
    this.clickOperation('更新');
    this.create_page.fillForm(data);
    this.auiDialog.clickCancel();
    this.waitElementNotPresent(
      $('aui-dialog>.aui-dialog'),
      '更新模板仓库失败',
      3000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '更新模板仓库失败');
    });
  }
  delete(name: string) {
    this.list_page.chartRepoTable.clickResourceNameByRow([name]);
    this.clickOperation('删除');
    this._auiDialog.input(name);
    this._auiDialog.clickConfirm();
    this.waitElementNotPresent(
      $('aui-dialog>.aui-dialog'),
      '删除模板仓库失败',
      5000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '删除模板仓库失败');
    });
  }
  delete_cancel(name: string) {
    this.list_page.chartRepoTable.clickResourceNameByRow([name]);
    this.clickOperation('删除');
    this._auiDialog.input(name);
    this._auiDialog.clickCancel();
    this.waitElementNotPresent(
      $('aui-dialog>.aui-dialog'),
      '删除模板仓库失败',
      3000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '删除模板仓库失败');
    });
  }
}
