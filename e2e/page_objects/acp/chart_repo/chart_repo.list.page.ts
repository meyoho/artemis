import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { $, by, element } from 'protractor';

import { CreatePage } from './chart_repo.create.page';

export class ListPage extends AcpPageBase {
  get chartRepoTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('ng-component>aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
    );
  }
  private get create_page() {
    return new CreatePage();
  }
  get _auiDialog() {
    return new AuiDialog(
      $('aui-dialog>.aui-dialog'),
      'aui-dialog-content',
      '.aui-button--danger',
      '.aui-button--default',
      '.aui-dialog__header-title',
    );
  }
  getButtonByText(buttonText): AlaudaButton {
    return new AlaudaButton(
      element(by.xpath(`//button/span[contains(text(),"${buttonText}")]`)),
    );
  }
  assert_error(assert: boolean, message) {
    if (!assert) {
      throw new Error(message);
    }
  }
  update(data: any, name: string) {
    this.chartRepoTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-menu-item',
    );
    this.create_page.fillForm(data);
    this.auiDialog.clickConfirm();
    this.waitElementNotPresent(
      this.auiDialog.auiDialog,
      '更新模板仓库失败',
      5000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '更新模板仓库失败');
    });
  }
  update_cancel(data: any, name: string) {
    this.chartRepoTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-menu-item',
    );
    this.create_page.fillForm(data);
    this.auiDialog.clickCancel();
    this.waitElementNotPresent(
      this.auiDialog.auiDialog,
      '更新模板仓库失败',
      3000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '更新模板仓库失败');
    });
  }
  delete(name: string) {
    this.chartRepoTable.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button',
      'aui-menu-item',
    );
    this._auiDialog.input(name);
    this._auiDialog.clickConfirm();
    this.waitElementNotPresent(
      this._auiDialog.auiDialog,
      '删除模板仓库失败',
      5000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '删除模板仓库失败');
    });
  }
  delete_cancel(name: string) {
    this.chartRepoTable.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button',
      'aui-menu-item',
    );
    this._auiDialog.input(name);
    this._auiDialog.clickCancel();
    this.waitElementNotPresent(
      this._auiDialog.auiDialog,
      '删除模板仓库失败',
      3000,
    ).then(isnotpresent => {
      this.assert_error(isnotpresent, '删除模板仓库失败');
    });
  }
}
