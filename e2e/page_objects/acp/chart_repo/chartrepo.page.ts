import { CreatePageVerify } from '@e2e/page_verify/acp/chart_repo/create.page';
import { DetailPageVerify } from '@e2e/page_verify/acp/chart_repo/detail.page';
import { ListPageVerify } from '@e2e/page_verify/acp/chart_repo/list.page';

import { AcpPageBase } from '../acp.page.base';

import { CreatePage } from './chart_repo.create.page';
import { DetailPage } from './chart_repo.detail.page';
import { ListPage } from './chart_repo.list.page';
import { PrepareData } from './prepare.page';

export class ChartRepoPage extends AcpPageBase {
    get createPage() {
        return new CreatePage();
    }
    get listPage() {
        return new ListPage();
    }
    get detailPage() {
        return new DetailPage();
    }
    get prepare() {
        return new PrepareData();
    }
    get createPageVerify() {
        return new CreatePageVerify();
    }
    get detailPageVerify() {
        return new DetailPageVerify();
    }
    get listPageVerify() {
        return new ListPageVerify();
    }
}
