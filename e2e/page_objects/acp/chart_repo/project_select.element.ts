import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementArrayFinder, ElementFinder, Key, browser } from 'protractor';

export class ProjectSelect extends AlaudaElementBase {
  private root;
  private tootip;
  private option_selector;
  constructor(root: ElementFinder, tootip: ElementFinder, option_selector) {
    super();
    this.root = root;
    this.tootip = tootip;
    this.option_selector = option_selector;
  }
  get inputBox(): AlaudaInputbox {
    return new AlaudaInputbox(this.root.$('input'));
  }
  select(search_key: string, name = '') {
    this.inputBox.inputbox.getAttribute('readonly').then(readonly => {
      if (readonly) {
        this.inputBox.inputbox.click();
      }
      this.inputBox.input(search_key).then(() => {
        browser.sleep(1000).then(() => {
          const opts: ElementArrayFinder = this.tootip
            .$$(this.option_selector)
            .filter(opt_ele => {
              return opt_ele.getText().then(opt_txt => {
                if (name === '') {
                  const display_ele: ElementFinder = opt_ele.$(
                    '.option__display-name',
                  );
                  const opt_txts = opt_txt;
                  return display_ele.isPresent().then(ispresent => {
                    if (ispresent) {
                      display_ele.getText().then(display_name => {
                        console.log(opt_txts);
                        const f: string = opt_txts
                          .replace(display_name, '')
                          .trim();
                        console.log(`|${f}|`);
                        return f === search_key;
                      });
                    } else {
                      return opt_txt.trim() === search_key;
                    }
                  });
                } else {
                  return opt_txt.trim() === name;
                }
              });
            });
          opts.count().then(length => {
            if (length > 0) {
              opts.first().click();
            } else {
              console.log('没有找到合适的选项');
              this.inputBox.inputbox.sendKeys(Key.ENTER);
            }
          });
        });
      });
    });
  }
}
