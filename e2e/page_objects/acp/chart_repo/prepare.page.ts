import { AcpPreparePage } from '@e2e/page_objects/acp/prepare.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { alauda_type_chartrepos } from '@e2e/utility/resource.type.k8s';
import { ServerConf } from '@e2e/config/serverConf';

export class PrepareData extends AcpPreparePage {
  get appmarketIsEnabled() {
    return this.featureIsEnabled('app-market');
  }
  public delete_chart_repo(name: string) {
    CommonKubectl.deleteResource(
      alauda_type_chartrepos,
      name,
      CommonKubectl.globalClusterName,
      ServerConf.GLOBAL_NAMESPCE,
    );
  }
  createChartRepoByTemplate(yamlFile: string, data) {
    return CommonKubectl.createResourceByTemplate(
      yamlFile,
      data,
      'chart_repo_data_tmp',
      CommonKubectl.globalClusterName,
    );
  }
  deleteChartRepoByTemplate(yamlFile: string, data) {
    return CommonKubectl.deleteResourceByTemplate(
      yamlFile,
      data,
      'chart_repo_data_tmp',
      CommonKubectl.globalClusterName,
    );
  }
}
