import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $ } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

export class ScriptInfoDialog extends AcpPageBase {
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement('.aui-dialog__content .item', '.item__label');
    }

    get _root() {
        const root = $('aui-dialog');
        this.waitElementPresent(root, '脚本信息dialog 没有打开');
        return root;
    }

    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性,返回值是右面的元素控件
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example
     * getElementByText('脚本').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(text: string): any {
        switch (text) {
            case '警告信息':
                return this._root.$('.aui-inline-alert__content');
            case '计算节点':
                return this.alaudaElement.getElementByText(text, '.item__node');

            case '脚本':
                return new AlaudaInputbox(
                    this.alaudaElement.getElementByText(text + ':', 'textarea')
                );
            case '复制脚本':
                return new AlaudaButton(
                    this._root.$('.aui-dialog__footer button[class*="primary"]')
                );
            case '取消':
                return new AlaudaButton(
                    this._root.$(
                        '.aui-dialog__footer button:not( [class*="primary"])'
                    )
                );
        }
    }

    close() {
        this.getElementByText('取消').click();
        this.waitElementNotPresent($('aui-dialog'), '脚本信息dialog 没有关闭');
    }
}
