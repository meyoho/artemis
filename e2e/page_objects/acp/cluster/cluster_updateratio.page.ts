import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class UpdateRatioPage extends AcpPageBase {
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-dialog-content .aui-form-item',
      '.aui-form-item__label',
    );
  }
  enterValue(name: string, value) {
    switch (name) {
      case 'CPU超售比':
      case '内存超售比':
        const aui_switch = new AuiSwitch(
          this.alaudaElement.getElementByText(name, 'aui-switch'),
        );
        if (value === 0) {
          aui_switch.close();
        } else {
          aui_switch.open();
          new AlaudaInputbox(
            this.alaudaElement.getElementByText(name, '.aui-input'),
          ).input(value);
        }
        break;
    }
  }
}
