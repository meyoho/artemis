import { $, $$, ElementFinder } from 'protractor';

import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AcpPageBase } from '../acp.page.base';

import { CreateTokenPage } from './token_create.page';

export class AccessClusterPage extends AcpPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }

  get alertMessage(): ElementFinder {
    return $('aui-inline-alert');
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性,返回值是右面的元素控件
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '集群地址':
        return new AlaudaInputbox(
          super.getElementByText(text, 'aui-input-group input.apiserver'),
        );
      case 'token':
        return new CreateTokenPage();
      default:
        return new AlaudaInputbox(super.getElementByText(text));
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述'，'描述信息')
   */
  enterValue(name: string, value: string, tagname: string = 'input') {
    switch (name) {
      case '集群地址':
        this.getElementByText(name).input(value, tagname);
        break;
      case 'token':
        this.getButtonByText('添加 Token').click();
        this.getElementByText(name).input(value);
        break;
      default:
        this.getElementByText(name).input(value);
        break;
    }
  }

  /**
   * 接入集群
   * @param testData 测试数据
   */
  access(testData) {
    this.clickLeftNavByText('集群');
    this.getButtonByText('接入集群');
    this.fillForm(testData);
    this.confirmDialog.clickConfirm();
  }

  get itemsRequired() {
    return $$('div[class*="required"]');
  }

  get itemsRequiredTooltip() {
    return $$('rc-tooltip-content');
  }
}
