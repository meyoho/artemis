import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AcpPageBase } from '../acp.page.base';

export class CreateTokenPage extends AcpPageBase {
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-dialog-content .aui-form-item',
            'aui-dialog-content .aui-form-item__label'
        );
    }
    /**
     * 根据左侧文字获得右面元素,
     * 注意：子类如果定位不到元素，需要重写此属性
     * @param text 左侧文字
     */
    getElementByText(text: string, tagname: string = 'input'): any {
        switch (text) {
            case '名称':
                return new AlaudaInputbox(super.getElementByText(text));
            case '类型':
                return new AlaudaRadioButton(
                    super.getElementByText(text, 'aui-radio-group')
                );
            case 'Token':
                return new AlaudaInputbox(
                    super.getElementByText(text, 'textarea')
                );
        }
        return this.alaudaElement.getElementByText(text, tagname);
    }

    /**
     * 在文本框中输入值
     * 注意：如果右侧不是inputbox定位，子类需要重写此方法
     * @param name 文本框左侧的文字
     * @param value 输入文本框中的值
     */
    enterValue(name, value) {
        switch (name) {
            case '名称':
                this.getElementByText(name).input(value);
                break;
            case '类型':
                this.getElementByText(name).clickByName(value);
                break;
            case 'Token':
                this.getElementByText(name).input(value);
                break;
        }
    }

    input(testData) {
        this.fillForm(testData);
        this.auiDialog.clickConfirm();
    }
}
