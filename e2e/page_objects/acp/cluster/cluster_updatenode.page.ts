import { $ } from 'protractor';

import { AuiConfirmDialog } from '../../../element_objects/alauda.aui_confirm_dialog';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { TableInput } from '../../../element_objects/alauda.table_input';
import { AcpPageBase } from '../acp.page.base';
// import { AuiDialog } from "@ele/alauda.aui_dialog";

export class UpdateNodePage extends AcpPageBase {
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-dialog aui-dialog--medium',
            'aui-dialog .aui-dialog__content'
        );
    }
    // get UpdateLabelDialog(){
    //     return new AuiConfirmDialog($('aui-dialog aui-dialog--medium'));
    // }
    /**
     * 可调度/不可调度对话框
     */
    get CrondonDialog() {
        return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
    }
    /**
     * 点击页面取消按钮
     */
    clickButon() {
        const btn = new AlaudaButton(
            $('aui-dialog .aui-dialog__footer button:last-child')
        );
        btn.click();
    }
    /**
     * 更新table数据
     * @param testData 要填写的数据
     */
    tableInput(testData) {
        const ele = new TableInput($('rc-array-form-table table'));
        ele.inputTableValues(testData);
    }
    /**
     * 点击指定添加按钮
     */
    clickAddButton() {
        const ele = new AlaudaButton(
            $('.rc-array-form-table__bottom-control-buttons button')
        );
        ele.click();
    }
}
