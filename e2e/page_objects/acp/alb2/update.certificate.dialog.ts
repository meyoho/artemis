import { $, ElementFinder } from 'protractor';

import { AuiDialog } from '../../../element_objects/alauda.aui_dialog';
import { CreateFrontendPage } from './create.frontend.page';

export class UpdateCertificateDialog extends CreateFrontendPage {
  get title(): ElementFinder {
    return $('aui-dialog-header .aui-dialog__header-title');
  }

  /**
   *  更新默认证书对话框
   */
  get updateDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  update(testData) {
    this.fillForm(testData);
    this.auiDialog.buttonConfirm.click();
    this.waitElementNotPresent(this.title, '更新默认证书对话框没有关闭').then(
      isNotPresent => {
        if (!isNotPresent) {
          throw Error('更新默认证书失败');
        }
      },
    );
  }
}
