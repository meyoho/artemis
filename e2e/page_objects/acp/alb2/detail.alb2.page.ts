import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, $$, browser, protractor } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';

import { Alb2ListPage } from './list.alb2.page';
import { UpdateServiceDialog } from './update.service.dialog';

export class DetaiAlb2Page extends AcpPageBase {
  get listAlb2Page(): Alb2ListPage {
    return new Alb2ListPage();
  }
  get name() {
    return $('rc-load-balancer-detail-info  .aui-card__header').getText();
  }
  /**
   * 更新默认内部路由
   */
  get updateServiceDialog(): UpdateServiceDialog {
    return new UpdateServiceDialog();
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-field-set-item',
      '.field-set-item__label label',
      $('rc-field-set-group'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '操作':
        return new AlaudaDropdown(
          $('.aui-card__header .aui-button aui-icon'),
          $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
        );
      case '侦听器':
        return new AuiTableComponent(
          $('.detail-listeners .aui-card'),
          '.aui-card__content aui-table',
        );
      case '添加监听端口':
        return this.getButtonByText('添加监听端口');
      default:
        return this.alaudaElement.getElementByText(
          text,
          '.field-set-item__value',
        );
    }
  }

  enterPortDetail(port) {
    const table: AuiTableComponent = this.getElementByText('侦听器');
    table.clickResourceNameByRow([port], '端口');
  }

  /**
   * 端口详情页更新默认内部路由
   * @param testData
   */
  updateDefaultInternalRouting(alb_name, port, testData) {
    this.listAlb2Page.enterAlb2Detail(alb_name);
    const table: AuiTableComponent = this.getElementByText('侦听器');
    table.clickOperationButtonByRow(
      [port],
      '更新默认内部路由',
      'button[class*="aui-button"]',
    );
    this.updateServiceDialog.update(testData);
  }

  deletePort(name, port) {
    this.listAlb2Page.enterAlb2Detail(name);
    this.getElementByText('侦听器').clickOperationButtonByRow(
      [port],
      '删除',
      'button[class*="aui-button"]',
    );

    this.confirmDialog.clickConfirm();

    this.waitElementPresent(
      $('.detail-listeners aui-table'),
      '等待 10s后，负载均衡详情页侦听器未显示',
      10000,
    );
  }
  updateProject(alb_name, projectName) {
    this.listAlb2Page.enterAlb2Detail(alb_name);
    this.getElementByText('操作').select('更新项目');
    this.listAlb2Page.project.auiIcon.click();
    this.listAlb2Page.project.inputBox.input(projectName);
    this.listAlb2Page.project.inputBox.inputbox.sendKeys(protractor.Key.ENTER);
    browser.sleep(20);
    this.auiDialog.clickConfirm();
  }
  deleteAlb(name) {
    this.listAlb2Page.enterAlb2Detail(name);
    this.getElementByText('操作').select('删除负载均衡器');
    new AlaudaInputbox($('.aui-dialog__content input')).input(name);
    new AlaudaButton($('.aui-dialog__footer button[aui-button="danger"]'))
      .click()
      .then(() => {
        browser.sleep(1000);
        const cancel = new AlaudaButton(
          $('.aui-dialog__footer button[aui-button=""]'),
        );
        cancel.isPresent().then(isPresent => {
          if (isPresent) {
            cancel.click();
          }
        });
      });
  }
}
