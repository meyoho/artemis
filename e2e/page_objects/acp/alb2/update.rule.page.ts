import { CreateRulePage } from './create.rule.page';
import { $ } from 'protractor';
export class UpdateRulePage extends CreateRulePage {
  /**
   * 更新规则
   * @param port 端口
   * @param testData 测试数据
   */
  updateRule(testData, flag) {
    this.fillForm(testData);
    if (flag) {
      this.clickConfirm().then(() => {
        this.waitElementPresent(
          $('.detail-port-list aui-table'),
          '等待 10s后，端口详情页规则未显示',
          10000,
        ).then(isPresent => {
          if (!isPresent) {
            throw new Error('更新规则 失败');
          }
        });
      });
    } else {
      this.clickCancel();
    }
  }
}
