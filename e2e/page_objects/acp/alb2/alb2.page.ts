import { DetaiAlb2PageVerify } from '@e2e/page_verify/acp/alb2/detail.alb2.page';
import { DetaiFrontendPageVerify } from '@e2e/page_verify/acp/alb2/detail.frontend.page';
import { Alb2ListPageVerify } from '@e2e/page_verify/acp/alb2/list.alb2.page';

import { AcpPageBase } from '../acp.page.base';

import { CreateAlbPage } from './create.alb.page';
import { CreateFrontendPage } from './create.frontend.page';
import { CreateRulePage } from './create.rule.page';
import { DeletePage } from './delete.page';
import { DetaiAlb2Page } from './detail.alb2.page';
import { DetaiFrontendPage } from './detail.frontend.page';
import { Alb2ListPage } from './list.alb2.page';
import { PreparePage } from './prepare.page';
import { UpdateRulePage } from './update.rule.page';

export class Alb2Page extends AcpPageBase {
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  get listPage(): Alb2ListPage {
    return new Alb2ListPage();
  }

  get alb2ListPageVerify(): Alb2ListPageVerify {
    return new Alb2ListPageVerify();
  }

  /**
   * 负载均衡详情页
   */
  get detailAlbPage() {
    return new DetaiAlb2Page();
  }
  get detailAlbPageVerify(): DetaiAlb2PageVerify {
    return new DetaiAlb2PageVerify();
  }
  get createAlbPage() {
    return new CreateAlbPage();
  }
  get createFrontendPage() {
    return new CreateFrontendPage();
  }
  get deletePage() {
    return new DeletePage();
  }
  get detailFrontendPage() {
    return new DetaiFrontendPage();
  }

  get detaiFrontendPageVerify(): DetaiFrontendPageVerify {
    return new DetaiFrontendPageVerify();
  }

  get createRulePage() {
    return new CreateRulePage();
  }
  get updateRulePage() {
    return new UpdateRulePage();
  }

  /**
   * port自动补全 5 位
   * @param port
   */
  autoComplete(port: string) {
    return port.padStart(5, '0');
    // return ('00000' + port).substr(-5);
  }
}
