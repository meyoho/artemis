import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $, browser, protractor } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AcpPageBase } from '../acp.page.base';

export class Alb2ListPage extends AcpPageBase {
  // get detailAlb2Page(): DetaiAlb2Page {
  //     return new DetaiAlb2Page();
  // }
  /**
   * 负载均衡列表
   */
  get table() {
    return new AuiTableComponent(
      $('.layout-page-content .aui-card'),
      'aui-table',
      'aui-search',
    );
  }
  /**
   * 过滤框
   */
  get searchInput() {
    return new AlaudaInputbox($('.aui-card__header aui-search input'));
  }

  search(name, rowCount = 1) {
    this.clickLeftNavByText('负载均衡');
    this.searchInput.input(name);
    this.searchInput.inputbox.sendKeys(protractor.Key.ENTER);
    if (rowCount !== 0) {
      this.table.waitRowCountChangeto(rowCount);
    }
  }
  /**
   * 按名称过滤
   * @param name 名称
   */
  filterAlb(name) {
    // this.search.input(`${name} \n`);
    // return browser.sleep(2000);
    this.table.searchByResourceName(name);
  }

  /**
   * 点击负载均衡列表页 名称进入详情页
   * @param name 名称
   */
  enterAlb2Detail(name: string) {
    this.clickLeftNavByText('负载均衡');
    this.table.clickResourceNameByRow([name]);
    return this.waitElementPresent(
      $('rc-load-balancer-detail-info aui-card .aui-card__header'),
      '等待 10s后，负载均衡详情页的基本信息未显示',
      10000,
    );
  }
  /**
   *  更新项目对话框
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-dialog__content  .aui-form-item',
      '.aui-dialog__content .aui-form-item__label',
    );
  }
  get project() {
    return new AuiSelect(
      this.alaudaElement.getElementByText('分配项目', 'aui-select'),
    );
  }
  updateProject(name, project_name) {
    this.table.clickOperationButtonByRow(
      [name],
      '更新项目',
      'button[class*="aui-button"]',
    );
    this.project.input(project_name);
    this.auiDialog.clickConfirm();
  }
  deleteAlb(name) {
    this.search(name);
    this.table.clickOperationButtonByRow(
      [name],
      '删除',
      'button[class*="aui-button"]',
    );
    new AlaudaInputbox($('.aui-dialog__content input')).input(name);
    new AlaudaButton($('.aui-dialog__footer button[aui-button="danger"]'))
      .click()
      .then(() => {
        browser.sleep(1000);
        const cancel = new AlaudaButton(
          $('.aui-dialog__footer button[aui-button=""]'),
        );
        cancel.isPresent().then(isPresent => {
          if (isPresent) {
            cancel.click();
          }
        });
      });
  }
}
