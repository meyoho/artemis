import { $, ElementFinder } from 'protractor';

import { AuiDialog } from '../../../element_objects/alauda.aui_dialog';

import { CreateFrontendPage } from './create.frontend.page';

export class UpdateServiceDialog extends CreateFrontendPage {
  get title(): ElementFinder {
    return $('aui-dialog-header .aui-dialog__header-title');
  }

  /**
   *  更新默认内部路由对话框
   */
  get updateDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  update(testData) {
    this.fillForm(testData);
    this.auiDialog.buttonConfirm.click();
    this.waitElementNotPresent(this.title, '更新内部路由对话框没有关闭').then(
      isNotPresent => {
        if (!isNotPresent) {
          throw Error('更新内部路由失败');
        }
      },
    );
  }
}
