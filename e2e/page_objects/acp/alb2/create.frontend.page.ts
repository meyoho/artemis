import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $, $$, element, by, ElementFinder } from 'protractor';

import { ArrayFormTable } from '../../../element_objects/alauda.arrayFormTable';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AcpPageBase } from '../acp.page.base';

import { DetaiAlb2Page } from './detail.alb2.page';
import { Alb2ListPage } from './list.alb2.page';
import { PreparePage } from './prepare.page';

export class CreateFrontendPage extends AcpPageBase {
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  /**
   * 列表页
   */
  get listPage(): Alb2ListPage {
    return new Alb2ListPage();
  }

  get detailPage(): DetaiAlb2Page {
    return new DetaiAlb2Page();
  }
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}") ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '协议':
        return this._getElementByText(text, 'aui-select');
      case '默认证书':
        return this._getElementByText(text, 'aui-select');
      case '内部路由组':
        return this._getElementByText(text, 'rc-array-form-table');
      case '会话保持':
        return this._getElementByText(text, 'aui-radio-group');
      default:
        return this._getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '协议':
        new AlaudaDropdown(
          this.getElementByText(name),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        ).select(value);
        break;
      case '内部路由组':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
      case '会话保持':
        new AlaudaRadioButton(this.getElementByText(name)).clickByName(value);
        break;
      case '默认证书':
        new AuiSelect(this.getElementByText(name)).select(
          value['命名空间'],
          value['保密字典'],
        );
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
  }

  /**
   * 添加监听端口
   * @param testData 数据
   */
  addPort(testData) {
    this.clickLeftNavByText('负载均衡');
    const alb2name = this.preparePage.isReady();
    // 进入alb2 的详情页
    this.listPage.enterAlb2Detail(alb2name);
    this.detailPage.getButtonByText('添加监听端口').click();
    this.fillForm(testData);
    this.clickConfirm().then(() => {
      this.waitElementPresent(
        $('rc-load-balancer-detail-port-info rc-field-set-group'),
        '等待 10s后，端口详情页规则未显示',
        10000,
      );
    });
  }
}
