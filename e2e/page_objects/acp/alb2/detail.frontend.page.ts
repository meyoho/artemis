import { $, $$, ElementFinder } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';

import { CreateRulePage } from './create.rule.page';
import { DetaiAlb2Page } from './detail.alb2.page';
import { Alb2ListPage } from './list.alb2.page';
import { UpdateCertificateDialog } from './update.certificate.dialog';
import { UpdateServiceDialog } from './update.service.dialog';
import { UpdateRulePage } from './update.rule.page';

export class DetaiFrontendPage extends AcpPageBase {
  get listPage(): Alb2ListPage {
    return new Alb2ListPage();
  }

  /**
   * 负载均衡详情页
   */
  get detailAlbPage() {
    return new DetaiAlb2Page();
  }

  get updateRulePage(): UpdateRulePage {
    return new UpdateRulePage();
  }

  get updateServiceDialog(): UpdateServiceDialog {
    return new UpdateServiceDialog();
  }
  get updateCertificateDialog(): UpdateCertificateDialog {
    return new UpdateCertificateDialog();
  }
  get createRulePage(): CreateRulePage {
    return new CreateRulePage();
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-field-set-item',
      '.field-set-item__label',
      $('rc-field-set-group'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '操作':
        return new AlaudaDropdown(
          $('.aui-card__header .aui-button aui-icon'),
          $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
        );
      case '规则':
        return new AuiTableComponent(
          $('.detail-port-list>div'),
          '.aui-card__content aui-table',
          '',
        );
      case '添加规则':
        return this.getButtonByText('添加规则');
      default:
        return this.alaudaElement.getElementByText(
          text,
          '.field-set-item__value',
        );
    }
  }

  /**
   * 表格为空时的显示, 子类不一样时需要重载
   */
  get noResult(): ElementFinder {
    return $('.aui-card__content .empty-placeholder');
  }

  /**
   * 进入frontend 的详情页
   * @param alb_name 负载均衡的名称
   * @param port 端口号
   */
  enterDetailPage(alb_name, port) {
    this.listPage.enterAlb2Detail(alb_name);
    this.detailAlbPage.enterPortDetail(port);
  }

  /**
   * 端口详情页更新默认内部路由
   * @param testData
   */
  updateDefaultInternalRouting(alb_name, port, testData) {
    this.enterDetailPage(alb_name, port);

    const operation: AlaudaDropdown = this.getElementByText('操作');
    operation.select('更新默认内部路由');
    this.updateServiceDialog.update(testData);
  }
  /**
   * 端口详情页更新默认证书
   * @param testData
   */
  updateDefaultCertificate(alb_name, port, testData) {
    this.enterDetailPage(alb_name, port);
    const operation: AlaudaDropdown = this.getElementByText('操作');
    operation.select('更新默认证书');
    this.updateCertificateDialog.update(testData);
  }

  /**
   * 添加规则
   * @param alb_name 负载均衡名称
   * @param port 端口号
   * @param testData 添加规则的信息
   * @param flag true时添加按钮，false时取消按钮
   */
  addRule(alb_name, port, testData, flag = true) {
    // 进入alb2 的详情页
    this.enterDetailPage(alb_name, port);
    this.getButtonByText('添加规则').click();
    this.createRulePage.fillForm(testData);
    if (flag) {
      this.createRulePage.clickConfirm();
    } else {
      this.createRulePage.clickCancel();
    }
    this.waitElementPresent(
      $('.detail-port-list .aui-card__header'),
      '等待 10s后，端口详情页规则未显示',
      10000,
    );
  }

  /**
   * 更新规则
   * @param alb_name 负载均衡名称
   * @param port 端口号
   * @param ruleName 规则描述（唯一）
   * @param testData 更新规则的信息
   * @param flag true时更新按钮，false时取消按钮
   */
  updateRule(alb_name, port, ruleName, testData, flag = true) {
    // 进入alb2 的详情页
    this.enterDetailPage(alb_name, port);
    const table: AuiTableComponent = this.getElementByText('规则');
    table.clickOperationButtonByRow(
      [ruleName],
      '更新规则',
      'button[aui-button="text"]',
    );
    this.updateRulePage.updateRule(testData, flag);
  }

  /**
   * 删除规则
   * @param alb_name 负载均衡名
   * @param port 端口号
   * @param ruleName 规则名
   * @param flag true时删除按钮，false时取消按钮
   */
  deleteRule(alb_name, port, ruleName, flag = true) {
    this.enterDetailPage(alb_name, port);
    const table: AuiTableComponent = this.getElementByText('规则');
    table.clickOperationButtonByRow(
      [ruleName],
      '删除规则',
      'button[aui-button="text"]',
    );
    if (flag) {
      this.confirmDialog.clickConfirm();
    } else {
      this.confirmDialog.clickCancel();
    }
    this.waitElementPresent($('.detail-port-list>div'), '规则表格没出现');
  }

  deletePort(name, port) {
    this.enterDetailPage(name, port);
    const operation: AlaudaDropdown = this.getElementByText('操作');
    operation.select('删除端口');

    this.confirmDialog.clickConfirm();

    this.waitElementPresent(
      $('.detail-listeners aui-table'),
      '等待 10s后，负载均衡详情页侦听器未显示',
      10000,
    );
  }
}
