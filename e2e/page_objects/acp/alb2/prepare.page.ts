import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';
import { AcpPreparePage } from '../prepare.page';
import { ServerConf } from '@e2e/config/serverConf';

export class PreparePage extends AcpPreparePage {
  alb_ns = ServerConf.GLOBAL_NAMESPCE;
  isReady(): string {
    try {
      const items = CommonMethod.parseYaml(
        CommonKubectl.execKubectlCommand(
          `kubectl get alb2 -n ${this.alb_ns} -o yaml`,
          this.clusterName,
        ),
      ).items;
      if (items.count < 1) {
        console.log(
          CommonKubectl.execKubectlCommand(
            `kubectl get alb2 -n ${this.alb_ns} -o yaml`,
            this.clusterName,
          ),
        );
        throw new Error(`当前集群${this.clusterName} 没有 alb2`);
      } else {
        return items[0].metadata.name;
      }
    } catch {
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl get alb2 -n ${this.alb_ns} -o yaml`,
          this.clusterName,
        ),
      );
      throw new Error(`当前集群${this.clusterName} 没有 alb2`);
    }
  }

  deleteFrontend(alb_name: string, port: string) {
    const tmp = ('00000' + port).substr(-5);
    const commond = `kubectl get rule -n ${this.alb_ns}|grep ${alb_name}-${tmp}|awk '{print $1}'`;
    CommonKubectl.execKubectlCommand(commond, this.clusterName)
      .split('\n')
      .forEach(tmp => {
        if (tmp !== '') {
          console.log(
            CommonKubectl.execKubectlCommand(
              `kubectl delete rule  -n ${this.alb_ns} ${tmp}`,
              this.clusterName,
            ),
          );
        }
      });
    CommonKubectl.deleteResource(
      'frontend',
      `${alb_name}-${tmp}`,
      this.clusterName,
      this.alb_ns,
    );
  }

  deleteService(name: string, ns_name: string = this.namespace1Name) {
    CommonKubectl.deleteResource('service', name, this.clusterName, ns_name);
  }

  deleteHelmRequest(name: string, ns_name = this.alb_ns) {
    CommonKubectl.deleteResource(
      'helmrequest',
      name,
      CommonKubectl.globalClusterName,
      ns_name,
    );
  }
}
