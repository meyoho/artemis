import { Alb2Rule } from '@e2e/element_objects/acp/alb2/alb2_rule';
import { browser, element, by, ElementFinder } from 'protractor';

import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AcpPageBase } from '../acp.page.base';

export class CreateRulePage extends AcpPageBase {
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}") ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '规则':
        return this._getElementByText(
          text,
          'div[@class="aui-form-item__content"]',
        );
      default:
        return this._getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '规则':
        new Alb2Rule(this.getElementByText(name)).input(value);
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
    browser.sleep(100);
  }
}
