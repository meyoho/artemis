import { AlaudaAuiNumberInput } from '@e2e/element_objects/alauda.aui_number_input';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $, protractor, browser, ElementFinder, element, by } from 'protractor';

import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AcpPageBase } from '../acp.page.base';

import { Alb2ListPage } from './list.alb2.page';

export class CreateAlbPage extends AcpPageBase {
  /**
   * 列表页
   */
  get listPage(): Alb2ListPage {
    return new Alb2ListPage();
  }

  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}") ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '类型':
        return this._getElementByText(text, 'aui-radio-group');
      case '副本数':
        return this._getElementByText(text, 'aui-number-input');
      case '主机标签':
        return this._getElementByText(text, 'aui-multi-select');
      case '分配项目':
        return this._getElementByText(text, 'aui-select');
      default:
        return this._getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '类型':
        new AlaudaRadioButton(this.getElementByText(name)).clickByName(value);
        break;
      case '副本数':
        new AlaudaAuiNumberInput(this.getElementByText(name)).input(value);
        break;
      case '主机标签':
        value.forEach(label => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(label);
        });
        break;
      case '分配项目':
        const aui_select = new AuiSelect(this.getElementByText(name));
        aui_select.auiIcon.click();
        aui_select.inputBox.input(value);
        aui_select.inputBox.inputbox.sendKeys(protractor.Key.ENTER);
        browser.sleep(20);
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
  }

  /**
   * 添加负载均衡
   * @param testData 数据
   */
  addAlb(testData) {
    this.clickLeftNavByText('负载均衡');
    this.getButtonByText('创建负载均衡器').click();
    this.fillForm(testData);
    this.auiDialog.clickConfirm().then(() => {
      this.confirmDialog.isPresent().then(ispresent => {
        if (ispresent) {
          this.confirmDialog.clickConfirm();
        }
      });
      this.waitElementPresent(
        $('rc-load-balancer-detail-info'),
        '等待 30s后，负载均衡详情页未显示',
        30000,
      ).then(ispresent => {
        if (!ispresent) {
          this.auiDialog.clickCancel();
        }
      });
    });
  }
}
