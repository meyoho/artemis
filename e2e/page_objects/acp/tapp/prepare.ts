import { CommonKubectl } from '@e2e/utility/common.kubectl';
import {
  tke_type_tapp,
  k8s_type_horizontalpodautoscalers,
  k8s_type_cronhpas,
} from '@e2e/utility/resource.type.k8s';

import { AcpPreparePage } from '../prepare.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { ServerConf } from '@e2e/config/serverConf';
import { browser } from 'protractor';

export class PreparePage extends AcpPreparePage {
  get cronhpaIsEnabled() {
    return this.featureIsEnabled('cron-hpa');
  }
  tappIsEnabled(cluster = this.clusterName) {
    return this.featureIsEnabled('tapp', cluster);
  }
  deleteTapp(
    name: string,
    namespace = this.namespace1Name,
    cluster_name = this.clusterName,
  ) {
    CommonKubectl.deleteResource(tke_type_tapp, name, cluster_name, namespace);
  }
  deletePodByApi(
    tappName: string,
    podName: string,
    ns = this.namespace1Name,
    cluster = this.clusterName,
  ) {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl -X PATCH ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}'  -H 'content-type:application/merge-patch+json' --data '{"spec":{"statuses":{"${
      podName.split('-')[podName.split('-').length - 1]
    }":"Killed"}}}' -k ${
      ServerConf.BASE_URL
    }/kubernetes/${cluster}/apis/apps.tkestack.io/v1/namespaces/${ns}/tapps/${tappName}`;
    console.log(cmd);
    const rsp: string = CommonMethod.execCommand(cmd);
    console.log(rsp);
  }
  createTapp(data, cluster_name = this.clusterName) {
    return CommonKubectl.createResourceByTemplate(
      'alauda.tapp.tpl.yaml',
      data,
      `alauda.tapp.${this.getRandomNumber()}`,
      cluster_name,
    );
  }
  waitTappPodRunning(
    tapp_name,
    namespace = this.namespace1Name,
    cluster_name = this.clusterName,
  ) {
    return browser.sleep(3000).then(() => {
      let times = 0;
      while (times < 20) {
        const ret = CommonKubectl.execKubectlCommand(
          `kubectl get pod -n ${namespace} --selector='service.${ServerConf.LABELBASEDOMAIN}/name=tapp-${tapp_name}'`,
          cluster_name,
        ).includes('Running');
        if (ret) {
          times = 20;
          return ret;
        } else {
          CommonMethod.sleep(3000);
          times += 1;
        }
      }
      return false;
    });

    // return browser.wait(
    //   browser.sleep(1000).then(() => {
    //     return CommonKubectl.execKubectlCommand(
    //       `kubectl get pod -n ${namespace} --selector='service.${ServerConf.LABELBASEDOMAIN}/name=tapp-${tapp_name}'`,
    //       cluster_name,
    //     ).includes('Running');
    //   }),
    //   60000,
    //   '等待pod启动失败',
    // );
  }
  deleteHpa(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_horizontalpodautoscalers,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
  deleteCronHpa(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_cronhpas,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
}
