import { DeploymentListPage } from '../deployment/list.page';
import { $ } from 'protractor';

export class TappListPage extends DeploymentListPage {
  /**
   * 检索 Tapp
   * @param row_count 期望检索到的行数，检索后会等待表格中行数据变为row_count,直到等待超时
   * @example
   * this.search('example')
   */
  search(name, row_count = 1) {
    this.clickLeftNavByText('Tapp');
    return this.WorkloadTable.searchByResourceName(name, row_count);
  }
  toUpdatePage(name) {
    this.search(name);
    this.WorkloadTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button__content aui-icon',
    );
  }
  conflictRetry() {
    this.confirmDialog.isPresent().then(ispresent => {
      if (ispresent) {
        this.confirmDialog.title.getText().then(text => {
          console.log(text);
        });
        this.confirmDialog.clickConfirm();
      }
    });
  }
  update(name, testData, flag = '确定') {
    this.search(name);
    this.WorkloadTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button__content aui-icon',
    );
    this.updatePage.fillForm(testData);
    if (flag === '确定') {
      this.clickConfirm();
      this.conflictRetry();
      this.waitElementPresent(
        $('.aui-page__content rc-detail-info .aui-card__header>span'),
        'the workload detail not present',
        10000,
      );
    } else if (flag === '取消') {
      this.clickCancel();
      this.waitElementPresent(
        $('aui-inline-alert>.aui-inline-alert'),
        '没有跳转到tapp列表页',
        10000,
      );
    } else if (flag === '面包屑') {
      this.breadcrumb.click('Tapp');
      this.waitElementPresent(
        $('aui-inline-alert>.aui-inline-alert'),
        '没有跳转到tapp列表页',
        10000,
      );
    }
  }
  clickRefresh() {
    return $('aui-search+button').click();
  }
  toDetail(name) {
    this.search(name);
    return this.WorkloadTable.clickResourceNameByRow([name]);
  }
}
