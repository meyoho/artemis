import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { $, browser } from 'protractor';

import { DeploymentCreatePage } from '../deployment/create.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { TappListPage } from './list.page';
export class TappCreatePage extends DeploymentCreatePage {
  get listPage(): TappListPage {
    return new TappListPage();
  }
  get imageSelect() {
    return new ImageSelect($('.cdk-global-overlay-wrapper aui-dialog'));
  }
  create(testData, image, method = '输入', tag = '', flag = '确定') {
    this.clickLeftNavByText('Tapp');
    this.listPage.getButtonByText('创建 Tapp').click();
    this.imageSelect.enterImage(image, method, tag);
    this.fillForm(testData);
    if (flag === '确定') {
      this.getButtonByText('确定').click();
      //创建成功后，跳转到详情页
      this.waitElementPresent(
        $('.aui-page__content rc-detail-info .aui-card__header>span'),
        'the workload detail not present',
        10000,
      );
    } else if (flag === '取消') {
      this.getButtonByText('取消').click();
      //点击取消跳转到列表页
      this.waitElementPresent(
        $('aui-inline-alert>.aui-inline-alert'),
        '没有跳转到tapp列表页',
        10000,
      );
    } else if (flag === '面包屑') {
      this.breadcrumb.click('Tapp');
      this.waitElementPresent(
        $('aui-inline-alert>.aui-inline-alert'),
        '没有跳转到tapp列表页',
        10000,
      );
    }
  }
  createByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('Tapp');
    this.yamlCreateDropDown.select('YAML创建');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        new AlaudaButton($('.form-footer button[aui-button=""]')).click();
        browser.sleep(100);
        break;
      default:
        new AlaudaButton(
          $('.form-footer button[aui-button="primary"]'),
        ).click();
        browser.sleep(100);
        break;
    }
  }
  toCreatePage(image, method = '输入', tag = '') {
    this.clickLeftNavByText('Tapp');
    this.listPage.getButtonByText('创建 Tapp').click();
    this.imageSelect.enterImage(image, method, tag);
  }
  toYamlCreatePage() {
    this.clickLeftNavByText('Tapp');
    this.yamlCreateDropDown.select('YAML创建');
  }
}
