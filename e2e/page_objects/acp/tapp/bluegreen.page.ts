import { AcpPageBase } from '../acp.page.base';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { $, ElementFinder } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';

export class BlueGreenPage extends AcpPageBase {
  get rootPage() {
    return new AuiDialog(
      $('aui-dialog>.aui-dialog--large'),
      '.aui-dialog__content',
      'button[aui-button="primary"]',
      'button[auidialogclose]',
      '.aui-dialog__header',
    );
  }
  get title() {
    return this.rootPage.title.$('.aui-dialog__header-title>div:nth-child(1)');
  }
  get podName() {
    return this.rootPage.title.$('div[class="pod-name"]');
  }
  getContainerFormByName(name: string) {
    const content = this.rootPage.content as ElementFinder;
    return content
      .$$('.container-item')
      .filter(ele => {
        return ele
          .$('aui-form-item:nth-child(1)')
          .$('.aui-form-item__content')
          .getText()
          .then(text => {
            return text.trim() === name;
          });
      })
      .first();
  }
  enterValue(name: string, value: Record<string, string>) {
    const container_form = new AlaudaElement(
      'aui-form-item',
      '.aui-form-item__label',
      this.getContainerFormByName(name),
    );
    new AlaudaButton(container_form.getElementByText('镜像地址', 'button'))
      .click()
      .then(() => {
        new ImageSelect($('aui-dialog>.aui-dialog--big')).enterImage(
          value['镜像地址'],
          value['方式'] ? value['方式'] : '输入',
          value['tag'] ? value['tag'] : '',
        );
      });
  }
}
