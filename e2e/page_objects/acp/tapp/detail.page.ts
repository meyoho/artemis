import { DetailDeploymentPage } from '../deployment/detail.page';
import { TappListPage } from './list.page';
import { $, ElementFinder } from 'protractor';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { BlueGreenPage } from './bluegreen.page';

export class TappDetailPage extends DetailDeploymentPage {
  get listPage(): TappListPage {
    return new TappListPage();
  }
  get detailElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-detail-info .info-list li',
      'rc-detail-info .info-list li div',
      $('rc-detail-info .info-list'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): ElementFinder | AlaudaTagsLabel {
    switch (text) {
      case '状态':
        return this.detailElement.getElementByText(
          text,
          'rc-workload-status-icon',
        );
      case '实例数':
        return $('rc-workload-status .rc-workload-status__total');
      case '容器组标签':
      case '主机选择器':
        // case '固定 IP':
        return new AlaudaTagsLabel(
          this.detailElement.getElementByText(text, 'rc-tags-label'),
          'aui-tag>.aui-tag>span',
          '.tooltip-toggle',
          'aui-tooltip .tooltip-tag span',
        );

      case '更新策略':
        return this.detailElement.getElementByText(text, 'span');
      case '镜像':
      case '资源限制':
      case '启动命令':
      case '参数':
        return this.containerElement.getElementByText(
          text,
          '.field-set-item__value',
        );
      case '存储卷':
      case 'Pod 亲和':
        this.foldableBlock(text).expand();
        return this.detailFoldableElement.getElementByText(
          text,
          'rc-array-form-table table',
        );
      case '已挂载存储卷':
        this.foldableBlock(text).expand();
        return this.containerFoldableElement.getElementByText(
          text,
          'rc-array-form-table table',
        );
      case '更新容器组标签':
        return this.detailElement.getElementByText('容器组标签', 'button');
      case '更新镜像':
        return this.containerElement.getElementByText('镜像', 'button');
      case '更新资源限制':
        return this.containerElement.getElementByText('资源限制', 'button');
      case '镜像版本':
        return this.updateImageElement.getElementByText(text, 'input');
      case '自动扩缩容':
        return $('rc-hpa .aui-card__content');
    }
  }
  deletePod(pod_line: number, flag = '确定', isstatefulset = false) {
    return this.podListTable
      .getRowByIndex(pod_line)
      .$('aui-table-cell:first-child')
      .getText()
      .then(podName => {
        this.podListTable.clickOperationButtonByRow(
          [podName],
          '禁用',
          'button aui-icon[icon="basic:ellipsis_v_s"]',
          '.aui-menu>div>*',
        );
        if (flag === '确定') {
          this.confirmDialog.clickConfirm();
        } else if (flag === '取消') {
          this.confirmDialog.clickCancel();
        }
        if (!isstatefulset) {
          this.waitPodNotExist(podName);
        }
        return podName;
      });
  }
  blueGreen(pod_name, data, flag = '确定') {
    return this.podListTable
      .clickOperationButtonByRow(
        [pod_name],
        '灰度发布',
        'button aui-icon[icon="basic:ellipsis_v_s"]',
        '.aui-menu>div>*',
      )
      .then(() => {
        const bluegreempage = new BlueGreenPage();
        bluegreempage.fillForm(data);
        if (flag === '确定') {
          return bluegreempage.rootPage.clickConfirm();
        } else if (flag === '取消') {
          return bluegreempage.rootPage.clickCancel();
        } else if (flag === '关闭') {
          return bluegreempage.rootPage.clickClose();
        }
      });
  }
}
