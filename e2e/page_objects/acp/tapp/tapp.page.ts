import { TappDetailVerify } from '@e2e/page_verify/acp/tapp/detail.page';
import { TappListVerify } from '@e2e/page_verify/acp/tapp/list.page';

import { AcpPageBase } from '../acp.page.base';

import { TappCreatePage } from './create.page';
import { TappDetailPage } from './detail.page';
import { TappListPage } from './list.page';
import { PreparePage } from './prepare';

export class TappPage extends AcpPageBase {
  get listPage(): TappListPage {
    return new TappListPage();
  }

  get createPage(): TappCreatePage {
    return new TappCreatePage();
  }

  get listPageVerify(): TappListVerify {
    return new TappListVerify();
  }
  get detailPageVerify(): TappDetailVerify {
    return new TappDetailVerify();
  }
  get detailPage(): TappDetailPage {
    return new TappDetailPage();
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
