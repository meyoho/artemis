import { $ } from 'protractor';

import { AuiConfirmDialog } from '../../../element_objects/alauda.aui_confirm_dialog';
import { AcpPageBase } from '../acp.page.base';

import { DomainListPage } from './list.page';

export class DeleteDomainPage extends AcpPageBase {
    get listPage(): DomainListPage {
        return new DomainListPage();
    }
    /**
     *  删除对话框
     */
    get deleteDialog() {
        return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
    }

    /**
     * 删除域名
     * @param name 名称
     * @param type 类型
     */
    deleteDomainFromList(name, type, flag = 'delete') {
        this.listPage.clickdelete(name, type);
        switch (flag) {
            case 'cancel':
                this.deleteDialog.clickCancel();
                break;
            default:
                this.deleteDialog.clickConfirm();
                break;
        }
    }
}
