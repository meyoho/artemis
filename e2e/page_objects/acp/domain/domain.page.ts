import { DomainListPageVerify } from '@e2e/page_verify/acp/domain/list.page';

import { AcpPageBase } from '../acp.page.base';

import { CreateDomainPage } from './create.page';
import { DeleteDomainPage } from './delete.page';
import { DomainListPage } from './list.page';
import { PreparePage } from './prepare.page';
import { UpdateDomainPage } from './update.page';

export class DomainPage extends AcpPageBase {
    get preparePage(): PreparePage {
        return new PreparePage();
    }
    /**
     * 创建页
     */
    get createPage() {
        return new CreateDomainPage();
    }
    /**
     * 更新页
     */
    get updatePage() {
        return new UpdateDomainPage();
    }
    /**
     * 删除页
     */
    get deletePage() {
        return new DeleteDomainPage();
    }

    get listPage(): DomainListPage {
        return new DomainListPage();
    }

    get listPageVerify(): DomainListPageVerify {
        return new DomainListPageVerify();
    }
}
