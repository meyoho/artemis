import { $ } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AcpPageBase } from '../acp.page.base';

export class DomainListPage extends AcpPageBase {
    /**
     * 域名管理列表
     */
    get domainNameTable() {
        return new AuiTableComponent(
            $('.aui-card .aui-card__content'),
            '.aui-card__content aui-table',
            '.aui-card__content .alo-search'
        );
    }
    /**
     * 创建域名按钮
     */
    clickadd() {
        const button = this.getButtonByText('创建域名');
        button.click();
    }
    /**
     * 点击操作栏的删除按钮
     * @param name 名称
     * @param type 类型
     */
    clickdelete(name: string, type: string) {
        this.domainNameTable.clickOperationButtonByRow(
            [name, type],
            '删除',
            'aui-table-row aui-icon'
        );
    }
    /**
     * 点击操作栏的更新按钮
     * @param name 名称
     * @param type 类型
     */
    clickupdate(name: string, type: string) {
        this.domainNameTable.clickOperationButtonByRow(
            [name, type],
            '更新',
            'aui-table-row aui-icon'
        );
    }
}
