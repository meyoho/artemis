import { CreateDomainPage } from './create.page';

export class UpdateDomainPage extends CreateDomainPage {
    /**
     * 更新域名
     * @param name 名称
     * @param type 类型
     * @param testData 更新数据
     */
    updateDomain(name, type, testData, flag = 'update') {
        this.listPage.clickupdate(name, type);
        this.fillForm(testData);
        switch (flag) {
            case 'cancel':
                this.auiDialog.clickCancel();
                break;
            case 'close':
                this.auiDialog.clickClose();
                break;
            default:
                this.auiDialog.clickConfirm();
                break;
        }
    }
}
