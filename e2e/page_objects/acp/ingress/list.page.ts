import { $, by, element } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { AcpPageBase } from '../acp.page.base';

export class IngressListPage extends AcpPageBase {
  /**
   * 访问规则管理列表
   */
  get ingressNameTable() {
    return new AuiTableComponent(
      $('aui-card .aui-card'),
      '.aui-card__content>aui-table',
      'aui-search .aui-search',
      'aui-table-cell',
      '.aui-card__content>aui-table>aui-table-row',
    );
  }
  /**
   * 检索框
   */
  get search() {
    return new AuiSearch($('.aui-card__header aui-search'));
  }
  get noData() {
    const empty = $('.empty-placeholder');
    this.waitElementPresent(empty, '【 无访问规则】控件没出现');
    return empty;
  }
  /**
   * 创建访问规则按钮
   */
  clickadd() {
    this.getButtonByText('创建访问规则').click();
  }
  /**
   * 点击名称链接
   * @param name 名称
   */
  clickview(name: string) {
    this.searchIngress(name);
    this.ingressNameTable.clickResourceNameByRow([name], '路由名称');
  }
  /**
   * 点击操作栏的删除按钮
   * @param name 名称
   */
  clickdelete(name: string) {
    this.searchIngress(name);
    this.ingressNameTable.clickOperationButtonByRow(
      [name],
      '删除',
      'aui-table-row aui-icon',
    );
  }
  /**
   * 点击操作栏的更新按钮
   * @param name 名称
   */
  clickupdate(name: string) {
    this.searchIngress(name);
    this.ingressNameTable.clickOperationButtonByRow(
      [name],
      '更新',
      'aui-table-row aui-icon',
    );
  }

  /**
   * 查看访问规则
   * @param name 名称
   */
  viewIngress(name) {
    this.clickview(name);
    this.waitElementPresent(
      element(by.css('aui-card .detail-content')),
      '',
      1000,
    );
  }
  /**
   * 搜索访问规则
   * @param name 名称
   * @param count 预期结果行数
   */
  searchIngress(name, count = 1) {
    this.clickLeftNavByText('访问规则');
    this.search.search(name);
    this.ingressNameTable.waitRowCountChangeto(count);
  }
  /**
   * 从列表页点击名称进入详情页
   * @param name 名称
   */
  enterDetailIngress(name) {
    this.searchIngress(name);
    this.ingressNameTable.clickResourceNameByRow([name], '路由名称');
  }
  /**
   * 列表页更新访问规则
   * @param name 名称
   * @param testData 更新数据
   */
  updateIngress(name, testData, flag = 'delete') {
    this.searchIngress(name);
    this.clickupdate(name);
    this.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.clickCancel();
        break;
      default:
        this.clickConfirm();
        break;
    }
  }
}
