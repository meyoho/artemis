import { DetailIngressPageVerify } from '@e2e/page_verify/acp/ingress/detail.page';
import { IngressListPageVerify } from '@e2e/page_verify/acp/ingress/list.page';

import { AcpPageBase } from '../acp.page.base';

import { CreateIngressPage } from './create.page';
import { DeleteIngressPage } from './delete.page';
import { DetailIngressPage } from './detail.page';
import { IngressListPage } from './list.page';
import { PreparePage } from './prepare.page';
import { UpdateIngressPage } from './update.page';

export class IngressPage extends AcpPageBase {
    get preparePage(): PreparePage {
        return new PreparePage();
    }

    /**
     * 创建页
     */
    get createPage() {
        return new CreateIngressPage();
    }
    /**
     * 详情页
     */
    get viewPage() {
        return new DetailIngressPage();
    }

    get detailPageVerify(): DetailIngressPageVerify {
        return new DetailIngressPageVerify();
    }
    /**
     * 更新页
     */
    get updatePage() {
        return new UpdateIngressPage();
    }
    /**
     * 删除页
     */
    get deletePage() {
        return new DeleteIngressPage();
    }

    get listPage(): IngressListPage {
        return new IngressListPage();
    }

    get listPageVerify(): IngressListPageVerify {
        return new IngressListPageVerify();
    }
}
