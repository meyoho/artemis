import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { $, $$, by } from 'protractor';

import { AlaudaAuiCodeEditor } from '../../../element_objects/alauda.aui_code_editor';
import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaTabItem } from '../../../element_objects/alauda.tabitem';

import { IngressListPage } from './list.page';
import { UpdateIngressPage } from './update.page';

export class DetailIngressPage extends IngressListPage {
  get name() {
    this.waitElementPresent(
      $('aui-card:first-child .aui-card__header'),
      '等待进入访问规则详情页',
    );
    return $('aui-card:first-child .aui-card__header').getText();
  }
  get updatePage(): UpdateIngressPage {
    return new UpdateIngressPage();
  }
  get listPage(): IngressListPage {
    return new IngressListPage();
  }
  get duplicatedDiaglog() {
    return new AuiDialog($('.cdk-overlay-pane aui-dialog'));
  }
  /**
   * 详情页tabs
   */
  tabs(name) {
    return new AlaudaTabItem(
      by.xpath("//div[contains(@class,'aui-tab-label')]"),
      by.xpath("//div[contains(@class,'aui-tab-label isActive')]"),
      by.xpath(
        "//div[contains(@class,'aui-tab-label')]/div[contains(text(),'" +
          name +
          "')]",
      ),
    );
  }
  /**
   * 切换tab
   */
  switchtab(name) {
    this.tabs(name).click();
  }

  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.detail-content .detail-item',
      '.detail-item label',
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'div'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }
  /**
   * 获取详情页的规则
   */
  get rules() {
    return new AlaudaAuiTable($('rc-ingress-rules-display aui-table'));
  }
  /**
   * 获取详情页的https证书
   */
  get tls() {
    return new AlaudaAuiTable($('rc-ingress-detail-tls aui-table'));
  }
  get notls() {
    const noData = $('aui-card .empty-placeholder');
    this.waitElementPresent(noData, '【无HTTPS 证书】 控件没出现');
    return noData;
  }
  /**
   * 获取Tab yaml页
   */
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('aui-tab-body aui-code-editor'));
  }
  /**
   * 详情页的操作按钮
   */
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header .aui-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }

  /**
   * 点击详情页的更新
   */
  clickupdate() {
    this.action.select('更新');
  }
  /**
   * 点击详情页的删除
   */
  clickdelete() {
    this.action.select('删除');
  }

  /**
   * 详情页-操作-更新
   * @param testData 数据
   */
  updateIngressFromDetail(name, testData) {
    this.listPage.viewIngress(name);
    this.clickupdate();
    this.updatePage.fillForm(testData);
    this.updatePage.clickConfirm();
  }

  /**
   * 详情页-操作-删除
   */
  deleteIngressFromDetail(name) {
    this.listPage.viewIngress(name);
    this.clickdelete();
    this.confirmDialog.clickConfirm();
  }
}
