import { CommonKubectl } from '@e2e/utility/common.kubectl';
// import { CommonMethod } from '@e2e/utility/common.method';

import { AcpPreparePage } from '../prepare.page';

export class PreparePage extends AcpPreparePage {
    /**
     * 删除 Ingress
     * @param name 名称
     * @param namespace 命名空间
     */
    deleteIngress(name, namespace) {
        const cmd = `kubectl delete ingress -n ${namespace} ${name}`;
        CommonKubectl.execKubectlCommand(cmd, this.clusterName);
    }
}
