import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, $$, browser, ElementFinder, element, by } from 'protractor';

import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

import { IngressListPage } from './list.page';

export class CreateIngressPage extends IngressListPage {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'form .aui-form-item',
      'form .aui-form-item__label',
    );
  }

  _getRuleElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}") ]/ancestor::aui-form-item[1]//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '名称':
        return this.alaudaElement.getElementByText(text, 'input');
      case '域名':
        return this._getRuleElementByText(text, 'aui-select');
      case '域名前缀':
        return this._getRuleElementByText(
          '域名',
          'input[@formcontrolname="subDomain"]',
        );
      case 'HTTPS':
        return this._getRuleElementByText(text, 'aui-select');
      case '规则':
        return this._getRuleElementByText(
          text,
          'rc-ingress-paths-form/rc-array-form-table',
        );
    }
    return super.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '域名':
        new AlaudaDropdown(
          this.getElementByText(name),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        ).select(value);
        break;
      case '域名前缀':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case 'HTTPS':
        new AlaudaDropdown(
          this.getElementByText(name),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        ).select(value);
        break;
      case '规则':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
    }
    browser.sleep(100);
  }

  /**
   * 添加访问规则
   * @param testData 创建数据
   */
  addIngress(testData, flag = 'add') {
    this.clickLeftNavByText('访问规则');
    this.clickadd();
    this.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.clickCancel();
        break;
      default:
        this.clickConfirm();
        break;
    }
  }
  get createTypeRadio() {
    return new AlaudaRadioButton($('.aui-card__header aui-radio-group'));
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-dialog__content aui-code-editor'));
  }
  /**
   * 添加访问规则
   * @param testData 创建数据
   */
  addIngressByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('访问规则');
    this.clickadd();
    this.createTypeRadio.clickByName('YAML');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        this.clickCancel();
        break;
      default:
        this.clickConfirm();
        break;
    }
  }
}
