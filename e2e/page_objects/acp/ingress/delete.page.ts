import { $ } from 'protractor';

import { AuiConfirmDialog } from '../../../element_objects/alauda.aui_confirm_dialog';

import { IngressListPage } from './list.page';

export class DeleteIngressPage extends IngressListPage {
  /**
   *  删除对话框
   */
  get deleteDialog() {
    return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
  }
  /**
   * 列表页删除访问规则
   * @param name 名称
   */
  deleteIngress(name, flag = 'delete') {
    this.searchIngress(name);
    this.clickdelete(name);
    switch (flag) {
      case 'cancel':
        this.deleteDialog.clickCancel();
        break;
      default:
        this.deleteDialog.clickConfirm();
        break;
    }
  }
}
