import { AcpPageBase } from '@acp/acp.page.base';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import {
  $,
  ElementFinder,
  ExpectedConditions,
  browser,
  by,
  element,
  promise,
  until,
  $$,
} from 'protractor';

import { DetailDaemonSetPage } from '../daemonset/detail.page';
import { DetailDeploymentPage } from '../deployment/detail.page';
import { DetailStatefulSetPage } from '../statefulset/detail.page';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { AppListPage } from './app.list.page';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
export class AppDetailPage extends AcpPageBase {
  get listPage() {
    return new AppListPage();
  }
  get name() {
    this.waitElementPresent(
      $('rc-app-detail-main>aui-card .aui-card__header'),
      '应用名称',
    );
    return $('rc-app-detail-main>aui-card .aui-card__header');
  }
  get alaudaElement() {
    return new AlaudaElement('rc-field-set-item', '.field-set-item__label');
  }
  get snapShotDiaglog() {
    return new AuiDialog($('aui-dialog'));
  }
  get Yaml() {
    this.getTab('YAML').click();
    return this.waitElementPresent(
      $('ng-monaco-editor>.ng-monaco-editor-container'),
      '等待yaml内容加载失败',
      10000,
    ).then(ispresent => {
      if (ispresent) {
        return new AlaudaYamlEditor();
      } else {
        throw new Error('Yaml 内容加载失败');
      }
    });
  }
  /**
   * 获取应用详情tab页
   * @param tabName
   */
  getTab(tabName: string) {
    const tab = $('.aui-page__content aui-tab-header')
      .$$('.aui-tab-label')
      .filter(ele => {
        return ele.getText().then(text => {
          return text.trim() === tabName;
        });
      })
      .first();
    browser.wait(
      ExpectedConditions.visibilityOf(tab),
      5000,
      'application details page not present',
    );
    return tab;
  }
  /**
   * 根据卡片头部名获取卡片
   * @param header 卡片头部名
   */
  getCardByHeader(header: string) {
    this.waitElementPresent(
      $('.aui-page__content aui-tab-body aui-card'),
      '等待aui-card出现',
    );
    return $('.aui-page__content aui-tab-body')
      .$$('aui-card')
      .filter(ele => {
        return ele
          .$('.aui-card__header')
          .getText()
          .then(text => {
            if (text.includes(header)) {
              return true;
            }
          });
      })
      .first();
  }
  /**
   * 获取组件详情页中的meta数据
   * @param header 卡片名
   * @param itemName 标签名
   * @param index 索引值
   */
  getMetaItem(header: string, itemName: string, index = 1) {
    const card = this.getCardByHeader(header);
    const item = card
      .$$('.meta__detail__item')
      .filter(ele => {
        return ele
          .$('label')
          .getText()
          .then(text => {
            return text.trim() === itemName;
          });
      })
      .first();
    return item.element(by.xpath(`child::div[${index}]`));
  }
  /**
   * 获取组件详情页元数据卡片中的按钮
   * @param header 卡片名
   * @param itemName 标签名
   * @param index 索引值
   */
  getMetaItemBtn(header: string, itemName: string, index = 1) {
    return this.getMetaItem(header, itemName, index).$('aui-icon');
  }
  /**
   * 展示pod的label
   * @param header 卡片名
   * @param itemName 标签名
   * @param index 索引值
   */
  showLabels(): promise.Promise<string> {
    const tagLabels = new AlaudaTagsLabel(
      element(
        by.xpath(
          '//label[contains(text(),"容器组标签")]/parent::div//rc-tags-label',
        ),
      ),
    );
    return tagLabels.getText();
  }
  /**
   * 获取应用的状态
   * @param header 卡片名
   * @param status 应用状态
   */
  appStatus(header: string, status: string) {
    const card = this.getCardByHeader(header);
    this.waitElementPresent(
      element(
        by.xpath(
          `//rc-app-status/rc-status-icon/span[contains(text(),"${status}")]`,
        ),
      ),
      `等待应用${status}`,
      120000,
    );
    return card.$('rc-app-status>rc-status-icon>span');
  }
  /**
   * 获取资源, eg: 获取应用详情页中，Deployment类型组件下的容器信息
   * @param header 卡片名
   * @param resourceName 组件名称
   */
  getResourceHeader(header: string, resourceName: string) {
    const card = this.getCardByHeader(header);
    return card
      .$$('.resource__header')
      .filter(ele => {
        return ele
          .$('a')
          .getText()
          .then(text => {
            return text.trim() === resourceName;
          });
      })
      .first();
  }
  /**
   * 根据卡片名、资源名确定资源名称的位置
   * @param header 卡片名
   * @param resourceName 组件名称
   */
  getResource(header: string, resourceName: string): ElementFinder {
    return this.getResourceHeader(header, resourceName).$('a');
  }
  /**
   * 获取计算组件的状态, eg: 获取应用详情页中，Deployment类型组件的运行状态
   * @param header 卡片名
   * @param resourceName 组件名称
   */
  getResourceStatus(header: string, resourceName: string) {
    const resource = this.getResourceHeader(header, resourceName);
    return resource.$('rc-workload-status-icon aui-icon+span');
  }
  /**
   * 获取计算组件的workload
   * @param header 卡片名
   * @param resourceName 组件名称
   */
  getResourceWorkload(header: string, resourceName: string) {
    return this.getResourceHeader(header, resourceName).$('rc-workload-status');
  }
  /**
   * 获取计算组件的内容, eg: 获取应用详情页中，Deployment类型组件的容器镜像信息、容器大小、exec、日志
   * @param header
   * @param resourceName
   */
  getResourceContent(header: string, resourceName: string) {
    return this.getResourceHeader(header, resourceName).element(
      by.css('.resource__content'),
    );
  }
  /**
   * 根据卡片的名字和按钮的名字获取按钮
   * @param header 卡片名
   * @param btnName 按钮名
   */
  getBtn(header: string, btnName: string) {
    const card = this.getCardByHeader(header);
    const btn = card
      .$$('button')
      .filter(ele => {
        return ele.getText().then(text => {
          return text.includes(btnName);
        });
      })
      .first();
    return btn;
  }
  /**
   * 等待按钮不可点击
   * @param header 卡片名
   * @param btnName 按钮名
   */
  waitBtnNotClicked(header: string, btnName: string) {
    return browser.driver
      .wait(function() {
        return until.elementIsDisabled(
          this.getBtn(header, btnName).getWebElement(),
        );
      }, 20000)
      .then(
        () => {
          return true;
        },
        error => {
          console.log(error);
          return false;
        },
      );
  }
  /**
   * 等待按钮可点击
   * @param header 卡片名
   * @param btnName 按钮名
   */
  waitBtnClicked(header: string, btnName: string) {
    return browser.driver
      .wait(function() {
        return until.elementIsEnabled(
          this.getBtn(header, btnName).getWebElement(),
        );
      }, 20000)
      .then(
        () => {
          return true;
        },
        error => {
          console.log(error);
          return false;
        },
      );
  }
  /**
   * 根据镜像地址获取镜像名称
   * @param imageAddress 镜像地址
   */
  getImageName(imageAddress: string) {
    const list = imageAddress.split('/');
    return list[list.length - 1]
      .split(':')[0]
      .replace(/[^0-9a-zA-Z-]/g, '-')
      .replace(/(^-{1,})|(-{1,}$)/g, '')
      .toLocaleLowerCase();
  }
  /**
   * 进入计算组件详情页
   * @param header 计算组件类型: Deployment/StatefulSet/DaemonSet
   * @param resourceName 资源名称
   */
  toResourceDetailPage(header: string, resourceName: string) {
    const ele = this.getResource(header, resourceName);
    ele.click();
    this.waitElementPresent(
      element(
        by.xpath(
          `//div[@class="aui-card__header"]/span[contains(text(),"${resourceName}")]`,
        ),
      ),
      `没有进入计算组件${header}:${resourceName}详情页`,
    );
  }
  get auiDialog() {
    return new AuiDialog(
      $('aui-confirm-dialog'),
      '.aui-confirm-dialog__content',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
    );
  }
  /**
   * 停止应用
   * @param appName 应用名称
   */
  stopApp(appName) {
    // expect(this.waitBtnNotClicked(appName, '开始')).toBeTruthy(
    //     'the【开始】button still can be clicked'
    // );
    // expect(this.waitBtnClicked(appName, '停止')).toBeTruthy(
    //     'the【停止】button can not be clicked'
    // );
    this.getBtn(appName, '停止').click();
    this.auiDialog.clickConfirm();
    this.waitElementPresent(
      element(
        by.xpath(
          '//rc-app-status/aui-tag/div/span/div/span[contains(text(),"处理中")]',
        ),
      ),
      '等待应用进入处理中',
    );
    this.waitElementNotPresent(
      element(
        by.xpath(
          '//rc-app-status/aui-tag/div/span/div/span[contains(text(),"处理中")]',
        ),
      ),
      '等待应用进人Stoped',
      30000,
    );
  }
  /**
   * 启动应用
   * @param appName 应用名称
   */
  startApp(appName) {
    // expect(this.waitBtnNotClicked(appName, '停止')).toBeTruthy(
    //     'the【停止】button still can be clicked'
    // );
    // expect(this.waitBtnClicked(appName, '开始')).toBeTruthy(
    //     'the【开始】button can not be clicked'
    // );
    this.getBtn(appName, '开始').click();
    this.auiDialog.clickConfirm();
    this.waitElementPresent(
      element(
        by.xpath(
          '//rc-app-status/aui-tag/div/span/div/span[contains(text(),"处理中")]',
        ),
      ),
      '等待应用进入处理中',
    );
    this.waitElementNotPresent(
      element(
        by.xpath(
          '//rc-app-status/aui-tag/div/span/div/span[contains(text(),"处理中")]',
        ),
      ),
      '等待应用进人Running',
      30000,
    );
  }
  /**
   *
   * @param name 计算组件类型 Deployment/StatefulSet/DaemonSet
   */
  getWorkloadPage(type_name) {
    if (type_name === 'Deployment') {
      return new DetailDeploymentPage();
    } else if (type_name === 'StatefulSet') {
      return new DetailStatefulSetPage();
    } else if (type_name === 'DaemonSet') {
      return new DetailDaemonSetPage();
    }
  }
  get otherResourceTable() {
    return new AuiTableComponent(
      $('rc-application-resources'),
      '.aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  /**
   * 获取容器组页
   */
  get podListTable() {
    return new AuiTableComponent(
      $('rc-pod-list .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  get historyListTable() {
    return new AuiTableComponent(
      $('rc-application-history .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  createSnapShot(comment = '') {
    this.getTab('历史版本')
      .click()
      .then(() => {
        this.getButtonByText('创建版本快照').click();
        if (comment.length > 0) {
          new AlaudaInputbox(this.snapShotDiaglog.content.$('textarea')).input(
            comment,
          );
        }
        this.snapShotDiaglog.clickConfirm();
        browser.sleep(3000);
      });
  }
  createSnapShotCancel(comment = '') {
    this.getTab('历史版本')
      .click()
      .then(() => {
        this.getButtonByText('创建版本快照').click();
        if (comment.length > 0) {
          new AlaudaInputbox(this.snapShotDiaglog.content.$('textarea')).input(
            comment,
          );
        }
        this.snapShotDiaglog.clickCancel();
      });
  }
  createSnapShotClose(comment = '') {
    this.getTab('历史版本')
      .click()
      .then(() => {
        this.getButtonByText('创建版本快照').click();
        if (comment.length > 0) {
          new AlaudaInputbox(this.snapShotDiaglog.content.$('textarea')).input(
            comment,
          );
        }
        this.snapShotDiaglog.clickClose();
      });
  }
  rollOut() {
    this.getTab('历史版本').click();
    const row = this.historyListTable.getRowByIndex(1);
    new AlaudaButton(row.$('aui-table-cell button')).click();
    return this.auiDialog.clickConfirm();
  }
  getElementByText_xpath(left: string, tagname = 'input', root = '/'): any {
    const xpath = `${root}/rc-field-set-item//label[normalize-space(text())="${left}" ]/ancestor::rc-field-set-item//${tagname}`;
    const temp = element(by.xpath(xpath));
    temp.isPresent().then(isPresent => {
      if (!isPresent) {
        console.log(
          `根据左侧文字[${left}], 没有找到右侧控件 ${tagname}\n${temp.locator()}`,
        );
      }
    });
    return temp;
  }
  changeImageTag(tag, click = 'confirm') {
    new AlaudaButton(
      this.getElementByText_xpath('镜像', 'acl-disabled-container').$(
        'aui-icon[icon="basic:pencil_s"]',
      ),
    )
      .click()
      .then(() => {
        const ele = new AlaudaElement(
          'aui-form-item',
          '.aui-form-item__label',
          this.snapShotDiaglog.content,
        );
        new AlaudaInputbox(ele.getElementByText('镜像版本', 'input'))
          .input(tag)
          .then(() => {
            if (click === 'x') {
              this.snapShotDiaglog.clickClose();
            } else if (click === 'cancel') {
              this.snapShotDiaglog.clickCancel();
            } else {
              this.snapShotDiaglog.clickConfirm();
            }
          });
      });
  }
  clickReplicaSet(header, rs_name, button_text = '-') {
    this.getResourceWorkload(header, rs_name)
      .$('.rc-workload-status__total>span')
      .getText()
      .then(text => {
        if (button_text === '-') {
          new AlaudaButton(
            this.getResourceWorkload(header, rs_name).$(
              'rc-workload-status .rc-workload-status__scaler aui-icon[icon="basic:minus"]',
            ),
          ).click();
          this.waitElementTextChangeTo(
            this.getResourceWorkload(header, rs_name).$(
              '.rc-workload-status__total>span',
            ),
            new String(Number.parseInt(text) - 1),
          );
        } else if (button_text === '+') {
          new AlaudaButton(
            this.getResourceWorkload(header, rs_name).$(
              'rc-workload-status .rc-workload-status__scaler aui-icon[icon="basic:plus"]',
            ),
          ).click();
          this.waitElementTextChangeTo(
            this.getResourceWorkload(header, rs_name).$(
              '.rc-workload-status__total>span',
            ),
            new String(Number.parseInt(text) + 1),
          );
        }
        this.conflictRetry();
      });
  }
  /**
   * 点击操作
   * @param operation 操作按钮文字
   */
  clickOperation(operation) {
    return new AlaudaDropdown(
      $('rc-app-detail-header>button'),
      $$('aui-tooltip aui-menu-item'),
    ).select(operation);
  }
  /**
   * 删除应用
   */
  deleteApp(app_name) {
    this.clickOperation('删除').then(() => {
      this.confirmDialog.content.getText().then(text => {
        expect(text.trim()).toBe(`确认要删除应用 ${app_name} 吗？`);
      });
      this.confirmDialog.clickConfirm();
    });
  }
  /**
   * 应用联邦化
   */
  federatedApp() {
    this.clickOperation('应用联邦化');
    new AlaudaButton(
      $('.aui-dialog__footer button[aui-button="primary"]'),
    ).click();
    this.waitElementNotPresent(
      $('aui-dialog'),
      '应用联邦化对话框未关闭',
      70000,
    ).then(present => {
      if (!present) {
        new AlaudaButton(
          $('.aui-dialog__footer button[aui-button=""]'),
        ).click();
      }
    });
  }
}
