import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder, promise } from 'protractor';

export class ResourcePreview extends AlaudaElementBase {
  private root: ElementFinder;
  private row_selector;
  private label_selector;
  private value_selector;
  constructor(
    root: ElementFinder,
    row_selector = '.rc-table-vertical__row',
    label_selector = 'div:nth-last-child(2)',
    value_selector = 'div:nth-child(2)',
  ) {
    super();
    this.root = root;
    this.row_selector = row_selector;
    this.label_selector = label_selector;
    this.value_selector = value_selector;
  }
  get isPresent() {
    return this.root.isPresent();
  }
  get_value_by_key(key: string): promise.Promise<string> {
    return this.get_value_ele_by_key(key).getText();
  }
  private get_value_ele_by_key(key: string): ElementFinder {
    const rows = this.root.$$(this.row_selector).filter(row => {
      return row
        .$(this.label_selector)
        .getText()
        .then(text => {
          // console.log(`key: ${key}: text ${text}`);
          return text === key;
        });
    });
    return rows.first().$(this.value_selector);
  }
  get_button(name: string): AlaudaButton {
    switch (name) {
      case '编辑':
        return new AlaudaButton(
          this.root.$('button aui-icon[icon="basic:pencil_edit"]'),
        );
      case '删除':
        return new AlaudaButton(
          this.root.$('button aui-icon[icon="basic:minus_circle"]'),
        );
    }
  }
  get_table(key: string): promise.Promise<AlaudaAuiTable> {
    return this.get_value_ele_by_key(key)
      .$('table')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          return new AlaudaAuiTable(
            this.get_value_ele_by_key(key).$('table'),
            'thead>tr>td',
            'tbody>tr>td',
            'tbody>tr',
          );
        } else {
          return new AlaudaAuiTable(
            this.get_value_ele_by_key(key).$('aui-table'),
            'aui-table-header-row>aui-table-header-cell',
            'aui-table-row>aui-table-cell',
            'aui-table-row',
          );
        }
      });
  }
  click_button(name: string) {
    return this.get_button(name).click();
  }
}
