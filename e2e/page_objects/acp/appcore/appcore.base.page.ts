import { RcFoldableBlock } from '@acp/appcore/rc.foldable.block';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import {
    $,
    $$,
    ElementFinder,
    ExpectedConditions,
    browser,
    by
} from 'protractor';

export class AppcoreCreateBase extends AlaudaElementBase {
    private _card = 'aui-card .aui-card';
    /**
     * 下拉单选控件
     * @param auiSelect ElementFinder
     * @param auiToolTip ElementFinder
     */
    auiselect(auiSelect: ElementFinder, auiToolTip: ElementFinder) {
        return new AuiSelect(auiSelect, auiToolTip);
    }
    /**
     * 下拉多选控件
     * @param auiSelect ElementFinder
     * @param auiToolTip ElementFinder
     */
    auiMultiSelect(auiSelect: ElementFinder, auiToolTip: ElementFinder) {
        return new AuiMultiSelect(auiSelect, auiToolTip);
    }
    /**
     *
     *
     */
    rcFoldableBlock(selector) {
        return new RcFoldableBlock(selector);
    }
    /**
     * 根据标题确定卡片
     * @title 卡片的名称
     */
    getCardByTitle(title: string) {
        const card = $$(this._card)
            .filter(ele => {
                return ele
                    .$('.aui-card__header')
                    .getText()
                    .then(text => {
                        return text.trim() === title;
                    });
            })
            .first();
        this.waitElementPresent(card, 20000);
        return card;
    }
    /**
     * 获取内部路由卡片
     */
    getServiceCard() {
        const card = $('rc-services-form');
        this.waitElementPresent(card, 20000);
        return card;
    }
    /**
     * 获取button
     * @param btnName
     */
    getBtn(btnName: string) {
        const btn = $('.resource-form__footer')
            .$$('button')
            .filter(ele => {
                return ele
                    .$('span')
                    .getText()
                    .then(t => {
                        return t.trim() === btnName;
                    });
            })
            .first();
        return btn;
    }
    /**
     * 根据文本获取元素
     * @title 卡片名称
     * @text 标签名
     */
    getElementByText(title: string, text: string) {
        const item = this.getCardByTitle(title)
            .$$('aui-form-item')
            .filter(ele => {
                return ele
                    .$('.aui-form-item__label')
                    .getText()
                    .then(t => {
                        return t.trim() === text;
                    });
            })
            .first();
        return item.$('.aui-form-item__content').$$('input');
    }
    /**
     * 根据文本获取容器组下的元素
     * @title 卡片名
     * @text 标签名
     */
    getElementUnderGroup(title: string, text: string) {
        const g = this.getCardByTitle(title)
            .$('rc-pod-template-form rc-container-form')
            .$$('aui-form-item')
            .filter(ele => {
                return ele
                    .$('.aui-form-item__label')
                    .getText()
                    .then(t => {
                        return t.trim() === text;
                    });
            })
            .first();
        return g.$('.aui-form-item__content input');
    }
    /**
     * 展开/收起 组件-高级、容器组-高级
     */
    upDownAdvance(text: string) {
        const advance = $$('rc-foldable-block .foldable-trigger')
            .filter(ele => {
                return ele
                    .$('div:nth-child(1)')
                    .getText()
                    .then(t => {
                        return t.trim() === text;
                    });
            })
            .first();
        return advance.$('aui-icon .aui-icon');
    }
    /**
     * 根据左侧文本确定组件
     */
    getComponentByText(text: string) {
        const component = $$('aui-form-item')
            .filter(ele => {
                return ele
                    .$('.aui-form-item__label')
                    .getText()
                    .then(t => {
                        return t.trim() === text;
                    });
            })
            .first();
        return component.$('.aui-form-item__content');
    }
    /**
     * 点击添加按钮
     * @text 文本名
     * @selector ElementFinder
     */
    clickAddButton(text: string, selector: string = '.aui-button--primary') {
        return this.getComponentByText(text)
            .$(selector)
            .click();
    }
    /**
     * 无数据时的提示信息
     * @text 标签名
     */
    elementPresent(text: string) {
        return this.getComponentByText(text).$('.zero-state-hint');
    }
    /**
     * 输入值, 适用于组件标签、启动命令、环境变量、日志、排除日志、更新策略
     */
    enterValue(text: string, parameters: Array<string>) {
        const _this = this;
        parameters.forEach(val => {
            _this
                .getComponentByText(text)
                .$$('tr.rc-array-form-table--row-group-start')
                .each(function(ele) {
                    ele.$$('input')
                        .count()
                        .then(count => {
                            if (count === val.length && count === 2) {
                                ele.$$('input').each(function(e, index) {
                                    e.clear();
                                    e.sendKeys(val[index]);
                                });
                            } else if (count === val.length && count === 3) {
                                ele.$$('input').each(function(e, index) {
                                    if (index === 0) {
                                        e.clear();
                                        e.sendKeys(val[index]);
                                    } else {
                                        if (
                                            Array.isArray(val[index]) &&
                                            val[index].length > 1
                                        ) {
                                            _this
                                                .auiselect(
                                                    e.element(by.xpath('..')),
                                                    $(
                                                        '.cdk-overlay-pane aui-tooltip'
                                                    )
                                                )
                                                .select(
                                                    val[index][0],
                                                    val[index][1]
                                                );
                                        } else {
                                            _this
                                                .auiselect(
                                                    e.element(by.xpath('..')),
                                                    $(
                                                        '.cdk-overlay-pane aui-tooltip'
                                                    )
                                                )
                                                .select(val[index]);
                                        }
                                    }
                                });
                            } else if (count === val.length && count === 1) {
                                ele.$('input').clear();
                                ele.$('input').sendKeys(val[0]);
                            }
                        });
                });
        });
    }
    /**
     * 容器大小输入值
     * @param text 文本名
     * @param parameters 输入值
     */
    containerSize(text: string, parameters: Array<string>) {
        const _this = this;
        this.getComponentByText(text)
            .$$('.input-groups-wrapper')
            .each(function(ele, index) {
                ele.$('input[type="number"]').clear();
                ele.$('input[type="number"]').sendKeys(parameters[index][0]);
                _this
                    .auiselect(
                        ele.$('aui-select'),
                        $('.cdk-overlay-pane aui-tooltip')
                    )
                    .select(parameters[index][1]);
            });
    }
    /**
     * 删除元素，适用于组件标签、启动命令、环境变量、日志、排除日志
     */
    removeElement(text: string, index: number) {
        const row = this.getComponentByText(text)
            .$$('.rc-array-form-table--row-group-start')
            .get(index);
        return row.$('aui-icon .aui-icon').click();
    }
    /**
     * 展开容器的其它设置
     */
    upDownContainerSet() {
        return $('rc-foldable-block[mode="button"] aui-icon .aui-icon').click();
    }
    /**
     * 环境变量引用
     * @param text 文本名
     * @param parameters 输入参数
     */
    configReference(text: string, parameters: Array<string>) {
        const _this = this;
        if (parameters.length > 1) {
            this.auiMultiSelect(
                _this.getComponentByText(text),
                $('.cdk-overlay-pane aui-tooltip')
            ).select(parameters[0], parameters[1]);
        } else {
            this.auiMultiSelect(
                _this.getComponentByText(text),
                $('.cdk-overlay-pane aui-tooltip')
            ).select(parameters[0]);
        }
    }
    /**
     * 选择部署模式
     * @param label 标签名
     * @param text 模式：Deployment、Daemonset、Statefulset
     */
    getDeployType(): AlaudaRadioButton {
        return new AlaudaRadioButton(
            this.getComponentByText('部署模式').$(`aui-radio-group`)
        );
    }
    /**
     * 新增参数
     * @param label 文本名
     * @param parameters 参数值
     * @param selector css selector
     */
    insertParams(
        label: string,
        parameters: Array<string>,
        selector: string = '.aui-button--primary'
    ) {
        const _this = this;
        this.clickAddButton(selector);
        browser.wait(
            ExpectedConditions.stalenessOf(_this.elementPresent(label)),
            1000,
            'the input not present'
        );
        this.enterValue(label, parameters);
    }
    /**
     * 解析创建应用时提交的数据
     * @param data
     */
    parseData(data: Object) {
        const _this = this;
        for (const title in data) {
            if (title === '应用') {
                const subData = data[title];
                if (!Array.isArray(subData) && subData instanceof Object) {
                    for (const label in subData) {
                        if (label === '名称') {
                            _this
                                .getElementByText(title, label)
                                .get(0)
                                .sendKeys(subData[label]);
                        } else if (label === '显示名称') {
                            _this
                                .getElementByText(title, label)
                                .get(0)
                                .clear();
                            _this
                                .getElementByText(title, label)
                                .get(0)
                                .sendKeys(subData[label]);
                        }
                    }
                }
            } else if (title === '计算组件') {
                const subData = data[title];
                if (!Array.isArray(subData) && subData instanceof Object) {
                    for (const label in subData) {
                        if (label === '部署模式') {
                            _this.getDeployType().clickByName(subData[label]);
                        } else if (label === '实例数量' || label === '名称') {
                            _this
                                .getElementByText(title, label)
                                .count()
                                .then(count => {
                                    if (count > 1) {
                                        _this
                                            .getElementByText(title, label)
                                            .get(1)
                                            .clear();
                                        _this
                                            .getElementByText(title, label)
                                            .get(1)
                                            .sendKeys(subData[label]);
                                    } else {
                                        _this
                                            .getElementByText(title, label)
                                            .get(0)
                                            .clear();
                                        _this
                                            .getElementByText(title, label)
                                            .get(0)
                                            .sendKeys(subData[label]);
                                    }
                                });
                        } else if (label === '组件 - 高级') {
                            // this.scrollToBottom();
                            _this.upDownAdvance(label).click();
                            browser.executeScript('window.scrollTo(0,270)');
                            const rc = _this.rcFoldableBlock(
                                $$(
                                    '.aui-card__content rc-foldable-block:nth-child(4)'
                                ).first()
                            );
                            const podAdvance = subData[label];
                            if (
                                !Array.isArray(podAdvance) &&
                                podAdvance instanceof Object
                            ) {
                                for (const l in podAdvance) {
                                    if (l === '标签') {
                                        rc.clickAddButton(l);
                                        _this.enterValue(l, podAdvance[l]);
                                    } else if (l === '更新策略') {
                                        rc.updateStrategy(l, podAdvance[l]);
                                    } else if (l === 'Host模式') {
                                        if (podAdvance[l]) {
                                            rc.networkModul(l).open();
                                        }
                                    }
                                }
                            }
                            _this.upDownAdvance(label).click();
                        } else if (label === '容器组') {
                            this.scrollToBottom();
                            const containerGroup = subData[label];
                            if (
                                !Array.isArray(containerGroup) &&
                                containerGroup instanceof Object
                            ) {
                                for (const lab in containerGroup) {
                                    if (lab === '名称') {
                                        _this
                                            .getElementUnderGroup(title, lab)
                                            .clear();
                                        _this
                                            .getElementUnderGroup(title, lab)
                                            .sendKeys(containerGroup[lab]);
                                        //资源限制
                                    } else if (lab === '资源限制') {
                                        _this.containerSize(
                                            lab,
                                            containerGroup[lab]
                                        );
                                    } else if (lab === '其它') {
                                        _this.upDownContainerSet();
                                        const rc = _this.rcFoldableBlock(
                                            $(
                                                'rc-foldable-block[mode="button"]'
                                            )
                                        );
                                        const container = containerGroup[lab];
                                        if (
                                            !Array.isArray(container) &&
                                            container instanceof Object
                                        ) {
                                            for (const l in container) {
                                                if (l === '启动命令') {
                                                    rc.clickAddButton(l);
                                                    _this.enterValue(
                                                        l,
                                                        container[l]
                                                    );
                                                } else if (l === '参数') {
                                                    rc.clickAddButton(l);
                                                    _this.enterValue(
                                                        l,
                                                        container[l]
                                                    );
                                                } else if (l === '环境变量') {
                                                    const env = container[l];
                                                    if (
                                                        !Array.isArray(env) &&
                                                        env instanceof Object
                                                    ) {
                                                        for (const btn in env) {
                                                            if (
                                                                btn === '添加'
                                                            ) {
                                                                rc.clickAddButton(
                                                                    l
                                                                );
                                                                _this.enterValue(
                                                                    l,
                                                                    env[btn]
                                                                );
                                                            } else if (
                                                                btn ===
                                                                '添加引用'
                                                            ) {
                                                                rc.clickAddButton(
                                                                    l,
                                                                    '.aui-button--default'
                                                                );
                                                                _this.enterValue(
                                                                    l,
                                                                    env[btn]
                                                                );
                                                            }
                                                        }
                                                    }
                                                } else if (l === '配置引用') {
                                                    _this.configReference(
                                                        l,
                                                        container[l]
                                                    );
                                                } else if (l === '健康检查') {
                                                    browser.executeScript(
                                                        'window.scrollTo(0,document.body.scrollHeight)'
                                                    );
                                                    const health = container[l];
                                                    const dialog = _this.rcFoldableBlock(
                                                        $(
                                                            '.cdk-overlay-pane aui-dialog'
                                                        )
                                                    );
                                                    if (
                                                        !Array.isArray(
                                                            health
                                                        ) &&
                                                        health instanceof Object
                                                    ) {
                                                        for (const btn in health) {
                                                            if (
                                                                btn ===
                                                                '添加存活性健康检查'
                                                            ) {
                                                                rc.healthcheckBtn(
                                                                    l,
                                                                    btn
                                                                );
                                                                dialog.dialogForm(
                                                                    health[btn]
                                                                );
                                                                dialog
                                                                    .confirm()
                                                                    .click();
                                                            } else if (
                                                                btn ===
                                                                '添加可用性健康检查'
                                                            ) {
                                                                rc.healthcheckBtn(
                                                                    l,
                                                                    btn
                                                                );
                                                                dialog.dialogForm(
                                                                    health[btn]
                                                                );
                                                                dialog
                                                                    .confirm()
                                                                    .click();
                                                            }
                                                        }
                                                    }
                                                } else if (l === '存储') {
                                                    const volume = container[l];
                                                    const dialog = _this.rcFoldableBlock(
                                                        $(
                                                            '.cdk-overlay-pane aui-dialog'
                                                        )
                                                    );
                                                    if (
                                                        !Array.isArray(
                                                            volume
                                                        ) &&
                                                        volume instanceof Object
                                                    ) {
                                                        for (const tp in volume) {
                                                            if (
                                                                volume.hasOwnProperty(
                                                                    tp
                                                                )
                                                            ) {
                                                                rc.clickAddButton(
                                                                    l
                                                                );
                                                                dialog.dialogForm(
                                                                    volume[tp],
                                                                    tp
                                                                );
                                                                dialog
                                                                    .confirm()
                                                                    .click();
                                                            }
                                                        }
                                                    }
                                                } else if (l === '日志文件') {
                                                    browser.executeScript(
                                                        'window.scrollTo(0,document.body.scrollHeight)'
                                                    );
                                                    rc.clickAddButton(l);
                                                    _this.enterValue(
                                                        l,
                                                        container[l]
                                                    );
                                                } else if (
                                                    l === '排除日志文件'
                                                ) {
                                                    rc.clickAddButton(l);
                                                    _this.enterValue(
                                                        l,
                                                        container[l]
                                                    );
                                                }
                                            }
                                        }
                                        _this.upDownContainerSet();
                                    }
                                }
                            }
                        } else if (label === '容器组 - 高级') {
                            _this.upDownAdvance(label).click();
                            // this.scrollToBottom();
                            const rc = _this.rcFoldableBlock(
                                $(
                                    'rc-foldable-block[ng-reflect-hint="容器组 - 高级"]'
                                )
                            );
                            const containerAdvance = subData[label];
                            if (
                                !Array.isArray(containerAdvance) &&
                                containerAdvance instanceof Object
                            ) {
                                for (const l in containerAdvance) {
                                    if (l === '容器组标签') {
                                        rc.clickAddButton(l);
                                        _this.enterValue(
                                            l,
                                            containerAdvance[l]
                                        );
                                    } else if (l === '主机选择器') {
                                        _this.configReference(
                                            l,
                                            containerAdvance[l]
                                        );
                                    }
                                }
                            }
                            _this.upDownAdvance(label).click();
                        }
                    }
                }
            } else if (title === '内部路由') {
                _this
                    .getServiceCard()
                    .$('button')
                    .click();
                const dialog = _this.rcFoldableBlock(
                    $('.cdk-overlay-pane aui-dialog')
                );
                const service = data[title];
                if (!Array.isArray(service) && service instanceof Object) {
                    dialog.dialogForm(service);
                    dialog.confirm().click();
                }
            }
        }
    }
}
