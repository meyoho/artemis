import { AcpPageBase } from '../acp.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $ } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';

export class VolumeCreatePage extends AcpPageBase {
  get head() {
    return this.alaudaElement.root.$('.aui-dialog__header-title');
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form-item',
      'label',
      $('aui-dialog>.aui-dialog'),
    );
  }
  clickClose() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__header-close'),
    );
    return button.click();
  }
  clickConfirm() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__footer .aui-button--primary'),
    );
    return button.click();
  }
  clickCancel() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__footer .aui-button--default'),
    );
    return button.click();
  }
  getElementByText(text): AlaudaInputbox | AlaudaRadioButton | AuiSelect {
    switch (text) {
      case '名称':
      case '主机路径':
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'input'),
        );
      case '类型':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
          'aui-radio-button',
        );
      case '持久卷声明':
      case '配置字典':
      case '保密字典':
        return new AuiSelect(
          this.alaudaElement.getElementByText(text, 'aui-select'),
        );
    }
  }
  input(values) {
    for (const key in values) {
      switch (key) {
        case '名称':
        case '主机路径':
          const ele_1 = this.getElementByText(key) as AlaudaInputbox;
          ele_1.input(values[key]);
          break;
        case '类型':
          const ele_2 = this.getElementByText(key) as AlaudaRadioButton;
          ele_2.clickByName(values[key]);
          break;
        case '持久卷声明':
        case '配置字典':
        case '保密字典':
          const ele_3 = this.getElementByText(key) as AuiSelect;
          ele_3.select(values[key]);
          break;
      }
    }
  }
}
