import { AcpPageBase } from '@acp/acp.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ElementFinder } from 'protractor';

export class AppCreateBasic extends AcpPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this.waitElementPresent(root, '没有找到应用创建页，名称，显示名称控件');
  }

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-application-metadata-form aui-form-item>div',
      'label',
      this._root,
    );
  }

  input(data) {
    for (const key in data) {
      switch (key) {
        case '名称':
        case '显示名称':
          new AlaudaInputbox(this.alaudaElement.getElementByText(key)).input(
            data[key],
          );
          break;
      }
    }
  }
}
