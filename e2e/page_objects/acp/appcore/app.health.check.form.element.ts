import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { HealthCheckCreatePage } from './app.health.check.create.page';

export class HealthCheckElement extends AlaudaElement {
  //   private health_check_form: ElementFinder;
  private liveness_check_block;
  private readness_check_block;
  constructor(
    health_check_form,
    liveness_check_block = '.hc-row:nth-child(1)',
    readness_check_block = '.hc-row:nth-child(3)',
  ) {
    super();
    this._root = health_check_form;
    this.liveness_check_block = liveness_check_block;
    this.readness_check_block = readness_check_block;
    this._parent_css_selector = '.healthcheck-table-cell';
    this._left_css_selector = '.healthcheck-table-cell-label';
  }
  get livenessCheckTable() {
    return this.root.$(this.liveness_check_block);
  }
  get readnessCheckTable() {
    return this.root.$(this.readness_check_block);
  }
  click_add_button(name = '添加存活性健康检查') {
    this.scrollToView(this.root);
    return new AlaudaButton(
      this.root
        .$$('button')
        .filter(ele => {
          this.scrollToView(ele);
          return ele.getText().then(txt => {
            return txt.trim() === name;
          });
        })
        .first(),
    ).click();
  }
  input(data) {
    for (const key in data) {
      switch (key) {
        case '存活性健康检查':
          this.click_add_button();
          const health_check_page_a = new HealthCheckCreatePage();
          health_check_page_a.input(data[key]);
          health_check_page_a.clickConfirm();
          break;
        case '存活性健康检查x':
          this.click_add_button();
          const health_check_page_b = new HealthCheckCreatePage();
          health_check_page_b.input(data[key]);
          health_check_page_b.clickClose();
          break;
        case '存活性健康检查取消':
          this.click_add_button();
          const health_check_page_c = new HealthCheckCreatePage();
          health_check_page_c.input(data[key]);
          health_check_page_c.clickCancel();
          break;
        case '可用性健康检查':
          this.click_add_button('添加可用性健康检查');
          const health_check_page_2 = new HealthCheckCreatePage();
          health_check_page_2.input(data[key]);
          health_check_page_2.clickConfirm();
          break;
        case '可用性健康检查x':
          this.click_add_button('添加可用性健康检查');
          const health_check_page_3 = new HealthCheckCreatePage();
          health_check_page_3.input(data[key]);
          health_check_page_3.clickClose();
          break;
        case '可用性健康检查取消':
          this.click_add_button('添加可用性健康检查');
          const health_check_page_4 = new HealthCheckCreatePage();
          health_check_page_4.input(data[key]);
          health_check_page_4.clickCancel();
          break;
      }
    }
  }
}
