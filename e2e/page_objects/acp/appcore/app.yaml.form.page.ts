import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { by } from 'protractor';
import { isString } from 'util';

export class AppYamlForm extends AuiDialog {
  protected jsyaml = require('js-yaml');
  get YamlEditor() {
    return new AlaudaYamlEditor(
      by.css(`aui-dialog-content aui-code-editor`),
      by.xpath(
        `//aui-dialog-content//following::div[contains(@class,"aui-code-editor-toolbar__control-button")]`,
      ),
    );
  }
  dumpYaml(yamlObject) {
    return this.jsyaml.dump(yamlObject);
  }
  loadYaml(yamlString) {
    return this.jsyaml.safeLoad(yamlString);
  }
  inputYaml(yaml) {
    if (isString(yaml)) {
      this.YamlEditor.setYamlValue(yaml);
    } else {
      console.warn(`传入的对象不是string yaml，尝试转成string yaml`);
      const yaml_content = this.dumpYaml(yaml);
      this.YamlEditor.setYamlValue(yaml_content);
    }
  }
  get getStringYaml() {
    return this.YamlEditor.getYamlValue();
  }
  get getYaml() {
    return this.YamlEditor.getYamlValue().then(yaml => {
      return this.loadYaml(yaml) as Record<string, any>;
    });
  }
}
