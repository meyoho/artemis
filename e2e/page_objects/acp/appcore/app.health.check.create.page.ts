import { AcpPageBase } from '../acp.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $ } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';

export class HealthCheckCreatePage extends AcpPageBase {
  get head() {
    return this.alaudaElement.root.$('.aui-dialog__header-title');
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form-item',
      'label',
      $('aui-dialog>.aui-dialog'),
    );
  }
  clickClose() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__header-close'),
    );
    return button.click();
  }
  clickConfirm() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__footer .aui-button--primary'),
    );
    return button.click();
  }
  clickCancel() {
    const button = new AlaudaButton(
      this.alaudaElement.root.$('.aui-dialog__footer .aui-button--default'),
    );
    return button.click();
  }
  getElementByText(text): AlaudaInputbox | AlaudaRadioButton | ArrayFormTable {
    switch (text) {
      case '协议类型':
      case '协议':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
          'aui-radio-button',
        );
      case '请求头':
        return new ArrayFormTable(
          this.alaudaElement.getElementByText(text, 'rc-array-form-table'),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
          '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
        );
      case '启动命令':
        return new ArrayFormTable(
          this.alaudaElement.getElementByText(
            text,
            'rc-string-array-form-table',
          ),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
          '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
        );
      default:
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, 'input'),
        );
    }
  }
  input(values) {
    for (const key in values) {
      switch (key) {
        case '协议类型':
        case '协议':
          const ele_1 = this.getElementByText(key) as AlaudaRadioButton;
          ele_1.clickByName(values[key]);
          break;
        case '请求头':
        case '启动命令':
          const ele_2 = this.getElementByText(key) as ArrayFormTable;
          ele_2.fill(values[key]);
          break;
        default:
          const ele_3 = this.getElementByText(key) as AlaudaInputbox;
          ele_3.input(values[key]);
          break;
      }
    }
  }
}
