import { AcpPageBase } from '@acp/acp.page.base';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ElementFinder, by, element, browser } from 'protractor';
import { $ } from 'protractor';
import { VolumeCreatePage } from './app.volume.create.page';
import { AlaudaAuiTagsInput } from '@e2e/element_objects/alauda.aui_tags_input';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { HealthCheckElement } from './app.health.check.form.element';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';

export class AppCreateContainerPod extends AcpPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this.waitElementPresent(root, '没有找到应用创建页，没有找到容器组控件');
  }

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label', this._root);
  }
  _getElementByText(
    left: string,
    tagname = 'input',
    root_xpath = '/',
  ): ElementFinder {
    const xpath = `${root_xpath}/aui-form-item//label[@class="aui-form-item__label" and normalize-space(text())="${left}" ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    const base_ele = new AlaudaElementBase();
    base_ele.scrollToView(element(by.xpath(xpath)));
    return element(by.xpath(xpath));
  }
  input(data) {
    for (const key in data) {
      switch (key) {
        case '名称':
          new AlaudaInputbox(
            $('rc-pod-template-form>form input[formcontrolname="name"]'),
          ).input(data[key]);
          break;
        case '镜像地址':
          new AlaudaButton(this._getElementByText(key, 'button')).click();

          const image: ImageSelect = new ImageSelect($('div aui-dialog'));
          //{方式: '输入'， 镜像地址: '***'}
          if (data[key]['方式'] === '输入') {
            image.enterImage(data[key]['镜像地址']);
          }
          break;
        case '资源限制':
          const resources: ElementFinder = this._getElementByText(
            key,
            'rc-resources-form',
          );
          // {资源限制: [{CPU: 10, 单位: 'm' }, {内存: 10, 单位: 'Mi'}]
          resources.$$('div[class*="input-groups"]').each((elem, index) => {
            const inputBoxGroup: AlaudaInputGroup = new AlaudaInputGroup(
              elem,
              '.addon-label',
              'input[formcontrolname="value"]',
            );
            inputBoxGroup.scrollToBoWindowBottom();
            const item = data[key][index];

            for (const keyItem in item) {
              if (keyItem !== '单位') {
                inputBoxGroup.input(item[keyItem], item['单位']);
              }
            }
          });
          break;
        case '启动命令':
        case '参数':
        case '日志文件':
        case '排除日志文件':
          this.expandContainer();
          new ArrayFormTable(
            this._getElementByText(key, 'rc-string-array-form-table'),
            '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
            '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
          ).fill(data[key]);
          break;
        case '端口':
          this.expandContainer();
          new ArrayFormTable(
            this._getElementByText(key, 'rc-array-form-table'),
            '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
            '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
          ).fill(data[key]);
          break;
        case '健康检查':
          this.expandContainer();
          new HealthCheckElement(
            this._getElementByText(key, 'rc-healthcheck-form'),
          ).input(data[key]);
          break;
        case '存储卷挂载':
          this.expandContainer();
          if (Array.isArray(data[key])) {
            new ArrayFormTable(
              this._getElementByText(key, 'rc-array-form-table'),
              '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
              '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
            ).fill(data[key]);
          } else {
            const add_volume = new ArrayFormTable(
              this._getElementByText(key, 'rc-array-form-table'),
              '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
              '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
            );
            browser.sleep(1).then(
              data[key]['存储卷'].forEach(volume => {
                add_volume.click(
                  '添加存储卷并挂载',
                  '.rc-array-form-table__bottom-control-buttons',
                );
                const add_volume_page = new VolumeCreatePage();
                add_volume_page.input(volume);
                add_volume_page.clickConfirm();
              }),
            );

            add_volume.fill(data[key]['存储卷挂载']);
          }
          break;
        case '环境变量':
          this.expandContainer();
          new ArrayFormTable(
            this._getElementByText(key, 'rc-array-form-table'),
            '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
            '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
          ).fill(data[key]);
          break;
        case '环境变量引用':
          this.expandContainer();
          const arrayFormTable = new ArrayFormTable(
            this._getElementByText('环境变量', 'rc-array-form-table'),
            '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
            '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
          );
          data[key].forEach(() => {
            arrayFormTable.click(
              '添加引用',
              '.rc-array-form-table__bottom-control-buttons',
            );
          });
          arrayFormTable.fill(data[key]);
          break;
        case '配置引用':
          this.expandContainer();
          const multi_select = new AuiMultiSelect(
            this._getElementByText(key, 'aui-multi-select'),
            $('.cdk-overlay-pane aui-tooltip'),
          );
          const configs = data[key] as Array<Array<string>>;
          configs.forEach(config => {
            if (config.length === 1) {
              multi_select.select(config[0]);
            } else {
              multi_select.select(config[0], config[1]);
            }
          });

          break;
        case '容器组 - 高级':
          const advanceInfo = data[key];
          for (const keyInfo in advanceInfo) {
            switch (keyInfo) {
              case '存储卷':
                const volume_data = advanceInfo[keyInfo] as Array<
                  Record<string, any>
                >;

                const formtale = new ArrayFormTable(
                  this._getElementByText('存储卷', 'rc-volumes-form'),
                  'aui-icon[icon*="add_circle"]',
                );
                volume_data.forEach(volume => {
                  formtale
                    .click(
                      '添加',
                      '.rc-array-form-table__bottom-control-buttons',
                    )
                    .then(() => {
                      const volume_page = new VolumeCreatePage();
                      volume_page.input(volume);
                      volume_page.clickConfirm();
                    });
                });
                break;
              case '存储卷取消':
                const volume_data_c = advanceInfo[keyInfo] as Array<
                  Record<string, any>
                >;

                const formtale_c = new ArrayFormTable(
                  this._getElementByText('存储卷', 'rc-volumes-form'),
                  'aui-icon[icon*="add_circle"]',
                );
                volume_data_c.forEach(volume => {
                  formtale_c
                    .click(
                      '添加',
                      '.rc-array-form-table__bottom-control-buttons',
                    )
                    .then(() => {
                      const volume_page_c = new VolumeCreatePage();
                      volume_page_c.input(volume);
                      volume_page_c.clickCancel();
                    });
                });
                break;
              case '存储卷x':
                const volume_data_x = advanceInfo[keyInfo] as Array<
                  Record<string, any>
                >;

                const formtale_x = new ArrayFormTable(
                  this._getElementByText('存储卷', 'rc-volumes-form'),
                  'aui-icon[icon*="add_circle"]',
                );
                volume_data_x.forEach(volume => {
                  formtale_x
                    .click(
                      '添加',
                      '.rc-array-form-table__bottom-control-buttons',
                    )
                    .then(() => {
                      const volume_page_x = new VolumeCreatePage();
                      volume_page_x.input(volume);
                      volume_page_x.clickClose();
                    });
                });
                break;
              case '容器组标签':
                // 展开容器组高级控件
                this.expandAdvance();

                // 填写标签, [['label-name', 'label-value']]
                new ArrayFormTable(
                  element(
                    by.xpath(
                      '//label[contains(text(),"容器组标签")]/ancestor::aui-form-item//rc-key-value-form-table',
                    ),
                  ),
                  'aui-icon[icon*="add_circle"]',
                ).fill(advanceInfo[keyInfo]);

                break;
              case '主机选择器':
                break;
              case '亲和性':
                break;
              case 'Host 模式':
                this.expandAdvance();
                const host_network = advanceInfo[keyInfo] as string;
                if (host_network === 'true') {
                  new AuiSwitch(
                    this._getElementByText(keyInfo, 'aui-switch'),
                  ).open();
                } else {
                  new AuiSwitch(
                    this._getElementByText(keyInfo, 'aui-switch'),
                  ).close();
                }
                break;
              case '凭据':
                const multi_select = new AuiMultiSelect(
                  this._getElementByText(keyInfo, 'aui-multi-select'),
                  $('.cdk-overlay-pane aui-tooltip'),
                );
                multi_select.select(advanceInfo[keyInfo]);
                break;
              case '固定 IP':
                // '固定 IP':['177.177.177.177']
                // 展开容器组高级控件
                this.expandAdvance();
                // 填写固定 IP
                const rightElem: ElementFinder = this._getElementByText(
                  keyInfo,
                  'aui-tags-input',
                );
                const tagInput = new AlaudaAuiTagsInput(rightElem);
                advanceInfo[keyInfo].forEach(ip => {
                  tagInput.input(ip);
                });
                break;
            }
          }
      }
    }
  }

  private expandAdvance() {
    new FoldableBlock(
      $(
        'rc-pod-template-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
      ),
    ).expand();
  }
  private expandContainer() {
    new FoldableBlock(
      $(
        'rc-container-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
      ),
    ).expand();
  }
}
