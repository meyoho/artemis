import { AcpPageBase } from '@acp/acp.page.base';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, ElementFinder } from 'protractor';

import { AppCreateContainerPod } from './acp.create.container.pod';

export class AppCreateComputeComponent extends AcpPageBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this.waitElementPresent(root, '没有找到应用创建页，没有找到计算组件控件');
  }

  get root(): ElementFinder {
    this.waitElementPresent(
      this._root,
      '没有找到应用创建页，没有找到计算组件控件',
    );
    return this._root;
  }

  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label', this._root);
  }
  get computeElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-pod-controller-form .aui-card__content>aui-form-item>div',
      'label',
    );
  }
  getElementByText(text): any {
    switch (text) {
      case '名称':
      case '实例数量':
        return this.computeElement.getElementByText(text, 'input');
      case '部署模式':
        return this.computeElement.getElementByText(text, 'aui-radio-group');
      case '容器组':
        return new AppCreateContainerPod($('rc-pod-template-form>form'));
    }
  }

  input(data) {
    for (const key in data) {
      switch (key) {
        case '容器组':
          this.getElementByText(key).input(data[key]);
          break;
        case '名称':
        case '实例数量':
          new AlaudaInputbox(this.getElementByText(key)).input(data[key]);
          break;
        case '部署模式':
          new AlaudaRadioButton(this.getElementByText(key)).clickByName(
            data[key],
          );
          break;
        case '组件 - 高级':
          const advanceInfo = data[key];
          for (const keyInfo in advanceInfo) {
            switch (keyInfo) {
              case '标签':
                // 展开组件高级控件
                new FoldableBlock(
                  $(
                    'rc-pod-controller-form>form>aui-card>div>div>rc-foldable-block>aui-form-item .aui-form-item__content button',
                  ),
                ).expand();

                // 填写标签
                new ArrayFormTable(
                  $(
                    'rc-pod-controller-form .aui-card__content>rc-foldable-block>div>aui-form-item rc-array-form-table',
                  ),
                  'aui-icon[icon*="add_circle"]',
                ).fill(advanceInfo[keyInfo]);

                break;
              case '更新策略':
                // 展开组件高级控件
                new FoldableBlock(
                  $(
                    'rc-pod-controller-form>form>aui-card>div>div>rc-foldable-block>aui-form-item .aui-form-item__content button',
                  ),
                ).expand();

                // 填写更新策略
                const rightElem: ElementFinder = $(
                  'rc-update-strategy-form .aui-form-item__content',
                );
                rightElem.$$('input').each((elem, index) => {
                  new AlaudaInputbox(elem).input(advanceInfo[keyInfo][index]);
                });
                break;
            }
          }
          break;
      }
    }
  }
}
