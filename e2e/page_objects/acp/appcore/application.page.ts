import { AppDetailVerify } from '@e2e/page_verify/acp/appcore/application_detail.page';
import { AppCreateVerifyPage } from '@e2e/page_verify/acp/appcore/application_create.page';
import { AppListVerify } from '@e2e/page_verify/acp/appcore/application_list.page';
import { AppPreviewVerify } from '@e2e/page_verify/acp/appcore/application_preview.page';
import { DaemonSetDetailVerify } from '@e2e/page_verify/acp/daemonset/detail.page';
import { DeploymentDetailVerify } from '@e2e/page_verify/acp/deployment/detail.page';
import { StatefulSetDetailVerify } from '@e2e/page_verify/acp/statefulset/detail.page';

import { AcpPageBase } from '../acp.page.base';

import { AppCreatePage } from './app.create.page';
import { AppDetailPage } from './app.detail.page';
import { AppListPage } from './app.list.page';
import { PreviewPage } from './app.preview.page';
import { AppPrepareData } from './prepare.page';

export class Application extends AcpPageBase {
  get createAppPage() {
    return new AppCreatePage();
  }
  get appCreateVerify() {
    return new AppCreateVerifyPage();
  }
  get detailAppPage() {
    return new AppDetailPage();
  }
  get appDetailVerifyPage() {
    return new AppDetailVerify();
  }
  get appListPage() {
    return new AppListPage();
  }
  get appListVerifyPage() {
    return new AppListVerify();
  }
  get appPreviewPage() {
    return new PreviewPage();
  }
  get appPreviewVerifyPage() {
    return new AppPreviewVerify();
  }
  get daemonsetVerifyPage() {
    return new DaemonSetDetailVerify();
  }
  get deploymentVerifyPage() {
    return new DeploymentDetailVerify();
  }
  get statefulsetVerifyPage() {
    return new StatefulSetDetailVerify();
  }
  get prepare() {
    return new AppPrepareData();
  }
}
