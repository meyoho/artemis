import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $, ElementFinder, browser } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

import { ResourcePreview } from './acp.appcore.resource.preview';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AuiDropdownButton } from '@e2e/element_objects/alauda.aui_dropdown_button';
import { AppCreatePage } from './app.create.page';
import { AppCreateService } from './acp.create.service';
import { AppCreateIngress } from './acp.create.ingress';
import { AppYamlForm } from './app.yaml.form.page';
import { isString } from 'util';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';

export class PreviewPage extends AcpPageBase {
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'aui-form-item .aui-form-item__label',
    );
  }
  get create_page() {
    return new AppCreatePage();
  }
  get rs_yaml_form() {
    return new AppYamlForm($('aui-dialog'));
  }
  get yamlEditor() {
    return new AlaudaYamlEditor();
  }
  get switchRadioButton() {
    return new AlaudaRadioButton(
      this.resourceListCard.$('aui-radio-group'),
      '.aui-radio-button__content',
    );
  }
  get headerCard(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'label',
      $('form aui-card:nth-child(1)'),
    );
  }
  get resourceListCard(): ElementFinder {
    this.waitElementPresent(
      $('form aui-card:nth-child(2)'),
      '应用预览页资源列表未出现',
    );
    return $('form aui-card:nth-child(2)');
  }
  get_resource_card(rs_type: string, rs_name: string): ResourcePreview {
    console.log(`PreviewPage.get_resource_card(${rs_type}, ${rs_name})`);
    const rss = this.resourceListCard
      .$$(
        'rc-array-form-table>table>tbody>tr[class*="rc-array-form-table--row-group-start"]',
      )
      .filter(tr_ele => {
        const rp = new ResourcePreview(tr_ele);

        return rp.get_value_by_key('资源类型').then(v_t => {
          // console.log(`资源类型: ${v.trim()}`);
          return rp.get_value_by_key('名称').then(v_n => {
            // console.log(`名称: ${v.trim()}`);
            return v_n.trim() === rs_name && v_t.trim() === rs_type;
          });
        });
      });
    this.waitElementPresent(rss.first(), '等待资源卡片出现', 3000);
    return new ResourcePreview(rss.first());
  }
  get version_comment_input_box() {
    return new AlaudaInputbox($('.resource-form__footer input'));
  }
  click_rs_mgt_button(button_text) {
    if (button_text === '添加工作负载') {
      return this.getButtonByText(button_text).click();
    } else {
      return new AuiDropdownButton(
        $('aui-dropdown-button button+button'),
        $('aui-tooltip'),
      ).select(button_text);
    }
  }
  input(testData: Record<string, any>) {
    return browser.sleep(1).then(() => {
      for (const key in testData) {
        switch (key) {
          case '版本注释':
            this.version_comment_input_box.input(testData[key]);
            break;
          case '添加工作负载取消':
          case '添加工作负载':
          case '添加工作负载不提交':
            const workloads = testData[key] as Array<Record<string, any>>;
            workloads.forEach(workload => {
              this.click_rs_mgt_button('添加工作负载').then(() => {
                this.create_page.fillForm(workload);
                browser.sleep(3000);
                if (key === '添加工作负载') {
                  this.getButtonByText('保存').click();
                } else if (key === '添加工作负载取消') {
                  this.getButtonByText('取消').click();
                }
              });
            });
            break;
          case '删除计算组件':
            this.get_resource_card(
              testData[key]['类型'],
              testData[key]['名称'],
            ).click_button('删除');
            break;
          case '修改计算组件':
          case '修改计算组件取消':
            this.get_resource_card(
              testData[key]['类型'],
              testData[key]['名称'],
            ).click_button('编辑');
            this.create_page.fillForm(testData[key]['数据']);
            if (key === '修改计算组件') {
              this.getButtonByText('保存').click();
            } else if (key === '修改计算组件取消') {
              this.getButtonByText('取消').click();
            }
            break;
          case '修改内部路由':
          case '修改内部路由取消':
            this.get_resource_card(
              'Service',
              testData[key]['名称'],
            ).click_button('编辑');
            new AppCreateService().input(testData[key]['数据']);
            if (key === '修改内部路由') {
              this.auiDialog.clickConfirm();
            } else if (key === '修改内部路由取消') {
              this.auiDialog.clickCancel();
            }
            break;
          case '删除内部路由':
            this.get_resource_card(
              'Service',
              testData[key]['名称'],
            ).click_button('删除');
            break;
          case '添加内部路由取消':
          case '添加内部路由':
            const services = testData[key] as Array<Record<string, any>>;
            services.forEach(service => {
              this.click_rs_mgt_button('添加内部路由').then(() => {
                new AppCreateService().input(service);
                if (key === '添加内部路由') {
                  this.create_page.auiDialog.clickConfirm();
                } else if (key === '添加内部路由取消') {
                  this.create_page.auiDialog.clickCancel();
                }
              });
            });
            break;
          case '修改访问规则':
          case '修改访问规则取消':
            this.get_resource_card(
              'Ingress',
              testData[key]['名称'],
            ).click_button('编辑');
            new AppCreateIngress().input(testData[key]['数据']);
            if (key === '修改访问规则') {
              this.auiDialog.clickConfirm();
            } else if (key === '修改访问规则取消') {
              this.auiDialog.clickCancel();
            }
            break;
          case '删除访问规则':
            this.get_resource_card(
              'Ingress',
              testData[key]['名称'],
            ).click_button('删除');
            break;
          case '添加访问规则':
          case '添加访问规则取消':
            const ingress = testData[key] as Array<Record<string, any>>;
            ingress.forEach(ingres => {
              this.click_rs_mgt_button('添加访问规则').then(() => {
                new AppCreateIngress().input(ingres);
                if (key === '添加访问规则') {
                  this.create_page.auiDialog.clickConfirm();
                } else if (key === '添加访问规则取消') {
                  this.create_page.auiDialog.clickCancel();
                }
              });
            });
            break;
          case 'YAML 添加资源':
          case 'YAML 添加资源取消':
            const yamls = testData[key] as Array<Record<string, any>>;
            yamls.forEach(yaml => {
              this.click_rs_mgt_button(key).then(() => {
                this.rs_yaml_form.inputYaml(yaml);
                if (key === 'YAML 添加资源') {
                  this.rs_yaml_form.clickConfirm();
                } else if (key === 'YAML 添加资源取消') {
                  this.rs_yaml_form.clickCancel();
                }
              });
            });
            break;
          case '名称':
          case '显示名称':
            new AlaudaInputbox(
              this.headerCard.getElementByText(key, 'input'),
            ).input(testData[key]);
            break;
          case 'YAML':
            this.switchRadioButton.clickByName('YAML').then(() => {
              if (isString(testData[key])) {
                this.yamlEditor.setYamlValue(testData[key]);
              } else {
                console.warn(
                  `testData['${key}'] 不是string yaml，尝试转成string yaml`,
                );
                const yaml_content = this.rs_yaml_form.dumpYaml(testData[key]);
                this.yamlEditor.setYamlValue(yaml_content);
              }
            });
            this.switchRadioButton.clickByName('资源列表');
            break;
          case 'YAML创建':
            if (isString(testData[key])) {
              this.yamlEditor.setYamlValue(testData[key]);
            } else {
              console.warn(
                `testData['${key}'] 不是string yaml，尝试转成string yaml`,
              );
              const yaml_content = this.rs_yaml_form.dumpYaml(testData[key]);
              this.yamlEditor.setYamlValue(yaml_content);
            }
            break;
        }
      }
    });
  }
}
