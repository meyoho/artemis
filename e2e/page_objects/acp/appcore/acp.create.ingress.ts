import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $ } from 'protractor';

import { CreateIngressPage } from '../ingress/create.page';

export class AppCreateIngress extends CreateIngressPage {
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-form-item',
            'label',
            $('aui-dialog ng-component')
        );
    }

    input(data) {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                this.enterValue(key, data[key]);
            }
        }
    }
}
