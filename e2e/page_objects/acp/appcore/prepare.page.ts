import { ServerConf } from '@e2e/config/serverConf';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { alauda_type_application } from '@e2e/utility/resource.type.k8s';

import { AcpPreparePage } from '../prepare.page';

export class AppPrepareData extends AcpPreparePage {
  /**
   * 删除应用
   * @param appName 应用名称
   * @param nsName namespace名称
   */
  deleteApp(appName: string, nsName, region_name = ServerConf.REGIONNAME) {
    return CommonKubectl.deleteResource(
      alauda_type_application,
      appName,
      region_name,
      nsName,
    );
  }
  /**
   * 删除应用
   * @param appYaml内容
   */
  deleteAppByTemplate(yamlFile: string, data, cluster = this.clusterName) {
    CommonKubectl.deleteResourceByTemplate(
      yamlFile,
      data,
      `application_data_tmp_${this.getRandomNumber()}`,
      cluster,
    );
  }
  /**
   * 创建一个应用
   * @param yamlFile
   * @param data 应用数据
   */
  createAppByTemplate(yamlFile: string, data, cluster = this.clusterName) {
    return CommonKubectl.createResourceByTemplate(
      yamlFile,
      data,
      `application_data_tmp_${this.getRandomNumber()}`,
      cluster,
    );
  }
}
