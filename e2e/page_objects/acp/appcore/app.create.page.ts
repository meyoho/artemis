import { AcpPageBase } from '@acp/acp.page.base';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { $, browser, by, element } from 'protractor';

import { AppCreateComputeComponent } from './acp.create.compute.component';
import { AppCreateIngress } from './acp.create.ingress';
import { AppCreateService } from './acp.create.service';
import { AppCreateBasic } from './app.create.basic';
import { AppListPage } from './app.list.page';
import { PreviewPage } from './app.preview.page';
import { AppDetailPage } from './app.detail.page';
import { ResourcePreview } from './acp.appcore.resource.preview';

export class AppCreatePage extends AcpPageBase {
  get listPage(): AppListPage {
    return new AppListPage();
  }
  get detailPage() {
    return new AppDetailPage();
  }
  get previewPage(): PreviewPage {
    return new PreviewPage();
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-card .aui-card', '.aui-card__header');
  }
  get_resource_card(rs_type: string, rs_name: string): ResourcePreview {
    console.log(`AppCreatePage.get_resource_card(${rs_type}, ${rs_name})`);
    if (rs_type === '内部路由') {
      const rss = $('rc-services-form')
        .$$(
          'rc-array-form-table>table>tbody>tr[class*="rc-array-form-table--row-group-start"]',
        )
        .filter(tr_ele => {
          const rp = new ResourcePreview(tr_ele);
          // console.log(`资源类型: ${v.trim()}`);
          return rp.get_value_by_key('名称').then(v_n => {
            // console.log(`名称: ${v.trim()}`);
            return v_n.trim() === rs_name;
          });
        });
      return new ResourcePreview(rss.first());
    } else if (rs_type === '访问规则') {
      const rss = $('rc-ingresses-form')
        .$$(
          'rc-array-form-table>table>tbody>tr[class*="rc-array-form-table--row-group-start"]',
        )
        .filter(tr_ele => {
          const rp = new ResourcePreview(tr_ele);
          // console.log(`资源类型: ${v.trim()}`);
          return rp.get_value_by_key('名称').then(v_n => {
            // console.log(`名称: ${v.trim()}`);
            return v_n.trim() === rs_name;
          });
        });
      return new ResourcePreview(rss.first());
    } else {
      return null;
    }
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(
    text: string,
  ):
    | ImageSelect
    | AppCreateBasic
    | AppCreateComputeComponent
    | AppCreateService
    | AppCreateIngress {
    switch (text) {
      case '选择镜像':
      case '选择镜像取消':
        return new ImageSelect($('div aui-dialog'));
      case '应用':
        return new AppCreateBasic($('rc-application-metadata-form form'));
      case '计算组件':
        return new AppCreateComputeComponent(
          $('form rc-pod-controller-form .aui-card__content'),
        );
      case '内部路由':
        return new AppCreateService();
      case '访问规则':
        return new AppCreateIngress();
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name: string, value) {
    switch (name) {
      case '选择镜像':
        const image: ImageSelect = this.getElementByText(name) as ImageSelect;
        if (value['方式'] === '输入') {
          image.enterImage(value['镜像地址']);
        }
        break;
      case '选择镜像取消':
        const _image: ImageSelect = this.getElementByText(name) as ImageSelect;
        if (value['方式'] === '输入') {
          _image.enterImage_cancel(value['镜像地址']);
        }
        break;
      case '应用':
        const app_: AppCreateBasic = this.getElementByText(
          name,
        ) as AppCreateBasic;
        app_.input(value);
        break;

      case '计算组件':
        if (Array.isArray(value)) {
          value.forEach((v: Record<string, any>) => {
            if ('选择镜像' in v) {
              this.getButtonByText('添加工作负载').click();
              this.fillForm(v);

              this.getButtonByText('保存')
                .click()
                .then(() => {
                  console.log('点击保存按钮');
                });
            } else {
              this.enterValue('计算组件', v);
            }
          });
        } else {
          const conpute = this.getElementByText(
            name,
          ) as AppCreateComputeComponent;
          conpute.input(value);
          //     this.getButtonByText('预览')
          //         .isPresent()
          //         .then(isPresent => {
          //             if (isPresent) {
          //                 this.getButtonByText('预览')
          //                     .getText()
          //                     .then(txt => {
          //                         console.log(txt);
          //                         browser.sleep(300);
          //                     });
          //                 this.getButtonByText('预览')
          //                     .click()
          //                     .then(() => {
          //                         console.log('点击预览按钮');
          //                     });
          //             } else {
          //                 this.getButtonByText('保存')
          //                     .click()
          //                     .then(() => {
          //                         console.log('点击保存按钮');
          //                     });
          //             }
          //         });
        }
        break;
      case '内部路由':
      case '访问规则':
      case '内部路由取消':
      case '访问规则取消':
      case '内部路由x':
      case '访问规则x':
        this.getButtonByText(`添加${name.slice(0, 4)}`).click();
        if (name.startsWith('内部路由')) {
          const service: AppCreateService = this.getElementByText(
            name.slice(0, 4),
          ) as AppCreateService;
          service.input(value);
        } else if (name.startsWith('访问规则')) {
          const ingress: AppCreateIngress = this.getElementByText(
            name.slice(0, 4),
          ) as AppCreateIngress;
          ingress.input(value);
        }
        if (name.length == 4) {
          this.auiDialog.clickConfirm();
        } else if (name === `${name.slice(0, 4)}取消`) {
          this.auiDialog.clickCancel();
        } else {
          this.auiDialog.clickClose();
        }
        break;
      case '修改内部路由':
      case '修改访问规则':
      case '修改内部路由取消':
      case '修改访问规则取消':
      case '修改内部路由x':
      case '修改访问规则x':
        this.get_resource_card(name.slice(2, 6), value['名称'])
          .click_button('编辑')
          .then(() => {
            if (name.slice(2, 6) === '内部路由') {
              const service: AppCreateService = this.getElementByText(
                name.slice(2, 6),
              ) as AppCreateService;
              service.input(value['数据']);
            } else if (name.slice(2, 6) === '访问规则') {
              const ingress: AppCreateIngress = this.getElementByText(
                name.slice(2, 6),
              ) as AppCreateIngress;
              ingress.input(value['数据']);
            }
            if (name.length == 6) {
              this.auiDialog.clickConfirm();
            } else if (name === `${name.slice(0, 6)}取消`) {
              this.auiDialog.clickCancel();
            } else {
              this.auiDialog.clickClose();
            }
          });
        break;
      case '删除内部路由':
      case '删除访问规则':
        this.get_resource_card(name.slice(2, 6), value['名称']).click_button(
          '删除',
        );
        break;
    }
  }
  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   * @example fillForm({ 应用名称: 'qq', 镜像源证书: '不使用' })
   */
  fillForm(data) {
    return browser.sleep(1).then(() => {
      for (const key in data) {
        //   if (data.hasOwnProperty(key)) {
        this.enterValue(key, data[key]);
        //   }
      }
    });
  }
  private _waitAppRunning(name, ns) {
    const errorMessage = CommonKubectl.execKubectlCommand(
      `kubectl get application ${name} -n ${ns} -o yaml`,
      this.clusterName,
    );
    this.waitElementPresent(
      element(
        by.xpath(
          '//rc-app-status/rc-status-icon/span[contains(text(),"运行中")]',
        ),
      ),
      `应用 ${name} 没有处于 running 状态\n + ${errorMessage}`,
      120000,
    );
  }

  private _waitAppPending(name, ns) {
    const errorMessage = CommonKubectl.execKubectlCommand(
      `kubectl get application ${name} -n ${ns} -o yaml`,
      this.clusterName,
    );
    this.waitElementPresent(
      element(
        by.xpath(
          '//rc-app-status/rc-status-icon/span[contains(text(),"处理中")]',
        ),
      ),
      `应用 ${name} 没有处于 pending 状态\n + ${errorMessage}`,
      60000,
    );
  }

  createAppNotWaitRunning(testData, ns) {
    this.clickLeftNavByText('应用');

    this.getButtonByText('创建应用').click();

    // this.enterValue('选择镜像', testData['选择镜像']);
    this.fillForm(testData);
    this.getButtonByText('确定')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          this.getButtonByText('确定').click();
        } else {
          this.getButtonByText('创建').click();
        }
      });
    this.waitElementPresent(
      $('rc-app-detail-main'),
      '页面没有跳转到应用详情页',
    );
    this._waitAppPending(testData['应用']['名称'], ns);
  }
  /**
   * 选择完镜像取消
   * @param testData
   */
  select_image_cancel(testData) {
    this.clickLeftNavByText('应用');

    this.getButtonByText('创建应用').click();

    // this.enterValue('选择镜像', testData['选择镜像']);
    this.fillForm(testData);
  }
  create(testData, ns) {
    this.clickLeftNavByText('应用');

    this.getButtonByText('创建应用').click();

    // this.enterValue('选择镜像', testData['选择镜像']);
    this.fillForm(testData);
    this.getButtonByText('确定')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          this.getButtonByText('确定').click();
        } else {
          this.getButtonByText('创建').click();
        }
      });
    this._waitAppRunning(testData['应用']['名称'], ns);
  }
  yamlCreate(testData, ns) {
    this.clickLeftNavByText('应用');
    this.listPage.clickYamlCreate();
    this.previewPage.input(testData);
    this.getButtonByText('创建').click();
    this._waitAppRunning(testData['名称'], ns);
  }
  create_cancel(testData) {
    this.clickLeftNavByText('应用');

    this.getButtonByText('创建应用').click();

    // this.enterValue('选择镜像', testData['选择镜像']);
    this.fillForm(testData);
    this.getButtonByText('取消').click();
    browser.sleep(1000);
    $('rc-app-create-preview-form')
      .isPresent()
      .then(ispresent => {
        if (ispresent) {
          this.getButtonByText('取消').click();
        }
      });
  }
  update(testData, name, ns) {
    this.clickLeftNavByText('应用');

    this.listPage.clickAction(name, '更新');
    this.getButtonByText('添加工作负载').click();

    this.fillForm(testData);
    browser.sleep(3000);
    this.getButtonByText('保存').click();
    browser.sleep(3000);
    this.getButtonByText('更新').click();

    this._waitAppRunning(name, ns);
  }
  new_update(testData, name, ns, wait_running = true) {
    this.clickLeftNavByText('应用');

    this.listPage.clickAction(name, '更新');
    this.previewPage.input(testData);
    browser.sleep(3000);
    this.getButtonByText('更新').click();
    if (wait_running) {
      this._waitAppRunning(name, ns);
    }
  }
  update_notConfirm(testData, name) {
    return this.listPage.clickAction(name, '更新').then(() => {
      return this.previewPage.input(testData);
    });
  }
  create_notConfirm(testData) {
    this.clickLeftNavByText('应用');
    return this.getButtonByText('创建应用')
      .click()
      .then(() => {
        this.fillForm(testData);
      });
  }
  update_workload_rs(
    testData,
    rs_type: string,
    rs_name: string,
    ns: string,
    app_name,
  ) {
    this.clickLeftNavByText('应用');
    this.listPage.clickAction(app_name, '更新');
    this.previewPage
      .get_resource_card(rs_type, rs_name)
      .get_button('编辑')
      .click();
    this.fillForm(testData);
    this.getButtonByText('保存').click();
    this.getButtonByText('更新').click();
    this._waitAppRunning(rs_name, ns);
  }
  update_app_yaml(yaml) {
    this.detailPage.clickOperation('更新');
    this.previewPage.input(yaml);
    this.getButtonByText('更新').click();
  }
}
