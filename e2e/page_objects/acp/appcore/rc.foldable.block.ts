import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { CommonPage } from '@e2e/utility/common.page';
import { $, ElementFinder, ExpectedConditions, browser, by } from 'protractor';

export class RcFoldableBlock {
    private _selector;
    constructor(selector: ElementFinder) {
        this._selector = selector;
    }
    /**
     * 滑块控件
     * @param auiSwitch ElementFinder
     */
    auiswitch(auiSwitch: ElementFinder) {
        return new AuiSwitch(auiSwitch);
    }
    /**
     * 下拉单选控件
     * @param auiSelect ElementFinder
     * @param auiToolTip ElementFinder
     */
    auiselect(auiSelect: ElementFinder, auiToolTip: ElementFinder) {
        return new AuiSelect(auiSelect, auiToolTip);
    }
    /**
     * 根据标签获取表单选项
     * @param label
     * @auiRadioBtn 单选按钮的名称
     */
    getFormItem(label: string = '', auiRadioBtn: string = '') {
        let item: any;
        if (!label && auiRadioBtn === 'configMap') {
            item = this._selector
                .$(`div[ng-reflect-name=${auiRadioBtn}]`)
                .$('aui-form-item:nth-child(3)');
        } else if (label === '类型' && !auiRadioBtn) {
            // item = this._selector.$('rc-container-volume-form>form>aui-form-item')
            item = this._selector
                .$$('aui-form-item')
                .filter(ele => {
                    return ele
                        .$('.aui-form-item__label')
                        .isPresent()
                        .then(preset => {
                            if (preset) {
                                return ele
                                    .$('.aui-form-item__label')
                                    .getText()
                                    .then(t => {
                                        return t.trim() === label;
                                    });
                            }
                        });
                })
                .first();
        } else if (
            label &&
            (auiRadioBtn === 'hostPath' ||
                auiRadioBtn === 'persistentVolumeClaim' ||
                auiRadioBtn === 'configMap')
        ) {
            item = this._selector
                .$(`div[ng-reflect-name=${auiRadioBtn}]`)
                .$$('aui-form-item')
                .filter(ele => {
                    return ele
                        .$('.aui-form-item__label')
                        .isPresent()
                        .then(preset => {
                            if (preset) {
                                return ele
                                    .$('.aui-form-item__label')
                                    .getText()
                                    .then(t => {
                                        return t.trim() === label;
                                    });
                            }
                        });
                })
                .first();
        } else {
            item = this._selector
                .$$('aui-form-item')
                .filter(ele => {
                    return ele
                        .$('.aui-form-item__label')
                        .getText()
                        .then(t => {
                            return t.trim() === label;
                        });
                })
                .first();
        }
        browser.wait(
            ExpectedConditions.visibilityOf(item),
            3000,
            `未找到 【${label}】 `
        );
        return item.$('.aui-form-item__content');
    }
    /**
     * 点击添加按钮
     * @text 文本名
     * @selector ElementFinder
     */
    clickAddButton(
        text: string,
        css: string = '.aui-button--primary',
        auiRadioBtn: string = ''
    ) {
        return this.getFormItem(text, auiRadioBtn)
            .$(css)
            .click();
    }
    /**
     * 更新策略
     * @param label 标签名
     * @param parameters 参数
     */
    updateStrategy(label: string, parameters: Array<string>) {
        this.getFormItem(label)
            .$$('input')
            .each(function(ele, index) {
                ele.clear();
                ele.sendKeys(parameters[index]);
            });
    }
    /**
     * 切换网络模式
     * @param text 文本名
     */
    networkModul(text: string, auiRadioBtn: string = '') {
        return this.auiswitch(
            this.getFormItem(text, auiRadioBtn).$('aui-switch')
        );
    }
    /**
     * 健康检查按钮
     * @param label 标签名
     * @param btnName 按钮名
     */
    healthcheckBtn(label: string, btnName: string) {
        const btn = this.getFormItem(label)
            .$$('button')
            .filter(e => {
                return e
                    .$('span')
                    .getText()
                    .then(text => {
                        return text.trim() === btnName;
                    });
            })
            .first();
        browser.driver.wait(
            function() {
                return ExpectedConditions.elementToBeClickable(btn);
            },
            3000,
            `the button ${btnName} can not be clicked`
        );
        btn.click();
    }
    /**
     * 协议类型
     * @param label
     * @param protocol
     */
    protocol(label: string, protocol: string) {
        return this.getFormItem(label)
            .$('aui-radio-group')
            .$(`aui-radio-button[ng-reflect-value=${protocol}]`);
    }
    /**
     * input输入框
     * @param label 标签名
     * @param parameters 参数
     */
    input(label: string, parameters: string, auiRadioBtn: string = '') {
        const ele = this.getFormItem(label, auiRadioBtn).$('input');
        ele.clear();
        ele.sendKeys(parameters);
    }
    /**
     * 添加参数
     * @param label 标签名
     * @param parameters 参数
     */
    insertParams(
        label: string = '',
        parameters: Array<string>,
        auiRadioBtn: string = ''
    ) {
        const _this = this;
        const row = this.getFormItem(label, auiRadioBtn);
        browser.wait(
            ExpectedConditions.visibilityOf(
                row.$('tr.rc-array-form-table--row-group-start')
            ),
            3000,
            'the input element not preset'
        );
        parameters.forEach(val => {
            row.$$('tr.rc-array-form-table--row-group-start').each(ele => {
                ele.$$('input')
                    .count()
                    .then(count => {
                        if (count === val.length && count === 1) {
                            ele.$('input').clear();
                            ele.$('input').sendKeys(val[0]);
                        } else if (count === val.length && count === 2) {
                            ele.$$('input').each(function(e, index) {
                                e.clear();
                                e.sendKeys(val[index]);
                            });
                        } else if (count === 4) {
                            ele.$$('input').each(function(e, index) {
                                if (index === 3) {
                                    _this
                                        .auiselect(
                                            e.element(by.xpath('..')),
                                            $('.cdk-overlay-pane aui-tooltip')
                                        )
                                        .select(val[index - 1]);
                                } else if (index > 0) {
                                    e.clear();
                                    e.sendKeys(val[index - 1]);
                                }
                            });
                        }
                    });
            });
        });
    }
    /**
     * 存储卷配置字典
     * @param parameters 参数
     */
    configReference(parameters: Array<string>, auiRadioBtn) {
        const _this = this;
        const ele = this.getFormItem('', auiRadioBtn);
        CommonPage.waitElementNotPresent(ele.$('.zero-state-hint'), 3000);
        parameters.forEach(val => {
            ele.$$('tr.rc-array-form-table--row-group-start').each(function(e) {
                e.$$('input').each(function(input, index) {
                    if (index === 0) {
                        _this
                            .auiselect(
                                input.element(by.xpath('..')),
                                $('.cdk-overlay-pane aui-tooltip')
                            )
                            .select(val[index]);
                    } else {
                        input.clear();
                        input.sendKeys(val[index]);
                    }
                });
            });
        });
    }
    /**
     * dialog表单，适用于健康检查、存储卷内、内部路由
     * @param parameters
     */
    dialogForm(parameters: Object, auiRadioBtn: string = '') {
        const _this = this;
        const ele = this._selector.$('.aui-dialog');
        CommonPage.waitElementPresent(ele, 2000);
        for (const label in parameters) {
            if (label === '协议类型' || label === '类型' || label === '协议') {
                _this.protocol(label, parameters[label]).click();
            } else if (label === '端口') {
                if (!Array.isArray(parameters[label])) {
                    _this.input(label, parameters[label]);
                } else {
                    _this.clickAddButton(label);
                    _this.insertParams(label, parameters[label]);
                }
            } else if (label === '请求头') {
                _this.clickAddButton(label);
                _this.insertParams(label, parameters[label]);
            } else if (label === '持久卷声明') {
                const e = _this.getFormItem(label, auiRadioBtn);
                _this
                    .auiselect(e, $('.cdk-overlay-pane aui-tooltip'))
                    .select(parameters[label]);
            } else if (label === '配置字典') {
                const e = _this.getFormItem(label, auiRadioBtn);
                _this
                    .auiselect(e, $('.cdk-overlay-pane aui-tooltip'))
                    .select(parameters[label]);
            } else if (label === '引用配置项') {
                _this.networkModul(label, auiRadioBtn).open();
                _this.clickAddButton('', '.aui-button--primary', auiRadioBtn);
                _this.configReference(parameters[label], auiRadioBtn);
            } else {
                _this.input(label, parameters[label], auiRadioBtn);
            }
        }
    }
    /**
     * 确定按钮
     */
    confirm() {
        const btn = this._selector.$('aui-dialog-footer .aui-button--primary');
        CommonPage.waitElementClickable(btn, 2000);
        return btn;
    }
    /**
     * 取消按钮
     */
    cancel() {
        const btn = this._selector.$('aui-dialog-footer .aui-button--default');
        CommonPage.waitElementClickable(btn, 2000);
        return btn;
    }
}
