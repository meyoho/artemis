import { AcpPageBase } from '@acp/acp.page.base';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { $, by, element } from 'protractor';
import { AuiDropdownButton } from '@e2e/element_objects/alauda.aui_dropdown_button';

export class AppListPage extends AcpPageBase {
  get appTable() {
    return new AuiTableComponent(
      $('aui-card>.aui-card'),
      'aui-table[class*="aui-table"]',
      'aui-card>div>.aui-card__header',
    );
  }
  get image_selector() {
    return new ImageSelect($('div aui-dialog'));
  }
  clickYamlCreate() {
    return new AuiDropdownButton(
      $('aui-dropdown-button button+button'),
      $('aui-tooltip'),
    ).select('YAML创建');
  }
  /**
   * 根据应用名称获取相关的操作
   * @param appName 应用名称
   * @param actionName 操作
   */
  clickAction(appName: string, actionName: string) {
    this.appTable.searchByResourceName(appName, 1);
    return this.appTable.clickOperationButtonByRow(
      [appName],
      actionName,
      'button[class]',
      'aui-menu-item>button',
    );
  }

  /**
   * 列表页删除应用
   * @param appName 应用的名称
   */
  deleteAppByName(appName) {
    this.clickAction(appName, '删除');
    this.confirmDialog.clickConfirm();
  }

  /**
   * 点击应用名称进入详情页
   * @param appName 应用名称
   */
  toAppDetail(appName: string) {
    this.appTable.clickResourceNameByRow([appName], '名称');
    this.waitElementPresent(
      element(by.css('.aui-tab-label__content')),
      '等待页面跳转到应用详情页',
    );
  }

  /**
   * 检索
   * @param appName 应用名称
   * @param rowCount 检索后，表格中期望出现的行个数
   */
  search(appName: string, rowCount = 1) {
    this.appTable.searchByResourceName(appName, rowCount);
  }
}
