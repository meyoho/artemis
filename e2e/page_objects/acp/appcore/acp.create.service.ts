import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $, browser } from 'protractor';

import { ArrayFormTable } from '../../../element_objects/alauda.arrayFormTable';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AcpPageBase } from '../acp.page.base';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';

export class AppCreateService extends AcpPageBase {
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'label',
      $('aui-dialog ng-component'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '名称':
        return this.alaudaElement.getElementByText(text);
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        return this.alaudaElement.getElementByText(text, 'aui-switch');

      case '端口':
        return this.alaudaElement.getElementByText(
          text,
          'rc-array-form-table table',
        );
      case '工作负载':
        return new AuiSelect(
          super.getElementByText(text, 'aui-input-group'),
          $('aui-tooltip'),
        );
    }
    return this.alaudaElement.getElementByText(text, tagname);
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        if (String(value).toLowerCase() === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        } else {
          new AuiSwitch(this.getElementByText(name)).close();
        }
        break;
      case '端口':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
      case '工作负载':
        this.getElementByText(name).select(value[0], value[1]);
        break;
    }
    browser.sleep(100);
  }

  input(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
