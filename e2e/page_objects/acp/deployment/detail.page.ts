import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import {
  $,
  browser,
  by,
  element,
  $$,
  ElementFinder,
  promise,
} from 'protractor';

import { AlaudaAuiCodeEditor } from '../../../element_objects/alauda.aui_code_editor';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';
import { DeploymentListPage } from './list.page';
import { AuiIcon } from '@e2e/element_objects/alauda.aui_icon';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { DeploymentUpdatePage } from './update.page';
import { DialogPage } from './dialog.page';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { UpdateHpaPage } from './update.hpa.page';
import { AuiToolTip } from '@e2e/element_objects/alauda.auiToolTip';
import { PodDetailPage } from '../pods/pods.detail.page';

export class DetailDeploymentPage extends AcpPageBase {
  /**
   * 点击切换tab页
   * @param name tab 名称如YAML
   */
  clickTab(name: string) {
    browser.executeScript('window.scrollTo(0,0)');
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(string(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }

  /**
   * 获取容器组页
   */
  get podListTable() {
    this.clickTab('容器组');
    browser.sleep(1000);
    return new AuiTableComponent(
      $('rc-pod-list .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  /**
   * 获取yaml页
   */
  get alaudaCodeEdit() {
    this.clickTab('YAML');
    return new AlaudaAuiCodeEditor($('.aui-dialog__content aui-code-editor'));
  }

  get detailElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-detail-info .info-list li',
      'rc-detail-info .info-list li div',
      $('rc-detail-info .info-list'),
    );
  }
  get containerElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-pod-controller-containers  rc-field-set-item',
      'rc-pod-controller-containers  rc-field-set-item label',
      $('rc-pod-controller-containers aui-card .hasDivider'),
    );
  }
  healthCheckElement(kind: string): AlaudaElement {
    return new AlaudaElement(
      'rc-container-healthcheck-display .healthcheck-table-cell',
      '.healthcheck-table-cell-label',
      element(by.xpath(`//div[contains(text(),"${kind}")]/../..`)),
    );
  }
  get healthCheck() {
    return this.foldableBlock('健康检查');
  }

  foldableBlock(kind: string) {
    return new FoldableBlock(
      element(
        by.xpath(
          `//rc-foldable-block/div/div[contains(text(),"${kind}")]/../..`,
        ),
      ),
    );
  }
  get detailFoldableElement() {
    return new AlaudaElement(
      'rc-foldable-block',
      '.toggle-bar',
      $('rc-detail-info'),
    );
  }
  get containerFoldableElement() {
    return new AlaudaElement(
      'rc-foldable-block',
      '.toggle-bar',
      $('rc-pod-controller-containers'),
    );
  }
  waitStatus(status) {
    this.waitElementPresent(
      element(
        by.xpath(
          `//rc-workload-status-icon/rc-status-icon/span[contains(text(),"${status}")]`,
        ),
      ),
      `等待状态变为${status}`,
      180000,
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): ElementFinder | AlaudaTagsLabel {
    switch (text) {
      case '状态':
        return this.detailElement.getElementByText(
          text,
          'rc-workload-status-icon',
        );
      case '实例数':
        return $('rc-workload-status .rc-workload-status__total');
      case '标签':
      case '主机选择器':
      case '凭据':
        // case '固定 IP':
        return new AlaudaTagsLabel(
          this.detailElement.getElementByText(text, 'rc-tags-label'),
          'aui-tag>.aui-tag>span',
          '.tooltip-toggle',
          'aui-tooltip .tooltip-tag span',
        );
      case '节点异常策略':
      case '更新策略':
        return this.detailElement.getElementByText(text, 'span');
      case '镜像':
      case '资源限制':
      case '启动命令':
      case '参数':
        return this.containerElement.getElementByText(
          text,
          '.field-set-item__value',
        );
      case '存储卷':
      case 'Pod 亲和':
        this.foldableBlock(text).expand();
        return this.detailFoldableElement.getElementByText(
          text,
          'rc-array-form-table table',
        );
      case '端口':
      case '日志文件':
      case '已挂载存储卷':
        this.foldableBlock(text).expand();
        return this.containerFoldableElement.getElementByText(
          text,
          '.foldable-content table',
        );
      case '更新容器组标签':
        return this.detailElement.getElementByText('容器组标签', 'button');
      case '更新镜像':
        return this.containerElement.getElementByText('镜像', 'button');
      case '更新资源限制':
        return this.containerElement.getElementByText('资源限制', 'button');
      case '镜像版本':
        return this.updateImageElement.getElementByText(text, 'input');
      case '自动扩缩容':
        return $('rc-hpa .aui-card__content');
    }
  }

  get listPage(): DeploymentListPage {
    return new DeploymentListPage();
  }
  stop(name) {
    this.listPage.enterDetail(name);
    this.getButtonByText('停止').click();
    this.getButtonByText('确定').click();
  }
  start(name) {
    this.listPage.enterDetail(name);
    this.getButtonByText('开始').click();
    this.getButtonByText('确定').click();
  }
  scaleUp(name) {
    this.listPage.enterDetail(name);
    new AuiIcon($('.rc-workload-status__scaler__increase aui-icon')).click();
    browser.sleep(1000);
  }
  scaleDown(name) {
    this.listPage.enterDetail(name);
    new AuiIcon($('.rc-workload-status__scaler__decrease aui-icon')).click();
    browser.sleep(1000);
  }
  /**
   * 更新标签的对话框
   */
  get updateTable() {
    return new ArrayFormTable(
      $('.aui-dialog__content rc-array-form-table'),
      '.rc-array-form-table__bottom-control-buttons button',
    );
  }
  updateLabel(name, testData) {
    this.listPage.enterDetail(name);
    const label = this.getElementByText('更新容器组标签') as ElementFinder;
    label.click();
    this.updateTable.fill(testData);
    this.auiDialog.clickConfirm();
  }
  get updateImageElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-dialog__content  .aui-form-item',
      '.aui-form-item__label',
      $('.cdk-overlay-pane aui-dialog aui-dialog-content'),
    );
  }
  updateImage(name, tag) {
    this.listPage.enterDetail(name);
    const button_image = this.getElementByText('更新镜像') as ElementFinder;
    button_image.click();
    new AlaudaInputbox(this.getElementByText(
      '镜像版本',
    ) as ElementFinder).input(tag);
    this.auiDialog.clickConfirm();
  }
  updateLimit(name, value) {
    this.listPage.enterDetail(name);
    const button_rs = this.getElementByText('更新资源限制') as ElementFinder;
    button_rs.click();
    const resources = this.updateImageElement.getElementByText(
      '资源限制',
      'rc-resources-form',
    );
    // [{CPU: 10, 单位: 'm' }, {内存: 10, 单位: 'Mi'}]
    resources.$$('div[class*="input-groups"]').each((elem, index) => {
      const inputBoxGroup: AlaudaInputGroup = new AlaudaInputGroup(
        elem,
        '.addon-label',
        'input[type="number"]',
      );
      const item: { [key: string]: string } = value[index];
      for (const keyItem in item) {
        if (keyItem !== '单位') {
          inputBoxGroup.input(item[keyItem], item['单位']);
        }
      }
    });
    this.auiDialog.clickConfirm();
  }
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header  aui-icon[icon="angle_down"]'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  get updatePage(): DeploymentUpdatePage {
    return new DeploymentUpdatePage();
  }

  update(name, testData) {
    this.listPage.enterDetail(name);
    this.action.select('更新');
    this.updatePage.fillForm(testData);
    this.clickConfirm();
    this.conflictRetry();
  }
  delete(name, flag = 'delete') {
    this.listPage.enterDetail(name);
    this.action.select('删除');
    if (flag === 'cancel') {
      $('aui-confirm-dialog .aui-confirm-dialog__cancel-button').click();
    } else {
      $('aui-confirm-dialog .aui-confirm-dialog__confirm-button').click();
    }
  }
  /**
   * 获取配置页
   */
  get alaudaEnvTable() {
    this.clickTab('配置');
    this.waitElementPresent(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      '等待tab页切换',
    );
    return new AlaudaAuiTable(
      element(
        by.xpath(
          '//h3[contains(text(),"环境变量")]/following-sibling::table[1]',
        ),
      ),
      'thead td',
      'tbody td',
      'tbody tr',
    );
  }
  get alaudaConfigTable() {
    this.clickTab('配置');
    this.waitElementPresent(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      '等待tab页切换',
    );
    return new AlaudaAuiTable(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      'thead td',
      'tbody td',
      'tbody tr',
    );
  }
  get alaudaEnvUpdate() {
    this.clickTab('配置');
    this.waitElementPresent(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      '等待tab页切换',
    );
    return new AlaudaButton(
      element(by.xpath('//h3[contains(text(),"环境变量")]//button')),
    );
  }
  get alaudaConfigUpdate() {
    this.clickTab('配置');
    this.waitElementPresent(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      '等待tab页切换',
    );
    this.waitElementPresent(
      element(
        by.xpath(
          '//h3[contains(text(),"配置引用")]/following-sibling::table[1]',
        ),
      ),
      '等待tab页切换',
    );
    return new AlaudaButton(
      element(by.xpath('//h3[contains(text(),"配置引用")]//button')),
    );
  }
  get dialogPage(): DialogPage {
    return new DialogPage();
  }
  updateEnv(name, testData) {
    this.listPage.enterDetail(name);
    this.alaudaEnvUpdate.click();
    this.dialogPage.fillForm(testData);
    this.dialogPage.auiDialog.clickConfirm();
  }
  updateConfig(name, testData) {
    this.listPage.enterDetail(name);
    this.alaudaConfigUpdate.click();
    this.dialogPage.fillForm(testData);
    this.dialogPage.auiDialog.clickConfirm();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  get updateHpaPage(): UpdateHpaPage {
    return new UpdateHpaPage();
  }
  updateHpa(name, testData, flag = 'update') {
    this.listPage.enterDetail(name);
    new AlaudaButton($('rc-hpa .aui-card__header button')).click();
    this.updateHpaPage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
      case 'close':
        this.auiDialog.clickClose();
        break;
      default:
        this.auiDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  waitPodNotExist(podName, time_out = 30) {
    this.refreshPodList().then(() => {
      browser.sleep(2000);
    });
    browser.sleep(1).then(() => {
      for (let i = 0; i < time_out; i++) {
        element(by.xpath(`//aui-table-cell/a[contains(text(),'${podName}')]`))
          .isPresent()
          .then(ispresent => {
            if (ispresent === true) {
              browser.sleep(2000);
              this.refreshPodList().then(() => {
                browser.sleep(1000);
              });
            }
          });
      }
    });
  }
  reCreatePod(pod_line: number, flag = '确定', istapp = false) {
    return this.podListTable
      .getRowByIndex(pod_line)
      .$('aui-table-cell:first-child')
      .getText()
      .then(podName => {
        return this.podListTable
          .clickOperationButtonByRow(
            [podName],
            '删除',
            'button aui-icon[icon="basic:ellipsis_v_s"]',
            '.aui-menu>div>*',
          )
          .then(() => {
            if (flag === '确定') {
              this.confirmDialog.clickConfirm();
            } else if (flag === '取消') {
              this.confirmDialog.clickCancel();
            }
            if (!istapp) {
              this.waitPodNotExist(podName);
            }
            return podName;
          });
      });
  }
  getPodNameByRowIndex(rowIndex) {
    return this.podListTable.getRowCell(rowIndex, 0).getText();
  }
  getPodTooltip() {
    return new AuiToolTip($('aui-tooltip>.aui-tooltip--bottom'));
  }
  refreshPodList() {
    return new AlaudaButton($('rc-pod-list aui-icon[icon="basic:refresh"]'))
      .click()
      .then(() => {
        this.waitElementNotPresent(
          element(
            by.xpath(
              "//acl-k8s-resource-list-footer/div/span[contains(text(),'加载中')]",
            ),
          ),
          '等待列表刷新完成失败',
        );
      });
  }
  clickPodOperationOnly(podName) {
    return this.podListTable.clickOperationButtonOnly(
      [podName],
      'button aui-icon[icon="basic:ellipsis_v_s"]',
      '.aui-menu>div>*',
    );
  }
  podDropDownList(podName) {
    return new AlaudaDropdown(
      this.podListTable
        .getRow([podName])
        .$('button aui-icon[icon="basic:ellipsis_v_s"]'),
      $$('.aui-menu>div>*'),
    );
  }
  getPodYaml(podName): promise.Promise<string> {
    return this.podListTable.clickResourceNameByRow([podName]).then(() => {
      this.waitElementPresent(
        $('rc-pod-detail-info'),
        '等待页面跳转到容器组详情页',
      );
      return new PodDetailPage().Yaml;
    });
  }
  visitPodLog(podName: string, containerName: string) {
    return this.podListTable.clickOperationButtonByRow(
      [podName],
      ['查看日志', containerName],
      'button aui-icon[icon="basic:ellipsis_v_s"]',
      '.aui-menu>div>*',
    );
  }
}
