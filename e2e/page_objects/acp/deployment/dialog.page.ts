import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaAuiNumberInput } from '@e2e/element_objects/alauda.aui_number_input';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, ElementFinder, element, by, $$ } from 'protractor';

import { AcpPageBase } from '../acp.page.base';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';

export class DialogPage extends AcpPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get dialogElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-dialog__content  .aui-form-item',
      '.aui-form-item__label',
      $$('.cdk-overlay-pane aui-dialog')
        .last()
        .$('.aui-dialog__content'),
    );
  }
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `(//aui-dialog)[last()]//aui-dialog-content//aui-form-item//label[@class="aui-form-item__label" and  contains(text(),"${left}") ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      default:
        return this._getElementByText(text, 'input');
      case '协议类型':
      case '协议':
      case '亲和性':
      case '方式':
      case '类型':
        return this._getElementByText(text, 'aui-radio-group');
      case '请求头':
      case '启动命令':
        return this._getElementByText(text, 'rc-array-form-table');
      case '持久卷声明':
      case '配置字典':
      case '保密字典':
      case '亲和组件':
        return this._getElementByText(text, 'aui-select');
      case '配置引用':
      case '删除配置引用':
        return this._getElementByText('配置引用', 'aui-multi-select');
      case '亲和类型':
        return this._getElementByText('类型', 'aui-select');
      case '匹配标签':
        return this._getElementByText(text, 'rc-array-form-table');
      case '环境变量':
      case '环境变量添加引用':
        return $('.aui-dialog__content rc-array-form-table');
      case '权重':
        return this._getElementByText(text, 'aui-number-input');
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(String(value));
        break;
      case '协议类型':
      case '协议':
      case '亲和性':
      case '方式':
      case '类型':
        new AlaudaRadioButton(this.getElementByText(name)).clickByName(value);
        break;
      case '请求头':
      case '启动命令':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
      case '持久卷声明':
      case '配置字典':
      case '保密字典':
      case '亲和类型':
        new AuiSelect(this.getElementByText(name)).select(value);
        break;
      case '配置引用':
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v[0], v[1]);
        });
        break;
      case '删除配置引用':
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).unselect(v);
        });
        break;
      case '亲和组件':
        new AuiSelect(this.getElementByText(name)).select(value[0], value[1]);
        break;
      case '环境变量':
      case '匹配标签':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        break;
      case '环境变量添加引用':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
        ).fill(value);
        break;
      case '权重':
        new AlaudaAuiNumberInput(this.getElementByText(name)).input(value);
        break;
    }
  }
}
