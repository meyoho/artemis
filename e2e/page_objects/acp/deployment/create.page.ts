import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaAuiNumberInput } from '@e2e/element_objects/alauda.aui_number_input';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, $$, browser, ElementFinder, element, by } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

import { DialogPage } from './dialog.page';
import { DeploymentListPage } from './list.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaAuiTagsInput } from '@e2e/element_objects/alauda.aui_tags_input';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
export class DeploymentCreatePage extends AcpPageBase {
  get listPage(): DeploymentListPage {
    return new DeploymentListPage();
  }
  get dialogPage(): DialogPage {
    return new DialogPage();
  }

  _getElementByText(
    left: string,
    tagname = 'input',
    root_xpath = '/',
  ): ElementFinder {
    const xpath = `${root_xpath}/aui-form-item//label[@class="aui-form-item__label" and normalize-space(text())="${left}" ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    const base_ele = new AlaudaElementBase();
    base_ele.scrollToView(element(by.xpath(xpath)));
    return element(by.xpath(xpath));
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      default:
        return this._getElementByText(text, 'input');
      case '实例数量':
        return this._getElementByText(text, 'aui-number-input');
      case 'Tapp - 高级':
        return $(
          'rc-tapp-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '部署 - 高级':
        return $(
          'rc-workload-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '容器 - 高级':
        return $(
          'rc-container-form  rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '容器组 - 高级':
        return $(
          'rc-pod-template-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '更新策略':
        return this._getElementByText(
          text,
          'div[@class="aui-form-item__content"]',
        );
      case '镜像地址':
        return this._getElementByText(text, 'button');
      case '组件标签':
        return this._getElementByText('标签', 'rc-array-form-table');
      case '标签':
      case '启动命令':
      case '参数':
      case '日志文件':
      case '排除日志文件':
      case '环境变量':
      case '存储卷挂载':
      case '容器组标签':
      case '端口':
        return this._getElementByText(text, 'rc-array-form-table');
      case '环境变量添加引用':
        return this._getElementByText('环境变量', 'rc-array-form-table');
      case '配置引用':
      case '主机选择器':
      case '凭据':
        return this._getElementByText(text, 'aui-multi-select');
      case 'Host 模式':
        return this._getElementByText(text, 'aui-switch');
      case '亲和性':
        return this._getElementByText(
          text,
          'button[contains(@class,"aui-button--primary")]',
        );
      case '存储卷':
        return this._getElementByText(
          text,
          'div[@class="rc-array-form-table__bottom-control-buttons"]/button',
        );
      case '子网':
        return this._getElementByText(text, 'aui-select');
      case '固定 IP':
        return this._getElementByText(text, 'aui-tags-input');
      case '添加容器':
        return $('rc-pod-template-form  aui-tab-header button:nth-child(1)');
      case '添加初始化容器':
        return $(
          'rc-pod-template-form  aui-tab-header aui-dropdown-button aui-icon',
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      case '实例数量':
        return new AlaudaAuiNumberInput(this.getElementByText(name)).input(
          value,
        );
      case '节点异常策略':
        new FoldableBlock(this.getElementByText('Tapp - 高级')).expand();
        const tapp_node_error_role = new AlaudaRadioButton(
          this._getElementByText('节点异常策略', 'aui-radio-group'),
          '.aui-radio-button__content',
        );
        tapp_node_error_role.clickByName(value);
        break;
      case 'Tapp更新策略':
        new FoldableBlock(this.getElementByText('Tapp - 高级')).expand();
        this.getElementByText('更新策略')
          .$$('input')
          .each((elem, index) => {
            new AlaudaInputbox(elem).input(String(value[index]));
          });
        new FoldableBlock(this.getElementByText('Tapp - 高级')).fold();
        break;
      case 'Tapp组件标签':
        new FoldableBlock(this.getElementByText('Tapp - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText('标签'),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('Tapp - 高级')).fold();
        break;
      case '更新策略':
        new FoldableBlock(this.getElementByText('部署 - 高级')).expand();
        this.getElementByText(name)
          .$$('input')
          .each((elem, index) => {
            new AlaudaInputbox(elem).input(String(value[index]));
          });
        new FoldableBlock(this.getElementByText('部署 - 高级')).fold();
        break;
      case '组件标签':
        new FoldableBlock(this.getElementByText('部署 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText('标签'),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('部署 - 高级')).fold();
        break;
      case '镜像地址':
        this.getElementByText(name).click();
        this.imageSelect.enterImage(value, '输入', '');
        break;
      case '资源限制':
        const resources = this._getElementByText(name, 'rc-resources-form');
        // {资源限制: [{CPU: 10, 单位: 'm' }, {内存: 10, 单位: 'Mi'}]
        resources.$$('div[class*="input-groups"]').each((elem, index) => {
          const inputBoxGroup: AlaudaInputGroup = new AlaudaInputGroup(
            elem,
            '.addon-label',
            'input[formcontrolname="value"]',
          );
          inputBoxGroup.scrollToView(inputBoxGroup.inputbox);
          const item: { [key: string]: string } = value[index];
          for (const keyItem in item) {
            if (keyItem !== '单位') {
              inputBoxGroup.input(item[keyItem], item['单位']);
            }
          }
        });
        break;
      case '启动命令':
      case '参数':
      case '日志文件':
      case '排除日志文件':
      case '环境变量':
      case '存储卷挂载':
      case '端口':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '环境变量添加引用':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '配置引用':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v[0], v[1]);
        });
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '添加存活性健康检查':
      case '添加可用性健康检查':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        this.getButtonByText(name).click();
        browser.sleep(1000);
        this.dialogPage.fillForm(value);
        this.dialogPage.auiDialog.clickConfirm();
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '存储卷':
        value.forEach(v => {
          new AlaudaButton(this.getElementByText(name)).click();
          this.dialogPage.fillForm(v);
          this.dialogPage.auiDialog.clickConfirm();
        });
        break;
      case '凭据':
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v);
        });
        break;
      case '容器组标签':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '主机选择器':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v);
        });
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '亲和性':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        value.forEach(v => {
          new AlaudaButton(this.getElementByText(name)).click();
          this.dialogPage.fillForm(v);
          this.dialogPage.auiDialog.clickConfirm();
        });
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case 'Host 模式':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        if (value === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        }
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '子网':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        new AuiSelect(this.getElementByText(name)).select(value);
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '固定 IP':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        value.forEach(ip => {
          new AlaudaAuiTagsInput(this.getElementByText(name)).input(ip);
        });
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '容器名称':
        new AlaudaInputbox(
          this._getElementByText('名称', 'input', '//rc-pod-template-form/'),
        ).input(value);
        break;
      case '组件名称':
        new AlaudaInputbox(
          this._getElementByText('名称', 'input', '//rc-tapp-form/form'),
        ).input(value);
        break;
      case '镜像':
        this.imageSelect.enterImage(value, '输入', '');
        break;
      case '添加容器':
        value.forEach(data => {
          new AlaudaButton(this.getElementByText(name)).click();
          this.fillForm(data);
        });
        break;
      case '添加初始化容器':
        value.forEach(data => {
          new AlaudaDropdown(
            this.getElementByText(name),
            $$('.cdk-overlay-pane aui-tooltip .aui-menu-item'),
          ).select(name);
          this.fillForm(data);
        });
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(String(value));
        break;
    }
  }

  get imageSelect() {
    return new ImageSelect($('.cdk-global-overlay-wrapper aui-dialog'));
  }
  create(testData, image, method = '输入', tag = '') {
    this.clickLeftNavByText('部署');
    this.listPage.getButtonByText('创建部署').click();
    this.imageSelect.enterImage(image, method, tag);
    this.fillForm(testData);
    this.getButtonByText('确定').click();
    this.waitElementPresent(
      $('rc-workload-detail  rc-detail-info  .aui-card__header>span'),
      'the workload detail not present',
      2000,
    );
  }
  get yamlCreateDropDown() {
    return new AlaudaDropdown(
      $('.aui-card__header aui-dropdown-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip .aui-menu-item'),
    );
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
  }

  createByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('部署');
    this.yamlCreateDropDown.select('YAML创建');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        new AlaudaButton($('.form-footer button[aui-button=""]')).click();
        browser.sleep(100);
        break;
      default:
        new AlaudaButton(
          $('.form-footer button[aui-button="primary"]'),
        ).click();
        browser.sleep(100);
        break;
    }
  }
}
