import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $, ElementFinder, browser } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AcpPageBase } from '../acp.page.base';
import { DeploymentUpdatePage } from './update.page';

export class DeploymentListPage extends AcpPageBase {
  /**
   * 部署列表
   */
  get WorkloadTable() {
    return new AuiTableComponent(
      $('.layout-page-content aui-card'),
      'aui-table',
      '.aui-card__header',
    );
  }

  get noData(): AlaudaLabel {
    return new AlaudaLabel($('.empty-placeholder'));
  }
  get tableFooterText() {
    return $('acl-k8s-resource-list-footer').getText();
  }
  /**
   * 检索部署
   * @param name 部署名称
   * @param row_count 期望检索到的行数，检索后会等待表格中行数据变为row_count,直到等待超时
   * @example
   * this.search('example')
   */
  search(name, row_count = 1) {
    this.clickLeftNavByText('部署');
    this.WorkloadTable.searchByResourceName(name, row_count);
  }

  /**
   * 进入部署的详情页
   * @param name 部署的名称
   * @example
   * this.enterDetail('example')
   */
  enterDetail(name) {
    this.search(name);
    this.WorkloadTable.clickResourceNameByRow([name], '名称');
    // 详情页，左上角部署名称控件
    const nameElem = $('.aui-card__header>span');
    this.waitElementPresent(nameElem, '详情页没有打开');
  }

  /**
   * 表格中操作列选择删除操作，删除部署
   * @param name 部署名称
   * @example
   * this.delete('example')
   */
  delete(name, flag = 'delete') {
    this.search(name);
    this.WorkloadTable.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button__content aui-icon',
    );
    if (flag === 'cancel') {
      this.confirmDialog.clickCancel();
    } else {
      this.confirmDialog.clickConfirm();
    }
  }
  get updatePage(): DeploymentUpdatePage {
    return new DeploymentUpdatePage();
  }
  update(name, testData) {
    this.search(name);
    this.WorkloadTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button__content aui-icon',
    );
    this.updatePage.fillForm(testData);
    this.clickConfirm();
  }

  private _wait(ele: ElementFinder, status: string, timeout = 60000) {
    const refreshBtn = new AlaudaButton($('.basic-refresh'));
    return browser.driver
      .wait(() => {
        browser.sleep(1000);
        refreshBtn.click();
        this.waitElementPresent(ele, '', 3000);
        return ele.getText().then(txt => {
          return txt.replace(/\s/g, '').includes(status);
        });
      }, timeout)
      .then(
        () => true,
        () => {
          console.warn(`状态与期望值(${status})不匹配`);
          return false;
        },
      );
  }
  waitStatus(name: string, status: string, timeout = 30000) {
    this.search(name);
    this.WorkloadTable.getCell('状态', [name]).then(ele => {
      this._wait(ele, status, timeout);
    });
  }
}
