import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { $, ElementFinder } from 'protractor';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';

export class UpdateHpaPage extends AcpPageBase {
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'label',
      $('aui-dialog>.aui-dialog'),
    );
  }

  getElementByText(text: string): ElementFinder {
    switch (text) {
      case '类型':
        return $('aui-dialog-content aui-radio-group');
      case '定时调节':
        return $(
          'aui-dialog-content rc-hpa-cron-rules-form rc-array-form-table',
        );
      case '触发策略':
        return this.alaudaElement.getElementByText(text, 'rc-array-form-table');
      default:
        return this.alaudaElement.getElementByText(text, 'input');
    }
  }
  enterValue(
    name: string,
    value: string | Array<Array<string>> | { [key: string]: string },
  ) {
    switch (name) {
      case '类型':
        new AlaudaRadioButton(
          this.getElementByText(name),
          '.aui-radio__content',
        ).clickByName(value as string);
        break;
      case '定时调节':
        new AlaudaRadioButton(
          this.getElementByText('类型'),
          '.aui-radio__content',
        ).clickByName(name as string);
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons',
        ).fill(value as Array<Array<string>>);
        break;
      case '指标调节':
        new AlaudaRadioButton(
          this.getElementByText('类型'),
          '.aui-radio__content',
        ).clickByName(name as string);
        for (const key in value as { [key: string]: string }) {
          if (key === '触发策略') {
            new ArrayFormTable(
              this.getElementByText(key),
              '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
            ).fill(value[key]);
          } else {
            new AlaudaInputbox(this.getElementByText(key)).input(value[key]);
          }
        }
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value as string);
        break;
    }
  }
}
