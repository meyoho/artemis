import { DeploymentDetailVerify } from '@e2e/page_verify/acp/deployment/detail.page';
import { DeploymentListVerify } from '@e2e/page_verify/acp/deployment/list.page';

import { AcpPageBase } from '../acp.page.base';

import { DeploymentCreatePage } from './create.page';
import { DetailDeploymentPage } from './detail.page';
import { DeploymentListPage } from './list.page';
import { PreparePage } from './prepare';
import { DeploymentUpdatePage } from './update.page';

export class DeploymentPage extends AcpPageBase {
  get listPage(): DeploymentListPage {
    return new DeploymentListPage();
  }

  get createPage(): DeploymentCreatePage {
    return new DeploymentCreatePage();
  }
  get updatePage(): DeploymentUpdatePage {
    return new DeploymentUpdatePage();
  }

  get listPageVerify(): DeploymentListVerify {
    return new DeploymentListVerify();
  }
  get detailPage(): DetailDeploymentPage {
    return new DetailDeploymentPage();
  }
  get detailPageVerify(): DeploymentDetailVerify {
    return new DeploymentDetailVerify();
  }

  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
