import { CommonKubectl } from '@e2e/utility/common.kubectl';
import {
  k8s_type_deployments,
  k8s_type_horizontalpodautoscalers,
  k8s_type_cronhpas,
} from '@e2e/utility/resource.type.k8s';

import { AcpPreparePage } from '../prepare.page';

export class PreparePage extends AcpPreparePage {
  get cronhpaIsEnabled() {
    return this.featureIsEnabled('cron-hpa');
  }
  delete(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_deployments,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
  deleteHpa(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_horizontalpodautoscalers,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
  deleteCronHpa(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_cronhpas,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
}
