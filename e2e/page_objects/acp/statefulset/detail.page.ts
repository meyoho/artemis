import { DetailDeploymentPage } from '../deployment/detail.page';
import { StatefulSetListPage } from './list.page';

export class DetailStatefulSetPage extends DetailDeploymentPage {
  get listPage(): StatefulSetListPage {
    return new StatefulSetListPage();
  }
}
