import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { k8s_type_statefulsets } from '@e2e/utility/resource.type.k8s';

import { AcpPreparePage } from '../prepare.page';

export class PreparePage extends AcpPreparePage {
  delete(name: string) {
    CommonKubectl.deleteResource(
      k8s_type_statefulsets,
      name,
      this.clusterName,
      this.namespace1Name,
    );
  }
}
