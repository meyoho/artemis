import { StatefulSetDetailVerify } from '@e2e/page_verify/acp/statefulset/detail.page';
import { StatefulSetListVerify } from '@e2e/page_verify/acp/statefulset/list.page';

import { AcpPageBase } from '../acp.page.base';

import { StatefulSetCreatePage } from './create.page';
import { DetailStatefulSetPage } from './detail.page';
import { StatefulSetListPage } from './list.page';
import { PreparePage } from './prepare';

export class StatefulSetPage extends AcpPageBase {
  get listPage(): StatefulSetListPage {
    return new StatefulSetListPage();
  }

  get createPage(): StatefulSetCreatePage {
    return new StatefulSetCreatePage();
  }

  get listPageVerify(): StatefulSetListVerify {
    return new StatefulSetListVerify();
  }
  get detailPageVerify(): StatefulSetDetailVerify {
    return new StatefulSetDetailVerify();
  }
  get detailPage(): DetailStatefulSetPage {
    return new DetailStatefulSetPage();
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
