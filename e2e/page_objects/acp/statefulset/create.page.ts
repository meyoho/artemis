import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { $, browser } from 'protractor';

import { DeploymentCreatePage } from '../deployment/create.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { StatefulSetListPage } from './list.page';
export class StatefulSetCreatePage extends DeploymentCreatePage {
  get listPage(): StatefulSetListPage {
    return new StatefulSetListPage();
  }
  get imageSelect() {
    return new ImageSelect($('.cdk-global-overlay-wrapper aui-dialog'));
  }
  create(testData, image, method = '输入', tag = '') {
    this.clickLeftNavByText('有状态副本集');
    this.listPage.getButtonByText('创建有状态副本集').click();
    this.imageSelect.enterImage(image, method, tag);
    this.fillForm(testData);
    this.getButtonByText('确定').click();
    //传经成功后，跳转到详情页
    this.waitElementPresent(
      $('rc-workload-detail  rc-detail-info  .aui-card__header>span'),
      'the workload detail not present',
      2000,
    );
  }
  createByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('有状态副本集');
    this.yamlCreateDropDown.select('YAML创建');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        new AlaudaButton($('.form-footer button[aui-button=""]')).click();
        browser.sleep(100);
        break;
      default:
        new AlaudaButton(
          $('.form-footer button[aui-button="primary"]'),
        ).click();
        browser.sleep(100);
        break;
    }
  }
}
