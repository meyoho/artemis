import { DeploymentListPage } from '../deployment/list.page';

export class StatefulSetListPage extends DeploymentListPage {
  /**
   * 检索有状态副本集
   * @param StatefulSetName 有状态副本集名称
   * @param row_count 期望检索到的行数，检索后会等待表格中行数据变为row_count,直到等待超时
   * @example
   * this.search('example')
   */
  search(name, row_count = 1) {
    this.clickLeftNavByText('有状态副本集');
    this.WorkloadTable.searchByResourceName(name, row_count);
  }
}
