import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { $, browser } from 'protractor';

import { DeploymentCreatePage } from '../deployment/create.page';
import { DaemonSetListPage } from './list.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
export class DaemonSetCreatePage extends DeploymentCreatePage {
  get listPage(): DaemonSetListPage {
    return new DaemonSetListPage();
  }
  get imageSelect() {
    return new ImageSelect($('.cdk-global-overlay-wrapper aui-dialog'));
  }
  create(testData, image, method = '输入', tag = '') {
    this.clickLeftNavByText('守护进程集');
    this.listPage.getButtonByText('创建守护进程集').click();
    this.imageSelect.enterImage(image, method, tag);
    this.fillForm(testData);
    this.getButtonByText('确定').click();
    this.waitElementPresent(
      $('rc-workload-detail  rc-detail-info  .aui-card__header>span'),
      'the workload detail not present',
      5000,
    );
  }
  createByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('守护进程集');
    this.yamlCreateDropDown.select('YAML创建');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        new AlaudaButton($('.form-footer button[aui-button=""]')).click();
        browser.sleep(100);
        break;
      default:
        new AlaudaButton(
          $('.form-footer button[aui-button="primary"]'),
        ).click();
        browser.sleep(100);
        break;
    }
  }
}
