import { DetailDeploymentPage } from '../deployment/detail.page';
import { DaemonSetListPage } from './list.page';

export class DetailDaemonSetPage extends DetailDeploymentPage {
  get listPage(): DaemonSetListPage {
    return new DaemonSetListPage();
  }
}
