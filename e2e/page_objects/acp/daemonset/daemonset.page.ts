import { DaemonSetDetailVerify } from '@e2e/page_verify/acp/daemonset/detail.page';
import { DaemonSetListVerify } from '@e2e/page_verify/acp/daemonset/list.page';

import { AcpPageBase } from '../acp.page.base';

import { DaemonSetCreatePage } from './create.page';
import { DetailDaemonSetPage } from './detail.page';
import { DaemonSetListPage } from './list.page';
import { PreparePage } from './prepare';

export class DaemonSetPage extends AcpPageBase {
  get listPage(): DaemonSetListPage {
    return new DaemonSetListPage();
  }

  get createPage(): DaemonSetCreatePage {
    return new DaemonSetCreatePage();
  }

  get listPageVerify(): DaemonSetListVerify {
    return new DaemonSetListVerify();
  }
  get detailPageVerify(): DaemonSetDetailVerify {
    return new DaemonSetDetailVerify();
  }
  get detailPage(): DetailDaemonSetPage {
    return new DetailDaemonSetPage();
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
