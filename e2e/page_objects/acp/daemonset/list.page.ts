import { DeploymentListPage } from '../deployment/list.page';

export class DaemonSetListPage extends DeploymentListPage {
  /**
   * 检索守护进程集
   * @param DaemonSetName 守护进程集名称
   * @param row_count 期望检索到的行数，检索后会等待表格中行数据变为row_count,直到等待超时
   * @example
   * this.search('example')
   */
  search(DaemonSetName, row_count = 1) {
    this.clickLeftNavByText('守护进程集');
    this.WorkloadTable.searchByResourceName(DaemonSetName, row_count);
  }
}
