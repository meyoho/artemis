import { CreateResourcePage } from './create.page';
import { browser } from 'protractor';

export class UpdateResourcePage extends CreateResourcePage {
  /**
   * 更新资源
   * @param name 名称
   * @param stringyaml 更新数据
   */
  updateIngress(name, namespace_name) {
    this.clickLeftNavByText('资源管理');
    this.listPage.resource.search('Ingress');
    this.listPage.searchResource(name, namespace_name);
    this.listPage.clickupdate(name);
    browser.sleep(1000);

    this.alaudaCodeEdit.getYamlValue().then(yaml => {
      const updateyaml = yaml.replace(
        'metadata:\n',
        'metadata:\n  labels:\n    update: update\n',
      );
      this.alaudaCodeEdit.setYamlValue(updateyaml);
    });
    browser.sleep(1000);
    this.buttonCreate.click();
    this.confirmDialog.clickConfirm();
    browser.sleep(1000);
  }

  /**
   * 更新资源
   * @param name 名称
   * @param stringyaml 更新数据
   */
  updateNs(name) {
    this.clickLeftNavByText('资源管理');
    this.listPage.searchCategory('Namespace');
    this.listPage.searchResource(name);
    this.listPage.clickupdate(name);

    this.alaudaCodeEdit.getYamlValue().then(yaml => {
      const updateyaml = yaml.replace('labels:\n', 'labels:\n    a: a\n');
      this.alaudaCodeEdit.setYamlValue(updateyaml);
    });

    this.buttonCreate.click();
    this.confirmDialog.clickConfirm();
  }
}
