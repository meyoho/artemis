import { ServerConf } from '@e2e/config/serverConf';
import { Resource } from '@e2e/element_objects/acp/resourcemanagement/resource';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$, ElementFinder, browser, protractor } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AcpPageBase } from '../acp.page.base';

export class ListPage extends AcpPageBase {
  /**
   * 资源管理列表
   */
  get ResourceNameTable() {
    return new AuiTableComponent(
      $('alu-resource-management-list .aui-card'),
      'aui-table',
      'aui-search',
    );
  }

  get noData(): ElementFinder {
    const noData = $('.empty-placeholder');
    this.waitElementPresent(noData, '无数据控件没出现');
    return noData;
  }

  get resource() {
    const elemResource = $('alu-resource-management-list>section:nth-child(1)');
    this.waitElementPresent(
      elemResource,
      `资源列表没加载出来${elemResource.locator()}`,
    );
    return new Resource(elemResource);
  }

  get namespaceSelect() {
    return new AuiSelect(
      $('.resource__list aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }

  selectNamespace(name) {
    this.namespaceSelect.inputBox.inputbox.click();
    this.namespaceSelect.inputBox.input(name);
    this.namespaceSelect.inputBox.inputbox.sendKeys(protractor.Key.ENTER);
    //this.namespaceSelect.select(name);
    return browser.sleep(1000);
  }
  get resourceSearch() {
    return new AuiSearch($('.aui-card__header aui-search'));
  }
  /**
   * 点击名称，打开资源的YAML 详情页
   * @param name 名称
   */
  clickview(name: string) {
    browser.sleep(1000);
    this.ResourceNameTable.clickResourceNameByRow([name]);
    return browser.sleep(1000);
  }

  /**
   * 点击操作栏的删除按钮
   * @param name 名称
   */
  clickdelete(name: string) {
    this.ResourceNameTable.clickOperationButtonByRow(
      [name],
      '删除',
      'aui-table-row aui-icon[icon="basic:ellipsis_v_s"]',
    );
  }
  /**
   * 点击操作栏的更新按钮
   * @param name 名称
   */
  clickupdate(name: string) {
    return this.ResourceNameTable.clickOperationButtonByRow(
      [name],
      '更新',
      'aui-table-row aui-icon[icon="basic:ellipsis_v_s"]',
    );
  }

  openResourceListPage() {
    this.switchSetting('平台管理');
    this.loginPage.waitLoadingDisappear();

    this.waitElementPresent($('div aui-breadcrumb'), '');
    this.clickLeftNavByText('资源管理');
    //this.waitElementNotPresent($('.loading span'), '');
    return browser.sleep(1000);
  }

  /**
   * 按名称过滤资源类型
   * @param type 资源类型
   */
  searchCategory(type, region_name = ServerConf.REGIONNAME) {
    this.openResourceListPage();
    this.switchCluster_onAdminView(region_name).then(() => {
      this.waitElementPresent(
        $('.aui-card__content aui-table'),
        '等待 10s后，列表页未显示',
        10000,
      );
    });
    browser.sleep(1000);
    this.resource.search(type);
    return browser.sleep(1);
  }
  /**
   * 按名称搜索资源对象
   * @param namespace 对象所在命名空间
   * @param name 资源对象
   */
  searchResource(name, namespace = '') {
    if (namespace !== '') {
      this.selectNamespace(namespace);
    }
    this.resourceSearch.search(name);
    return browser.sleep(1000);
  }
  /**
   * 批量删除
   * @param region_name 资源类型
   * @param type 资源类型
   * @param name 模糊搜索的名称
   * @param namespace 对象所在命名空间
   */
  batchDelete(type, name, region_name = ServerConf.REGIONNAME, namespace = '') {
    this.searchCategory(type, region_name);
    this.searchResource(name, namespace);
    const rowlist = this.ResourceNameTable.auiTable.$$('aui-table-row');
    rowlist.each(row => {
      const option = new AlaudaDropdown(
        row.$('aui-table-row aui-icon[icon="basic:ellipsis_v_s"]'),
        $$('aui-menu-item button'),
      );
      option.select('删除');
      browser.sleep(500);
      this.confirmDialog.clickConfirm();
      browser.sleep(500);
    });
    return browser.sleep(1000);
  }
}
