import { DetailPageVeriy } from '@e2e/page_verify/acp/resourcemanagement/detail.page';
import { ListPageVerify } from '@e2e/page_verify/acp/resourcemanagement/list.page';

import { AcpPageBase } from '../acp.page.base';

import { CreateResourcePage } from './create.page';
import { DeleteResourcePage } from './delete.page';
import { DetailResourcePage } from './detail.page';
import { ListPage } from './list.page';
import { PreparePage } from './prepare.page';
import { UpdateResourcePage } from './update.page';

export class ResourceManagementPage extends AcpPageBase {
    get preparePage(): PreparePage {
        return new PreparePage();
    }
    /**
     * 创建页
     */
    get createPage(): CreateResourcePage {
        return new CreateResourcePage();
    }
    /**
     * 详情页
     */
    get detailPage(): DetailResourcePage {
        return new DetailResourcePage();
    }
    get detailPageVeriy(): DetailPageVeriy {
        return new DetailPageVeriy();
    }
    /**
     * 更新页
     */
    get updatePage(): UpdateResourcePage {
        return new UpdateResourcePage();
    }
    /**
     * 列表页
     */
    get listPage(): ListPage {
        return new ListPage();
    }

    get listPageVerify(): ListPageVerify {
        return new ListPageVerify();
    }

    /**
     * 删除页
     */
    get deletePage(): DeleteResourcePage {
        return new DeleteResourcePage();
    }
}
