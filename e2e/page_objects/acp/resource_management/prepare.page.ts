import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';

export class PreparePage extends AcpPageBase {
  deleteIngress(ingress_name: string, namespace_name: string) {
    CommonKubectl.deleteResource(
      'Ingress',
      ingress_name,
      this.clusterName,
      namespace_name,
    );
  }
  deleteNs(name) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete namespace ${name} --grace-period=0 --force`,
      this.clusterName,
    );
  }
  getIngressYaml(name: string, type: string, namespace_name: string) {
    switch (type) {
      case 'Ingress':
        return CommonMethod.readTemplateFile('alauda.resource_ingress.yaml', {
          ingress_name: name,
          namespace: namespace_name,
        });
      case 'Namespace':
        return CommonMethod.readTemplateFile('alauda.resource_namespace.yaml', {
          cluster: this.clusterName,
          project: this.projectName,
          ns_name: namespace_name,
        });
    }
  }
}
