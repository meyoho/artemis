import { AlaudaConfirmDialog } from '@e2e/element_objects/alauda.confirmdialog';
import { by, browser } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

import { ListPage } from './list.page';

export class DeleteResourcePage extends AcpPageBase {
  get deleteDialog() {
    return new AlaudaConfirmDialog(
      by.css('aui-dialog aui-confirm-dialog'),
      '.aui-confirm-dialog__title',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
    );
  }

  get listPage(): ListPage {
    return new ListPage();
  }

  /**
   * 删除资源
   * @param name 名称
   */
  deleteResource(name, namespace_name, type = 'Ingress') {
    this.clickLeftNavByText('资源管理');

    switch (type) {
      case 'Ingress':
        this.listPage.resource.search('Ingress');
        this.listPage.searchResource(name, namespace_name);
        break;
      case 'Namespace':
        this.listPage.resource.search('Namespace');
        this.listPage.searchResource(name);
        break;
    }

    this.listPage.clickdelete(name);
    browser.sleep(1000);
    this.confirmDialog.clickConfirm();
    return browser.sleep(1000);
  }
}
