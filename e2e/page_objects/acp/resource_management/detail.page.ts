import { $ } from 'protractor';

import { AlaudaAuiCodeEditor } from '../../../element_objects/alauda.aui_code_editor';
import { AcpPageBase } from '../acp.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class DetailResourcePage extends AcpPageBase {
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('aui-dialog-content aui-code-editor'));
  }
  get closeButton() {
    return new AlaudaButton($('aui-dialog-header aui-icon'));
  }
}
