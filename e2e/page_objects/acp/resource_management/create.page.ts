import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $, browser } from 'protractor';

import { AcpPageBase } from '../acp.page.base';

import { ListPage } from './list.page';
import { PreparePage } from './prepare.page';

export class CreateResourcePage extends AcpPageBase {
  /**
   * 列表页面
   */
  get listPage(): ListPage {
    return new ListPage();
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-page__content aui-code-editor'));
  }

  /**
   * 创建按钮
   */
  get buttonCreate() {
    return new AlaudaButton($('.form__footer button[class*="primary"]'));
  }

  /**
   * 取消按钮
   */
  get buttonCancel() {
    return new AlaudaButton($('.form__footer button:not([class*="primary"])'));
  }

  /**
   * 添加资源
   * @param stringyaml 创建数据
   */
  create(name, type, namespace_name) {
    this.clickLeftNavByText('资源管理');
    browser.sleep(3000);
    // this.listPage.resource.search(type);
    // switch (type) {
    //   case 'Ingress':
    //     this.listPage.selectNamespace(namespace_name);
    //     break;
    //   case 'Namespace':
    //     break;
    // }
    this.getButtonByText('创建资源对象').click();

    this.alaudaCodeEdit.setYamlValue(
      this.preparePage.getIngressYaml(name, type, namespace_name),
    );
    this.buttonCreate.click();
    this.confirmDialog.clickConfirm();
    return browser.sleep(1);
  }

  // /**
  //  * 添加资源
  //  * @param stringyaml 创建数据
  //  */
  // addIngress(ingress_name, namespace_name) {
  //     this.clickLeftNavByText('资源管理');
  //     this.listPage.searchCategory('Ingress');
  //     this.listPage.selectNamespace(namespace_name);
  //     this.getButtonByText('创建资源对象').click();

  //     this.alaudaCodeEdit.setYamlValue(
  //         this.preparePage.getIngressYaml(ingress_name, namespace_name)
  //     );
  //     this.buttonCreate.click();
  //     this.confirmDialog.clickConfirm();
  // }
}
