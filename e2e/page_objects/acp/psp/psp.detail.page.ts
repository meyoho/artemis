import { AcpPageBase } from '../acp.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { element, by, $, $$, browser } from 'protractor';
import { PSPEditPage } from './psp.edit.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
export class PSPDetailPage extends AcpPageBase {
  private root = $('ng-component aui-card');
  get editPage() {
    return new PSPEditPage();
  }
  get isNotHavePSP() {
    return new AlaudaButton(
      element(by.xpath('//button/span[normalize-space(text())="立即配置"]')),
    ).isPresent();
  }
  get HelpInfo() {
    return this.isNotHavePSP.then(ishave => {
      if (ishave) {
        return $('.acl-page-guard--unavailable h3+p').getText();
      } else {
        throw new Error('pod 安全策略已配置，无法查看帮助信息');
      }
    });
  }
  getSectionByText(text) {
    return new AlaudaElement(
      'aui-form-item',
      '.aui-form-item__label',
      element(
        by.xpath(
          `//rc-psp-form/section/div/span[normalize-space(text())="${text}"]/../..`,
        ),
      ),
    );
  }
  /**
   *
   * @param section 大的模块如基本策略、主机路径策略、主机端口策略、用户运行策略
   * @param text 文本
   */
  getElementByText(section, text) {
    switch (section) {
      case '基本策略':
        switch (text) {
          case '默认允许':
          case '默认禁止':
            this.waitElementPresent(
              element(by.xpath(`//span[contains(text(),'${text}')]/../..`)),
              '元素未出现',
            );
            return element(
              by.xpath(`//span[contains(text(),'${text}')]/../..`),
            );
          default:
            this.waitElementPresent(
              element(
                by.xpath(
                  `//span[contains(text(),'${text}')]/preceding-sibling::aui-switch/div`,
                ),
              ),
              '元素未出现',
            );
            return element(
              by.xpath(
                `//span[contains(text(),'${text}')]/preceding-sibling::aui-switch/div`,
              ),
            );
        }
        break;
      case '主机路径策略（allowedHostPaths）':
      case '主机端口策略（hostPorts）':
      case '用户运行策略（runAsUser）':
        switch (text) {
          case '范围':
            this.waitElementPresent(
              this.getSectionByText(section).getElementByText(
                text,
                '.aui-form-item__content',
              ),
              '元素未出现',
            );
            return this.getSectionByText(section).getElementByText(
              text,
              '.aui-form-item__content',
            );
          case '主机路径范围':
          case '主机端口范围':
          case '用户 UID 范围':
            this.waitElementPresent(
              this.getSectionByText(section).getElementByText(
                text,
                '.aui-form-item__content',
              ),
              '元素未出现',
            );
            return new AlaudaAuiTable(
              this.getSectionByText(section).getElementByText(
                text,
                '.aui-form-item__content',
              ),
              'th',
              'td',
              'tr',
            );
        }
        break;
    }
  }
  clickOperation(operation: string, child_operation = null) {
    return new AlaudaDropdown(
      this.root.$('.aui-card__header button'),
      $$('aui-menu-item'),
    ).select(operation, child_operation);
  }
  editPSP(data, flag = '确定') {
    return this.clickLeftNavByText('Pod 安全策略').then(() => {
      this.isNotHavePSP.then(ishave => {
        if (ishave) {
          this.clickOperation('更新');
        } else {
          return this.getButtonByText('立即配置').click();
        }
        this.editPage.fillForm(data);
        if (flag === '确定') {
          this.editPage.getButtonByText('确定').click();
        } else if (flag === '取消') {
          this.editPage.getButtonByText('取消').click();
        }
      });
    });
  }
  deletePSP(flag = '确定') {
    return this.clickLeftNavByText('Pod 安全策略').then(() => {
      this.clickOperation('删除');
      if (flag === '确定') {
        this.confirmDialog.clickConfirm();
      } else if (flag === '取消') {
        this.confirmDialog.clickCancel();
      }
    });
  }
  waitRegionSwitch(cluster = this.clusterName) {
    this.waitElementPresent(
      $(`div[title="当前集群"]+div span[title="集群:${cluster}"]`),
      '切换集群失败',
    ).then(ispresent => {
      if (!ispresent) {
        throw new Error('切换集群失败');
      }
    });
    return browser.sleep(3000);
  }
}
