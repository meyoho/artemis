import { AcpPageBase } from '../acp.page.base';
import { PSPDetailPage } from './psp.detail.page';
import { element, by, $ } from 'protractor';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class PSPEditPage extends AcpPageBase {
  get detailPage() {
    return new PSPDetailPage();
  }
  getAuiSwitchByText(text) {
    return new AuiSwitch(
      element(
        by.xpath(
          `//span[@class='switch_label' and normalize-space(text())="${text}"]/preceding-sibling::aui-switch`,
        ),
      ),
    );
  }
  getFormSectionByText(text) {
    return new AlaudaElement(
      'aui-form-item',
      '.aui-form-item__label',
      element(
        by.xpath(
          `//rc-psp-form/section/div/span[normalize-space(text())="${text}"]/../..`,
        ),
      ),
    );
  }
  enterValue(name: string, value: any) {
    switch (name) {
      case '基本策略':
        for (const v_name in value) {
          switch (v_name) {
            case '以特权模式运行（privileged）':
            case '主机 PID（hostPID）':
            case '主机 IPC（hostIPC）':
            case '主机网络（hostNetwork）':
            case '只读文件系统（readOnlyRootFilesystem）':
            case '提升特权（allowPrivilegeEscalation）':
              if (value[v_name]) {
                return this.getAuiSwitchByText(v_name).open();
              } else {
                return this.getAuiSwitchByText(v_name).close();
              }
            case '提升特权默认值':
              return new AlaudaRadioButton(
                element(
                  by.xpath(
                    `//span[@class='switch_label' and normalize-space(text())="提升特权（allowPrivilegeEscalation）"]/..//following-sibling::div/aui-radio-group`,
                  ),
                ),
                'aui-radio',
              ).clickByName(value[v_name]);
          }
        }
        break;
      case '主机路径策略（allowedHostPaths）':
      case '主机端口策略（hostPorts）':
      case '用户运行策略（runAsUser）':
        for (const v_name in value) {
          switch (v_name) {
            case '范围':
              return new AlaudaRadioButton(
                this.getFormSectionByText(name).getElementByText(
                  v_name,
                  'aui-radio-group',
                ),
                'aui-radio-button',
              ).clickByName(value[v_name]);
            case '主机路径范围':
            case '主机端口范围':
            case '用户 UID 范围':
              return new ArrayFormTable(
                this.getFormSectionByText(name).getElementByText(
                  v_name,
                  'rc-array-form-table',
                ),
                '.rc-array-form-table__bottom-control-buttons>button',
              ).fill(value[v_name]);
          }
        }
        break;
    }
  }
  create(data: Record<string, any>, flag = '确定') {
    return this.detailPage.isNotHavePSP.then(isNotHavePSP => {
      if (isNotHavePSP) {
        new AlaudaButton(
          element(
            by.xpath('//button/span[normalize-space(text())="立即配置"]'),
          ),
        )
          .click()
          .then(() => {
            this.fillForm(data);
            if (flag === '确定') {
              this.clickConfirm();
              this.waitElementNotPresent(
                $('.resource-form__footer'),
                '等待页面跳转',
              );
            } else if (flag === '取消') {
              this.clickCancel();
              this.waitElementNotPresent(
                $('.resource-form__footer'),
                '等待页面跳转',
              );
            }
          });
      }
    });
  }
}
