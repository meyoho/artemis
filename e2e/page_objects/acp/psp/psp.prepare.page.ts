import { AcpPageBase } from '../acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';

export class PSPPreparePage extends AcpPageBase {
  createPSP(cluster = this.clusterName) {
    CommonKubectl.execKubectlCommand(
      `kubectl label psp 20-user-restricted psp.${ServerConf.LABELBASEDOMAIN}/deleted- --overwrite=true`,
      cluster,
    );
  }
  deletePSP(cluster = this.clusterName) {
    CommonKubectl.execKubectlCommand(
      `kubectl label psp 20-user-restricted psp.${ServerConf.LABELBASEDOMAIN}/deleted=true --overwrite=true`,
      cluster,
    );
  }
}
