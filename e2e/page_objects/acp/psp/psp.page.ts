import { AcpPageBase } from '../acp.page.base';
import { PSPEditPage } from './psp.edit.page';
import { PSPDetailPage } from './psp.detail.page';
import { PSPPreparePage } from './psp.prepare.page';
import { PSPDetailVerifyPage } from '@e2e/page_verify/acp/psp/psp.detail.verify.page';

export class PSPPage extends AcpPageBase {
  get editPage() {
    return new PSPEditPage();
  }
  get detailPage() {
    return new PSPDetailPage();
  }
  get preparePage() {
    return new PSPPreparePage();
  }
  get detailVerifyPage() {
    return new PSPDetailVerifyPage();
  }
}
