import { AcpPageBase } from '../acp.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $, $$, ElementFinder, browser } from 'protractor';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { WorkLoadCreatePage } from './workload.page';
import { ServiceCreatePage } from './service.page';
import { ConfigCreatePage } from './config.page';
import { SecretCreatePage } from './secret.page';
import { OverrideCreatePage } from './override.page';
import { isArray } from 'util';

export class CreatePage extends AcpPageBase {
  addDeployButton() {
    return new AlaudaButton(
      $('rc-fa-resource-list .aui-card__header button:nth-child(1)'),
    );
  }
  addOtherDropDown() {
    return new AlaudaDropdown(
      $('rc-fa-resource-list .aui-card__header aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip .aui-menu-item'),
    );
  }
  addOverrideButton() {
    return new AlaudaButton($('rc-fa-override-list button'));
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'form .aui-form-item',
      'form .aui-form-item__label',
    );
  }
  get workloadPage(): WorkLoadCreatePage {
    return new WorkLoadCreatePage();
  }
  get servicePage(): ServiceCreatePage {
    return new ServiceCreatePage();
  }
  get configPage(): ConfigCreatePage {
    return new ConfigCreatePage();
  }
  get secretPage(): SecretCreatePage {
    return new SecretCreatePage();
  }
  get overridePage(): OverrideCreatePage {
    return new OverrideCreatePage();
  }
  getRow(keys, root): ElementFinder {
    if (!isArray(keys)) {
      throw new Error(`参数 keys(${keys}) 不是数组类型的`);
    }
    this.waitElementPresent($(root), '', 10000);
    const rowlist = $(root).$$('table tr');
    // 根据关键字找到唯一行
    const row = rowlist
      .filter(elem => {
        return elem.getText().then(text => {
          return keys.every((key: string) => text.includes(key));
        });
      })
      .first();
    return row;
  }
  get_button(name: string, root: ElementFinder): AlaudaButton {
    browser.executeScript('arguments[0].scrollIntoView();', root);
    switch (name) {
      case '编辑':
        return new AlaudaButton(
          root.$('button aui-icon[icon="basic:pencil_edit"]'),
        );
      case '删除':
        return new AlaudaButton(
          root.$('button aui-icon[icon="basic:minus_circle"]'),
        );
    }
  }
  click_button(keys, root, name) {
    this.get_button(name, this.getRow(keys, root));
  }
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    return this.alaudaElement.getElementByText(text, tagname);
  }
  enterValue(name, value) {
    switch (name) {
      case '名称':
      case '显示名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '部署':
        value.forEach(data => {
          this.addDeployButton().click();
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.workloadPage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '有状态副本集':
        value.forEach(data => {
          this.addOtherDropDown().select(name);
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.workloadPage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '内部路由':
        value.forEach(data => {
          this.addOtherDropDown().select(name);
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.servicePage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '配置字典':
        value.forEach(data => {
          this.addOtherDropDown().select(name);
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.configPage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '保密字典':
        value.forEach(data => {
          this.addOtherDropDown().select(name);
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.secretPage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '差异化':
        value.forEach(data => {
          this.addOverrideButton().click();
          this.waitElementPresent($('div aui-dialog'), '等待对话框出现', 10000);
          this.overridePage.fillForm(data);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '删除资源':
        value.forEach(data => {
          this.get_button(
            '删除',
            this.getRow(
              [data['资源名称']],
              'rc-fa-resource-list  .aui-card__content',
            ),
          ).click();
        });
        break;
      case '删除差异化':
        value.forEach(data => {
          this.get_button(
            '删除',
            this.getRow(
              [data['资源名称']],
              'rc-fa-override-list  .aui-card__content',
            ),
          ).click();
        });
        break;
      case '编辑资源':
        value.forEach(data => {
          this.get_button(
            '编辑',
            this.getRow(
              [data['资源名称']],
              'rc-fa-resource-list  .aui-card__content',
            ),
          ).click();
          switch (data['资源类型']) {
            case '部署':
            case '有状态副本集':
              this.workloadPage.fillForm(data['数据']);
              break;
            case '内部路由':
              this.servicePage.fillForm(data['数据']);
              break;
            case '配置字典':
              this.configPage.fillForm(data['数据']);
              break;
            case '保密字典':
              this.secretPage.fillForm(data['数据']);
              break;
          }
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
      case '编辑差异化':
        value.forEach(data => {
          this.get_button(
            '编辑',
            this.getRow(
              [data['资源类型'], data['资源名称']],
              'rc-fa-override-list  .aui-card__content',
            ),
          ).click();
          this.overridePage.fillForm(data['数据']);
          this.auiDialog.clickConfirm();
          this.waitElementNotPresent(
            $('div aui-dialog'),
            '等待对话框消失',
            5000,
          );
        });
        break;
    }
  }
}
