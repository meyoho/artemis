import { AcpPageBase } from '../acp.page.base';
import { ElementFinder, element, by } from 'protractor';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ItemFieldset } from '../container/element_objects/items_fieldset';

export class ConfigCreatePage extends AcpPageBase {
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-dialog-content//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}")]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      case '配置项':
        return this._getElementByText(text, 'rc-config-items-fieldset');
      case '描述':
        return this._getElementByText(text, 'textarea');
      default:
        return this._getElementByText(text, 'input');
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      case '配置项':
        new ItemFieldset(this.getElementByText(name)).add(value);
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
    }
  }
}
