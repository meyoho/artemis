import { AcpPageBase } from '../acp.page.base';
import { ElementFinder, element, by, browser, $$ } from 'protractor';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $ } from 'protractor';
import { ImageSelect } from '@e2e/element_objects/acp/workload/image_select';
import { AlaudaAuiNumberInput } from '@e2e/element_objects/alauda.aui_number_input';
import { FoldableBlock } from '@e2e/element_objects/alauda.foldable_block';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { DialogPage } from '../deployment/dialog.page';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
export class WorkLoadCreatePage extends AcpPageBase {
  get dialogPage(): DialogPage {
    return new DialogPage();
  }
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-dialog-content//aui-form-item//label[@class="aui-form-item__label" and text()="${left}"]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      case '实例数量':
        return this._getElementByText(text, 'aui-number-input');
      case '部署 - 高级':
        return $(
          'rc-workload-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '容器 - 高级':
        return $(
          'rc-container-form  rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '容器组 - 高级':
        return $(
          'rc-pod-template-form>form>rc-foldable-block>aui-form-item .aui-form-item__content  button',
        );
      case '更新策略':
        return this._getElementByText(
          text,
          'div[@class="aui-form-item__content"]',
        );
      case '镜像地址':
        return this._getElementByText(text, 'button');
      case '标签':
      case '启动命令':
      case '参数':
      case '日志文件':
      case '排除日志文件':
      case '环境变量':
      case '存储卷挂载':
      case '容器组标签':
      case '端口':
        return this._getElementByText(text, 'rc-array-form-table');
      case '环境变量添加引用':
        return this._getElementByText('环境变量', 'rc-array-form-table');
      case '配置引用':
      case '凭据':
      case '主机选择器':
        return this._getElementByText(text, 'aui-multi-select');
      case 'Host 模式':
        return this._getElementByText(text, 'aui-switch');
      case '亲和性':
        return this._getElementByText(
          text,
          'button[contains(@class,"aui-button--primary")]',
        );
      case '存储卷':
        return this._getElementByText(
          text,
          'div[@class="rc-array-form-table__bottom-control-buttons"]/button',
        );
      default:
        return this._getElementByText(text, 'input');
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      case '镜像':
        const image: ImageSelect = new ImageSelect($('div aui-dialog'));
        //{方式: '输入'， 镜像地址: '***'}
        if (value['方式'] === '输入') {
          image.enterImage(value['镜像地址']);
        }
        browser.sleep(500);
        break;
      case '名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '实例数量':
        new AlaudaAuiNumberInput(this.getElementByText(name)).input(value);
        break;
      case '更新策略':
        new FoldableBlock(this.getElementByText('部署 - 高级')).expand();
        this.getElementByText(name)
          .$$('input')
          .each((elem, index) => {
            new AlaudaInputbox(elem).input(String(value[index]));
          });
        new FoldableBlock(this.getElementByText('部署 - 高级')).fold();
        break;
      case '标签':
        new FoldableBlock(this.getElementByText('部署 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('部署 - 高级')).fold();
        break;
      case '资源限制':
        const resources = this._getElementByText(name, 'rc-resources-form');
        // {资源限制: [{CPU: 10, 单位: 'm' }, {内存: 10, 单位: 'Mi'}]
        resources.$$('div[class*="input-groups"]').each((elem, index) => {
          const inputBoxGroup: AlaudaInputGroup = new AlaudaInputGroup(
            elem,
            '.addon-label',
            'input[type="number"]',
          );
          inputBoxGroup.scrollToView(inputBoxGroup.inputbox);
          const item: { [key: string]: string } = value[index];
          for (const keyItem in item) {
            if (keyItem !== '单位') {
              inputBoxGroup.input(item[keyItem], item['单位']);
            }
          }
        });
        break;
      case '启动命令':
      case '参数':
      case '日志文件':
      case '排除日志文件':
      case '环境变量':
      case '存储卷挂载':
      case '端口':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '环境变量添加引用':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(2)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '配置引用':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v[0], v[1]);
        });
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '添加存活性健康检查':
      case '添加可用性健康检查':
        new FoldableBlock(this.getElementByText('容器 - 高级')).expand();
        this.getButtonByText(name).click();
        browser.sleep(1000);
        this.dialogPage.fillForm(value);
        $$('.aui-dialog__footer button[aui-button="primary"]')
          .last()
          .click();
        new FoldableBlock(this.getElementByText('容器 - 高级')).fold();
        break;
      case '容器组标签':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        ).fill(value);
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '凭据':
      case '主机选择器':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        value.forEach(v => {
          new AuiMultiSelect(
            this.getElementByText(name),
            $('.cdk-overlay-pane aui-tooltip'),
          ).select(v);
        });
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case '亲和性':
      case '存储卷':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        value.forEach(v => {
          new AlaudaButton(this.getElementByText(name)).click();
          this.dialogPage.fillForm(v);
          $$('.aui-dialog__footer button[aui-button="primary"]')
            .last()
            .click();
        });
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
      case 'Host 模式':
        new FoldableBlock(this.getElementByText('容器组 - 高级')).expand();
        if (value === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        }
        new FoldableBlock(this.getElementByText('容器组 - 高级')).fold();
        break;
    }
  }
}
