import { AcpPageBase } from '../acp.page.base';
import { ElementFinder, element, by } from 'protractor';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $ } from 'protractor';
export class ServiceCreatePage extends AcpPageBase {
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-dialog-content//aui-form-item//label[@class="aui-form-item__label" and contains(text(),"${left}")]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): ElementFinder {
    switch (text) {
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        return this._getElementByText(text, 'aui-switch');
      case '端口':
        return this._getElementByText(text, 'rc-array-form-table');
      case '工作负载':
        return this._getElementByText(text, 'aui-select');
      default:
        return this._getElementByText(text, 'input');
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      case '名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        if (String(value).toLowerCase() === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        } else {
          new AuiSwitch(this.getElementByText(name)).close();
        }
        break;
      case '端口':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
      case '工作负载':
        new AuiSelect(
          this.getElementByText(name),
          $('.cdk-overlay-pane aui-tooltip'),
        ).select(value['资源类型'], value['资源名称']);
        break;
    }
  }
}
