import { AcpPreparePage } from '../prepare.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';
import { CommonHttp } from '@e2e/utility/common.http';
const { prepareTestData } = require('../../../utility/prepare.testdata');

export class PreparePage extends AcpPreparePage {
  get kubefedIsEnabled() {
    return this.featureIsEnabled('kubefed');
  }

  fed_region_name = ServerConf.FEDERATIONREGIONNAME;

  fedRegionDetail(fed_region_name = this.fed_region_name) {
    const result = { Host: '', Member: [], All: [] };
    const clusters = JSON.parse(
      CommonKubectl.execKubectlCommand(
        `kubectl get clusterfed ${fed_region_name} -o json`,
      ),
    )['spec']['clusters'];
    clusters.forEach(cluster => {
      switch (cluster.type) {
        case 'Host':
          result.Host = cluster.name;
          break;
        case 'Member':
          result.Member.push(cluster.name);
          break;
      }
      result.All.push(cluster.name);
    });
    return result;
  }
  detail = this.fedRegionDetail();

  createFederatedPro(project_name, fed_region_name = this.fed_region_name) {
    prepareTestData.createProjectByTemplate(
      project_name,
      this.detail.All,
      'FedMember',
      fed_region_name,
    );
  }

  async createFederatedNs(
    namespace_name,
    project_name,
    fed_region_name = this.fed_region_name,
  ) {
    const data = {
      namespace_name: namespace_name,
      project_name: project_name,
      fed_region_name: fed_region_name,
      fed_host_name: this.detail.Host,
      clusters: this.detail.All,
    };
    const yaml = CommonMethod.readTemplateFile(
      'alauda.federatednamespace.yaml',
      data,
    );
    await CommonHttp.get(
      `kubernetes/${data.fed_host_name}/apis/types.kubefed.io/v1beta1/namespaces/${namespace_name}/federatednamespaces/${namespace_name}`,
    ).then(response => {
      switch (response.status) {
        case 404:
          CommonHttp.post(
            `/acp/v1/kubernetes/${data.fed_host_name}/federatednamespaces?project_name=${project_name}`,
            CommonMethod.parseYaml(yaml),
          ).then(response => {
            switch (response.status) {
              case 200:
                console.log(
                  `创建联邦命名空间成功${data.namespace_name} ${response.statusText}`,
                );
                break;
              default:
                console.log(
                  `创建联邦命名空间失败${data.namespace_name} ${response.statusText}`,
                );
                break;
            }
          });
          break;
        default:
          console.log(
            `获取到联邦命名空间详情，不需要创建${data.namespace_name} ${response.statusText}`,
          );
          break;
      }
    });
  }
  async deleteFederatedNs(namespace_name) {
    await CommonHttp.delete(
      `kubernetes/${this.detail.Host}/apis/types.kubefed.io/v1beta1/namespaces/${namespace_name}/federatednamespaces/${namespace_name}`,
    ).then(response => {
      switch (response.status) {
        case 200:
          console.log(
            `删除联邦命名空间成功${namespace_name} ${response.statusText}`,
          );
          break;
        default:
          console.log(
            `删除联邦命名空间失败${namespace_name} ${response.statusText}`,
          );
          break;
      }
    });
  }
  deleteFederatedResource(kind, namespace_name, app_name) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete ${kind} -n ${namespace_name} ${app_name} --timeout=10s`,
      this.detail.Host,
    );
  }

  waitResourceExist(
    resource_kind,
    resource_name,
    resource_namespace = '',
    flag = true,
  ) {
    const cmd =
      resource_namespace === ''
        ? `kubectl get  ${resource_kind} ${resource_name}`
        : `kubectl get  ${resource_kind} ${resource_name} -n ${resource_namespace}`;
    const fed_region_name = this.detail.Host;
    let timeout = 0;
    while (
      flag
        ? prepareTestData.isNotExist(fed_region_name, cmd)
        : !prepareTestData.isNotExist(fed_region_name, cmd)
    ) {
      prepareTestData.sleep(5000);
      if (timeout++ > 10) {
        // console.warn(`创建资源${resource_kind} ${resource_name} 超时了`);
        return false;
      }
    }
    return true;
  }
}
