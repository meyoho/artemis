import { AcpPageBase } from '../acp.page.base';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$, By, element, browser } from 'protractor';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { PreparePage } from './prepare';
import { ListPage } from './list.page';

export class DetailPage extends AcpPageBase {
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header  aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  get name(): AlaudaLabel {
    return new AlaudaLabel(
      $('acl-page-state>aui-card  .aui-card__header>div:nth-child(1)'),
    );
  }

  get noResource(): AlaudaLabel {
    return new AlaudaLabel($('rc-fa-resource-list .no-data-placement'));
  }
  get noOverride(): AlaudaLabel {
    return new AlaudaLabel($('rc-fa-override-list .no-data-placement'));
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  get appStatus(): AlaudaAuiTable {
    return new AlaudaAuiTable($('.aui-section table'), 'thead', 'td', 'tr');
  }
  enterAppDetail(region) {
    browser.refresh();
    this.appStatus
      .getRow([region])
      .$('button')
      .click();
  }
  get workloadTable(): AlaudaAuiTable {
    return new AlaudaAuiTable(
      element(
        By.xpath(
          '//rc-fa-resource-list//div[text()="工作负载"]/following-sibling::table',
        ),
      ),
      'thead',
      'td',
      'tr',
    );
  }
  get configTable(): AlaudaAuiTable {
    return new AlaudaAuiTable(
      element(
        By.xpath(
          '//rc-fa-resource-list//div[text()="配置"]/following-sibling::table',
        ),
      ),
      'thead',
      'td',
      'tr',
    );
  }
  get serviceTable(): AlaudaAuiTable {
    return new AlaudaAuiTable(
      element(
        By.xpath(
          '//rc-fa-resource-list//div[text()="内部路由"]/following-sibling::table',
        ),
      ),
      'thead',
      'td',
      'tr',
    );
  }
  get overrideTable(): AlaudaAuiTable {
    return new AlaudaAuiTable(
      $('rc-fa-override-list table'),
      'thead',
      'td',
      'tr',
    );
  }
  get listPage(): ListPage {
    return new ListPage();
  }
  delete(name, flag = 'delete') {
    this.listPage.enterDetail(name);
    this.action.select('删除');
    if (flag === 'cancel') {
      this.confirmDialog.clickCancel();
    } else {
      this.confirmDialog.clickConfirm();
    }
  }
  update(name, testData) {
    this.listPage.enterDetail(name);
    this.action.select('更新');
    this.listPage.updatePage.fillForm(testData);
    this.clickConfirm();
  }
}
