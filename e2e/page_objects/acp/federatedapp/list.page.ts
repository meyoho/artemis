import { AcpPageBase } from '../acp.page.base';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $ } from 'protractor';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { CreatePage } from './create.page';
import { UpdatePage } from './update.page';
import { PreparePage } from './prepare';

export class ListPage extends AcpPageBase {
  get FedAppTable() {
    return new AuiTableComponent(
      $('.layout-page-content aui-card'),
      'aui-table',
      '.aui-card__header',
    );
  }
  get noData(): AlaudaLabel {
    return new AlaudaLabel($('.empty-placeholder'));
  }
  search(name, row_count = 1) {
    this.clickLeftNavByText('联邦应用');
    this.FedAppTable.searchByResourceName(name, row_count);
  }
  enterDetail(name) {
    this.search(name);
    this.FedAppTable.clickResourceNameByRow([name], '名称');
    const resourceElem = $('acl-page-state rc-fa-resource-list');
    this.waitElementPresent(resourceElem, '详情页没有打开');
  }
  delete(name, flag = 'delete') {
    this.search(name);
    this.FedAppTable.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button__content aui-icon',
    );
    if (flag === 'cancel') {
      this.confirmDialog.clickCancel();
    } else {
      this.confirmDialog.clickConfirm();
    }
  }
  get createPage(): CreatePage {
    return new CreatePage();
  }
  get updatePage(): UpdatePage {
    return new UpdatePage();
  }
  update(name, testData) {
    this.search(name);
    this.FedAppTable.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button__content aui-icon',
    );
    this.updatePage.fillForm(testData);
    this.clickConfirm();
  }
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  create(testData) {
    this.clickLeftNavByText('联邦应用');
    this.getButtonByText('创建联邦应用').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('确定').click();
    this.waitElementPresent(
      $('acl-page-state>aui-card  .aui-card__header>div:nth-child(1)'),
      'the fed app detail not present',
      60000,
    );
  }
}
