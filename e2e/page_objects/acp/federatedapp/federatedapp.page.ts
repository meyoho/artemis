import { AcpPageBase } from '../acp.page.base';
import { PreparePage } from './prepare';
import { ListPage } from './list.page';
import { CreatePage } from './create.page';
import { UpdatePage } from './update.page';
import { DetailVerify } from '@e2e/page_verify/acp/federatedapp/detail.page';
import { ListVerify } from '@e2e/page_verify/acp/federatedapp/list.page';
import { DetailPage } from './detail.page';

export class FederatedAppPage extends AcpPageBase {
  get preparePage(): PreparePage {
    return new PreparePage();
  }
  get listPage(): ListPage {
    return new ListPage();
  }
  get detailPage(): DetailPage {
    return new DetailPage();
  }
  get createPage(): CreatePage {
    return new CreatePage();
  }
  get updatePage(): UpdatePage {
    return new UpdatePage();
  }
  get detailVerify(): DetailVerify {
    return new DetailVerify();
  }
  get listVerify(): ListVerify {
    return new ListVerify();
  }
}
