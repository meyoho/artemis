import { AcpPageBase } from '../acp.page.base';
import { PreparePage } from './app_store.prepare.page';
import { ChartListPage } from './chart.list.page';
import { ChartDetailPage } from './chart.detail.page';
import { AppDetailPage } from './app.detail.page';
import { AppListPage } from './app.list.page';
import { AppCreatePage } from './app.create.page';
import { TplAppDetailVerify } from '@e2e/page_verify/acp/app_store/tpl_app.detail.page';
import { TplAppListVerify } from '@e2e/page_verify/acp/app_store/tpl_app.list.page';
import { ChartDetailVerify } from '@e2e/page_verify/acp/app_store/chartdetail.page';
import { ChartListVerify } from '@e2e/page_verify/acp/app_store/chartlist.page';

export class AppStorePage extends AcpPageBase {
  get preparePage() {
    return new PreparePage();
  }
  get chartListPage() {
    return new ChartListPage();
  }
  get chartDetailPage() {
    return new ChartDetailPage();
  }
  get appDetailPage() {
    return new AppDetailPage();
  }
  get appListPage() {
    return new AppListPage();
  }
  get appCreatePage() {
    return new AppCreatePage();
  }
  get appDetailVerifyPage() {
    return new TplAppDetailVerify();
  }
  get appListVerifyPage() {
    return new TplAppListVerify();
  }
  get chartListVerify() {
    return new ChartListVerify();
  }
  get chartDetailVerify() {
    return new ChartDetailVerify();
  }
}
