import { AcpPageBase } from '../acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { alauda_type_helmrequests } from '@e2e/utility/resource.type.k8s';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';

export class PreparePage extends AcpPageBase {
  public delete_helm_request(name: string, ns = ServerConf.GLOBAL_NAMESPCE) {
    CommonKubectl.deleteResource(
      alauda_type_helmrequests,
      name,
      this.clusterName,
      ns,
    );
  }
  create_helm_request(yamlFile: string, data) {
    return CommonKubectl.createResourceByTemplate(
      yamlFile,
      data,
      'helmreqest_data_tmp',
      this.clusterName,
    );
  }
  get appmarketIsEnabled() {
    return this.featureIsEnabled('app-market');
  }
  getChartVersion() {
    const token = CommonMethod.loginGetToken();
    const cmd = `curl ${
      ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
    } -H 'authorization:Bearer ${token}' -k ${
      ServerConf.BASE_URL
    }/apis/app.alauda.io/v1alpha1/namespaces/${
      ServerConf.GLOBAL_NAMESPCE
    }/charts/helloworld.stable`;
    console.log(cmd);
    const rsp: string = CommonMethod.execCommand(cmd);
    console.log(rsp);
    return JSON.parse(rsp)['spec']['versions'][0]['version'];
  }
}
