import { AcpPageBase } from '../acp.page.base';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $, promise, $$ } from 'protractor';
import { TableInput } from '@e2e/element_objects/alauda.table_input';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AppDetailPage } from './app.detail.page';

export class AppCreatePage extends AcpPageBase {
  get appDetailPage() {
    return new AppDetailPage();
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item',
      'label',
      $('rc-helm-request-form'),
    );
  }
  getElementByText(text: string) {
    const ele = this.alaudaElement.getElementByText(
      text,
      '.aui-form-item__container',
    );
    switch (text) {
      case '模板版本':
        return new AuiSelect(ele.$('aui-select'), $('aui-tooltip>div'));
      default:
        return ele
          .$('rc-key-value-form-table')
          .isPresent()
          .then(ispresent => {
            if (ispresent) {
              return new TableInput(ele.$('rc-key-value-form-table table'));
            } else {
              return new AlaudaInputbox(ele.$('input'));
            }
          });
    }
  }
  enterValue(name: string, value: any) {
    switch (name) {
      case '模板版本':
        (this.getElementByText(name) as AuiSelect).select(value);
        break;
      case 'global.registry':
      case 'global.images.helloworld':
      case 'service':
      case 'resources.requests':
      case 'resources.limits':
      case 'nodeSelector':
      case 'affinity':
        (this.getElementByText(name) as promise.Promise<TableInput>).then(
          tb_input => {
            tb_input.inputTableValues(value);
          },
        );
        break;
      default:
        (this.getElementByText(name) as promise.Promise<AlaudaInputbox>).then(
          input => {
            input.input(value);
          },
        );
        break;
    }
  }
  click(button: string) {
    return $$('form+div button')
      .filter(ele => {
        return ele.getText().then(text => {
          return text.trim() === button;
        });
      })
      .first()
      .click();
  }
}
