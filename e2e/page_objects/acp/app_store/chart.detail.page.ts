import { AcpPageBase } from '../acp.page.base';
import { $, promise } from 'protractor';
import { AppCreatePage } from './app.create.page';
import { ChartListPage } from './chart.list.page';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';

export class ChartDetailPage extends AcpPageBase {
  get chart_list_page() {
    return new ChartListPage();
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('rc-field-set-item', '.field-set-item__label');
  }
  get tpl_app_create_page() {
    return new AppCreatePage();
  }
  get name(): promise.Promise<string> {
    return this.waitElementPresent(
      $('aui-card:nth-child(1) .aui-card__header'),
      'chart详情页名称元素没有出现',
    ).then(ispresent => {
      if (ispresent) {
        return $('aui-card:nth-child(1) .aui-card__header').getText();
      } else {
        return null;
      }
    });
  }
  create_tpl_app(
    chart_repo: string,
    chart_name: string,
    data: Record<string, any>,
  ) {
    this.chart_list_page.switch_chart_repo(chart_repo);
    this.chart_list_page.get_chart_card_by_name(chart_name).then(chart_card => {
      chart_card.helm_chart_card.click();
      this.getButtonByText('创建应用').click();
      this.tpl_app_create_page.fillForm(data);
      this.tpl_app_create_page.click('创建');
      this.tpl_app_create_page.waitElementPresent(
        $('button+aui-menu+button'),
        '等待页面跳转到模板应用详情',
      );
    });
  }
  create_tpl_app_cancel(
    chart_repo: string,
    chart_name: string,
    data: Record<string, any>,
  ) {
    this.chart_list_page.switch_chart_repo(chart_repo);
    this.chart_list_page.get_chart_card_by_name(chart_name).then(chart_card => {
      chart_card.helm_chart_card.click();
      this.getButtonByText('创建应用').click();
      this.tpl_app_create_page.fillForm(data);
      this.tpl_app_create_page.click('取消');
    });
  }
}
