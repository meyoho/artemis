import { AcpPageBase } from '../acp.page.base';
import { $, $$, browser } from 'protractor';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AppCreatePage } from './app.create.page';
import { AppListPage } from './app.list.page';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';

export class AppDetailPage extends AcpPageBase {
  get tpl_app_create_page() {
    return new AppCreatePage();
  }
  get app_list() {
    return new AppListPage();
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('rc-field-set-item', '.field-set-item__label');
  }
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog'),
      '.aui-confirm-dialog__content',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
      '.aui-confirm-dialog__title',
    );
  }
  get name() {
    this.waitElementPresent(
      $(
        'acl-page-state .aui-tab-body__content>aui-card:nth-child(1) .aui-card__header',
      ),
      '详情页应用名称没有出现',
    ).then(ispresent => {
      if (!ispresent) {
        throw new Error('详情页应用名称没有出现');
      }
    });
    return $(
      'acl-page-state .aui-tab-body__content>aui-card:nth-child(1) .aui-card__header',
    ).getText();
  }
  get rs_list_name() {
    this.waitElementPresent(
      $(
        'acl-page-state .aui-tab-body__content>aui-card:nth-child(2) .aui-card__header',
      ),
      '详情页资源列表标题没有出现',
    ).then(ispresent => {
      if (!ispresent) {
        throw new Error('详情页资源列表标题没有出现');
      }
    });
    return $(
      'acl-page-state .aui-tab-body__content>aui-card:nth-child(2) .aui-card__header',
    ).getText();
  }
  get rs_list_table() {
    return new AlaudaAuiTable(
      $('.aui-card__content aui-table'),
      'aui-table-header-cell',
      'aui-table-cell',
      'aui-table-row',
    );
  }
  get refresh_button() {
    this.waitElementPresent($('button+aui-menu+button'), '等待刷新按钮出现');
    return $('button+aui-menu+button');
  }
  get operation_button() {
    return $('.aui-card__header>div+button');
  }
  click_operation(operation: string) {
    this.operation_button.click();
    const ops = $$('aui-tooltip aui-menu-item>button').filter(ele => {
      return ele.getText().then(text => {
        return text.trim() === operation;
      });
    });
    ops.count().then(ct => {
      if (ct > 0) {
        ops.first().click();
      } else {
        throw new Error(`没有找到按钮[${operation}]`);
      }
    });
  }
  waitDeploySuccess(timeout = 10000) {
    browser.refresh();
    browser.driver.wait(() => {
      // return this.refresh_button.click().then(() => {
      return this.getElementByText('状态', '.field-set-item__value')
        .getText()
        .then(text => {
          console.log(text);
          if (text.includes('部署成功')) {
            return true;
          } else {
            return false;
          }
        });
      // });
    }, timeout);
  }
  update_app(name: string, data: Record<string, any>) {
    this.app_list.tpl_app_table.searchByResourceName(name, 1);
    this.app_list.tpl_app_table.clickResourceNameByRow([name]);
    this.click_operation('更新');
    this.tpl_app_create_page.fillForm(data);
    return this.tpl_app_create_page.click('保存');
  }
  update_app_cancel(name: string, data: Record<string, any>) {
    this.app_list.tpl_app_table.searchByResourceName(name, 1);
    this.app_list.tpl_app_table.clickResourceNameByRow([name]);
    this.click_operation('更新');
    this.tpl_app_create_page.fillForm(data);
    return this.tpl_app_create_page.click('取消');
  }
  delete_app(name: string) {
    this.app_list.tpl_app_table.clickResourceNameByRow([name]);
    this.app_list.tpl_app_table.searchByResourceName(name, 1);
    this.click_operation('删除');
    return this.auiDialog.buttonConfirm.click();
  }
  delete_app_cancel(name: string) {
    this.app_list.tpl_app_table.clickResourceNameByRow([name]);
    this.app_list.tpl_app_table.searchByResourceName(name, 1);
    this.click_operation('删除');
    return this.auiDialog.buttonCancel.click();
  }
}
