import { AcpPageBase } from '../acp.page.base';
import { promise, $ } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AppCreatePage } from './app.create.page';

export class AppListPage extends AcpPageBase {
  get tpl_app_create_page() {
    return new AppCreatePage();
  }
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog'),
      '.aui-confirm-dialog__content',
      '.aui-confirm-dialog__confirm-button',
      '.aui-confirm-dialog__cancel-button',
      '.aui-confirm-dialog__title',
    );
  }
  get alert_content_text(): promise.Promise<string> {
    this.waitElementPresent(
      $('.aui-inline-alert__content'),
      '模板应用列表页提示信息',
      5000,
    );
    return $('.aui-inline-alert__content').getText();
  }
  get tpl_app_table() {
    return new AuiTableComponent(
      $('aui-inline-alert+aui-card'),
      'aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  search_app_by_name(name: string, row_count) {
    this.tpl_app_table.searchByResourceName(name, row_count);
  }
  click_app(name: string) {
    this.tpl_app_table.clickResourceNameByRow([name], '名称');
  }
  delete_app(name: string) {
    this.search_app_by_name(name, 1);
    this.tpl_app_table.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button',
      'aui-tooltip aui-menu-item>button',
    );
    this.auiDialog.buttonConfirm.click();
  }
  delete_app_cancel(name: string) {
    this.search_app_by_name(name, 1);
    this.tpl_app_table.clickOperationButtonByRow(
      [name],
      '删除',
      '.aui-button',
      'aui-tooltip aui-menu-item>button',
    );
    this.auiDialog.buttonCancel.click();
  }
  update_app(name: string, data: Record<string, any>) {
    this.search_app_by_name(name, 1);
    this.tpl_app_table.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-tooltip aui-menu-item>button',
    );
    this.tpl_app_create_page.fillForm(data);
    this.tpl_app_create_page.click('保存');
  }
  update_app_cancel(name: string, data: Record<string, any>) {
    this.search_app_by_name(name, 1);
    this.tpl_app_table.clickOperationButtonByRow(
      [name],
      '更新',
      '.aui-button',
      'aui-tooltip aui-menu-item>button',
    );
    this.tpl_app_create_page.fillForm(data);
    this.tpl_app_create_page.click('取消');
  }
}
