import { AcpPageBase } from '../acp.page.base';
import { $, promise, $$, browser } from 'protractor';
import { HelmChartCard } from '@e2e/element_objects/alauda.rc_helm_chart_card';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AppCreatePage } from './app.create.page';

export class ChartListPage extends AcpPageBase {
  get tpl_app_crate_page() {
    return new AppCreatePage();
  }
  get alert_content_text(): promise.Promise<string> {
    this.waitElementPresent(
      $('.aui-inline-alert__content'),
      '应用目录列表页提示信息',
      5000,
    );
    return $('.aui-inline-alert__content').getText();
  }
  get chart_repos_text(): promise.Promise<string> {
    return $$('.rc-repo__content>button>span').getText();
  }
  wait_loding(timeout = 20000) {
    this.waitElementPresent(
      $('rc-zero-state+.rc-charts__content'),
      `${timeout} 没有加载完成`,
      timeout,
    ).then(isloding => {
      if (!isloding) {
        throw new Error('应用模板加载失败');
      }
    });
  }
  get_chart_card_by_name(chart_name: string): promise.Promise<HelmChartCard> {
    const chart_cards = $$(
      '.rc-charts__content>rc-helm-chart-card[class="ng-star-inserted"]',
    ).filter(ele => {
      const chart_card = new HelmChartCard(ele);
      return chart_card.name.then(text => {
        return text === chart_name;
      });
    });
    return chart_cards.count().then(ct => {
      if (ct > 0) {
        return new HelmChartCard(chart_cards.first());
      } else {
        return null;
      }
    });
  }
  click_chart(chart_name: string) {
    this.get_chart_card_by_name(chart_name).then(card => {
      if (card === null) {
        throw new Error(`chart列表中没有找到[${chart_name}]`);
      } else {
        card.helm_chart_card.click();
      }
    });
  }
  switch_chart_repo(repo_name: string) {
    this.wait_loding();
    this.waitElementPresent(
      $('.rc-repo__content>button'),
      '等待页面元素加载完成',
    );
    const chart_repos = $$('.rc-repo__content>button').filter(ele => {
      return ele.getText().then(text => {
        return text === repo_name;
      });
    });
    return chart_repos.count().then(ct => {
      if (ct > 0) {
        const chart_repo = chart_repos.first();
        chart_repo.getAttribute('class').then(attr => {
          if (!attr.includes('aui-button--primary')) {
            chart_repo.click();
            this.wait_loding();
          }
        });
      } else {
        throw new Error(`chart repo 列表中没有找到[${repo_name}]`);
      }
    });
  }
  search(text: string) {
    const input_box = new AlaudaInputbox($('aui-search input'));
    input_box.input(text).then(() => {
      this.wait_loding();
    });
  }
  get_chart_cards_count(): promise.Promise<number> {
    return $$(
      '.rc-charts__content>rc-helm-chart-card[class="ng-star-inserted"]',
    ).count();
  }
  create_tpl_app(
    chart_repo: string,
    chart_name: string,
    data: Record<string, any>,
  ) {
    this.switch_chart_repo(chart_repo);
    this.get_chart_card_by_name(chart_name).then(chart_card => {
      chart_card.clickAddApp();
      this.tpl_app_crate_page.fillForm(data);
      this.tpl_app_crate_page.click('部署');
      browser.sleep(3000);
      this.tpl_app_crate_page.appDetailPage.waitDeploySuccess(60000);
    });
  }
  create_tpl_app_cancel(
    chart_repo: string,
    chart_name: string,
    data: Record<string, any>,
  ) {
    this.switch_chart_repo(chart_repo);
    this.get_chart_card_by_name(chart_name).then(chart_card => {
      chart_card.clickAddApp();
      this.tpl_app_crate_page.fillForm(data);
      this.tpl_app_crate_page.click('取消');
    });
  }
}
