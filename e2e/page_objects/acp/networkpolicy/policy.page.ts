import { AcpPageBase } from '../acp.page.base';
import { CreatePolicyPage } from './create.page';
import { PolicyListPage } from './list.page';
import { PolicyListPageVerify } from '@e2e/page_verify/acp/networkpolicy/list.page';

export class PolicyPage extends AcpPageBase {
  get createPage(): CreatePolicyPage {
    return new CreatePolicyPage();
  }
  get listPage(): PolicyListPage {
    return new PolicyListPage();
  }
  get listPageVerify(): PolicyListPageVerify {
    return new PolicyListPageVerify();
  }
  networkPolicyIsEnabled(region_name) {
    return this.featureIsEnabled('network-policy', region_name);
  }
}
