import { AcpPageBase } from '../acp.page.base';
import { $, $$, browser } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';

export class CreatePolicyPage extends AcpPageBase {
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '模板':
        new AlaudaRadioButton(
          $('.aui-dialog__content aui-radio-group'),
          '.aui-radio-button__content span',
        ).clickByName(value);
        break;
    }
    browser.sleep(100);
  }

  /**
   * 添加网络策略
   * @param testData 创建数据
   */
  addPolicy(testData, flag = 'add') {
    this.clickLeftNavByText('网络策略');
    this.getButtonByText('创建网络策略').click();
    this.fillForm(testData);
    this.auiDialog.clickConfirm();
    switch (flag) {
      case 'cancel':
        this.clickCancel();
        break;
      default:
        this.clickConfirm();
        this.toast.getMessage(5000).then(message => {
          console.log(message);
        });
        break;
    }
  }
  get yamlCreateDropDown() {
    return new AlaudaDropdown(
      $('.aui-card__header aui-dropdown-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip .aui-menu-item'),
    );
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
  }
  /**
   * 添加网络策略
   * @param testData 创建数据
   */
  addByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('网络策略');
    this.yamlCreateDropDown.select('YAML 创建网络策略');
    this.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        new AlaudaButton(
          $('.resource-form__footer button[aui-button="default"]'),
        ).click();
        break;
      default:
        this.clickConfirm();
        this.toast.getMessage(5000).then(message => {
          console.log(message);
        });
        break;
    }
  }
}
