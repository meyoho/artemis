import { $, $$ } from 'protractor';

import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AcpPageBase } from '../acp.page.base';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';

export class PolicyListPage extends AcpPageBase {
  /**
   * 网络策略管理列表
   */
  get policyNameTable() {
    return new AuiTableComponent(
      $('aui-card .aui-card'),
      '.aui-card__content aui-table',
      'aui-search .aui-search',
      'aui-table-cell',
      'aui-table-row',
    );
  }
  /**
   * 检索框
   */
  get search() {
    return new AuiSearch($('.aui-card__header aui-search'));
  }
  get noData() {
    const empty = $('.empty-placeholder');
    this.waitElementPresent(empty, '【 无网络策略】控件没出现');
    return empty;
  }

  /**
   * 点击名称链接
   * @param name 名称
   */
  clickview(name: string) {
    this.searchPolicy(name);
    this.policyNameTable.clickResourceNameByRow([name], '名称');
  }
  /**
   * 点击操作栏的删除按钮
   * @param name 名称
   */
  clickdelete(name: string) {
    this.searchPolicy(name);
    this.policyNameTable.clickOperationButtonByRow(
      [name],
      '删除',
      'aui-table-row aui-icon',
    );
  }
  /**
   * 点击操作栏的更新按钮
   * @param name 名称
   */
  clickupdate(name: string) {
    this.searchPolicy(name);
    this.policyNameTable.clickOperationButtonByRow(
      [name],
      '更新',
      'aui-table-row aui-icon',
    );
  }

  /**
   * 查看网络策略
   * @param name 名称
   */
  viewPolicy(name) {
    this.clickview(name);
  }
  /**
   * 搜索网络策略
   * @param name 名称
   * @param count 预期结果行数
   */
  searchPolicy(name, count = 1) {
    this.clickLeftNavByText('网络策略');
    this.search.search(name);
    this.policyNameTable.waitRowCountChangeto(count);
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
  }
  /**
   * 列表页更新网络策略
   * @param name 名称
   * @param testData 更新数据
   */
  updatePolicy(name, flag = 'update') {
    this.searchPolicy(name);
    this.clickupdate(name);
    // this.fillForm(testData);
    this.alaudaCodeEdit.getYamlValue().then(yaml => {
      const updateyaml = yaml.replace(
        'metadata:\n',
        'metadata:\n  labels:\n    update: update\n',
      );
      this.alaudaCodeEdit.setYamlValue(updateyaml);
    });
    switch (flag) {
      case 'cancel':
        this.clickCancel();
        break;
      default:
        this.clickConfirm();
        this.toast.getMessage(5000).then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 列表页删除网络策略
   * @param name 名称
   */
  deletePolicy(name, flag = 'delete') {
    this.searchPolicy(name);
    this.clickdelete(name);
    switch (flag) {
      case 'cancel':
        this.confirmDialog.clickCancel();
        break;
      default:
        this.confirmDialog.clickConfirm();
        this.toast.getMessage(5000).then(message => {
          console.log(message);
        });
        break;
    }
  }
  batchDelete() {
    this.clickLeftNavByText('网络策略');
    const rowlist = this.policyNameTable.auiTable.$$('aui-table-row');
    rowlist.each(row => {
      const option = new AlaudaDropdown(
        row.$('aui-table-row aui-icon[icon="basic:ellipsis_v_s"]'),
        $$('aui-menu-item button'),
      );
      option.select('删除');
      this.confirmDialog.clickConfirm();
    });
  }
}
