import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
// import { CommonMethod } from '@e2e/utility/common.method';

export class PreparePage extends AcpPageBase {
    deleteService(name: string, ns_name: string = this.namespace1Name) {
        CommonKubectl.deleteResource(
            'service',
            name,
            this.clusterName,
            ns_name
        );
    }
}
