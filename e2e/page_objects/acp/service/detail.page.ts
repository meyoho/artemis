import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$, browser, by, element } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { ArrayFormTable } from '../../../element_objects/alauda.arrayFormTable';
import { AlaudaAuiCodeEditor } from '../../../element_objects/alauda.aui_code_editor';
import { AuiIcon } from '../../../element_objects/alauda.aui_icon';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../acp.page.base';

import { DeleteServicePage } from './delete.page';
import { ServiceListPage } from './list.page';
import { UpdateServicePage } from './update.page';

export class DetailServicePage extends AcpPageBase {
  get name() {
    this.waitElementPresent(
      $('aui-card:first-child .aui-card__header'),
      '等待进入内部路由详情页',
    );
    return $('aui-card:first-child .aui-card__header').getText();
  }
  /**
   * 点击切换tab页
   * @param name tab 名称如YAML
   */
  clickTab(name: string) {
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(text(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }
  /**
   * 获取yaml页
   */
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-dialog__content aui-code-editor'));
  }
  /**
   * 获取容器组页
   */
  get podListTable() {
    return new AuiTableComponent(
      $('rc-pod-list .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  /**
   * 关闭对话框
   */
  closeDialog() {
    new AuiIcon($('.aui-dialog__header-close aui-icon')).click();
  }
  get ports() {
    return new AuiTableComponent($('.aui-card__content rc-service-ports'));
  }
  get selectors() {
    return new AuiTableComponent($('.aui-card__content rc-key-value-display'));
  }
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-service-detail-info  rc-field-set-item',
      'rc-service-detail-info  rc-field-set-item label',
      $('rc-service-detail-info aui-card .hasDivider'),
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '标签':
      case '注解':
        return this.alaudaElement.getElementByText(text, 'rc-tags-label');
      default:
        return this.alaudaElement.getElementByText(
          text,
          '.field-set-item__value',
        );
    }
  }
  /**
   * 点击详情页的更新注解按钮
   */
  clickupdateannotation() {
    this.getElementByText('注解')
      .$('aui-icon[icon="basic:pencil_s"]')
      .click();
    browser.sleep(1000);
  }
  /**
   * 点击详情页的更新标签按钮
   */
  clickupdatelabel() {
    this.getElementByText('标签')
      .$('aui-icon[icon="basic:pencil_s"]')
      .click();
    browser.sleep(1000);
  }
  /**
   * 更新注解、标签的对话框
   */
  get updateTable() {
    return new ArrayFormTable(
      $('.aui-dialog__content rc-array-form-table'),
      '.rc-array-form-table__bottom-control-buttons button',
    );
  }
  /**
   * 详情页的操作按钮
   */
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header .aui-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  /**
   * 列表页
   */
  get listPage() {
    return new ServiceListPage();
  }
  /**
   * 更新注解
   * @param name 内部路由名称
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  updateAnnotation(name, tesData, flag = 'update') {
    this.listPage.viewService(name);
    this.clickupdateannotation();
    this.updateTable.fill(tesData);
    switch (flag) {
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
      case 'close':
        this.auiDialog.clickClose();
        break;
      default:
        this.auiDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 更新标签
   * @param name 内部路由名称
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  updateLabel(name, tesData, flag = 'update') {
    this.listPage.viewService(name);
    this.clickupdatelabel();
    this.updateTable.fill(tesData);
    switch (flag) {
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
      case 'close':
        this.auiDialog.clickClose();
        break;
      default:
        this.auiDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 更新页
   */
  get updatePage() {
    return new UpdateServicePage();
  }
  /**
   * 删除页
   */
  get deletePage() {
    return new DeleteServicePage();
  }
  /**
   * 详情页更新
   * @param name 内部路由名称
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  update(name, testData, flag = 'update') {
    this.listPage.viewService(name);
    this.action.select('更新');
    this.updatePage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.updatePage.clickCancel();
        break;
      default:
        this.updatePage.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }

  /**
   * 详情页删除
   * @param name 内部路由名称
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  delete(name, flag = 'delete') {
    this.listPage.viewService(name);
    this.action.select('删除');
    switch (flag) {
      case 'cancel':
        this.deletePage.deleteDialog.clickCancel();
        break;
      default:
        this.deletePage.deleteDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
}
