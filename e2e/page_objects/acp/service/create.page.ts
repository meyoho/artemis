import { AlaudaAuiCodeEditor } from '@e2e/element_objects/alauda.aui_code_editor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { $, $$, browser, ElementFinder } from 'protractor';

import { ArrayFormTable } from '../../../element_objects/alauda.arrayFormTable';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AcpPageBase } from '../acp.page.base';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';

export class CreateServicePage extends AcpPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-card .aui-form-item',
      '.aui-card  .aui-form-item__label',
    );
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '名称':
        return this.alaudaElement.getElementByText(text);
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        return this.alaudaElement.getElementByText(text, 'aui-switch');

      case '端口':
      case '选择器':
        return this.alaudaElement.getElementByText(text, 'rc-array-form-table');
      case '从计算资源导入':
        return this.alaudaElement.getElementByText(
          '选择器',
          'button[aui-button="primary"]',
        );
      case '资源类型':
      case '资源名称':
        return this.workloadElement.getElementByText(text, 'aui-select');
    }
    return this.alaudaElement.getElementByText(text, tagname);
  }
  get workloadElement(): AlaudaElement {
    return new AlaudaElement(
      'rc-import-workloads-form aui-form-item',
      '.aui-form-item .aui-form-item__label',
    );
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '名称':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case '虚拟 IP':
      case '外网访问':
      case '会话保持':
        if (String(value).toLowerCase() === 'true') {
          new AuiSwitch(this.getElementByText(name)).open();
        } else {
          new AuiSwitch(this.getElementByText(name)).close();
        }
        break;
      case '端口':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons button',
        ).fill(value);
        break;
      case '选择器':
        new ArrayFormTable(
          this.getElementByText(name),
          '.rc-array-form-table__bottom-control-buttons .aui-button--default',
        ).fill(value);
        break;
      case '从计算资源导入':
        new AlaudaButton(this.getElementByText(name)).click();
        new AlaudaDropdown(
          this.getElementByText('资源类型'),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        ).select(value['资源类型']);
        new AuiSelect(
          this.getElementByText('资源名称'),
          $('.cdk-overlay-pane aui-tooltip'),
        ).select(value['资源名称']);
        this.auiDialog.clickConfirm();
    }
    browser.sleep(100);
  }
  get createTypeRatio(): AlaudaRadioButton {
    return new AlaudaRadioButton($('.aui-card__header aui-radio-group'));
  }
  get alaudaCodeEdit() {
    return new AlaudaAuiCodeEditor($('.aui-dialog__content aui-code-editor'));
  }
}
