import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { $, $$, ElementFinder, browser } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AcpPageBase } from '../acp.page.base';

import { CreateServicePage } from './create.page';
import { DeleteServicePage } from './delete.page';
import { DetailServicePage } from './detail.page';
import { UpdateServicePage } from './update.page';

export class ServiceListPage extends AcpPageBase {
  /**
   * 内部路由管理列表
   */
  get serviceNameTable() {
    return new AuiTableComponent(
      $('aui-card .aui-card'),
      '.aui-card__content>aui-table',
      '.aui-card__header',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  get noResult() {
    const elem = $('.empty-placeholder');
    this.waitElementPresent(elem, '检索后，无结果控件没出现');
    return elem;
  }
  ports(element: ElementFinder) {
    return new AlaudaDropdown(
      element.$('.service-table-ports__more button span'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item'),
    );
  }
  /**
   * 创建内部路由按钮
   */
  clickadd() {
    const button = this.getButtonByText('创建内部路由');
    this.waitElementPresent(button.button, '创建内部路由按钮未出现', 1000);
    button.click();
  }

  /**
   * 点击名称进去详情页
   * @param name 名称
   */
  clickview(name: string) {
    this.searchService(name);
    this.serviceNameTable.clickResourceNameByRow([name]);
  }
  /**
   * 点击操作栏的删除按钮
   * @param name 名称
   */
  clickdelete(name: string) {
    this.serviceNameTable.clickOperationButtonByRow(
      [name],
      '删除',
      'aui-table-row aui-icon',
    );
  }
  /**
   * 点击操作栏的更新按钮
   * @param name 名称
   */
  clickupdate(name: string) {
    this.serviceNameTable.clickOperationButtonByRow(
      [name],
      '更新',
      'aui-table-row aui-icon',
    );
  }
  /**
   * 检索框
   */
  get search() {
    return new AuiSearch($('.aui-card__header aui-search'));
  }
  /**
   * 创建页
   */
  get createPage() {
    return new CreateServicePage();
  }
  /**
   * 查看页
   */
  get detailPage() {
    return new DetailServicePage();
  }
  /**
   * 更新页
   */
  get updatePage() {
    return new UpdateServicePage();
  }
  /**
   * 删除页
   */
  get deletePage() {
    return new DeleteServicePage();
  }

  /**
   * 添加内部路由
   * @param testData 创建数据
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  addService(testData, flag = 'add') {
    this.clickLeftNavByText('内部路由');
    this.clickadd();
    this.createPage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.createPage.clickCancel();
        break;
      default:
        this.createPage.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 添加内部路由
   * @param testData 创建数据
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  addServiceByYaml(testData, flag = 'add') {
    this.clickLeftNavByText('内部路由');
    this.clickadd();
    this.createPage.createTypeRatio.clickByName('YAML');
    this.createPage.alaudaCodeEdit.setYamlValue(testData);
    switch (flag) {
      case 'cancel':
        this.createPage.clickCancel();
        break;
      default:
        this.createPage.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 更新内部路由
   * @param name 名称
   * @param testData 更新数据
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  updateService(name, testData, flag = 'update') {
    this.searchService(name);
    this.clickupdate(name);
    this.updatePage.fillForm(testData);
    switch (flag) {
      case 'cancel':
        this.updatePage.clickCancel();
        break;
      default:
        this.updatePage.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 删除内部路由
   * @param name 名称
   * @param flag cancel 点击取消按钮 close 点击关闭按钮 其他 点击确认按钮
   */
  deleteService(name, flag = 'delete') {
    this.searchService(name);
    this.clickdelete(name);
    switch (flag) {
      case 'cancel':
        this.deletePage.deleteDialog.clickCancel();
        break;
      default:
        this.deletePage.deleteDialog.clickConfirm();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
    }
  }
  /**
   * 查看内部路由
   * @param name 名称
   */
  viewService(name) {
    this.searchService(name);
    this.clickview(name);
    this.waitElementPresent(
      this.detailPage.getElementByText('虚拟 IP'),
      '详情页没打开',
    );
  }
  /**
   * 搜索内部路由
   * @param name 名称
   * @param count 预期结果行数
   */
  searchService(name, count = 1) {
    this.clickLeftNavByText('内部路由');
    this.search.search(name);
    //表格的端口单元格是 rc-service-ports aui-table 结构，里面有一行，所以查询出来的行数是期望值的2倍

    if (count === 0) {
      this.noResult.isPresent().then(isPresent => {
        if (!isPresent) {
          throw new Error('检索为空的控件没出现');
        }
      });
    } else {
      const table = new AlaudaAuiTable(
        this.serviceNameTable.auiTable.$('aui-table'),
      );
      table.getRowCount().then(rowCount => {
        this.serviceNameTable.waitRowCountChangeto(count + rowCount);
      });
    }

    browser.sleep(1);
  }
}
