import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { DetailServicePageVerify } from '@e2e/page_verify/acp/service/detail.page';
import { ServiceListPageVerify } from '@e2e/page_verify/acp/service/list.page';

import { CreateServicePage } from './create.page';
import { DeleteServicePage } from './delete.page';
import { DetailServicePage } from './detail.page';
import { ServiceListPage } from './list.page';
import { PreparePage } from './prepare.page';
import { UpdateServicePage } from './update.page';

export class ServicePage extends AcpPageBase {
    /**
     * 准备测试数据
     */
    get preparePage() {
        return new PreparePage();
    }
    /**
     * 创建页
     */
    get createPage() {
        return new CreateServicePage();
    }

    /**
     * 列表页
     */
    get listPage() {
        return new ServiceListPage();
    }

    get listPageVerify() {
        return new ServiceListPageVerify();
    }

    /**
     * 删除页
     */
    get deletePage() {
        return new DeleteServicePage();
    }

    /**
     * 详情页
     */
    get detailPage() {
        return new DetailServicePage();
    }

    get detailPageVerify() {
        return new DetailServicePageVerify();
    }

    /**
     * 更新页
     */
    get updatePage() {
        return new UpdateServicePage();
    }
}
