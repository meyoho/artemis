import { AuditListPageVerify } from '@e2e/page_verify/ait/audit/audit.list.page';

import { AitPageBase } from '../ait.page.base';

import { AuditListPage } from './auditlist.page';
import { AuditPreparePage } from './auditprepare.page';

export class AuditPage extends AitPageBase {
  get listPage(): AuditListPage {
    return new AuditListPage();
  }

  get listPageVerify(): AuditListPageVerify {
    return new AuditListPageVerify();
  }

  get preparePage(): AuditPreparePage {
    return new AuditPreparePage();
  }
}
