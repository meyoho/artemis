// import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $, browser } from 'protractor';

import { AitPageBase } from '../ait.page.base';

export class AuditListPage extends AitPageBase {
  get alaudaElement() {
    return new AlaudaElement('aui-form-item .aui-form-item', 'label');
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '时间范围':
      case '操作人':
      case '操作类型':
      case '对象类型':
        return new AuiSelect(
          this.alaudaElement.getElementByText(text, 'aui-select'),
        );
      case '操作对象':
        return new AuiSearch(
          this.alaudaElement.getElementByText(text, 'aui-search'),
        );
    }
  }

  get auditTable() {
    return new AlaudaAuiTable(
      $('.aui-card__content table'),
      'thead td',
      'tbody td',
      'tbody tr',
    );
  }

  get noData(): AlaudaLabel {
    return new AlaudaLabel($('td[class="row-empty"]'));
  }

  search(data) {
    this.clickLeftNavByText('审计');
    for (const key in data) {
      if (data.hasOwnProperty(key)) {
        switch (key) {
          case '时间范围':
          case '操作人':
          case '操作类型':
          case '对象类型':
            console.log(`${key}: ${data[key]}`);
            browser.sleep(100);
            this.getElementByText(key).select(data[key]);

            break;
          case '操作对象':
            console.log(data[key]);
            this.getElementByText(key).search(data[key]);
            break;
        }
      }
    }
    // this.auditTable.waitRowCountChangeto(rowCount);
    // 等待页面发生变化
    return browser.sleep(2000);
  }

  expandByIndex(index) {
    this.auditTable.getRowByIndex(index);
    const tdElem = this.auditTable
      .getRowByIndex(0)
      .$('td[class="col-expand-icon"]');
    tdElem
      .$('aui-icon[icon*="down"]')
      .getAttribute('hidden')
      .then(hidden => {
        if (hidden) {
          tdElem.click();
        }
      });
  }

  foldByIndex(index) {
    this.auditTable.getRowByIndex(index);
    const tdElem = this.auditTable
      .getRowByIndex(0)
      .$('td[class="col-expand-icon"]');
    tdElem
      .$('aui-icon[icon*="down"]')
      .getAttribute('hidden')
      .then(hidden => {
        if (!hidden) {
          tdElem.click();
        }
      });
  }
}
