/**
 * Created by liuwei on 2019/6/13.
 */
import { AlaudaTabItem } from '@ele/alauda.tabitem';
import { $, ElementFinder, browser, by, promise, $$ } from 'protractor';

import { ServerConf } from '../../config/serverConf';
import { AuiConfirmDialog } from '../../element_objects/alauda.aui_confirm_dialog';
import { AuiDialog } from '../../element_objects/alauda.aui_dialog';
import { AlaudaAuiTable } from '../../element_objects/alauda.aui_table';
import { AlaudaAuiBreadCrumb } from '../../element_objects/alauda.auiBreadcrumb';
import { AlaudaButton } from '../../element_objects/alauda.button';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { PageBase } from '../page.base';
import { LoginPage } from '../login.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class AitPageBase extends PageBase {
  constructor() {
    super();
    browser.baseUrl = `${ServerConf.BASE_URL}/console-acp/`;
  }
  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    const branchName = ServerConf.BRANCH_NAME.substring(0, 10)
      .replace('_', '-')
      .replace('/', '')
      .toLowerCase();
    return `acp-${branchName}-${prefix}`;
  }

  get clusterName() {
    return ServerConf.REGIONNAME;
  }

  get projectName(): string {
    return 'uiauto-acp';
  }

  get namespace1Name(): string {
    return 'uiauto-acp-ns1';
  }
  get namespace2Name(): string {
    return 'uiauto-acp-ns2';
  }

  get project2Name(): string {
    return 'uiauto-acp2';
  }

  get project2ns1(): string {
    return 'uiauto-acp2-ns1';
  }
  get project2ns2(): string {
    return 'uiauto-acp2-ns2';
  }

  /**
   * 从console-acp进入项目管理页面
   */
  enterManageProjectFromAcp() {
    this.switchSetting('项目管理');
  }
  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }
  /**
   * progressbar 出现后，等待消失
   * @example waitProgressBarNotPresent()
   */
  waitProgressBarNotPresent(timeout = 20000): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(
          this.progressbar,
          'progressbar 没消失',
          timeout,
        );
      }
    });
  }
  /**
   * 获取面包屑
   * @param timeout
   */
  get breadcrumb(): AlaudaAuiBreadCrumb {
    return new AlaudaAuiBreadCrumb(
      $('aui-breadcrumb'),
      'aui-breadcrumb-item span',
    );
  }
  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  /**
   * 进入管理视图页面
   */
  enterOperationView() {
    // browser.sleep(1000);
    // 管理视图页面，切换视图的按钮

    this.switchAdminView();
  }

  /**
   * 进入平台中心页面
   */
  enterPlatformView(name) {
    const platformname = new AlaudaDropdown(
      $('acl-platform-center-nav .menu'),
      $$('aui-menu-item button span'),
    );
    platformname.select(name);
  }

  /**
   * 页面集群导航条
   * @param region_name 集群名称
   */
  regionMenu(region_name): AlaudaTabItem {
    return new AlaudaTabItem(
      by.xpath('//aui-tag'),
      by.xpath("//aui-tag/div[contains(@class,'aui-tag--primary')]"),
      by.xpath('//aui-tag/div/span[contains(text(),"' + region_name + '")]'),
    );
  }

  /**
   * 页面命名空间导航条
   */
  namespaceMenu(): AlaudaAuiTable {
    return new AlaudaAuiTable($('aui-card aui-table'));
  }
  /**
   * 进入用户视图页面
   * @param namespace_name 命名空间
   */
  enterUserView(
    namespace_name = null,
    projectName = this.projectName,
    region_name = this.clusterName,
  ) {
    if (namespace_name) {
      const url = `${ServerConf.BASE_URL}/console-acp/workspace;project=${projectName};cluster=${region_name};namespace=${namespace_name}/app/list`;
      const loginPage = new LoginPage();
      loginPage.waitEnterUserView(url, '/app/list');

      this.waitElementPresent(
        $(`acl-namespace-select a[title~=${namespace_name}]`),
        '切换用户视图失败',
        30000,
      ).then(isPresent => {
        if (!isPresent) {
          throw new Error('切换用户视图失败, 测试无法继续');
        }
      });
    } else {
      // 切换到用户视图
      this.switchUserView();
      // 选择一个项目
      this.switchProject(projectName);
      // 单击一个集群名称
      if (this.clusterDisplayName(region_name) === undefined) {
        this.regionMenu(region_name).click();
      } else {
        this.regionMenu(this.clusterDisplayName(region_name)).click();
      }
    }
    browser.sleep(100);
  }
  /**
   * 用户视图返回项目列表
   */
  backtoproject() {
    $('.aui-page__header .project-icon').click();
  }
  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(
      $('.resource-form__footer button[aui-button="primary"]'),
    );
  }
  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton($('.resource-form__footer button[aui-button=""]'));
  }

  /**
   * 单击确定按钮
   */
  clickConfirm(): promise.Promise<void> {
    this.buttonConfirm.click();
    // this.waitProgressBarNotPresent();
    return browser.sleep(100);
  }

  /**
   * 单击取消按钮
   */
  clickCancel(): promise.Promise<void> {
    this.buttonCancel.click();
    return browser.sleep(100);
  }
  /**
   * 创建service
   * @param serviceName
   * @param namespaceName
   */
  createService(serviceName: string, namespaceName: string): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.service.yaml',
      {
        service_name: serviceName,
        ns_name: namespaceName,
      },
      'qa-service' + String(new Date().getMilliseconds()),
      ServerConf.REGIONNAME,
    );
  }
  /**
   * 创建secret
   * @param secretName
   * @param namespaceName
   */
  createSecret(secretName: string, namespaceName: string): void {
    CommonKubectl.createResource(
      'alauda.secret-https.yaml',
      {
        '${SECRET_NAME}': secretName,
        '${NAMESPACE_NAME}': namespaceName,
      },
      'qa-secret' + String(new Date().getMilliseconds()),
      ServerConf.REGIONNAME,
    );
  }
  deleteSecret(name: string, ns_name: string = this.namespace1Name) {
    CommonKubectl.deleteResource('secret', name, this.clusterName, ns_name);
  }
  /**
   * 创建domain
   * @param domainName 域名名称
   * @param kind 域名类型 full extensive
   * @param clusterName 分配集群
   * @param projectName 分配项目
   */
  createDomain(
    domainName: string,
    kind = 'extensive',
    clusterName: string,
    projectName: string,
  ): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.domain.template.yaml',
      {
        domain_name: domainName,
        cluster_name: clusterName,
        project_name: projectName,
        kind: kind,
      },
      'qa-domain' + String(new Date().getMilliseconds()),
    );
  }

  get confirmDialog_name() {
    // return new AuiConfirmDialog($('aui-dialog .aui-dialog ng-component'));
    return new AuiConfirmDialog($('alu-confirm-delete'));
  }
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
  }

  /**
   * 删除应用
   * @param appName 应用名称
   * @param namespace 命名空间
   * @param tempFile 临时文件
   */
  deleteApp(appName: string, namespace: string, tempFile: string) {
    const command = `kubectl get pod ${appName} -n ${namespace}`;
    let isDeleted = CommonKubectl.execKubectlCommand(
      command,
      ServerConf.REGIONNAME,
    ).includes('NotFound');
    let timeout = 0;
    while (!isDeleted) {
      console.log(
        CommonKubectl.deleteResourceByYmal(tempFile, ServerConf.REGIONNAME),
      );
      CommonMethod.sleep(1000);
      isDeleted = CommonKubectl.execKubectlCommand(
        command,
        ServerConf.REGIONNAME,
      ).includes('NotFound');

      if (timeout++ > 120) {
        throw new Error(`删除应用${appName}超时`);
      }
    }
  }
}
