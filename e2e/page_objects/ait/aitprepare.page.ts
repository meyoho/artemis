import { CommonKubectl } from '../../utility/common.kubectl';

import { AitPageBase } from './ait.page.base';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';

export class AitPreparePage extends AitPageBase {
  /**
   * 删保密字典
   * @param secretName 保密字典名称
   * @param namespace 命名空间名称
   * @param clusterName 集群名称
   */
  deleteSecret(
    secretName: string,
    namespace: string,
    clusterName: string = this.clusterName,
  ) {
    try {
      CommonKubectl.execKubectlCommand(
        `kubectl delete secret ${secretName} -n ${namespace}`,
        clusterName,
      );
    } catch (e) {
      console.log(e.message);
    }
  }

  /**
   * 创建应用
   * @param appName 应用名称
   * @param namespace 命名空间
   * @param tempFile 临时文件
   */
  createApp(
    appName: string,
    namespace: string,
    clusterName: string = this.clusterName,
  ) {
    const yamlfile = CommonKubectl.createResourceByTemplate(
      'alauda.application.tpl.yaml',
      {
        app_name: appName,
        ns_name: namespace,
        pro_name: this.projectName,
        image: ServerConf.TESTIMAGE,
      },
      'qa-application' + String(new Date().getMilliseconds()),
      this.clusterName,
    );
    let spec = this._getAppSpec(appName, namespace);

    let timeout = 0;
    let isOk = false;
    while (spec === undefined || !isOk) {
      spec.forEach(value => {
        const command = `kubectl get pods -n ${namespace} -l app.${ServerConf.LABELBASEDOMAIN}/name=${value} -o yaml`;
        const items = CommonMethod.parseYaml(
          CommonKubectl.execKubectlCommand(command, clusterName),
        ).items;

        let isRunning = true;

        items.forEach(item => {
          isRunning =
            String(item.status.phase).includes('Running') && isRunning;
        });

        if (isRunning) {
          console.log('\n应用创建成功');
          console.log(
            CommonKubectl.execKubectlCommand(
              `kubectl get pods -n ${namespace} -l app.${ServerConf.LABELBASEDOMAIN}/name=${value}`,
              clusterName,
            ),
          );
        }

        isOk = isOk || isRunning;
      });

      CommonMethod.sleep(1000);
      spec = this._getAppSpec(appName, namespace);

      if (timeout++ > 120) {
        throw new Error(`创建应用${appName}超时`);
      }
    }

    return yamlfile;
  }

  private _getAppSpec(
    appName: string,
    namespace: string,
    clusterName = this.clusterName,
  ): Map<string, string> {
    const command = `kubectl get application -n ${namespace} ${appName} -o yaml`;
    const compenet = new Map<string, string>();
    try {
      const spec = CommonMethod.parseYaml(
        CommonKubectl.execKubectlCommand(command, clusterName),
      ).spec;
      spec.componentKinds.forEach(component => {
        compenet.set(
          component.kind,
          spec.selector.matchLabels[`app.${ServerConf.LABELBASEDOMAIN}/name`],
        );
      });
      return compenet;
    } catch (ex) {
      console.log(CommonKubectl.execKubectlCommand(command, clusterName));
    }
  }

  private _deleteComponent(command, clusterName) {
    let isDeleted = CommonKubectl.execKubectlCommand(
      command,
      clusterName,
    ).includes('NotFound');
    let timeout = 0;
    while (!isDeleted) {
      console.log(CommonKubectl.execKubectlCommand(command, clusterName));
      CommonMethod.sleep(1000);
      isDeleted = CommonKubectl.execKubectlCommand(
        command,
        clusterName,
      ).includes('NotFound');
      if (timeout++ > 120) {
        throw new Error(`删除超时`);
      }
    }
  }
  /**
   * 删除应用
   * @param appName 应用名称
   * @param namespace 命名空间
   * @param tempFile 临时文件
   */
  deleteApp(
    appName: string,
    namespace: string,
    clusterName = this.clusterName,
  ) {
    const compenets: Map<string, string> = this._getAppSpec(
      appName,
      namespace,
      clusterName,
    );

    if (compenets !== undefined) {
      compenets.forEach((value, key) => {
        const command = `kubectl get ${key} -n ${namespace} -l app.${ServerConf.LABELBASEDOMAIN}/name=${value} -o yaml`;
        const items = CommonMethod.parseYaml(
          CommonKubectl.execKubectlCommand(command, clusterName),
        ).items;

        items.forEach(item => {
          console.log(`\n删除${item.kind} : ${item.metadata.name}`);
          const deleteCmd = `kubectl delete ${item.kind} -n ${item.metadata.namespace} ${item.metadata.name}`;
          this._deleteComponent(deleteCmd, clusterName);
        });
      });
    }

    console.log(`\n删除Application: ${appName}`);
    const commandDelApp = `kubectl delete application -n ${namespace} ${appName}`;
    this._deleteComponent(commandDelApp, clusterName);
  }
}
