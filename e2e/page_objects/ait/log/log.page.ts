import { LogListPageVerify } from '@e2e/page_verify/ait/log/log.list.page';

import { AitPageBase } from '../ait.page.base';

import { LogListPage } from './loglist.page';
import { PreparePage } from './logprepare.page';

export class LogPage extends AitPageBase {
  get listPage(): LogListPage {
    return new LogListPage();
  }

  get listPageVerify(): LogListPageVerify {
    return new LogListPageVerify();
  }

  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
