import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaLogEvents } from '@e2e/element_objects/alauda.log_events';
import { AlaudaLogResult } from '@e2e/element_objects/alauda.log_result';
import { $, ElementFinder, browser } from 'protractor';

import { AitPageBase } from '../ait.page.base';

export class LogListPage extends AitPageBase {
  get timeRange() {
    return new AuiSelect(
      $('.field-container aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }
  get queryCondition() {
    return new AuiMultiSelect(
      $('aui-form-item aui-multi-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }
  get logEvent(): AlaudaLogEvents {
    return new AlaudaLogEvents($('.log-detail .alu-log-dashboard-events'));
  }
  get logResult(): AlaudaLogResult {
    return new AlaudaLogResult($('.log-detail .alu-log-result'));
  }

  /**
   * 查询日志
   * @param condition 查询条件
   */
  search(condition: string = 'cluster: high') {
    this.clickLeftNavByText('日志');
    browser.sleep(1000);
    this.waitLoading();
    this.queryCondition.select(condition);
    this.waitLoading();
    this.waitElementPresent(
      $('.alu-log-result .alu-log-item'),
      '没有查询出任何日志',
    );
    // 等待5检索结果发生变化
    browser.sleep(5000);
  }

  waitLoading() {
    const loading: ElementFinder = $(
      '.log-detail .alu-loading-mask-placeholder:not([hidden])',
    );

    this.waitElementNotPresent(loading, 'Loading 图标没有消失');
    this.waitElementNotPresent(
      $('.log-detail .alu-loading-mask-placeholder'),
      '正在加载数据提示没有消失',
    );
  }
}
