/**
 * Created by zhangjiao on 2019/6/13.
 */
import { NotificationDetailPageVerify } from '@e2e/page_verify/ait/notification/notification.detail.page';

import { AitPageBase } from '../ait.page.base';

import { NotificationCreatePage } from './create.page';
import { NotificationListPage } from './notificationlist.page';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { $ } from 'protractor';
import { PreparePage } from './notificationprepare.page';

export class NotificationPage extends AitPageBase {
  get preparePage(): PreparePage {
    return new PreparePage();
  }

  get listPage(): NotificationListPage {
    return new NotificationListPage();
  }
  get createPage() {
    return new NotificationCreatePage();
  }
  createNotificationtmp(testData) {
    this.getButtonByText('创建通知模板').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  createNotificationReceiver(testData) {
    this.getButtonByText('添加通知对象').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('添加').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  createNotificationServer(testData) {
    this.getButtonByText('创建通知服务器').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
  }
  createNotificationSender(testData) {
    this.getButtonByText('添加发送人').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('添加').click();
    this.ConfirmDialog_add.WaitDialogNotPresent();
  }
  createNotification(testData) {
    this.getButtonByText('创建通知').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
  }
  createNotification_add(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  createPageMessage() {
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  get detailPageVerify(): NotificationDetailPageVerify {
    return new NotificationDetailPageVerify();
  }

  updateNotificationtmp(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  updateNotificationServer(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  updateNotificationReceiver(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  updateNotification(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  detailPage_operate(operateName) {
    this.getButtonByText('操作').click();
    this.getButtonByText(operateName).click();
  }

  get ConfirmDialog_add() {
    return new AuiConfirmDialog($('aui-dialog ng-component'));
  }
}
