import { CommonKubectl } from '@e2e/utility/common.kubectl';

import { AitPreparePage } from '../aitprepare.page';
import { ServerConf } from '@e2e/config/serverConf';

export class PreparePage extends AitPreparePage {
  /**
   * 删除通知
   * @param name 通知名称
   */
  delete(name: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete Notification -n ${ServerConf.GLOBAL_NAMESPCE} ${name}`,
    );
  }

  // 下面是用到的
  deleteNotifacation(name: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete Notification -n ${ServerConf.GLOBAL_NAMESPCE} ${name}`,
    );
  }
  deleteNotifacationTmp(name: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete NotificationTemplate ${name}`,
    );
  }
  deleteNotifacationReceiver(name: string) {
    const common =
      `kubectl delete NotificationReceiver -n ${ServerConf.GLOBAL_NAMESPCE} ` +
      `\`kubectl get NotificationReceiver -n ${ServerConf.GLOBAL_NAMESPCE} |grep ${name}|awk '{print $1}'\``;
    //console.log(common);
    CommonKubectl.execKubectlCommand(common);
  }

  deleteNotifacationSender(name: string) {
    name = name.replace('@', '-');
    name = name.replace('.', '-');
    if (
      CommonKubectl.execKubectlCommand(
        `kubectl get secret -n ${ServerConf.GLOBAL_NAMESPCE} |grep ${name}|awk '{print $1}'`,
      ) !== ''
    ) {
      const common =
        `kubectl delete secret -n ${ServerConf.GLOBAL_NAMESPCE} ` +
        `\`kubectl get secret -n ${ServerConf.GLOBAL_NAMESPCE} |grep ${name}|awk '{print $1}'\``;

      CommonKubectl.execKubectlCommand(common);
    }
  }

  deleteNotifacationServer(name: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete NotificationServer ${name}`,
    );
  }
}
