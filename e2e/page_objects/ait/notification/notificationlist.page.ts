import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $ } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AitPageBase } from '../ait.page.base';

export class NotificationListPage extends AitPageBase {
  /**
   * 资源管理列表
   */
  get notificationTable() {
    return new AuiTableComponent(
      $('.layout-page-content aui-card'),
      'aui-table',
      '.aui-card__header',
    );
  }

  get noData(): AlaudaLabel {
    return new AlaudaLabel($('rc-resource-list-footer .empty-placeholder'));
  }

  /**
   * 检索通知
   * @param notificationName 通知名称
   * @param row_count 期望检索到的行数，检索后会等待表格中行数据变为row_count,直到等待超时
   * @example
   * this.search('example')
   */
  search(notificationName) {
    this.notificationTable.searchResource(notificationName);
  }
  searchCount(notificationName, row_count = 1) {
    this.notificationTable.searchByResourceName(notificationName, row_count);
  }

  /**
   * 进入通知的详情页
   * @param notificationName 通知的名称
   * @example
   * this.enterDetail('example')
   */
  enterDetail(notificationName) {
    // this.search(notificationName);
    this.notificationTable.clickResourceNameByRow([notificationName], '名称');

    // 详情页，左上角通知名称控件
    const nameElem = $('.aui-card__header h3');
    this.waitElementNotPresent(nameElem, '详情页没有打开');
  }

  /**
   * 表格中操作列选择删除操作，删除通知
   * @param notificationName 通知名称
   * @example
   * this.delete('example')
   */
  delete(notificationName) {
    // this.search(notificationName);
    this.notificationTable.clickOperationButtonByRow(
      [notificationName],
      '删除',
      '.aui-button__content aui-icon',
    );
    this.confirmDialog.clickConfirm();
  }

  /**
   * 表格中操作列选择更新操作，更新通知模板
   * @param notificationName 通知名称
   * @example
   * this.delete('example')
   */
  update(notificationName) {
    this.search(notificationName);
    this.notificationTable.clickOperationButtonByRow(
      [notificationName],
      '更新',
      '.aui-button__content aui-icon',
    );
  }
  updateTmp(notificationTmpName) {
    this.notificationTable.clickOperationButtonByRow(
      [notificationTmpName],
      '更新',
      '.aui-button__content aui-icon',
    );
  }
  /**
   * 列表页点击删除
   * @param notificationName
   */
  delete_operate(notificationName) {
    // this.search(notificationName);
    this.notificationTable.clickOperationButtonByRow(
      [notificationName],
      '删除',
      '.aui-button__content aui-icon',
    );
  }
}
