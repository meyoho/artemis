import { AuiTableComponent } from '../../../component/aui_table.component';
// import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaLabel } from '../../../element_objects/alauda.label';
import { $, ElementFinder, element, by } from 'protractor';

import { AitPageBase } from '../ait.page.base';
export class NotificationDetailPage extends AitPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  // get alaudaElement(): AlaudaElement {
  //   return new AlaudaElement(
  //     'rc-field-set-item',
  //     '.field-set-item__label',
  //     $('rc-field-set-group'),
  //   );
  // }
  _getElementByText(left: string): ElementFinder {
    const xpath = `//div[@class='aui-card__content']//label[text()="${left}" ]/ancestor::alu-field-set-item//div[@class="field-set-item__value field-set-item__value__overflow"]`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    return new AlaudaLabel(this._getElementByText(text));
  }

  get notificationTable(): AuiTableComponent {
    return new AuiTableComponent($('acl-page-state aui-card:nth-child(2)'));
  }

  getNotificationText(index, key): any {
    return new AlaudaLabel(
      element(
        by.xpath(
          `//div[@class="aui-card__content"]//div[@class="ng-star-inserted"][${index}]//div[contains(text(), "${key}")]`,
        ),
      ),
    );
  }
  getNotificationReceiver(index, key): any {
    return new AlaudaLabel(
      element(
        by.xpath(
          `//div[@class="aui-card__content"]//div[@class="ng-star-inserted"][${index}]//div[contains(text(), "${key}")]//span`,
        ),
      ),
    );
  }
  getNotificationTitle(): any {
    return new AlaudaLabel(
      element(by.xpath(`//div[@class="aui-card__header"]/span`)),
    );
  }
}
