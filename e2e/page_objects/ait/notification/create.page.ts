/**
 * Created by zhangjiao on 2019/10/31.
 */
import { $, $$, browser, by, element, ElementFinder } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AitPageBase } from '../ait.page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaText } from '@e2e/element_objects/alauda.text';

export class NotificationCreatePage extends AitPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form aui-form-item .aui-form-item',
      'aui-form-item label[class*=aui-form-item__label]',
    );
  }
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//aui-form-item//label[@class="aui-form-item__label" and text()="${left}" ]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '模板类型':
        return new AlaudaRadioButton(
          this._getElementByText(text, 'aui-radio-group'),
          '.aui-radio__content',
        );
      case '名称':
      case '显示名称':
      case '服务器地址':
      case '端口':
      case '邮箱':
      case '密码':
        return new AlaudaInputbox(this._getElementByText(text, tagname));
      case '描述':
        return new AlaudaInputbox(this._getElementByText(text, 'textarea'));
      case '模板描述':
        return new AlaudaInputbox(this._getElementByText('描述', tagname));
      case '添加通知对象':
        return new ArrayFormTable(
          $('.aui-dialog__content alu-array-form-table'),
          '.alu-array-form-table__bottom-control-buttons button:nth-child(1)',
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case '查看样例':
        const viewClick = new AlaudaButton(
          element(by.xpath('//div[contains(text(),"通知内容样例")]/a')),
        );
        viewClick.click();
        browser.sleep(1000);
        const close = new AlaudaButton($('.aui-dialog__header .aui-icon'));
        close.click();
        break;
      case '名称':
      case '显示名称':
      case '服务器地址':
      case '端口':
      case '描述':
      case '模板描述':
      case '邮箱':
      case '密码':
        this.getElementByText(name, tagname).input(value);
        break;
      case '模板类型':
        const tmpType: AlaudaRadioButton = this.getElementByText(name);
        tmpType.clickByName(value);
        break;
      case '添加通知对象':
        const arrayFormTable: ArrayFormTable = this.getElementByText(name);
        arrayFormTable.fill(value);
        break;
      case '使用SSL':
      case '跳过非安全验证':
        this.alaudaElement.scrollToBoWindowBottom();
        const checkBox = new AlaudaAuiCheckbox(
          element(by.xpath(`//span[contains(text(),'${name}')]/../../..`)),
        );
        if (value === 'check') {
          checkBox.check();
        } else {
          checkBox.uncheck();
        }
        break;
      case '邮件服务器':
        const emailserver = new AlaudaDropdown(
          element(
            by.xpath(`//label[contains(text(), '${name}')]/../..//input`),
          ),
          $$('.aui-option'),
        );
        emailserver.select(value);
        break;
      case '通知方式1':
      case '通知方式2':
      case '通知方式3':
        const index = String(name).replace('通知方式', '');
        this.alaudaElement.scrollToBoWindowBottom();
        const addButton = new AlaudaButton(
          $('.alu-array-form-table__bottom-control-buttons .aui-button'),
        );
        addButton.click();
        for (const key in value) {
          const data = value[key];
          switch (key) {
            case '类型':
            case '通知模板':
            case '发送人':
              const dropdownName = new AlaudaDropdown(
                element(
                  by.xpath(
                    `//alu-array-form-table//tr[${index}]//label[contains(text(), '${key}')]/../..//input`,
                  ),
                ),
                $$('.aui-option'),
              );
              dropdownName.select(data);
              break;
            case '通知对象':
              this.alaudaElement.scrollToBoWindowBottom();
              const dropvalue = new AlaudaDropdown(
                element(
                  by.xpath(
                    `//alu-array-form-table//tr[${index}]//label[contains(text(), '${key}')]/../..//aui-multi-select`,
                  ),
                ),
                $$('.aui-option'),
              );
              dropvalue.select(data);
              browser.sleep(100);
              break;
          }
        }
        break;
      case '删除通知方式1':
      case '删除通知方式2':
        this.alaudaElement.scrollToBoWindowBottom();
        const num = String(name).replace('删除通知方式', '');
        const deleteButton = new AlaudaButton(
          $(
            `.alu-array-form-table--row-group-start:nth-child(${num}) .alu-array-form-table__action-col aui-icon`,
          ),
        );
        deleteButton.click();
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  _getToolTip(): ElementFinder {
    return $('alu-error-mapper');
  }

  inputByText(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
  ) {
    if (value != '') {
      this.enterValue(text, value);
    }
    if (errortip != '') {
      browser.sleep(20);
      this._getToolTip()
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }

  errorAlert() {
    return new AlaudaText(by.xpath(`//div[text()='资源已存在']`));
  }
  get errorAlert_close() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
}
