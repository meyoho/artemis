import { EventListPageVerify } from '@e2e/page_verify/ait/event/event.list.page';

import { AitPageBase } from '../ait.page.base';

import { EventListPage } from './eventlist.page';
import { PreparePage } from './eventprepare.page';

export class EventPage extends AitPageBase {
  get listPage() {
    return new EventListPage();
  }

  get listPageVerify(): EventListPageVerify {
    return new EventListPageVerify();
  }

  get preparePage(): PreparePage {
    return new PreparePage();
  }
}
