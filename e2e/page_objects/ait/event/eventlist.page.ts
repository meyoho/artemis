import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AuiMultiSelect } from '@e2e/element_objects/alauda.auiMultiSelect';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $, $$, by, element } from 'protractor';

import { AitPageBase } from '../ait.page.base';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class EventListPage extends AitPageBase {
  get timeRange() {
    return new AuiSelect(
      $('.form-inline__control aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }
  get queryCondition() {
    return new AuiMultiSelect(
      $('alu-k8s-event-list aui-multi-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }

  get auiTable() {
    return new AlaudaAuiTable($('.event-list aui-table'));
  }

  search(queryCondition: string) {
    this.queryCondition.select(queryCondition);
  }
  clearSearch() {
    this.queryCondition.clearTags();
  }
  get listPage_getEventTotal() {
    return new AlaudaText(by.css('.aui-paginator__total'));
  }
  get listPage_nextPage_button() {
    return new AlaudaButton($('.aui-icon-angle_right'));
  }

  get listPage_fistPage_button() {
    return new AlaudaButton(
      $('//span[@class="aui-button__content" and text()=" 1 "]'),
    );
  }
  get listPage_pageNum() {
    return new AlaudaDropdown(
      $('.aui-paginator__sizes .aui-input-group'),
      $$('.aui-option'),
    );
  }
  get listPage_eventDetaillink() {
    return new AlaudaButton(element(by.xpath('//aui-table-row[1]//a')));
  }
  get listPage_eventDialogClose() {
    return new AlaudaButton($('.aui-icon-close'));
  }
}
