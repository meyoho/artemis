import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $, $$ } from 'protractor';

import { AcpPageBase } from '../../acp/acp.page.base';

export class AlarmAppDetailPage extends AcpPageBase {
  /**
   * 右上角操作按钮
   */
  get action() {
    return new AlaudaDropdown(
      $('.hasDivider button'),
      $$('aui-menu-item button'),
    );
  }

  getAlaudaElement(name): AlaudaElement {
    switch (name) {
      case '基本信息':
        return new AlaudaElement(
          'rc-field-set-item',
          '.field-set-item__label',
          $('acl-page-state aui-card:nth-child(1) rc-field-set-group'),
        );
      case '告警规则':
        return new AlaudaElement(
          '.aui-card',
          '.aui-card__header',
          $('acl-page-state aui-card:nth-child(2)'),
        );
      case '操作指令':
        return new AlaudaElement(
          '.aui-card',
          '.aui-card__header',
          $('acl-page-state aui-card:nth-child(3)'),
        );
    }
  }

  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '名称':
      case '描述':
      case '资源类型':
      case '资源名称':
      case '创建时间':
      case '更新时间':
        const basicElement = this.getAlaudaElement('基本信息');
        return new AlaudaLabel(
          basicElement.getElementByText(text, '.field-set-item__value'),
        );
      // case '告警规则':
      //   const alarmRuleElement = this.getAlaudaElement(text);
      //   return new AlaudaAuiTable(
      //     alarmRuleElement.getElementByText(
      //       text,
      //       'rc-alarm-detail-rule-list aui-table',
      //     ),
      //   );
      case '操作指令':
        const alarmActionElement = this.getAlaudaElement(text);
        return new AlaudaAuiTable(
          alarmActionElement.getElementByText(
            text,
            'rc-alarm-action-list aui-table',
          ),
        );
    }
  }

  detailPage_operate(operateName) {
    this.getButtonByText('操作').click();
    this.getButtonByText(operateName).click();
  }
}
