import { AlarmThreshold } from '@e2e/element_objects/acp/alarm/alarm_threshold';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { $, ElementFinder, element, by } from 'protractor';

import { AcpPageBase } from '../../acp/acp.page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaAuiTagsInput } from '@e2e/element_objects/alauda.aui_tags_input';

export class AlarmCreateDialog extends AcpPageBase {
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string): any {
    switch (text) {
      case '告警类型':
      case '数据类型':
        return new AlaudaRadioButton(
          this.getElementByText_xpath(text, 'aui-radio-button'),
        );
      case '指标':
      case '告警等级':
      case '持续时间':
      case '单位':
      case '时间范围':
        return new AuiSelect(this.getElementByText_xpath(text, 'aui-select'));
      case '事件原因':
        return new AuiSelect(
          this.getElementByText_xpath(text, 'aui-multi-select'),
        );
      case '阈值':
        return new AlarmThreshold(
          this.getElementByText_xpath(
            text,
            'div[@class="aui-form-item__content"]//aui-input-group[@class="threshold-group aui-form-item__control"]',
          ),
        );
      case '标签':
      case '注解':
        return new ArrayFormTable(
          this.getElementByText_xpath(text, 'alu-array-form-table'),
          '.alu-array-form-table__bottom-control-buttons button:nth-child(1)',
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述', '描述信息')
   */
  enterValue(name, value) {
    switch (name) {
      case '告警类型':
      case '数据类型':
        const type = new AlaudaButton(
          element(
            by.xpath(
              `//label[text() = "${name}"]/../..//span[text()=" ${value} "]`,
            ),
          ),
        );
        type.click();
        break;
      case '指标':
      case '告警等级':
      case '持续时间':
      case '单位':
      case '时间范围':
        const auiSelect: AuiSelect = this.getElementByText(name);
        auiSelect.select(value);
        break;
      case '事件原因':
        const causeSelect: AuiSelect = this.getElementByText(name);
        causeSelect.select(value);
        const textClick = new AlaudaButton(
          element(by.xpath('//label[contains(text(), "事件原因")]')),
        );
        textClick.click();
        break;
      case '阈值':
        this.alaudaElement.scrollToBoWindowBottom();
        const thresold: AlarmThreshold = this.getElementByText(name);
        thresold.input(value);
        break;
      case '自定义指标':
        const inputbox = new AlaudaInputbox(
          element(
            by.xpath(
              '//input[@class="aui-input aui-input--medium ng-untouched ng-pristine ng-invalid"]',
            ),
          ),
        );
        inputbox.input(value);
        break;
      case '日志内容':
        const loginput = new AlaudaAuiTagsInput(
          $('aui-tags-input .aui-tags-input'),
        );
        loginput.input(value);

        break;
      case '高级':
        for (const key in value) {
          const data = value[key];
          switch (key) {
            case '标签':
            case '注解':
              const arrayFormTable: ArrayFormTable = this.getElementByText(key);
              arrayFormTable.fill(data);
              break;
          }
        }
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   * @example fillForm({ 应用名称: 'qq', 镜像源证书: '不使用' })
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  /**
   * 展开高级
   * @param elem
   */
  expandAdvence() {
    const elem: ElementFinder = element(by.xpath('//span[text() = " 高级 "]'));
    this.waitElementPresent(elem, '创建页面高级按钮没出现');
    elem.click();
  }

  /**
   * 折叠高级
   * @param elem
   */
  foldAdvence() {
    const elem: ElementFinder = $('.foldable-trigger aui-icon');
    this.waitElementPresent(elem, '创建页面高级按钮没出现');
    elem.getAttribute('ng-reflect-icon').then(attributeValue => {
      if (attributeValue.includes('up')) {
        elem.click();
      }
    });
  }

  get buttonAdd(): AlaudaButton {
    return new AlaudaButton($('.aui-dialog__footer button[class*="primary"]'));
  }

  get buttonCancel(): AlaudaButton {
    return new AlaudaButton(
      $('.aui-dialog__footer button:not([class*="primary"])'),
    );
  }

  create(data) {
    if (data['高级']) {
      this.alaudaElement.scrollToBoWindowBottom();
      this.expandAdvence();
    }
    this.buttonAdd.scrollToBoWindowBottom();
    this.fillForm(data);
    this.buttonAdd.click();
  }

  waitClose(timeout = 20000) {
    this.waitElementNotPresent(
      $('aui-dialog .aui-dialog__header'),
      '告警创建页dialog 没关闭',
      timeout,
    );
  }
}
