import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AitPageBase } from '../ait.page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AlarmCreateDialog } from './alarmcreate.dialog';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $, by, element, browser, ElementFinder } from 'protractor';
import { AlaudaText } from '@e2e/element_objects/alauda.text';

export class AlarmCreate_AppPage extends AitPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-card .hasDivider', '.aui-card__header');
  }

  private _getAlaudaElement(name): AlaudaElement {
    switch (name) {
      case '基本信息':
        return new AlaudaElement(
          '.aui-form-item',
          '.aui-form-item__label',
          this.alaudaElement.getElementByText(name, 'form'),
        );
      case '告警规则':
      case '操作指令':
        return this.alaudaElement;
    }
  }
  get createDialog(): AlarmCreateDialog {
    return new AlarmCreateDialog();
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '名称':
      case '描述':
        return new AlaudaInputbox(
          this._getAlaudaElement('基本信息').getElementByText(text, 'input'),
        );
      case '操作指令':
        this.alaudaElement.scrollToBoWindowBottom();
        return new ArrayFormTable(
          this.alaudaElement.getElementByText(text, 'rc-array-form-table'),
          '.rc-array-form-table__bottom-control-buttons button:nth-child(1)',
        );
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '基本信息':
        for (const key in value) {
          const data = value[key];
          switch (key) {
            case '名称':
            case '描述':
              const inputbox: AlaudaInputbox = this.getElementByText(key);
              inputbox.input(data);
              break;
            case '资源类型':
              const type = new AlaudaText(
                by.xpath(`//label[text() = "资源类型"]/../..//span`),
              );
              expect(type.getText()).toBe(data);
              break;
            case '资源名称':
              const appname = new AlaudaText(
                by.xpath(`//label[text() = "资源名称"]/../..//span`),
              );
              expect(appname.getText()).toBe(data);
              break;
          }
        }
        break;
      case '告警规则':
      case '告警规则1':
      case '告警规则2':
      case '告警规则3':
      case '告警规则4':
      case '告警规则5':
      case '告警规则6':
      case '告警规则7':
      case '告警规则8':
      case '告警规则9':
      case '告警规则10':
      case '告警规则11':
      case '告警规则12':
      case '告警规则13':
      case '告警规则14':
      case '告警规则15':
      case '告警规则16':
      case '告警规则17':
      case '告警规则18':
      case '告警规则19':
      case '告警规则20':
      case '告警规则21':
      case '告警规则22':
      case '告警规则23':
      case '告警规则24':
      case '告警规则25':
      case '告警规则26':
      case '告警规则27':
      case '告警规则28':
      case '告警规则29':
      case '告警规则30':
      case '告警规则31':
      case '告警规则32':
      case '告警规则33':
      case '告警规则34':
      case '告警规则35':
      case '告警规则36':
      case '告警规则37':
      case '告警规则38':
      case '告警规则39':
      case '告警规则40':
      case '告警规则41':
      case '告警规则42':
      case '告警规则43':
      case '告警规则44':
      case '告警规则45':
      case '告警规则46':
      case '告警规则47':
      case '告警规则48':
      case '告警规则49':
      case '告警规则50':
      case '告警规则51':
      case '告警规则52':
      case '告警规则53':
      case '告警规则54':
      case '告警规则55':
      case '告警规则56':
      case '告警规则57':
      case '告警规则58':
      case '告警规则59':
      case '告警规则60':
      case '告警规则61':
      case '告警规则62':
      case '告警规则63':
      case '告警规则64':
      case '告警规则65':
      case '告警规则66':
      case '告警规则67':
        const index = String(name).replace('告警规则', '');
        this.alaudaElement.scrollToBoWindowBottom();
        const addButton = new AlaudaButton(
          element(
            by.xpath(
              `//span[@class="aui-button__content" and text()=" 添加告警规则 "]/..`,
            ),
          ),
        );
        addButton.click();
        this.createDialog.create(value);
        value['验证规则'].forEach((text, num) => {
          num = num + 1;
          const rule = new AlaudaText(
            by.xpath(`//rc-alarm-rule-list//tbody//tr[${index}]//td[${num}]`),
          );
          expect(rule.getText()).toBe(text);
        });
        break;

      case '更新告警规则1':
        const trnum = String(name).replace('更新告警规则', '');
        this.alaudaElement.scrollToBoWindowBottom();
        const updateButton = new AlaudaButton(
          element(
            by.xpath(
              `//rc-alarm-rule-list//tbody//tr[${trnum}]//td[4]//button[1]`,
            ),
          ),
        );
        updateButton.click();
        this.createDialog.create(value);
        value['验证规则'].forEach((text, tdnum) => {
          tdnum = tdnum + 1;
          const rule = new AlaudaText(
            by.xpath(`//rc-alarm-rule-list//tbody//tr[${trnum}]//td[${tdnum}]`),
          );
          expect(rule.getText()).toBe(text);
        });
        break;
      case '删除告警规则1':
        const deletenum = String(name).replace('删除告警规则', '');
        this.alaudaElement.scrollToBoWindowBottom();
        const deleteButton = new AlaudaButton(
          element(
            by.xpath(
              `//rc-alarm-rule-list//tbody//tr[${deletenum}]//td[4]//button[2]`,
            ),
          ),
        );
        deleteButton.click();
        break;
      case '操作指令':
        this.createDialog.waitClose();
        const arrayFormTable: ArrayFormTable = this.getElementByText(name);
        arrayFormTable.fill(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  _getToolTip(): ElementFinder {
    const xpath = `//rc-tooltip-content[1]//span`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  inputByText(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -, .',
  ) {
    if (value != '') {
      const inputbox: AlaudaInputbox = this.getElementByText(text);
      inputbox.input(value);
    }
    if (errortip != '') {
      browser.sleep(20);
      this._getToolTip()
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }
  errorAlert(text: string) {
    return new AlaudaText(by.xpath(`//div[text()='${text}']`));
  }
  get errorAlert_close() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
}
