import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AitPageBase } from '../ait.page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $, by, element, browser, ElementFinder, $$ } from 'protractor';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class AlarmCreate_UseTmpPage extends AitPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-card .hasDivider', '.aui-card__header');
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '告警名称':
        const inputbox = new AlaudaInputbox(
          element(by.xpath('//input[@name="name"]')),
        );
        inputbox.input(value);
        break;
      case '模板名称':
        const dropdownName = new AlaudaDropdown(
          element(by.xpath(`//label[text()='模板名称']/../..//aui-select`)),
          $$('.aui-option'),
        );
        dropdownName.select(value);
        break;
      case '资源类型':
      case '资源名称':
        const text = new AlaudaText(
          by.xpath(
            `//label[text()='${name}']/../..//div[@class="aui-form-item__content"]`,
          ),
        );
        expect(text.getText()).toBe(value);
        break;
      case '告警规则':
        value.forEach((itemList, trnum) => {
          trnum = trnum + 1;
          itemList.forEach((text, tdnum) => {
            tdnum = tdnum + 1;
            const rule = new AlaudaText(
              by.xpath(
                `//label[text()="告警规则"]/../..//table[@class="alarm-table-rules"]//tbody//tr[${trnum}]//td[${tdnum}]`,
              ),
            );
            expect(rule.getText()).toBe(text);
          });
        });
        break;
      case '操作指令':
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }

  _getToolTip(): ElementFinder {
    const xpath = `//rc-tooltip-content[1]//span`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  inputByText(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
  ) {
    if (value != '') {
      const inputbox: AlaudaInputbox = this.getElementByText(text);
      inputbox.input(value);
    }
    if (errortip != '') {
      browser.sleep(20);
      this._getToolTip()
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }
  errorAlert(text: string) {
    return new AlaudaText(by.xpath(`//div[text()='${text}']`));
  }
  get errorAlert_close() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
}
