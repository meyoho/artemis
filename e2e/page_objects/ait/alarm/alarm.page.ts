import { $, browser } from 'protractor';
import { AitPageBase } from '../ait.page.base';
import { AlarmCreatePage } from './alarmcreate.page';
import { PreparePage } from './alarmprepare.page';
import { AlarmListPage } from './alarmlist.page';
import { AlarmListPageVerify } from '@e2e/page_verify/ait/alarm/alarm.list.page';
import { AlarmDetailPageVerify } from '@e2e/page_verify/ait/alarm/alarm.detail.page';
import { AlarmAppDetailPageVerify } from '@e2e/page_verify/ait/alarm/alarm-app.detail.page';
import { AlarmDetailPage } from './alarmdetail.page';
import { AlarmTmpCreatePage } from './alarmtemplatecreate.page';
import { AlarmCreate_UseTmpPage } from './alarmcreate-usetmp.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlarmCreate_AppPage } from './alarmcreate-app.page';

export class AlarmPage extends AitPageBase {
  get listPage(): AlarmListPage {
    return new AlarmListPage();
  }
  get listPageVerify(): AlarmListPageVerify {
    return new AlarmListPageVerify();
  }
  get detailPageVerify(): AlarmDetailPageVerify {
    return new AlarmDetailPageVerify();
  }
  get detailAppPageVerify(): AlarmAppDetailPageVerify {
    return new AlarmAppDetailPageVerify();
  }
  get createPage() {
    return new AlarmCreatePage();
  }
  get createPage_useTmp() {
    return new AlarmCreate_UseTmpPage();
  }
  get createTmpPage() {
    return new AlarmTmpCreatePage();
  }
  get detailPage(): AlarmDetailPage {
    return new AlarmDetailPage();
  }

  get preparePage(): PreparePage {
    return new PreparePage();
  }
  get createAppPage() {
    return new AlarmCreate_AppPage();
  }
  createAlarm(testData) {
    browser.sleep(500);
    this.getButtonByText('创建告警').click();
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  updateAlarm(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  createAlarm_add(testData) {
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
  }

  get svnbutton() {
    return new AlaudaButton($('.alarm-dashboard .aui-icon-angle_down'));
  }
  createAlarm_useTmp(testData) {
    this.svnbutton.click();
    this.getButtonByText('模板创建告警').click();
    this.createPage_useTmp.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }

  createAlarmTemplate(testData) {
    this.getButtonByText('创建告警模板').click();
    this.createTmpPage.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  updateAlarmTemplate(testData) {
    this.createTmpPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  createAlarmTemplate_add(testData) {
    this.createTmpPage.fillForm(testData);
    this.getButtonByText('创建').click();
  }

  createAlarmApp(testData) {
    this.getButtonByText('创建告警').click();
    this.createAppPage.fillForm(testData);
    this.getButtonByText('创建').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  updateAlarmApp(testData) {
    this.createAppPage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
  private _waitListPageDisplay(errorMessage) {
    this.waitElementPresent(
      $('acl-page-guard aui-table'),
      errorMessage,
      10000,
    ).then(isPresent => {
      if (!isPresent) {
        browser.refresh();
        browser.sleep(2000);
      }
    });
  }

  async switchCluster_onAdminView(name: string) {
    await super.switchCluster_onAdminView(name);
    this._waitListPageDisplay(`切换${name}集群后，告警列表页没有正常加载`);
    return browser.sleep(1000);
  }

  /**
   * 单击左导航
   * @example clickLeftNavByText('配置字典')
   */
  async clickLeftNavByText(text) {
    await super.clickLeftNavByText(text);
    this._waitListPageDisplay(`单击左导航${text}后，告警列表页没有正常加载`);
  }
  clickLeftNavByText_nowait(text): void {
    super.clickLeftNavByText(text);
  }
}
