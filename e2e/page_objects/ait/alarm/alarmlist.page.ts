import { TableComponent } from '@e2e/component/table.component';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $, browser } from 'protractor';

import { AcpPageBase } from '../../acp/acp.page.base';

export class AlarmListPage extends AcpPageBase {
  get alarmTable() {
    return new TableComponent('alu-prometheus-list', '.aui-card__header');
  }

  get nameSpaceDropDown(): AuiSelect {
    return new AuiSelect($('alu-prometheus-list aui-select'));
  }

  get auiSearch() {
    return new AuiSearch($('alu-prometheus-list aui-search'));
  }

  get noData(): AlaudaLabel {
    return new AlaudaLabel(
      $('acl-k8s-resource-list-footer .empty-placeholder'),
    );
  }

  search(name, rowCount = 1) {
    this.alarmTable.search(name, rowCount);
  }

  enterDetail(name) {
    this.waitElementPresent(
      $('acl-page-guard aui-table'),
      '告警列表页没有正常加载',
      10000,
    ).then(isPresent => {
      if (!isPresent) {
        browser.refresh();
        browser.sleep(2000);
      }
    });
    this.search(name);
    return this.alarmTable.clickResourceNameByRow([name]);
  }

  private _wait(elem, name, timeout = 20000) {
    return browser.driver
      .wait(() => {
        this.clickLeftNavByText('告警模板');
        browser.sleep(1000);
        this.search(name);
        browser.sleep(1000);
        return elem.isPresent().then(isPresent => {
          return isPresent;
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }

  /**
   * 等待告警状态变化
   * @param name 告警名称
   * @param status success, pending, error
   */
  waitStatusChange(name, status = 'success', timeout = 20000) {
    this.search(name);
    this.alarmTable.getCell('状态', [name]).then(cell => {
      const statusBar = cell.$('.ratio-container aui-status-bar');
      browser.sleep(1000);

      switch (status) {
        case 'success':
          this._wait(statusBar.$('div[class*="success"]'), name, timeout);
          break;
        case 'error':
          this._wait(statusBar.$('div[class*="error"]'), name, timeout);
          break;
        case 'pending':
          this._wait(statusBar.$('div[class*="pending"]'), name, timeout);
          break;
      }
    });
  }

  update(name) {
    this.alarmTable.clickOperationButtonByRow(
      [name],
      '更新',
      'button aui-icon',
    );
  }

  delete(name) {
    this.search(name);
    this.alarmTable.clickOperationButtonByRow(
      [name],
      '删除',
      'button aui-icon',
    );
    this.confirmDialog.clickConfirm();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }

  // 告警模板列表页
  get alarmTemplateTable() {
    return new TableComponent('aui-card', '.aui-card__header');
  }
  search_AlarmtmpList(name, rowCount = 1) {
    this.clickLeftNavByText('告警模板');
    this.alarmTemplateTable.search(name, rowCount);
  }
  enterDetail_AlarmtmpList(name) {
    this.search_AlarmtmpList(name);
    this.alarmTemplateTable.clickResourceNameByRow([name]);
  }
  delete_AlarmtmpList(name) {
    this.search_AlarmtmpList(name);
    this.alarmTemplateTable.clickOperationButtonByRow(
      [name],
      '删除',
      'button aui-icon',
    );
    this.confirmDialog.clickConfirm();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }

  // 应用告警列表页
  get alarmappTable() {
    return new TableComponent('aui-card', '.aui-card__header');
  }
  search_AlarmappList(name, rowCount = 1) {
    // this.clickLeftNavByText('告警模板');
    this.alarmappTable.search(name, rowCount);
  }
  enterDetail_AlarmappList(name) {
    this.search_AlarmappList(name);
    this.alarmappTable.clickResourceNameByRow([name]);
  }
  delete_AlarmappList(name) {
    this.search_AlarmappList(name);
    this.alarmappTable.clickOperationButtonByRow(
      [name],
      '删除',
      'button aui-icon',
    );
    this.confirmDialog.clickConfirm();
    this.toast.getMessage().then(text => {
      console.log(text);
    });
  }
}
