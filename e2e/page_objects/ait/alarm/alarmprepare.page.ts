import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { AitPreparePage } from '../aitprepare.page';
import { ServerConf } from '@e2e/config/serverConf';

export class PreparePage extends AitPreparePage {
  deleteAlarm(name, ns = ServerConf.GLOBAL_NAMESPCE) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete PrometheusRule -n ${ns} ${name}`,
      this.clusterName,
    );
  }
  deleteAlarmTemplate(name) {
    CommonKubectl.execKubectlCommand(`kubectl delete alerttemplate ${name}`);
  }
}
