import { AcpPageBase } from '../../acp/acp.page.base';
import { element, by, $$ } from 'protractor';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class MonitoringDetail extends AcpPageBase {
  monitoring_view(name, text) {
    const ele = element(
      by.xpath(`//div[text()="${text}"]//..//div[2]//highcharts-chart//div`),
    );
    this.waitElementPresent(ele, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`${name}监控--${text} 未出现`);
      }
    });
    return ele;
  }
  monitoring_line(name) {
    const ele = element(by.css('.highcharts-tracker-line'));
    this.waitElementPresent(ele, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`${name}没有监控`);
      }
    });
    return ele;
  }
  group_dropdown(value) {
    const dropdownName = new AlaudaDropdown(
      element(by.xpath(`//label[text()='分组方式']/../..//aui-select`)),
      $$('.aui-option'),
    );
    dropdownName.select(value);
  }
  value_dropdown(value) {
    const dropdownName = new AlaudaDropdown(
      element(by.xpath(`//label[text()=' 聚合方式 ']/../..//aui-select`)),
      $$('.aui-option'),
    );
    dropdownName.select(value);
  }
  time_dropdown(value) {
    const dropdownName = new AlaudaDropdown(
      element(by.xpath(`//label[text()=' 时间范围 ']/../..//aui-select`)),
      $$('.aui-option'),
    );
    dropdownName.select(value);
  }
}
