import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $ } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

export class RoleListPage extends PlatformPageBase {
  get roleTable(): AlaudaAuiTable {
    return new AlaudaAuiTable($('.aui-card__content aui-table'));
  }

  /**
   * 进入角色的详情页
   * @param roleName 角色名称
   */
  enterDetail(roleName) {
    const button = new AlaudaButton(this.roleTable.getRow([roleName]).$('a'));
    button.click();
    this.waitElementPresent(
      $('.hasDivider>div[class="aui-card__header"]'),
      `角色【${roleName}】 详情页没有打开`,
    );
  }
}
