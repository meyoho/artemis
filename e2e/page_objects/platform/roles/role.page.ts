import { RoleDetailPageVerify } from '@e2e/page_verify/platform/roles/role.details.page';
import { RoleListPageVerify } from '@e2e/page_verify/platform/roles/role.list.page';

import { PlatformPageBase } from '../platform.page.base';

import { RoleDetailPage } from './role.details.page';
import { RoleListPage } from './role.list.page';
import { RoleCreatePage } from './role.create.page';
import { RolePreparePage } from './role.prepare.page';

export class RolePage extends PlatformPageBase {
  /**
   * 详情页面
   */
  get detailPage(): RoleDetailPage {
    return new RoleDetailPage();
  }

  /**
   * 列表页
   */
  get listPage(): RoleListPage {
    return new RoleListPage();
  }

  get roleListPageVerify(): RoleListPageVerify {
    return new RoleListPageVerify();
  }

  get roleDetailPageVerify(): RoleDetailPageVerify {
    return new RoleDetailPageVerify();
  }

  get createPage(): RoleCreatePage {
    return new RoleCreatePage();
  }
  get preparePage(): RolePreparePage {
    return new RolePreparePage();
  }
}
