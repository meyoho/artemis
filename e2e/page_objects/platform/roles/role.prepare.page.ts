import { CommonKubectl } from '../../../utility/common.kubectl';

import { ServerConf } from '../../../config/serverConf';
import { AcpPreparePage } from '../../../page_objects/acp/prepare.page';

export class RolePreparePage extends AcpPreparePage {
  /**
   * 删除自定义的角色
   * @param name 角色名称
   */
  deleteRoleTemplate(name: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete roletemplates.auth.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} ${name}`,
    );
  }
}
