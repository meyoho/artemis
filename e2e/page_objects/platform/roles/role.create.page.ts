/**
 * Created by zhangjiao on 2018/7/5.
 */
import { $, browser, element, by } from 'protractor';

import { AlaudaElement } from '../../../element_objects/alauda.element';
import { PlatformPageBase } from '../platform.page.base';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaAuiTagsInput } from '../../../element_objects/alauda.aui_tags_input';

export class RoleCreatePage extends PlatformPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item', '.aui-form-item__label');
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '':
        return new AlaudaRadioButton();
      default:
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, tagname),
        );
    }
  }

  entryValue(text: string, value: string): any {
    switch (text) {
      //   case '名称':
      //     break;
      //   case '描述':
      //     this.getElementByText(text, 'textarea').sendKeys(value);
      //     break;
      case 'API 组':
        const selectclick = new AlaudaButton(
          element(
            by.xpath(
              `//label[text()="API 组"]/../..//aui-radio-group//span[text()="${value}"]`,
            ),
          ),
        );
        selectclick.click();
        break;
      case '添加自定义资源':
        this.alaudaElement.scrollToBoWindowBottom();
        const addClick = new AlaudaButton(
          element(
            by.xpath(
              '//span[@class="aui-button__content" and text()=" 添加自定义资源 "]/..',
            ),
          ),
        );
        addClick.click();
        break;
      case '权限':
        this.alaudaElement.scrollToBoWindowBottom();
        const addVerb = new AlaudaButton(
          element(
            by.xpath(
              `//div[text()="${value}"]/../../..//div[@class="aui-accordion-item__content-body"]//td[@class="verb"][1]`,
            ),
          ),
        );
        addVerb.click();
        break;
      default:
        const inputbox: AlaudaInputbox = this.getElementByText(text);
        inputbox.input(value);
      // this.getElementByText(text).sendKeys(value);
    }
  }

  /**
   * 填写表单
   * @param data 测试数据
   */
  fillForm(data) {
    for (const key in data) {
      this.entryValue(key, data[key]);
    }
  }

  AlaudaElement(formName = 'alu-role-basic-info-form') {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'label[class="aui-form-item__label"]',
      $(formName),
    );
  }

  inputByText(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
  ) {
    const root = this.AlaudaElement();
    if (value != '') {
      new AlaudaInputbox(root.getElementByText(text, 'input')).input(value);
    }
    if (errortip != '') {
      browser.sleep(20);
      root
        .getElementByText(text, '.aui-form-item__error')
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }

  inputByText1(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -，.',
  ) {
    const root = this.AlaudaElement('.aui-dialog ng-component');
    if (value != '') {
      if (text == '资源名称') {
        const resource = new AlaudaAuiTagsInput(
          root.getElementByText(text, '.aui-tags-input'),
        );
        resource.delete();
        resource.input(value);
      } else {
        new AlaudaInputbox(root.getElementByText(text, 'input')).input(value);
      }
    }
    if (errortip != '') {
      browser.sleep(20);
      root
        .getElementByText(text, '.aui-form-item__error')
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }
  errorAlert() {
    return new AlaudaText(
      by.xpath(
        // `//div[text()='roletemplates.auth.alauda.io "${name}" already exists']`,
        `//div[text()='资源已存在']`,
      ),
    );
  }
  get errorAlert_close() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
}
