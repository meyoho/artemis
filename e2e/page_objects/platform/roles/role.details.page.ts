import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { $ } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';

export class RoleDetailPage extends PlatformPageBase {
  /**
   * 资源详情页的‘基本信息’部分
   */
  get alaudaElement() {
    return new AlaudaElement(
      'div alu-field-set-item',
      'label',
      $('alu-field-set-group'),
    );
    // return new AlaudaElement('.alu-detail__field', 'label', $('.alu-detail'));
  }

  detailPage_operate(operateName) {
    this.getButtonByText('操作').click();
    this.getButtonByText(operateName).click();
  }
  get confirmDialog_name() {
    return new AuiConfirmDialog($('alu-confirm-delete'));
  }
}
