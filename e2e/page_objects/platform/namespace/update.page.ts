import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $ } from 'protractor';

import { CreateNamespacePage } from './create.page';

export class UpdateNamespacePage extends CreateNamespacePage {
    /**
     *  更新对话框
     */
    get updateDialog() {
        return new AuiDialog($('.aui-dialog-overlay-pane aui-dialog'));
    }
    getElementByText(text: string, tagname: string = 'input') {
        return new AlaudaInputbox(
            this.alaudaElement.getElementByText(text, tagname)
        );
    }
    enterValue(name, value) {
        this.getElementByText(name).input(value);
    }
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            '.aui-dialog__content .aui-form-item',
            '.aui-form-item  .aui-form-item__label'
        );
    }
}
