import { CommonKubectl } from '@e2e/utility/common.kubectl';
import {
  k8s_type_namespaces,
  k8s_type_limitranges,
  k8s_type_resourcequotas,
} from '@e2e/utility/resource.type.k8s';

import { PlatformPageBase } from '../platform.page.base';

export class NsPrepare extends PlatformPageBase {
  deleleNs(nsname: string, region_name = this.RegionName) {
    CommonKubectl.deleteResource(k8s_type_namespaces, nsname, region_name);
    CommonKubectl.deleteResource(
      k8s_type_limitranges,
      'default',
      region_name,
      nsname,
    );
    CommonKubectl.deleteResource(
      k8s_type_resourcequotas,
      'default',
      region_name,
      nsname,
    );
  }
  createNs(testData, region_name = this.RegionName): void {
    CommonKubectl.createResourceByTemplate(
      'alauda.ns-for-import.yaml',
      testData,
      'qa-namespace' + String(new Date().getMilliseconds()),
      region_name,
    );
  }
}
