import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $ } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

export class DeleteNamespacePage extends PlatformPageBase {
  /**
   * 输入待删除的名称
   * @param name 名称
   */
  inputname(name) {
    new AlaudaInputbox($('.aui-dialog__content input')).input(name);
  }
  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(
      $('.aui-dialog__footer button[aui-button="danger"]'),
    );
  }
  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton($('.aui-dialog__footer button[aui-button=""]'));
  }
}
