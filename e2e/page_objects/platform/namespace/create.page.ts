import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiDropdownButton } from '@e2e/element_objects/alauda.aui_dropdown_button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';
import { $, $$, ElementFinder, by, element } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

export class CreateNamespacePage extends PlatformPageBase {
  /**
   * 获取表单section
   * @param text 基本信息
   */
  getSectionByText(text: string): ElementFinder {
    const sections = $$('alu-card-section').filter(ele => {
      return ele
        .$('.card-section__title')
        .getText()
        .then(str => {
          return str === text;
        });
    });
    return sections.first();
  }
  /**
   * 获取表单块
   * @param form_type CPU/内存
   * @param section_name 容器限额
   */
  getFormByType(form_type: string, section_name = '容器限额') {
    const form = this.getSectionByText(section_name)
      .$$('.limit-range-form__item')
      .filter(ele => {
        return ele
          .$('.limit-range-form__item__type')
          .getText()
          .then(text => {
            return text === form_type;
          });
      })
      .first();
    return form;
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  findElementByText(
    text: string,
    root_element: ElementFinder,
    tagname = 'input',
  ): any {
    const root = new AlaudaElement(
      '.aui-card__content .aui-form-item',
      '.aui-form-item  .aui-form-item__label',
      root_element,
    );
    switch (text) {
      case '所属集群':
        return new AlaudaDropdown(
          root.getElementByText(text, 'aui-select'),
          $$('.cdk-overlay-pane aui-tooltip .aui-option'),
        );
      case '标签':
        return new ArrayFormTable(
          root.getElementByText(
            text,
            'alu-key-value-form-table[formcontrolname="labels"] alu-array-form-table',
          ),
          'button:nth-child(1)',
        );
      case '注解':
        return new ArrayFormTable(
          root.getElementByText(
            text,
            'alu-key-value-form-table[formcontrolname="annotations"] alu-array-form-table',
          ),
          'button:nth-child(1)',
        );
      default:
        return new AlaudaInputbox(root.getElementByText(text, tagname));
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '基本信息':
      case '资源配额':
        for (const key in value) {
          if (key === '所属集群') {
            this.clusterDisplayName(value[key]);
            this.findElementByText(key, this.getSectionByText(name)).select(
              `${value[key]}(${this.clusterDisplayName(value[key])})`,
            );
          } else {
            this.findElementByText(key, this.getSectionByText(name)).input(
              value[key],
            );
          }
        }
        break;
      case '容器限额':
        this.alaudaElement.scrollToBoWindowBottom();
        for (const key in value) {
          for (const k in value[key]) {
            switch (key) {
              case 'CPU':
                switch (k) {
                  case '请求默认值':
                    if (!this._getFeature().includes('cpu')) {
                      this.findElementByText(k, this.getFormByType(key)).input(
                        value[key][k],
                      );
                    }
                    break;
                  default:
                    this.findElementByText(k, this.getFormByType(key)).input(
                      value[key][k],
                    );
                    break;
                }
                break;
              case '内存':
                switch (k) {
                  case '请求默认值':
                    if (!this._getFeature().includes('memory')) {
                      this.findElementByText(k, this.getFormByType(key)).input(
                        value[key][k],
                      );
                    }
                    break;
                  default:
                    this.findElementByText(k, this.getFormByType(key)).input(
                      value[key][k],
                    );
                    break;
                }
                break;
            }
          }
        }
        break;
      case '更多配置':
        for (const key in value) {
          this.findElementByText(key, this.getSectionByText(name)).fill(
            value[key],
          );
        }
        break;
    }
  }

  /**
   * 获得超售比信息
   */
  private _getFeature(): string {
    const result = CommonKubectl.execKubectlCommand(
      'kubectl get Feature resourceratio -o yaml',
      this.RegionName,
    );
    if (result.includes('NotFound')) {
      return '';
    } else {
      return CommonMethod.parseYaml(result).spec.deployInfo;
    }
  }
  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   * @example fillForm({ 应用名称: 'qq', 镜像源证书: '不使用' })
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
  clickButtonByText(text) {
    this.waitElementPresent(
      element(by.xpath(`//button/span[contains(text(),'${text}')]`)),
      `按钮${text}未出现`,
    );
    element(by.xpath(`//button/span[contains(text(),'${text}')]`)).click();
  }

  /**
   * 创建ns
   * @param testData 创建ns的表单数据
   */
  create_ns(testData) {
    this.clickLeftNavByText('命名空间');
    new AuiDropdownButton(
      $('acl-disabled-container aui-dropdown-button'),
      $('.cdk-overlay-pane aui-tooltip'),
    ).click();
    this.fillForm(testData);
    this.clickButtonByText('创建');
  }
}
