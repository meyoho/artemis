import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaTabItem } from '@e2e/element_objects/alauda.tabitem';
import { $, $$, ElementFinder, by } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

import { NamespaceListPage } from './list.page';

export class DetailNamespacePage extends PlatformPageBase {
  get listPage(): NamespaceListPage {
    return new NamespaceListPage();
  }
  get name(): ElementFinder {
    this.waitElementPresent(
      $(
        'alu-namespace-detail-info aui-card:nth-child(1) div[fxflexalign="center"]',
      ),
      '校验namespace名称是否存在',
    );
    return $(
      'alu-namespace-detail-info aui-card:nth-child(1) div[fxflexalign="center"]',
    );
  }
  /**
   * 详情页tabs
   */
  tabs(name) {
    return new AlaudaTabItem(
      by.xpath("//div[contains(@class,'aui-tab-label')]"),
      by.xpath(
        "//div[@class='aui-tab-header__labels']/div[contains(@class,'isActive')]",
      ),
      by.xpath(
        `//div[contains(@class,'aui-tab-label')]/div[contains(text(),'${name}')]`,
      ),
    );
  }
  /**
   * 切换tab
   */
  switchtab(name) {
    this.tabs(name).click();
  }
  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog($('aui-dialog .aui-dialog'));
  }

  /**
   * 基本信息 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'alu-namespace-basic-info alu-field-set-item',
      '.field-set-item__label label',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
    tagname = '.field-set-item__value',
  ): ElementFinder {
    switch (text) {
      case '注解':
      case '标签':
        return this.alaudaElement.getElementByText(text, 'alu-tags-label');
      default:
        return this.alaudaElement.getElementByText(text, tagname);
    }
  }
  /**
   * 获取详情页的资源配额
   */
  get quotaTable() {
    return new AlaudaAuiTable($('alu-quota-horizontal aui-table'));
  }
  /**
   *
   * @param resourcetype CPU、内存、存储、Pods 数、PVC 数
   * @param name 配额名称 已分配、总配额
   */
  getResourceQuotaValue(resourcetype: string, name = '总配额') {
    return this.quotaTable.getCell(name, [resourcetype]).then(ele => {
      return ele.getText();
    });
  }
  /**
   * 获取详情页的容器限额
   */
  get limitrange() {
    return new AlaudaAuiTable($('alu-limit-range aui-table'));
  }
  /**
   *
   * @param indicator 指标cpu、memory
   * @param name 指标名称 默认值/最大值
   */
  getLimitRangeValue(indicator: string, name: string) {
    switch (indicator) {
      case 'CPU':
      case 'cpu':
        return this.limitrange.getCell(name, ['CPU']).then(ele => {
          return ele.getText();
        });
      case '内存':
      case 'memory':
        return this.limitrange.getCell(name, ['内存']).then(ele => {
          return ele.getText();
        });
      default:
        return null;
    }
  }
  /**
   * 详情页的操作按钮
   */
  get action() {
    return new AlaudaDropdown(
      $('aui-card .aui-button aui-icon'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }

  /**
   * 点击详情页的更新资源配额
   */
  clickupdatequota() {
    this.action.select('更新资源配额');
  }
  /**
   * 点击详情页的更新容器限额
   */
  clickupdatelimitrange() {
    this.action.select('更新容器限额');
  }
  /**
   * 点击详情页的移除命名空间
   */
  clickremove() {
    this.action.select('移除命名空间');
  }
  /**
   * 点击详情页的删除命名空间
   */
  clickdelete() {
    this.action.select('删除命名空间');
  }
  /**
   * 点击详情页的更新注解按钮
   */
  clickupdateannotation() {
    this.getElementByText('注解')
      .$('.basic-pencil_s')
      .click();
  }
  /**
   * 点击详情页的更新标签按钮
   */
  clickupdatelabel() {
    this.getElementByText('标签')
      .$('.basic-pencil_s')
      .click();
  }
  /**
   * 更新注解、标签的对话框
   */
  get updateTable() {
    return new ArrayFormTable(
      $('aui-dialog-content alu-array-form-table'),
      '.alu-array-form-table__bottom-control-buttons button',
    );
  }
  /**
   * 更新注解
   */
  updateannotation(name, tesData) {
    this.listPage.enterDetail(name);
    this.clickupdateannotation();
    this.updateTable.fill(tesData);
    this.auiDialog.clickConfirm();
  }
  /**
   * 更新标签
   */
  updatelabel(name, tesData) {
    this.listPage.enterDetail(name);
    this.clickupdatelabel();
    this.updateTable.fill(tesData);
    this.auiDialog.clickConfirm();
  }

  private _getElement(type, name) {
    if (type === 'CPU') {
      return new AlaudaInputbox(
        new AlaudaElement(
          '.aui-dialog__content .aui-form-item',
          '.aui-form-item__label',
          $('alu-limit-range-base-form>div:nth-child(1)'),
        ).getElementByText(name, 'input'),
      );
    } else {
      return new AlaudaInputbox(
        new AlaudaElement(
          '.aui-dialog__content .aui-form-item',
          '.aui-form-item__label',
          $('alu-limit-range-base-form>div:nth-child(2)'),
        ).getElementByText(name, 'input'),
      );
    }
  }

  /**
   * 更新容器限额
   * @param testData 测试数据
   */
  updateContainerLimitRange(name, testData) {
    this.listPage.enterDetail(name);
    this.clickupdatelimitrange();
    for (const key in testData) {
      switch (key) {
        case '容器限额':
          for (const keyType in testData[key]) {
            switch (keyType) {
              case 'CPU':
                for (const itemKey in testData[key][keyType]) {
                  const inputValue = testData[key][keyType][itemKey];
                  switch (itemKey) {
                    case '请求默认值':
                      this._getElement(keyType, itemKey).input(inputValue);
                      break;
                    default:
                      this._getElement(keyType, itemKey).input(inputValue);
                      break;
                  }
                }

                break;
              case '内存':
                for (const itemKey in testData[key][keyType]) {
                  const inputValue = testData[key][keyType][itemKey];
                  switch (itemKey) {
                    case '请求默认值':
                      this._getElement(keyType, itemKey).input(inputValue);

                      break;
                    default:
                      this._getElement(keyType, itemKey).input(inputValue);
                      break;
                  }
                }
                break;
            }
          }
          break;
      }
    }

    new AlaudaButton($('.aui-dialog__footer button[class*="primary"]')).click();
  }
}
