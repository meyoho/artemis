import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiDropdownButton } from '@e2e/element_objects/alauda.aui_dropdown_button';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, $$, ElementFinder, by, element } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class ImportNamespacePage extends PlatformPageBase {
  /**
   * 获取表单section
   * @param text 基本信息
   */
  getSectionByText(text: string): ElementFinder {
    const sections = $$('alu-card-section').filter(ele => {
      return ele
        .$('.card-section__title')
        .getText()
        .then(str => {
          return str === text;
        });
    });
    return sections.first();
  }
  /**
   * 获取表单块
   * @param form_type CPU/内存
   * @param section_name 容器限额
   */
  getFormByType(form_type: string, section_name = '容器限额') {
    const form = this.getSectionByText(section_name)
      .$$('.limit-range-form__item')
      .filter(ele => {
        return ele
          .$('.limit-range-form__item__type')
          .getText()
          .then(text => {
            return text === form_type;
          });
      })
      .first();
    return form;
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  findElementByText(
    text: string,
    root_element: ElementFinder,
    tagname = 'input',
  ): ElementFinder {
    const root = new AlaudaElement(
      '.aui-card__content .aui-form-item',
      '.aui-form-item  .aui-form-item__label',
      root_element,
    );
    switch (text) {
      case '所属集群':
      case '命名空间':
        return root.getElementByText(text, 'aui-select');
      case '标签':
        return root.getElementByText(
          text,
          'alu-key-value-form-table[formcontrolname="labels"] alu-array-form-table',
        );
      case '注解':
        return root.getElementByText(
          text,
          'alu-key-value-form-table[formcontrolname="annotations"] alu-array-form-table',
        );
      default:
        return root.getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value) {
    switch (name) {
      case '基本信息':
      case '资源配额':
        for (const key in value) {
          if (key === '所属集群' || key === '命名空间') {
            new AlaudaDropdown(
              this.findElementByText(key, this.getSectionByText(name)),
              $$('.cdk-overlay-pane aui-tooltip .aui-option'),
            ).select(value[key]);
          } else {
            new AlaudaInputbox(
              this.findElementByText(key, this.getSectionByText(name)),
            ).input(value[key]);
          }
        }
        break;
      case '容器限额':
        this.alaudaElement.scrollToBoWindowBottom();
        for (const key in value) {
          for (const k in value[key]) {
            switch (key) {
              case 'CPU':
                new AlaudaInputbox(
                  this.findElementByText(k, this.getFormByType(key)),
                ).input(value[key][k]);
                break;

              case '内存':
                new AlaudaInputbox(
                  this.findElementByText(k, this.getFormByType(key)),
                ).input(value[key][k]);
                break;
            }
          }
        }
        break;
      case '更多配置':
        for (const key in value) {
          new ArrayFormTable(
            this.findElementByText(key, this.getSectionByText(name)),
            'button:nth-child(1)',
          ).fill(value[key]);
        }
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   * @example fillForm({ 应用名称: 'qq', 镜像源证书: '不使用' })
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
  clickButtonByText(text) {
    this.waitElementPresent(
      element(by.xpath(`//button/span[contains(text(),'${text}')]`)),
      `按钮${text}未出现`,
    );
    element(by.xpath(`//button/span[contains(text(),'${text}')]`)).click();
  }
  /**
   * 导入ns
   * @param testData 创建ns的表单数据
   */
  import_ns(testData) {
    this.clickLeftNavByText('命名空间');
    new AuiDropdownButton(
      $('acl-disabled-container aui-dropdown-button'),
      $('.cdk-overlay-pane aui-tooltip'),
    ).select('导入命名空间');
    this.fillForm(testData);
    this.clickButtonByText('导入');
  }
  verify_ns(region_name, namespace) {
    this.clickLeftNavByText('命名空间');
    new AuiDropdownButton(
      $('acl-disabled-container aui-dropdown-button'),
      $('.cdk-overlay-pane aui-tooltip'),
    ).select('导入命名空间');
    new AlaudaDropdown(
      this.findElementByText('所属集群', this.getSectionByText('基本信息')),
      $$('.cdk-overlay-pane aui-tooltip .aui-option'),
    ).select(region_name);
    const result = new AlaudaDropdown(
      this.findElementByText('命名空间', this.getSectionByText('基本信息')),
      $$('.cdk-overlay-pane aui-tooltip .aui-option'),
    ).checkDataExistInDropdown(namespace);
    new AlaudaButton($('.form__footer button[aui-button=""]')).click();
    return result;
  }
}
