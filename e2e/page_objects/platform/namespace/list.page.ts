import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { $, browser, by, element } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

import { CreateNamespacePage } from './create.page';
import { DeleteNamespacePage } from './delete.page';
import { DetailNamespacePage } from './detail.page';
import { UpdateNamespacePage } from './update.page';

export class NamespaceListPage extends PlatformPageBase {
  /**
   * 命名空间管理列表
   */
  get namespaceNameTable() {
    return new AuiTableComponent(
      $('aui-card .aui-card'),
      '.aui-card__content aui-table',
      'aui-search .aui-search',
    );
  }
  /**
   * 检索框
   */
  get search() {
    return new AuiSearch($('.aui-card__content aui-search'));
  }
  /**
   * 创建命名空间按钮
   */
  clickadd() {
    const button = this.getButtonByText('创建命名空间');
    this.waitElementPresent(button.button, '创建命名空间按钮未出现', 100);
    button.click();
  }
  /**
   * 点击名称进入详情页
   * @param name 名称
   */
  enterDetail(name: string) {
    this.clickLeftNavByText('命名空间');
    this.namespaceNameTable.clickResourceNameByRow([name], '名称');
  }

  /**
   * 点击操作栏的更新资源配额按钮
   * @param name 名称
   */
  clickupdatequota(name: string) {
    this.namespaceNameTable.clickOperationButtonByRow(
      [name],
      '更新资源配额',
      'aui-table-row aui-icon',
    );
  }

  /**
   * 获取namespace的状态
   * @param ns_name namespace名称
   */
  getNsStatus(ns_name) {
    return this.namespaceNameTable.getCell('名称', [ns_name]).then(cell => {
      return cell.$('aui-tag .aui-tag__content').getText();
    });
  }
  /**
   * 创建页
   */
  get createPage() {
    return new CreateNamespacePage();
  }
  /**
   * 详情页
   */
  get detailPage() {
    return new DetailNamespacePage();
  }
  /**
   * 更新页
   */
  get updatePage() {
    return new UpdateNamespacePage();
  }
  /**
   * 删除页
   */
  get deletePage() {
    return new DeleteNamespacePage();
  }

  /**
   * 添加命名空间
   * @param testData 创建数据
   */
  addnamespace(testData) {
    this.clickadd();
    this.createPage.fillForm(testData);
    this.getButtonByText('创建').click();
  }
  /**
   * 列表页更新命名空间资源配额
   * @param name 名称
   * @param testData 更新数据
   */
  updatenamespace(name, testData) {
    this.clickupdatequota(name);
    this.updatePage.fillForm(testData);
    this.getButtonByText('更新').click();
  }
  /**
   * 详情页更新命名空间资源配额
   * @param name 名称
   * @param testData 更新数据
   */
  updatenamespacequota_list(name, testData) {
    // this.searchnamespace(name, 1);
    this.namespaceNameTable.clickOperationButtonByRow(
      [name],
      '更新资源配额',
      'alu-menu-trigger button',
    );

    this.updatePage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.waitElementNotPresent(
      $('.aui-dialog-overlay-pane aui-dialog'),
      '等待更新资源配额dialog元素消失',
    );
  }

  /**
   * 详情页更新命名空间资源配额
   * @param name 名称
   * @param testData 更新数据
   */
  updatenamespacequota_detail(name, testData) {
    this.enterDetail(name);
    this.detailPage.clickupdatequota();
    this.updatePage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.waitElementNotPresent(
      $('.aui-dialog-overlay-pane aui-dialog'),
      '等待更新资源配额dialog元素消失',
    );
  }
  /**
   * 详情页更新命名空间容器限额
   * @param name 名称
   * @param testData 更新数据
   */
  updatenamespacelimitrange_detail(name, testData) {
    this.enterDetail(name);
    this.detailPage.clickupdatelimitrange();
    this.updatePage.fillForm(testData);
    this.updatePage.updateDialog.clickConfirm();
  }
  /**
   * 详情页删除命名空间
   * @param name 名称
   */
  deletenamespace(name) {
    this.enterDetail(name);
    this.detailPage.clickdelete();
    this.deletePage.inputname(name);
    this.deletePage.buttonConfirm.click();
  }
  get confirmDialog() {
    return new AuiConfirmDialog($('aui-dialog aui-confirm-dialog'));
  }
  /**
   * 详情页移除命名空间
   * @param name 名称
   */
  removenamespace(name) {
    this.enterDetail(name);
    this.detailPage.clickremove();
    this.confirmDialog.clickConfirm();
  }
  /**
   * 查看命名空间
   * @param name 名称
   */
  viewnamespace(name) {
    this.enterDetail(name);
    this.waitElementPresent(
      element(by.className('aui-card__header')),
      '',
      1000,
    );
  }
  /**
   * 搜索命名空间
   * @param name 名称
   */
  searchnamespace(name, rowCount) {
    browser.driver.wait(
      () => {
        this.search.search(name);
        browser.sleep(2000);
        return this.namespaceNameTable.getRowCount().then(count => {
          return count === rowCount;
        });
      },
      10000,
      `检索后，表格中的数据与期望值(${rowCount})不匹配`,
    );
    return browser.sleep(100);
  }
}
