import { ListPage } from '@e2e/page_objects/platform/project/list.page';
import { NsDetailVerify } from '@e2e/page_verify/platform/namespace/ns.detail';
import { NslistVerify } from '@e2e/page_verify/platform/namespace/ns.list';

import { PlatformPageBase } from '../platform.page.base';

import { CreateNamespacePage } from './create.page';
import { DetailNamespacePage } from './detail.page';
import { ImportNamespacePage } from './import.page';
import { NamespaceListPage } from './list.page';
import { NsPrepare } from './prepare';
import { UpdateNamespacePage } from './update.page';

export class NamespacePage extends PlatformPageBase {
    get nsListPage(): NamespaceListPage {
        return new NamespaceListPage();
    }
    get nsDetailPage(): DetailNamespacePage {
        return new DetailNamespacePage();
    }
    get nsCreatePage(): CreateNamespacePage {
        return new CreateNamespacePage();
    }
    get nsImportPage(): ImportNamespacePage {
        return new ImportNamespacePage();
    }
    get nsUpdatePage(): UpdateNamespacePage {
        return new UpdateNamespacePage();
    }
    get nsListVerifyPage(): NslistVerify {
        return new NslistVerify();
    }
    get nsDetailVerifyPage(): NsDetailVerify {
        return new NsDetailVerify();
    }
    get nsPrepare(): NsPrepare {
        return new NsPrepare();
    }

    get projectlistPage(): ListPage {
        return new ListPage();
    }
}
