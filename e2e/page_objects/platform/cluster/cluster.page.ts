import { AccessClusterPageVerify } from '@e2e/page_verify/platform/cluster/cluster_access.page';
import { ScriptInfoDialogVerify } from '@e2e/page_verify/platform/cluster/script_info_dialog.page';

import { AccessClusterPage } from '../../acp/cluster/cluster_access.page';
import { CreateClusterPage } from './cluster.create.page';
import { ClusterDetailPage } from './cluster.detail.page';
import { ClusterListPage } from './cluster.list.page';
import { UpdateNodePage } from '../../acp/cluster/cluster_updatenode.page';
import { ScriptInfoDialog } from '../../acp/cluster/script_info_dialog.page';
import { DetailClusterPageVerify } from '@e2e/page_verify/platform/cluster/cluster_detail.page';
import { ClusterListPageVerify } from '@e2e/page_verify/platform/cluster/cluster.list.page';
import { AitPageBase } from '@e2e/page_objects/ait/ait.page.base';

export class ClusterPage extends AitPageBase {
  /**
   * 集群接入页面
   */
  get accessPage() {
    return new AccessClusterPage();
  }

  get accessPageVerify() {
    return new AccessClusterPageVerify();
  }

  /**
   * 创建集群页面
   */
  get createPage() {
    return new CreateClusterPage();
  }

  /**
   * 集群详情页面
   */
  get detailPage() {
    return new ClusterDetailPage();
  }

  /**
   * 集群列表页面
   */
  get listPage() {
    return new ClusterListPage();
  }

  /**
   * 集群更新页面
   */
  get UpdateNodePage() {
    return new UpdateNodePage();
  }

  /**
   * 集群列表校验
   */
  get listPageVerify(): ClusterListPageVerify {
    return new ClusterListPageVerify();
  }

  get scriptInfoDialog(): ScriptInfoDialog {
    return new ScriptInfoDialog();
  }

  get scriptInfoDialogVerify(): ScriptInfoDialogVerify {
    return new ScriptInfoDialogVerify();
  }
  get detailPageVerify(): DetailClusterPageVerify {
    return new DetailClusterPageVerify();
  }
}
