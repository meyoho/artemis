import { AccessClusterPage } from '../../acp/cluster/cluster_access.page';
import { CreateClusterPage } from './cluster.create.page';
import { ClusterDetailPage } from './cluster.detail.page';
import { TableComponent } from '@e2e/component/table.component';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { $, element, by } from 'protractor';
import { PlatformPageBase } from '../platform.page.base';

export class ClusterListPage extends PlatformPageBase {
  /**
   * cluster 列表
   */
  get clusterTable() {
    return new TableComponent('.aui-card', '.aui-card__header');
  }
  /**
   * 接入集群页
   */
  get accessPage() {
    return new AccessClusterPage();
  }
  /**
   * 创建集群页
   */
  get createPage() {
    return new CreateClusterPage();
  }
  /**
   *
   * 集群详情页
   */
  get datailPage() {
    return new ClusterDetailPage();
  }
  /**
   * 接入集群
   * @param testData 接入数据
   */
  accessCluster(testData) {
    // browser.driver
    //     .manage()
    //     .window()
    //     .maximize();
    this.accessPage.fillForm(testData);
    this.accessPage.clickConfirm();
  }
  /**
   * 创建集群
   * @param testData 创建数据
   */
  createCluster(testData) {
    // browser.driver
    //     .manage()
    //     .window()
    //     .maximize();
    this.createPage.fillForm(testData);
    this.createPage.clickConfirm();
  }
  /**
   * 进入集群的详情页
   * @param clusterName 集群名称
   */
  enterDetail(name) {
    return this.clusterTable.clickResourceNameByRow([name]);
  }

  search(name, rowCount = 1) {
    this.clusterTable.enterSearch(name, rowCount);
  }
  get noData(): AlaudaLabel {
    return new AlaudaLabel(
      $('acl-k8s-resource-list-footer .empty-placeholder'),
    );
  }
  /**
   * 集群管理列表页，CPU使用率和内存使用率
   * @param clustername 集群名称
   */
  list_monitoring(clustername) {
    const ele_cpu1 = element(
      by.xpath(
        `//a[text()=' ${clustername} ']//..//..//aui-table-cell[5]//div//span[contains(text(), 'CPU')]//..//aui-status-bar`,
      ),
    );
    this.waitElementPresent(ele_cpu1, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`列表页没有 ${clustername} 集群的CPU监控条`);
      }
    });

    const ele_cpu2 = element(
      by.xpath(
        `//a[text()=' ${clustername} ']//..//..//aui-table-cell[5]//div//span[contains(text(), 'CPU')]//..//..//div[contains(text(), '%')]`,
      ),
    );
    this.waitElementPresent(ele_cpu2, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`列表页没有 ${clustername} 集群的CPU监控 核`);
      }
    });

    const ele_mem1 = element(
      by.xpath(
        `//a[text()=' ${clustername} ']//..//..//aui-table-cell[5]//div//span[contains(text(), '内存')]//..//aui-status-bar`,
      ),
    );
    this.waitElementPresent(ele_mem1, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`列表页没有 ${clustername} 集群的内存监控条`);
      }
    });

    const ele_mem2 = element(
      by.xpath(
        `//a[text()=' ${clustername} ']//..//..//aui-table-cell[5]//div[contains(text(), '%')]//span[contains(text(), '内存')]`,
      ),
    );
    this.waitElementPresent(ele_mem2, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`列表页没有 ${clustername} 集群的内存监控  核`);
      }
    });
  }
}
