import { $, by, element, promise, $$, ElementFinder } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AcpPageBase } from '../../acp/acp.page.base';

import { UpdateNodePage } from '../../acp/cluster/cluster_updatenode.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { ClusterListPage } from './cluster.list.page';
import { UpdateRatioPage } from '../../acp/cluster/cluster_updateratio.page';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';

export class ClusterDetailPage extends AcpPageBase {
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'alu-field-set-item',
      '.field-set-item__label label',
    );
  }
  getElementByText(text: string): ElementFinder {
    switch (text) {
      default:
        return this.alaudaElement.getElementByText(
          text,
          '.field-set-item__value',
        );
    }
  }

  /**
   * 返回cluster详情页title: 集群/集群/<name>/
   */
  get title() {
    this.waitElementPresent(
      $('aui-breadcrumb .aui-breadcrumb'),
      'configmap详情页title',
    );
    return $('aui-breadcrumb .aui-breadcrumb').getText();
  }
  /**
   * 返回cluster名称
   */
  get name() {
    this.waitElementPresent($('.aui-card>.aui-card__header'), 'cluster名称');
    return $('.aui-card>.aui-card__header').getText();
  }
  /**
   * 获取node
   */
  get nodeTable() {
    return new TableComponent('alu-cluster-nodes .aui-card', 'aui-search');
  }
  // get clusterTable() {
  //   return new TableComponent('.aui-card', '.aui-card__header');
  // }
  /**
   * 更新主机标签label页
   */
  get updatePage() {
    return new UpdateNodePage();
  }
  /**
   * 点击切换tab页
   * @param tab_name tab页名称如 项目
   */
  clickTab(tab_name: string) {
    element(
      by.xpath(
        '//div[@class="aui-tab-label__content" and contains(text(),"' +
          tab_name +
          '")]',
      ),
    ).click();
  }
  /**
   * 点击操作栏更新主机标签按钮
   * @param row 定位某行
   */
  clickUpdateLabel(row) {
    this.nodeTable.clickOperationButtonByRow(
      [row],
      '更新主机标签',
      '.action-button',
      'aui-menu-item>button',
    );
  }
  /**
   * 点击操作栏停止调度按钮
   * @param row 定位某行
   */
  clickUncordon(row) {
    this.nodeTable.clickOperationButtonByRow(
      [row],
      '停止调度',
      '.action-button',
      'aui-menu-item>button',
    );
  }
  /**
   * 点击操作栏开始调度按钮
   * @param row 定位某行
   */
  clickCordon(row) {
    this.nodeTable.clickOperationButtonByRow(
      [row],
      '开始调度',
      '.action-button',
      'aui-menu-item>button',
    );
  }
  /**
   * 获取详情页的信息
   * @param key 属性的key
   * 如getDetail("创建时间")
   */
  getDetail(key: string): promise.Promise<string> {
    const ele = new AlaudaElement('alu-field-set-item', 'label');
    return ele
      .getElementByText(key, 'alu-field-set-item .field-set-item__value')
      .getText();
  }
  /**
   * 更新node标签label
   * @param row 用于定位某行
   * @param testDdata 更新的数据
   */
  updateNodeLabel(testData, row) {
    this.clickUpdateLabel(row);
    this.updatePage.clickAddButton();
    this.updatePage.tableInput(testData);
    this.updatePage.clickButon();
  }

  /**
   * 点击停止调度/开始调度
   * @param testData 执行操作的按钮
   * @param row 用于定位某行
   * @example updateCordon('停止调度','node')
   */
  updateCordon(row, testData) {
    switch (row) {
      case '停止调度':
        this.clickUncordon(testData);
        this.updatePage.CrondonDialog.clickCancel();
        break;
      case '开始调度':
        this.clickCordon(testData);
        this.updatePage.CrondonDialog.clickConfirm();
        break;
    }
  }

  /**
   * 点击页面按钮
   * @param name 按钮名称
   */
  clickButon(name: string) {
    element(by.xpath('//button/span[contains(text(),"' + name + '")]')).click();
  }

  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header  aui-icon[icon="angle_down"]'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  get listPage() {
    return new ClusterListPage();
  }
  get updateRatioPage() {
    return new UpdateRatioPage();
  }
  updateRatio(regionname, testData) {
    this.listPage.enterDetail(regionname);
    this.action.select('更新超售比');
    this.updateRatioPage.fillForm(testData);
    this.auiDialog.clickConfirm();
  }

  detailPage_operate(operateName) {
    this.getButtonByText('操作').click();
    this.getButtonByText(operateName).click();
  }
  get confirmDialog_name() {
    return new AuiConfirmDialog($('.aui-dialog ng-component'));
  }
  /**
   * 进入主机的详情页
   * @param nodeName 集群名称
   */
  enterNodeDetail(nodeName) {
    return this.nodeTable.clickResourceNameByRow([nodeName]);
  }

  /**
   * 集群详情页，主机列表中CPU使用率和内存使用率
   * @param nodename 主机名称
   */
  nodetable_monitoring(nodename) {
    const ele_cpu1 = element(
      by.xpath(
        `//a[text()=' ${nodename} ']//..//..//aui-table-cell[8]//div//span[contains(text(), 'CPU')]//..//aui-status-bar`,
      ),
    );
    this.waitElementPresent(ele_cpu1, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`主机列表没有 ${nodename} 集群的CPU监控: ${ele_cpu1}`);
      }
    });

    const ele_cpu2 = element(
      by.xpath(
        `//a[text()=' ${nodename} ']//..//..//aui-table-cell[8]//div//span[contains(text(), 'CPU')]//..//..//div[contains(text(), '%')]`,
      ),
    );
    this.waitElementPresent(ele_cpu2, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`主机列表没有 ${nodename} 集群的CPU监控: ${ele_cpu2}`);
      }
    });

    const ele_mem1 = element(
      by.xpath(
        `//a[text()=' ${nodename} ']//..//..//aui-table-cell[8]//div//span[contains(text(), '内存')]//..//aui-status-bar`,
      ),
    );
    this.waitElementPresent(ele_mem1, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`主机列表没有 ${nodename} 集群的内存监控: ${ele_mem1}`);
      }
    });

    const ele_mem2 = element(
      by.xpath(
        `//a[text()=' ${nodename} ']//..//..//aui-table-cell[8]//div[contains(text(), '%')]//span[contains(text(), '内存')]`,
      ),
    );
    this.waitElementPresent(ele_mem2, '').then(isPresent => {
      if (!isPresent) {
        throw new Error(`主机列表没有 ${nodename} 集群的内存监控: ${ele_mem2}`);
      }
    });
  }

  clickCrumb(href: string) {
    element(by.xpath(`//a[@href="${href}"]`)).click();
  }
}
