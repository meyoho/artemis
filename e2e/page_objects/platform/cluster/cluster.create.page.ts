import { AuthInfo } from '@e2e/element_objects/acp/cluster/cluster_authinfo.page';
import { $, ElementFinder, by, element, browser } from 'protractor';

import { AuiSwitch } from '../../../element_objects/alauda.auiSwitch';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AcpPageBase } from '../../acp/acp.page.base';

import { ScriptInfoDialog } from '../../acp/cluster/script_info_dialog.page';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaText } from '@e2e/element_objects/alauda.text';

export class CreateClusterPage extends AcpPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item>div', 'label');
  }
  /**
   * @description 根据左侧文字获得右面元素,
   *              注意：子类如果定位不到元素，需要重写此属性,返回值是右面的元素控件
   *              可以是输入框，选择框，文字，单元按钮等
   * @param text 左侧文字
   * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '集群类型':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
        );
      case '认证信息':
        return new AuthInfo(
          this.alaudaElement.getElementByText(
            text,
            '.aui-form-item__container',
          ),
        );
      case '网络模式':
        return new AlaudaRadioButton(
          this.alaudaElement.getElementByText(text, 'aui-radio-group'),
        );
      case '名称':
      case '显示名称':
      case 'SSH 端口':
      case '用户名':
      case '密码':
        return new AlaudaInputbox(
          this.alaudaElement.getElementByText(text, tagname),
        );
    }
  }
  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   * @example enterValue('描述'，'描述信息')
   */
  enterValue(name: string, value) {
    switch (name) {
      case '集群地址':
        for (const key in value) {
          const data = value[key];
          switch (key) {
            case 'IP 地址/域名':
              const ipinput = new AlaudaInputbox(
                element(by.name('apiserverAddress')),
              );
              ipinput.input(data);
              break;
            case '端口':
              const portinput = new AlaudaInputbox(
                element(by.name('address_port')),
              );
              portinput.input(data);
              break;
          }
        }
        break;
      case 'Cluster CIDR':
        const clustercidrinput = new AlaudaInputbox(
          element(by.name('clusterCIDR')),
        );
        clustercidrinput.input(value);
        break;
      case 'Service CIDR':
        const servicecidrinput = new AlaudaInputbox(
          element(by.name('serviceCIDR')),
        );
        servicecidrinput.input(value);
        break;
      case '控制节点':
        const portinput = new AlaudaInputbox(
          $('alu-array-form-table table .aui-input'),
        );
        portinput.input(value);
        break;
      case 'SSH 端口':
      case '名称':
      case '显示名称':
      case '用户名':
      case '密码':
        this.getElementByText(name).input(value);
        break;
    }
  }

  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
  get scriptInfoDialog(): ScriptInfoDialog {
    return new ScriptInfoDialog();
  }
  /**
   * 用于获取返回文本框内容
   */
  get shellText(): ElementFinder {
    const testarea = $('aui-dialog-content textarea');
    this.waitElementPresent(testarea, '等待5s没有出现文本框');
    return testarea;
  }

  get switchButton(): AuiSwitch {
    return new AuiSwitch($('.aui-form-item__content .aui-switch'));
  }

  clickLeftNav() {
    const temp = element(
      by.xpath(
        '//div[contains(@class,"aui-nav-item-li")]//div[@data-set-depth="1"]//div[@class="aui-platform-nav__label"  and normalize-space(text()) ="集群"]',
      ),
    );
    this.waitElementPresent(temp, '左导航集群没出现');
    temp.click();
  }

  _getToolTip(): ElementFinder {
    return $('alu-error-mapper');
  }

  inputByText(
    text: string,
    value: string,
    errortip = '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
  ) {
    if (value != '') {
      const inputbox: AlaudaInputbox = this.getElementByText(text);
      inputbox.input(value);
    }
    if (errortip != '') {
      browser.sleep(20);
      this._getToolTip()
        .getText()
        .then(text => {
          expect(text).toBe(errortip);
        });
    }
  }
  errorAlert(text: string) {
    return new AlaudaText(by.xpath(`//div[text()='${text}']`));
  }
  get errorAlert_close() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
}
