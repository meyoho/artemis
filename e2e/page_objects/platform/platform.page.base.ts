/**
 * Created by liuwei on 2019/6/13.
 */

import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, $$, browser, by, element } from 'protractor';

import { AlaudaDropdown } from '../../element_objects/alauda.dropdown';
import { PageBase } from '../page.base';

import { ServerConf } from './../../config/serverConf';

export class PlatformPageBase extends PageBase {
  constructor() {
    super();
    browser.baseUrl = `${ServerConf.BASE_URL}/console-platform`;
  }
  /**
   * 页面右上角用户图标的按钮
   */
  get accountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('acl-account-menu'),
      $$('aui-menu-item button'),
    );
  }
  get RegionName(): string {
    return ServerConf.REGIONNAME;
  }
  get projectName(): string {
    return 'uiauto-acp2';
  }
  get acpAccountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('.account-label__content'),
      $$('aui-menu-item button'),
    );
  }

  get asmAccountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('.user-action-account'),
      $$('aui-menu-item button'),
    );
  }

  get devopsAccountMenu(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('.account-label__content'),
      $$('aui-menu-item button'),
    );
  }

  get projects(): AlaudaDropdown {
    return new AlaudaDropdown(
      $('acl-product-select a'),
      $$('aui-menu-item button'),
    );
  }

  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    return `platform-${ServerConf.BRANCH_NAME.replace('_', '-')
      .replace('/', '')
      .toLowerCase()
      .substring(0, 10)}-${prefix}`;
  }
  goToChinese(project: string): any {
    switch (project) {
      case 'acp':
        this.waitElementPresent($('div.project-title'), 'logo 未出现');
        $('aui-breadcrumb-item .aui-breadcrumb__content')
          .getText()
          .then(text => {
            if (text !== '命名空间') {
              this.acpAccountMenu.select('简体中文');
            }
          });
        break;
      case 'asm':
        this.waitElementPresent($('alo-logo span'), 'logo 未出现');
        $('alo-breadcrumb span')
          .getText()
          .then(text => {
            if (text !== '项目') {
              this.asmAccountMenu.select('简体中文');
            }
          });
        break;
      case 'platform':
        this.waitElementPresent($('alu-logo span'), 'logo 未出现');
        $('alu-logo span')
          .getText()
          .then(text => {
            if (text !== '项目管理') {
              this.accountMenu.select('中文');
            }
          });
        break;
      case 'devops':
        this.waitElementPresent($('alo-logo span'), 'logo 未出现');
        $('alo-breadcrumb span')
          .getText()
          .then(text => {
            if (text !== '项目') {
              this.devopsAccountMenu.select('简体中文');
            }
          });
        break;
    }
  }
  /**
   * 从console-acp进入项目管理页面
   */
  enterManageProjectFromAcp() {
    this.switchSetting('项目管理');
  }
  /**
   * 进入项目详情页
   * @param pro_name 项目名称
   */
  toProjectDetail(pro_name: string = this.projectName) {
    this.waitElementPresent($('alu-project-list'), '验证项目列表是否出现');
    const pro_table = new AuiTableComponent(
      $('alu-project-list'),
      'aui-table',
      'alu-search-group-wrapper',
    );
    browser.sleep(1).then(() => {
      console.log('search project: ' + pro_name);
    });
    pro_table.searchByResourceName(pro_name, 1);
    browser.sleep(1).then(() => {
      console.log('to project detail: ' + pro_name);
    });
    element(by.linkText(pro_name)).click();
  }
  /**
   * 进入平台中心页面
   */
  enterPlatformView(name) {
    const platformname = new AlaudaDropdown(
      $('acl-platform-center-nav .menu'),
      $$('aui-menu-item button span'),
    );
    platformname.select(name);
  }
}
