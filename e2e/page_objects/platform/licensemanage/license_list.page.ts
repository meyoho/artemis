import { PlatformPageBase } from '../platform.page.base';
import { $, element, by } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AuiConfirmDialog } from '@e2e/element_objects/alauda.aui_confirm_dialog';
import { LicenseManageImportPage } from './license_import.page';
import { AlaudaAuiBreadCrumb } from '@e2e/element_objects/alauda.auiBreadcrumb';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { ServerConf } from '@e2e/config/serverConf';
import { LoginPage } from '@e2e/page_objects/login.page';

export class LicenseManageListPage extends PlatformPageBase {
  /**
   * 面包屑
   */
  get breadcrumb(): AlaudaAuiBreadCrumb {
    return new AlaudaAuiBreadCrumb($('aui-breadcrumb'), 'aui-breadcrumb-item');
  }
  toPlatformManagePage() {
    const url = `${ServerConf.BASE_URL}/console-platform/manage-platform/cluster/list`;
    const loginpage = new LoginPage();
    loginpage.waitEnterUserView(
      url,
      '/console-platform/manage-platform/cluster/list',
    );
    return this.waitElementPresent(
      element(by.xpath("//button/span[contains(text(),'创建集群')]")),
      '切换平台管理视图失败',
      30000,
    ).then(isPresent => {
      if (!isPresent) {
        throw new Error('切换平台管理视图失败, 测试无法继续');
      }
    });
  }
  waitPagePresent(timeout = 10000) {
    this.waitElementPresent($('aui-breadcrumb'), '等待页面加载完成');
    this.waitElementPresent(
      element(by.xpath("//button/span[contains(text(),'导入许可证')]")),
      'license管理页面切换失败',
      timeout,
    );
  }
  get importLicenseButton() {
    return new AlaudaButton(
      element(by.xpath('//button/span[contains(text(),"导入许可证")]')),
    );
  }
  get alarmInfo() {
    return this.waitElementPresent(
      $('aui-inline-alert>.aui-inline-alert'),
      '等待license管理页面帮助信息出现',
    ).then(ispresent => {
      if (ispresent) {
        return $('aui-inline-alert>.aui-inline-alert').getText();
      } else {
        throw new Error('license管理页面帮助信息未出现');
      }
    });
  }
  get licenseImportPage() {
    return new LicenseManageImportPage();
  }
  get confirmDiaglog() {
    return new AuiConfirmDialog($('aui-confirm-dialog'));
  }
  get licenseTable() {
    return new AuiTableComponent(
      $('aui-inline-alert+aui-card'),
      'aui-table',
      '',
      'aui-table-cell',
      'aui-table-row',
      'aui-table-header-cell',
    );
  }
  clickLicenseOperation(keys: Array<string>, opt: string) {
    return this.licenseTable.clickOperationButtonByRow(
      keys,
      opt,
      '.alu-menu-trigger',
      'aui-menu-item>button',
    );
  }
  deleteLicense(keys: Array<string>, flag = '确定') {
    return this.clickLicenseOperation(keys, '删除').then(() => {
      if (flag === '确定') {
        this.confirmDiaglog.clickConfirm();
      } else if (flag === '取消') {
        this.confirmDiaglog.clickCancel();
      }
    });
  }
  importLicense(license_content, flag = '确定') {
    this.clickLeftNavByText('许可证管理');
    return this.importLicenseButton.click().then(() => {
      this.licenseImportPage.inputLicense(license_content);
      if (flag === '确定') {
        this.licenseImportPage.clickConfirm();
      } else if (flag === '取消') {
        this.licenseImportPage.clickCancel();
      } else if (flag === '关闭') {
        this.licenseImportPage.clickClose();
      }
    });
  }
}
