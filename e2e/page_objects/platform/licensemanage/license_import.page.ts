import { PlatformPageBase } from '../platform.page.base';
import { $, by } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class LicenseManageImportPage extends PlatformPageBase {
  private _root = new AlaudaElement(
    'aui-form-item',
    '.aui-form-item__label',
    $('aui-dialog'),
  );
  get header() {
    return this._root.root.$('.aui-dialog__header-title').getText();
  }
  get isPresent() {
    return $('aui-dialog').isPresent();
  }
  clickConfirm() {
    return new AlaudaButton(
      this._root.root.element(
        by.xpath('//aui-dialog-footer//button/span[contains(text(),"导入")]'),
      ),
    ).click();
  }
  clickCancel() {
    return new AlaudaButton(this._root.root.$('button+button')).click();
  }
  clickClose() {
    return new AlaudaButton(
      this._root.root.$('.aui-dialog__header-close'),
    ).click();
  }
  get platformInfo() {
    return this._root.getElementByText('平台信息', 'input').getText();
  }
  inputLicense(license: string) {
    return new AlaudaInputbox(
      this._root.getElementByText('许可证', 'textarea'),
    ).input(license);
  }
}
