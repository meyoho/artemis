import { PlatformPageBase } from '../platform.page.base';
import { CommonMethod } from '@e2e/utility/common.method';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';

export class LicenseManagePreparePage extends PlatformPageBase {
  createLicense(products = 'AutoTest', days = null) {
    console.log(process.platform);
    if (process.platform != 'darwin' && process.platform != 'win32') {
      const cwd = process.cwd();
      const token = CommonMethod.loginGetToken();
      const cmd = `curl ${
        ServerConf.PROXY ? `-x ${ServerConf.PROXY}` : ''
      } -H 'authorization:Bearer ${token}' -k ${
        ServerConf.BASE_URL
      }/lic/v1/metadata`;
      process.chdir(`${cwd}/scripts`);
      console.log(cmd);
      const rsp: string = CommonMethod.execCommand(cmd);
      console.log(rsp);
      const platform_info = JSON.parse(rsp)['cfc'];
      const license = CommonMethod.execCommand(
        `curl http://10.0.128.217:8000/license -X POST -H "Content-Type: application/json" -d '{"platform_info":"${platform_info}", ${
          days ? `"days": ${days}, ` : ''
        }"products":["${products}"]}'`,
      );
      const fs = require('fs');
      fs.writeFileSync(ServerConf.TESTDATAPATH + '/' + 'valid.lic', license);
      process.chdir(cwd);
    }
  }
  createLabels(keys_values: Array<Record<string, string>>) {
    const labels = {};
    keys_values.forEach(key_value => {
      labels[key_value.key] = key_value.value;
    });
    return labels;
  }
  fileExists(filePath) {
    const fs = require('fs');
    return fs.existsSync(filePath) && fs.statSync(filePath).isFile();
  }
  getLicenseContent(filePath) {
    if (this.fileExists(filePath)) {
      return CommonMethod.execCommand(
        `openssl x509 -outform der -in ${filePath}|base64 -w0|sed 's/[\\\\&*./+!]/\\\\&/g'`,
      );
    } else {
      console.log(`License 文件 '不存在'`);
      return '';
    }
  }
  getLicenseSN(filePath) {
    if (this.fileExists(filePath)) {
      return CommonMethod.execCommand(
        `openssl x509 -noout -serial -in ${filePath}| cut -d"=" -f2|tr "[:upper:]" "[:lower:]"`,
      );
    } else {
      console.log(`License 文件 '不存在'`);
      return '';
    }
  }
  deleteLicense(filePath) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete secret -n kube-public license-${this.getLicenseSN(
        filePath,
      )}`,
      CommonKubectl.globalClusterName,
    );
  }
  importLicense(filePath) {
    const license_content: string = this.getLicenseContent(filePath);
    const data = {
      name: `license-${this.getLicenseSN(filePath)}`,
      namespace: 'kube-public',
      annotations: this.createLabels([
        {
          key: `license.${ServerConf.LABELBASEDOMAIN}/serial-number`,
          value: this.getLicenseSN(filePath),
        },
      ]),
      labels: this.createLabels([
        {
          key: `${ServerConf.LABELBASEDOMAIN}/license`,
          value: '',
        },
      ]),
      datas: {
        license: license_content,
      },
      base64: false,
    };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      data,
      `license-${this.getLicenseSN(filePath)}.yaml`,
    );
  }
}
