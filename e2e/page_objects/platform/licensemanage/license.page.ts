import { LicenseManageListPage } from './license_list.page';
import { LicenseManageImportPage } from './license_import.page';
import { LicenseManagePreparePage } from './prepare.page';
import { LicenseListVerifyPage } from '@e2e/page_verify/platform/licensemanage/licenselist.verify.page';
import { PlatformPageBase } from '../platform.page.base';

export class LicensePage extends PlatformPageBase {
  get licenseListPage() {
    return new LicenseManageListPage();
  }
  get licenseImportPage() {
    return new LicenseManageImportPage();
  }
  get licensePreparePage() {
    return new LicenseManagePreparePage();
  }
  get licenseVeryfyPage() {
    return new LicenseListVerifyPage();
  }
}
