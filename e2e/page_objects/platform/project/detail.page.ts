import { ElementFinder } from 'protractor';
import { $, $$ } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { PlatformPageBase } from '../platform.page.base';

export class DetailPage extends PlatformPageBase {
  /**
   * 资源详情页的‘基本信息’部分
   */
  get alaudaElement() {
    return new AlaudaElement(
      'div alu-field-set-item',
      'label',
      $('alu-field-set-group'),
    );
  }
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  getElement(
    parent: string,
    children: string,
    root = $('html body'),
  ): AlaudaElement {
    return new AlaudaElement(parent, children, root);
  }

  /**
   * 页面上根据父节点，查找子节点
   * @param parent 父节点
   * @param left_label_name 左侧label名称
   */
  getElementByElementFinder(parent: ElementFinder, left_label_name) {
    const temp = parent.$$('alu-quota > div');
    this.waitElementPresent(temp.first(), `${temp.locator()} not found`);
    const parentElement = parent
      .$$('alu-quota > div')
      .filter(element => {
        return element
          .$('.quota__title')
          .getText()
          .then(text => {
            return text.trim() === left_label_name;
          });
      })
      .first();
    return [
      parentElement.$('.quota__chart'),
      parentElement.$('.quota__content'),
    ];
  }

  /**
   * 项目详情页，配额
   * 根据title获取配额使用百分比，配置使用额度,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getQuotaElementByText(cluster: string, text: string): Array<ElementFinder> {
    const grandParentElement = this.getElement(
      'aui-card .hasDivider',
      '.aui-card__header',
      $('.clusters-container'),
    );
    const parentElement = grandParentElement.getElementByText(
      cluster,
      '.aui-card__content',
    );
    this.waitElementPresent(parentElement, `${parentElement.locator()} 没出现`);
    return this.getElementByElementFinder(parentElement, text);
  }

  /**
   * 更新基本信息
   */
  updataDetail(text: string, tagname = 'input') {
    const parent_element = this.getElement('aui-form-item', 'label');
    switch (text) {
      case '名称':
        return parent_element.getElementByText(text, 'span');

      case '描述':
        return parent_element.getElementByText(text, 'textarea');

      default:
        return parent_element.getElementByText(text, tagname);
    }
  }

  /**
   * 更新基本信息
   */
  addCluster(text: string): ElementFinder {
    return this.getElement('aui-form-item', 'label').getElementByText(
      text,
      'input',
    );
  }

  entryUpdateValue(text, value): any {
    this.updataDetail(text).clear();
    this.updataDetail(text).sendKeys(value);
  }

  entryAddClusterValue(text, value): any {
    switch (text) {
      case '集群':
        const cluster = new AlaudaDropdown(
          $('input.aui-select__input'),
          $$('.cluster-select-option>span:nth-child(1)'),
        );
        cluster.select(value);
        break;
      default:
        this.addCluster(text).sendKeys(value);
    }
  }

  /**
   * 填写表单
   * @param data 测试数据
   */
  fillUpdateForm(data, opertion) {
    for (const key in data) {
      switch (opertion) {
        case '添加集群':
          this.entryAddClusterValue(key, data[key]);
          break;
        case '更新基本信息':
          this.entryUpdateValue(key, data[key]);
          break;
        default:
          throw Error('操作不识别，请检查');
      }
    }
  }
}
