import { $, browser } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { PlatformPageBase } from '../platform.page.base';

export class ListPage extends PlatformPageBase {
  private timeout_enterProject: number;
  constructor() {
    super();
    this.timeout_enterProject = 0;
  }
  get projectsTable() {
    return new TableComponent();
  }

  get noResult() {
    return $('alu-project-list alu-no-data');
  }
  searchContext() {
    return new AuiSearch(
      $('aui-search'),
      'input',
      '.aui-search__clear',
      '.aui-search__button',
    );
  }

  searchProject(project_name: string) {
    this.searchContext().search(project_name);
  }
  /**
   * 进入项目详情页面
   * @param project_name 项目名称
   */
  enterProject(project_name) {
    this.searchProject(project_name);
    browser.sleep(1000);
    this.waitElementPresent(
      this.projectsTable.getRow([project_name]),
      `没有检索到项目${project_name}`,
      10000,
    );
    this.projectsTable.clickResourceNameByRow([project_name]);
    const temp = $('acl-disabled-container button');
    temp.isPresent().then(isPresent => {
      if (isPresent) {
        this.timeout_enterProject++;
        if (this.timeout_enterProject < 30) {
          this.enterProject(project_name);
        } else {
          throw new Error(`项目${project_name}详情页进入失败`);
        }
      }
    });
    return browser.sleep(1000);
  }
}
