/**
 * Created by zhangjiao on 2018/7/5.
 */
import { $, $$, ElementFinder } from 'protractor';

import { AlaudaElement } from '../../../element_objects/alauda.element';
import { PlatformPageBase } from '../platform.page.base';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';

export class CreatePage extends PlatformPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('aui-form-item', '.aui-form-item__label');
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  entryValue(text: string, value: string): any {
    switch (text) {
      case '所属集群':
        const cluster = new AlaudaDropdown(
          $('.aui-input.aui-multi-select.aui-multi-select--medium'),
          $$('aui-option .cluster-select-option>span:nth-child(1)'),
        );
        cluster.select(value);
        break;
      case '描述':
        this.getElementByText(text, 'textarea').sendKeys(value);
        break;
      default:
        this.getElementByText(text).sendKeys(value);
    }
  }

  /**
   * 填写表单
   * @param data 测试数据
   */
  fillForm(data) {
    for (const key in data) {
      this.entryValue(key, data[key]);
    }
  }

  /**
   * 打开设置配额弹框
   */
  openQuotaConfig(): void {
    const quotaflag: ElementFinder = $(
      'aui-icon.aui-accordion-item__header-toogle--expanded',
    );
    quotaflag.isPresent().then(flag => {
      if (!flag) {
        $('aui-icon.aui-accordion-item__header-toogle').click();
      }
    });
  }
}
