import { $ } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { PlatformPageBase } from '../platform.page.base';

export class NamespaceListPage extends PlatformPageBase {
    get namespacesTable() {
        return new TableComponent();
    }

    get noResult() {
        return $('alu-project-list alu-no-data');
    }
    searchContext() {
        return new AuiSearch(
            $('aui-search'),
            'input',
            '.aui-search__clear',
            '.aui-search__button'
        );
    }

    searchNamespace(namespace_name: string) {
        this.searchContext().search(namespace_name);
    }
}
