import { CreatePageVerify } from '@e2e/page_verify/platform/project/create.page';
import { DetailPageVerify } from '@e2e/page_verify/platform/project/detail.page';
import { ListPageVerify } from '@e2e/page_verify/platform/project/list.page';
import { $, browser, by, element } from 'protractor';

import { AlaudaToast } from '../../../element_objects/alauda.toast';
import { PlatformPageBase } from '../platform.page.base';

import { AlaudaAuiCheckbox } from './../../../element_objects/alauda.auicheckbox';
import { AlaudaConfirmDialog } from './../../../element_objects/alauda.confirmdialog';
import { CreatePage } from './create.page';
import { DetailPage } from './detail.page';
import { ListPage } from './list.page';
import { NamespaceCreatePage } from './namespace.create.page';
import { NamespaceListPage } from './namespace.list.page';
import { NamespaceMemberListPage } from './namespace.member.list.page';
import { PreparePage } from './prepare.page';
import { ProjectMemberListPage } from './project.member.list.page';
import { UpdataQuptaPage } from './update.quota.page';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';

export class ProjectPage extends PlatformPageBase {
  get createPage() {
    return new CreatePage();
  }
  get createPageVerify(): CreatePageVerify {
    return new CreatePageVerify();
  }
  get listPage() {
    return new ListPage();
  }

  get listPageVerify(): ListPageVerify {
    return new ListPageVerify();
  }

  get preparePage(): PreparePage {
    return new PreparePage();
  }

  get detailPage() {
    return new DetailPage();
  }

  get detailPageVerify(): DetailPageVerify {
    return new DetailPageVerify();
  }

  get updateQuotaPage() {
    return new UpdataQuptaPage();
  }

  get projectMemberPage() {
    return new ProjectMemberListPage();
  }

  get namespaceCreatePage() {
    return new NamespaceCreatePage();
  }

  get namespaceMemberPage() {
    return new NamespaceMemberListPage();
  }

  get namespacelist() {
    return new NamespaceListPage();
  }

  createPlatformProject(detaildata: object, quotadata: object): any {
    this.getButtonByText('创建项目').click();

    this.createPage.fillForm(detaildata);
    this.getButtonByText('下一步').click();
    this.createPage.fillForm(quotadata);
    this.getButtonByText('创建项目').click();
    return new AlaudaToast().getMessage();
  }

  updateDetail(data): any {
    this.clickLeftNavByText('项目详情');
    this.getButtonByText('操作').click();
    this.getButtonByText('更新基本信息').click();
    this.detailPage.fillUpdateForm(data, '更新基本信息');
    this.getButtonByText('更新').click();
    return new AlaudaToast().getMessage();
  }

  addCluster(data): any {
    this.clickLeftNavByText('项目详情');
    this.getButtonByText('操作').click();
    this.getButtonByText('添加集群').click();
    this.detailPage.fillUpdateForm(data, '添加集群');
    this.getButtonByText('添加').click();
    return new AlaudaToast().getMessage();
  }

  removeCluster(cluster: string): any {
    this.clickLeftNavByText('项目详情');
    this.getButtonByText('操作').click();
    this.getButtonByText('移除集群').click();
    $('input.ng-valid').sendKeys(cluster);
    this.getButtonByText('移除').click();
    return new AlaudaToast().getMessage();
  }

  /**
   * 判断集群不存在
   * @param
   */
  checkClusterNotExist(cluster: string) {
    return element(
      by.xpath(
        '//*[@class="aui-card__header" and contains(text(), "' +
          cluster +
          '")]',
      ),
    ).isPresent();
  }

  /**
   * 删除项目
   */
  deleteProject(project: string) {
    this.clickLeftNavByText('项目详情');
    this.getButtonByText('操作').click();
    this.getButtonByText('删除项目').click();
    $('input.ng-valid').sendKeys(project);
    this.getButtonByText('移除').click();
    return new AlaudaToast().getMessage();
  }

  /**
   * 更新配额
   */
  updateQuota(data) {
    this.clickLeftNavByText('项目详情');
    this.getButtonByText('操作').click();
    this.getButtonByText('更新配额').click();
    this.updateQuotaPage.fillForm(data);
    this.getButtonByText('更新').click();
    return new AlaudaToast().getMessage();
  }

  /**
   * 项目导入成员
   */
  importProjectMember(username: string, rolename: string) {
    this.clickLeftNavByText('项目成员');
    this.getButtonByText('导入成员').click();
    this.projectMemberPage.searchImportUser(username);
    browser.sleep(500);

    const table = new AlaudaAuiTable($('aui-dialog-content aui-table'));
    new AlaudaAuiCheckbox(table.getRow([username]).$('aui-checkbox')).check();
    this.projectMemberPage.selectRole(rolename);
    this.getButtonByText('导入(1)').click();
    return new AlaudaToast().getMessage();
  }

  /**
   * 项目删除成员
   */
  deleteProjectMember(username: string) {
    this.clickLeftNavByText('项目成员');
    this.projectMemberPage
      .memberList()
      .clickOperationButtonByRow(
        [username],
        '移除',
        'alu-menu-trigger aui-icon',
      );
    browser.sleep(500);
    new AlaudaConfirmDialog(by.css('aui-confirm-dialog')).clickConfirm();
    return new AlaudaToast().getMessage();
  }

  /**
   * 创建namespace
   */
  createPlatformNamespace(data: object) {
    this.clickLeftNavByText('命名空间');
    browser.sleep(500);
    this.getButtonByText('创建命名空间').click();
    this.namespaceCreatePage.fillForm(data);
    this.getButtonByText('创建').click();
    browser.sleep(50);
    return new AlaudaToast().getMessage();
  }

  /**
   * 命名空间导入成员
   */
  importNamespaceMember(username: string, rolename: string, namespace: string) {
    this.clickLeftNavByText('命名空间');
    this.namespacelist.namespacesTable.clickResourceNameByRow([namespace]);
    $('.aui-tab-header__labels > div:nth-child(2)').click();
    this.getButtonByText('导入成员').click();
    this.namespaceMemberPage.searchImportUser(username);
    browser.sleep(500);
    const table = new AlaudaAuiTable($('aui-dialog-content aui-table'));

    new AlaudaAuiCheckbox(table.getRow([username]).$('aui-checkbox')).check();

    this.namespaceMemberPage.selectRole(rolename);
    this.getButtonByText('导入(1)').click();
    return new AlaudaToast().getMessage();
  }

  /**
   * 命名空间删除成员
   */
  deleteNamespaceMember(username: string, namespace: string) {
    this.clickLeftNavByText('命名空间');
    this.namespacelist.namespacesTable.clickResourceNameByRow([namespace]);
    $('.aui-tab-header__labels > div:nth-child(2)').click();
    this.namespaceMemberPage
      .memberList()
      .clickOperationButtonByRow(
        [username],
        '移除',
        'alu-menu-trigger aui-icon',
      );
    browser.sleep(500);
    new AlaudaConfirmDialog(by.css('aui-confirm-dialog')).clickConfirm();
    return new AlaudaToast().getMessage();
  }
}
