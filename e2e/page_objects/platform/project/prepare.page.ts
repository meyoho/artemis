import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { ProjectPage } from '@e2e/page_objects/devops/project/project.page';

export class PreparePage extends AcpPageBase {
  deleteProject(projectName) {
    const page = new ProjectPage();
    page.deleteProject(projectName);
  }

  deleteNamespace(namespaceName, clusterName) {
    const page = new ProjectPage();
    page.deleteNamespace(namespaceName, clusterName);
  }
}
