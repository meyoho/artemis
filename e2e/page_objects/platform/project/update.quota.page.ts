import { AlaudaElement } from '../../../element_objects/alauda.element';

import { CreatePage } from './create.page';

export class UpdataQuptaPage extends CreatePage {
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-form-item',
            'aui-form-item label[class*=aui-form-item__label]'
        );
    }

    getElementByText(text: string, tagName: string = 'input') {
        return this.alaudaElement.getElementByText(text, tagName);
    }
    entryValue(text, value) {
        this.getElementByText(text).sendKeys(value);
    }
    /**
     * 填写表单
     * @param data 测试数据
     */
    fillForm(data) {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                this.enterValue(key, data[key]);
            }
        }
    }
}
