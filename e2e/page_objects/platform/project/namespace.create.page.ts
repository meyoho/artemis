/**
 * Created by zhangjiao on 2018/7/5.
 */
import { $, $$, ElementArrayFinder, ElementFinder } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { PlatformPageBase } from '../platform.page.base';

export class NamespaceCreatePage extends PlatformPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('section', 'h3');
  }

  get quotaElement() {
    return new AlaudaElement(
      'alu-quota-base-form aui-form-item',
      'div.aui-form-item__label-wrapper',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    return this.alaudaElement.getElementByText(text, tagname);
  }

  getQuotaElmenetByTest(text: string, tagname = 'input'): ElementFinder {
    return this.quotaElement.getElementByText(text, tagname);
  }

  entryValue(text: string, value: string) {
    switch (text) {
      case '所属集群':
        const cluster = new AlaudaDropdown(
          $('.aui-select__input.aui-input.aui-input--medium'),
          $$('aui-option'),
        );
        cluster.select(`${value}(${this.clusterDisplayName(value)})`);
        break;
      case '命名空间':
        this.waitElementPresent(
          $('[formcontrolname="_name"]'),
          '命名空间 创建命名空间 页面 命名空间名称 控件没找到',
        );
        $('[formcontrolname="_name"]').sendKeys(value);
        break;
      case '容器限额':
        const arrayelemnet: ElementArrayFinder = this.getElementByText(
          text,
          'alu-limit-range-base-form',
        ).$$('input');
        arrayelemnet.each(ele => {
          ele.sendKeys(value);
        });
        break;
      default:
        this.getQuotaElmenetByTest(text).sendKeys(value);
    }
  }

  /**
   * 填写表单
   * @param data 测试数据
   */
  fillForm(data) {
    for (const key in data) {
      this.entryValue(key, data[key]);
    }
  }

  /**
   * 打开设置配额弹框
   */
  openQuotaConfig(): void {
    const quotaflag: ElementFinder = $(
      'aui-icon.aui-accordion-item__header-toogle--expanded',
    );
    quotaflag.isPresent().then(flag => {
      if (!flag) {
        $('aui-icon.aui-accordion-item__header-toogle').click();
      }
    });
  }
}
