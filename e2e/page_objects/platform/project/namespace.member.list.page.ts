import { $, $$ } from 'protractor';

import { TableComponent } from '../../../component/table.component';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { PlatformPageBase } from '../platform.page.base';

export class NamespaceMemberListPage extends PlatformPageBase {
  importMemberList() {
    return new TableComponent('aui-dialog-content');
  }

  memberList() {
    return new TableComponent('alu-scope-member-list');
  }

  get noResult() {
    return $('.alu-project-list.empty-placeholder.ng-star-inserted');
  }
  searchContext() {
    return new AuiSearch(
      $('.aui-dialog__header-title > aui-search'),
      'input',
      '.aui-search__clear',
      '.aui-search__button',
    );
  }

  searchImportUser(username: string) {
    this.searchContext().search(username);
  }

  selectRole(role_name: string) {
    return new AlaudaDropdown(
      $('aui-select[name="role"] > .aui-select'),
      $$('aui-option'),
    ).select(role_name);
  }

  getUserByRowIndex(index: number) {
    return $$('alu-scope-member-list  .aui-table__column-username')
      .get(index)
      .getText();
  }

  getUserRoleByRowIndex(index: number) {
    return $$('alu-scope-member-list  .cdk-column-user_role')
      .get(index)
      .getText();
  }

  getUserCount() {
    return $$('alu-scope-member-list  .aui-table__column-username').count();
  }
}
