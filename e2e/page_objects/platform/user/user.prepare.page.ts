import { AcpPreparePage } from '@e2e/page_objects/acp/prepare.page';

import { LdapPage } from './ldap.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

export class UserPreparePage extends AcpPreparePage {
  get ldapPage(): LdapPage {
    return new LdapPage();
  }

  /**
   * 创建一个ladap 用户
   * @param email 邮箱， 使用此邮箱可以登陆平台，密码是: password
   * @param lastName 名
   * @returns `${givenName} ${lastName}` , 返回值可以传给deleteLadapUser 函数，删除ldap 用户
   */
  createLdapUser(email, lastName) {
    return this.ldapPage.create(email, lastName);
  }

  /**
   * ladp 中删除一个用户
   * @param name ， createLdapUser 函数的返回值
   */
  deleteLdapUser(name) {
    this.ldapPage.delete(name);
  }

  /**
   * 删除用户
   * @param email 用户
   */
  deleteUser(email) {
    const user = CommonKubectl.execKubectlCommand(
      `kubectl get user -o=custom-columns=name:.metadata.name,email:.spec.email | grep ${email} | awk '{print $1}'`,
    ).trim();
    if (user !== '') {
      CommonKubectl.execKubectlCommand(`kubectl delete user ${user}`);
    }
  }
}
