import { AlaudaAuiCheckbox } from '@e2e/element_objects/alauda.auicheckbox';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { $ } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

export class UserClearUpDialog extends PlatformPageBase {
  get table() {
    return new AlaudaAuiTable($('aui-dialog-content aui-table'));
  }

  /**
   * 清理无效用户
   * @param userList 要清理用户列表, [‘qa@alauda.io’, 'qa1@alauda.io']
   */
  clear(userList) {
    userList.forEach(user => {
      const checkBox = new AlaudaAuiCheckbox(
        this.table.getRow([user]).$('aui-checkbox'),
      );
      checkBox.check();
    });
    this.getButtonByText('清理(1)').click();
  }
}
