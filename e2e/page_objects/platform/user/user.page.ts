import { UserListPageVerify } from '@e2e/page_verify/platform/user/user.list.page';
import { $, $$, browser } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';

import { PlatformPageBase } from './../platform.page.base';
import { UserDetailPage } from './user.detail.page';
import { UserListPage } from './user.list.page';
import { UserPreparePage } from './user.prepare.page';
export class UserPage extends PlatformPageBase {
  get preparePage(): UserPreparePage {
    return new UserPreparePage();
  }

  get listPage() {
    return new UserListPage();
  }

  get listPageVerify(): UserListPageVerify {
    return new UserListPageVerify();
  }
  get detaiPage() {
    return new UserDetailPage();
  }

  ldapUserLogin(username: string, password: string) {
    //a[href*="ldap"]
    new AlaudaButton($$('a[href*="ldap"]').first()).click();
    new AlaudaInputbox($('#login')).input(username);
    new AlaudaInputbox($('#password')).input(password);
    new AlaudaButton($('form button')).click();
  }

  async keycloakUserLogin(username: string, password: string) {
    await this.getButtonByText('OIDC 登录').click();
    await this.waitElementPresent($('#username'), '没有跳转到keycloak登录页面');
    await $('#username').sendKeys(username);
    await $('#password').sendKeys(password);
    await $('#kc-login').click();
  }

  async localUserLogin(username: string, password: string) {
    await new AlaudaButton($('a.dex-subtle-text')).click();
    await new AlaudaInputbox($('#login')).input(username);
    await new AlaudaInputbox($('#password')).input(password);
    await new AlaudaButton($('.dex-btn')).click();
  }

  async verifyHomePageLoginUsername(product: string) {
    switch (product) {
      case 'Container Platform':
        this.waitElementPresent(
          $('.account-label__content span'),
          '右上角用户名称未出现',
        );
        return await $('.account-label__content span').getText();
      case 'Service Mesh':
        this.waitElementPresent(
          $('.user-action-account span'),
          '右上角用户名称未出现',
        );
        return await $('.user-action-account span').getText();
      case 'platform':
        this.waitElementPresent(
          $('.account-menu__display'),
          '右上角用户名称未出现',
        );
        return await $('.account-menu__display').getText();
      case 'DevOps':
        this.waitElementPresent(
          $('.user-action-account span'),
          '右上角用户名称未出现',
        );
        return await $('.user-action-account span').getText();
      default:
        console.error('Product Name Uncertainty!!!!!!');
    }
  }

  logout() {
    this.acpAccountMenu.select('退出登录');
    $('.aui-confirm-dialog .aui-button--primary').click();
    this.waitElementPresent(
      $('a.dex-subtle-text'),
      '退出登录失败, 登录页未加载完成',
    );
    return browser.getCurrentUrl();
  }

  searchUser(username: string) {
    this.clickLeftNavByText('用户管理');
    this.listPage.searchUser(username);
  }

  addRole(role: string) {
    this.getButtonByText('添加角色').click();
    $('.add-button').click();
    new AlaudaDropdown(
      $('.aui-dialog aui-input-group input'),
      $$('.aui-option'),
    ).select(role);
    $('aui-dialog-footer .aui-button--primary').click(); // 点击添加
  }
  deleteRole(role: string) {
    this.detaiPage.rolesTable
      .getRow([role])
      .$('button')
      .click();
    browser.sleep(500);
    new AlaudaButton($('aui-confirm-dialog button[class*="primary"]')).click();
  }
}
