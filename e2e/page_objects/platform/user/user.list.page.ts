import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { $ } from 'protractor';
import { AuiSearch } from '../../../element_objects/alauda.auiSearch';
import { PlatformPageBase } from '../platform.page.base';
import { UserClearUpDialog } from './user.clearup.dialog';
import { TableComponent } from '@e2e/component/table.component';

export class UserListPage extends PlatformPageBase {
  get usersTable() {
    return new TableComponent('aui-card .aui-card', '.aui-card__header');
  }

  get noResult() {
    return $('alu-resource-list-footer .empty-placeholder');
  }

  get _searAuiSelect(): AuiSelect {
    return new AuiSelect($('alu-search-group-wrapper aui-select'));
  }

  get _searchBox(): AuiSearch {
    return new AuiSearch(
      $('aui-search'),
      'input',
      '.aui-search__clear',
      '.aui-search__button',
    );
  }

  get clearUpDialog(): UserClearUpDialog {
    return new UserClearUpDialog();
  }

  /**
   * 检索用户
   * @param username 用户名
   * @param type 类型：用户名，显示名称，用户组
   */
  searchUser(username: string, count = 1, type = '用户名') {
    if (type !== '用户名') {
      this._searAuiSelect.select(type);
    }
    this.usersTable.search(username, count);
  }

  /**
   * 同步用户
   */
  syncUser() {
    this.getButtonByText('同步用户').click();
    this.getButtonByText('确定').click();
    this.waitElementNotPresent(
      $('aui-dialog .loading-container'),
      '【正在同步，请稍等...】提示没有消失',
    );
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 清理无效用户
   * @param userList 要清理用户列表, [‘qa@alauda.io’, 'qa1@alauda.io']
   */
  clear(userList) {
    this.getButtonByText('清理无效用户').click();
    this.clearUpDialog.clear(userList);
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  /**
   * 删除用户
   * @param userList 要清理用户列表, [‘qa@alauda.io’, 'qa1@alauda.io']
   */
  deleteUser(username) {
    this.searchUser(username);
    this.usersTable.clickOperationButtonByRow(
      [username],
      '删除',
      'button aui-icon',
    );
    this.getButtonByText('确定').click();
  }

  /**
   * 进入用户的详情页
   * @param userName 用户名称
   */
  enterDetail(userName) {
    this.searchUser(userName);
    this.usersTable.clickResourceNameByRow([userName]);
  }
}
