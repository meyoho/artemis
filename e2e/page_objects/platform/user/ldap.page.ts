import { ServerConf } from '@e2e/config/serverConf';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { $, $$, browser } from 'protractor';

import { PlatformPageBase } from '../platform.page.base';

export class LdapPage extends PlatformPageBase {
  private _ldapUserName: string;
  private _ldapPassword: string;
  private _ldapUrl: string;
  constructor() {
    super();
    this._ldapUserName = ServerConf.LDAPLOGIN_USER;
    this._ldapPassword = ServerConf.LDAPLOGIN_PASSWORD;
    this._ldapUrl = ServerConf.LDAPLOGIN_URL;
  }

  private _expand() {
    browser.get(this._ldapUrl);
    const loginLink = $('td[class="logged_in"] a');
    this.waitElementPresent(loginLink, 'ldap Login 没出现').then(isPresent => {
      if (isPresent) {
        new AlaudaButton(loginLink).click();
        browser.sleep(1000);
        $('input[id="login"]')
          .isPresent()
          .then(isPresent => {
            if (isPresent) {
              new AlaudaInputbox($('input[id="login"]')).input(
                this._ldapUserName,
              );
              new AlaudaInputbox($('input[id="password"]')).input(
                this._ldapPassword,
              );
              new AlaudaButton($('input[name="submit"]')).click();
            }
          });
      }
    });

    const expandButton = $('.tree td div a img[src*="expand"]');

    this.waitElementPresent(expandButton, '').then(isPresent => {
      if (isPresent) {
        new AlaudaButton(expandButton).click();
      }
    });
  }

  /**
   *
   * @param email
   */
  create(email, lastName, giverName = 'qa') {
    this._expand();
    new AlaudaButton($$('div a[title*="Create new entry"]').first()).click();
    browser.sleep(100);

    new AlaudaButton($('.icon label[for*="courierMailAccount"]')).click();
    const listInput = $$('table[style*="left"] td input[class="value"]');

    new AlaudaInputbox(listInput.get(0)).input(giverName);

    new AlaudaInputbox(listInput.get(1)).input(lastName);

    new AlaudaInputbox(listInput.get(3)).input(`${giverName}${lastName}`);

    new AlaudaInputbox(listInput.get(4)).input(email);

    new AlaudaInputbox(listInput.get(5)).input('password');

    new AlaudaInputbox(listInput.get(6)).input('password');

    new AlaudaButton($('input[id*="create"]')).click();
    new AlaudaButton($('input[value*="Commit"]')).click();

    this.waitElementPresent($('input[value*="Update"]'), 'ldap 用户没创建成功');

    return `${giverName} ${lastName}`;
  }

  delete(name) {
    this._expand();
    new AlaudaButton($$(`a[title*="${name}"]`).last()).click();
    new AlaudaButton($('a[title*="confirm"]')).click();
    new AlaudaButton($('input[value="Delete"]')).click();
    this.waitElementNotPresent(
      $$(`a[title*="${name}"]`).last(),
      'ldap 用户没有删除成功',
    );
  }
}
