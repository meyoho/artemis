import { AlaudaElement } from '../../../element_objects/alauda.element';
import { PlatformPageBase } from '../platform.page.base';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { $ } from 'protractor';

export class UserDetailPage extends PlatformPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement('alu-field-set-item', 'label');
  }

  get rolesTable() {
    return new AlaudaAuiTable($('.aui-card .aui-table'));
  }
  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'span'): any {
    return this.alaudaElement.getElementByText(text, tagname);
  }
}
