import { AcpPageBase } from '@e2e/page_objects/acp/acp.page.base';
import { CreatePage } from './create.page';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { $, browser, element, by } from 'protractor';
import { UpdatePage } from './update.page';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { PrepareNsPage } from './prepare.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';

export class ListPage extends AcpPageBase {
  private _timeoutResourcequota: number;

  constructor() {
    super();
    this._timeoutResourcequota = 0;
  }

  get preparePage(): PrepareNsPage {
    return new PrepareNsPage();
  }

  get createPage(): CreatePage {
    return new CreatePage();
  }
  addButton() {
    return new AlaudaButton($('.aui-card__header button:nth-child(1)'));
  }

  /**
   * 递归：等待资源配额显示
   */
  waitResourcequotaDispaly() {
    const xpath =
      '//div[@class="aui-card__header" and normalize-space(text())="资源配置" ]/parent::div//div[@class="aui-tab-header__labels"]';

    element(by.xpath(xpath))
      .isPresent()
      .then(isDisappear => {
        if (!isDisappear) {
          browser.refresh();
          browser.sleep(1000);
          console.log(
            `Loading 资源配额，刷新${this._timeoutResourcequota++}次页面`,
          );
          if (this._timeoutResourcequota > 30) {
            throw new Error('等待Loading 资源配额失超时了');
          }
          this.waitResourcequotaDispaly();
        }
      });
  }

  create(testData, flag = 'create') {
    this.clickLeftNavByText('联邦命名空间');
    this.addButton().click();
    this.createPage.fillForm(testData);
    switch (flag) {
      default:
        this.getButtonByText('创建').click();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        this.waitResourcequotaDispaly();
        browser.sleep(1000);
        break;
      case 'cancel':
        this.getButtonByText('取消').click();
        break;
    }
  }
  get FedNsTable() {
    return new AuiTableComponent(
      $('.aui-page__content aui-card'),
      'aui-table',
      '.aui-card__content',
    );
  }
  get noData(): AlaudaLabel {
    return new AlaudaLabel($('.empty-placeholder'));
  }
  get searchbox() {
    return new AuiSearch($('.aui-card__header aui-search'));
  }
  search(name, row_count = 1) {
    this.clickLeftNavByText('联邦命名空间');
    this.FedNsTable.searchByResourceName(name, row_count);
    // browser.driver.wait(
    //   () => {
    //     this.searchbox.search(name);
    //     browser.sleep(2000);
    //     return this.FedNsTable.getRowCount().then(count => {
    //       console.log(count);
    //       return count === row_count;
    //     });
    //   },
    //   10000,
    //   `检索后，表格中的数据与期望值(${row_count})不匹配`,
    // );
    // return browser.sleep(100);
  }
  enterDetail(name) {
    this.search(name);
    this.FedNsTable.clickResourceNameByRow([name], '名称');
  }
  get updatePage(): UpdatePage {
    return new UpdatePage();
  }
  update(name, testData) {
    this.search(name);
    this.FedNsTable.clickOperationButtonByRow(
      [name],
      '更新联邦命名空间',
      '.aui-button__content aui-icon',
    );
    this.updatePage.fillForm(testData);
    this.getButtonByText('更新').click();
    this.toast.getMessage().then(message => {
      console.log(message);
      if (testData['资源配额'] !== undefined) {
        const cpu =
          testData['资源配额'][0]['CPU'] === undefined
            ? testData['资源配额'][0]['cpu']
            : testData['资源配额'][0]['CPU'];
        this.waitResourcequota(name, cpu);
      }
      if (testData['容器限额'] !== undefined) {
        const cpu =
          testData['容器限额'][0]['CPU'] === undefined
            ? testData['容器限额'][0]['cpu']['默认值']
            : testData['容器限额'][0]['CPU']['默认值'];
        this.waitLimitRange(name, cpu);
      }
    });
    browser.sleep(2000);
    browser.refresh();
    browser.sleep(1000);
    browser.refresh();
    browser.sleep(1000);
  }

  waitResourcequota(
    name: string,
    cpu: string,
    region = this.preparePage.detail.Host,
  ) {
    const cmd = `kubectl get resourcequota -n ${name} default  -o=custom-columns=name:.metadata.name,limit:.spec.hard | grep limits.cpu:${cpu} | awk '{print $1}'`;
    let test = CommonKubectl.execKubectlCommand(cmd, region).trim();
    let timeout = 0;
    while (test === '') {
      if (timeout++ < 90) {
        console.log(`第${timeout}次，等资源配额生效`);
        console.log(
          CommonKubectl.execKubectlCommand(
            `kubectl get resourcequota -n ${name} default -o yaml`,
            region,
          ),
        );
        CommonMethod.sleep(1000);
        test = CommonKubectl.execKubectlCommand(cmd, region).trim();
      } else {
        console.log(
          CommonKubectl.execKubectlCommand(
            `kubectl get resourcequota -n ${name} default -o yaml`,
            region,
          ),
        );
        throw new Error(`第${timeout}次，等资源配额生效超时了`);
      }
    }
  }

  waitLimitRange(
    name: string,
    cpu: string,
    region = this.preparePage.detail.Host,
  ) {
    const cmd = `kubectl get LimitRange -n ${name} default -o=custom-columns=name:.metadata.name,limit:.spec.limits | grep cpu:${cpu}m | awk '{print $1}'`;
    let test = CommonKubectl.execKubectlCommand(cmd, region).trim();
    let timeout = 0;
    while (test === '') {
      if (timeout++ < 90) {
        console.log(`第${timeout}次，等容器限额生效`);
        console.log(
          CommonKubectl.execKubectlCommand(
            `kubectl get LimitRange -n ${name} default -o yaml`,
            region,
          ),
        );
        CommonMethod.sleep(1000);
        test = CommonKubectl.execKubectlCommand(cmd, region).trim();
      } else {
        console.log(
          CommonKubectl.execKubectlCommand(
            `kubectl get LimitRange -n ${name} default -o yaml`,
            region,
          ),
        );
        throw new Error(`第${timeout}次，等容器限额生效超时了`);
      }
    }
  }
}
