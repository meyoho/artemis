import { ListPage } from './list.page';
import { CreatePage } from './create.page';
import { UpdatePage } from './update.page';

import { DetailPage } from './detail.page';
import { DetailVerify } from '@e2e/page_verify/acp/federatedns/detail.page';
import { ListVerify } from '@e2e/page_verify/acp/federatedns/list.page';
import { PlatformPageBase } from '../platform.page.base';
import { PrepareNsPage } from './prepare.page';
import { DeletePage } from './delete.page';

export class FederatedNsPage extends PlatformPageBase {
  get preparePage(): PrepareNsPage {
    return new PrepareNsPage();
  }
  get listPage(): ListPage {
    return new ListPage();
  }
  get detailPage(): DetailPage {
    return new DetailPage();
  }
  get deletePage(): DeletePage {
    return new DeletePage();
  }
  get createPage(): CreatePage {
    return new CreatePage();
  }
  get updatePage(): UpdatePage {
    return new UpdatePage();
  }
  get detailVerify(): DetailVerify {
    return new DetailVerify();
  }
  get listVerify(): ListVerify {
    return new ListVerify();
  }
}
