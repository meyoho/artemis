import { AcpPageBase } from '../../acp/acp.page.base';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { $, element, by, ElementFinder, browser, $$ } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { ListPage } from './list.page';
import { UpdatePage } from './update.page';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { DeletePage } from './delete.page';
import { DialogPage } from '@e2e/page_objects/acp/deployment/dialog.page';

export class DetailPage extends AcpPageBase {
  get name(): ElementFinder {
    this.waitElementPresent(
      $(
        'alu-namespace-detail-info aui-card:nth-child(1) div[fxflexalign="center"]',
      ),
      '校验namespace名称是否存在',
    );
    return $(
      'alu-namespace-detail-info aui-card:nth-child(1) div[fxflexalign="center"]',
    );
  }
  /**
   * 获取基本信息
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'alu-namespace-basic-info alu-field-set-item',
      '.field-set-item__label label',
    );
  }
  getElementByText(
    text: string,
    tagname = '.field-set-item__value',
  ): ElementFinder {
    switch (text) {
      default:
        return this.alaudaElement.getElementByText(text, tagname);
    }
  }
  /**
   * 切换集群
   * @param name 集群名称
   */
  changeRegion(name: string) {
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(text(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }
  /**
   * 获取详情页的资源配额
   */
  get quotaTable() {
    return new AlaudaAuiTable(
      $('aui-card aui-tab-body:not([hidden]) alu-quota-horizontal aui-table'),
    );
  }
  /**
   *
   * @param resourcetype CPU、内存、存储、Pods 数、PVC 数
   * @param name 配额名称 已分配、总配额
   */
  getQuotaValue(resourcetype: string, name = '总配额') {
    return this.quotaTable.getCell(name, [resourcetype]).then(ele => {
      return ele.getText();
    });
  }
  /**
   * 获取详情页的容器限额
   */
  get limitrange() {
    return new AlaudaAuiTable(
      $('aui-card aui-tab-body:not([hidden]) alu-limit-range aui-table'),
    );
  }
  /**
   *
   * @param region 集群名称
   * @param indicator 指标cpu、memory
   * @param name 指标名称 默认值/最大值
   */
  getLimitRangeValue(indicator: string, name: string) {
    switch (indicator) {
      case 'CPU':
      case 'cpu':
        return this.limitrange.getCell(name, ['CPU']).then(ele => {
          return ele.getText();
        });
      case '内存':
      case 'memory':
        return this.limitrange.getCell(name, ['内存']).then(ele => {
          return ele.getText();
        });
      default:
        return null;
    }
  }
  get listPage(): ListPage {
    return new ListPage();
  }
  get updatePage(): UpdatePage {
    return new UpdatePage();
  }
  get action() {
    return new AlaudaDropdown(
      $('.aui-card__header  aui-icon[icon="angle_down"]'),
      $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
    );
  }
  update(name, testData, flag = 'update') {
    this.listPage.enterDetail(name);
    this.action.select('更新联邦命名空间');
    this.updatePage.fillForm(testData);
    switch (flag) {
      default:
        this.getButtonByText('更新').click();
        this.toast.getMessage().then(message => {
          console.log(message);
          this.toast.getMessage().then(message => {
            console.log(message);
            if (testData['资源配额'] !== undefined) {
              const cpu =
                testData['资源配额'][0]['CPU'] === undefined
                  ? testData['资源配额'][0]['cpu']
                  : testData['资源配额'][0]['CPU'];
              this.listPage.waitResourcequota(name, cpu);
            }
            if (testData['容器限额'] !== undefined) {
              const cpu =
                testData['容器限额'][0]['CPU'] === undefined
                  ? testData['容器限额'][0]['cpu']['默认值']
                  : testData['容器限额'][0]['CPU']['默认值'];
              this.listPage.waitLimitRange(name, cpu);
            }
            browser.sleep(2000);
            browser.refresh();
          });
        });
        break;
      case 'cancel':
        this.getButtonByText('取消').click();
        break;
    }
    browser.refresh();
  }
  get deletePage(): DeletePage {
    return new DeletePage();
  }
  delete(name, flag = 'delete') {
    this.listPage.enterDetail(name);
    this.action.select('删除联邦命名空间');
    this.deletePage.inputname(name);
    switch (flag) {
      default:
        this.deletePage.buttonConfirm.click();
        this.toast.getMessage().then(message => {
          console.log(message);
        });
        break;
      case 'cancel':
        this.deletePage.buttonCancel.click();
        break;
    }
    browser.refresh();
  }
  get dialogPage(): DialogPage {
    return new DialogPage();
  }
  updatedisplayname(name, testData, flag = 'update') {
    this.listPage.enterDetail(name);
    this.alaudaElement.getElementByText('显示名称', 'button').click();
    this.dialogPage.fillForm(testData);
    switch (flag) {
      default:
        this.auiDialog.clickConfirm();
        break;
      case 'cancel':
        this.auiDialog.clickCancel();
        break;
    }
  }
}
