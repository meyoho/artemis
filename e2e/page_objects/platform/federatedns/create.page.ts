import { AcpPageBase } from '../../acp/acp.page.base';
import { ElementFinder, browser, element, by } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class CreatePage extends AcpPageBase {
  get quotaElement(): AlaudaElement {
    return new AlaudaElement(
      'alu-quota-form .aui-form-item',
      'alu-quota-form .aui-form-item__label',
    );
  }
  getLimitRangeElementByText(
    type: string,
    left: string,
    tagname = 'input',
    different: boolean,
  ): ElementFinder {
    const root = different
      ? '//aui-tab-group'
      : `//div[contains(@class,"card-bordered")]`;
    const xpath =
      root +
      `//alu-limit-range-form//div[contains(text()," ${type} ")]/following-sibling::aui-form-item//label[@class="aui-form-item__label" and text()=" ${left} "]/ancestor::aui-form-item//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }
  changeRegion(name: string) {
    browser.executeScript('window.scrollTo(0,0)');
    const ele = element(
      by.xpath(`//div[@role="tab"]/div[contains(text(),"${name}")]`),
    );
    this.waitElementPresent(ele, `${name} 未出现`);
    ele.click();
  }
  getElementByText(text: string, tagname = 'input'): ElementFinder {
    switch (text) {
      case '联邦集群':
        return this.getElementByText_xpath(text, 'aui-select');
      case '差异化配置':
        return this.getElementByText_xpath(text, 'aui-switch');
      default:
        return this.getElementByText_xpath(text, tagname);
    }
  }
  enterValue(name, value) {
    switch (name) {
      case '联邦集群':
        new AuiSelect(this.getElementByText(name)).select(value);
        break;
      case '名称':
        new AlaudaInputbox(this.getElementByText(name, 'input')).input(value);
        break;
      case '差异化配置':
        const aui_switch = new AuiSwitch(this.getElementByText(name));
        value ? aui_switch.open() : aui_switch.close();
        break;
      case '资源配额':
        value.forEach(data => {
          for (const key in data) {
            if (key === '集群') {
              this.changeRegion(data[key]);
            } else {
              new AlaudaInputbox(
                this.quotaElement.getElementByText(key, 'input'),
              ).input(data[key]);
            }
          }
        });
        break;
      case '容器限额':
        value.forEach(data => {
          for (const key in data) {
            if (key === '集群') {
              this.changeRegion(data[key]);
            } else {
              const different = '集群' in data;
              for (const key2 in data[key]) {
                new AlaudaInputbox(
                  this.getLimitRangeElementByText(
                    key,
                    key2,
                    'input',
                    different,
                  ),
                ).input(data[key][key2]);
              }
            }
          }
        });
        break;
    }
  }
}
