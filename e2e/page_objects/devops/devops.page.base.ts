/**
 * Created by liuwei on 2019/6/13.
 */

import { $, ElementFinder, browser, promise } from 'protractor';

import { AuiTableComponent } from '../../component/aui_table.component';
import { TableComponent } from '../../component/table.component';
import { ServerConf } from '../../config/serverConf';
import { AloSearch } from '../../element_objects/alauda.aloSearch';
import { AuiDialog } from '../../element_objects/alauda.aui_dialog';
import { CommonKubectl } from '../../utility/common.kubectl';
import { CommonMethod } from '../../utility/common.method';
import { alauda_type_project } from '../../utility/resource.type.k8s';
import { PageBase } from '../page.base';
import { AlaudaButton } from '../../element_objects/alauda.button';
import { DevopsPreparePage } from './devops.prepare.page';

export class DevopsPageBase extends PageBase {
  constructor() {
    super();
    browser.baseUrl = `${ServerConf.BASE_URL}/console-devops`;
  }

  get preparePage(): DevopsPreparePage {
    return new DevopsPreparePage();
  }

  get clusterName() {
    return ServerConf.REGIONNAME;
  }

  get projectName1(): string {
    return 'uiauto-devops1';
  }
  get project1ns1(): string {
    return 'uiauto-devops1-ns1';
  }
  get projectName2(): string {
    return 'uiauto-devops2';
  }
  get project2ns1(): string {
    return 'uiauto-devops2-ns1';
  }
  get projectName3(): string {
    return 'uiauto-devops3';
  }
  get project3ns1(): string {
    return 'uiauto-devops3-ns1';
  }
  get projectName4(): string {
    return 'uiauto-devops4';
  }
  get jenkinsbind1(): string {
    return 'uiauto-jenkinsbind1';
  }
  get jenkinsbind2(): string {
    return 'uiauto-jenkinsbind2';
  }
  get jenkinsbind3(): string {
    return 'uiauto-jenkinsbind3';
  }
  get gitlabsecretPrivate1(): string {
    return 'uiauto-gitlabsecret-private1';
  }
  get gitlabsecretPrivate3(): string {
    return 'uiauto-gitlabsecret-private3';
  }
  get gitlabsecretPrivate4(): string {
    return 'uiauto-gitlabsecret-private4';
  }
  get harborsecretPrivate1(): string {
    return 'uiauto-harborsecret-private1';
  }
  get harborsecretPrivate3(): string {
    return 'uiauto-harborsecret-private3';
  }
  get harborsecretPrivate4(): string {
    return 'uiauto-harborsecret-private4';
  }
  get jenkinssecretPrivate4(): string {
    return 'uiauto-jenkinssecret-private4';
  }
  get gitlabsecretGlobal(): string {
    return 'uiauto-gitlabsecret-global';
  }
  get harborsecretGlobal(): string {
    return 'uiauto-harborsecret-global';
  }
  get jenkinssecretGlobal(): string {
    return 'uiauto-jenkinssecret-global';
  }
  get devopsApp1(): string {
    return 'devops-app1';
  }
  get devopsApp2(): string {
    return 'devops-app2';
  }

  get devopsApp3(): string {
    return 'devops-app3';
  }
  get harborbind2(): string {
    return 'uiauto-harborbind2';
  }
  get worloadDeployDir1(): string {
    return 'app';
  }
  get worloadDeployDir2(): string {
    return 'dep';
  }
  /**
   * 生成测试数据
   * @param prefix 前缀
   * @example getTestData('asm');
   */
  getTestData(prefix = ''): string {
    const branchName = ServerConf.BRANCH_NAME.substring(0, 10)
      .replace('_', '-')
      .replace('/', '')
      .toLowerCase();
    const name = `devops-${branchName}-${prefix}`;
    return name.substring(name.length - 32, name.length);
  }

  /**
   * 随机生成测试数据
   * @param prefix
   */
  randomGetTestData(prefix = ''): string {
    return CommonMethod.random_generate_testData(prefix);
  }

  get resourceTable() {
    return new TableComponent('aui-card');
  }

  /**
   * 面包屑下边的进度条
   */
  get progressbar(): ElementFinder {
    return $('div[class="global-loader loading"]');
  }

  /**
   * progressbar 出现后，等待消失
   * @example waitProgressBarNotPresent()
   */
  waitProgressBarNotPresent(timeout = 20000): promise.Promise<boolean> {
    return this.progressbar.isPresent().then(isPresent => {
      if (isPresent) {
        return this.waitElementNotPresent(
          this.progressbar,
          'progressbar 没消失',
          timeout,
        );
      }
    });
  }

  /**
   * 单击按钮后，出现的确认框
   */
  get auiDialog() {
    return new AuiDialog(
      $('aui-dialog .aui-dialog'),
      '.aui-confirm-dialog__title span:nth-child(2)',
      'button[class*="aui-button--primary"]',
      'button:not([class*="aui-button--primary"])',
      '.aui-confirm-dialog__title span:nth-child(2)',
    );
  }

  // private _goToChinese() {
  //     const icon = $('alo-global-actions .user-actions a');
  //     CommonPage.waitElementPresent(icon);
  //     icon.click();
  //     browser.sleep(100);
  //     // 找到中文下拉选项
  //     const items = $$('.aui-tooltip--bottom aui-menu-item button');
  //     const item = items
  //         .filter(elem => {
  //             return elem.getText().then(text => {
  //                 return text === '简体中文';
  //             });
  //         })
  //         .first();
  //     // 如果找到，单击
  //     item.isPresent().then(isPresent => {
  //         if (isPresent) {
  //             item.click();
  //             this.waitProgressBarNotPresent();
  //         } else {
  //             icon.click();
  //         }
  //     });
  // }

  /**
   * 项目列表
   */
  get projectTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('main'),
      'alo-project-list',
      '.alo-search',
      '.project-card>div span',
      '.project-card',
    ); // 检索框的父元素);
  }

  /**
   * 进入项目管理，项目列表页面
   */
  enterProjectDashboard() {
    browser.get(browser.baseUrl);
    browser.sleep(1000);
    // this._goToChinese();
    // 系统默认在项目管理页
    this.waitProgressBarNotPresent();
  }

  /**
   * 进入平台管理页面
   */
  enterAdminDashboard() {
    //browser.get(browser.baseUrl);
    // this._goToChinese();
    const button = new AlaudaButton(
      $('alo-global-actions .user-actions aui-icon[icon="basic:project_s"]'),
    );
    button.isPresent().then(isPresent => {
      if (isPresent) {
        button.click();
      }
    });
  }

  isProjectReady(projectName) {
    try {
      let isReady = false;
      let count = 0;
      while (!isReady) {
        const project = CommonMethod.parseYaml(
          CommonMethod.execCommand(
            `kubectl get ${alauda_type_project} ${projectName} -o yaml`,
          ),
        );
        CommonMethod.sleep(1000);
        if (count++ > 60) {
          console.log(
            CommonMethod.execCommand(
              `kubectl get ${alauda_type_project} ${projectName} -o yaml`,
            ),
          );
          throw new Error(
            `timeout: project [${projectName}], 没有处于Ready 状态`,
          );
          break;
        }
        isReady = project.metadata.name === projectName;
      }
    } catch (error) {
      console.log(
        CommonMethod.execCommand(
          `kubectl get ${alauda_type_project} ${projectName}`,
        ),
      );
      throw new Error(`平台中没有查询到project [${projectName}]`);
    }
  }

  private _searchProject(projectName: string, timeout = 20000) {
    const nodata = $('alo-no-data');
    const searchBox = new AloSearch($('.list-header .alo-search'));
    return browser.driver
      .wait(() => {
        searchBox.search(projectName);
        return nodata.isPresent().then(isPresent => !isPresent);
      }, timeout)
      .then(
        () => true,
        () => {
          throw new Error(`项目列表页没有查询到[${projectName}]`);
        },
      );
  }

  /**
   * 获得 'global-credentials'
   */
  getCredentialsNs(ns) {
    if (ns === 'global-credentials') {
      return ServerConf.GLOBAL_NAMESPCE === 'alauda-system'
        ? 'global-credentials'
        : `${ServerConf.GLOBAL_NAMESPCE}-global-credentials`;
    }
    return ns;
  }

  /**
   * 进入项目中
   * @param projectName 项目名称
   * @example enterProject('asm-demo')
   */
  enterProject(projectName: string) {
    this._searchProject(projectName);
    this.waitElementPresent(
      this.projectTable.getRow([projectName]),
      `在项目列表页没有找到项目【${projectName}】, 无法进入到详情页`,
    );

    this.projectTable.clickResourceNameByRow([projectName]);
  }

  /**
   * 集成一个Gitlab代码仓库
   * @param gitlab_name 集成Gitlab的名字
   */
  integrateGitlab(gitlab_name: string): string {
    const lab = CommonKubectl.getServiceNameByHost('crs', gitlab_name);
    if (lab === 'false') {
      CommonKubectl.createResource(
        'alauda.codereposervice.yaml',
        {
          '${NAME}': gitlab_name,
          '${CODEREPO_NAME}': gitlab_name,
          '${CODEREPO_HOST}': ServerConf.GITLAB_URL,
          '${TYPE}': 'Gitlab',
          '${PUBLIC}': 'false',
        },
        'qa-gitlab' + String(new Date().getMilliseconds()),
      );
    } else {
      gitlab_name = lab;
    }
    return gitlab_name;
  }

  /**
   * 集成一个制品仓库Harbor
   * @param harbor_name 集成的Harbor的名称
   */
  integrateHarbor(harbor_name: string): string {
    const harbor = CommonKubectl.getServiceNameByHost(
      'imageregistry',
      `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
    );
    if (harbor === 'false') {
      CommonKubectl.createResourceByTemplate(
        'alauda.imageregistry.yaml',
        {
          NAME: harbor_name,
          REGISTRY_NAME: harbor_name,
          REGISTRY_HOST: `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
          REGISTRY_TYPE: 'Harbor',
        },
        'qa-harbor' + String(new Date().getMilliseconds()),
      );
    } else {
      harbor_name = harbor;
    }
    return harbor_name;
  }

  deleteSecret(secretName: string, namespace: string) {
    try {
      // 获得项目绑定的所有集群
      CommonKubectl.execKubectlCommand(
        `kubectl delete secret ${secretName} -n ${namespace}`,
      );
    } catch (e) {
      console.log(e.message);
    }
  }

  deletePipelineconfig(pipeilneconfig: string, namespace: string) {
    try {
      // 获得项目绑定的所有集群
      CommonKubectl.execKubectlCommand(
        `kubectl delete pipelineconfig ${pipeilneconfig} -n ${namespace}`,
      );
    } catch (e) {
      console.log(e.message);
    }
  }

  deleteToolsBind(toolsbind: string, bindName: string, namespace: string) {
    try {
      // 获得项目绑定的所有集群
      CommonKubectl.execKubectlCommand(
        `kubectl delete ${toolsbind} ${bindName} -n ${namespace}`,
      );
    } catch (e) {
      console.log(e.message);
    }
  }
}
