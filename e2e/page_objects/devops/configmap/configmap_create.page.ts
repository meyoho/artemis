import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ItemFieldset } from '@e2e/element_objects/devops/element_objects/items_fieldset';
import { $, ElementFinder, browser } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';

import { ConfigmapDetailPage } from './configmap_detail.page';

export class ConfigmapCreatePage extends DevopsPageBase {
    get detailPage() {
        return new ConfigmapDetailPage();
    }
    // private _inputGroup: ElementFinder;
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement('aui-form-item>div', 'label');
    }

    auiSelect(ele: ElementFinder): AuiSelect {
        return new AuiSelect(
            ele.$('form>aui-select'),
            $('.cdk-overlay-pane aui-tooltip')
        );
    }
    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(text: string): any {
        switch (text) {
            case '数据':
                return new ItemFieldset(
                    this.alaudaElement.getElementByText(
                        text,
                        '.aui-form-item__content alo-key-value-form-list'
                    )
                );
            default:
                return new AlaudaInputbox(
                    this.alaudaElement.getElementByText(text, 'input')
                );
        }
    }

    /**
     * 在文本框中输入值
     * 注意：如果右侧不是inputbox定位，子类需要重写此方法
     * @param name 文本框左侧的文字
     * @param value 输入文本框中的值
     * @example enterValue('描述', '描述信息')
     */
    enterValue(name: string, value: string) {
        switch (name) {
            case '数据':
                this.getElementByText(name).add(value);
                break;
            default:
                this.getElementByText(name).input(value);
                break;
        }
    }

    create(testData) {
        this.clickLeftNavByText('配置字典');
        this.getButtonByText('创建配置字典').click();
        this.fillForm(testData);
        this.getButtonByText('创建').click();
        browser.sleep(100);
        return this.waitElementPresent(
            this.detailPage.name,
            '详情页 comfigmap名称没出现'
        );
    }
}
