/**
 * Created by zhangjiao on 2019/6/14.
 */

import { ConfigmapDetailVerify } from '@e2e/page_verify/devops/configmap/configmap_detail.page';
import { ConfigmapListVerify } from '@e2e/page_verify/devops/configmap/configmap_list.page';

import { DevopsPageBase } from '../devops.page.base';

import { ConfigmapCreatePage } from './configmap_create.page';
import { ConfigmapDetailPage } from './configmap_detail.page';
import { ConfigmapListPage } from './configmap_list.page';
import { PrepareData } from './prepare.page';

export class ConfigPage extends DevopsPageBase {
    get createPage() {
        return new ConfigmapCreatePage();
    }
    get detailPage() {
        return new ConfigmapDetailPage();
    }

    get detailPageVerify(): ConfigmapDetailVerify {
        return new ConfigmapDetailVerify();
    }

    get listPage() {
        return new ConfigmapListPage();
    }
    get listPageVerify(): ConfigmapListVerify {
        return new ConfigmapListVerify();
    }

    get prepareData(): PrepareData {
        return new PrepareData();
    }
}
