import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { DevopsPreparePage } from '@e2e/page_objects/devops/devops.prepare.page';

export class PrepareData extends DevopsPreparePage {
  delete(name: string, ns: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete configmap -n ${ns} ${name} `,
      this.clusterName,
    );
  }
}
