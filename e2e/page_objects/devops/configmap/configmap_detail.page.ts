import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { DevopsConfigmapDataView } from '@e2e/element_objects/devops/element_objects/devops.configmap';
import { $, ElementFinder } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';

export class ConfigmapDetailPage extends DevopsPageBase {
    /**
     * 返回定时任务记录详情页title: 任务/任务记录/<名称>
     */
    get title(): ElementFinder {
        this.waitElementPresent(
            $('aui-breadcrumb .aui-breadcrumb'),
            'configmap详情页title'
        );
        return $('aui-breadcrumb .aui-breadcrumb');
    }
    /**
     * 返回任务记录名称
     */
    get name(): ElementFinder {
        this.waitElementPresent(
            $('.aui-card__header div'),
            '.aui-card__header div 没有定位到 comfigmap名称'
        );
        return $('.aui-card__header div');
    }

    /**
     * 获取configmap 数据父元素
     */
    get data(): DevopsConfigmapDataView {
        return new DevopsConfigmapDataView($('alo-configmap-data-viewer'));
    }

    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            '.alo-detail__field',
            'label',
            $('.alo-detail__row')
        );
    }

    /**
     * @description 根据左侧文字获得右面元素,
     *              注意：子类如果定位不到元素，需要重写此属性, 返回值是右面元素控件,
     *              可以是输入框，选择框，文字，单元按钮等
     * @param text 左侧文字
     * @example getElementByText('名称').getText().then((text) =>{ console.log(text)} )
     */
    getElementByText(text: string): any {
        switch (text) {
            case '显示名称':
            case '创建时间':
                const root: ElementFinder = this.alaudaElement.getElementByText(
                    text,
                    'span'
                );
                return new AlaudaLabel(root);
            case '数据':
                return this.data;
        }
    }
}
