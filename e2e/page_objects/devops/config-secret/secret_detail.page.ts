/**
 * Created by zhangjiao on 2019/6/14.
 */
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { AloPassword } from '@e2e/element_objects/devops/element_objects/alo.password';
import { DevopsConfigmapDataView } from '@e2e/element_objects/devops/element_objects/devops.configmap';
import { SecretListPage } from '@e2e/page_objects/acp/container/secret/secret_list.page';
import { $, $$, ElementFinder } from 'protractor';

import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { DevopsPageBase } from '../devops.page.base';

export class SecretDetailPage extends DevopsPageBase {
    get listPage(): SecretListPage {
        return new SecretListPage();
    }
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            '.alo-detail__field',
            '.alo-detail__field label'
        );
    }

    getElementByText(text: string): any {
        switch (text) {
            case '密码':
                // const iconButton = new AlaudaButton(
                //     this.alaudaElement.getElementByText(text, 'aui-icon')
                // );
                // iconButton.click();
                // const password: ElementFinder = this.alaudaElement.getElementByText(
                //     text,
                //     'span'
                // );
                // return new AlaudaLabel(password);
                return new AloPassword(
                    this.alaudaElement.getElementByText(text, 'alo-password')
                );
            case '数据':
                return this.data;
            default:
                const root: ElementFinder = this.alaudaElement.getElementByText(
                    text,
                    'span'
                );
                return new AlaudaLabel(root);
        }
    }

    get data(): DevopsConfigmapDataView {
        return new DevopsConfigmapDataView($('alo-configsecret-data-viewer'));
    }

    get actionDropdown(): AlaudaDropdown {
        const dropdown = new AlaudaDropdown(
            $('.aui-page__toolbar button'),
            $$('button[class*="aui-menu-item"]')
        );
        return dropdown;
    }

    delete(name) {
        this.listPage.search(name);
        this.actionDropdown.select('删除');
        this.auiDialog.clickConfirm();
    }
}
