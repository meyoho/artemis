/**
 * Created by zhangjiao on 2019/6/14.
 */
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { ItemFieldset } from '@e2e/element_objects/devops/element_objects/items_fieldset';
import { by, element } from 'protractor';

import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { DevopsPageBase } from '../devops.page.base';

export class SecretCreatePage extends DevopsPageBase {
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            'aui-form-item .aui-form-item',
            '.aui-form-item__label'
        );
    }

    /**
     * 根据左侧文字获得右面元素,
     * 注意：子类如果定位不到元素，需要重写此属性
     * @param text 左侧文字
     */
    getElementByText(text: string): any {
        switch (text) {
            case '类型':
                return new AlaudaRadioButton(
                    element(by.xpath('//aui-radio-group'))
                );
            case '数据':
                return new ItemFieldset(
                    this.alaudaElement.getElementByText(
                        text,
                        '.aui-form-item__content alo-key-value-form-list'
                    )
                );
            case 'SSH 私钥':
            case '证书':
            case '私钥':
                return new AlaudaInputbox(
                    this.alaudaElement.getElementByText(text, 'textarea')
                );
            default:
                return new AlaudaInputbox(
                    this.alaudaElement.getElementByText(text, 'input')
                );
        }
    }

    /**
     * 在文本框中输入值
     * 注意：如果右侧不是inputbox定位，子类需要重写此方法
     * @param name 文本框左侧的文字
     * @param value 输入文本框中的值
     */
    enterValue(name, value) {
        switch (name) {
            case '类型':
                this.getElementByText(name).clickByName(value);
                break;
            case '数据':
                this.getElementByText(name).add(value);
                break;
            default:
                this.getElementByText(name).input(value);
                break;
        }
    }

    /**
     * 填写表单
     * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
     */
    fillForm(data) {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                this.enterValue(key, data[key]);
            }
        }
    }

    /**
     * 创建保密字典
     * @param testData 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
     */
    create(testData) {
        this.clickLeftNavByText('保密字典');
        this.getButtonByText('创建保密字典').click();
        this.fillForm(testData);
        this.getButtonByText('创建').click();
    }
}
