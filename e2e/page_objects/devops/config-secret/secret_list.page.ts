import { AuiTableComponent } from '@e2e/component/aui_table.component';
// import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $ } from 'protractor';

import { DevopsPageBase } from '../devops.page.base';

export class SecretListPage extends DevopsPageBase {
    get secretTable() {
        return new AuiTableComponent(
            // $('aui-card hasDivider'),
            $('div>.aui-card__content'),
            'aui-table[class="aui-table"]',
            'aui-card>div>div>.list-header',
            'aui-table-cell',
            'aui-table-row',
            'aui-table-header-row'
        );
    }

    /**
     * 检索configmap
     * @param name 名称
     * @param resultCount 期望检索的个数
     */
    search(name: string, resultCount: number = 1) {
        this.clickLeftNavByText('保密字典');
        this.secretTable.searchByResourceName(name, resultCount);
    }

    /**
     * 单击操作下的选项
     * @param name 名称
     * @param actionName 更新，或删除
     */
    operation(name: string, actionName: string) {
        this.search(name, 1);
        this.secretTable.clickOperationButtonByRow(
            [name],
            actionName,
            'aui-table-cell button',
            'aui-menu-item>button'
        );
    }

    delete(name: string) {
        this.search(name);
        this.operation(name, '删除');
        this.auiDialog.clickConfirm();
    }

    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case 'Header':
                        expect(this.secretTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.secretTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
