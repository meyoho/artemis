/**
 * Created by zongyao on 2019/8/30.
 */

import { SecretDetailVerify } from '@e2e/page_verify/devops/config-secret/secret_detail.page';
import { SecretListVerify } from '@e2e/page_verify/devops/config-secret/secret_list.page';

import { DevopsPageBase } from '../devops.page.base';

import { PrepareData } from './prepare.page';
import { SecretCreatePage } from './secret_create.page';
import { SecretDetailPage } from './secret_detail.page';
import { SecretListPage } from './secret_list.page';

export class SecretPage extends DevopsPageBase {
    get createPage() {
        return new SecretCreatePage();
    }
    get detailPage() {
        return new SecretDetailPage();
    }
    get detailPageVerify(): SecretDetailVerify {
        return new SecretDetailVerify();
    }
    get listPageVerify(): SecretListVerify {
        return new SecretListVerify();
    }
    get listPage() {
        return new SecretListPage();
    }
    get prepareData(): PrepareData {
        return new PrepareData();
    }
}
