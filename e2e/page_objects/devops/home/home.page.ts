/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $, ElementFinder } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { DevopsPageBase } from '../devops.page.base';

export class HomePage extends DevopsPageBase {
    get projectTable(): AuiTableComponent {
        return new AuiTableComponent(
            $('main'),
            'alo-project-list',
            '.alo-search',
            '.project-card>div span',
            '.project-card'
        ); // 检索框的父元素);
    }

    /**
     * 无项目时的提示信息
     * 暂无项目，你可以通过上方 “创建项目” 按钮或切换至平台管理下创建项目
     */
    get noData(): ElementFinder {
        return $('alo-no-data span');
    }

    /**
     * 分页元素
     */
    get paginator(): ElementFinder {
        return $('aui-paginator>div');
    }
}
