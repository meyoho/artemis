/**
 * Created by zhangjiao on 2018/3/1.
 */
import { $, $$, by, element } from 'protractor';

import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { DevopsPageBase } from '../devops.page.base';
import { AlaudaDropdownWithSearch } from '../../../element_objects/alauda.dropdown_search';
import { CreatePage } from './create.page';
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';

export class DevopsBindPage extends DevopsPageBase {
  get createPage() {
    return new CreatePage();
  }
  bindDialog_tools_type(tooltype) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//div[@class="aui-dialog__content"]//button/span[text()="' +
            tooltype +
            '"]',
        ),
      ),
    );
  }
  bindDialog_tools_card(toolname) {
    return new AlaudaButton(
      element(
        by.xpath('//div[@class="title" and text()="' + toolname + '"]/..'),
      ),
    );
  }

  // -----------begin-----------bindlist page
  bindListPage_toolName(binName) {
    return new AlaudaList(by.xpath('//div[@title="' + binName + '"]/..'));
  }
  get bindListPage_search() {
    return new AlaudaInputbox($('.aui-search__input'));
  }
  get bindListPage_empty() {
    return new AlaudaText(by.css('div.empty-list-hint'));
  }
  // -----------end-----------bindlist page
  // -----------begin-----------  bind page
  bindPage_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.bind_firstStep_bindButton.click();
    this.waitProgressBarNotPresent(180000);
  }
  bindPage_sencondinput(testData) {
    this.createPage.fillForm(testData);
    this.bind_secondStep_createButton.click();
  }
  bindPage_assigninput(testData) {
    this.createPage.fillForm(testData);
    this.bindCode_assigndialog_upateButton.click();
  }

  get bindPage_header_text() {
    return new AlaudaText(by.xpath('//div[@class="aui-card__header"]'));
  }
  get bind_firstStep_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }
  get bind_firstStep_bindButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button--primary', '绑定账号')),
    );
  }
  get bind_firstStep_secretType() {
    return new AlaudaRadioButton();
  }
  get bindPage_addSecret() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '添加凭据')),
    );
  }
  click_firstStep_bindButton() {
    this.bind_firstStep_bindButton.click();
    // 等待状态消失
    this.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
      60000,
    );
  }

  get bindCode_secondStep_autoCheckbox() {
    return new AlaudaButton(
      element(
        by.cssContainingText('.aui-checkbox__content', '自动同步全部仓库'),
      ),
    );
  }
  get bindCode_secondStep_accounts() {
    return new AlaudaText(by.css('.alo-repository-selector__photo+span'));
  }
  bindCode_secondStep_selectCode(account) {
    return new AlaudaDropdown(
      element(
        by.xpath(
          '//span[contains(text(), "' +
            account +
            '")]/../../..//aui-multi-select/div',
        ),
      ),
      $$('.aui-option'),
    );
  }
  get bindCode_secondStep_cancelButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '取消')),
    );
  }
  get bindCode_firstStep_erroralert() {
    return new AlaudaText(
      by.cssContainingText('.aui-notification__title', '代码仓库绑定失败'),
    );
  }
  get addSecretDialog_type() {
    return new AlaudaText(by.xpath('//label[text()="类型"]/../..//span'));
  }
  get addSecretDialog_cancelButton() {
    return new AlaudaButton(
      element(
        by.cssContainingText('aui-dialog-footer .aui-button__content', '取消'),
      ),
    );
  }
  get bind_secondStep_createButton() {
    return this.getButtonByText('分配仓库');
  }
  get bindJenkins_erroralert() {
    return new AlaudaText(
      by.cssContainingText('.aui-notification__title', '账号绑定失败'),
    );
  }
  get bindRegistry_erroralert() {
    return new AlaudaText(
      by.cssContainingText('.aui-notification__title', '绑定账号失败'),
    );
  }
  get bindPage_closeButton() {
    return new AlaudaButton($('aui-notification .aui-notification__close'));
  }
  /* 
  绑定详情页面选择凭据下拉框
  */
  get bindPage_secret(): AlaudaDropdownWithSearch {
    return new AlaudaDropdownWithSearch(
      $('.acl-project-select aui-icon'),
      $$('.acl-project-select__options .aui-menu-item span'),
      $('.aui-menu aui-search .hasIcon'),
    );
  }
  // -----------begin-----------  bind page

  // -----------begin-----------detail page
  get detailPage_toProject_link() {
    return new AlaudaButton(
      element(by.xpath('//a[contains(text(), "前往项目")]')),
    );
  }
  // -----------end-----------detail page

  // -----------begin-----------  bindCodeRepoService detail page

  _getElementByText(text, tag) {
    const xpath = `//div[contains(@class,"field")]//label[normalize-space(text())="${text}" ]/ancestor::div[contains(@class,"field")]/${tag}`;
    return new AlaudaLabel(element(by.xpath(xpath)));
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string): any {
    switch (text) {
      case '类型':
        return this._getElementByText(text, 'span/parent::div');
      case 'API 地址':
        return this._getElementByText(text, 'span');
      case '访问地址':
        return this._getElementByText(text, 'a/span');

      case '集成名称':
      case '凭据':
        return this._getElementByText(text, '/a');
      case '仓库类型':
      case '描述':
      case '认证方式':
        return this._getElementByText(text, 'span');
    }
  }

  //harbor绑定页面
  _getElementByText_harbor(text, tag = "*[@class='value']") {
    const xpath = `//li[contains(@class,"info-item")]//span[normalize-space(text())="${text}"]/ancestor::li[contains(@class,"info-item")]//${tag}`;
    return new AlaudaLabel(element(by.xpath(xpath)));
  }

  /**
   *
   * @param harbor绑定详情页面获取数据
   */
  getElementByText_harbor(text: string): any {
    switch (text) {
      case '集成名称：':
      case '凭据：':
      case '绑定时间：':
      case '描述：':
        return this._getElementByText_harbor(text);
      case '类型：':
        return this._getElementByText_harbor(
          text,
          'span[contains(@class,"label")]/parent::li',
        );
    }
  }

  get binddetailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      by.css('.aui-card__header div span'),
      'label',
      'span',
    );
  }
  get binddetailPage_Content1() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .info-item'),
      by.css('.aui-card__header div span'),
      '.label',
      '.value',
    );
  }
  get binddetailPage_Jenkins() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.info-header .header-title span'),
      'label',
      'span',
    );
  }
  get binddetailPage_operate() {
    return new AlaudaButton($('.aui-card__header button'));
  }
  get binddetailPage_updateButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-menu-item', '更新')),
    );
  }
  get binddetailPage_remove_button() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-menu-item', '解除绑定')),
    );
  }
  get bindCodedetailPage_assign_button() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '分配仓库')),
    );
  }

  click_bindCodedetailPage_assign_button() {
    this.bindCodedetailPage_assign_button.click();
    // 等待状态消失
    this.waitElementNotPresent(
      $('div .status aui-icon'),
      '【代码仓库获取中...】的提示没有消失',
      60000,
    );
  }

  get binddetailPage_remove_name() {
    return new AlaudaInputbox($('.aui-dialog__content .aui-input'));
  }
  get binddetailPage_assign_repo() {
    return new AlaudaDropdown(
      $('.aui-tree-select__input'),
      $$('.aui-tree-node__title'),
    );
  }
  get binddetailPage_assign_add() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '添加地址')),
    );
  }

  // 需要输入绑定名称时的“解除绑定”按钮
  get binddetailPage_removedialog_remove() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-dialog__footer .aui-button__content',
          '解除绑定',
        ),
      ),
    );
  }
  // 不需要输入绑定名称时的“解除绑定”按钮
  get binddetailPage_removedialog_remove1() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-confirm-dialog .aui-button__content',
          '解除绑定',
        ),
      ),
    );
  }

  // 不需要输入绑定名称时解除绑定弹框的“取消”按钮
  get binddetailPage_removedialog_remove1_cancell() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-confirm-dialog .aui-button__content',
          '取消',
        ),
      ),
    );
  }
  binddetailPage_assignTable_repoName(account, reponame) {
    return new AlaudaText(
      by.xpath(`//span[contains(text(),"${account}/${reponame}")]`),
    );
  }
  get binddetailPage_codeTable() {
    return new AlaudaAuiTable($('.aui-card__content aui-table'));
  }
  // -----------end-----------  bindCodeRepoService detail page
  // -----------end-----------  update bindCodeRepoService dialog
  get bindUpdateDialog_name() {
    return new AlaudaText(
      by.xpath('//div[@class="aui-form-item__container"]/div/span'),
    );
  }
  get bindUpdateDialog_desc() {
    return new AlaudaInputbox(element(by.name('description')));
  }
  get bindUpdateDialog_updateButton() {
    return new AlaudaButton($('.ng-star-inserted .aui-button--primary'));
  }
  // -----------end-----------  update bindCodeRepoService dialog

  // -----------begin-----------  bindCodeRepoService assign CodeRepo dialog
  get bindCode_assigndialog_accounts() {
    return new AlaudaText(by.css('.alo-repository-selector__photo+span'));
  }
  bindCode_assigndialog_selectcode(account) {
    return new AlaudaDropdown(
      element(
        by.xpath(
          '//span[contains(text(), "' +
            account +
            '")]/../../..//aui-multi-select/div',
        ),
      ),
      $$('.aui-option'),
    );
  }
  bindCode_assigndialog_auto(account) {
    return new AlaudaButton(
      element(by.xpath('//span[text()="' + account + '"]/../..//aui-checkbox')),
    );
  }
  get bindCode_assigndialog_upateButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '更新')),
    );
  }
  get bindCode_assigndialog_title() {
    return new AlaudaText(
      by.cssContainingText('.aui-dialog__header-title', '分配仓库'),
    );
  }

  get bindCode_assigndialog_title_cancel() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '取消')),
    );
  }
  // -----------end-----------  bindCodeRepoService assign CodeRepo dialog

  // -----------begin-----------bind service page
  get bindJenkins_inputcontent() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      by.css('.aui-dialog__header .aui-dialog__header-title'),
      '.aui-form-item__label-wrapper label',
      '.aui-form-item__content .aui-input',
      '.aui-form-item__error-wrapper .aui-form-item__error',
    );
  }

  bindJenkins_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.bindJenkinsDialog_bindButton.click();
  }
  get bindJenkinsDialog_bindButton() {
    return new AlaudaButton(
      element(by.xpath('//button[@aui-button="primary"]')),
    );
  }

  get bindJenkinsDialog_cancelButton() {
    return this.getButtonByText('取消');
  }
  // -----------end-----------bind service page
}
