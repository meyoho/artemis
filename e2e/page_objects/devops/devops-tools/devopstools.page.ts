/**
 * Created by zhangjiao on 2018/3/1.
 */
import { $, browser, by, element, ElementFinder } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { TableComponent } from '../../../component/table.component';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { DevopsPageBase } from '../devops.page.base';

import { CreatePage } from './create.page';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';

export class DevopsToolsPage extends DevopsPageBase {
  get createPage() {
    return new CreatePage();
  }

  waitLoadingNotPresent() {
    return this.waitElementNotPresent(
      $('div[class*="loading"]'),
      'loading 没消失',
    );
  }
  // -----------begin-----------Devopstools list page
  navigationButton() {
    this.clickLeftNavByText('DevOps 工具链');
    this.waitElementPresent(
      this.getButtonByText('集成').button,
      '列表页，集成按钮没出现',
    );
    return this.waitLoadingNotPresent();
  }

  get listPage_servicesTable() {
    return this.resourceTable;
  }

  get listPage_createButton() {
    return this.getButtonByText('集成');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox($('.alo-search-input'));
  }

  listPage_serviceName(serviceName) {
    return new AlaudaList(by.xpath('//div[@title="' + serviceName + '"]/..'));
  }

  listPage_getServiceName(host) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="description" and text()="' +
          host +
          '"]/..//div[@class="title"]',
      ),
    );
  }

  listPage_getServiceHost(host) {
    browser.sleep(2000);
    return element(
      by.xpath('//div[@class="description" and text()="' + host + '"]'),
    ).isPresent();
  }
  // -----------end-----------Devopstools list page

  // -----------begin-----------Integration dialog

  createDialog_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.createDialog_createButton.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  get createDialog_headerTitle_Text() {
    return new AlaudaText(by.xpath('//'));
  }
  createDialog_tools_type(tooltype) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//div[@class="aui-dialog__content"]//button/span[text()="' +
            tooltype +
            '"]',
        ),
      ),
    );
  }
  createDialog_tools_card(toolname) {
    return new AlaudaButton(
      element(
        by.xpath(
          `//div[contains(@class,"name") and contains(text(), "${toolname}")]/..`,
        ),
      ),
    );
  }
  // get createDialog_inputbox() {
  //     return new AlaudaBasicInfo(
  //         by.css('.integrate-form .aui-form-item'),
  //         '.base-header',
  //         '.aui-form-item__label-wrapper label',
  //         '.aui-form-item__content .aui-input',
  //         '.aui-form-item__error-wrapper .aui-form-item__error'
  //     );
  // }
  get createDialog_createButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-dialog .aui-button', '集成')),
    );
  }
  get createDialog_cancelButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-dialog .aui-button', '取消')),
    );
  }
  get integrate_erroralert() {
    return new AlaudaText(
      by.cssContainingText('.aui-notification__title', '集成失败'),
    );
  }
  /**
   * 集成页面必填项校验
   */
  get integrate_input_content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      'input',
      '.aui-form-item__error',
    );
  }
  // -----------begin-----------Integration dialog

  // -----------begin-----------tools detail page
  get detailPage_bind_table() {
    return this.resourceTable;
  }

  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.base-body .field'),
      by.css('.header-title span'),
      'label',
      'span',
    );
  }
  get detailPage_errorIcon() {
    return new AlaudaButton($('.info-header .header-title .aui-icon'));
  }
  get detailPage_svgButton() {
    return new AlaudaButton($('.info-header .alo-menu-trigger'));
  }
  detailPage_operateButton(operateName) {
    return this.getButtonByText(operateName);
  }
  get deleteConfirm_deleteButton() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-confirm-dialog .aui-button__content',
          '删除',
        ),
      ),
    );
  }
  get deleteConfirm_cancelButton() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-confirm-dialog .aui-button__content',
          '取消',
        ),
      ),
    );
  }
  get deleteConfirm_name() {
    return new AlaudaInputbox($('.aui-dialog__content .aui-input'));
  }
  get deleteConfirm_cancelButton1() {
    return new AlaudaButton(
      element(
        by.cssContainingText(
          '.aui-dialog__footer .aui-button__content',
          '取消',
        ),
      ),
    );
  }

  get detailPage_nodata() {
    return new AlaudaText(
      by.cssContainingText('.no-data .ng-star-inserted', '无绑定账号'),
    );
  }
  get detailPage_acount_table() {
    return new TableComponent('aui-card');
  }
  get detailPage_bindButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '绑定')),
    );
  }
  get selectProjectTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('.aui-dialog ng-component'),
      '.alo-select-project__content',
      '.alo-search',
      '.aui-table-cell',
      '.project-card',
    ); // 检索框的父元素;
  }
  selectProject_project(projectName) {
    return new AlaudaButton(
      element(by.xpath('//div[text()="' + projectName + '"]/../../..')),
    );
  }
  get bindPage_cancelButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-card__footer .aui-button', '取消')),
    );
  }
  get createDialog_name_inputbox() {
    return new AlaudaInputbox(element(by.name('name')));
  }
  get createDialog_host_inputbox() {
    return new AlaudaInputbox(element(by.name('host')));
  }
  // -----------end-----------toos detail page

  // -----------begin-----------service updateDialog
  get updateDialog_host_inputbox() {
    return new AlaudaInputbox(element(by.name('host')));
  }
  get updateDialog_updateButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-dialog .aui-button__content', '更新')),
    );
  }
  get updateDialog_cancelsButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-dialog .aui-button__content', '取消')),
    );
  }
  get updateDialog_updateerror() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-notification__title', '更新失败')),
    );
  }
  get updateDialog_updateerror_close() {
    return new AlaudaButton($('.aui-notification__close'));
  }
  // -----------end-----------service updateDialog

  getHintByText(
    text: string,
    targetSelect = '.aui-form-item__error',
  ): ElementFinder {
    const root = new AlaudaElement(
      'aui-form-item .aui-form-item',
      'label[class="aui-form-item__label"]',
      $('aui-dialog-content'),
    );

    return root.getElementByText(text, targetSelect);
  }

  inputByText(text: string, value: string, targetSelect = 'input') {
    const root = new AlaudaElement(
      'aui-form-item .aui-form-item',
      'label[class="aui-form-item__label"]',
      $('aui-dialog-content'),
    );

    new AlaudaInputbox(root.getElementByText(text, targetSelect)).input(value);
  }
}
