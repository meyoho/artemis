/**
 * Created by zhangjiao on 2018/9/4.
 */
import { $, $$, by, element } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { PageBase } from '../../page.base';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';

export class CreatePage extends PageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      '.aui-form aui-form-item',
      'aui-form-item label[class*=aui-form-item__label]',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(text: string, tagname = 'input'): any {
    switch (text) {
      case '认证方式':
        return new AlaudaRadioButton();
      case '类型':
        return new AlaudaRadioButton();
      default:
        return this.alaudaElement.getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case '类型':
        this.getElementByText(name, tagname).clickByName(value);
        break;
      case '认证方式':
        this.getElementByText(
          name,
          '.aui-radio-group aui-radio-button',
        ).clickByName(value);
        break;
      case '描述':
        this.getElementByText(name, 'textarea').clear();
        this.getElementByText(name, 'textarea').sendKeys(value);
        break;
      case '凭据':
        const secret = new AlaudaDropdown(
          element(by.name('secret')),
          $$('.aui-option'),
        );
        secret.input(value);
        break;
      case '选择镜像repo':
        const imagerepo = new AlaudaDropdown(
          $('.select-line input'),
          $$('.aui-tree-node .aui-tree-node__title'),
        );
        imagerepo.select(value);
        break;
      case '点击添加地址':
        const okclick = new AlaudaButton(
          $('.select-line .aui-button__content'),
        );
        okclick.click();
        break;
      default:
        new AlaudaInputbox(this.getElementByText(name, tagname)).input(value);
        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
