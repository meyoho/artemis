/**
 * Created by zhangjiao on 2019/6/13.
 */

import { $, browser, by, element } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { DevopsPageBase } from '../devops.page.base';

import { PipelineCreatePage } from './create.page';
import { PipelineUpdatePage } from './update.page';
import { CommonPage } from '@e2e/utility/common.page';

export class PipelineTemplatePage extends DevopsPageBase {
  get createPage() {
    return new PipelineCreatePage();
  }
  get updatePage() {
    return new PipelineUpdatePage();
  }
  navigationButton() {
    return this.clickLeftNavByText('流水线');
  }

  async executePipeline() {
    await this.getButtonByText('操作').click();
    await browser.sleep(5000);
    await this.getButtonByText('执行').click();
    const message = await this.toast.getMessage();
    expect(message).toBe('流水线开始执行成功');
    console.log(message);
  }

  async listPage_pipelineExecuteResult() {
    const result_element = element(by.xpath('//span[text()=" 执行成功 "]/.'));
    await CommonPage.waitElementPresent(result_element, 300000);
    return result_element.getText();
  }

  createPipiline_first(testData, tmp_name) {
    this.getButtonByText('创建流水线').click();
    this.listPage_createMode_card.isPresent().then(isPresent => {
      if (!isPresent) {
        this.getButtonByText('创建流水线').click();
        this.waitElementPresent(
          this.listPage_createMode_card.button,
          'Devops/流水线，列表页，单击【创建流水线】按钮后，创建流水线 dialog 页面没打开',
        );
      }
      browser.sleep(200);
      this.listPage_createMode_card.click();
      this.createPipeline_select_tmp(tmp_name).click();
      this.createPage_button('下一步').click();
      this.createPage.fillForm(testData);
      this.createPage_button('下一步').click();
    });
  }
  get listPage_createMode_card() {
    return new AlaudaButton(
      element(by.xpath('//span[normalize-space(text())="模版创建"]/..')),
    );
  }
  createPipeline_select_tmp(tmp_name) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//span[normalize-space(text())="' +
            tmp_name +
            '"]/../../..//button[@aui-button="primary"]',
        ),
      ),
    );
  }
  createPage_button(buttonName) {
    return this.getButtonByText(buttonName);
  }
  createPipiline_second(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
    browser.sleep(1000);
    this.createPage_button('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  createPipiline_second1(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
  }
  createPipiline_third(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  createPipiline_second_cancel(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
    browser.sleep(1000);
    this.createPage_button('取消').click();
  }
  createPipiline_sync_second(testData) {
    this.advanced_button('源镜像信息').click();
    this.advanced_button('目标镜像信息').click();
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
    browser.sleep(1000);
    this.createPage_button('创建').click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
  advanced_button(module) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//div[text()=" ' +
            module +
            ' "]/..//div[contains(text(), "展开高级选项")]',
        ),
      ),
    );
  }
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.basic-body .field'),
      by.css('.aui-card__header span span'),
      'label',
      'span',
    );
  }
  configDialog_selectType(typename) {
    this.createPage.enterValue('选择镜像方式', typename);
  }
  createPipiline_selectclick(name) {
    this.createPage.enterValue(name, 'click');
  }
  get configDialog_secretInput() {
    return new AlaudaText(by.css('aui-dialog aui-select .aui-select__label'));
  }
  get configDialog_cancelButton() {
    return new AlaudaButton(
      $('.aui-dialog__footer button[class*=aui-button--default]'),
    );
  }

  // 更新流水线
  updatePipiline(testData) {
    this.updatePage.fillForm(testData);
    browser.sleep(1000);
    this.createPage_button('更新').click();
    return this.toast.getMessage().then(message => {
      console.log(message);
    });
  }
}
