/**
 * Created by zhangjiao on 2019/6/13.
 */
import { $, $$, by, element, ElementFinder, browser } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { AuiSwitch } from '../../../element_objects/alauda.auiSwitch';
import { DevopsPageBase } from '../devops.page.base';
import { protractor } from 'protractor/built/ptor';

export class PipelineCreatePage extends DevopsPageBase {
  /**
   * 用于方法 getElementByText 定位元素使用，
   */
  get alaudaElement(): AlaudaElement {
    return new AlaudaElement(
      'aui-form-item .aui-form-item',
      'label[class*=aui-form-item__label]',
    );
  }

  /**
   * 根据左侧文字获得右面元素,
   * 注意：子类如果定位不到元素，需要重写此属性
   * @param text 左侧文字
   */
  getElementByText(
    text: string,
    tagname = 'input',
  ): AlaudaRadioButton | ElementFinder {
    switch (text) {
      case '':
        return new AlaudaRadioButton();
      case '方式':
        return new AlaudaRadioButton(
          element(by.xpath('//label[text()="方式"]/../..//aui-radio-group')),
        );
      case '执行方式':
        return new AlaudaRadioButton(
          element(
            by.xpath('//label[text()="执行方式"]/../..//aui-radio-group'),
          ),
        );
      case '仓库类型':
        return new AlaudaRadioButton(
          element(
            by.xpath('//label[text()="仓库类型"]/../..//aui-radio-group'),
          ),
        );
      case 'Jenkinsfile 位置':
        return new AlaudaRadioButton(
          element(
            by.xpath(
              '//label[text()="Jenkinsfile 位置"]/../..//aui-radio-group',
            ),
          ),
        );
      case '镜像仓库':
        const temp = new AlaudaElement(
          'aui-form-item .aui-form-item',
          'label[class*=aui-form-item__label]',
          $('acl-image-push-input'),
        );
        return temp.getElementByText(text, tagname);
      default:
        return this.alaudaElement.getElementByText(text, tagname);
    }
  }

  /**
   * 在文本框中输入值
   * 注意：如果右侧不是inputbox定位，子类需要重写此方法
   * @param name 文本框左侧的文字
   * @param value 输入文本框中的值
   */
  enterValue(name, value, tagname = 'input') {
    switch (name) {
      case '':
        this.getElementByText(name, tagname).clickByName(value);
        break;
      case '代码选择':
        const selectclick = new AlaudaButton(
          element(
            by.xpath(
              '//label[contains(text(), "代码仓库")]/../../div/div/alo-repository-selector',
            ),
          ),
        );
        selectclick.click();
        break;
      case '镜像选择':
        const imageclick = new AlaudaButton(
          element(
            by.xpath(
              '//div[text()=" Docker 构建 "]/..//acl-image-push-control',
            ),
          ),
        );
        imageclick.click();
        break;
      case '添加':
        const addtagClick = new AlaudaButton($('.tag-form-style button'));
        addtagClick.click();
        break;
      case '添加版本':
        const addtagInput = new AlaudaInputbox(
          element(by.css('.tag-form-style div:nth-child(2) input')),
        );
        addtagInput.input(value);
        break;
      case '更新应用镜像选择':
        const appimageclick = new AlaudaButton(
          $('acl-image-pull-control .select-button'),
        );
        appimageclick.click();
        break;
      case '选择仓库确定':
        const okclick = new AlaudaButton(
          $('.aui-dialog__footer .aui-button--primary'),
        );
        okclick.click();
        break;
      case '选择镜像确定':
        const okclick1 = new AlaudaButton(
          $('.aui-dialog__footer .aui-button--primary'),
        );
        okclick1.click();
        break;
      case '选择代码方式':
        this.getElementByText('方式', 'aui-radio-button').clickByName(value);
        break;
      case '选择仓库类型':
        this.getElementByText('仓库类型', 'aui-radio-button').clickByName(
          value,
        );
        break;
      case '选择镜像方式':
        this.getElementByText('方式', 'aui-radio-button').clickByName(value);
        break;
      case '执行方式':
        this.getElementByText(name, 'aui-radio-button').clickByName(value);
        break;
      case 'Jenkinsfile 位置':
        this.getElementByText(name, tagname).clickByName(value);
        break;
      case 'Jenkins':
        const jenkins = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "Jenkins")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        jenkins.select(value);
        break;
      case '所属应用':
        const app = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "所属应用")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        app.select(value);
        break;
      case '仓库凭据':
        const secret = new AlaudaDropdown(
          element(by.xpath('//label[contains(text(), "凭据")]/../..//input')),
          $$('.aui-option'),
        );
        secret.select(value);
        break;
      case '镜像凭据':
        const secret1 = new AlaudaDropdown(
          element(by.xpath('//label[text()="凭据"]/../..//aui-select')),
          $$('.aui-option'),
        );
        secret1.select(value);
        break;
      case '镜像仓库-选择':
        const imagerepo_select = new AlaudaDropdown(
          element(
            by.xpath('//label[contains(text(), "镜像仓库")]/../..//aui-select'),
          ),
          $$('.cdk-overlay-pane .aui-option .image-name'),
        );
        imagerepo_select.select(value);
        break;
      case '镜像仓库-标签':
        const imagerepo_tag = new AlaudaInputbox(
          element(by.xpath('//label[contains(text(), "版本")]/../..//input')),
        );
        imagerepo_tag.input(value);
        break;
      case '老镜像仓库-选择':
        const oldimagerepo_select = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "镜像仓库")]/../..//aui-select[@formcontrolname="repository"]',
            ),
          ),
          $$('.cdk-overlay-pane .aui-option .image-name'),
        );
        oldimagerepo_select.select(value);
        break;
      case '老镜像仓库-标签':
        const oldimagerepo_tag = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "镜像仓库")]/../..//aui-select[@formcontrolname="tag"]',
            ),
          ),
          $$('.cdk-overlay-pane .aui-option'),
        );
        oldimagerepo_tag.select(value);
        break;
      case '源镜像仓库':
        const imagerepo1 = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[text()=" 源镜像仓库 "]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        imagerepo1.select(value);
        break;
      case '目标镜像仓库':
        const imagerepo2 = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[text()=" 目标镜像仓库 "]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        imagerepo2.select(value);
        break;
      case '项目':
        const project = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "项目")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        project.select(value);
        break;
      case '创建应用':
        const createApp = new AuiSwitch(
          this.alaudaElement.getElementByText('创建应用', 'aui-switch'),
        );
        value ? createApp.open() : createApp.close();
        break;
      case '集群':
        const region = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "集群")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        region.select(value);
        break;
      case '命名空间':
        const namespace = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "命名空间")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        namespace.select(value);
        break;
      case '应用名称':
      case '更新应用-应用':
        const appname = new AlaudaDropdown(
          element(
            by.xpath(
              '//aui-form-item//label[@class="aui-form-item__label" and @title="应用" ]/ancestor::aui-form-item//aui-select',
            ),
          ),
          $$('.aui-option'),
        );
        appname.select(value);
        break;
      case '容器名称':
      case '更新应用-容器':
        const container = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "容器")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        container.select(value);
        break;
      case '创建应用-部署配置目录':
        const inputDir = new AlaudaInputbox(
          element(
            by.xpath('//label[contains(text(), "部署配置目录")]/../..//input'),
          ),
        );
        inputDir.input(value);
        break;
      case '创建应用-容器':
        const inputContainer = new AlaudaInputbox(
          element(by.xpath('//label[contains(text(), "容器")]/../..//input')),
        );
        inputContainer.input(value);
        break;
      case '代码仓库地址':
        const coderepoinput = new AlaudaInputbox(
          element(
            by.xpath('//label[contains(text(), "代码仓库地址")]/../..//input'),
          ),
        );
        coderepoinput.input(value);
        break;
      case '代码仓库-选择':
        const coderepo = new AlaudaDropdown(
          $('aui-dialog-content aui-select'),
          $$('.repo-name'),
        );
        coderepo.select(value);
        break;
      case '构建镜像标签':
        const taginput1 = new AlaudaInputbox(
          element(
            by.xpath(
              '//div[text()=" Docker 构建 "]/..//label[text()=" 镜像标签 "]/../..//input',
            ),
          ),
        );
        taginput1.input(value);
        break;
      case '代码检出展开高级选项':
        const advanced_code = new AlaudaButton(
          element(
            by.xpath(
              '//div[text()=" 代码检出 "]/..//div[contains(text(), "展开高级选项")]',
            ),
          ),
        );
        advanced_code.click();
        break;
      case 'Docker构建展开高级选项':
        const advanced_image = new AlaudaButton(
          element(
            by.xpath(
              '//div[text()=" Docker 构建 "]/..//div[contains(text(), "展开高级选项")]',
            ),
          ),
        );
        advanced_image.click();
        break;
      case '更新应用展开高级选项':
        const advanced_app = new AlaudaButton(
          element(
            by.xpath(
              '//div[text()=" 更新应用 "]/..//div[contains(text(), "展开高级选项")]',
            ),
          ),
        );
        advanced_app.click();
        break;
      case '定时扫描':
        const triggerscan = new AlaudaButton(
          element(
            by.xpath('//div[text()=" 定时扫描 "]/../..//div/aui-checkbox'),
          ),
        );
        triggerscan.click();
        const select_trigger = new AlaudaDropdown(
          $('.card .aui-select'),
          $$('.aui-option'),
        );
        select_trigger.select(value);
        break;
      case '定时触发器':
        const trigger = new AlaudaButton(
          element(
            by.xpath(
              '//div[text()=" 定时触发器 "]/../..//div[@class="checkbox"]/aui-checkbox',
            ),
          ),
        );
        trigger.click();
        break;
      case '构建命令':
        const commandsinput = new AlaudaInputbox(
          element(
            by.xpath(
              '//label[contains(text(), "构建命令")]/../..//div[@class="view-lines"]',
            ),
          ),
        );
        commandsinput.input(value);
        break;
      case '镜像仓库-仓库':
        const repoinput = new AlaudaInputbox(
          element(
            by.xpath(
              '//label[contains(text(), "镜像仓库")]/../..//div[@class="image-input"]/input',
            ),
          ),
        );
        repoinput.input(value);
        break;
      case '分发仓库':
      case '依赖仓库':
        const repoSelect = new AlaudaDropdown(
          element(
            by.xpath(
              `//label[contains(text(), "${name}")]/../following-sibling::div`,
            ),
          ),
          $$('.aui-option'),
        );
        repoSelect.select(value);
        break;
      case '分支':
        const branch = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "分支")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        branch.select(value);
        break;
      case '输入分支':
        const inputbranch = new AlaudaDropdown(
          element(
            by.xpath(
              '//label[contains(text(), "分支")]/../following-sibling::div',
            ),
          ),
          $$('.aui-option'),
        );
        inputbranch.input(value);
        break;
      default:
        const inputbox = new AlaudaInputbox(this.getElementByText(
          name,
          tagname,
        ) as ElementFinder);

        inputbox.input(value);

        $('aui-tooltip aui-suggestion-group')
          .isPresent()
          .then(isPresent => {
            if (isPresent) {
              inputbox.inputbox.sendKeys(protractor.Key.ENTER);
              browser.sleep(50);
            }
          });

        break;
    }
  }

  /**
   * 填写表单
   * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
   */
  fillForm(data) {
    for (const key in data) {
      this.enterValue(key, data[key]);
    }
  }
}
