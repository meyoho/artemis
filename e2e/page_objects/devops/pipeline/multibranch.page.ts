/**
 * Created by zhangjiao on 2019/6/13.
 */

import { browser, by, element } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { DevopsPageBase } from '../devops.page.base';

import { PipelineCreatePage } from './create.page';
import { PipelineUpdatePage } from './update.page';

export class MultiBranchPage extends DevopsPageBase {
  get createPage() {
    return new PipelineCreatePage();
  }
  get updatePage() {
    return new PipelineUpdatePage();
  }
  navigationButton() {
    return this.clickLeftNavByText('流水线');
  }

  createMultiPipiline_first(testData) {
    this.getButtonByText('创建流水线').click();
    this.listPage_createMode_card.click();
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
  }
  get listPage_createMode_card() {
    return new AlaudaButton(
      element(by.xpath('//span[text()="多分支流水线"]/..')),
    );
  }
  createPipiline_second(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
    browser.sleep(1000);
    this.createPage_button('创建').click();
  }
  createPipiline_second1(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('下一步').click();
  }

  createPipiline_third(testData) {
    this.createPage.fillForm(testData);
    this.createPage_button('创建').click();
  }

  createPage_button(buttonName) {
    return this.getButtonByText(buttonName);
  }
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.basic-body .field'),
      by.css('.aui-card__header span span'),
      'label',
      'span',
    );
  }
  get detailPage_pipeline_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.aui-card .header'),
      'label',
      'span',
    );
  }
  get detailPage_pipelinetab_code() {
    return new AlaudaText(
      by.css('.aui-card__content .alo-detail__field span span'),
    );
  }
  // 更新流水线
  updatePipiline(testData) {
    this.updatePage.fillForm(testData);
    browser.sleep(1000);
    this.createPage_button('更新').click();
  }
}
