/**
 * Created by zhangjiao on 2019/6/13.
 */

import { $, $$, by, element } from 'protractor';

import { AlaudaAuiCodeEditor } from '../../../element_objects/alauda.aui_code_editor';
import { AlaudaAuiTable } from '../../../element_objects/alauda.aui_table';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaTabItem } from '../../../element_objects/alauda.tabitem';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { AlaudaYamlEditor } from '../../../element_objects/alauda.yamleditor';
import { DevopsPageBase } from '../devops.page.base';

import { PipelineCreatePage } from './create.page';
import { PipelineUpdatePage } from './update.page';

export class PipelineJenkinsfilePage extends DevopsPageBase {
  get createPage() {
    return new PipelineCreatePage();
  }
  get updatePage() {
    return new PipelineUpdatePage();
  }
  navigationButton() {
    return this.clickLeftNavByText('流水线');
  }

  createPipiline_byscript_first(testData) {
    this.getButtonByText('创建流水线').click();
    this.listPage_createMode_card.isPresent();
    this.listPage_createMode_card.click();

    this.createPage_nextButton.isPresent();

    this.createPage.fillForm(testData);
    this.createPage_nextButton.click();
  }

  get createPipeline_select() {
    return new AlaudaButton($('.select .select-button'));
  }
  createPipeline_sourceType(typename) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//aui-radio-group//span[contains(text(), "' + typename + '")]',
        ),
      ),
    );
  }
  get createPipeline_repourl_input() {
    return new AlaudaInputbox(
      element(
        by.xpath('//label[contains(text(), "代码仓库地址")]/../..//input'),
      ),
    );
  }
  get createPipeline_secret_dropdown() {
    return new AlaudaDropdown(
      element(by.xpath('//label[contains(text(), "凭据")]/../..//input')),
      $$('.aui-option'),
    );
  }
  get createPipeline_coderepo_dropdown() {
    return new AlaudaDropdown(
      element(by.name('bindingRepository')),
      $$('.aui-option'),
    );
  }
  get createPipeline_addSecretButton() {
    return this.getButtonByText('添加凭据');
  }
  createPipiline_byscript_second(testData) {
    this.createPage.fillForm(testData);
    // this.createPage_confirmButton.click(); // 选择代码仓库dialog中点击确认按钮
    this.createPage_nextButton.click();
  }
  get createPipeline_location() {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-radio-button aui-radio-button--medium isChecked isPlain"]/label/span',
      ),
    );
  }

  // -----------begin-----------pipeline List page
  get listPage_createPipeline_Button() {
    return this.getButtonByText('创建流水线');
  }

  get listPage_nameFilter_input() {
    return new AlaudaInputbox($('input[placeholder="请输入名称或显示名称"]'));
  }

  listPage_pipelineName(projectName, pipelineName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="/console-devops/workspace/' +
          projectName +
          '/pipelines/all/' +
          pipelineName +
          '"]',
      ),
    );
  }

  listPage_historyView(projectName, pipelineName) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//a[@href="/console-devops/workspace/' +
            projectName +
            '/pipelines/' +
            pipelineName +
            '"]/../../../..//aui-icon[@class="alo-history-preview__overview"]',
        ),
      ),
    );
  }

  logView_dialog_header(pipelineName) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-dialog__header-title"]//span[contains(text(), "' +
          pipelineName +
          '")]',
      ),
    );
  }
  get logView_dialog_cancel() {
    return new AlaudaButton($('.aui-dialog__header .aui-dialog__header-close'));
  }
  listPage_pipelineOperate(projectName, pipelineName, operateName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="/console-devops/workspace/' +
          projectName +
          '/pipelines/all/' +
          pipelineName +
          '"]',
      ),
      by.xpath(
        '//a[@href="/console-devops/workspace/' +
          projectName +
          '/pipelines/all/' +
          pipelineName +
          '"]/../../../..//alo-menu-trigger',
      ),
      by.cssContainingText('.aui-menu-item', operateName),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  get listPage_createMode_card() {
    return new AlaudaButton(element(by.xpath('//span[text()="脚本创建"]/..')));
  }

  listPage_confirmdelete_button() {
    return this.getButtonByText('删除');
  }

  // -----------end-----------pipeline List page

  // -----------begin-----------pipeline detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card .field'),
      by.css('.aui-card__header span span'),
      'label',
      'span',
    );
  }
  get detailPage_pipeline_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.aui-card .header'),
      'label',
      'span',
    );
  }

  get detailPage_pipelinetab_code() {
    return new AlaudaText(
      by.css('.aui-card__content .alo-detail__field span span'),
    );
  }

  detailPage_Content_trigger(triggertype) {
    const elem = element(
      by.xpath(
        `//label[contains(text(), "触发器")]/..//*[@class="aui-icon ${triggertype}"]`,
      ),
    );
    return new AlaudaButton(elem);
  }
  detailPage_TabItem(tabName) {
    return new AlaudaTabItem(
      by.xpath('//div[@class="aui-tab-header__labels"]'),
      by.xpath(
        '//div[@class="aui-tab-header__labels"]//div[@class="aui-tab-label ng-star-inserted isActive"]',
      ),
      by.xpath(
        '//div[@class="aui-tab-header__labels"]/div/div/span[contains(text(), "' +
          tabName +
          '")]',
      ),
    );
  }
  detail_executeHistory_id(id) {
    return new AlaudaList(by.xpath(`//a[normalize-space(text()) = "#${id}"]`));
  }
  detail_executeHistory_status(id) {
    return new AlaudaList(
      by.xpath(
        `//a[normalize-space(text()) = "#${id}"]/../../../..//span//span`,
      ),
    );
  }

  detailPage_executeHistory_operate(historyId, operateName) {
    return new AlaudaList(
      by.xpath(`//a[normalize-space(text()) = "#${historyId}"]`), ////a[contains(text(), "' + historyId + '")]
      by.xpath(
        `//a[normalize-space(text()) = "#${historyId}"]/../../../..//button`,
      ),
      by.xpath('//button[contains(text(), "' + operateName + '")]'),
      by.css('aui-confirm-dialog .aui-confirm-dialog__confirm-button'),
    );
  }

  get detailPage_executeHistory_Table() {
    return new AlaudaAuiTable($('.alo-card__section-body aui-table'));
  }

  get detailPage_historyTable_logview() {
    return new AlaudaButton(
      element(by.xpath('//aui-icon[@class="history-name__overview"]')),
    );
  }

  get detailPage_operate_all() {
    return this.getButtonByText('操作');
  }
  detailPage_operate_button(operateName) {
    return this.getButtonByText(operateName);
  }
  get detailPage_PipelineTab_jenkins() {
    return new AlaudaText(
      by.xpath('//div[contains(text(), "Jenkinsfile (只读)")]'),
    );
  }

  get codeEditor() {
    return new AlaudaAuiCodeEditor($('.aui-card__content aui-code-editor'));
  }
  // -----------end-----------pipeline detail page

  // -----------begin----------pipeline create page

  get createPage_fistStep_input() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      'label',
      'input',
    );
  }

  get createPage_nextButton() {
    return this.getButtonByText('下一步');
  }
  get createPage_createButton() {
    return new AlaudaButton(
      element(by.xpath('//button[@aui-button="primary"]')),
    );
  }
  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }

  get createPage_yaml_textarea() {
    return new AlaudaYamlEditor(
      by.css('.ng-monaco-editor-container'),
      by.xpath(
        '//following::div[contains(@class,"aui-code-editor-toolbar__control-button")]',
      ),
    );
  }

  // -----------begin-----------pipeline update page
  get updatePage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper',
      '.aui-form-item__content',
    );
  }
  get updatePage_inputbox() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper',
      '.aui-form-item__content input',
    );
  }
  // 更新流水线
  updatePipiline(testData) {
    this.updatePage.fillForm(testData);
    this.updatePage_updateButton.click();
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  get updatePage_updateButton() {
    return this.getButtonByText('更新');
  }
  get updatePage_cancelButton() {
    return this.getButtonByText('取消');
  }
  // -----------end-----------pipeline update page

  // -----------begin-----------pipeline executedetail page
  get execute_detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      '.base-header',
      'label',
      'span',
    );
  }
  execute_detailPage_title(pipeline) {
    return new AlaudaText(
      by.xpath(`//div[@class="content"]/span[text()="${pipeline}"]`),
    );
  }
  // -----------end-----------pipeline executedetail page
}
