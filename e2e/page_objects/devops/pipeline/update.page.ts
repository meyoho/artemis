/**
 * Created by zhangjiao on 2019/6/13.
 */
import { $, $$, browser, by, element } from 'protractor';

import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaElement } from '../../../element_objects/alauda.element';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '../../../element_objects/alauda.radioButton';
import { DevopsPageBase } from '../devops.page.base';

export class PipelineUpdatePage extends DevopsPageBase {
    /**
     * 用于方法 getElementByText 定位元素使用，
     */
    get alaudaElement(): AlaudaElement {
        return new AlaudaElement(
            '.aui-form aui-form-item',
            'aui-form-item label[class*=aui-form-item__label]'
        );
    }

    /**
     * 根据左侧文字获得右面元素,
     * 注意：子类如果定位不到元素，需要重写此属性
     * @param text 左侧文字
     */
    getElementByText(text: string, tagname: string = 'input'): any {
        switch (text) {
            case '':
                return new AlaudaRadioButton();
            case '方式':
                return new AlaudaRadioButton(
                    element(
                        by.xpath(
                            '//label[text()="方式"]/../..//aui-radio-group'
                        )
                    )
                );
            case '执行方式':
                return new AlaudaRadioButton(
                    element(
                        by.xpath(
                            '//label[text()="执行方式"]/../..//aui-radio-group'
                        )
                    )
                );
            case '仓库类型':
                return new AlaudaRadioButton(
                    element(
                        by.xpath(
                            '//label[text()="仓库类型"]/../..//aui-radio-group'
                        )
                    )
                );
            case 'Jenkinsfile 位置':
                return new AlaudaRadioButton();
            default:
                return this.alaudaElement.getElementByText(text, tagname);
        }
    }

    /**
     * 在文本框中输入值
     * 注意：如果右侧不是inputbox定位，子类需要重写此方法
     * @param name 文本框左侧的文字
     * @param value 输入文本框中的值
     */
    enterValue(name, value, tagname: string = 'input') {
        switch (name) {
            case '':
                this.getElementByText(name, tagname).clickByName(value);
                break;
            case '代码选择':
                const selectclick = new AlaudaButton(
                    $('.select .existed-repo__edit')
                );
                selectclick.click();
                break;
            case '镜像选择':
                const imageclick = new AlaudaButton(
                    element(
                        by.xpath(
                            '//label[text()=" 镜像仓库 "]/../..//div[@class="modify ng-star-inserted"]/aui-icon'
                        )
                    )
                );
                imageclick.click();
                break;
            case '更新应用镜像选择':
                const appimageclick = new AlaudaButton(
                    $('acl-image-pull-control aui-icon')
                );
                appimageclick.click();
                break;
            case '选择仓库确定':
                const okclick = new AlaudaButton(
                    $('.aui-dialog__footer .aui-button--primary')
                );
                okclick.click();
                break;
            case '选择镜像确定':
                const okclick1 = new AlaudaButton(
                    $('.aui-dialog__footer .aui-button--primary')
                );
                okclick1.click();
                break;
            case '选择代码方式':
                this.getElementByText('方式', 'aui-radio-button').clickByName(
                    value
                );
                break;
            case '选择仓库类型':
                this.getElementByText(
                    '仓库类型',
                    'aui-radio-button'
                ).clickByName(value);
                break;
            case '选择镜像方式':
                this.getElementByText('方式', 'aui-radio-button').clickByName(
                    value
                );
                break;
            case '执行方式':
                this.getElementByText(name, 'aui-radio-button').clickByName(
                    value
                );
                break;
            case 'Jenkinsfile 位置':
                this.getElementByText(name, tagname).clickByName(value);
                break;
            case '所属应用':
                const app = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "所属应用")]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                app.select(value);
                break;
            case '仓库凭据':
                const secret = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "凭据")]/../..//input'
                        )
                    ),
                    $$('.aui-option')
                );
                secret.select(value);
                break;
            case '镜像凭据':
                const secret1 = new AlaudaDropdown(
                    element(by.name('secret')),
                    $$('.aui-option')
                );
                secret1.select(value);
                break;
            case '镜像仓库-选择':
                const imagerepo_select = new AlaudaDropdown(
                    element(by.name('selectPath')),
                    $$('.cdk-overlay-pane .aui-option .image-name')
                );
                imagerepo_select.select(value);
                break;
            case '镜像仓库-标签':
                const imagerepo_tag = new AlaudaDropdown(
                    element(by.name('tag')),
                    $$('.cdk-overlay-pane .aui-option')
                );
                imagerepo_tag.select(value);
                break;
            case '源镜像仓库':
                const imagerepo1 = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[text()=" 源镜像仓库 "]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                imagerepo1.select(value);
                break;
            case '目标镜像仓库':
                const imagerepo2 = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[text()=" 目标镜像仓库 "]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                imagerepo2.select(value);
                break;
            case '项目':
                const project = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "项目")]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                project.select(value);
                break;
            case '应用名称':
                const appname = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "应用名称")]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                appname.select(value);
                break;
            case '容器名称':
                const container = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "容器名称")]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                container.select(value);
                break;
            case '代码仓库地址':
                const coderepoinput = new AlaudaInputbox(
                    element(
                        by.xpath(
                            '//label[contains(text(), "代码仓库地址")]/../..//input'
                        )
                    )
                );
                coderepoinput.input(value);
                break;
            case '代码仓库-选择':
                const coderepo = new AlaudaDropdown(
                    $('aui-dialog-content aui-select'),
                    $$('.repo-name')
                );
                coderepo.select(value);
                break;
            case '构建镜像标签':
                const taginput1 = new AlaudaInputbox(
                    element(
                        by.xpath(
                            '//div[text()=" Docker 构建 "]/..//label[text()=" 镜像标签 "]/../..//input'
                        )
                    )
                );
                taginput1.input(value);
                break;
            case '定时扫描':
                const triggerscan = new AlaudaButton(
                    element(
                        by.xpath(
                            '//div[text()=" 定时扫描 "]/../..//div/aui-checkbox'
                        )
                    )
                );
                triggerscan.click();
                const select_trigger = new AlaudaDropdown(
                    $('.card .aui-select'),
                    $$('.aui-option')
                );
                select_trigger.select(value);
                break;
            case '定时触发器':
                const trigger = new AlaudaButton(
                    element(
                        by.xpath(
                            '//div[text()=" 定时触发器 "]/../..//div[@class="checkbox"]/aui-checkbox'
                        )
                    )
                );
                trigger.click();
                const input_trigger = new AlaudaInputbox(
                    element(
                        by.xpath(
                            '//div[text()=" 定时触发器 "]/following-sibling::div//input'
                        )
                    )
                );
                input_trigger.input(value);
                break;
            case '输入分支':
                const inputbranch = new AlaudaDropdown(
                    element(
                        by.xpath(
                            '//label[contains(text(), "分支")]/../following-sibling::div'
                        )
                    ),
                    $$('.aui-option')
                );
                inputbranch.input(value);
                break;
            default:
                this.getElementByText(name, tagname).clear();
                this.getElementByText(name, tagname).sendKeys(value);
                browser.sleep(10);
                this.getElementByText(name, tagname);
                break;
        }
    }

    /**
     * 填写表单
     * @param data 测试数据 { 应用名称: 'qq', 镜像源证书: '不使用' }
     */
    fillForm(data) {
        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                this.enterValue(key, data[key]);
            }
        }
    }
}
