import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { DevopsPreparePage } from '@e2e/page_objects/devops/devops.prepare.page';

export class PrepareData extends DevopsPreparePage {
  delete(kind: string, name: string, ns: string) {
    CommonKubectl.execKubectlCommand(
      `kubectl delete ${kind} -n ${ns} ${name} `,
      this.clusterName,
    );
  }
}
