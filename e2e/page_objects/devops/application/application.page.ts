import { DevopsPageBase } from '../devops.page.base';

import { ApplicationYamlCreatePage } from './application_yaml_create.page';
import { ApplicationWorkloadCreateDialog } from './application_workload_create_dialog.page';
import { ApplicationListPage } from './application_list.page';
import { ApplicationDetailPage } from './application_detail.page';

import { PrepareData } from './prepare.page';

export class ApplicationPage extends DevopsPageBase {
  get listPage() {
    return new ApplicationListPage();
  }
  get detailPage() {
    return new ApplicationDetailPage();
  }
  get yamlCreatePage() {
    return new ApplicationYamlCreatePage();
  }

  get workloadCreateDialog() {
    return new ApplicationWorkloadCreateDialog();
  }

  get prepareData(): PrepareData {
    return new PrepareData();
  }
}
