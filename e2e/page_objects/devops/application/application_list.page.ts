import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { $, by, element } from 'protractor';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';
import { alauda_type_application } from '@e2e/utility/resource.type.k8s';

import { DevopsPageBase } from '../devops.page.base';

export class ApplicationListPage extends DevopsPageBase {
  get createApplicationButton() {
    return new AlaudaButton(element(by.buttonText('创建应用')));
  }
  get applicationListTable() {
    return $('.list-card-container');
  }

  get applicationPageTitle() {
    return new AlaudaText(by.css('aui-breadcrumb>.aui-breadcrumb'));
  }

  deleteApp(appName: string, nsName, region_name = ServerConf.REGIONNAME) {
    return CommonKubectl.deleteResource(
      alauda_type_application,
      appName,
      region_name,
      nsName,
    );
  }

  deleteWorkload(
    kind: string,
    name: string,
    nsName,
    region_name = ServerConf.REGIONNAME,
  ) {
    return CommonKubectl.deleteResource(kind, name, region_name, nsName);
  }

  getApplictionByName(name: string) {
    return $('.list-card-container')
      .$$('alo-application-list-card')
      .filter(elem => {
        return elem
          .$('.app-name > a')
          .getText()
          .then(appText => {
            return appText.trim() === name;
          });
      })
      .first();
  }
}
