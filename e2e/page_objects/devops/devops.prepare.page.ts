import { CommonKubectl } from '../../utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';
import { PageBase } from '../page.base';
import { CommonMethod } from '@e2e/utility/common.method';

export class DevopsPreparePage extends PageBase {
  constructor() {
    super();
  }
  isReady(): string {
    return this.globalfeatureIsEnabled('devops-app');
  }
  /**
   * 删保密字典
   * @param secretName 保密字典名称
   * @param namespace 命名空间名称
   * @param clusterName 集群名称
   */
  deleteSecret(
    secretName: string,
    namespace: string,
    clusterName: string = this.clusterName,
  ) {
    try {
      console.log(`删除${namespace} 下的保密字典${secretName} `);
      CommonKubectl.execKubectlCommand(
        `kubectl delete secret ${secretName} -n ${namespace}`,
        clusterName,
      );
    } catch (e) {
      console.log(e.message);
    }
  }

  createSecret(values: Record<string, any>) {
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      values,
      CommonMethod.random_generate_testData('secret'),
      'global',
    );
  }

  /**
   * 删除global-credentials 下保密字典
   * @param secretName 保密字典名称
   */
  deleteGlobalSecret(secretName) {
    const ns =
      ServerConf.GLOBAL_NAMESPCE === 'alauda-system'
        ? 'global-credentials'
        : `${ServerConf.GLOBAL_NAMESPCE}-global-credentials`;
    this.deleteSecret(secretName, ns, 'global');
  }
}
