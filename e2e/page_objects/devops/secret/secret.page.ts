/**
 * Created by zhangjiao on 2019/6/14.
 */
import { $, $$, browser, by, element } from 'protractor';

import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaConfirmDialog } from '../../../element_objects/alauda.confirmdialog';
import { AlaudaDropdown } from '../../../element_objects/alauda.dropdown';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaText } from '../../../element_objects/alauda.text';
import { CommonPage } from '../../../utility/common.page';
import { DevopsPageBase } from '../devops.page.base';

import { SecretCreatePage } from './create.page';

export class SecretPage extends DevopsPageBase {
  get createPage() {
    return new SecretCreatePage();
  }

  addSecrets(testData) {
    this.getButtonByText('添加凭据').click();
    const createButton = element(
      by.xpath(
        '//span[text()="创建凭据"]/../../../..//button[@aui-button="primary"]',
      ),
    );
    CommonPage.waitElementPresent(createButton);

    this.createPage.fillForm(testData);
    browser.sleep(500);
    createButton.click();
    // 等待创建成功的toast 提示消失
    this.createPage.toast.getMessage().then(message => {
      console.log(message);
    });
    browser.sleep(1000);
    CommonPage.waitElementNotPresent(createButton);
  }

  createSecrets(testData) {
    this.getButtonByText('创建凭据').click();
    const createButton = $(
      `.aui-card__footer button[class*='aui-button--primary']`,
    );
    CommonPage.waitElementPresent(createButton);

    this.createPage.fillForm(testData);
    createButton.click();
    CommonPage.waitElementNotPresent(createButton);
    this.toast.getMessage().then(message => {
      console.log(message);
    });
  }

  get updateDialog(): AlaudaConfirmDialog {
    return new AlaudaConfirmDialog(
      by.css('.aui-dialog'),
      '.aui-dialog__header div span',
      'button[aui-button=primary]',
      `button[aui-button='']`,
    );
  }

  // get secretTable(): AuiTableComponent {
  //     return new AuiTableComponent(
  //         $('.aui-layout__page .layout-page-content'),
  //         'aui-table',
  //         '.alo-search',
  //         'aui-table-cell',
  //         'aui-table-row'
  //     ); // 检索框的父元素;
  // }
  // -----------begin-----------secret list page
  navigationButton() {
    // return this.clickLeftNavByText('凭据', false);
    return this.clickLeftNavByText('凭据');
  }

  get listPage_createButton() {
    return this.getButtonByText('创建凭据');
  }

  listPage_SecretName(project_name, secretName) {
    return new AlaudaList(
      by.xpath(
        '//*[@href="/console-devops/admin/secrets/' +
          project_name +
          '/' +
          secretName +
          '"]',
      ),
    );
  }

  get confirmDialog_okButton() {
    return new AlaudaButton(
      $('aui-confirm-dialog .aui-confirm-dialog__confirm-button'),
    );
  }
  get listPage_getSecretTotal() {
    return new AlaudaText(by.css('.aui-paginator__total'));
  }

  get listPage_getSecretName() {
    return new AlaudaText(by.css('.name-cell a span'));
  }
  get listPage_nextPage_button() {
    return new AlaudaButton($('.aui-icon-angle_right'));
  }

  listPage_SecretOperate(SecretName, ns, operateName) {
    return new AlaudaList(
      by.xpath(
        '//a[@href="/console-devops/admin/secrets/' +
          ns +
          '/aaasdfsf' +
          SecretName +
          '"]',
      ),
      by.xpath(
        '//a[@href="/console-devops/admin/secrets/' +
          ns +
          '/aaasdfsf' +
          SecretName +
          '"]/../../../..//alo-menu-trigger',
      ),
      by.cssContainingText('.aui-menu-item', operateName),
      by.xpath('//button[@aui-button="error"]'),
    );
  }

  // -----------end-----------secret list page

  // -----------begin-----------Create secret page
  get createPage_headerTitle_Text() {
    return new AlaudaText(by.css('.aui-card__header span'));
  }

  get createPage_secretName_inputbox() {
    return new AlaudaInputbox(element(by.name('name')));
  }

  get createPage_project_dropdown() {
    return new AlaudaDropdown(element(by.name('namespace')), $$('.aui-option'));
  }

  get createPage_typeItem() {
    return element.all(by.css('.aui-radio-group .aui-radio-button'));
  }

  get createPage_createButton() {
    return this.getButtonByText('创建');
  }

  get createPage_cancelButton() {
    return this.getButtonByText('取消');
  }

  get createpage_input_content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '.base-header',
      '.aui-form-item__label-wrapper label',
      'input',
      '.aui-form-item__error',
    );
  }
  // -----------end-----------Create secret page

  // -----------begin-----------secret detail page
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.title span'),
      'label',
      'span',
    );
  }
  get detailPage_triggerButton() {
    return new AlaudaButton($('.alo-menu-trigger'));
  }
  // -----------end-----------secret detail page

  // -----------begin-----------secret update page
  updateDialog_SecretName(secretname) {
    return new AlaudaText(
      by.xpath(
        '//div[@class="aui-form-item__content"]/span[contains(text(), "' +
          secretname +
          '")]',
      ),
    );
  }
  get updatePage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-form .aui-form-item'),
      '',
      'label',
      '.aui-form-item__content .aui-form-item__control',
    );
  }
  get updatePage_updateButton() {
    return new AlaudaButton(
      element(by.xpath('//button[@aui-button="primary"]')),
    );
  }
  // -----------end-----------secret update page
}
