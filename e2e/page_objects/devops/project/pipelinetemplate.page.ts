/**
 * Created by jiaozhang on 2019/6/13.
 */
import { $, by, element } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';

import { AlaudaText } from '../../../element_objects/alauda.text';
import { DevopsPageBase } from '../devops.page.base';

import { CreatePage } from './create.page';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';

export class PipelineTemplate extends DevopsPageBase {
  get createPage() {
    return new CreatePage();
  }
  get detailTab_configButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-button__content', '配置模版仓库')),
    );
  }
  get detailTab_syncing() {
    return new AlaudaButton(
      element(by.xpath('//span[contains(text(), "模版仓库同步中")]')),
    );
  }
  get detailTab_operateButton() {
    return new AlaudaButton(element(by.tagName('alo-menu-trigger')));
  }
  get detailTab_syncButton() {
    return new AlaudaButton(
      element(by.cssContainingText('.aui-menu-item', '同步模版仓库')),
    );
  }
  get detailTab_configButton_oprate() {
    return new AlaudaText(
      by.cssContainingText('.aui-menu-item', '配置模版仓库'),
    );
  }
  get configDialog_title() {
    return new AlaudaText(
      by.cssContainingText('.aui-dialog__header-title', '配置模版仓库'),
    );
  }

  configDialog_inputbox(testData) {
    this.createPage.fillForm(testData);
    this.configDialog_confirmButton.click();
    this.waitProgressBarNotPresent();
  }

  configDialog_selectType(typename) {
    this.createPage.enterValue('方式', typename);
  }
  get configDialog_secret() {
    return new AuiSelect($('aui-dialog aui-select'));
  }

  get configDialog_secretInput() {
    return new AlaudaText(by.css('aui-select .aui-select__label'));
  }

  confirmDialog_type(value) {
    return new AlaudaButton(
      element(by.xpath(`//span[normalize-space(text())="${value}"]/../..`)),
    );
  }
  get configDialog_repo() {
    return element(by.css('.aui-dialog__content input[disabled]'));
  }
  get configDialog_confirmButton() {
    return new AlaudaText(by.cssContainingText('.aui-button__content', '确定'));
  }

  get configDialog_cancelButton() {
    return new AlaudaText(by.cssContainingText('.aui-button__content', '取消'));
  }

  get detailTab_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .field'),
      '.base-header',
      'label',
      'span',
    );
  }

  get detailTab_successNum() {
    return new AlaudaText(by.xpath('//span[@class="color--success"]'));
  }
  get detailTab_failureNum() {
    return new AlaudaText(by.xpath('//span[@class="color--failure"]'));
  }
  get detailTab_skipNum() {
    return new AlaudaText(by.xpath('//span[@class="color--skip"]'));
  }
  get detailTab_resultLink() {
    return new AlaudaButton($('span .sync-result'));
  }

  get resultDialog_title() {
    return new AlaudaText(by.cssContainingText('.title', '同步结果'));
  }
  get resultDialog_close() {
    return new AlaudaButton($('.aui-dialog__header-close'));
  }
  detailTab_templateSourceButton(type) {
    return new AlaudaButton(
      element(
        by.xpath(
          '//div[@class="header-action"]//button[contains(text(), "' +
            type +
            '")]',
        ),
      ),
    );
  }

  get detailTab_templateSourceText() {
    return new AlaudaText(by.css('.header-action .isSelected'));
  }

  detailTab_templateName(name) {
    return new AlaudaButton(
      element(
        by.xpath(
          `//alo-pipeline-template-list-card//div[@class="name-wrapper"]//a[contains(text(),"${name}")]`,
        ),
      ),
    );
  }
  /**
   * 无项目自定义模版
   */
  get noData() {
    return new AlaudaText(by.css('alo-no-data span'));
  }
  get detailDialog_title() {
    return new AlaudaText(by.css('.aui-dialog__header-title .name'));
  }
  get detailDialog_close() {
    return new AlaudaButton($('.aui-icon-close'));
  }

  get templateTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('alo-pipeline-template-list-container .aui-card'),
      'alo-pipeline-template-list',
      'alo-pipeline-template-list-container .header',
      '.card>div span',
      '.card',
    ); // 检索框的父元素);
  }

  get templateTableofficial(): AuiTableComponent {
    return new AuiTableComponent(
      $('.aui-page__content .aui-card'),
      'alo-pipeline-template-list',
      '.aui-page__content .list-header',
      '.card>div span',
      '.card',
    ); // 检索框的父元素);
  }
}
