/**
 * Created by zhangjiao on 2019/6/13.
 */
import { $, by, element } from 'protractor';

import { AuiTableComponent } from '../../../component/aui_table.component';
import { AlaudaBasicInfo } from '../../../element_objects/alauda.basicinfo';
import { AlaudaButton } from '../../../element_objects/alauda.button';
import { AlaudaInputbox } from '../../../element_objects/alauda.inputbox';
import { AlaudaList } from '../../../element_objects/alauda.list';
import { AlaudaTabItem } from '../../../element_objects/alauda.tabitem';
import { DevopsPageBase } from '../devops.page.base';

export class ProjectPage extends DevopsPageBase {
  getLanguage() {
    return element(by.css('.alo-switch-language')).getText();
  }
  navigationButton() {
    return this.clickLeftNavByText('项目');
  }
  /**
   * 进入到项目详情页
   * @param projectName 项目名称
   */
  enterProject(projectName: string) {
    this.searchPrject(projectName);
    this.projectTable.clickResourceNameByRow([projectName]);
  }
  /**
   * 检索项目
   * @param projectName 项目名称
   */
  searchPrject(projectName: string) {
    this.projectTable.searchByResourceName(projectName);
  }

  get projectTable(): AuiTableComponent {
    return new AuiTableComponent(
      $('.aui-page__content .layout-page-content'),
      'alo-project-list',
      '.list-header .alo-search',
      '.project-card>div span',
      '.project-card',
    ); // 检索框的父元素);
  }

  listPage_projectName(projectName) {
    return new AlaudaList(
      by.xpath(
        '//*[@href="/console-devops/admin/projects/' + projectName + '"]',
      ),
    );
  }
  get listPage_nameFilter_input() {
    return new AlaudaInputbox($('input[placeholder="按名称搜索"]'));
  }
  get detailPage_Content() {
    return new AlaudaBasicInfo(
      by.css('.aui-card__content .alo-detail__field'),
      by.css('.base-header div span'),
      'label',
      'span',
    );
  }
  get listPage_nextPage_button() {
    return new AlaudaButton($('.aui-icon-angle_right'));
  }
  // -----------begin-----------project detail page
  detailPage_TabItem(tabName) {
    return new AlaudaTabItem(
      by.xpath('//*[@class="aui-tab-header__labels"]/div/div'),
      by.xpath('//div[@class="isActive aui-tab-label ng-star-inserted"]/div'),
      by.xpath(
        '//*[@class="aui-tab-header__labels"]/div/div[contains(text(), "' +
          tabName +
          '")]',
      ),
    );
  }
  get detailPage_bindButton() {
    return this.getButtonByText('绑定');
  }
  get detailPage_Breadcrumb() {
    return new AlaudaButton(
      element(by.xpath('//a[@href="/console-devops/admin/projects"]')),
    );
  }
}
