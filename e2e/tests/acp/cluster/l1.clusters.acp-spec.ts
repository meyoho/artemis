// // import { CreateClusterPage } from "../../../page_objects/acp/cluster/cluster_create.page"
// import { ServerConf } from '@e2e/config/serverConf';
// import { ClusterPage } from '@e2e/page_objects/acp/cluster/cluster.page';
// import { CommonKubectl } from '@e2e/utility/common.kubectl';
// import { browser } from 'protractor';

// describe('cluster自动化测试', () => {
//     const page = new ClusterPage();
//     const cluster_name = page.getTestData('cluster');
//     const tokenName = 'accesstoken';
//     const CIDR = '10.33.0.0/16';
//     const cluster_manage_endpoint = 'https://127.0.0.1';
//     const master_ip = '127.0.0.1';
//     const cluster_endpoint = '127.0.0.2';

//     beforeAll(() => {
//         page.login();
//         page.enterOperationView();
//         CommonKubectl.execKubectlCommand(
//             `kubectl delete secret -n alauda-system ${tokenName}`
//         );
//     });
//     afterAll(() => {
//         CommonKubectl.execKubectlCommand(
//             `kubectl delete secret -n alauda-system ${tokenName}`
//         );
//     });
//     it('ACP2UI-53340 : 接入集群-正常跳转页面-验证面包屑显示/跳转', () => {
//         //多集群管理页点击接入集群
//         page.getButtonByText('接入集群').click();
//         // page.accessPageVerify.verify({ 面包屑: '集群/集群/接入' });
//         page.accessPageVerify.verify({
//             警示信息:
//                 '请严格按照如下参数调整 K8S API Server 相关配置信息，否则可能会遇到权限不同步等问题，导致您无法正常使用平台。'
//         });
//         page.accessPageVerify.verify({
//             警示信息: '--oidc-issuer-url=https://'
//         });
//         page.accessPageVerify.verify({
//             警示信息: '--oidc-client-id=alauda-auth；OIDC 客户端的唯一标识（ID'
//         });
//         page.accessPageVerify.verify({
//             警示信息: '--oidc-ca-file=/etc/kubernetes/pki/apiserver-dex-ca.crt'
//         });
//         page.accessPageVerify.verify({
//             警示信息: '--oidc-username-claim=email'
//         });
//         page.accessPageVerify.verify({
//             警示信息: '--oidc-groups-claim=groups'
//         });

//         page.getButtonByText('接入').click();
//         page.accessPageVerify.verify({ 必填项: 4 });
//     });
//     it('ACP2UI-3899 : 参数校验-全部参数-接入-二次确认提示', () => {
//         //接入集群页填写参数
//         const testData = {
//             集群名称: cluster_name,
//             显示名称: cluster_name,
//             集群地址: '1.0.0.80:6443',
//             token: { 名称: tokenName, Token: 'token' },
//             CIDR: CIDR,
//             集群管理地址: cluster_manage_endpoint
//         };
//         page.accessPage.fillForm(testData);
//         page.getButtonByText('接入').click();
//         const expectData = {
//             标题: `确定要接入“${cluster_name}”集群吗？`,
//             内容:
//                 '请再次确认您已按照页面的提示信息调整 K8S API Server 相关配置，如果配置信息有误可能会导致您接入的集群无法正常使用。'
//         };
//         page.accessPageVerify.verify(expectData);

//         page.confirmDialog.clickCancel();
//         expect(page.confirmDialog.isPresent()).toBeFalsy();
//     });
//     it('ACP2UI-53340 : 创建集群-正常跳转页面-验证面包屑显示/跳转', () => {
//         page.clickLeftNavByText('集群');
//         //多集群管理页点击创建集群
//         page.getButtonByText('创建集群').click();
//         expect(page.breadcrumb.getText()).toContain('集群/创建');
//     });
//     it('ACP2UI-53255 : 填写全部参数-创建集群-给出命令', () => {
//         //创建集群页填写参数
//         const testData = {
//             集群名称: cluster_name,
//             显示名称: cluster_name,
//             集群地址: cluster_endpoint,
//             集群类型: '单点集群',
//             控制节点: master_ip,

//             认证信息: {
//                 类型: '密码',
//                 用户名: 'root',
//                 密码: '07Apples',
//                 端口: '22'
//             },
//             网络模式: 'Flannel Vxlan',
//             CIDR: CIDR
//         };
//         page.createPage.createCluster(testData);
//         browser.sleep(3000);
//         page.scriptInfoDialog
//             .getElementByText('脚本')
//             .getText()
//             .then(text => {
//                 console.log(text);
//             });

//         const expectData = {
//             脚本: {
//                 '--region-nam': cluster_name,
//                 '--ssh-type': '=password',
//                 '--apiserver': `https://${cluster_endpoint}`,
//                 '--download-url': 'console-download-server',
//                 '--ssh-username': 'root',
//                 '--ssh-password': '07Apples',
//                 '--ssh-port': '22',
//                 '--network': 'none'
//             }
//         };

//         page.scriptInfoDialogVerify.verify(expectData);
//         page.scriptInfoDialog.clickCancel();
//         // page.breadcrumb.click('集群');
//     });
//     it('ACP2UI-53358: 集群基本信息验证展示', () => {
//         page.listPage.viewCluster(ServerConf.REGIONNAME);
//         //验证集群信息
//         expect(page.detailPage.name).toBe(ServerConf.REGIONNAME + '\n操作');
//         expect(page.detailPage.getDetail('集群地址')).toContain('6443');
//         expect(page.detailPage.getDetail('CIDR')).toBe(CIDR);
//     });
//     // TO DO: 节点停止调度这个case，影响比较大，节点名称也不能hardcode
//     // it('ACP2UI-53361: 集群详情页页面布局-主机列表展示-更新标签验证', () => {
//     //     const testData = new Map<string, Array<string>>();
//     //     testData.set('键', ['label']);
//     //     testData.set('值', ['testlabel']);
//     //     cluster_detail_page.updateNodeLabel(testData, 'access2');
//     // });
//     // it('ACP2UI-53362:集群详情页页面布局-主机列表展示-调度/不可调度', () => {
//     //     cluster_detail_page.updateCordon('停止调度', 'access2');
//     //     // cluster_detail_page.nodeTable.getCell('可调度',['access2']).then(elem=>{
//     //     //     expect(elem.getText()).toContain('否')
//     //     // })
//     //     // cluster_detail_page.updateCordon('开始调度','access2');
//     // });
// });
