// import { ClusterPage } from '@e2e/page_objects/acp/cluster/cluster.page';
// import { CommonKubectl } from '@e2e/utility/common.kubectl';
// import { CommonMethod } from '@e2e/utility/common.method';

// describe('集群 L0自动化测试', () => {
//     const page = new ClusterPage();
//     const cluster_name = page.getTestData('cluster');
//     const tokenName = 'accesstoken';
//     const master_ip = '127.0.0.2';

//     function getIssueUrl() {
//         const data = CommonMethod.parseYaml(
//             CommonKubectl.execKubectlCommand(
//                 'kubectl get configmap -n alauda-system dex-configmap -o yaml'
//             )
//         );

//         return CommonMethod.parseYaml(String(data.data['config.yaml'])).issuer;
//     }

//     beforeAll(() => {
//         page.login();
//         page.enterOperationView();
//     });
//     afterAll(() => {
//         CommonKubectl.execKubectlCommand(
//             `kubectl delete secret -n alauda-system ${tokenName}`
//         );
//     });

//     it('ACP2UI-53255 : 填写全部参数-创建集群-给出命令', () => {
//         // 创建集群页填写参数;
//         const testData = {
//             集群名称: cluster_name,
//             显示名称: cluster_name,
//             集群地址: master_ip,
//             集群类型: '单点集群',
//             控制节点: master_ip,
//             认证信息: {
//                 类型: '密码',
//                 用户名: 'root',
//                 密码: '07Apples',
//                 端口: '22'
//             },
//             网络模式: 'Flannel Vxlan'
//         };
//         page.createPage.createCluster(testData);
//         const expectData = {
//             脚本: {
//                 '-kfsSL':
//                     'console-download-server/setup > /tmp/dd.sh ;sudo bash /tmp/dd.sh',
//                 '--region-name': master_ip,
//                 '--ssh-type': '=password --insecure-registry-list',
//                 '--apiserver': `https://${master_ip}`,
//                 '--download-url': 'console-download-server',
//                 '--ssh-username': 'root',
//                 '--ssh-password': '07Apples',
//                 '--ssh-port': '22'
//             }
//         };

//         page.scriptInfoDialogVerify.verify(expectData);
//     });
//     it('ACP2UI-53340 : 接入集群-正常跳转页面-验证面包屑显示/跳转', () => {
//         //多集群管理页点击接入集群
//         page.createPage.clickLeftNav();
//         page.getButtonByText('接入集群').click();
//         const expectData = {
//             警示信息: {
//                 信息:
//                     '请严格按照如下参数调整 K8S API Server 相关配置信息，否则可能会遇到权限不同步等问题，导致您无法正常使用平台。',
//                 '--oidc-issuer-url': `--oidc-issuer-url=${getIssueUrl()}`,
//                 '--oidc-client-id':
//                     '--oidc-client-id=alauda-auth；OIDC 客户端的唯一标识',
//                 '--oidc-ca-file':
//                     '--oidc-ca-file=/etc/kubernetes/pki/apiserver-dex-ca.crt',
//                 '--oidc-username-claim': '--oidc-username-claim=email',
//                 '--oidc-groups-claim': '--oidc-groups-claim=groups'
//             }
//             // 面包屑: '集群/集群/接入'
//         };
//         page.accessPageVerify.verify(expectData);

//         page.getButtonByText('接入').click();
//         page.accessPageVerify.verify({ 必填项: 4 });
//     });
// });
