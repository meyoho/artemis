import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const cm_prepare = new ConfigMapPrepareData();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56041');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  const conifgmap_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
    cm_prepare.create(conifgmap_data);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
  });
  it('ACP2UI-56041 : 配置引用，点击更新，弹出更新配置引用页面', () => {
    const testData = {
      配置引用: [['配置字典', tapp_name]],
    };
    page.detailPage.updateConfig(tapp_name, testData);
    const expectHpaData = {
      配置引用: ['类型', '名称', '配置字典', tapp_name],
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
