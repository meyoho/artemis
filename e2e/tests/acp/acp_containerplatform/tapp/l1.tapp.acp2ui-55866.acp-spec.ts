import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55866');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55866 : tapp详情页->点击更新->自动扩缩容详情页点击更新按钮->点击更新->选择指标调节->输入最大最小实例数和cpu阈值->点击更新->能成功保存', () => {
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 5,
        'CPU 利用率': 30,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率30%最大实例数5',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
