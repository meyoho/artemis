import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-57055');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();

  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-57055 : 创建Tapp->输入镜像->输入名称->展开容器组-高级->端口，点击添加->选择udp->输入容器端口(如80)->输入主机空闲端口(如8000)->点击确定->创建成功，端口映射成功,协议为udp', () => {
    const testData = {
      组件名称: tapp_name,
      端口: [['TCP', '80', '50055'], ['UDP', '81', '60055']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      YAML: {
        spec: {
          template: {
            spec: {
              containers: [
                {
                  ports: [
                    {
                      hostPort: 50055,
                      containerPort: 80,
                      protocol: 'TCP',
                    },
                    {
                      hostPort: 60055,
                      containerPort: 81,
                      protocol: 'UDP',
                    },
                  ],
                },
              ],
            },
          },
        },
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
