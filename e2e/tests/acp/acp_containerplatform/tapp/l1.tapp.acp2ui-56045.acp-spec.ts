import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
import { browser } from 'protractor';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56045');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-56045 : 详情-容器组-点击刷新按钮-页面刷新', () => {
    page.listPage.toDetail(tapp_name).then(() => {
      page.detailPage.clickTab('容器组');
      page.detailPage.getPodNameByRowIndex(0).then(pod_name => {
        browser.sleep(1).then(() => {
          page.preparePage.deletePodByApi(
            tapp_name,
            pod_name,
            namespace_name,
            page.clusterName,
          );
        });
        page.detailPage.waitPodNotExist(pod_name);
        page.detailPage.refreshPodList().then(() => {
          const v_data = {
            容器组列表: {
              不存在: pod_name,
            },
          };
          page.detailPageVerify.verify(v_data);
        });
      });
    });
  });
});
