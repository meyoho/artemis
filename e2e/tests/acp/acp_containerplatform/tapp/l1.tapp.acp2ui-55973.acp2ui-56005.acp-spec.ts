import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55973');
  const cm_prepare = new ConfigMapPrepareData();
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const conifgmap_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
    cm_prepare.create(conifgmap_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
  });
  it('ACP2UI-55973 : 创建Tapp-输入镜像-名称-异常策略迁移-添加存储卷-类型配置字典-存储卷挂载-添加配置字典-日志文件-排除日志文件-添加可用性健康检查-EXEC-组件标签->点击创建 |ACP2UI-56005 : 详情信息-存储卷，亲和性，已挂载存储卷，健康检查，日志文件（没有时则不显示', () => {
    const testData = {
      组件名称: tapp_name,
      节点异常策略: '迁移',
      Tapp组件标签: [['lkey', 'lvalue']],
      存储卷: [
        {
          名称: 'cm-volume',
          类型: '配置字典',
          配置字典: conifgmap_data.name,
        },
      ],
      添加可用性健康检查: { 协议类型: 'EXEC', 启动命令: [['ls']] },
      存储卷挂载: [
        [['配置字典', 'cm-volume'], 'key', '/test/cmsub'],
        [['配置字典', 'cm-volume'], '', '/test/cmall', true],
      ],
      日志文件: [['/var/*.*']],
      排除日志文件: [['/var/hehe.txt']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      节点异常策略: '迁移',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      已挂载存储卷: [
        '存储卷名称',
        '子路径',
        '挂载路径',
        '只读',
        'cm-volume',
        'key',
        '/test/cmsub',
        '/test/cmall',
        '是',
      ],
      可用性健康检查: {
        健康检查类型: '可用性',
        协议类型: 'EXEC',
        启动时间: '300',
        间隔: '60',
        超时时长: '30',
        正常阈值: '0',
        不正常阈值: '5',
        启动命令: 'ls',
      },
      日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
      YAML: {
        metadata: {
          labels: {
            lkey: 'lvalue',
          },
        },
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
