import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55991');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55991 : 创建Tapp-YAML-输入YAML内容-点击取消', () => {
    const yaml = CommonMethod.readTemplateFile('alauda.tapp.tpl.yaml', {
      name: tapp_name,
      namespace: namespace_name,
      image: imageAddress,
    });
    page.createPage.createByYaml(yaml, 'cancel');
    const expectDetailData = {
      标题: `计算/Tapp`,
      数量: 0,
    };
    page.listPage.search(tapp_name, 0);
    page.listPageVerify.verify(expectDetailData);
  });
});
