import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56026');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-56026 : 详情信息-点击日志-页面跳转到日志tab', () => {
    page.preparePage.waitTappPodRunning(tapp_name).then(isRunning => {
      if (isRunning) {
        page.listPage.enterDetail(tapp_name);

        const expectDetailData = {
          日志: 'log',
        };
        page.detailPageVerify.verify(expectDetailData);
      } else {
        expect('Tapp Pod 没有Running').toBe('isRunning');
      }
    });
  });
});
