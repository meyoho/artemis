import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55993');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55993 : 更新Tapp-更新内容-点击取消/点击面包屑', () => {
    const testData = {
      显示名称: tapp_name.toUpperCase(),
    };
    page.listPage.update(tapp_name, testData, '取消');
    const expectDetailData_1 = {
      标题: `计算/Tapp`,
      不存在行: [tapp_name.toLocaleUpperCase()],
    };
    page.listPageVerify.verify(expectDetailData_1);
    page.listPage.update(tapp_name, testData, '面包屑');
    page.listPageVerify.verify(expectDetailData_1);
  });
});
