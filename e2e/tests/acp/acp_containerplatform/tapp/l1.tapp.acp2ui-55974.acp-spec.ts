import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55974');
  const secret_prepare = new PrepareData();
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const secret_data = {
    name: tapp_name,
    namespace: namespace_name,
    datatype: 'kubernetes.io/dockerconfigjson',
    datas: {
      '.dockerconfigjson': `{"auths":{"index.alauda.cn":{"username":"test","password":"test","email":"test@alauda.io"}}}`,
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
    secret_prepare.create(secret_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
  });
  it('ACP2UI-55974 : 创建Tapp-输入镜像-名称-添加存储卷-类型保密字典-存储卷挂载-添加保密字典-添加存活性健康检查-HTTP-启动命令和参数-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      存储卷: [
        {
          名称: 'secret-volume',
          类型: '保密字典',
          保密字典: tapp_name,
        },
      ],
      启动命令: [['/bin/sh']],
      参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
      添加存活性健康检查: { 协议类型: 'HTTP' },
      存储卷挂载: [
        [['保密字典', 'secret-volume'], '.dockerconfigjson', '/test/secretsub'],
        [['保密字典', 'secret-volume'], '', '/test/secretall'],
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['/bin/sh'],
      参数: ['-c', 'while true; do sleep 10; echo 123; done'],
      已挂载存储卷: [
        '存储卷名称',
        '子路径',
        '挂载路径',
        '只读',
        'secret-volume',
        '.dockerconfigjson',
        'secret-volume',
        '/test/secretsub',
        '/test/secretall',
      ],
      存活性性健康检查: {
        健康检查类型: '存活性',
        协议类型: 'HTTP',
        启动时间: '300',
        间隔: '60',
        超时时长: '30',
        正常阈值: '1',
        不正常阈值: '5',
        协议: 'HTTP',
        端口: 80,
        路径: '/',
        请求头: '无请求头',
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
