import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55852');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  const hpav2_is_enabled = page.featureIsEnabled('hpav2');
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55852 : tapp详情页->点击更新->选择定时调节->触发规则选择周一周二的12:00->目标实例输入0->点击更新->能正常保存|ACP2UI-55867 : tapp详情页->点击更新->自动扩缩容详情页点击更新按钮->点击更新->选择定时调节->输入目标实例数->点击更新->能成功保存', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['时间', ['星期一,星期二', '12:00'], 0]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '星期一、星期二12:00触发0',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55863 : 已存在一个定时调节hpa->点击更新->选择指标调节->输入最大实例输5，最小实例数1，cpu阈值30->点击更新->保存成功，原来的hpa被删除，新hpa创建成功', () => {
    if (!cronhpa_is_enabled || hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 5,
        'CPU 利用率': 30,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率30%最大实例数5',
    };
    page.detailPageVerify.verify(expectHpaData);
    browser.sleep(1).then(() => {
      const reg = new RegExp('^(E|e)rror.?');
      expect(
        reg.test(
          CommonKubectl.execKubectlCommand(
            `kubectl get cronhpa -n ${namespace_name} tapp-${tapp_name}`,
            page.clusterName,
          ),
        ),
      ).toBe(true);
    });
  });
  it('ACP2UI-55862 : 已存在一个指标调节hpa->点击更新->选择定时调节->输入目标实例数3->点击更新->保存成功，原来的hpa被删除，新hpa创建成功 ', () => {
    if (!cronhpa_is_enabled || hpav2_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['', ['每天', '00:00'], 3]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '每天00:00触发3',
    };
    page.detailPageVerify.verify(expectHpaData);
    browser.sleep(1).then(() => {
      const reg = new RegExp('^(E|e)rror.?');
      expect(
        reg.test(
          CommonKubectl.execKubectlCommand(
            `kubectl get hpa -n ${namespace_name} tapp-${tapp_name}`,
            page.clusterName,
          ),
        ),
      ).toBe(true);
    });
  });
});
