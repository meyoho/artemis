import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

/**
 * 因http://jira.alauda.cn/browse/DEV-21290 bug先注释
 */
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55966');
  const namespace_name = page.calicoNamespaceName;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled(
    page.calicoClusterName,
  );
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: `${tapp_name}-a`,
    namespace: namespace_name,
    project: page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(
      tapp_name,
      namespace_name,
      page.calicoClusterName,
    );
    page.preparePage.deleteTapp(
      tapp_data.name,
      namespace_name,
      page.calicoClusterName,
    );
    page.preparePage.createTapp(tapp_data, page.calicoClusterName);
    page.login();
    page.enterUserView(
      namespace_name,
      page.projectName,
      page.calicoClusterName,
    );
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  page.preparePage.deleteTapp(
    tapp_name,
    namespace_name,
    page.calicoClusterName,
  );
  page.preparePage.deleteTapp(
    tapp_data.name,
    namespace_name,
    page.calicoClusterName,
  );
  it('ACP2UI-55966 : 创建Tapp-输入镜像-名称-异常策略迁移-host模式关闭(calico网络模式)-pod亲和-方式高级-选择类型preferred-添加容器-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      亲和性: [
        {
          亲和性: 'Pod 亲和',
          方式: '高级',
          亲和类型: 'Preferred',
          匹配标签: [
            [
              `service.${ServerConf.LABELBASEDOMAIN}/name`,
              `tapp-${tapp_data.name}`,
            ],
          ],
        },
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      'Pod 亲和': [
        'Pod 亲和',
        'Preferred',
        '1',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: tapp-${tapp_name}`,
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
