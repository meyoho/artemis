import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56044');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-56044 : 详情-容器组-信息显示', () => {
    page.listPage.toDetail(tapp_name).then(() => {
      const v_data_1 = {
        容器组列表: {
          表头: [
            '名称',
            '镜像',
            '状态',
            '资源限制',
            '重启次数',
            '容器组 IP',
            '节点名称',
            '创建时间',
            '',
          ],
        },
      };
      page.detailPageVerify.verify(v_data_1);

      page.detailPage.getPodNameByRowIndex(0).then(pod_name => {
        const v_data_2 = {
          容器组列表: {
            操作项: {
              podName: pod_name,
              verify_data: ['查看日志', 'EXEC', '灰度发布', '删除', '禁用'],
            },
          },
        };
        page.detailPageVerify.verify(v_data_2);
      });
    });
  });
});
