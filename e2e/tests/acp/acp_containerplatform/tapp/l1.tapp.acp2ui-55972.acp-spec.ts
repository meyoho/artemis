import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PreparePage as PVCPreparePage } from '@e2e/page_objects/acp/container/storage/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const storage_prepare = new PVCPreparePage();
  const tapp_name = page.getTestData('acp2ui-55972');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const pvc_data = {
    name: tapp_name,
    namespace: namespace_name,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    storage_prepare.deletePvc(tapp_name, namespace_name);
    storage_prepare.createPvcByYaml(pvc_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    storage_prepare.deletePvc(tapp_name, namespace_name);
  });
  it('ACP2UI-55972 : 创建Tapp-输入镜像-名称-实例(2)-主机选择器选择多个-添加存储卷-类型持久卷声明-存储卷挂载-添加持久卷声明-添加可用性健康检查-TCP-更新策略->点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      实例数量: '2',
      Tapp更新策略: ['2'],
      存储卷: [
        {
          名称: 'pvc-volume',
          持久卷声明: tapp_name,
        },
      ],
      添加可用性健康检查: { 协议类型: 'TCP' },
      存储卷挂载: [[['持久卷声明', 'pvc-volume'], 'pvcdata', '/test/pvc']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 2',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      已挂载存储卷: [
        '存储卷名称',
        '子路径',
        '挂载路径',
        '只读',
        'pvc-volume',
        'pvcdata',
        '/test/pvc',
      ],
      可用性健康检查: {
        健康检查类型: '可用性',
        协议类型: 'TCP',
        启动时间: 300,
        间隔: 60,
        超时时长: 30,
        正常阈值: 0,
        不正常阈值: 5,
        端口: 80,
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
