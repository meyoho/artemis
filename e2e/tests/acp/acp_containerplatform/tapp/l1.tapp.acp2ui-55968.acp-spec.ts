import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const secret_prepare = new PrepareData();
  const tapp_name = page.getTestData('acp2ui-55968');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const secret_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  const tapp_data = {
    name: `${tapp_name}-a`,
    namespace: namespace_name,
    project: page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
    page.preparePage.deleteTapp(tapp_data.name);
    secret_prepare.create(secret_data);
    page.preparePage.createTapp(tapp_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_data.name);
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
  });
  it('ACP2UI-55968 : 创建Tapp-输入镜像-异常策略迁移-host模式关闭(galaxy网络模式)-pod反亲和-方式高级-选择类型required-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      节点异常策略: '迁移',
      配置引用: [['保密字典', tapp_name]],
      亲和性: [
        {
          亲和性: 'Pod 反亲和',
          方式: '高级',
          匹配标签: [
            [
              `service.${ServerConf.LABELBASEDOMAIN}/name`,
              `tapp-${tapp_data.name}`,
            ],
          ],
        },
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      节点异常策略: '迁移',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      'Pod 亲和': [
        'Pod 反亲和',
        'Required',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: tapp-${tapp_name}`,
      ],
      配置引用: ['类型', '名称', '保密字典', tapp_name],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
