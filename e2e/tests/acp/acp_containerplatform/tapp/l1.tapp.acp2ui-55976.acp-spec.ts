import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55976');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55976 : 创建Tapp-输入镜像-名称-添加存储卷-类型空目录-存储卷挂载-添加空目录-添加存活性健康检查-EXEC-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      节点异常策略: '迁移',
      存储卷: [
        {
          名称: 'empty-volume',
          类型: '空目录',
        },
      ],
      添加存活性健康检查: { 协议类型: 'EXEC', 启动命令: [['ls']] },
      存储卷挂载: [[['空目录', 'empty-volume'], '', '/test/empty']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      节点异常策略: '迁移',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      已挂载存储卷: [
        '存储卷名称',
        '子路径',
        '挂载路径',
        '只读',
        'empty-volume',
        '/test/empty',
      ],
      存活性健康检查: {
        健康检查类型: '存活性',
        协议类型: 'EXEC',
        启动时间: '300',
        间隔: '60',
        超时时长: '30',
        正常阈值: '1',
        不正常阈值: '5',
        启动命令: 'ls',
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
