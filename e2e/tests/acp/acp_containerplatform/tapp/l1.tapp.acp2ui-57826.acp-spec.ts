import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const secret_prepare = new PrepareData();
  const cm_prepare = new ConfigMapPrepareData();
  const tapp_name = page.getTestData('acp2ui-57826');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();

  if (!tapp_is_enabled) {
    return;
  }
  const secret_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      skey: 'svalue',
    },
  };
  const conifgmap_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      ckey: 'cvalue',
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(conifgmap_data.name);
    secret_prepare.delete(secret_data.name);
    cm_prepare.create(conifgmap_data);
    secret_prepare.create(secret_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(conifgmap_data.name);
    secret_prepare.delete(secret_data.name);
  });
  it('ACP2UI-57826 : 创建Tapp-输入镜像-名称-环境变量-添加引用-选择配置字典和保密字典-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      环境变量添加引用: [
        ['cm_key', ['配置字典', conifgmap_data.name], 'ckey'],
        ['sec_key', ['保密字典', secret_data.name], 'skey'],
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      环境变量: [
        'cm_key',
        `配置字典 ${conifgmap_data.name} : ckey`,
        'sec_key',
        `保密字典 ${secret_data.name} : skey`,
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
