import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56091');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
    replicas: 2,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-56091 : 详情-容器组-选择容器组-点击灰度发布-弹出灰度发布对话框-点击选择镜像按钮-弹出选择镜像对话框-输入镜像-点击取消', () => {
    page.listPage.toDetail(tapp_name).then(() => {
      page.detailPage.clickTab('容器组');
      page.detailPage.getPodNameByRowIndex(1).then(pod_name => {
        const data = {};
        data[tapp_name] = {
          镜像地址: `${imageAddress}_test`,
        };
        page.detailPage.blueGreen(pod_name, data, '取消').then(() => {
          const v_data = {
            容器组列表: {
              YAML: {},
            },
          };
          v_data['容器组列表']['YAML'][pod_name] = {
            apiVersion: 'v1',
            kind: 'Pod',
            metadata: {
              name: pod_name,
              namespace: namespace_name,
              ownerReferences: [
                {
                  apiVersion: 'apps.tkestack.io/v1',
                  controller: true,
                  kind: 'TApp',
                  name: tapp_name,
                },
              ],
            },
            spec: {
              containers: [
                {
                  image: `${imageAddress}`,
                  name: tapp_name,
                  resources: {
                    limits: {
                      cpu: '15m',
                      memory: '15Mi',
                    },
                  },
                },
              ],
            },
          };
          page.detailPageVerify.verify(v_data);
        });
      });
    });
  });
});
