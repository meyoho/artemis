import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const secret_prepare = new PrepareData();
  const cm_prepare = new ConfigMapPrepareData();
  const tapp_name = page.getTestData('acp2ui-55964');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const secret_data = {
    name: tapp_name,
    namespace: namespace_name,
    datatype: 'kubernetes.io/dockerconfigjson',
    datas: {
      '.dockerconfigjson': `{"auths":{"index.alauda.cn":{"username":"test","password":"test","email":"test@alauda.io"}}}`,
    },
  };
  const tapp_data = {
    name: `${tapp_name}-a`,
    namespace: namespace_name,
    project: page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  const conifgmap_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
    page.preparePage.deleteTapp(tapp_data.name);
    cm_prepare.delete(tapp_name);
    cm_prepare.create(conifgmap_data);
    secret_prepare.create(secret_data);
    page.preparePage.createTapp(tapp_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_data.name);
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
    cm_prepare.delete(tapp_name);
  });
  it('ACP2UI-55964 : 创建Tapp-输入镜像-名称-显示名称-选择凭据-pod亲和-方式基本-选择亲和组件-配置引用-配置字典-存储卷挂载-添加存储卷并挂载->点击创建|ACP2UI-56003 : 详情信息-基本信息显示正确|ACP2UI-56004 : 详情信息-容器基本信息(容器名称、镜像、资源限制、启动命令、参数)', () => {
    const testData = {
      组件名称: tapp_name,
      显示名称: tapp_name,
      存储卷: [
        {
          名称: 'cm-volume',
          类型: '配置字典',
          配置字典: tapp_name,
        },
        {
          名称: 'secret-volume',
          类型: '保密字典',
          保密字典: tapp_name,
        },
      ],
      亲和性: [
        {
          亲和性: 'Pod 亲和',
          方式: '基本',
          亲和组件: ['Tapp', tapp_data.name],
        },
      ],
      凭据: [tapp_name],
      配置引用: [['配置字典', tapp_name]],
      存储卷挂载: [
        [['配置字典', 'cm-volume'], 'key', '/test/cmsub'],
        [['配置字典', 'cm-volume'], '', '/test/cm', true],
        [['保密字典', 'secret-volume'], '.dockerconfigjson', '/test/secretsub'],
        [['保密字典', 'secret-volume'], '', '/test/secret'],
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      状态: '运行中',
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      已挂载存储卷: [
        '存储卷名称',
        '子路径',
        '挂载路径',
        '只读',
        'cm-volume',
        'key',
        '/test/cmsub',
        '/test/cm',
        '是',
        'secret-volume',
        '.dockerconfigjson',
        'secret-volume',
        '/test/secretsub',
        '/test/secret',
      ],
      配置引用: ['类型', '名称', '配置字典', tapp_name],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
