import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55989');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55989 : 创建Tapp-YAML-输入错误格式的YAML内容-点击创建', () => {
    const yaml = 'a';
    page.createPage.createByYaml(yaml);
    const expectDetailData = {
      面包屑: `计算/Tapp/YAML创建`,
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
