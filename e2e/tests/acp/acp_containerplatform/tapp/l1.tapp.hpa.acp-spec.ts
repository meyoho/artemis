import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 部署自动扩缩容L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('tapp-hpa');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  const hpav2_is_enabled = page.featureIsEnabled('hpav2');
  if (!tapp_is_enabled) {
    return;
  }

  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.deleteHpa(`tapp-${tapp_name}`);
    page.preparePage.deleteCronHpa(`tapp-${tapp_name}`);
    page.preparePage.createTapp(tapp_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.deleteHpa(`tapp-${tapp_name}`);
    page.preparePage.deleteCronHpa(`tapp-${tapp_name}`);
  });
  it('ACP2UI-55849 : tapp详情页->点击更新->点击自动扩缩容编辑弹窗的更新按钮->更新成功，页面提示无自动扩缩容', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55873 : tapp详情页->查看不调节hpa详情->显示无自动扩缩容', () => {
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55850 : tapp详情页->点击更新->选择指标调节->输入最大实例数3，最小实例数1，cpu阈值10->点击更新按钮->指标扩缩容能正常保存', () => {
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率10%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55871 : tapp详情页->查看指标调节hpa详情->页面显示最大最小实例数和cpu阈值，名称为tapp-<tappname>', () => {
    if (hpav2_is_enabled) {
      return;
    }
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率10%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55851 : tapp详情页->点击更新->选择定时调节->输入目标实例数3->点击更新->能正常保存', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['时间', ['每天', ''], 3]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天00:00触发3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55872 : tapp详情页->查看定时调节hpa详情->页面展示触发规则和目标实例数，名称为tapp-<tappname>', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天00:00触发3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });

  it('ACP2UI-55853 : tapp详情页->点击更新->选择定时调节->类型选择自定义->输入触发规则1 12 * * * ->目标实例数输入2->点击更新->能正常保存', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '1 12 * * *', 2]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天12:01触发2',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55854 : tapp详情页->点击更新->选择定时调节->输入目标实例数3->点击取消->没有保存，没有后端网络请求', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '10 12 * * *', 3]],
    };
    page.detailPage.updateHpa(tapp_name, testData, 'cancel');
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天12:01触发2',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55855 : tapp详情页->点击更新->选择定时调节->输入目标实例数3->添加一条调节规则->点击更新->能正常保存', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [
        ['自定义', '30 18 * * 3,6', 3],
        ['时间', ['星期一,星期二', '20:50'], 0],
      ],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容:
        '触发规则目标实例数星期三、星期六18:30触发3星期一、星期二20:50触发0',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55856 : tapp详情页->点击更新->选择定时调节->输入目标实例数->选择不调节->点击更新->保存成功，没有创建定时调节', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '1 12 * * *', 3, true]],
      类型: '不设置',
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55857 : tapp详情页->点击更新->选择指标调节->输入最大实例数3，最小实例数1，cpu阈值10->选择不调节->点击更新按钮->能正常保存，指标调节没有创建', () => {
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
      类型: '不设置',
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55858 : tapp详情页->点击更新->选择指标调节->输入最大实例数3，最小实例数1，cpu阈值10->选择定时调节->输入目标实例数2->点击更新按钮->能正常保存，指标调节没有创建，定时调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
      定时调节: [['自定义', '1 2 3 * *', 0]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = { 自动扩缩容: '触发规则目标实例数123**0' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55874 : 已存在一个定时调节hpa->点击更新->输入目标实例数6，选择周一周日，1:01触发->点击"X"->弹窗成功关闭，修改没有保存', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['时间', ['星期一,星期日', '01:01'], 6]],
    };
    page.detailPage.updateHpa(tapp_name, testData, 'close');
    const expectHpaData = { 自动扩缩容: '触发规则目标实例数123**0' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55865 : 已存在一个定时调节hpa->点击更新->输入目标实例数6，选择周一周日，1:01触发->点击更新->保存成功，值成功被修改', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['时间', ['星期一,星期日', '01:01'], 6]],
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数星期日、星期一01:01触发6',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55861 : 已存在一个定时调节hpa->点击更新->选择不设置->点击更新->保存成功，原来的hpa被删除', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55859 : tapp详情页->点击更新->选择定时调节->输入目标实例数2->选择指标调节->输入最大最小实例数和cpu阈值->点击更新->保存成功，没有创建定时调节，指标调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '1 2 3 4 5', 2]],
      指标调节: {
        最小实例数: 1,
        最大实例数: 1,
        'CPU 利用率': 1,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率1%最大实例数1',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55864 : 已存在一个指标调节hpa->点击更新->输入最大实例3，最小实例3，cpu阈值20->点击更新->保存成功，值成功修改', () => {
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 3,
        最大实例数: 3,
        'CPU 利用率': 20,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数3CPU利用率20%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55860 : 已存在一个指标调节hpa->点击更新->选择不设置->点击更新->保存成功，原来的hpa被删除 ', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(tapp_name, testData);
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
