import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55967');
  const namespace_name = page.ovnNamespaceName;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled(page.ovnClusterName);
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: `${tapp_name}-a`,
    namespace: namespace_name,
    project: page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name, namespace_name, page.ovnClusterName);
    page.preparePage.deleteTapp(
      tapp_data.name,
      namespace_name,
      page.ovnClusterName,
    );
    page.preparePage.createTapp(tapp_data, page.ovnClusterName);
    page.login();
    page.enterUserView(namespace_name, page.projectName, page.ovnClusterName);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  page.preparePage.deleteTapp(tapp_name, namespace_name, page.ovnClusterName);
  page.preparePage.deleteTapp(
    tapp_data.name,
    namespace_name,
    page.ovnClusterName,
  );
  it('ACP2UI-55967 : 创建Tapp-输入镜像-名称-host模式关闭(ovn网络模式)-pod反亲和-方式基本-选择组件-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
      亲和性: [
        {
          亲和性: 'Pod 反亲和',
          方式: '基本',
          亲和组件: ['Tapp', tapp_data.name],
        },
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
      'Pod 亲和': [
        'Pod 反亲和',
        'Required',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: tapp-${tapp_name}`,
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
