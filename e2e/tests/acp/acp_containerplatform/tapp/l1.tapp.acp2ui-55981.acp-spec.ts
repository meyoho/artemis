import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55981');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55981 : 更新Tapp-实例数量-异常策略为不迁移-容器名称-镜像地址-资源限制-host模式开启-点击确定', () => {
    const testData = {
      显示名称: tapp_name.toUpperCase(),
      实例数量: 2,
      节点异常策略: '不迁移',
      容器名称: 'test',
      镜像地址: 'index.alauda.cn/alaudaorg/qaimages:helloworld_1',
      资源限制: [{ CPU: 11, 单位: 'm' }, { 内存: 11, 单位: 'Mi' }],
      'Host 模式': 'true',
    };
    page.listPage.update(tapp_name, testData);
    const v_data = {
      镜像: testData.镜像地址,
      实例数量: 2,
      节点异常策略: testData.节点异常策略,
      资源限制: 'CPU:11m内存:11Mi',
    };
    page.detailPageVerify.verify(v_data);
  });
});
