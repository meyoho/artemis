import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55995');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55995 : 计算-Tapp-列表页-点击【创建Tapp】/【YAML创建Tapp】-进入创建Tapp页面/创建TappYAML页', () => {
    page.createPage.toCreatePage(imageAddress);
    page.detailPageVerify.verify({ 面包屑: '计算/Tapp/创建' });
    page.listPage.search(tapp_name, 0);
    page.createPage.toYamlCreatePage();
    page.detailPageVerify.verify({
      面包屑: '计算/Tapp/YAML创建',
    });
  });
});
