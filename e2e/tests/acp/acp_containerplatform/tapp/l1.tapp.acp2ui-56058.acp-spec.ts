import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
xdescribe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56058');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-56058 : 详情-容器组-点击【查看日志】-切换到日志tab,并选择相应的容器组', () => {
    page.listPage.enterDetail(tapp_name);
    page.detailPage.clickTab('容器组');
    page.detailPage.getPodNameByRowIndex(0).then(podName => {
      page.detailPage.visitPodLog(podName, tapp_name).then(() => {
        const expectDetailData = {
          日志容器组: podName,
          日志容器: tapp_name,
        };
        page.detailPageVerify.verify(expectDetailData);
      });
    });
  });
});
