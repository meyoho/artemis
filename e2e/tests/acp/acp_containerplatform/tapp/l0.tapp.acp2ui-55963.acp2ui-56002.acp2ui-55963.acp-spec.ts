import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';

describe('用户视图 Tapp L0自动化', () => {
  const page = new TappPage();
  const tapp_name = page.getTestData('acp2ui-55963');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55963 : L0:创建Tapp-选择镜像-输入名称-实例数量-异常策略默认不迁移-容器名称-镜像地址-资源限制-host模式默认关闭-点击创建', () => {
    const testData = {
      组件名称: tapp_name,
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/Tapp/${tapp_name}`,
      主机选择器: '-',
      更新策略: '最多不可用数: 1',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-56002 : 计算-Tapp-显示当前命名空间下的所有Tapp，按名称排序，采用瀑布流方式加载显示-信息显示正确', () => {
    page.listPage.waitStatus(tapp_name, '运行中');
    const expectCreateData = {
      标题: '计算/Tapp',
      header: ['名称', '状态', '创建时间'],
      数量: 1,
      创建: {
        名称: tapp_name,
        // 状态: '运行中',
      },
    };
    page.listPageVerify.verify(expectCreateData);
  });
  it('ACP2UI-56001 : 计算-Tapp-操作-点击【删除】-删除成功', () => {
    page.listPage.delete(tapp_name);
    // 验证搜索正确
    page.listPage.search(tapp_name, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
