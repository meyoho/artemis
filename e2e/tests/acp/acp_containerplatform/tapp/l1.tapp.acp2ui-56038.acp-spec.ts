import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const cm_prepare = new ConfigMapPrepareData();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-56038');
  const namespace_name = page.namespace1Name;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  const conifgmap_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  let file_name = '';
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
    cm_prepare.create(conifgmap_data);
    file_name = page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    cm_prepare.delete(tapp_name);
  });
  it('ACP2UI-56038 : 环境变量，点击更新，弹出更新环境变量页面', () => {
    const testData = {
      环境变量添加引用: [['cmref', ['配置字典', tapp_name], 'key']],
    };
    page.detailPage.updateEnv(tapp_name, testData);
    const expectHpaData = {
      环境变量: ['cmref', `配置字典 ${tapp_name} : key`],
    };
    page.detailPageVerify.verify(expectHpaData);
    browser.sleep(10).then(() => {
      page.clickLeftNavByText('Tapp');
      CommonKubectl.execKubectlCommand(
        `kubectl apply -f ${file_name}`,
        page.clusterName,
      );
      const testData_1 = {
        环境变量: [['env_key', 'env_value']],
      };
      page.detailPage.updateEnv(tapp_name, testData_1);
      const expectHpaData = {
        环境变量: ['env_key', 'env_value'],
      };
      page.detailPageVerify.verify(expectHpaData);
    });
  });
});
