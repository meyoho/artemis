import { ServerConf } from '@e2e/config/serverConf';
import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { PrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';

describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const secret_prepare = new PrepareData();
  const tapp_name = page.getTestData('acp2ui-55986');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  if (!tapp_is_enabled) {
    return;
  }
  const secret_data = {
    name: tapp_name,
    namespace: namespace_name,
    datas: {
      key: 'value',
    },
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
    secret_prepare.create(secret_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    secret_prepare.delete(tapp_name);
  });
  it('ACP2UI-55986 : 创建Tapp-输入必填项-点击取消/点击面包屑', () => {
    const testData = {
      组件名称: tapp_name,
      配置引用: [['保密字典', tapp_name]],
    };
    page.createPage.create(testData, imageAddress, '输入', '', '取消');
    const expectDetailData = {
      标题: `计算/Tapp`,
      不存在行: [tapp_name],
    };
    page.listPageVerify.verify(expectDetailData);
    page.createPage.create(testData, imageAddress, '输入', '', '面包屑');
    const expectDetailData_1 = {
      标题: `计算/Tapp`,
      不存在行: [tapp_name],
    };
    page.listPageVerify.verify(expectDetailData_1);
  });
});
