import { TappPage } from '@e2e/page_objects/acp/tapp/tapp.page';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';
import { CommonMethod } from '@e2e/utility/common.method';
describe('用户视图 Tapp L1自动化', () => {
  const page = new TappPage();
  const imageAddress = ServerConf.TESTIMAGE;
  const tapp_name = page.getTestData('acp2ui-55886');
  const namespace_name = page.namespace1Name;
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  const tapp_is_enabled = page.preparePage.tappIsEnabled();
  const hpav2_is_enabled = page.featureIsEnabled('hpav2');
  if (!tapp_is_enabled) {
    return;
  }
  const tapp_data = {
    name: tapp_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.preparePage.deleteTapp(tapp_name);
    page.preparePage.createTapp(tapp_data, page.clusterName);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('Tapp');
  });
  afterAll(() => {
    page.preparePage.deleteTapp(tapp_name);
  });
  it('ACP2UI-55886 : tapp详情页->点击更新->自动扩缩容详情页点击更新按钮->点击更新->选择指标调节->输入最大最小实例数和cpu阈值->点击更新->能成功保存', () => {
    if (hpav2_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 5,
        'CPU 利用率': 30,
      },
    };
    page.detailPage.updateHpa(tapp_name, testData);
    browser.sleep(1).then(() => {
      const reg = new RegExp('^(E|e)rror.?');
      const hpa_yaml = CommonKubectl.execKubectlCommand(
        `kubectl get hpa -n ${namespace_name} tapp-${tapp_name} -o yaml`,
        page.clusterName,
      );
      expect(reg.test(hpa_yaml)).toBe(false);
      page.jsonContain(
        {
          metadata: {
            ownerReferences: [
              {
                apiVersion: 'apps.tkestack.io/v1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'TApp',
                name: tapp_name,
              },
            ],
          },
        },
        CommonMethod.parseYaml(hpa_yaml),
        true,
      );
    });
    page.clickLeftNavByText('Tapp');
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData_ = {
      定时调节: [['时间', ['星期一,星期二', '12:00'], 0]],
    };
    page.detailPage.updateHpa(tapp_name, testData_);
    browser.sleep(1).then(() => {
      const reg = new RegExp('^(E|e)rror.?');
      const hpa_yaml = CommonKubectl.execKubectlCommand(
        `kubectl get cronhpa -n ${namespace_name} tapp-${tapp_name} -o yaml`,
        page.clusterName,
      );
      expect(reg.test(hpa_yaml)).toBe(false);
      page.jsonContain(
        {
          metadata: {
            ownerReferences: [
              {
                apiVersion: 'apps.tkestack.io/v1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'TApp',
                name: tapp_name,
              },
            ],
          },
        },
        CommonMethod.parseYaml(hpa_yaml),
        true,
      );
    });
  });
});
