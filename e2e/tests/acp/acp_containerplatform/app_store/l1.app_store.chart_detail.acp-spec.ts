import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';

describe('模板应用ui自动化L1case', () => {
  const app_store = new AppStorePage();
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    app_store.login();
    app_store.enterUserView(
      app_store.namespace1Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('应用目录');
  });
  afterEach(() => {});
  afterAll(() => {});
  it('ACP2UI-54885 : 应用目录详情页', () => {
    app_store.chartListPage.switch_chart_repo(chart_repo);
    app_store.chartListPage.click_chart(chart_name);
    const v_data = {
      名称: chart_name,
      仓库名称: chart_repo,
      描述: 'A Helm chart for Kubernetes',
    };
    app_store.chartDetailVerify.verify(v_data);
  });
});
