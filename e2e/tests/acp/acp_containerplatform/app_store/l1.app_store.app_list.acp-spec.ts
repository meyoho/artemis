import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('模板应用ui自动化L1case', () => {
  const app_store = new AppStorePage();
  const app_name = app_store.getTestData('tpl-appl1-2');
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  const docker_registry = ServerConf.TESTIMAGE.split('/')[0];
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  const chart_version = app_store.preparePage.getChartVersion();
  const hr_data = {
    name: app_name,
    namespace: app_store.namespace1Name,
    chart_repo: `${chart_repo}/${chart_name}`,
    region: app_store.clusterName,
    registry_address: docker_registry,
    version: chart_version,
  };
  beforeAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace1Name,
    );
    app_store.preparePage.create_helm_request(
      'alauda.helm.request.yaml',
      hr_data,
    );
    app_store.login();
    app_store.enterUserView(
      app_store.namespace1Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('模板应用');
  });
  afterEach(() => {});
  afterAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace1Name,
    );
  });
  it('ACP2UI-54886 : 模板应用-模板应用列表页', () => {
    const v_data = {
      提示信息:
        '模版应用是通过应用目录内的应用模版，快速部署的定制化应用实例。您可以查看模版应用的部署情况、升级模版应用的版本或删除模版应用。',
      表头: ['名称', '状态', '应用模板', '创建时间', ''],
    };
    app_store.appListVerifyPage.verify(v_data);
  });
});
