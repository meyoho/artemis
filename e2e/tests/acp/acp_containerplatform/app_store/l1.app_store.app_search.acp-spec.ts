import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('模板应用ui自动化L1case', () => {
  const app_store = new AppStorePage();
  const app_name = app_store.getTestData('tpl-appl1-3');
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  const docker_registry = ServerConf.TESTIMAGE.split('/')[0];
  const chart_version = app_store.preparePage.getChartVersion();
  const hr_data = {
    name: app_name,
    namespace: app_store.namespace2Name,
    chart_repo: `${chart_repo}/${chart_name}`,
    region: app_store.clusterName,
    registry_address: docker_registry,
    version: chart_version,
  };
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace2Name,
    );
    app_store.preparePage.create_helm_request(
      'alauda.helm.request.yaml',
      hr_data,
    );
    app_store.login();
    app_store.enterUserView(
      app_store.namespace2Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('模板应用');
  });
  afterEach(() => {});
  afterAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace2Name,
    );
  });
  it('ACP2UI-54892 : 模板应用搜索', () => {
    app_store.appListPage.tpl_app_table.searchByResourceName(app_name, 1);
    const v_data = {
      数量: 1,
    };
    app_store.appListVerifyPage.verify(v_data);
  });
});
