import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';

describe('模板应用ui自动化L1case', () => {
  const app_store = new AppStorePage();
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    app_store.login();
    app_store.enterUserView(
      app_store.namespace1Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('应用目录');
  });
  afterEach(() => {});
  afterAll(() => {});
  it('ACP2UI-54883 : 应用目录列表页', () => {
    app_store.chartListPage.switch_chart_repo(chart_repo);
    const v_data = {
      帮助信息: '应用目录通过可视化方式，基于应用模版一键快速创建应用。',
      当前chart源: chart_repo,
      模板: [{ 名称: chart_name, 注释: 'A Helm chart for Kubernetes' }],
    };
    app_store.chartListVerify.verify(v_data);
  });
});
