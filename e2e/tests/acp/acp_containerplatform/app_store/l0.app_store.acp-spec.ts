import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('模板应用ui自动化L0case', () => {
  const app_store = new AppStorePage();
  const app_name = app_store.getTestData('tpl-appl0');
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  const docker_registry = ServerConf.TESTIMAGE.split('/')[0];
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace1Name,
    );
    app_store.login();
    app_store.enterUserView(
      app_store.namespace1Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('应用目录');
  });
  afterEach(() => {});
  afterAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace1Name,
    );
  });
  it('ACP2UI-56181 : 应用目录详情页->点击【部署】->输入名称->点击确定', () => {
    app_store.chartListPage.wait_loding();
    const data = {
      名称: app_name,
      'global.registry': {
        键: ['address'],
        值: [docker_registry],
      },
      namespace: app_store.namespace1Name,
    };
    app_store.chartListPage.create_tpl_app(chart_repo, chart_name, data);
    const v_data = {
      名称: app_name,
      应用模板: `${chart_repo}/${chart_name}`,
    };
    app_store.appDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-54887 : 更新模板应用 ', () => {
    app_store.clickLeftNavByText('模板应用');
    const data = {
      显示名称: app_name,
    };
    app_store.appListPage.update_app(app_name, data);
    const v_data = {
      名称: app_name,
      应用模板: `${chart_repo}/${chart_name}`,
      显示名称: app_name,
    };
    app_store.appDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-56189 : 模板应用列表页->点击应用名称->点击操作->点击删除->点击确定->删除成功', () => {
    app_store.clickLeftNavByText('模板应用');
    app_store.appListPage.delete_app(app_name);
    const v_data = { 数量: 0 };
    app_store.appListPage.tpl_app_table.searchByResourceName(app_name, 0);
    app_store.appListVerifyPage.verify(v_data);
  });
});
