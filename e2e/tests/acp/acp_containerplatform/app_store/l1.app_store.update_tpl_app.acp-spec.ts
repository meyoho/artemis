import { AppStorePage } from '@e2e/page_objects/acp/app_store/app_store.page';
import { ServerConf } from '@e2e/config/serverConf';
import { browser } from 'protractor';
// import { browser } from 'protractor';

describe('模板应用ui自动化L1case', () => {
  const app_store = new AppStorePage();
  const app_name = app_store.getTestData('tpl-appl1-4');
  const chart_repo = 'stable';
  const chart_name = 'helloworld';
  const docker_registry = ServerConf.TESTIMAGE.split('/')[0];
  if (!app_store.preparePage.appmarketIsEnabled) {
    return;
  }
  const chart_version = app_store.preparePage.getChartVersion();
  const hr_data = {
    name: app_name,
    namespace: app_store.namespace2Name,
    chart_repo: `${chart_repo}/${chart_name}`,
    region: app_store.clusterName,
    registry_address: docker_registry,
    version: chart_version,
  };

  beforeAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace2Name,
    );
    app_store.preparePage.create_helm_request(
      'alauda.helm.request.yaml',
      hr_data,
    );
    app_store.login();
    app_store.enterUserView(
      app_store.namespace2Name,
      app_store.projectName,
      app_store.clusterName,
    );
  });
  beforeEach(() => {
    app_store.clickLeftNavByText('模板应用');
  });
  afterEach(() => {});
  afterAll(() => {
    app_store.preparePage.delete_helm_request(
      app_name,
      app_store.namespace2Name,
    );
  });
  it('ACP2UI-56187 : 模板应用列表页->点击应用名称->点击操作->点击更新->更新显示名称->点击保存->更新成功', () => {
    const data = {
      显示名称: app_name,
      模板版本: chart_version,
    };
    app_store.appDetailPage.update_app(app_name, data).then(() => {
      browser.sleep(3000);
      app_store.appDetailPage.waitDeploySuccess(60000);
    });
    const v_data = {
      名称: app_name,
      显示名称: app_name,
      应用模板: `${chart_repo}/${chart_name}`,
      状态: '部署成功',
      资源列表名称: '资源列表',
      资源列表表头: ['名称', '类型'],
      资源: [
        [`${app_name}-helloworld`, 'Deployment'],
        [`${app_name}-helloworld`, 'Service'],
      ],
    };
    app_store.appDetailVerifyPage.verify(v_data);
  });
});
