import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56908');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56908|ACP2UI-52581 : 后台通过kubectl 创建应用->通过前端创建版本快照->能成功创建', () => {
    app_page.detailAppPage.createSnapShot(`${app_name}-0`);
    const v_data_1 = {
      历史版本: {
        数量: 1,
        注释: `${app_name}-0`,
        表头: ['版本号', '修改时间', '修改人', '注释', '', ''],
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-52583|ACP2UI-54796 : 点击版本号左边的折叠按钮，展示变更内容', () => {
    app_page.detailAppPage.createSnapShot(`${app_name}-1`);
    const v_data_1 = {
      历史版本: {
        数量: 2,
        注释: `${app_name}-1`,
        变更内容: [
          '{new} 与 {old} 比较的变更内容:',
          `更新了 Application 类型资源: ${app_name}`,
        ],
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-52585 : 点击‘回滚至该版本’，会产生一个新版本，版本数加1', () => {
    app_page.detailAppPage.rollOut().then(() => {
      const v_data_1 = {
        历史版本: {
          数量: 3,
          回滚注释: `Rollback from revision`,
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
    });
  });
  it('ACP2UI-56736 : 点击"创建版本快照"按钮->点击创建按钮->成功创建', () => {
    app_page.detailAppPage.createSnapShot();
    const v_data_1 = {
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56738 : 点击"创建版本快照"按钮->输入注释信息->点击取消->成功取消，不会创建历史版本', () => {
    app_page.detailAppPage.createSnapShotCancel();
    const v_data_1 = {
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56739 : 点击"创建版本快照"按钮->输入注释信息->点击右上角"x"->弹窗关闭成功，没有创建历史版本', () => {
    app_page.detailAppPage.createSnapShotClose();
    const v_data_1 = {
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56733|ACP2UI-52573 : 应用详情页->点击镜像右侧小铅笔->输入新的镜像版本->点击确定->成功更新，历史版本不变', () => {
    app_page.detailAppPage.changeImageTag('latest');
    const v_data_1 = {
      Deployment: {
        镜像: `${ServerConf.TESTIMAGE.split(/:[^:]+$/i)[0]}:latest`,
      },
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
    app_page.detailAppPage.changeImageTag(
      ServerConf.TESTIMAGE.split(':')[
        ServerConf.TESTIMAGE.split(':').length - 1
      ],
    );
    const v_data_2 = {
      Deployment: {
        镜像: ServerConf.TESTIMAGE,
      },
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_2);
  });
  it('ACP2UI-56709 : 点击镜像右侧"小铅笔按钮"->输入新的版本->点击取消->弹窗关闭，镜像版本没有更新', () => {
    app_page.detailAppPage.changeImageTag('latest', 'cancel');
    const v_data_1 = {
      Deployment: {
        镜像: ServerConf.TESTIMAGE,
      },
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56710 : 点击镜像右侧"小铅笔按钮"->输入新的版本->点击弹窗右上角"x"->弹窗关闭，数据没有更新', () => {
    app_page.detailAppPage.changeImageTag('latest', 'x');
    const v_data_2 = {
      Deployment: {
        镜像: ServerConf.TESTIMAGE,
      },
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_2);
  });
  it('ACP2UI-56713 : 更新应用->更新显示名称->历史版本加1', () => {
    app_page.createAppPage.new_update(
      { 显示名称: 'test', 版本注释: 'update display name' },
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      历史版本: {
        数量: 5,
        注释: 'update display name',
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-54712|ACP2UI-54563 : Kubectl创建应用->前端页面更新、删除应用->功能正常', () => {
    app_page.detailAppPage.deleteApp(app_name);
    app_page.appListPage.search(app_name, 0);
    const v_data_1 = {
      数量: 0,
    };
    app_page.appListVerifyPage.verify(v_data_1);
  });
});
