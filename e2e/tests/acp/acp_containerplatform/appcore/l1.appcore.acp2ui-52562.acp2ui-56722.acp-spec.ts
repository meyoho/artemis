import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('52562');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52562|ACP2UI-56722 : 点击应用的操作->点击更新->在更新预览页，选择一个计算组件，点击右侧的小铅笔按钮->输入修改信息->点击保存->点击更新->成功更新', () => {
    const up_data = {
      计算组件: {
        实例数量: '2',
      },
    };
    app_page.createAppPage.update_workload_rs(
      up_data,
      'Deployment',
      `${app_name}-qaimages`,
      app_page.namespace1Name,
      app_name,
    );
    const v_data_1 = {
      Deployment: {
        实例数量: {
          pod名称: `${app_name}-qaimages`,
          pod数量: '2',
        },
      },
      历史版本: {
        数量: 1,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
