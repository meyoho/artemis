import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name_: string = app_page.getTestData('52564');
  // const imageAddress = ServerConf.TESTIMAGE;
  // const container_name = app_page.detailAppPage.getImageName(imageAddress);
  const data = {
    app_name: app_name_,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52564|ACP2UI-56719 : 应用更新预览页->点击添加工作负载->输入或选择镜像仓库->点击确定->输入计算组件信息->点击保存->点击更新->成功更新', () => {
    const up_data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      计算组件: {
        名称: 's',
        部署模式: 'StatefulSet',
        实例数量: '1',
        '组件 - 高级': {
          标签: [['componentKey', 'componentValue']],
        },
        容器组: {
          名称: app_name_,
          资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
        },
      },
    };
    app_page.createAppPage.update(up_data, app_name_, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name_}`,
      名称: app_name_,
      应用状态: {
        app名称: app_name_,
        状态: '运行中',
      },
      StatefulSet: {
        名称: `${app_name_}-s`,
        状态: {
          pod名称: `${app_name_}-s`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name_}-s`,
          pod数量: '1',
        },
        镜像: ServerConf.TESTIMAGE,
      },
      历史版本: {
        数量: 1,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
