import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const domain_name_full = app_page.getTestData('ingress-52610');
  const app_name: string = app_page.getTestData('52610');
  app_page.prepare.deleteDomain(domain_name_full);
  app_page.prepare.createDomain(domain_name_full, 'full');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  app_page.waitDomainInProject(domain_name_full, 'full');
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
    app_page.clickLeftNavByText('应用');
  });
  beforeEach(() => {});
  afterAll(() => {
    app_page.prepare.deleteDomain(domain_name_full);
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52610|ACP2UI-53769|ACP2UI-53770 : 更新应用(预览)，添加工作负载，添加内部路由-添加', () => {
    const update_data = {
      添加工作负载不提交: [
        {
          选择镜像: {
            方式: '输入',
            镜像地址: ServerConf.TESTIMAGE,
          },
          计算组件: {
            部署模式: 'StatefulSet',
          },
          内部路由: {
            名称: `${app_name}-qaimages`,
            会话保持: 'true',
            端口: [['TCP', '81', '81']],
          },
          访问规则: {
            名称: `${app_name}-qaimages`,
            域名: domain_name_full,
            规则: [['/c', `${app_name}-qaimages`, '81']],
          },
        },
      ],
    };
    app_page.createAppPage.update_notConfirm(update_data, app_name).then(() => {
      const v_data_1 = {
        内部路由: {
          名称: `${app_name}-qaimages`,
          '虚拟 IP': '是',
          外网访问: '否',
          会话保持: '是',
          端口: {
            表头: ['端口名称', '协议', '服务端口', '容器端口'],
            数据: [['tcp-81-81', 'TCP', '81', '81']],
          },
        },
        访问规则: {
          名称: `${app_name}-qaimages`,
          规则: {
            表头: ['地址', '内部路由', '端口', 'HTTPS'],
            数据: [['/c', `${app_name}-qaimages`, '81']],
          },
        },
      };
      app_page.appCreateVerify.verify(v_data_1);
    });
  });
  it('ACP2UI-53758 : 更新应用(预览)，添加工作负载，更新内部路由-取消', () => {
    const update_data = {
      修改内部路由取消: {
        名称: `${app_name}-qaimages`,
        数据: {
          端口: [['TCP', '82', '82']],
        },
      },
    };
    app_page.createAppPage.fillForm(update_data).then(() => {
      const v_data_1 = {
        内部路由: {
          名称: `${app_name}-qaimages`,
          '虚拟 IP': '是',
          外网访问: '否',
          会话保持: '是',
          端口: {
            表头: ['端口名称', '协议', '服务端口', '容器端口'],
            数据: [['tcp-81-81', 'TCP', '81', '81']],
          },
        },
      };
      app_page.appCreateVerify.verify(v_data_1);
    });
  });
  it('ACP2UI-53757 : 更新应用(预览)，添加工作负载，更新内部路由-更新', () => {
    const update_data = {
      修改内部路由: {
        名称: `${app_name}-qaimages`,
        数据: {
          端口: [['TCP', '82', '82']],
        },
      },
    };
    app_page.createAppPage.fillForm(update_data).then(() => {
      const v_data_1 = {
        内部路由: {
          名称: `${app_name}-qaimages`,
          '虚拟 IP': '是',
          外网访问: '否',
          会话保持: '是',
          端口: {
            表头: ['端口名称', '协议', '服务端口', '容器端口'],
            数据: [['tcp-82-82', 'TCP', '82', '82']],
          },
        },
      };
      app_page.appCreateVerify.verify(v_data_1);
    });
  });
  it('ACP2UI-53768|ACP2UI-53725 : 更新应用(预览)，添加工作负载，添加访问规则-取消|更新应用(预览)，添加工作负载，添加内部路由-取消', () => {
    const update_data = {
      内部路由取消: {
        名称: `${app_name}-qaimagesc`,
        会话保持: 'true',
        端口: [['TCP', '81', '81']],
      },
      访问规则取消: {
        名称: `${app_name}-qaimagesc`,
        域名: domain_name_full,
        规则: [['/cc', `${app_name}-qaimages`, '82']],
      },
    };
    app_page.createAppPage.fillForm(update_data).then(() => {
      const v_data_1 = {
        不存在内部路由: {
          名称: `${app_name}-qaimagesc`,
        },
        不存在访问规则: {
          名称: `${app_name}-qaimagesc`,
        },
      };
      app_page.appCreateVerify.verify(v_data_1);
    });
  });
  it('ACP2UI-53743|ACP2UI-53772 : 更新应用(预览)，添加工作负载，删除内部路由|更新应用(预览)页，添加工作负载，删除规则 ', () => {
    const update_data = {
      删除内部路由: {
        名称: `${app_name}-qaimages`,
      },
      删除访问规则: {
        名称: `${app_name}-qaimages`,
      },
    };
    app_page.createAppPage.fillForm(update_data).then(() => {
      const v_data_1 = {
        不存在内部路由: {
          名称: `${app_name}-qaimages`,
        },
        不存在访问规则: {
          名称: `${app_name}-qaimages`,
        },
      };
      app_page.appCreateVerify.verify(v_data_1);
    });
  });
});
