import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('53762');
  const container_name = 'qaimages';
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-53762 : 更新应用(预览)，添加访问规则-取消 ', () => {
    const up_data = {
      添加内部路由: [
        {
          '虚拟 IP': 'true',
          会话保持: 'false',
          工作负载: ['部署', `${app_name}-qaimages`],
          端口: [['TCP', '80', '80']],
        },
      ],
      添加访问规则取消: [
        {
          名称: `${app_name}-qaimages`,
          规则: [['/update', `${app_name}-qaimages`, '80']],
        },
      ],
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}-qaimages`, 'Service']],
      其他资源不存在: [[`${app_name}-qaimages`, 'Ingress']],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-53761 : 更新应用(预览)，添加访问规则-添加', () => {
    const up_data = {
      添加访问规则: [
        {
          名称: `${app_name}-qaimages`,
          规则: [['/update', `${app_name}-qaimages`, '80']],
        },
      ],
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [
        [`${app_name}-qaimages`, 'Service'],
        [`${app_name}-qaimages`, 'Ingress'],
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-53766 : 更新应用(预览)页，更新访问规则-已创建-取消', () => {
    const up_data = {
      修改访问规则取消: {
        名称: `${app_name}-qaimages`,
        数据: { 规则: [['/upc', `${app_name}-qaimages`, '80']] },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [
        [`${app_name}-qaimages`, 'Service'],
        [`${app_name}-qaimages`, 'Ingress'],
      ],
      YAML: [
        {
          apiVersion: 'app.k8s.io/v1beta1',
          kind: 'Application',
          metadata: {
            name: app_name,
            namespace: app_page.namespace1Name,
          },
        },
        {
          kind: 'Deployment',
          metadata: {
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
          },
        },
        {
          apiVersion: 'extensions/v1beta1',
          kind: 'Ingress',
          metadata: {
            labels: app_page.createLabels([
              {
                key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `${app_name}.${app_page.namespace1Name}`,
              },
            ]),
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
            ownerReferences: [
              {
                apiVersion: 'app.k8s.io/v1beta1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'Application',
                name: app_name,
              },
            ],
          },
          spec: {
            rules: [
              {
                http: {
                  paths: [
                    {
                      backend: {
                        serviceName: `${app_name}-${container_name}`,
                        servicePort: 80,
                      },
                      path: '/update',
                    },
                  ],
                },
              },
            ],
          },
        },
        {
          apiVersion: 'v1',
          kind: 'Service',
          metadata: {
            labels: app_page.createLabels([
              {
                key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `${app_name}.${app_page.namespace1Name}`,
              },
            ]),
            name: `${app_name}-qaimages`,
            namespace: app_page.namespace1Name,
          },
        },
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-53765 : 更新应用(预览)页，更新访问规则-已创建-更新', () => {
    const up_data = {
      修改访问规则: {
        名称: `${app_name}-qaimages`,
        数据: { 规则: [['/up', `${app_name}-qaimages`, '80']] },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [
        [`${app_name}-qaimages`, 'Service'],
        [`${app_name}-qaimages`, 'Ingress'],
      ],
      YAML: [
        {
          apiVersion: 'app.k8s.io/v1beta1',
          kind: 'Application',
          metadata: {
            name: app_name,
            namespace: app_page.namespace1Name,
          },
        },
        {
          kind: 'Deployment',
          metadata: {
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
          },
        },
        {
          apiVersion: 'extensions/v1beta1',
          kind: 'Ingress',
          metadata: {
            labels: app_page.createLabels([
              {
                key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `${app_name}.${app_page.namespace1Name}`,
              },
            ]),
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
            ownerReferences: [
              {
                apiVersion: 'app.k8s.io/v1beta1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'Application',
                name: app_name,
              },
            ],
          },
          spec: {
            rules: [
              {
                http: {
                  paths: [
                    {
                      backend: {
                        serviceName: `${app_name}-${container_name}`,
                        servicePort: 80,
                      },
                      path: '/up',
                    },
                  ],
                },
              },
            ],
          },
        },
        {
          apiVersion: 'v1',
          kind: 'Service',
          metadata: {
            labels: app_page.createLabels([
              {
                key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `${app_name}.${app_page.namespace1Name}`,
              },
            ]),
            name: `${app_name}-qaimages`,
            namespace: app_page.namespace1Name,
          },
        },
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-53771 : 更新应用(预览)页，删除规则', () => {
    const up_data = {
      删除访问规则: { 名称: `${app_name}-qaimages` },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}-qaimages`, 'Service']],
      其他资源不存在: [[`${app_name}-qaimages`, 'Ingress']],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-53724 : 更新应用(预览)，添加内部路由-取消', () => {
    const up_data = {
      添加内部路由取消: [
        {
          名称: 'update-cancel',
          '虚拟 IP': 'true',
          会话保持: 'false',
          工作负载: ['部署', `${app_name}-qaimages`],
          端口: [['TCP', '80', '80']],
        },
      ],
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}-qaimages`, 'Service']],
      其他资源不存在: [['update-cancel', 'Service']],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
