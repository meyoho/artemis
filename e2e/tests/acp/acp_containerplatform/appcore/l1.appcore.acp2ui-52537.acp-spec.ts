import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('52537');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-52537 : L1 点击创建应用->输入镜像地址->确定->输入应用名称->实例数量修改为2->点击创建->应用能成功创建', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        实例数量: '2',
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '2',
        },
        镜像: imageAddress,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
