import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();

  const app_name: string = app_page.getTestData('52608');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
    service_name: `${app_name}-qaimages`,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52608|ACP2UI-53722 : 更新应用(预览)，更新内部路由-已创建-取消', () => {
    const update_data = {
      修改内部路由取消: {
        名称: data.service_name,
        数据: {
          外网访问: 'true',
          会话保持: 'true',
          端口: [['TCP', '90', '90']],
        },
      },
    };
    app_page.createAppPage.update_notConfirm(update_data, app_name).then(() => {
      const v_data = {
        资源: [
          {
            资源类型: 'Service',
            名称: data.service_name,
            '虚拟 IP': '是',
            外网访问: '否',
            会话保持: '否',
            端口: {
              表头: ['端口名称', '协议', '服务端口', '容器端口'],
              数据: [['tcp-80-80', 'TCP', '80', '80']],
            },
          },
        ],
      };
      app_page.appPreviewVerifyPage.verify(v_data);
    });
  });
});
