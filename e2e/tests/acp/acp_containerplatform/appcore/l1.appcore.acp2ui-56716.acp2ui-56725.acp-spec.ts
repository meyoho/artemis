import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56716');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56716|ACP2UI-56725 : 更新应用->更新应用预览页->修改应用计算组件->提交更新->历史版本加1', () => {
    const up_data = {
      计算组件: {
        实例数量: '2',
        '组件 - 高级': {
          更新策略: ['1', '1'],
        },
      },
    };
    app_page.createAppPage.update_workload_rs(
      up_data,
      'Deployment',
      `${app_name}-qaimages`,
      app_page.namespace1Name,
      app_name,
    );
    const v_data_1 = {
      Deployment: {
        实例数量: {
          pod名称: `${app_name}-qaimages`,
          pod数量: '2',
        },
      },
      历史版本: {
        数量: 1,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
