import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('56533');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-56533 : 应用创建页->输入应用名称->展开容器组-高级->存储卷-主机路径存储卷->容器-存储卷挂载-点击添加->选择存储卷->输入挂载路径->点击创建->应用创建成功', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          '容器组 - 高级': {
            存储卷: [
              {
                名称: app_name,
                类型: '主机路径',
                主机路径: '/tmp',
              },
            ],
          },
          存储卷挂载: [[['主机路径', app_name], '', '/home/hostpath']],
        },
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        镜像: imageAddress,
      },
      YAML: [
        {
          apiVersion: 'app.k8s.io/v1beta1',
          kind: 'Application',
          metadata: {
            name: app_name,
            namespace: app_page.namespace1Name,
          },
          spec: {
            assemblyPhase: 'Succeeded',
            componentKinds: [{ group: 'apps', kind: 'Deployment' }],
            descriptor: {},
            selector: {
              matchLabels: app_page.createLabels([
                {
                  key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: `${app_name}.${app_page.namespace1Name}`,
                },
              ]),
            },
          },
        },
        {
          kind: 'Deployment',
          metadata: {
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
            ownerReferences: [
              {
                apiVersion: 'app.k8s.io/v1beta1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'Application',
                name: app_name,
              },
            ],
          },
          spec: {
            progressDeadlineSeconds: 600,
            replicas: 1,
            revisionHistoryLimit: 10,
            selector: {
              matchLabels: app_page.createLabels([
                {
                  key: `project.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: app_page.projectName,
                },
                {
                  key: `service.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: `deployment-${app_name}-${container_name}`,
                },
              ]),
            },
            strategy: {
              rollingUpdate: { maxSurge: '25%', maxUnavailable: '25%' },
              type: 'RollingUpdate',
            },
            template: {
              metadata: {
                creationTimestamp: null,
                labels: app_page.createLabels([
                  {
                    key: 'app',
                    value: `deployment-${app_name}-${container_name}`,
                  },
                  {
                    key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: `${app_name}.${app_page.namespace1Name}`,
                  },
                  {
                    key: `project.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: app_page.projectName,
                  },
                  {
                    key: `service.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: `deployment-${app_name}-${container_name}`,
                  },
                  {
                    key: 'version',
                    value: 'v1',
                  },
                ]),
              },
              spec: {
                containers: [
                  {
                    image: imageAddress,
                    imagePullPolicy: 'IfNotPresent',
                    name: container_name,
                    terminationMessagePath: '/dev/termination-log',
                    terminationMessagePolicy: 'File',
                    volumeMounts: [
                      {
                        mountPath: '/home/hostpath',
                        name: app_name,
                        // readOnly: false,
                        // subPath: 'key1',
                      },
                    ],
                  },
                ],
                dnsPolicy: 'ClusterFirst',
                restartPolicy: 'Always',
                schedulerName: 'default-scheduler',
                securityContext: {},
                terminationGracePeriodSeconds: 30,
                volumes: [
                  {
                    name: app_name,
                    hostPath: {
                      path: '/tmp',
                    },
                  },
                ],
              },
            },
          },
        },
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
