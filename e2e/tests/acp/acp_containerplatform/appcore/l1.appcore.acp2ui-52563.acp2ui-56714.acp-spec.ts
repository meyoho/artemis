import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('52563');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52563|ACP2UI-56714 : 应用更新预览页->点击yaml->修改yaml内容->点击更新->成功更新', () => {
    app_page.detailAppPage.Yaml.then(yaml => {
      yaml.getYamlValue().then(yamls_text => {
        const deploy_yaml = CommonMethod.parseYaml(
          String(yamls_text)
            .split('---\n')
            .filter(yaml_text => {
              const yaml_obj = CommonMethod.parseYaml(yaml_text);
              return yaml_obj.kind === 'Deployment';
            })[0],
        );
        deploy_yaml.spec.template.spec.containers[0].image = `${
          ServerConf.TESTIMAGE.split(':')[0]
        }:latest`;
        app_page.clickLeftNavByText('应用');
        app_page.appListPage.toAppDetail(app_name);
        app_page.createAppPage.update_app_yaml({ YAML: deploy_yaml });
        const v_data_1 = {
          Deployment: {
            镜像: `${ServerConf.TESTIMAGE.split(':')[0]}:latest`,
          },
          历史版本: {
            数量: 1,
          },
        };
        app_page.appDetailVerifyPage.verify(v_data_1);
      });
    });
  });
});
