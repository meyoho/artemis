import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('54384');
  const imageAddress = 'index.alauda.cn/alaudaorg/qa_im.ages.test:helloworld';

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-54384 : 输入镜像地址-异常case - Version1', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: imageAddress,
      },

      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          //名称: app_name,
          资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
        },
      },
    };
    app_page.createAppPage.createAppNotWaitRunning(
      data,
      app_page.namespace1Name,
    );
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        镜像: imageAddress,
        资源限制: 'CPU: 10m内存: 10Mi',
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
