import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DetailIngressPageVerify } from '@e2e/page_verify/acp/ingress/detail.page';
import { DetailServicePageVerify } from '@e2e/page_verify/acp/service/detail.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('53759');
  const ingress_detail_verify = new DetailIngressPageVerify();
  const service_detail_verify = new DetailServicePageVerify();
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
    service_name: `${app_name}-qaimages`,
    ingress_name: `${app_name}-qaimages`,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-53759 : 应用详情页，访问规则页面展示', () => {
    app_page.appListPage.toAppDetail(app_name);
    app_page.detailAppPage.otherResourceTable.clickResourceNameByRow([
      `${app_name}-qaimages`,
      'Ingress',
    ]);
    const v_data = {
      面包屑: `网络/访问规则/${app_name}-qaimages`,
      名称: `${app_name}-qaimages`,
    };
    ingress_detail_verify.verify(v_data);
  });
  it('ACP2UI-53732 : 应用详情页，内部路由页面展示', () => {
    app_page.appListPage.toAppDetail(app_name);
    app_page.detailAppPage.otherResourceTable.clickResourceNameByRow([
      `${app_name}-qaimages`,
      'Service',
    ]);
    const v_data = {
      面包屑: `网络/内部路由/${app_name}-qaimages`,
      名称: `${app_name}-qaimages`,
    };
    service_detail_verify.verify(v_data);
  });
});
