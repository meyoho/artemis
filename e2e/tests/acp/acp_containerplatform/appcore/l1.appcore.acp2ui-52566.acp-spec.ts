import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('52566');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  const configmap_data = {
    name: app_name,
    namespace: app_page.namespace1Name,
    datas: { key: 'value' },
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52566 : 应用更新预览页->点击添加工作负载按钮右侧"v"->点击yaml添加资源->输入yaml内容->点击添加->点击更新->成功更新', () => {
    const up_data = {
      'YAML 添加资源': [
        CommonMethod.readTemplateFile('alauda.configmap.yaml', configmap_data),
      ],
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}`, 'ConfigMap']],
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
