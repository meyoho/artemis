import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {});
  it('ACP2UI-55491 : L1 点击创建应用->输入镜像地址->点击取消->取消成功', () => {
    const data = {
      选择镜像取消: {
        方式: '输入',
        镜像地址: imageAddress,
      },
    };
    app_page.createAppPage.select_image_cancel(data);
    const v_data = {
      // 验证不存在
      选择镜像弹窗存在: false,
    };
    app_page.appListVerifyPage.verify(v_data);
  });
});
