import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const deploy_page = new DeploymentPage();
  const app_name: string = app_page.getTestData('52584');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52584 : 应用计算组件详情页->修改应用计算组件信息->应用历史版本数量不变', () => {
    app_page.clickLeftNavByText('部署');
    const testData2 = {
      环境变量: [['test', 'test']],
    };
    deploy_page.detailPage.updateEnv(`${app_name}-qaimages`, testData2);
    const v_data_1 = {
      历史版本: {
        数量: 0,
      },
    };
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
    app_page.appDetailVerifyPage.verify(v_data_1);
    app_page.clickLeftNavByText('部署');
    deploy_page.detailPage.updateImage(`${app_name}-qaimages`, 'latest');
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
    app_page.appDetailVerifyPage.verify(v_data_1);
    // app_page.clickLeftNavByText('部署');
    // testData2 = {
    //   环境变量: [[], [], [], [], ['test', 'test']],
    // };
    // deploy_page.detailPage.updateLabel(`${app_name}-qaimages`, testData2);
    // app_page.clickLeftNavByText('应用');
    // app_page.appListPage.toAppDetail(app_name);
    // app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
