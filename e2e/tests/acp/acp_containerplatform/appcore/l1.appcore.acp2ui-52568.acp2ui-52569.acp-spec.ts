import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name_: string = app_page.getTestData('52568');
  const data = {
    app_name: app_name_,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name_);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-52568 : 点击停止按钮->应用能正常停止', () => {
    const v_data = {
      标题: `应用/${app_name_}`,
      名称: app_name_,
      应用状态: {
        app名称: app_name_,
        状态: '运行中',
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
    app_page.detailAppPage.stopApp(app_name_);
    const v_data_1 = {
      标题: `应用/${app_name_}`,
      名称: app_name_,
      应用状态: {
        app名称: app_name_,
        状态: '停止',
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-52569 : 点击开始按钮->开始应用,应用能正常启动', () => {
    const v_data = {
      标题: `应用/${app_name_}`,
      名称: app_name_,
      应用状态: {
        app名称: app_name_,
        状态: '运行中',
      },
    };
    app_page.detailAppPage.startApp(app_name_);
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
