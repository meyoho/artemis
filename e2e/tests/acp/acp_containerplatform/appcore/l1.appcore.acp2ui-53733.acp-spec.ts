import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('53733');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);
  const domain_name_full = app_page.getTestData('ingress-53733');

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.prepare.deleteDomain(domain_name_full);
    app_page.prepare.createDomain(domain_name_full, 'full');
    app_page.waitDomainInProject(domain_name_full, 'full');
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteDomain(domain_name_full);
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-53733 : 创建应用页，添加访问规则-添加', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: imageAddress,
      },

      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          //名称: app_name,
          资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
        },
      },
      内部路由: {
        名称: `${app_name}`,
        '虚拟 IP': 'true',
        外网访问: 'true',
        会话保持: 'true',
        // 目标组件: `${app_name}-${container_name}`,
        端口: [['TCP', '80', '80']],
      },
      访问规则: {
        名称: `${app_name}`,
        域名: domain_name_full,
        规则: [['/create', `${app_name}`, '80']],
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        镜像: imageAddress,
        资源限制: 'CPU: 10m内存: 10Mi',
      },
      其他资源: [[`${app_name}`, 'Ingress'], [`${app_name}`, 'Service']],
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
