import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('53759');
  const domain_name_full = app_page.getTestData('ingress-53759');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
    service_name: `${app_name}-qaimages`,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.deleteDomain(domain_name_full);
    app_page.prepare.createDomain(domain_name_full, 'full');
    app_page.waitDomainInProject(domain_name_full, 'full');
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteDomain(domain_name_full);
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-53762 : 更新应用(预览)，添加访问规则-取消', () => {
    const updata = {
      添加访问规则取消: [
        {
          名称: data.service_name,
          域名: domain_name_full,
          规则: [['/create', data.service_name, '80']],
        },
      ],
    };
    app_page.createAppPage.update_notConfirm(updata, app_name).then(() => {
      const v_data = {
        不存在资源: [
          {
            资源类型: 'Ingress',
            名称: data.service_name,
          },
        ],
      };
      app_page.appPreviewVerifyPage.verify(v_data);
    });
  });
  it('ACP2UI-53761|ACP2UI-53767|ACP2UI-53763|ACP2UI-53764 : 更新应用(预览)，添加访问规则-添加|更新应用(预览)页，访问规则界面展示', () => {
    const updata = {
      添加访问规则: [
        {
          名称: data.service_name,
          域名: domain_name_full,
          规则: [['/create', data.service_name, '80']],
        },
      ],
    };
    app_page.createAppPage.update_notConfirm(updata, app_name).then(() => {
      const v_data = {
        资源: [
          {
            资源类型: 'Ingress',
            名称: data.service_name,
            规则: {
              表头: ['地址', '内部路由', '端口', 'HTTPS'],
              数据: [[`${domain_name_full}/create`, data.service_name, '80']],
            },
          },
        ],
      };
      app_page.appPreviewVerifyPage.verify(v_data).then(() => {
        const updata_c = {
          修改访问规则取消: {
            名称: data.service_name,
            数据: {
              规则: [['/update', data.service_name, '80']],
            },
          },
        };
        app_page.appPreviewPage.input(updata_c).then(() => {
          app_page.appPreviewVerifyPage.verify(v_data).then(() => {
            const updata_y = {
              修改访问规则: {
                名称: data.service_name,
                数据: {
                  规则: [['/update', data.service_name, '80']],
                },
              },
            };
            app_page.appPreviewPage.input(updata_y).then(() => {
              const v_data_y = {
                资源: [
                  {
                    资源类型: 'Ingress',
                    名称: data.service_name,
                    规则: {
                      表头: ['地址', '内部路由', '端口', 'HTTPS'],
                      数据: [
                        [`${domain_name_full}/update`, data.service_name, '80'],
                      ],
                    },
                  },
                ],
              };
              app_page.appPreviewVerifyPage.verify(v_data_y);
            });
          });
        });
      });
    });
  });
});
