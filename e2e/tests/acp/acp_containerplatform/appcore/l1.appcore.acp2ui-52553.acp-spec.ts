import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('52553');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);
  const deploy_data = {
    '${NAME}': `${app_name}-${container_name}`,
    '${NAMESPACE}': app_page.namespace1Name,
    '${IMAGE}': imageAddress,
  };

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-52553 : 应用列表页->点击创建应用按钮右侧"v"->点击Yaml创建->输入应用名称->输入应用组件yaml内容->点击创建->应用成功创建 ', () => {
    const data = {
      名称: app_name,
      显示名称: app_name,
      YAML创建: CommonMethod.readyamlfile(
        'alauda.deployment.yaml',
        deploy_data,
      ),
    };
    app_page.createAppPage.yamlCreate(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        镜像: imageAddress,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
