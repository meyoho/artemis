import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('55485');

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-55485 : L1 点击创建应用->输入镜像地址->确定->输入应用名称->点击取消->应用没有创建', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        实例数量: '1',
        '组件 - 高级': {
          标签: [['label-name', 'label-value']],
          更新策略: ['1', '1'],
          'Host 模式': false,
        },
        容器组: {
          资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
        },
      },
    };
    app_page.createAppPage.create_cancel(data);
    const v_data = { 数量: 0 };
    app_page.appListPage.search(app_name, 0);
    app_page.appListVerifyPage.verify(v_data);
  });
});
