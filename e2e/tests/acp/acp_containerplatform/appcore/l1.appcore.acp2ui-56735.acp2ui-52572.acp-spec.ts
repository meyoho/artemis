import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56735');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56735|ACP2UI-52572 : 应用详情页->点击"+""-"号按钮手动扩缩容->成功扩缩容，历史版本不变', () => {
    app_page.detailAppPage.clickReplicaSet(
      'Deployment',
      `${app_name}-qaimages`,
      '-',
    );
    const v_data_0 = {
      Deployment: {
        实例数量: {
          pod名称: `${app_name}-qaimages`,
          pod数量: '0',
        },
      },
      历史版本: {
        数量: 0,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_0);
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
    app_page.detailAppPage.clickReplicaSet(
      'Deployment',
      `${app_name}-qaimages`,
      '+',
    );
    const v_data_1 = {
      Deployment: {
        实例数量: {
          pod名称: `${app_name}-qaimages`,
          pod数量: '1',
        },
      },
      历史版本: {
        数量: 0,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
