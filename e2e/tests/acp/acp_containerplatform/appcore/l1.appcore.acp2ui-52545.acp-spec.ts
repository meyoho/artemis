import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('52545');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-52545 : 应用创建页->输入应用名称->容器-启动命令输入sleep,容器-参数输入3600->点击创建->应用成功创建, pod每隔1小时退出一次', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          启动命令: [['sleep']],
          参数: [['3600']],
        },
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        镜像: imageAddress,
      },
      YAML: [
        {
          apiVersion: 'app.k8s.io/v1beta1',
          kind: 'Application',
          metadata: {
            name: app_name,
            namespace: app_page.namespace1Name,
          },
          spec: {
            assemblyPhase: 'Succeeded',
            componentKinds: [{ group: 'apps', kind: 'Deployment' }],
            descriptor: {},
            selector: {
              matchLabels: app_page.createLabels([
                {
                  key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: `${app_name}.${app_page.namespace1Name}`,
                },
              ]),
            },
          },
        },
        {
          kind: 'Deployment',
          metadata: {
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
            ownerReferences: [
              {
                apiVersion: 'app.k8s.io/v1beta1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'Application',
                name: app_name,
              },
            ],
          },
          spec: {
            progressDeadlineSeconds: 600,
            replicas: 1,
            revisionHistoryLimit: 10,
            selector: {
              matchLabels: app_page.createLabels([
                {
                  key: `project.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: app_page.projectName,
                },
                {
                  key: `service.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: `deployment-${app_name}-${container_name}`,
                },
              ]),
            },
            strategy: {
              rollingUpdate: { maxSurge: '25%', maxUnavailable: '25%' },
              type: 'RollingUpdate',
            },
            template: {
              metadata: {
                creationTimestamp: null,
                labels: app_page.createLabels([
                  {
                    key: 'app',
                    value: `deployment-${app_name}-${container_name}`,
                  },
                  {
                    key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: `${app_name}.${app_page.namespace1Name}`,
                  },
                  {
                    key: `project.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: app_page.projectName,
                  },
                  {
                    key: `service.${ServerConf.LABELBASEDOMAIN}/name`,
                    value: `deployment-${app_name}-${container_name}`,
                  },
                  {
                    key: 'version',
                    value: 'v1',
                  },
                ]),
              },
              spec: {
                containers: [
                  {
                    image: imageAddress,
                    imagePullPolicy: 'IfNotPresent',
                    args: ['3600'],
                    command: ['sleep'],
                    name: container_name,
                    terminationMessagePath: '/dev/termination-log',
                    terminationMessagePolicy: 'File',
                  },
                ],
                dnsPolicy: 'ClusterFirst',
                restartPolicy: 'Always',
                schedulerName: 'default-scheduler',
                securityContext: {},
                terminationGracePeriodSeconds: 30,
              },
            },
          },
        },
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
