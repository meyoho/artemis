import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56721');
  const container_name = 'qaimages';
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56721|ACP2UI-53723 : 更新应用->更新应用预览页->添加内部路由->提交更新->历史版本加1', () => {
    const up_data = {
      添加内部路由: [
        {
          '虚拟 IP': 'true',
          会话保持: 'false',
          工作负载: ['部署', `${app_name}-qaimages`],
          端口: [['TCP', '80', '80']],
        },
      ],
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}-qaimages`, 'Service']],
      历史版本: {
        数量: 1,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56717|ACP2UI-53790 : 更新应用->更新应用预览页->修改内部路由->提交更新->历史版本加1', () => {
    const up_data = {
      修改内部路由: {
        名称: `${app_name}-qaimages`,
        数据: {
          '虚拟 IP': 'true',
          会话保持: 'true',
          端口: [['TCP', '80', '81']],
        },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      其他资源: [[`${app_name}-qaimages`, 'Service']],
      YAML: [
        {
          apiVersion: 'app.k8s.io/v1beta1',
          kind: 'Application',
          metadata: {
            name: app_name,
            namespace: app_page.namespace1Name,
          },
        },
        {
          kind: 'Deployment',
          metadata: {
            name: `${app_name}-${container_name}`,
            namespace: app_page.namespace1Name,
          },
        },
        {
          apiVersion: 'v1',
          kind: 'Service',
          metadata: {
            labels: app_page.createLabels([
              {
                key: `app.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `${app_name}.${app_page.namespace1Name}`,
              },
            ]),
            name: `${app_name}-qaimages`,
            namespace: app_page.namespace1Name,
            ownerReferences: [
              {
                apiVersion: 'app.k8s.io/v1beta1',
                blockOwnerDeletion: true,
                controller: true,
                kind: 'Application',
                name: app_name,
              },
            ],
          },

          spec: {
            ports: [
              {
                name: 'tcp-80-81',
                port: 80,
                protocol: 'TCP',
                targetPort: 81,
              },
            ],
            selector: app_page.createLabels([
              {
                key: `service.${ServerConf.LABELBASEDOMAIN}/name`,
                value: `deployment-${app_name}-qaimages`,
              },
            ]),
            sessionAffinity: 'ClientIP',
            type: 'ClusterIP',
          },
        },
      ],
      历史版本: {
        数量: 2,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56720|ACP2UI-53742 : 更新应用->更新应用预览页->删除内部路由->提交更新->历史版本加1', () => {
    const up_data = {
      删除内部路由: { 名称: `${app_name}-qaimages` },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      其他资源不存在: [[`${app_name}-qaimages`, 'Service']],
      历史版本: {
        数量: 3,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56718 : 更新应用->更新应用预览页->删除应用计算组件->提交更新->历史版本加1', () => {
    const up_data = {
      删除计算组件: { 名称: `${app_name}-qaimages`, 类型: 'Deployment' },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
