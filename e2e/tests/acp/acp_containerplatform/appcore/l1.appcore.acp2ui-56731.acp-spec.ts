import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';
import { DeploymentListVerify } from '@e2e/page_verify/acp/deployment/list.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const deploy_list_page = new DeploymentListPage();
  const deploy_list_verify_page = new DeploymentListVerify();
  const app_name: string = app_page.getTestData('56731');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56731 : 应用计算组件详情页->点击操作->点击删除->点击确定->历史版本数量不变', () => {
    app_page.clickLeftNavByText('部署');
    deploy_list_page.delete(`${app_name}-qaimages`);
    deploy_list_page.search(`${app_name}-qaimages`, 0);
    deploy_list_verify_page.verify({ 数量: 0 });
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
    const v_data_1 = {
      历史版本: {
        数量: 0,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
