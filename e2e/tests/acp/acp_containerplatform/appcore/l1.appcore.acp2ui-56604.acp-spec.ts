import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56604');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56604 : 应用更新预览页->输入版本注释->点击更新->成功更新，版本历史中有输入的注释信息', () => {
    const up_data = {
      版本注释: `${app_name}`,
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      历史版本: {
        数量: 1,
        注释: `${app_name}`,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
