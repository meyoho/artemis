import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const imageAddress = ServerConf.TESTIMAGE;
  const daemonset = app_page.getTestData('55446');
  const container_name = app_page.detailAppPage.getImageName(imageAddress);

  beforeAll(() => {
    app_page.prepare.deleteApp(daemonset, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(daemonset, app_page.namespace1Name);
  });
  // the is a bug when create daemonset, disable this case
  it('ACP2UI-55446 : L1 点击创建应用->输入镜像地址->确定->输入应用名称->切换部署模式为Daemonset->点击创建->应用创建成功', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: imageAddress,
      },
      应用: {
        名称: daemonset,
        显示名称: daemonset,
      },
      计算组件: {
        部署模式: 'DaemonSet',
        '组件 - 高级': {
          标签: [['label-name', 'label-value']],
        },
        容器组: {
          //名称: daemonset,
          资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
        },
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${daemonset}`,
      名称: daemonset,
      显示名称: daemonset,
      DaemonSet: {
        名称: daemonset,
        状态: {
          pod名称: `${daemonset}-${container_name}`,
          pod状态: '运行中',
        },
        镜像: imageAddress,
        资源限制: 'CPU: 10m内存: 10Mi',
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
