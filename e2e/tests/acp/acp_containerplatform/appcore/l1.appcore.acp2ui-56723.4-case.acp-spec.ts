import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name: string = app_page.getTestData('56723');
  const data = {
    app_name: app_name,
    ns_name: app_page.namespace1Name,
    pro_name: app_page.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.prepare.createAppByTemplate('alauda.application.tpl.yaml', data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
    app_page.appListPage.toAppDetail(app_name);
  });
  afterAll(() => {
    app_page.prepare.deleteAppByTemplate('alauda.application.tpl.yaml', data);
  });
  it('ACP2UI-56723 : 更新应用->更新应用预览页->修改应用计算组件->修改容器组标签->提交更新->历史版本加1', () => {
    const up_data = {
      修改计算组件: {
        名称: `${app_name}-qaimages`,
        类型: 'Deployment',
        数据: {
          计算组件: {
            容器组: {
              '容器组 - 高级': {
                容器组标签: [['key1', 'value1']],
              },
            },
          },
        },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      历史版本: {
        数量: 1,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56724 : 更新应用->更新应用预览页->修改应用计算组件->修改健康检查->提交更新->历史版本加1', () => {
    const up_data = {
      修改计算组件: {
        名称: `${app_name}-qaimages`,
        类型: 'Deployment',
        数据: {
          计算组件: {
            容器组: {
              健康检查: {
                存活性健康检查: {
                  协议类型: 'HTTP',
                  启动时间: '30',
                  间隔: '5',
                  超时时长: '3',
                },
              },
            },
          },
        },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      历史版本: {
        数量: 2,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56726 : 更新应用->更新应用预览页->修改应用计算组件->修改网络策略->提交更新->历史版本加1', () => {
    const up_data = {
      修改计算组件: {
        名称: `${app_name}-qaimages`,
        类型: 'Deployment',
        数据: {
          计算组件: {
            容器组: {
              '容器组 - 高级': {
                'Host 模式': 'true',
              },
            },
          },
        },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
    );
    const v_data_1 = {
      其他资源不存在: [[`${app_name}-qaimages`, 'Service']],
      历史版本: {
        数量: 3,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
  it('ACP2UI-56730 : 更新应用->更新应用预览页->修改应用计算组件->修改容器信息->提交更新->历史版本加1', () => {
    const up_data = {
      修改计算组件: {
        名称: `${app_name}-qaimages`,
        类型: 'Deployment',
        数据: {
          计算组件: {
            容器组: {
              资源限制: [{ CPU: '11', 单位: 'm' }, { 内存: '11', 单位: 'Mi' }],
            },
          },
        },
      },
    };
    app_page.createAppPage.new_update(
      up_data,
      app_name,
      app_page.namespace1Name,
      false,
    );
    const v_data_1 = {
      历史版本: {
        数量: 4,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data_1);
  });
});
