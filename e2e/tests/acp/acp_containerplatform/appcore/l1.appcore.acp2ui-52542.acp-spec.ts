import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const app_name = app_page.getTestData('52542');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);

  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-52542 : 应用创建页->输入应用名称->修改默认容器名称->点击创建->容器名称修改成功，应用成功创建', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
      },
      计算组件: {
        容器组: {
          名称: '52542',
        },
      },
    };
    app_page.createAppPage.createAppNotWaitRunning(
      data,
      app_page.namespace1Name,
    );
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      Deployment: {
        名称: app_name,
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        容器: '52542',
        镜像: imageAddress,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
