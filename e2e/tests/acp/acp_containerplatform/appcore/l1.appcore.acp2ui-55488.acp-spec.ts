import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { PreparePage } from '@e2e/page_objects/acp/container/storage/prepare.page';

describe('应用ui自动化L1case', () => {
  const app_page = new Application();
  const storage_prepare = new PreparePage();
  const app_name = app_page.getTestData('55488');
  const imageAddress = ServerConf.TESTIMAGE;
  const container_name = app_page.detailAppPage.getImageName(imageAddress);
  const pvc_data = {
    name: app_name,
    namespace: app_page.namespace1Name,
  };
  beforeAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    storage_prepare.deletePvc(app_name, app_page.namespace1Name);
    storage_prepare.createPvcByYaml(pvc_data);
    app_page.login();
    app_page.enterUserView(app_page.namespace1Name);
  });
  beforeEach(() => {
    app_page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    app_page.prepare.deleteApp(app_name, app_page.namespace1Name);
    storage_prepare.deletePvc(app_name, app_page.namespace1Name);
  });
  it('ACP2UI-55488 : 应用创建页->输入应用名称->展开容器组-高级->存储卷-点击添加->输入存储卷名称->选择pvc->点击x关闭->存储卷没有保存', () => {
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      应用: {
        名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          '容器组 - 高级': {
            存储卷x: [
              {
                名称: app_name,
                持久卷声明: app_name,
              },
            ],
          },
        },
      },
    };
    app_page.createAppPage.create(data, app_page.namespace1Name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: '-',
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        实例数量: {
          pod名称: `${app_name}-${container_name}`,
          pod数量: '1',
        },
        镜像: imageAddress,
      },
      YAML内容不包含: [
        '      volumes:',
        `        - name: ${app_name}`,
        '          persistentVolumeClaim:',
        `            claimName: ${app_name}`,
      ],
    };
    app_page.appDetailVerifyPage.verify(v_data);
  });
});
