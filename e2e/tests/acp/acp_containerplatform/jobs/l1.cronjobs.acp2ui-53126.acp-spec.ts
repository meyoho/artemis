import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53126');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('任务记录');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53126 : 定时任务历史列表-操作->验证定时任务列表可操作的各种功能是否正常', () => {
    cronjob.clickLeftNavByText('定时任务');
    cronjob.cronjobListPage.exec_menual(cronjob_name);
    cronjob.clickLeftNavByText('任务记录');
    cronjob.jobListPage.getJobNameFromCronjob(cronjob_name).then(jobname => {
      cronjob.jobListPage.operation(jobname, '查看日志');
      const v_data_1 = {
        标题: `计算/任务记录/${jobname}`,
        日志: [jobname],
      };
      cronjob.jobDetailVerifyPage.verify(v_data_1);
      cronjob.clickLeftNavByText('任务记录');

      cronjob.jobListPage.operation(jobname, '删除');
      const v_data_2 = {
        弹窗: {
          标题: `删除`,
          内容: `确定要删除任务记录${jobname}吗?`,
        },
      };
      cronjob.jobDetailVerifyPage.verify(v_data_2);
    });
  });
});
