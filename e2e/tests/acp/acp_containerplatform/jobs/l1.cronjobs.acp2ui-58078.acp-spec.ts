import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-58078');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-58078 : 定时任务详情页->详细信息tab->点击操作->点击更新->页面成功跳转到更新页', () => {
    cronjob.cronjobListPage.toCronjobDetailPage(cronjob_name);
    cronjob.detailCronjonPage.clickOperation('更新');
    const v_data = {
      标题: `计算/定时任务/更新/${cronjob_name}`,
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
});
