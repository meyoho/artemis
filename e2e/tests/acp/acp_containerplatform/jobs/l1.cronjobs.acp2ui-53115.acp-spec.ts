import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53115');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53115 : 定时任务列表-操作->验证定时任务列表页操作的功能是否正确', () => {
    cronjob.cronjobListPage.operation(cronjob_name, '更新');
    const v_data_1 = {
      标题: `计算/定时任务/更新/${cronjob_name}`,
    };
    cronjob.cronjobDetailVerify.verify(v_data_1);
    cronjob.clickLeftNavByText('定时任务');
    cronjob.cronjobListPage.operation(cronjob_name, '立即执行');
    const v_data_2 = {
      弹窗: { 标题: `确定立即执行定时任务${cronjob_name}吗?`, 内容: '' },
    };
    cronjob.cronjobDetailVerify.verify(v_data_2);
    cronjob.cronjobDetailVerify.auiDialog.clickCancel();
    cronjob.cronjobListPage.operation(cronjob_name, '删除');
    const v_data_3 = {
      弹窗: {
        标题: `确定要删除定时任务 "${cronjob_name}" 吗?`,
        内容: '删除定时任务，将同时删除所生产的任务记录和容器组',
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data_3);
  });
});
