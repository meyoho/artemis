import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';
import { browser } from 'protractor';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53208');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);

    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53208 : 定时任务列表-操作->刷新->刷新功能正常', () => {
    cronjob.cronjobListPage.search(cronjob_name, 0);
    cronjob.cronjobListVerify.verify({
      数量: 0,
    });
    browser.sleep(1).then(() => {
      cronjob.preparePage.createCronjob(cronjob_data);
    });
    cronjob.cronjobListPage.refreshButton.click();
    // cronjob.cronjobListPage.search(cronjob_name, 1);
    cronjob.cronjobListVerify.verify({
      存在: cronjob_name,
    });
  });
  it('ACP2UI-53121 : 后端删除定时任务->验证页面功能是否正常 ', () => {
    cronjob.cronjobListPage.search(cronjob_name, 1);
    cronjob.cronjobListVerify.verify({
      数量: 1,
    });
    browser.sleep(1).then(() => {
      cronjob.preparePage.delete(cronjob_name);
    });
    cronjob.cronjobListPage.refreshButton.click();
    cronjob.cronjobListPage.search(cronjob_name, 0);
    cronjob.cronjobListVerify.verify({
      数量: 0,
    });
  });
});
