import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53118');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  const cronjob_yaml = CommonMethod.readTemplateFile(
    'alauda.cronjob.tpl.yaml',
    cronjob_data,
  );
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53118 : 创建定时任务-yaml方式->验证通过yaml的方式创建定时任务', () => {
    const data = {
      镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      YAML: cronjob_yaml,
    };
    cronjob.createCronjobPage.create(data);
    const v_data_1 = {
      标题: `计算/定时任务/${cronjob_name}`,
    };
    cronjob.cronjobDetailVerify.verify(v_data_1);
  });
});
