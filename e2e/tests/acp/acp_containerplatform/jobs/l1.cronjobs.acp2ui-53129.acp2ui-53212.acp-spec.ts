import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';
import { browser } from 'protractor';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53129');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('任务记录');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53129 : 后端删除任务历史->验证页面功能是否正常|ACP2UI-53212 : 定时任务历史列表->刷新->刷新功能正常', () => {
    cronjob.clickLeftNavByText('定时任务');
    cronjob.cronjobListPage.exec_menual(cronjob_name);
    cronjob.clickLeftNavByText('任务记录');
    cronjob.jobListPage.getJobNameFromCronjob(cronjob_name).then(jobname => {
      cronjob.jobListPage.jobtable.searchByResourceName(jobname, 1);
      cronjob.jobListVerifyPage.verify({ 数量: 1 });
      browser.sleep(1).then(() => {
        cronjob.preparePage.deleteJob(jobname);
      });
      cronjob.jobListPage.refreshButton().click();
      const v_data = {
        不存在: jobname,
      };
      cronjob.jobListVerifyPage.verify(v_data);
    });
  });
});
