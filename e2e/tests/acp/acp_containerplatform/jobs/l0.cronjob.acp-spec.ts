import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';

import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试L0', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('l0-cronjob');

  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    // browser.manage().window().maximize();
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  afterEach(() => {
    console.log('aftereach');
  });
  it('ACP2UI-53206 : 创建定时任务-只填必填项->定时任务能正常运行', () => {
    const data = {
      镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      名称: cronjob_name,
      触发规则: '0-59 * * * *',
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      //标题: `计算/定时任务/${cronjob_name}`,
      名称: cronjob_name,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: '成功 20失败 20',
      任务类型: '单次作业',
      失败重试次数: '6',
      任务超时时间: '7200 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name,
        },
        spec: {
          concurrencyPolicy: 'Allow',
          failedJobsHistoryLimit: 20,
          successfulJobsHistoryLimit: 20,
          schedule: '0-59 * * * *',
          suspend: false,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 7200,
              backoffLimit: 6,
              template: {
                metadata: {
                  annotations: {
                    'sidecar.istio.io/inject': 'false',
                  },
                },
                spec: {
                  restartPolicy: 'Never',
                  containers: [
                    {
                      image: ServerConf.TESTIMAGE,
                      terminationMessagePath: '/dev/termination-log',
                    },
                  ],
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
  it('ACP2UI-53123 : 更新定时任务->验证修改定时任务功能是否正常 ', () => {
    const data = {
      任务超时时间: '600秒',
    };
    cronjob.createCronjobPage.update(cronjob_name, data);
    const v_data = {
      //标题: `计算/定时任务/${cronjob_name}`,
      名称: cronjob_name,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: '成功 20失败 20',
      任务类型: '单次作业',
      失败重试次数: '6',
      任务超时时间: '600 秒',
      镜像: ServerConf.TESTIMAGE,
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
  it('ACP2UI-53124 : 删除定时任务->验证定时任务删除功能是否正常', () => {
    cronjob.cronjobListPage.delete(cronjob_name);
    cronjob.cronjobListPage.search(cronjob_name, 0);
    cronjob.cronjobListVerify.verify({
      数量: 0,
    });
  });
});
