import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-58076');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-58076 : 定时任务详情页->详细信息tab->点击操作->点击立即执行->手动执行成功，页面跳转到任务历史详情页', () => {
    cronjob.cronjobListPage.toCronjobDetailPage(cronjob_name);
    cronjob.detailCronjonPage.execManual();
    const v_data = {
      标题: `计算/任务记录/${cronjob_name}`,
    };
    cronjob.jobDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-58077 : 定时任务详情页->详细信息tab->点击操作->点击删除->定时任务能成功删除，页面跳转到定时任务列表页', () => {
    cronjob.cronjobListPage.toCronjobDetailPage(cronjob_name);
    cronjob.detailCronjonPage.delete();
    cronjob.cronjobListPage.cronjobtable.searchByResourceName(cronjob_name, 0);
    cronjob.cronjobListVerify.verify({ 数量: 0 });
  });
});
