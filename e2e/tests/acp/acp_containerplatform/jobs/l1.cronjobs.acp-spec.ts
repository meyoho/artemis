import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';

import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name_more = cronjob.getTestData('more-cronjob');
  const cronjob_name_parallel = cronjob.getTestData('paralel-cronjob');
  const cronjob_name_single = cronjob.getTestData('single-cronjob');

  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name_more);
    cronjob.preparePage.delete(cronjob_name_parallel);
    cronjob.preparePage.delete(cronjob_name_single);
    // browser.manage().window().maximize();
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name_more);
    cronjob.preparePage.delete(cronjob_name_parallel);
    cronjob.preparePage.delete(cronjob_name_single);
  });
  afterEach(() => {
    console.log('aftereach');
  });
  it('ACP2UI-53107 : 创建定时任务-定时执行-允许触发新任务，当前任务继续执行-固定次数作业->任务会定时执行，旧任务未结束，新任务也能触发执行', () => {
    const data = {
      镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      名称: cronjob_name_more,
      执行方式: '定时触发',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: {
        成功任务历史: '3',
        失败任务历史: '3',
      },
      任务类型: '固定次数作业',
      计划成功次数: '2',
      任务超时时间: '7200秒',
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name_more}`,
      名称: cronjob_name_more,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: '成功 3失败 3',
      任务类型: '固定次数作业',
      失败重试次数: '6',
      最大并行次数: '1',
      计划成功次数: '2',
      任务超时时间: '7200 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name_more,
        },
        spec: {
          concurrencyPolicy: 'Allow',
          failedJobsHistoryLimit: 3,
          successfulJobsHistoryLimit: 3,
          schedule: '0-59 * * * *',
          suspend: false,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name_more,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 7200,
              backoffLimit: 6,
              completions: 2,
              parallelism: 1,
              template: {
                spec: {
                  restartPolicy: 'Never',
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
  it('ACP2UI-53109 : 创建定时任务-定时执行-并行作业-禁止触发新任务，当前任务继续执行->定时任务能定时并行执行执行，旧任务未结束，新任务不能触发', () => {
    const data = {
      镜像: { 方式: '输入', 镜像地址: ServerConf.TESTIMAGE },
      名称: cronjob_name_parallel,
      执行方式: '定时触发',
      触发规则: '0-59 * * * *',
      定时并发策略: '禁止触发新任务，当前任务继续执行',
      保留任务历史: {
        成功任务历史: '4',
        失败任务历史: '4',
      },
      任务类型: '并行作业',
      最大并行次数: '2',
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name_parallel}`,
      名称: cronjob_name_parallel,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '禁止触发新任务，当前任务继续执行',
      保留任务历史: '成功 4失败 4',
      任务类型: '并行作业',
      失败重试次数: '6',
      最大并行次数: '2',
      任务超时时间: '7200 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name_parallel,
        },
        spec: {
          concurrencyPolicy: 'Forbid',
          failedJobsHistoryLimit: 4,
          successfulJobsHistoryLimit: 4,
          schedule: '0-59 * * * *',
          suspend: false,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name_parallel,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 7200,
              backoffLimit: 6,
              parallelism: 2,
              template: {
                spec: {
                  restartPolicy: 'Never',
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });

  it('ACP2UI-53113 : 创建定时任务-定时执行-单次作业-并发策略->允许触发新任务，同时终止执行中的任务->任务能定时触发，旧任务未结束，新任务被触发时会先删除旧任务，然后创建新任务', () => {
    const data = {
      镜像: { 方式: '输入', 镜像地址: ServerConf.TESTIMAGE },
      名称: cronjob_name_single,
      执行方式: '定时触发',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，同时终止执行中的任务',
      保留任务历史: {
        成功任务历史: '2',
        失败任务历史: '2',
      },
      任务类型: '单次作业',
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name_single}`,
      名称: cronjob_name_single,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，同时终止执行中的任务',
      保留任务历史: '成功 2失败 2',
      任务类型: '单次作业',
      失败重试次数: '6',
      任务超时时间: '7200 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name_single,
        },
        spec: {
          concurrencyPolicy: 'Replace',
          failedJobsHistoryLimit: 2,
          successfulJobsHistoryLimit: 2,
          schedule: '0-59 * * * *',
          suspend: false,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name_single,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 7200,
              backoffLimit: 6,
              template: {
                spec: {
                  restartPolicy: 'Never',
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
  it('ACP2UI-53123 : 更新定时任务->验证修改定时任务功能是否正常 ', () => {
    const data = {
      任务超时时间: '600秒',
    };
    cronjob.createCronjobPage.update(cronjob_name_more, data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name_more}`,
      名称: cronjob_name_more,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: '成功 3失败 3',
      任务类型: '固定次数作业',
      计划成功次数: '2',
      最大并行次数: '1',
      失败重试次数: '6',
      任务超时时间: '600 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name_more,
        },
        spec: {
          concurrencyPolicy: 'Allow',
          failedJobsHistoryLimit: 3,
          successfulJobsHistoryLimit: 3,
          schedule: '0-59 * * * *',
          suspend: false,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name_more,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 600,
              backoffLimit: 6,
              completions: 2,
              parallelism: 1,
              template: {
                spec: {
                  restartPolicy: 'Never',
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
  it('ACP2UI-53494 : 定时任务列表->操作->立即执行->手动执行定时任务', () => {
    cronjob.cronjobListPage.exec_menual(cronjob_name_more);
    const data = {
      标题: `计算/任务记录/${cronjob_name_more}`,
      名称: cronjob_name_more,
      所属定时任务: cronjob_name_more,
      任务状态: '执行中',
      计划成功次数: '2',
      最大并行次数: '1',
      失败重试次数: '6',
      任务超时时间: '600 秒',
      镜像: ServerConf.TESTIMAGE,
    };
    cronjob.jobDetailVerifyPage.verify(data);
  });
  it('ACP2UI-53209 : 定时任务列表-操作->点击定时任务名称->页面成功跳转到详情页', () => {
    cronjob.cronjobListPage.toCronjobDetailPage(cronjob_name_more);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name_more}`,
      名称: cronjob_name_more,
      执行方式: '定时',
      触发规则: '0-59 * * * *',
      定时并发策略: '允许触发新任务，当前任务继续执行',
      保留任务历史: '成功 3失败 3',
      任务类型: '固定次数作业',
      计划成功次数: '2',
      最大并行次数: '1',
      失败重试次数: '6',
      任务超时时间: '600 秒',
      镜像: ServerConf.TESTIMAGE,
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });

  it('ACP2UI-53207 : 定时任务列表-操作->搜索->搜索功能正常', () => {
    cronjob.cronjobListPage.search(cronjob_name_more, 1);
    cronjob.cronjobListVerify.verify({
      数量: 1,
    });
    cronjob.cronjobListPage.search('ABCDEF', 0);
    cronjob.cronjobListVerify.verify({
      数量: 0,
    });
  });
  it('ACP2UI-53114 : 定时任务列表-查看->验证定时任务列表ui显示是否正常', () => {
    cronjob.cronjobListPage.clickLeftNavByText('定时任务');
    const v_data = {
      Header: [
        '名称',
        '执行方式',
        '下次触发时间',
        '最近执行时间',
        '创建时间',
        '',
      ],
    };
    cronjob.cronjobListVerify.verify(v_data);
  });
  it('ACP2UI-53125 : 定时任务历史列表-查看->验证定时任务列表页ui显示是否正常 ', () => {
    cronjob.jobListPage.clickLeftNavByText('任务记录');
    const v_data = {
      Header: ['名称', '状态', '所属定时任务', '创建时间', ''],
    };
    cronjob.jobListVerifyPage.verify(v_data);
  });
  it('ACP2UI-53211 : 定时任务历史列表-搜索->搜索功能正常', () => {
    cronjob.jobListPage.search(cronjob_name_more, 1);
    cronjob.jobListVerifyPage.verify({
      数量: 1,
    });
    cronjob.jobListPage.jobtable.searchByResourceName('ABCDEF', 0);
    cronjob.jobListVerifyPage.verify({
      数量: 0,
    });
  });
  it('ACP2UI-53127 : 定时任务历史详情-查看->验证定时任务历史详情页ui显示是否正常', () => {
    cronjob.jobListPage.toDetail(cronjob_name_more).then(jobname => {
      const v_data = {
        标题: `计算/任务记录/${jobname}`,
        名称: jobname,
        所属定时任务: cronjob_name_more,
        计划成功次数: '2',
        失败重试次数: '6',
        // 任务执行超时时间: '600 秒',
        镜像: ServerConf.TESTIMAGE,
      };
      cronjob.jobDetailVerifyPage.verify(v_data);
    });
  });
  it('ACP2UI-53132 : 删除定时任务历史->验证定时任务历史删除功能是否正常 ', () => {
    cronjob.jobListPage.delete(cronjob_name_more).then(jobname => {
      cronjob.jobListPage.jobtable.searchByResourceName(jobname, 0);
      cronjob.jobListVerifyPage.verify({
        数量: 0,
      });
    });
  });
  it('ACP2UI-53124 : 删除定时任务->验证定时任务删除功能是否正常', () => {
    cronjob.cronjobListPage.delete(cronjob_name_more);
    cronjob.cronjobListPage.search(cronjob_name_more, 0);
    cronjob.cronjobListVerify.verify({
      数量: 0,
    });
  });
});
