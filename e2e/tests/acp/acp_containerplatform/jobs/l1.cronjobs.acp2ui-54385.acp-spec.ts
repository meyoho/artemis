import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';
import { PrepareData as SecretPrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const sc_prepare = new SecretPrepareData();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-54385');
  const image = 'index.alauda.cn/alaudaorg/qa__ima-ge.s:helloworld';
  const sc_data = {
    name: cronjob_name,
    namespace: ns_name,
    datas: {
      '.dockerconfigjson': `{"auths":{"index.alauda.cn":{"username":"test","password":"test","email":"test@alauda.io"}}}`,
    },
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    sc_prepare.delete(cronjob_name);
    sc_prepare.create(sc_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
    sc_prepare.delete(cronjob_name);
  });
  it('ACP2UI-54385 : 输入镜像地址-异常case', () => {
    const data = {
      镜像: {
        方式: '输入',
        镜像地址: image,
      },
      名称: cronjob_name,
      执行方式: '手动触发',
      凭据: cronjob_name,
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name}`,
      名称: cronjob_name,
      执行方式: '手动',
      镜像: image,
      JSON: {
        metadata: {
          name: cronjob_name,
        },
        spec: {
          schedule: '1 1 30 2 *',
          suspend: true,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name,
                },
              ]),
            },
            spec: {
              template: {
                spec: {
                  containers: [
                    {
                      image: image,
                    },
                  ],
                  restartPolicy: 'Never',
                  imagePullSecrets: [{ name: cronjob_name }],
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
});
