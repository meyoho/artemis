import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { ServerConf } from '../../../../config/serverConf';
import { PrepareData as ConfigMapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const app_page = new Application();
  const cm_prepare = new ConfigMapPrepareData();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53106');
  const container_name = app_page.detailAppPage.getImageName(
    ServerConf.TESTIMAGE,
  );
  const configmap_data = {
    name: cronjob_name,
    namespace: ns_name,
    datas: {
      SUCCESS_PERCENT: '50',
      SLEEP_SECOND: '10',
    },
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cm_prepare.delete(cronjob_name);
    cm_prepare.create(configmap_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('定时任务');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
    cm_prepare.delete(cronjob_name);
  });
  it('ACP2UI-53106 : 创建定时任务-手动执行-单次作业->任务需要手动触发执行，任务执行时只有一个pod在运行，有一个pod成功，任务就执行成功', () => {
    const data = {
      镜像: {
        方式: '输入',
        镜像地址: ServerConf.TESTIMAGE,
      },
      名称: cronjob_name,
      执行方式: '手动触发',
      保留任务历史: {
        成功任务历史: '3',
        失败任务历史: '3',
      },
      任务类型: '单次作业',
      失败重试次数: '5',
      任务超时时间: '7200秒',
      启动命令: [['python'], ['/entrypoint.py']],
      参数: [['-h']],
      配置引用: [['配置字典', cronjob_name]],
      日志文件: [['/var/*.*']],
      排除日志文件: [['/var/hehe.txt']],
    };
    cronjob.createCronjobPage.create(data);
    const v_data = {
      标题: `计算/定时任务/${cronjob_name}`,
      名称: cronjob_name,
      执行方式: '手动',
      保留任务历史: '成功 3失败 3',
      任务类型: '单次作业',
      失败重试次数: '5',
      任务超时时间: '7200 秒',
      镜像: ServerConf.TESTIMAGE,
      JSON: {
        metadata: {
          name: cronjob_name,
        },
        spec: {
          failedJobsHistoryLimit: 3,
          successfulJobsHistoryLimit: 3,
          schedule: '1 1 30 2 *',
          suspend: true,
          jobTemplate: {
            metadata: {
              labels: cronjob.createLabels([
                {
                  key: `cronjob.${ServerConf.LABELBASEDOMAIN}/name`,
                  value: cronjob_name,
                },
              ]),
            },
            spec: {
              activeDeadlineSeconds: 7200,
              backoffLimit: 5,
              template: {
                spec: {
                  containers: [
                    {
                      image: ServerConf.TESTIMAGE,
                      args: ['-h'],
                      command: ['python', '/entrypoint.py'],
                      name: container_name,
                      env: [
                        { name: '__FILE_LOG_PATH__', value: '/var/*.*' },
                        {
                          name: '__EXCLUDE_LOG_PATH__',
                          value: '/var/hehe.txt',
                        },
                      ],
                      envFrom: [
                        {
                          configMapRef: {
                            name: cronjob_name,
                          },
                        },
                      ],
                    },
                  ],
                  restartPolicy: 'Never',
                },
              },
            },
          },
        },
      },
    };
    cronjob.cronjobDetailVerify.verify(v_data);
  });
});
