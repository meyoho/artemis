import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-53128');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('任务记录');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-53128 : 定时任务历史详情-操作->删除->删除成功，页面跳转到列表页', () => {
    cronjob.clickLeftNavByText('定时任务');
    cronjob.cronjobListPage.exec_menual(cronjob_name);
    cronjob.clickLeftNavByText('任务记录');
    cronjob.jobListPage.getJobNameFromCronjob(cronjob_name).then(jobname => {
      cronjob.jobListPage.toDetail(jobname).then(() => {
        cronjob.jobDetailPage.delete().then(() => {
          cronjob.jobListPage.jobtable.searchByResourceName(jobname, 0);
          const v_data_2 = {
            数量: 0,
          };
          cronjob.jobListVerifyPage.verify(v_data_2);
        });
      });
    });
  });
});
