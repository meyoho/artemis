import { JobPage } from '@e2e/page_objects/acp/container/jobs/jobs.page';
import { ServerConf } from '../../../../config/serverConf';

describe('定时任务自动化测试', () => {
  const cronjob = new JobPage();
  const ns_name = cronjob.namespace1Name;
  const cronjob_name = cronjob.getTestData('acp2ui-59218');
  const cronjob_data = {
    cronjob_name: cronjob_name,
    namespace: ns_name,
    project: cronjob.projectName,
    image: ServerConf.TESTIMAGE,
  };
  beforeAll(() => {
    console.log('beforall');
    cronjob.preparePage.delete(cronjob_name);
    cronjob.preparePage.createCronjob(cronjob_data);
    cronjob.login();
    cronjob.enterUserView(ns_name);
  });
  beforeEach(() => {
    console.log('beforeach');
    cronjob.clickLeftNavByText('任务记录');
  });
  afterAll(() => {
    console.log('afterall');
    cronjob.preparePage.delete(cronjob_name);
  });
  it('ACP2UI-59218 : 定时任务详情页->点击资源限制小铅笔按钮->更新资源限制->更新成功', () => {
    cronjob.clickLeftNavByText('定时任务');
    cronjob.cronjobListPage.toCronjobDetailPage(cronjob_name).then(() => {
      const data = [{ CPU: 11, 单位: 'm' }, { 内存: 11, 单位: 'Mi' }];
      cronjob.detailCronjonPage.updateResourceLimit(data).then(() => {
        const v_data = { 资源限制: '11m11Mi' };
        cronjob.cronjobDetailVerify.verify(v_data);
      });
    });
  });
});
