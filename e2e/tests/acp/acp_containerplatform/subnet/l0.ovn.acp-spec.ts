//TODO 封板后在调试

// import { ServerConf } from '@e2e/config/serverConf';
// import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';

// describe('管理视图 ovn子网L0自动化', () => {
//   const page = new SubnetPage();
//   const subnet_name = page.getTestData('l0-ovn');
//   const region_name = ServerConf.OVNREGIONNAME;
//   if (page.checkRegionNotExist(region_name)) {
//     return;
//   }
//   beforeAll(() => {
//     page.login();
//     page.enterOperationView();
//     page.preparePage.delete(region_name, '166.166.0.0/16');
//   });

//   afterAll(() => {
//     page.preparePage.delete(region_name, '166.166.0.0/16');
//   });

//   it('ACP2UI-54358 : L0:单击左导航子网，点击创建子网，必填名称、网段，保留 IP 为空，网关类型默认分布式，外出流量 NAT 默认开启，子网隔离默认关闭，点击创建', () => {
//     const testData = {
//       名称: subnet_name,
//       网段: '166.166.0.0/16',
//     };
//     page.listPage.createSubnet(region_name, testData);
//     page.detailPage.waitStatus();
//     const expectData = {
//       所属集群: region_name,
//       网段: '166.166.0.0/16',
//       创建人: ServerConf.ADMIN_USER,
//       '已用 IP 数': 0,
//       '可用 IP 数': 65535,
//       '保留 IP': ['166.166.0.1'],
//       网关类型: '分布式',
//       '外出流量 NAT': '是',
//       子网隔离: '否',
//       白名单: ['-'],
//       命名空间: ['-'],
//       '已用 IP': 0,
//     };
//     page.detailPageVerify.verify(expectData);
//   });
//   it('ACP2UI-54357 : 单击左导航子网，在列表页，按名称过滤', async () => {
//     const expectData = {
//       检索: 1,
//       创建: {
//         名称: subnet_name,
//         网段: '166.166.0.0/16',
//       },
//     };
//     await page.listPage.search(region_name, subnet_name);
//     await page.listPageVerify.verify(expectData);
//     await page.listPage.search(region_name, subnet_name.toUpperCase());
//     await page.listPageVerify.verify(expectData);
//     await page.listPage.search(region_name, subnet_name.slice(1, -2));
//     await page.listPageVerify.verify(expectData);
//     await page.listPage.search(region_name, 'notexist', 0);
//     const expectNoData = {
//       检索: 0,
//     };
//     await page.listPageVerify.verify(expectNoData);
//   });
//   it('ACP2UI-54408 : L0:单击左导航子网，在列表页，点击操作-删除，点击确定，验证已删除', async () => {
//     await page.listPage.deleteSubnet(region_name, subnet_name);
//     await page.listPage.search(region_name, subnet_name, 0);
//     const expectData = { 检索: 0 };
//     await page.listPageVerify.verify(expectData);
//   });
// });
