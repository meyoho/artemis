import { ServerConf } from '@e2e/config/serverConf';
import { ResourceManagementPage } from '@e2e/page_objects/acp/resource_management/resource.management';
import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';
import { browser } from 'protractor';

describe('管理视图 calico子网blocksizeL1自动化', () => {
  const page = new SubnetPage();
  const subnet_name = page.getTestData('l1-blocksize');
  const region_name = ServerConf.CALICOREGIONNAME;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  const resourcePage = new ResourceManagementPage();
  beforeAll(() => {
    page.login();
    page.enterOperationView();
    page.preparePage.delete(region_name, subnet_name, '名称');
  });

  afterAll(() => {
    page.preparePage.delete(region_name, subnet_name, '名称');
  });
  it('ACP2UI-55308 : blocksize验证：创建子网 cidr从 1-32，验证资源管理中子网yaml中的blocksize和下表的自动生成的值一致', async () => {
    const Data = [
      { cidr: 10, blocksize: 26 },
      { cidr: 16, blocksize: 26 },
      { cidr: 17, blocksize: 27 },
      { cidr: 19, blocksize: 27 },
      { cidr: 20, blocksize: 28 },
      { cidr: 21, blocksize: 29 },
      { cidr: 22, blocksize: 30 },
      { cidr: 25, blocksize: 30 },
      { cidr: 26, blocksize: 31 },
      { cidr: 31, blocksize: 31 },
    ];
    for (let i = 0; i < Data.length; i++) {
      const testData = {
        名称: subnet_name,
        网段: `118.118.0.0/${Data[i]['cidr']}`,
      };

      console.log(testData.网段);

      await page.listPage.createSubnet(region_name, testData);
      await browser.sleep(2000);
      await resourcePage.listPage.searchCategory('Subnet', region_name);
      await resourcePage.listPage.searchResource(subnet_name);

      await resourcePage.listPage.clickview(subnet_name);

      const expectYaml = { 详情YAML: `blockSize: ${Data[i]['blocksize']}` };
      await resourcePage.detailPageVeriy.verify(expectYaml);

      await page.switchProduct('Container Platform');
      await page.loginPage.waitLoadingDisappear();
      await page.enterOperationView();
      await page.listPage.deleteSubnet(region_name, subnet_name);
    }
  });
});
