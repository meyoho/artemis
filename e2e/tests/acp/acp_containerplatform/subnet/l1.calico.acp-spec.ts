import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { ResourceManagementPage } from '@e2e/page_objects/acp/resource_management/resource.management';
import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';

describe('管理视图 calico子网L1自动化', () => {
  const page = new SubnetPage();
  const subnet_name = page.getTestData('l1-calico');
  const subnet_name2 = page.getTestData('l1-calico-2');
  const region_name = ServerConf.CALICOREGIONNAME;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  const project_name = page.projectName;
  const ns_page = new NamespacePage();
  const namespace_name = `${project_name}-calico`;
  const app_page = new Application();
  const imageAddress = ServerConf.TESTIMAGE;
  const app_name = app_page.getTestData('subnet');
  const app_ip_name = app_page.getTestData('ip');
  const container_name = app_page.detailAppPage.getImageName(imageAddress);
  const resourcePage = new ResourceManagementPage();
  const cidr = '177.177.0.0/16';
  const ip1 = page.ipIsUsed('177.177.177.177', subnet_name, region_name)
    ? '177.177.177.177'
    : page.randomCreateIP(cidr);
  console.log(ip1);
  const ip2 = page.ipIsUsed('177.177.177.178', subnet_name, region_name)
    ? '177.177.177.178'
    : page.randomCreateIP(cidr);
  console.log(ip2);
  beforeAll(() => {
    page.login();
    page.enterOperationView();
    resourcePage.listPage.batchDelete('IP', '177.177', region_name);
    page.preparePage.delete(region_name, cidr);
    page.preparePage.createNamespace(project_name, 'calico', region_name);
  });
  beforeEach(() => {
    page.switchWindow(0);
    page.enterOperationView();
  });
  afterAll(() => {
    resourcePage.listPage.batchDelete('IP', '177.177', region_name);
    page.preparePage.delete(region_name, cidr);
    ns_page.nsPrepare.deleleNs(namespace_name, region_name);
  });

  it('ACP2UI-54780 : 创建子网按钮，输入名称、网段、NAT 关闭，点击创建，验证创建成功，跳转到详情页，详情页各项数据正确', () => {
    const testData = {
      名称: subnet_name,
      网段: cidr,
      '外出流量 NAT': false,
    };
    page.listPage.createSubnet(region_name, testData);
    page.detailPage.waitStatus();
    const expectData = {
      所属集群: region_name,
      网段: cidr,
      '已用 IP 数': 0,
      '可用 IP 数': 65536,
      '外出流量 NAT': '否',
      创建人: ServerConf.ADMIN_USER,
      命名空间: ['-'],
      '已用 IP': 0,
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54788 : l0:列表页点击名称进入详情页，点击操作-更新命名空间，选择一个ns，点击更新，验证更新成功。详情页的命名空间区域及时更新，展示正确', () => {
    const testData = {
      命名空间: namespace_name,
    };
    page.detailPage.updateNamespace(region_name, subnet_name, testData);
    const expectData = { 命名空间: [namespace_name] };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54790 : l0:验证命名空间使用指定子网：进入用户视图创建个应用，验证 IP 是当前子网的', () => {
    page.enterUserView(namespace_name, project_name, region_name);
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: imageAddress,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        实例数量: '1',
      },
    };
    app_page.createAppPage.create(data, namespace_name);
    const v_data = {
      标题: `应用/${app_name}`,
      名称: app_name,
      显示名称: app_name,
      Deployment: {
        名称: app_name,
        状态: {
          pod名称: `${app_name}-${container_name}`,
          pod状态: '运行中',
        },
        镜像: imageAddress,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
    app_page.detailAppPage.getTab('容器组').click();

    app_page.detailAppPage.podListTable
      .getRowCell(0, 0)
      .getText()
      .then(name => {
        const pod_name = name;
        console.log(pod_name);
        app_page.detailAppPage.podListTable
          .getRowCell(0, 4)
          .getText()
          .then(ip => {
            const pod_ip = ip;
            console.log(pod_ip);
            page.enterOperationView();
            page.detailPage.searchIp(region_name, subnet_name, pod_ip);
            const expectData = {
              过滤IP: 1,
              容器组: `${namespace_name}/${pod_name}`,
            };
            page.detailPageVerify.verify(expectData);
          });
      });
  });
  it('ACP2UI-54798 : l0:验证命名空间使用指定子网内的 IP：创建应用固定 IP，验证只能输入指定子网内未使用的 IP，能正常运行', () => {
    page.enterUserView(namespace_name, project_name, region_name);
    const data = {
      选择镜像: {
        方式: '输入',
        镜像地址: imageAddress,
      },
      应用: {
        名称: app_ip_name,
        显示名称: app_ip_name,
      },
      计算组件: {
        部署模式: 'Deployment',
        容器组: {
          '容器组 - 高级': {
            '固定 IP': [ip1, ip2],
          },
        },
      },
    };
    app_page.createAppPage.create(data, namespace_name);
    const v_data = {
      标题: `应用/${app_ip_name}`,
      名称: app_ip_name,
      显示名称: app_ip_name,
      Deployment: {
        名称: app_ip_name,
        状态: {
          pod名称: `${app_ip_name}-${container_name}`,
          pod状态: '运行中',
        },
        镜像: imageAddress,
      },
    };
    app_page.appDetailVerifyPage.verify(v_data);
    app_page.detailAppPage.getTab('容器组').click();

    app_page.detailAppPage.podListTable
      .getRowCell(0, 0)
      .getText()
      .then(name => {
        const pod_name = name;
        console.log(pod_name);
        app_page.detailAppPage.podListTable
          .getRowCell(0, 4)
          .getText()
          .then(ip => {
            const pod_ip = ip;
            console.log(pod_ip);
            expect([ip1, ip2].includes(pod_ip)).toBeTruthy();
            page.enterOperationView();
            page.detailPage.searchIp(region_name, subnet_name, pod_ip);
            const expectData = {
              过滤IP: 1,
              容器组: `${namespace_name}/${pod_name}`,
            };
            page.detailPageVerify.verify(expectData);
          });
      });
  });
  it('ACP2UI-54789 : l0:子网详情-使用 IP，展示的地址、节点、容器（ns/podname）正确，按 IP过滤（已使用 IP、模糊 IP有结果，未使用 IP、错误 IP无结果）', () => {
    const expectNoData = {
      过滤IP: 0,
    };
    page.detailPage.searchIp(region_name, subnet_name, '166.166.166.166', 0);
    page.detailPageVerify.verify(expectNoData);
    page.detailPage.searchIp(region_name, subnet_name, 'notexist', 0);
    page.detailPageVerify.verify(expectNoData);
    page.detailPage.searchIp(region_name, subnet_name, '177.177.0.1', 0);
    page.detailPageVerify.verify(expectNoData);
    page.detailPage.searchIp(region_name, subnet_name, '177.177', 3);
    const expectData = {
      过滤IP: 3,
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54785 : 列表页点击名称进入详情页，面包屑（网络/子网/名称）两个Tab(详细信息、使用 IP 未使用时，IP 列表为空地址、节点、容器组)', () => {
    page.listPage.enterDetail(region_name, subnet_name);
    const expectData = {
      面包屑: `网络/子网/${subnet_name}`,
      所属集群: region_name,
      网段: cidr,
      '已用 IP 数': 3,
      '可用 IP 数': 65533,
      '外出流量 NAT': '否',
      创建人: ServerConf.ADMIN_USER,
      命名空间: [namespace_name],
      '已用 IP': 3,
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54774 : 创建子网按钮，输入名称、网段，点击取消，验证未创建，列表中没有数据', () => {
    const testData = {
      名称: subnet_name2,
      网段: '167.166.0.0/16',
      '外出流量 NAT': false,
    };
    page.listPage.createSubnet(region_name, testData, 'cancel');
    page.listPage.search(region_name, subnet_name2, 0);
    const expectNoData = {
      检索: 0,
    };
    page.listPageVerify.verify(expectNoData);
  });
  it('ACP2UI-54775 : 创建子网按钮，输入名称、网段，点击关闭，验证未创建，列表中没有数据', () => {
    const testData = {
      名称: subnet_name2,
      网段: '167.166.0.0/16',
      '外出流量 NAT': 'true',
    };
    page.listPage.createSubnet(region_name, testData, 'close');
    page.listPage.search(region_name, subnet_name2, 0);
    const expectNoData = {
      检索: 0,
    };
    page.listPageVerify.verify(expectNoData);
  });
  it('ACP2UI-54783 : 列表页，点击操作-删除，点击取消，验证未删除，列表页还存在', () => {
    page.listPage.deleteSubnet(region_name, subnet_name, 1, 'cancel');
    page.listPage.search(region_name, subnet_name, 1);
    const expectData = { 检索: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-54804 : 列表页点击名称进入详情页，点击操作-删除，点击取消，验证删除取消，停留在详情页，进入列表页还可以搜索到 ', () => {
    page.detailPage.delete(region_name, subnet_name, 'cancel');
    const expectData = {
      面包屑: `网络/子网/${subnet_name}`,
      所属集群: region_name,
      网段: cidr,
    };
    page.detailPageVerify.verify(expectData);
    page.listPage.search(region_name, subnet_name);
    const expectData2 = { 检索: 1 };
    page.listPageVerify.verify(expectData2);
  });
  it('ACP2UI-54803 : l0:列表页点击名称进入详情页，点击操作-删除，点击删除，验证删除成功，返回列表页，子网不存在', () => {
    page.enterUserView(namespace_name, project_name, region_name);
    app_page.appListPage.deleteAppByName(app_name);
    app_page.appListPage.appTable.searchByResourceName(app_name, 0);
    app_page.appListPage.deleteAppByName(app_ip_name);
    app_page.appListPage.appTable.searchByResourceName(app_ip_name, 0);
    page.enterOperationView();
    resourcePage.listPage.batchDelete('IP', '177.177', region_name);
    page.detailPage.delete(region_name, subnet_name);
    page.listPage.search(region_name, subnet_name, 0);
    const expectData = { 检索: 0 };
    page.listPageVerify.verify(expectData);
  });
});
