import { ServerConf } from '@e2e/config/serverConf';
import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';
import { ProjectPage } from '@e2e/page_objects/platform/project/project.page';
import { browser } from 'protractor';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';
describe('管理视图 macvlan子网L1自动化', () => {
  const page = new SubnetPage();
  const subnet_name = page.getTestData('l1-macvlan');
  const subnet_name2 = page.getTestData('l1-mac-ips');
  const subnet_name3 = page.getTestData('l1-mac-many');
  const subnet_name4 = page.getTestData('l1-cancel');
  const subnet_name5 = page.getTestData('l1-close');
  const region_name = ServerConf.MACVLANREGIONNAME;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  const projectPage = new ProjectPage();
  const project_name: string = page.getTestData('l1-macvlan');
  const namespace_page = new NamespacePage();
  const namespace_name = `${project_name}-ns`;
  const deployment_page = new DeploymentPage();
  const deployment_name = page.getTestData('macvlan');
  const deployment_name_ip = page.getTestData('mac-ip');
  const imageAddress = ServerConf.TESTIMAGE;
  const isReady = ['int'].includes(process.env.ENV);
  if (isReady) {
    const cidr = '166.166.166.0/29';
    const ip1 = page.ipIsUsed('166.166.166.3', subnet_name, region_name)
      ? '166.166.166.3'
      : page.randomCreateIP(cidr);
    console.log(ip1);
    const ip2 = page.ipIsUsed('166.166.166.4', subnet_name, region_name)
      ? '166.166.166.4'
      : page.randomCreateIP(cidr);
    console.log(ip2);
    beforeAll(() => {
      page.login();
      page.enterOperationView();
      page.preparePage.delete(region_name, '166.166.166.0/29');
      page.preparePage.delete(region_name, '166.166.167.0/29');
      page.preparePage.delete(region_name, '166.166.168.0/29');
      page.switchSetting('项目管理');
      const create_project_data = {
        名称: project_name,
        显示名称: project_name,
        描述: project_name,
        所属集群: `${region_name} (${page.clusterDisplayName(region_name)})`,
      };
      const project_quota = {
        CPU: '1',
        内存: '1',
        存储: '1',
        'PVC 数': '1',
        'Pods 数': '10',
      };
      const namespace_data = {
        基本信息: {
          所属集群: region_name,
          命名空间: 'ns',
        },
        容器限额: {
          CPU: {
            默认值: '10',
            最大值: '100',
          },
          内存: {
            默认值: '10',
            最大值: '124',
          },
        },
      };
      projectPage.listPage.searchContext().search(project_name);
      browser.sleep(5000);
      projectPage.listPage.projectsTable.getRowCount().then(count => {
        if (count === 0) {
          projectPage.createPlatformProject(create_project_data, project_quota);
        } else {
          projectPage.listPage.enterProject(project_name);
        }
        namespace_page.nsCreatePage.create_ns(namespace_data);
      });
    });
    beforeEach(() => {
      page.switchWindow(0);
      page.enterOperationView();
    });
    afterAll(() => {
      page.enterOperationView();
      page.preparePage.delete(region_name, '166.166.167.0/29');
      page.preparePage.delete(region_name, '166.166.168.0/29');
      page.preparePage.delete(region_name, '166.166.166.0/29');
      // page.switchWindow(1);
      // projectPage.deleteProject(project_name);
    });
    it('ACP2UI-56987 : L0:创建子网按钮，输入名称、网段、网关、输入保留ip段，点击创建，验证创建成功，跳转到详情页，详情页各项数据正确', () => {
      const testData = {
        名称: subnet_name2,
        网段: '166.166.167.0/29',
        网关: '166.166.167.1',
        '保留 IP': ['166.166.167.2..166.166.167.4'],
      };
      page.listPage.createSubnet(region_name, testData);
      const expectData = {
        所属集群: region_name,
        网段: '166.166.167.0/29',
        网关: '166.166.167.1',
        '已用 IP 数': 0,
        '可用 IP 数': 2,
        创建人: ServerConf.ADMIN_USER,
        '保留 IP': ['166.166.167.2..166.166.167.4'],
      };
      page.detailPageVerify.verify(expectData);
    });
    it('ACP2UI-56988 : L0:创建子网按钮，输入名称、网段、网关、输入多个保留ip和ip段，点击创建，验证创建成功，跳转到详情页，详情页各项数据正确', () => {
      const testData = {
        名称: subnet_name3,
        网段: '166.166.168.0/29',
        网关: '166.166.168.1',
        '保留 IP': ['166.166.168.2..166.166.168.4', '166.166.168.5'],
      };
      page.listPage.createSubnet(region_name, testData);
      const expectData = {
        所属集群: region_name,
        网段: '166.166.168.0/29',
        网关: '166.166.168.1',
        '已用 IP 数': 0,
        '可用 IP 数': 1,
        创建人: ServerConf.ADMIN_USER,
        '保留 IP': ['166.166.168.2..166.166.168.4', '166.166.168.5'],
      };
      page.detailPageVerify.verify(expectData);
    });
    it('ACP2UI-56953 : 创建子网按钮，输入名称、网关、网段，点击取消，验证未创建，列表中没有数据', () => {
      const testData = {
        名称: subnet_name4,
        网段: '166.166.169.0/29',
        网关: '166.166.169.1',
      };
      page.listPage.createSubnet(region_name, testData, 'cancel');
      page.listPage.search(region_name, subnet_name, 0);
      const expectData = { 检索: 0 };
      page.listPageVerify.verify(expectData);
    });
    it('ACP2UI-56954 : 创建子网按钮，输入名称、网段、网关，点击关闭，验证未创建，列表中没有数据', () => {
      const testData = {
        名称: subnet_name5,
        网段: '166.166.169.0/29',
        网关: '166.166.169.1',
      };
      page.listPage.createSubnet(region_name, testData, 'close');
      page.listPage.search(region_name, subnet_name, 0);
      const expectData = { 检索: 0 };
      page.listPageVerify.verify(expectData);
    });
    it('ACP2UI-56967 : l0:子网列表页点击名称进入详情页，点击操作-更新网关，验证名称和网段是只读，网关和保留ip可以更新成功。详情页及时更新，展示正确', () => {
      const testData = {
        网关: '166.166.167.7',
        '保留 IP': ['166.166.167.3'],
      };
      page.detailPage.updateGateWay(region_name, subnet_name2, testData);
      const expectData = {
        网段: '166.166.167.0/29',
        网关: '166.166.167.7',
        '已用 IP 数': 0,
        '可用 IP 数': 5,
        '保留 IP': ['166.166.167.3'],
      };
      page.detailPageVerify.verify(expectData);
    });
    it('ACP2UI-56978 : l0:列表页点击名称进入详情页，点击操作-删除，点击删除，验证删除成功，返回列表页，子网不存在', () => {
      page.detailPage.delete(region_name, subnet_name2);
      page.listPage.search(region_name, subnet_name, 0);
      const expectData = { 检索: 0 };
      page.listPageVerify.verify(expectData);
    });
    it('ACP2UI-56979 : 列表页点击名称进入详情页，点击操作-删除，点击取消，验证删除取消，停留在详情页，进入列表页还可以搜索到', () => {
      page.detailPage.delete(region_name, subnet_name3, 'cancel');
      page.listPage.search(region_name, subnet_name3, 1);
      const expectData = { 检索: 1 };
      page.listPageVerify.verify(expectData);
    });
    it('ACP2UI-56952 : L0:创建子网按钮，输入名称、网段、网关、输入一个保留ip，点击创建，验证创建成功，跳转到详情页，详情页各项数据正确', () => {
      const testData = {
        名称: subnet_name,
        网段: '166.166.166.0/29',
        网关: '166.166.166.1',
        '保留 IP': ['166.166.166.2'],
      };
      page.listPage.createSubnet(region_name, testData);
      const expectData = {
        所属集群: region_name,
        网段: '166.166.166.0/29',
        网关: '166.166.166.1',
        '已用 IP 数': 0,
        '可用 IP 数': 4,
        创建人: ServerConf.ADMIN_USER,
        '保留 IP': ['166.166.166.2'],
      };
      page.detailPageVerify.verify(expectData);
    });
    it('ACP2UI-56964 : L0:创建应用，验证能正常运行，使用集群随机子网内的一个随机 IP，主机和pod能相互访问', () => {
      page.enterUserView(namespace_name, project_name, region_name);
      const testData = {
        名称: deployment_name,
        子网: subnet_name,
      };
      deployment_page.createPage.create(testData, imageAddress);
      const expectDetailData = {
        状态: '运行中',
      };
      deployment_page.detailPageVerify.verify(expectDetailData);
      deployment_page.detailPage.podListTable
        .getRowCell(0, 0)
        .getText()
        .then(name => {
          const pod_name = name;
          console.log(pod_name);
          deployment_page.detailPage.podListTable
            .getRowCell(0, 3)
            .getText()
            .then(ip => {
              const pod_ip = ip;
              console.log(pod_ip);
              page.enterOperationView();
              page.detailPage.searchIp(region_name, subnet_name, pod_ip);
              const expectData = {
                过滤IP: 1,
                容器组: `${namespace_name}/${pod_name}`,
              };
              page.detailPageVerify.verify(expectData);
            });
        });
      page.listPage.enterDetail(region_name, subnet_name);
      const expectData = {
        网段: '166.166.166.0/29',
        '已用 IP 数': 1,
        '可用 IP 数': 3,
      };
      page.detailPageVerify.verify(expectData);
      page.enterUserView(namespace_name, project_name, region_name);
      deployment_page.listPage.delete(deployment_name);
    });
    it('ACP2UI-56965 : L0:创建应用，选择子网使用固定 IP，验证可以输入子网内的任意未使用的IP，能正常运行', () => {
      page.enterUserView(namespace_name, project_name, region_name);
      const testData = {
        名称: deployment_name_ip,
        子网: subnet_name,
        '固定 IP': [ip1, ip2],
      };
      deployment_page.createPage.create(testData, imageAddress);
      const expectDetailData = {
        状态: '运行中',
      };
      deployment_page.detailPageVerify.verify(expectDetailData);
      deployment_page.detailPage.podListTable
        .getRowCell(0, 0)
        .getText()
        .then(name => {
          const pod_name = name;
          console.log(pod_name);
          deployment_page.detailPage.podListTable
            .getRowCell(0, 3)
            .getText()
            .then(ip => {
              const pod_ip = ip;
              console.log(pod_ip);
              expect(testData['固定 IP'].includes(pod_ip)).toBeTruthy();
              page.enterOperationView();
              page.detailPage.searchIp(region_name, subnet_name, pod_ip);
              const expectData = {
                过滤IP: 1,
                容器组: `${namespace_name}/${pod_name}`,
              };
              page.detailPageVerify.verify(expectData);
            });
        });
      page.enterUserView(namespace_name, project_name, region_name);
      deployment_page.listPage.delete(deployment_name_ip);
    });
  }
});
