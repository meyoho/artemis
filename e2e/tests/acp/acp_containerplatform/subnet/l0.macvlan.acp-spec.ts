import { ServerConf } from '@e2e/config/serverConf';
import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';

describe('管理视图 macvlan子网L0自动化', () => {
  const page = new SubnetPage();
  const subnet_name = page.getTestData('l0-macvlan');
  const region_name = ServerConf.MACVLANREGIONNAME;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  const isReady = ['int'].includes(process.env.ENV);
  if (isReady) {
    beforeAll(() => {
      page.login();
      page.enterOperationView();
      page.preparePage.delete(region_name, '166.166.166.0/30');
    });
    beforeEach(() => {});
    afterAll(() => {
      page.preparePage.delete(region_name, '166.166.166.0/30');
    });
    it('ACP2UI-56951 : L0:创建子网-输入名称、网段，网关，点击创建，验证创建成功，跳转到详情页，详情页的基本信息正确（所属集群、网关、网段、创建人、已用 IP、可用 IP、创建时间)', () => {
      const testData = {
        名称: subnet_name,
        网段: '166.166.166.0/30',
        网关: '166.166.166.1',
      };
      page.listPage.createSubnet(region_name, testData);
      const expectData = {
        所属集群: region_name,
        网段: '166.166.166.0/30',
        网关: '166.166.166.1',
        '已用 IP 数': 0,
        '可用 IP 数': 1,
        创建人: ServerConf.ADMIN_USER,
        '保留 IP': ['-'],
        '已用 IP': 0,
      };
      page.detailPageVerify.verify(expectData);
    });
    it('ACP2UI-56960 : 列表页-按名称过滤 正确名称、模糊名称、大小写名称，验证搜索结果正确；错误名称，验证结果无数据', () => {
      const expectData = {
        检索: 1,
        创建: {
          名称: subnet_name,
          网段: '166.166.166.0/30',
        },
      };
      page.listPage.search(region_name, subnet_name);
      page.listPageVerify.verify(expectData);
      page.listPage.search(region_name, subnet_name.toUpperCase());
      page.listPageVerify.verify(expectData);
      page.listPage.search(region_name, subnet_name.slice(1, -2));
      page.listPageVerify.verify(expectData);
      page.listPage.search(region_name, 'notexist', 0);
      const expectNoData = {
        检索: 0,
      };
      page.listPageVerify.verify(expectNoData);
    });
    it('ACP2UI-56961 : l0:列表页，点击操作-删除，点击删除，验证删除成功，列表页及时更新不显示', () => {
      page.listPage.deleteSubnet(region_name, subnet_name);
      page.listPage.search(region_name, subnet_name, 0);
      const expectData = { 检索: 0 };
      page.listPageVerify.verify(expectData);
    });
  }
});
