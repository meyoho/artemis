import { ServerConf } from '@e2e/config/serverConf';
import { SubnetPage } from '@e2e/page_objects/acp/subnet/subnet.page';

describe('管理视图 calico子网L0自动化', () => {
  const page = new SubnetPage();
  const subnet_name = page.getTestData('l0-calico');
  const region_name = ServerConf.CALICOREGIONNAME;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  beforeAll(() => {
    page.login();
    page.enterOperationView();
    page.preparePage.delete(region_name, '166.166.0.0/16');
  });

  afterAll(() => {
    page.preparePage.delete(region_name, '166.166.0.0/16');
  });

  it('ACP2UI-54773 : l0创建子网按钮，输入名称、网段，点击创建，验证创建成功，跳转到详情页，详情页的基本信息正确', async () => {
    const testData = {
      名称: subnet_name,
      网段: '166.166.0.0/16',
    };
    await page.listPage.createSubnet(region_name, testData);
    // page.detailPage.waitStatus();
    const expectData = {
      所属集群: region_name,
      网段: '166.166.0.0/16',
      '已用 IP 数': 0,
      '可用 IP 数': 65536,
      '外出流量 NAT': '是',
      创建人: ServerConf.ADMIN_USER,
      命名空间: ['-'],
      '已用 IP': 0,
    };
    await page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54784 : 列表页-按名称过滤', async () => {
    const expectData = {
      检索: 1,
      创建: {
        名称: subnet_name,
        网段: '166.166.0.0/16',
      },
    };
    await page.listPage.search(region_name, subnet_name);
    await page.listPageVerify.verify(expectData);
    await page.listPage.search(region_name, subnet_name.toUpperCase());
    await page.listPageVerify.verify(expectData);
    await page.listPage.search(region_name, subnet_name.slice(1, -2));
    await page.listPageVerify.verify(expectData);
    await page.listPage.search(region_name, 'notexist', 0);
    const expectNoData = {
      检索: 0,
    };
    await page.listPageVerify.verify(expectNoData);
  });
  it('ACP2UI-54781 : l0:列表页，点击操作-删除，点击删除，验证删除成功，列表页及时更新不显示', async () => {
    await page.listPage.deleteSubnet(region_name, subnet_name);
    await page.listPage.search(region_name, subnet_name, 0);
    const expectData = { 检索: 0 };
    await page.listPageVerify.verify(expectData);
  });
});
