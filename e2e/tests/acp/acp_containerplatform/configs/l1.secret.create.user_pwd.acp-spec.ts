import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52517');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52517 : Secret管理-创建Secret-Secret创建成功(类型选择用户名/密码)(UI)', () => {
    const testData = {
      名称: name,
      描述: 'description',
      类型: '用户名/密码',
      用户名: `example@alauda.io`,
      密码: `&&$$.*12L`,
    };
    page.createPage.create(testData);

    const expectData = {
      名称: name,
      类型: '用户名/密码',
      描述: 'description',
      数据: [
        {
          键: 'username',
          值: `example@alauda.io`,
        },
        {
          键: 'password',
          值: `&&$$.*12L`,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
