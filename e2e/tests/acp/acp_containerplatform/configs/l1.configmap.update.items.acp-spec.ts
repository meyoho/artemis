import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';

describe('configmap自动化测试', () => {
  const page = new ConfigPage();
  const name = page.getTestData('52502');
  const data = {
    name: name,
    namespace: page.namespace1Name,
    datas: {
      key1: 'value1',
      key2: 'value2',
    },
  };
  beforeAll(() => {
    page.prepareData.delete(name);
    page.prepareData.create(data);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('配置字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52502 : 配置项更新', () => {
    page.listPage.configmapTable.clickResourceNameByRow([name]);
    page.detailPage.updateIterm('key2', 'value3');
    const expectData = {
      名称: name,
      配置项: [{ 键: 'key1', 值: 'value1' }, { 键: 'key2', 值: 'value3' }],
    };
    page.detailPageVerify.verify(expectData);
  });
});
