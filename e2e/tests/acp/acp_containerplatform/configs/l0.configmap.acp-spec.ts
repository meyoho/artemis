import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';

describe('configmap自动化测试', () => {
  const page = new ConfigPage();

  const name = page.getTestData('52494');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52494 : 创建配置-表单', () => {
    const testData = {
      名称: name,
      描述: 'description',
      配置项: [{ 键: 'key1', 值: 'value1' }],
    };
    page.createPage.create(testData).then(isPresent => {
      if (isPresent) {
        const expectData = {
          名称: name,
          描述: 'description',
          配置项: [{ 键: 'key1', 值: 'value1' }],
        };
        page.detailPageVerify.verify(expectData);
      } else {
        throw new Error('Configmap 创建成功后，没有自动进入详情页');
      }
    });
  });
  it('ACP2UI-52504 : 更新配置', () => {
    page.listPage.operation(name, '更新');
    const testData = {
      描述: 'descriptiontest',
    };
    page.updatePage.update(name, testData);
    const expectData = {
      描述: 'descriptiontest',
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53492 : 配置字典列表->查看配置列表是否正常', () => {
    page.clickLeftNavByText('配置字典');

    const expectVaue = {
      Header: ['名称\n描述\n创建时间'],
    };

    page.listPageVerify.verify(expectVaue);
  });
  it('ACP2UI-53491 : 配置字典列表->搜索->验证搜索功能是否正常', () => {
    page.listPage.search(name, 1);
    const expectVaue = {
      数量: 1,
    };

    page.listPageVerify.verify(expectVaue);
  });
  it('ACP2UI-52505 : 删除配置', () => {
    page.listPage.delete(name);
    page.listPage.search(name, 0);
    const expectVaue = {
      数量: 0,
    };

    page.listPageVerify.verify(expectVaue);
  });
});
