import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';
// import { CommonMethod } from '@e2e/utility/common.method';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52524');
  const Data = {
    name: name,
    namespace: page.namespace1Name,
    datatype: 'Opaque',
    datas: {
      key1: 'value1',
      key2: 'value2',
    },
  };

  beforeAll(() => {
    page.prepareData.delete(name);
    page.prepareData.create(Data);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52524 : Secret管理-更新Secret页-取消更新-返回到Secret详情页(UI) ', () => {
    const testData = {
      描述: 'description',
      数据: [{ 键: 'key1', 值: 'value3' }],
    };
    page.createPage.update_cancel(name, testData);
    page.listPage.viewDatail(name);

    const expectData = {
      名称: name,
      描述: '-',
      数据: [{ 键: 'key1', 值: 'value1' }, { 键: 'key2', 值: 'value2' }],
    };
    page.detailPageVerify.verify(expectData);
  });
});
