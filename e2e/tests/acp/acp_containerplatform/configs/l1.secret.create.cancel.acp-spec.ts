import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52521');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52521 : Secret管理-创建Secret页-取消创建-返回列表页(UI)', () => {
    const testData = {
      名称: name,
      描述: 'description',
      数据: [{ 键: 'key1', 值: 'value1' }],
    };
    page.createPage.create_cancel(testData);

    page.listPage.search(name, 0);
    const expectVaue = {
      数量: 0,
    };

    page.listPageVerify.verify(expectVaue);
  });
});
