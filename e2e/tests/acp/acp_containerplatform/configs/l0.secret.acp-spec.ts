import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52514');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52514 : Secret管理-创建Secret-Secret创建成功(类型选择Opaque)(UI)', () => {
    const testData = {
      名称: name,
      描述: 'description',
      数据: [{ 键: 'key1', 值: 'value1' }],
    };
    page.createPage.create(testData);

    const expectData = {
      名称: name,
      类型: 'Opaque',
      描述: 'description',
      数据: [{ 键: 'key1', 值: 'value1' }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52522 : Secret管理-更新Secret-更新成功(UI)', () => {
    page.createPage.update(name, { 描述: name });
    const expectData = {
      名称: name,
      描述: name,
      数据: [{ 键: 'key1', 值: 'value1' }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53493 : secret列表->查看secret列表->验证数据展示', () => {
    const expectVaue = {
      Header: ['名称\n类型\n描述\n创建时间'],
    };

    page.listPageVerify.verify(expectVaue);
  });
  it('ACP2UI-52526 : Secret管理列表页-搜索Secret(UI)', () => {
    page.listPage.search(name, 1);
    const expectVaue = {
      数量: 1,
    };

    page.listPageVerify.verify(expectVaue);
  });
  it('ACP2UI-52525 : Secret管理-Secret详情页 ', () => {
    page.listPage.viewDatail(name);
    const expectData = {
      名称: name,
      // 描述: 'description',
      数据: [{ 键: 'key1', 值: 'value1' }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52528 : Secret管理-删除Secret-删除成功', () => {
    page.listPage.delete(name);
    const expectVaue = {
      数量: 0,
    };

    page.listPageVerify.verify(expectVaue);
  });
});
