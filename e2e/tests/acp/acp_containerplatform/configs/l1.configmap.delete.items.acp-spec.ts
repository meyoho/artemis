import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';

describe('configmap自动化测试', () => {
  const page = new ConfigPage();
  const name = page.getTestData('52503');
  const data = {
    name: name,
    namespace: page.namespace1Name,
    datas: {
      key1: 'value1',
      key2: 'value2',
    },
  };
  beforeAll(() => {
    page.prepareData.delete(name);
    page.prepareData.create(data);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('配置字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52503 : 配置项删除', () => {
    page.listPage.configmapTable.clickResourceNameByRow([name]);
    page.detailPage.deleteIterm('key2');
    // 配置项只有键没有值时，校验键不存在
    const expectData = {
      名称: name,
      配置项: [{ 键: 'key1', 值: 'value1' }, { 键: 'key2' }],
    };
    page.detailPageVerify.verify(expectData);
  });
});
