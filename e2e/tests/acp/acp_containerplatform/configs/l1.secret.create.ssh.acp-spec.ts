import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52516');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52516 : Secret管理-创建Secret-Secret创建成功(类型选择SSH)(UI)', () => {
    const testData = {
      名称: name,
      描述: 'description',
      类型: 'SSH 认证',
      'SSH 私钥': `-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAACFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAgEAuMg7zdeCDowaJqsakrxkgRBUaO3taSTsiphwLrDme3HieZiK+b/z
zqn3IBKeh/tkE0F/xNJIgpzlLN76sRCY1DbLB1mdVklX81ihE/0OoimjdAa8IinE0YzcAg
hXaCUUiQAp6q5viZryEblfpzpCNXe00Z+DzaBP3GQ16VmjYjb6GvJ3evyMWPeE4RyUsDT/
A4+QJs+mwYV0JmaRSPJi5FK9J5dqlMY2wDP+n0vsz41wszxNxIx9+ornk0g77DAIc3jtFe
45CZV3UWfPe/Ev44YLsGvfq01UJXKqc+rTdPRXyo6o2uZdVvSnMEtJn7cV+3eQXmj/qkG9
Aya+ldcrmF1J+SMlJIjPjOdHNwGdHg6yDGZHvzxGh61/E20lnGjqzdPKyXqqpgYPwSZXry
JghwaKcXlP3fQsCndoEPkNXhCgdwLAxtzUnxuQA4SnOU5SGqE1+iW/gSfAuUWAV35BAQLB
31VJbwZ+Q3Z881P5Y0LufFR/oFoNz07GdTuOpOeat3mbb2QaVvs3fZ9muJ3O1zoQefzdPE
u/2gn44d3JqiXwDhzRmoQurJX1q/eBafjxIAWxkUbZKUgbPqk9dIpDgq8ierZovcOZOw4/
kQ2v1Ui/NN7nX8Py9Sc+cprQxtmHpJeCi6W2zWXGfUuIdskdykq7FxsAIym6NSXE+xAFuB
UAAAdQWkh/nlpIf54AAAAHc3NoLXJzYQAAAgEAuMg7zdeCDowaJqsakrxkgRBUaO3taSTs
iphwLrDme3HieZiK+b/zzqn3IBKeh/tkE0F/xNJIgpzlLN76sRCY1DbLB1mdVklX81ihE/
0OoimjdAa8IinE0YzcAghXaCUUiQAp6q5viZryEblfpzpCNXe00Z+DzaBP3GQ16VmjYjb6
GvJ3evyMWPeE4RyUsDT/A4+QJs+mwYV0JmaRSPJi5FK9J5dqlMY2wDP+n0vsz41wszxNxI
x9+ornk0g77DAIc3jtFe45CZV3UWfPe/Ev44YLsGvfq01UJXKqc+rTdPRXyo6o2uZdVvSn
MEtJn7cV+3eQXmj/qkG9Aya+ldcrmF1J+SMlJIjPjOdHNwGdHg6yDGZHvzxGh61/E20lnG
jqzdPKyXqqpgYPwSZXryJghwaKcXlP3fQsCndoEPkNXhCgdwLAxtzUnxuQA4SnOU5SGqE1
+iW/gSfAuUWAV35BAQLB31VJbwZ+Q3Z881P5Y0LufFR/oFoNz07GdTuOpOeat3mbb2QaVv
s3fZ9muJ3O1zoQefzdPEu/2gn44d3JqiXwDhzRmoQurJX1q/eBafjxIAWxkUbZKUgbPqk9
dIpDgq8ierZovcOZOw4/kQ2v1Ui/NN7nX8Py9Sc+cprQxtmHpJeCi6W2zWXGfUuIdskdyk
q7FxsAIym6NSXE+xAFuBUAAAADAQABAAACAQC2sWJPli+EaojAipNvWMyVvqt2QydjuZoV
PbpMr6Jxkpu0VVmyrFJFlk47a61KDQdY8n18/9upJ65+usdpoVs5FiOOVM/2q/VFJ++6b4
y0UC7HXJFNxbZO6NHtQIoK8f5npb3LxkOI7aVAWON112f2rTAdwXTzLPlIkdurp3CefnSx
h+ERu/iXTcIXP8bSoNMxlhOUC+J3m5bHEMm8McoMrLFQH6jFB2TCi6XHoa21V8aBr7HEDp
PPzt4/BEAauTLh2EGwWSQTka+y0MyXYlNSxQlxmHZAS+hSThixGw+OprbWEk5ofzLx8loK
qaVVQBN/2Srn7TA6CVzzbK8qPt9dyVMcvfRdeZvUTZpZUWdoso2F0jfN4A13G9uK3cCPwT
1SygXUB/jY677dkk0tQkwONInepjanDpCuZKyMCHVEg0i/fX+YujmgpLXoJu0hMHucqaeb
sTILkJTypBJgEzkltcQNgCJ0KFH6driWfnmLqEmviYywmRAg6fMviTUtTXdvkltfmespUV
xpXvZylEM7Ahp9FupdktSEy+9EmP3Hgp76ndtQshK0BO1keb8LK6XZwC4GPYyf7r4qMxRi
y8mxuI03I6b6JNyJ3Zo4DFt5xwPLoBp9c3+r009aA4XRBD4FN+HfLHy4aaLdj459Uj01uk
ubjhPw5rrfzA6o/0g6AQAAAQA+Roh3R6WhkeED/nMq7cUQ1j24+eT06Cz1ub4Upp532Bm6
ePjHl7QQblNNTZbTT+0jy+JvGWp8/vRhu0D3e0HqR8lsdeX3L1FVFvUX8Axym6FKKL5uRK
IjvDJ6wZqOidEAa1gAf24MeiqCy4aaRzPJISdQP7taKXLtdlcAgPbSCCVBKuee1R9INDDH
2dZgEiXxNrQJCF25i9cUXeXwgU1HwcVhsFFP/xE/YJrKze8f8w/SXBiG55o3ywKC6wTECA
CQwfMvdGRYjixCk8tGFj3qlG0aI6li4tEGLWiQy8/9kTJOS2RPXCTuk2hpIl6M/G1iP5pX
OraRFnA56pz4JxO7AAABAQDwBfpSilaG5JKa3DLe4oOTn4HN5VMXub9WSzog+7fJo3JxVY
jbZctN4qV+FXkXqQhWHz0v7gNkPmdxiKC238RedLYAtceHATnCEGb+vD/SbRGeh2J4NDgl
S3O5MTOjQSlCaHdLSy9HQW7rJ+Ul/bGTdSl+Y9T3rrzXfZfz6DAFICimtyZk8okvMyP9HV
0L1fAKcTXu/pd72RzU9ptsC3iuEyoQKLkOAmstBLZqACFlp9PFoDwrwDllFFobX+sn4d3J
/BdLxT+/83HmJpwzwvibp3Vk/XklbX3leQT16KMw+EcHDChjZwgUYySan7VpyhzRVH0HwD
X5V5E4HBu65UDhAAABAQDFFPC60rxAnuyLOetLKsSa4EDbfsoMIt8HdrPSiueJ6IEsWDag
k8Hn2yVrT0hD64icgZG/dd8n/XPH9YdoFeAlQy3ys5BfQEk96VE3ZrsZ9SJmz8IzqTrOx1
+mvom32QKRM1rHegU7UV0daEB0PsFnErJa+6hXUmfgF7nAjz9I5d//vv9Asn95FuiMBSzw
eEAHGJ+pRGJqyFm9B2S09h6hAVQj1fEqLS39LAbkfTPcuIxvkuEoXGlzDR2ZhKKG4qxcfG
hgGu119Yj0X1qxvSyx6BcTOvdlBiPhyMoJGe8OctmtUrkcHXcAprAnTvPori8grMsfV2ID
aNte/gRmVPm1AAAAF3lvdXIyLmVtYWlsQGV4YW1wbGUuY29tAQID
-----END OPENSSH PRIVATE KEY-----`,
    };
    page.createPage.create(testData);

    const expectData = {
      名称: name,
      类型: 'SSH 认证',
      描述: 'description',
      数据: [
        {
          键: 'ssh-privatekey',
          值: `-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAACFwAAAAdzc2gtcn
NhAAAAAwEAAQAAAgEAuMg7zdeCDowaJqsakrxkgRBUaO3taSTsiphwLrDme3HieZiK+b/z
zqn3IBKeh/tkE0F/xNJIgpzlLN76sRCY1DbLB1mdVklX81ihE/0OoimjdAa8IinE0YzcAg
hXaCUUiQAp6q5viZryEblfpzpCNXe00Z+DzaBP3GQ16VmjYjb6GvJ3evyMWPeE4RyUsDT/
A4+QJs+mwYV0JmaRSPJi5FK9J5dqlMY2wDP+n0vsz41wszxNxIx9+ornk0g77DAIc3jtFe
45CZV3UWfPe/Ev44YLsGvfq01UJXKqc+rTdPRXyo6o2uZdVvSnMEtJn7cV+3eQXmj/qkG9
Aya+ldcrmF1J+SMlJIjPjOdHNwGdHg6yDGZHvzxGh61/E20lnGjqzdPKyXqqpgYPwSZXry
JghwaKcXlP3fQsCndoEPkNXhCgdwLAxtzUnxuQA4SnOU5SGqE1+iW/gSfAuUWAV35BAQLB
31VJbwZ+Q3Z881P5Y0LufFR/oFoNz07GdTuOpOeat3mbb2QaVvs3fZ9muJ3O1zoQefzdPE
u/2gn44d3JqiXwDhzRmoQurJX1q/eBafjxIAWxkUbZKUgbPqk9dIpDgq8ierZovcOZOw4/
kQ2v1Ui/NN7nX8Py9Sc+cprQxtmHpJeCi6W2zWXGfUuIdskdykq7FxsAIym6NSXE+xAFuB
UAAAdQWkh/nlpIf54AAAAHc3NoLXJzYQAAAgEAuMg7zdeCDowaJqsakrxkgRBUaO3taSTs
iphwLrDme3HieZiK+b/zzqn3IBKeh/tkE0F/xNJIgpzlLN76sRCY1DbLB1mdVklX81ihE/
0OoimjdAa8IinE0YzcAghXaCUUiQAp6q5viZryEblfpzpCNXe00Z+DzaBP3GQ16VmjYjb6
GvJ3evyMWPeE4RyUsDT/A4+QJs+mwYV0JmaRSPJi5FK9J5dqlMY2wDP+n0vsz41wszxNxI
x9+ornk0g77DAIc3jtFe45CZV3UWfPe/Ev44YLsGvfq01UJXKqc+rTdPRXyo6o2uZdVvSn
MEtJn7cV+3eQXmj/qkG9Aya+ldcrmF1J+SMlJIjPjOdHNwGdHg6yDGZHvzxGh61/E20lnG
jqzdPKyXqqpgYPwSZXryJghwaKcXlP3fQsCndoEPkNXhCgdwLAxtzUnxuQA4SnOU5SGqE1
+iW/gSfAuUWAV35BAQLB31VJbwZ+Q3Z881P5Y0LufFR/oFoNz07GdTuOpOeat3mbb2QaVv
s3fZ9muJ3O1zoQefzdPEu/2gn44d3JqiXwDhzRmoQurJX1q/eBafjxIAWxkUbZKUgbPqk9
dIpDgq8ierZovcOZOw4/kQ2v1Ui/NN7nX8Py9Sc+cprQxtmHpJeCi6W2zWXGfUuIdskdyk
q7FxsAIym6NSXE+xAFuBUAAAADAQABAAACAQC2sWJPli+EaojAipNvWMyVvqt2QydjuZoV
PbpMr6Jxkpu0VVmyrFJFlk47a61KDQdY8n18/9upJ65+usdpoVs5FiOOVM/2q/VFJ++6b4
y0UC7HXJFNxbZO6NHtQIoK8f5npb3LxkOI7aVAWON112f2rTAdwXTzLPlIkdurp3CefnSx
h+ERu/iXTcIXP8bSoNMxlhOUC+J3m5bHEMm8McoMrLFQH6jFB2TCi6XHoa21V8aBr7HEDp
PPzt4/BEAauTLh2EGwWSQTka+y0MyXYlNSxQlxmHZAS+hSThixGw+OprbWEk5ofzLx8loK
qaVVQBN/2Srn7TA6CVzzbK8qPt9dyVMcvfRdeZvUTZpZUWdoso2F0jfN4A13G9uK3cCPwT
1SygXUB/jY677dkk0tQkwONInepjanDpCuZKyMCHVEg0i/fX+YujmgpLXoJu0hMHucqaeb
sTILkJTypBJgEzkltcQNgCJ0KFH6driWfnmLqEmviYywmRAg6fMviTUtTXdvkltfmespUV
xpXvZylEM7Ahp9FupdktSEy+9EmP3Hgp76ndtQshK0BO1keb8LK6XZwC4GPYyf7r4qMxRi
y8mxuI03I6b6JNyJ3Zo4DFt5xwPLoBp9c3+r009aA4XRBD4FN+HfLHy4aaLdj459Uj01uk
ubjhPw5rrfzA6o/0g6AQAAAQA+Roh3R6WhkeED/nMq7cUQ1j24+eT06Cz1ub4Upp532Bm6
ePjHl7QQblNNTZbTT+0jy+JvGWp8/vRhu0D3e0HqR8lsdeX3L1FVFvUX8Axym6FKKL5uRK
IjvDJ6wZqOidEAa1gAf24MeiqCy4aaRzPJISdQP7taKXLtdlcAgPbSCCVBKuee1R9INDDH
2dZgEiXxNrQJCF25i9cUXeXwgU1HwcVhsFFP/xE/YJrKze8f8w/SXBiG55o3ywKC6wTECA
CQwfMvdGRYjixCk8tGFj3qlG0aI6li4tEGLWiQy8/9kTJOS2RPXCTuk2hpIl6M/G1iP5pX
OraRFnA56pz4JxO7AAABAQDwBfpSilaG5JKa3DLe4oOTn4HN5VMXub9WSzog+7fJo3JxVY
jbZctN4qV+FXkXqQhWHz0v7gNkPmdxiKC238RedLYAtceHATnCEGb+vD/SbRGeh2J4NDgl
S3O5MTOjQSlCaHdLSy9HQW7rJ+Ul/bGTdSl+Y9T3rrzXfZfz6DAFICimtyZk8okvMyP9HV
0L1fAKcTXu/pd72RzU9ptsC3iuEyoQKLkOAmstBLZqACFlp9PFoDwrwDllFFobX+sn4d3J
/BdLxT+/83HmJpwzwvibp3Vk/XklbX3leQT16KMw+EcHDChjZwgUYySan7VpyhzRVH0HwD
X5V5E4HBu65UDhAAABAQDFFPC60rxAnuyLOetLKsSa4EDbfsoMIt8HdrPSiueJ6IEsWDag
k8Hn2yVrT0hD64icgZG/dd8n/XPH9YdoFeAlQy3ys5BfQEk96VE3ZrsZ9SJmz8IzqTrOx1
+mvom32QKRM1rHegU7UV0daEB0PsFnErJa+6hXUmfgF7nAjz9I5d//vv9Asn95FuiMBSzw
eEAHGJ+pRGJqyFm9B2S09h6hAVQj1fEqLS39LAbkfTPcuIxvkuEoXGlzDR2ZhKKG4qxcfG
hgGu119Yj0X1qxvSyx6BcTOvdlBiPhyMoJGe8OctmtUrkcHXcAprAnTvPori8grMsfV2ID
aNte/gRmVPm1AAAAF3lvdXIyLmVtYWlsQGV4YW1wbGUuY29tAQID
-----END OPENSSH PRIVATE KEY-----`,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
