import { ServerConf } from '@e2e/config/serverConf';
import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';

describe('configmap自动化测试', () => {
  const page = new ConfigPage();
  let app_yaml_name: string;
  const name = page.getTestData('52496');
  const data = {
    name: name,
    namespace: page.namespace1Name,
    datas: {
      key1: 'value1',
    },
  };
  beforeAll(() => {
    page.prepareData.delete(name);
    page.prepareData.create(data);
    app_yaml_name = CommonKubectl.createResourceByTemplate(
      'alauda.application.tpl.yaml',
      {
        app_name: 'configmap-test',
        ns_name: page.namespace1Name,
        image: ServerConf.TESTIMAGE,
        cm_name: name,
        volumes: [
          {
            name: name,
            cm_name: name,
          },
        ],
        volume_mounts: [
          {
            name: name,
            readOnly: true,
            subPath: 'key1',
          },
        ],
        pro_name: page.projectName,
      },
      'alauda.configmap-application',
      ServerConf.REGIONNAME,
    );
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('配置字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
    browser.sleep(100).then(() => {
      CommonKubectl.deleteResourceByYmal(app_yaml_name, ServerConf.REGIONNAME);
    });
  });
  afterEach(() => {});
  it('ACP2UI-52496 : 查看配置-详情信息->内容展示正确，计算组件tab页 deployment 信息展示正确', () => {
    browser.sleep(5000);
    page.listPage.configmapTable.clickResourceNameByRow([name]);
    const v_data = {
      名称: name,
      配置项: [{ 键: 'key1', 值: 'value1' }],
    };
    page.detailPageVerify.verify(v_data);
    page.detailPage.clickTab('工作负载');
    const v_data_1 = {
      计算组件数量: 1,
      计算组件: [[['configmap-test'], '类型', 'Deployment']],
    };
    page.detailPageVerify.verify(v_data_1);
  });
});
