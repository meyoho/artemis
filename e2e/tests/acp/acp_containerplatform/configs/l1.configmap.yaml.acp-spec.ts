import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('configmap自动化测试', () => {
  const page = new ConfigPage();
  const name = page.getTestData('52495');
  const data = {
    name: name,
    namespace: page.namespace1Name,
    datas: {
      key1: 'value1',
    },
  };
  const create_data = CommonMethod.readTemplateFile(
    'alauda.configmap.yaml',
    data,
  );
  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('配置字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52495 : 创建配置-yaml', () => {
    page.createPage.create_by_yaml(create_data);
    const expectData = {
      名称: name,
      配置项: [{ 键: 'key1', 值: 'value1' }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52497 : 查看配置-yaml', () => {
    page.listPage.configmapTable.clickResourceNameByRow([name]);

    const expectData = CommonMethod.parseYaml(create_data);
    page.detailPageVerify.verify(expectData);
  });
});
