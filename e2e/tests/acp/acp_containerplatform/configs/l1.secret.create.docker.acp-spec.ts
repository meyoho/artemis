import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('secret自动化测试', () => {
  const page = new SecretPage();

  const name = page.getTestData('52518');

  beforeAll(() => {
    page.prepareData.delete(name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('保密字典');
  });
  afterAll(() => {
    page.prepareData.delete(name);
  });
  afterEach(() => {});
  it('ACP2UI-52518 : Secret管理-创建Secret-Secret创建成功(类型选择镜像服务)(UI)', () => {
    const testData = {
      名称: name,
      描述: 'description',
      类型: '镜像服务',
      地址: 'index.alauda.cn',
      用户名: `example`,
      密码: `&&$$.*12L`,
      邮箱: 'example@alauda.io',
    };
    page.createPage.create(testData);

    const expectData = {
      名称: name,
      类型: '镜像服务',
      描述: 'description',
      数据: [
        {
          键: '.dockerconfigjson',
          值: `{"auths":{"index.alauda.cn":{"username":"example","password":"&&$$.*12L","email":"example@alauda.io"}}}`,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
