import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const job_name = page.getTestData('acp2ui-58604');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const job_data = {
    name: job_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deleteJob(job_name);
    page.podPreparePage.createJob(job_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deleteJob(job_name);
  });
  it('ACP2UI-58604 : 容器组列表->点击操作->点击删除->查看确认提示信息->提示信息正确', () => {
    page.podListPage.searchPod(job_name, 1).then(() => {
      page.podListPage.deletePod(0, '').then(podName => {
        const v_data = {
          删除确认信息: `确认删除容器组 “${podName}” 吗？`,
        };
        page.podListVerifyPage.verify(v_data);
      });
    });
  });
});
