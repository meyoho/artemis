import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58608');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-58608 : 容器组详情页->查看状态分析->展示类型、状态、原因、消息、最近更新时间', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          const expectData = {
            面包屑: `计算/容器组/${pod_name}`,
            状态分析: {
              表头: ['类型', '状态', '原因', '信息', '最后更新时间'],
            },
          };
          page.podDetailVerifyPage.verify(expectData);
        });
    });
  });
});
