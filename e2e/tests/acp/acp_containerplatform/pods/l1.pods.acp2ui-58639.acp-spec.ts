import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('58639');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const deploy_data = {
    app_name: `dp-${pod_name}`,
    ns_name: namespace_name,
    pro_name: page.projectName,
    image: imageAddress,
  };
  const statefulset_data = {
    app_name: `s-${pod_name}`,
    ns_name: namespace_name,
    pro_name: page.projectName,
    image: imageAddress,
    kind: 'StatefulSet',
  };
  const daemonset_data = {
    app_name: `d-${pod_name}`,
    ns_name: namespace_name,
    pro_name: page.projectName,
    image: imageAddress,
    kind: 'DaemonSet',
  };
  const tapp_data = {
    name: `t-${pod_name}`,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  const job_data = {
    name: `j-${pod_name}`,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deleteApp(deploy_data);
    page.podPreparePage.deleteApp(statefulset_data);
    page.podPreparePage.deleteApp(daemonset_data);
    page.podPreparePage.deleteTapp(tapp_data.name);
    page.podPreparePage.deleteJob(job_data.name);
    page.podPreparePage.createApp(deploy_data);
    page.podPreparePage.createApp(statefulset_data);
    page.podPreparePage.createApp(daemonset_data);
    page.podPreparePage.createTapp(tapp_data);
    page.podPreparePage.createJob(job_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deleteApp(deploy_data);
    page.podPreparePage.deleteApp(statefulset_data);
    page.podPreparePage.deleteApp(daemonset_data);
    page.podPreparePage.deleteTapp(tapp_data.name);
    page.podPreparePage.deleteJob(job_data.name);
  });
  it('ACP2UI-59037|ACP2UI-58639 : 容器组详情页->基本信息，来源显示正确->点击名称能正确跳转到指定的计算组件详情页', () => {
    page.podListPage.searchPodByKey(deploy_data.app_name).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([deploy_data.app_name])
        .then(() => {
          const v_data = {
            来源: `${deploy_data.app_name}-qaimages (部署)`,
          };
          page.podDetailVerifyPage.verify(v_data);
          page.podDetailPage.toWorkloadDetailPage().then(() => {
            page.podDetailPage.isNotInPage().then(() => {
              const v_data = {
                面包屑: `计算/部署/${deploy_data.app_name}-qaimages`,
              };
              page.podDetailVerifyPage.verify(v_data);
            });
          });
        });
    });
    page.clickLeftNavByText('容器组');
    page.podListPage.searchPodByKey(statefulset_data.app_name).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([statefulset_data.app_name])
        .then(() => {
          const v_data = {
            来源: `${statefulset_data.app_name}-qaimages (有状态副本集)`,
          };
          page.podDetailVerifyPage.verify(v_data);
          page.podDetailPage.toWorkloadDetailPage().then(() => {
            page.podDetailPage.isNotInPage().then(() => {
              const v_data = {
                面包屑: `计算/有状态副本集/${statefulset_data.app_name}-qaimages`,
              };
              page.podDetailVerifyPage.verify(v_data);
            });
          });
        });
    });
    page.clickLeftNavByText('容器组');
    page.podListPage.searchPodByKey(daemonset_data.app_name).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([daemonset_data.app_name])
        .then(() => {
          const v_data = {
            来源: `${daemonset_data.app_name}-qaimages (守护进程集)`,
          };
          page.podDetailVerifyPage.verify(v_data);
          page.podDetailPage.toWorkloadDetailPage().then(() => {
            page.podDetailPage.isNotInPage().then(() => {
              const v_data = {
                面包屑: `计算/守护进程集/${daemonset_data.app_name}-qaimages`,
              };
              page.podDetailVerifyPage.verify(v_data);
            });
          });
        });
    });
    page.clickLeftNavByText('容器组');
    page.podListPage.searchPodByKey(tapp_data.name).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([tapp_data.name])
        .then(() => {
          const v_data = {
            来源: `${tapp_data.name} (Tapp)`,
          };
          page.podDetailVerifyPage.verify(v_data);
          page.podDetailPage.toWorkloadDetailPage().then(() => {
            page.podDetailPage.isNotInPage().then(() => {
              const v_data = {
                面包屑: `计算/Tapp/${tapp_data.name}`,
              };
              page.podDetailVerifyPage.verify(v_data);
            });
          });
        });
    });
    page.clickLeftNavByText('容器组');
    page.podListPage.searchPodByKey(job_data.name).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([job_data.name])
        .then(() => {
          const v_data = {
            来源: `${job_data.name} (任务)`,
          };
          page.podDetailVerifyPage.verify(v_data);
          page.podDetailPage.toWorkloadDetailPage().then(() => {
            page.podDetailPage.isNotInPage().then(() => {
              const v_data = {
                面包屑: `计算/任务记录/${job_data.name}`,
              };
              page.podDetailVerifyPage.verify(v_data);
            });
          });
        });
    });
  });
});
