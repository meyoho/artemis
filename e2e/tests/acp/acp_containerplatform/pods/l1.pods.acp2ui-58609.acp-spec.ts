import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58609');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.podPreparePage.waitPodRunning(pod_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-58609 : 容器组详情页->查看容器组信息页面展示->容器信息分tab页展示，容器信息展示正确', () => {
    page.podPreparePage.waitPodRunning(pod_name).then(() => {
      page.podListPage.searchPod(pod_name, 1).then(() => {
        page.podListPage.podListTable
          .clickResourceNameByRow([pod_name])
          .then(() => {
            const expectData = {
              面包屑: `计算/容器组/${pod_name}`,
              名称: pod_name,
              容器: {
                'init-container': {
                  镜像: imageAddress,
                  资源限制: '10m10Mi',
                  就绪: 'True',
                  重启次数: '0',
                  状态: 'terminated(Completed)',
                  信息: '-',
                  启动命令: 'echo共2条',
                  参数: '-',
                },
                helloworld: {
                  镜像: imageAddress,
                  资源限制: '30m30Mi',
                  状态: 'running',
                  已挂载存储卷: {
                    表头: ['存储卷名称', '子路径', '挂载路径', '只读', ''],
                    数据: [
                      ['shared', '-', '/home/mnt', '否'],
                      [
                        'default-token-bxwwt',
                        '-',
                        '/var/run/secrets/kubernetes.io/serviceaccount',
                        '是',
                      ],
                    ],
                  },
                  端口: {
                    表头: ['协议', '容器端口', '主机端口', ''],
                    数据: [['TCP', '80']],
                  },
                  存活性健康检查: {
                    健康检查类型: '存活性',
                    协议类型: 'HTTP',
                    启动时间: '15',
                    间隔: '20',
                    超时时长: '1',
                    正常阈值: '1',
                    不正常阈值: '3',
                    协议: 'HTTP',
                    端口: '80',
                    路径: '/',
                    请求头: '无请求头',
                  },
                  可用性健康检查: {
                    健康检查类型: '可用性',
                    协议类型: 'HTTP',
                    启动时间: '5',
                    间隔: '10',
                    超时时长: '1',
                    正常阈值: '1',
                    不正常阈值: '3',
                    协议: 'HTTP',
                    端口: '80',
                    路径: '/',
                    请求头: '无请求头',
                  },
                },
              },
            };
            page.podDetailVerifyPage.verify(expectData);
          });
      });
    });
  });
});
