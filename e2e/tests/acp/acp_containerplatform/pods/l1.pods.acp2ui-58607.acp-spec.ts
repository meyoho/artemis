import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58607');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-58607 : 容器组列表->点击名称进入详情页->查看容器组详情页展示->详情页信息展示正确', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          const expectData = {
            面包屑: `计算/容器组/${pod_name}`,
            名称: pod_name,
            容器组标签: [`pod/name: ${pod_name}.${namespace_name}`],
            主机选择器: [
              'beta.kubernetes.io/arch=amd64',
              'beta.kubernetes.io/os=linux',
              '， ',
            ],
            凭据: ['alaudak8s'],
            'Service Account': 'default',
            'Host 模式': '关闭',
            来源: '-',
            创建人: 'admin',
            创建时间: '',
            存储卷: {
              表头: ['名称', '类型', '相关配置', ''],
              存在数据: [
                ['shared', '空目录'],
                ['default-token-bxwwt', '保密字典', 'alaudak8s'],
              ],
            },
            'Pod 亲和': {
              表头: ['类型', '类别', '权重', '主机拓扑域', '匹配标签'],
              存在数据: [['Pod 反亲和', 'Required', 'kubernetes.io/hostname']],
            },
          };
          page.podDetailVerifyPage.verify(expectData);
        });
    });
  });
});
