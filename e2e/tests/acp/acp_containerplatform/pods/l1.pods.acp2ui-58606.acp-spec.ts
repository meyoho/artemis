import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const job_name = page.getTestData('acp2ui-58606');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const job_data = {
    name: job_name,
    namespace: namespace_name,
    project: page.projectName,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deleteJob(job_name);
    page.podPreparePage.createJob(job_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deleteJob(job_name);
  });
  it('ACP2UI-58606 : 容器组列表->点击操作->点击删除->点击取消->确认提示关闭，没有删除 ', () => {
    page.podListPage.searchPod(job_name, 1).then(() => {
      page.podListPage.deletePod(0, '取消').then(podName => {
        const v_data = {
          存在Pod: [podName],
        };
        page.podListVerifyPage.verify(v_data);
      });
    });
  });
});
