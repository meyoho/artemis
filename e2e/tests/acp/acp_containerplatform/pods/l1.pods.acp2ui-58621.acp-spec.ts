import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';
import { ElementFinder } from 'protractor';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58621');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-59032|ACP2UI-58621 : 容器组详情页->容器详情->点击日志->页面跳转到日志tab，且展示指定的容器的日志', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          page.podDetailPage.containers.clickTab('helloworld').then(() => {
            const button = page.podDetailPage.containers.getElementByText(
              '日志',
            ) as ElementFinder;
            button.click().then(() => {
              const expectData = { 日志: ['helloworld'] };
              page.podDetailVerifyPage.verify(expectData);
            });
          });
        });
    });
  });
});
