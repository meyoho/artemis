import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58612');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-58612 : 容器组详情页->点击操作->点击删除->成功删除容器组，页面跳转到容器组列表页', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          page.podDetailPage.deletePod().then(podName => {
            page.podListPage.searchPod(pod_name, 1).then(() => {
              page.podListPage.waitPodNotExist(podName);
              const expectData = {
                面包屑: `计算/容器组`,
                不存在Pod: [podName],
              };
              page.podListVerifyPage.verify(expectData);
            });
          });
        });
    });
  });
});
