import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L0自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58597');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-58597 : 查看容器组列表->帮助信息和容器组列表信息展示正确', () => {
    const expectDetailData = {
      面包屑: `计算/容器组`,
      提示信息:
        '容器组即 Kubernetes Pod，是在 Kubernetes 中可以创建和管理的最小计算单元。',
      表头: [
        '名称',
        '状态',
        '资源限制',
        '重启次数',
        '容器组 IP',
        '节点名称',
        '创建时间',
        '',
      ],
      存在Pod: [pod_name],
    };
    page.podListVerifyPage.verify(expectDetailData);
  });
  it('ACP2UI-58889 : 容器组列表->点击名称进入详情页 ', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          const expectData = {
            面包屑: `计算/容器组/${pod_name}`,
            名称: pod_name,
            容器组标签: [`pod/name: ${pod_name}.${namespace_name}`],
            凭据: ['alaudak8s'],
            'Service Account': 'default',
            'Host 模式': '关闭',
            来源: '-',
            存储卷: {
              表头: ['名称', '类型', '相关配置', ''],
              存在数据: [
                ['shared', '空目录'],
                ['default-token-bxwwt', '保密字典', 'alaudak8s'],
              ],
            },
            'Pod 亲和': {
              表头: ['类型', '类别', '权重', '主机拓扑域', '匹配标签'],
              存在数据: [['Pod 反亲和', 'Required', 'kubernetes.io/hostname']],
            },
            容器: {
              'init-container': {
                镜像: imageAddress,
                资源限制: '10m10Mi',
              },
              helloworld: {
                镜像: imageAddress,
                资源限制: '30m30Mi',
                已挂载存储卷: {
                  表头: ['存储卷名称', '子路径', '挂载路径', '只读', ''],
                  数据: [
                    ['shared', '-', '/home/mnt', '否'],
                    [
                      'default-token-bxwwt',
                      '-',
                      '/var/run/secrets/kubernetes.io/serviceaccount',
                      '是',
                    ],
                  ],
                },
                端口: {
                  表头: ['协议', '容器端口', '主机端口', ''],
                  数据: [['TCP', '80']],
                },
                存活性健康检查: {
                  健康检查类型: '存活性',
                  协议类型: 'HTTP',
                  启动时间: '15',
                  间隔: '20',
                  超时时长: '1',
                  正常阈值: '1',
                  不正常阈值: '3',
                  协议: 'HTTP',
                  端口: '80',
                  路径: '/',
                  请求头: '无请求头',
                },
                可用性健康检查: {
                  健康检查类型: '可用性',
                  协议类型: 'HTTP',
                  启动时间: '5',
                  间隔: '10',
                  超时时长: '1',
                  正常阈值: '1',
                  不正常阈值: '3',
                  协议: 'HTTP',
                  端口: '80',
                  路径: '/',
                  请求头: '无请求头',
                },
              },
            },
          };
          page.podDetailVerifyPage.verify(expectData);
        });
    });
  });
  it('ACP2UI-58605 : 容器组列表->点击操作->点击删除->点击确认->删除成功', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.deletePod(0).then(() => {
        const v_data = {
          不存在Pod: [pod_name],
        };
        page.podListVerifyPage.verify(v_data);
      });
    });
  });
});
