import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';
import { PrepareData as SecretPrepareData } from '@e2e/page_objects/acp/container/secret/prepare.page';
import { PrepareData as ConfigmapPrepareData } from '@e2e/page_objects/acp/container/conifgmap/prepare.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-58615');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const configmap_prepare = new ConfigmapPrepareData();
  const secret_prepare = new SecretPrepareData();
  const configmap_data = {
    name: pod_name,
    namespace: namespace_name,
    datas: {
      ckey1: 'cvalue1',
      ckey2: 'cvalue2',
    },
  };
  const secret_data = {
    name: pod_name,
    namespace: namespace_name,
    datas: {
      skey1: 'svalue1',
      skey2: 'svalue2',
    },
  };
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
    configmap_name: pod_name,
    configmap_key: 'ckey1',
    secret_name: pod_name,
    secret_key: 'skey1',
  };

  beforeAll(() => {
    configmap_prepare.delete(configmap_data.name, configmap_data.namespace);
    secret_prepare.delete(secret_data.name, secret_data.namespace);
    page.podPreparePage.deletePod(pod_name);
    configmap_prepare.create(configmap_data);
    secret_prepare.create(secret_data);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
    configmap_prepare.delete(configmap_data.name, configmap_data.namespace);
    secret_prepare.delete(secret_data.name, secret_data.namespace);
  });
  it('ACP2UI-58615|ACP2UI-59026 : 容器组详情页->点击配置tab->配置信息只能查看->没有编辑入口', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          page.podDetailPage.clickTab('配置').then(() => {
            const v_data = {
              配置: {
                '': {
                  环境变量: {
                    更新按钮: '无',
                    表头: ['键', '值/引用值'],
                    数据: [
                      ['POD_NAME', pod_name],
                      ['NAMESPACE', 'fieldRef: metadata.namespace'],
                      ['CPU_LIMITS', 'resourceFieldRef: limits.cpu'],
                      ['SECRET_KEY', '保密字典 acp-jlli-acp2ui-58615: skey1'],
                      [
                        'CONFIGMAP_KEY',
                        '配置字典 acp-jlli-acp2ui-58615: ckey1',
                      ],
                    ],
                  },
                  配置引用: {
                    更新按钮: '无',
                    表头: ['类型', '名称'],
                    数据: [['配置字典', pod_name], ['保密字典', pod_name]],
                  },
                },
              },
            };
            page.podDetailVerifyPage.verify(v_data);
          });
        });
    });
  });
});
