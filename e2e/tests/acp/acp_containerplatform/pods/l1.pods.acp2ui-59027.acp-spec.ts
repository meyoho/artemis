import { ServerConf } from '@e2e/config/serverConf';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';

describe('用户视图 容器组 L1自动化', () => {
  const page = new PodsPage();
  const pod_name = page.getTestData('acp2ui-59027');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const pod_data = {
    name: pod_name,
    namespace: namespace_name,
    image: imageAddress,
  };
  beforeAll(() => {
    page.podPreparePage.deletePod(pod_name);
    page.podPreparePage.createPod(pod_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('容器组');
  });
  afterAll(() => {
    page.podPreparePage.deletePod(pod_name);
  });
  it('ACP2UI-59027 : 容器组详情页->点击日志tab->能正确展示容器日志，能通过切换容器展示其他容器的日志，init容器单独标出', () => {
    page.podListPage.searchPod(pod_name, 1).then(() => {
      page.podListPage.podListTable
        .clickResourceNameByRow([pod_name])
        .then(() => {
          page.podDetailPage.clickTab('日志').then(() => {
            page.podDetailPage
              .switch_pod_in_log_tab('初始化容器', 'init-container')
              .then(() => {
                const expectData = { 日志: ['init-container'] };
                page.podDetailVerifyPage.verify(expectData);
              });
            page.podDetailPage
              .switch_pod_in_log_tab('容器', 'helloworld')
              .then(() => {
                const expectData = { 日志: ['helloworld'] };
                page.podDetailVerifyPage.verify(expectData);
              });
          });
        });
    });
  });
});
