// import { ClusterPage } from '../../../../page_objects/acp/cluster/cluster.page';

// describe('管理视图 集群更新超售比L1自动化', () => {
//   const page = new ClusterPage();

//   beforeAll(() => {
//     page.login();
//     page.enterOperationView();
//   });
//   beforeEach(() => {});
//   afterAll(() => {});
//   it('ACP2UI-53795 : 更新超售比，cpu和内存的超售比不相同', () => {
//     const testData = {
//       CPU超售比: 4,
//       内存超售比: 2,
//     };
//     page.detailPage.updateRatio(page.clusterName, testData);
//     const expectDetailData = {
//       超售比: 'CPU4；内存2',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-53796 : 更新超售比，cpu和内存的超售比只有一个打开，一个关闭', () => {
//     const testData = {
//       CPU超售比: 0,
//       内存超售比: 1,
//     };
//     page.detailPage.updateRatio(page.clusterName, testData);
//     const expectDetailData = {
//       超售比: 'CPU未启用；内存1',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-53796 : 更新超售比，cpu和内存的超售比只有一个打开，一个关闭', () => {
//     const testData = {
//       CPU超售比: 1,
//       内存超售比: 0,
//     };
//     page.detailPage.updateRatio(page.clusterName, testData);
//     const expectDetailData = {
//       超售比: 'CPU1；内存未启用',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-53797 : 关闭超售比，cpu和内存的超售比都从打开状态更新为关闭状态', () => {
//     const testData = {
//       CPU超售比: 0,
//       内存超售比: 0,
//     };
//     page.detailPage.updateRatio(page.clusterName, testData);
//     const expectDetailData = {
//       超售比: 'CPU未启用；内存未启用',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
// });
