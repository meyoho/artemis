import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡-http L1自动化', () => {
  const page = new Alb2Page();

  const service_name1 = page.getTestData('forhttp1');
  const service_name2 = page.getTestData('forhttp2');
  const http_port = '1002';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.preparePage.createService(service_name2, page.namespace1Name);
    page.preparePage.deleteFrontend(alb_name, http_port);

    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');

    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(http_port);
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteService(service_name1, page.namespace1Name);
    page.preparePage.deleteService(service_name2, page.namespace1Name);
    page.preparePage.deleteFrontend(alb_name, http_port);
  });

  it('ACP2UI-54870 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口http-1个默认内部路由，会话保持，添加', () => {
    if (isReady) {
      const testData = {
        端口: http_port,
        协议: 'HTTP',
        内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
        会话保持: '源地址哈希',
      };
      page.createFrontendPage.addPort(testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        名称: frontend_name,
        协议: 'HTTP',
        默认内部路由: service_name1,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-53482 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http操作栏-更新默认内部路由，更新 1个内部路由，会话保持Cookie key，确定', () => {
    if (isReady) {
      const testData = {
        内部路由组: [[page.namespace1Name, service_name2, '80', 100]],
        会话保持: 'Cookie key',
        会话保持属性: 'JSESSIONID',
      };
      page.detailAlbPage.updateDefaultInternalRouting(
        alb_name,
        http_port,
        testData,
      );
      const expectData = {
        侦听器: {
          端口: http_port,
          协议: 'HTTP',
          默认内部路由: service_name2,
        },
      };

      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54871 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http操作栏-更新默认内部路由，更新 1个内部路由，不会话保持，确定', () => {
    if (isReady) {
      const testData = {
        内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
        会话保持: '不会话保持',
      };
      page.detailFrontendPage.updateDefaultInternalRouting(
        alb_name,
        http_port,
        testData,
      );

      const expectData = {
        名称: frontend_name,
        协议: 'HTTP',
        默认内部路由: service_name1,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-53483 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http名称进入详情页-操作-删除，删除成功', () => {
    if (isReady) {
      page.detailFrontendPage.deletePort(alb_name, http_port);
      const expectData = { 删除: http_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
