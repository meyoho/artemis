import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡 tcp L1自动化', () => {
  const page = new Alb2Page();

  const service_name1 = page.getTestData('fortcp1');
  const service_name2 = page.getTestData('fortcp2');
  const tcp_port = '1001';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    page.preparePage.deleteFrontend(alb_name, tcp_port);
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.preparePage.createService(service_name2, page.namespace1Name);

    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');
    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(tcp_port);
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteService(service_name1, page.namespace1Name);
    page.preparePage.deleteService(service_name2, page.namespace1Name);

    page.preparePage.deleteFrontend(alb_name, tcp_port);
  });

  it('ACP2UI-52415 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口TCP，没有内部路由，不会话保持，添加', () => {
    if (isReady) {
      const testData = {
        端口: tcp_port,
        协议: 'TCP',
      };

      page.createFrontendPage.addPort(testData);

      const expectData = {
        名称: frontend_name,
        协议: 'TCP',
        默认内部路由: '-',
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53481 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口TCP操作栏-更新默认内部路由，添加一条，切换会话保持，点击确定', () => {
    if (isReady) {
      const testData = {
        内部路由组: [[page.namespace1Name, service_name2, '80', 100]],
        会话保持: '源地址哈希',
      };
      page.detailAlbPage.updateDefaultInternalRouting(
        alb_name,
        tcp_port,
        testData,
      );

      const expectData = {
        侦听器: {
          端口: tcp_port,
          协议: 'TCP',
          默认内部路由: service_name2,
        },
      };

      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52420 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口TCP-操作栏-删除，删除成功', () => {
    if (isReady) {
      page.detailAlbPage.deletePort(alb_name, tcp_port);
      const expectData = { 删除: tcp_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54151 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口TCP，有两条内部路由，源地址哈希，添加', () => {
    if (isReady) {
      const testData = {
        端口: tcp_port,
        协议: 'TCP',
        内部路由组: [
          [page.namespace1Name, service_name1, '80', 100],
          [page.namespace1Name, service_name2, '80', 100],
        ],
        会话保持: '源地址哈希',
      };

      page.createFrontendPage.addPort(testData);

      const expectData = {
        名称: frontend_name,
        协议: 'TCP',
        默认内部路由: service_name1,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52417 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口TCP名称进入详情-操作-更新默认内部路由，成功', () => {
    if (isReady) {
      const testData = {
        内部路由组: [
          [page.namespace1Name, service_name2, '80', 100],
          [page.namespace1Name, service_name1, '80', 100, true],
        ],
        会话保持: '不会话保持',
      };
      page.detailFrontendPage.updateDefaultInternalRouting(
        alb_name,
        tcp_port,
        testData,
      );
      const expectData = {
        名称: frontend_name,
        协议: 'TCP',
        默认内部路由: service_name2,
        默认内部路由不包含: service_name1,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52419 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口TCP名称进入详情-操作-删除，删除成功', () => {
    if (isReady) {
      page.detailFrontendPage.deletePort(alb_name, tcp_port);
      const expectData = { 删除: tcp_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
