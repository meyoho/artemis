import { IngressPage } from '@e2e/page_objects/acp/ingress/ingress.page';

describe('用户视图 访问规则L1自动化', () => {
  const page = new IngressPage();
  const ingress_base_http_name = page.getTestData('ingress-base-http-check');
  const ingress_base_https_name = page.getTestData('ingress-base-https-check');
  const ingress_http_name = page.getTestData('ingress-http-check');
  const ingress_https_name = page.getTestData('ingress-https-check');

  const service_name = page.getTestData('ingress-check');
  const secret_name1 = page.getTestData('ingress-check');
  const domain_name2 = page.getTestData('ingress-host-check');
  const domain_name_ext = `*.${domain_name2}`;

  beforeAll(() => {
    page.preparePage.deleteIngress(ingress_base_http_name, page.namespace1Name);
    page.preparePage.deleteIngress(
      ingress_base_https_name,
      page.namespace1Name,
    );
    page.preparePage.deleteIngress(ingress_http_name, page.namespace1Name);
    page.preparePage.deleteIngress(ingress_https_name, page.namespace1Name);
    page.preparePage.deleteService(service_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name2);

    page.createService(service_name, page.namespace1Name);
    page.createSecret(secret_name1, page.namespace1Name);
    page.preparePage.createDomain(domain_name2, 'extensive');

    page.login();
    page.enterUserView(page.namespace1Name);
  });
  afterAll(() => {
    page.preparePage.deleteIngress(ingress_base_http_name, page.namespace1Name);
    page.preparePage.deleteIngress(
      ingress_base_https_name,
      page.namespace1Name,
    );
    page.preparePage.deleteIngress(ingress_http_name, page.namespace1Name);
    page.preparePage.deleteIngress(ingress_https_name, page.namespace1Name);
    page.preparePage.deleteService(service_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name2);
  });
  it('ACP2UI-55173 : Ingress 增加重复检查：创建http的访问规则时， host+path 和当前命名空间的http的访问规则相同，禁止提交，提示“规则在 <其他访问规则名字> 中已存在”', () => {
    const testData = {
      名称: ingress_base_http_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.createPage.addIngress(testData);
    const expectData = {
      规则: [
        {
          地址: `check.${domain_name2}/uiauto-check`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
    const testDataHttps = {
      名称: ingress_http_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.createPage.addIngress(testDataHttps);
    const expectDataHttps = {
      重复校验: ingress_base_http_name,
    };
    page.detailPageVerify.verify(expectDataHttps);
  });
  it('ACP2UI-55175 : Ingress 增加重复检查：创建https的访问规则时， host+path 和当前命名空间的http的访问规则相同，可以提交', () => {
    const testData = {
      名称: ingress_base_https_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      HTTPS: secret_name1,
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.createPage.addIngress(testData);
    const expectData = {
      规则: [
        {
          地址: `check.${domain_name2}/uiauto-check`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书: [{ 保密字典: secret_name1, 域名: domain_name2 }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-55174 : Ingress 增加重复检查：创建https的访问规则时， host+path 和当前命名空间的https的访问规则相同，禁止提交，提示“规则在 <其他访问规则名字> 中已存在”', () => {
    const testDataHttps = {
      名称: ingress_https_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      HTTPS: secret_name1,
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.createPage.addIngress(testDataHttps);
    const expectDataHttps = {
      重复校验: ingress_base_https_name,
    };
    page.detailPageVerify.verify(expectDataHttps);
  });
  it('ACP2UI-55176 : Ingress 增加重复检查：更新http的访问规则时， host+path 和当前命名空间的http的访问规则相同，禁止提交，提示“规则在 <其他访问规则名字> 中已存在”', () => {
    const testDataHttp = {
      名称: ingress_http_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      规则: [['/uiauto-other', service_name, '80']],
    };
    page.createPage.addIngress(testDataHttp);
    const expectData = {
      规则: [
        {
          地址: `check.${domain_name2}/uiauto-other`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
    const testData = {
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.updatePage.updateIngress(ingress_http_name, testData);
    const expectDataHttp = {
      重复校验: ingress_base_http_name,
    };
    page.detailPageVerify.verify(expectDataHttp);
  });
  it('ACP2UI-55177 : Ingress 增加重复检查：更新https的访问规则时， host+path 和当前命名空间的https的访问规则相同，禁止提交，提示“规则在 <其他访问规则名字> 中已存在”', () => {
    const testDataHttps = {
      名称: ingress_https_name,
      域名: domain_name_ext,
      域名前缀: 'check',
      HTTPS: secret_name1,
      规则: [['/uiauto-other', service_name, '80']],
    };
    page.createPage.addIngress(testDataHttps);
    const expectData = {
      规则: [
        {
          地址: `check.${domain_name2}/uiauto-other`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书: [{ 保密字典: secret_name1, 域名: domain_name2 }],
    };
    page.detailPageVerify.verify(expectData);
    const testData = {
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.updatePage.updateIngress(ingress_https_name, testData);
    const expectDataHttps = {
      重复校验: ingress_base_https_name,
    };
    page.detailPageVerify.verify(expectDataHttps);
  });
  it('ACP2UI-55178 : Ingress 增加重复检查：更新http的访问规则时， host+path 和当前命名空间的https的访问规则相同，可以提交', () => {
    page.deletePage.deleteIngress(ingress_base_http_name);
    const testData = {
      规则: [['/uiauto-check', service_name, '80']],
    };
    page.updatePage.updateIngress(ingress_http_name, testData);
    const expectData = {
      规则: [
        {
          地址: `check.${domain_name2}/uiauto-check`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
