import { IngressPage } from '@e2e/page_objects/acp/ingress/ingress.page';

describe('用户视图 访问规则L0自动化', () => {
  const page = new IngressPage();
  const ingress_name = page.getTestData('l0-ingress');
  const service_name = page.getTestData('l0-ingress-l0');
  const domain_name_full = page.getTestData('l0-ingress1');

  beforeAll(() => {
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteService(service_name, page.namespace1Name);

    page.preparePage.createService(service_name, page.namespace1Name);
    page.preparePage.createDomain(domain_name_full, 'full');

    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('访问规则');
  });
  afterAll(() => {
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteService(service_name, page.namespace1Name);
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
  });

  it('ACP2UI-53462 : 单击左导航访问规则，在列表页单击创建访问规则按钮，创建访问规则-全域名，成功', () => {
    const testData = {
      名称: ingress_name,
      域名: domain_name_full,
      规则: [['/create', service_name, '80']],
    };
    page.createPage.addIngress(testData);
    const expectData = {
      规则: [
        {
          地址: `${domain_name_full}/create`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书无数据: '无HTTPS 证书',
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-52663 : 单击左导航访问规则，在列表页，按名称搜索，成功', () => {
    page.listPage.searchIngress(ingress_name, 1);
    let expectData = {
      数量: 1,
    };
    page.listPageVerify.verify(expectData);

    expectData = {
      数量: 1,
    };
    // 按大写过滤
    page.listPage.searchIngress(ingress_name.toUpperCase(), 1);
    page.listPageVerify.verify(expectData);

    expectData = {
      数量: 0,
    };

    // 按不存在的名称过滤
    page.listPage.searchIngress('notexist', 0);
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-52651 : 单击左导航访问规则，在列表页单击操作列-更新，成功 ', () => {
    const testData = {
      域名: domain_name_full,
      规则: [
        ['/update1', service_name, '80'],
        ['/update2', service_name, '80'],
      ],
    };
    page.updatePage.updateIngress(ingress_name, testData);
    const expectData = {
      规则: [
        {
          地址: `${domain_name_full}/update1`,
          内部路由: service_name,
          服务端口: 80,
        },
        {
          地址: `${domain_name_full}/update2`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书无数据: '无HTTPS 证书',
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52653 : 单击左导航访问规则，在列表页单击操作列-删除，成功', () => {
    page.deletePage.deleteIngress(ingress_name);
    // 验证搜索正确
    page.listPage.searchIngress(ingress_name, 0);
    const expectData = {
      数量: 0,
    };
    page.listPageVerify.verify(expectData);
  });
});
