import { ServicePage } from '@e2e/page_objects/acp/service/service.page';

describe('用户视图 内部路由L0自动化', () => {
  const page = new ServicePage();
  const service_name = page.getTestData('l0-svc');
  const namespace_name = page.namespace1Name;

  beforeAll(() => {
    page.preparePage.deleteService(service_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('内部路由');
  });
  afterAll(() => {
    page.preparePage.deleteService(service_name);
  });
  it('ACP2UI-52664 : L0(clusterip)单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP默认打开-外网访问默认关闭-端口 TCP-选择器不选择-会话保持默认关闭-点击创建', () => {
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      外网访问: false,
      端口: [['TCP', 80, 80]],
      会话保持: false,
    };
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-clusterip/nodeport': true,
      会话保持: '否',
      端口: [
        '端口名称',
        '服务端口',
        '容器端口',
        '主机端口',
        'tcp-80-80',
        '80',
        '80',
        '-',
      ],
      选择器: ['键', '值'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53666 : 单击左导航内部路由，在列表页单击名称进入详情,切换到容器组，没有selector时列表为空', () => {
    page.listPage.viewService(service_name);
    const expectData = { 容器组: [] };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52666 : 单击左导航内部路由，在列表页，按名称搜索，成功', () => {
    // 验证搜索正确
    page.listPage.searchService(service_name, 1);
    let expectData = { 检索: 1 };
    page.listPageVerify.verify(expectData);

    const expectCreateData = {
      创建: {
        名称: service_name,
        端口: '协议\n服务端口\n容器端口\n主机端口\nTCP\n80\n80\n-',
      },
    };
    page.listPageVerify.verify(expectCreateData);
    // 按大写过滤
    page.listPage.searchService(service_name.toUpperCase(), 1);
    page.listPageVerify.verify(expectData);

    // 按不存在的名称过滤
    page.listPage.searchService('notexist', 0);
    expectData = { 检索: 0 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-52668 : 单击左导航内部路由，在列表页单击操作列-更新，成功', () => {
    const testData = {
      选择器: [['key', 'updatevalue']],
    };
    page.listPage.updateService(service_name, testData);
    const expectData = {
      选择器: ['键', '值', 'key', 'updatevalue'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52669 : L0:单击左导航内部路由，在列表页单击操作列-删除，成功', () => {
    page.listPage.deleteService(service_name);
    // 验证搜索正确
    page.listPage.searchService(service_name, 0);
    const expectData = { 检索: 0 };
    page.listPageVerify.verify(expectData);
  });
});
