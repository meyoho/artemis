import { ServerConf } from '@e2e/config/serverConf';
import { DaemonSetPage } from '@e2e/page_objects/acp/daemonset/daemonset.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { ServicePage } from '@e2e/page_objects/acp/service/service.page';
import { StatefulSetPage } from '@e2e/page_objects/acp/statefulset/statefulset.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('用户视图 内部路由L1自动化', () => {
  const page = new ServicePage();
  const service_name = page.getTestData('l1-add-svc');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const deployment_name = page.getTestData('forsvc');
  const deployment_name2 = page.getTestData('svckey');
  const deployment_page = new DeploymentPage();
  const statefulset_name = page.getTestData('forsvc');
  const statefulset_page = new StatefulSetPage();
  const daemonset_name = page.getTestData('forsvc');
  const daemonset_page = new DaemonSetPage();

  beforeAll(() => {
    deployment_page.preparePage.delete(deployment_name);
    deployment_page.preparePage.delete(deployment_name2);
    statefulset_page.preparePage.delete(statefulset_name);
    daemonset_page.preparePage.delete(daemonset_name);
    page.preparePage.deleteService(service_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.preparePage.deleteService(service_name);
  });
  afterAll(() => {
    page.preparePage.deleteService(service_name);
    deployment_page.preparePage.delete(deployment_name);
    deployment_page.preparePage.delete(deployment_name2);
    statefulset_page.preparePage.delete(statefulset_name);
    daemonset_page.preparePage.delete(daemonset_name);
  });

  it('ACP2UI-53611 : (nodeport)单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP打开-外网访问打开-端口HTTP-选择器从计算资源导入deployment-会话保持打开-点击创建', () => {
    const deploymentData = {
      名称: deployment_name,
    };
    deployment_page.createPage.create(deploymentData, imageAddress);
    deployment_page.detailPage.waitStatus('运行中');
    deployment_page.detailPage.clickTab('容器组');
    const pod_list = deployment_page.detailPage.podListTable.getColumeTextByName(
      '名称',
    );
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      外网访问: true,
      端口: [['HTTP', 80, 80]],
      从计算资源导入: { 资源类型: '部署', 资源名称: deployment_name },
      会话保持: true,
    };
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-clusterip/nodeport': true,
      会话保持: '是',
      外网访问: '是',
      '主机端口-nodeport': true,
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'http-80-80',
        'TCP',
        '80',
        '80',
      ],
      选择器: [
        '键',
        '值',
        `service.${ServerConf.LABELBASEDOMAIN}/name`,
        `deployment-${deployment_name}`,
      ],
      容器组: pod_list,
    };
    page.detailPageVerify.verify(expectData);
    deployment_page.preparePage.delete(deployment_name);
  });

  it('ACP2UI-53612 : (headless)单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP关闭-(验证外网访问不能打开）-端口UDP-选择器从计算资源导入statefulset-会话保持打开-点击创建', () => {
    const statefulsetData = {
      名称: statefulset_name,
    };
    statefulset_page.createPage.create(statefulsetData, imageAddress);
    statefulset_page.detailPage.waitStatus('运行中');
    statefulset_page.detailPage.clickTab('容器组');
    const pod_list = statefulset_page.detailPage.podListTable.getColumeTextByName(
      '名称',
    );
    const testData = {
      名称: service_name,
      '虚拟 IP': false,
      端口: [['UDP', 80, 80]],
      从计算资源导入: {
        资源类型: '有状态副本集',
        资源名称: statefulset_name,
      },
      会话保持: true,
    };
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-headless': true,
      会话保持: '是',
      外网访问: '否',
      '主机端口-clusterip/headless': true,
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'udp-80-80',
        'UDP',
        '80',
        '80',
      ],
      选择器: [
        '键',
        '值',
        `service.${ServerConf.LABELBASEDOMAIN}/name`,
        `statefulset-${statefulset_name}`,
      ],
      容器组: pod_list,
    };
    page.detailPageVerify.verify(expectData);
    statefulset_page.preparePage.delete(statefulset_name);
  });
  it('ACP2UI-53613 : (clusterip)单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP打开-端口HTTPS-选择器从计算资源导入daemonset-点击创建.验证clusterip可以访问', () => {
    const daemonsetData = {
      名称: daemonset_name,
    };
    daemonset_page.createPage.create(daemonsetData, imageAddress);
    daemonset_page.detailPage.waitStatus('运行中');
    daemonset_page.detailPage.clickTab('容器组');
    const pod_list = daemonset_page.detailPage.podListTable.getColumeTextByName(
      '名称',
    );
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      端口: [['HTTPS', 80, 80]],
      从计算资源导入: { 资源类型: '守护进程集', 资源名称: daemonset_name },
    };
    page.listPage.addService(testData);
    const expectData = {
      '主机端口-clusterip/headless': true,
      外网访问: '否',
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'https-80-80',
        'TCP',
        '80',
        '80',
      ],
      选择器: [
        '键',
        '值',
        `service.${ServerConf.LABELBASEDOMAIN}/name`,
        `daemonset-${daemonset_name}`,
      ],
      容器组: pod_list,
    };
    page.detailPageVerify.verify(expectData);
    daemonset_page.preparePage.delete(daemonset_name);
  });
  it('ACP2UI-53615 : 单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP打开-外网访问打开-端口gRPC-选择器添加正确的key/value-点击创建', () => {
    const deploymentData = {
      名称: deployment_name2,
    };
    deployment_page.createPage.create(deploymentData, imageAddress);
    deployment_page.detailPage.clickTab('容器组');
    const pod_list = deployment_page.detailPage.podListTable.getColumeTextByName(
      '名称',
    );
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      外网访问: true,
      端口: [['gRPC', 80, 80]],
      选择器: [
        [
          `service.${ServerConf.LABELBASEDOMAIN}/name`,
          `deployment-${deployment_name2}`,
        ],
      ],
    };
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-clusterip/nodeport': true,
      '主机端口-nodeport': true,
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'grpc-80-80',
        'TCP',
        '80',
        '80',
      ],
      选择器: [
        '键',
        '值',
        `service.${ServerConf.LABELBASEDOMAIN}/name`,
        `deployment-${deployment_name2}`,
      ],
      容器组: pod_list,
    };
    page.detailPageVerify.verify(expectData);
    deployment_page.preparePage.delete(deployment_name2);
  });
  it('ACP2UI-53617 : 单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP打开-外网访问关闭-端口TCP UDP HTTP HTTPS HTTP2 gRPC-选择器不选择-点击创建', () => {
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      外网访问: false,
      端口: [
        ['TCP', 81, 80],
        ['HTTP', 82, 80],
        ['HTTPS', 83, 80],
        ['HTTP2', 84, 80],
        ['gRPC', 85, 80],
        ['UDP', 86, 80],
      ],
      选择器: [
        [
          `service.${ServerConf.LABELBASEDOMAIN}/name`,
          `deployment-${deployment_name}`,
        ],
      ],
    };
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-clusterip/nodeport': true,
      '主机端口-clusterip/nodeport': true,
      外网访问: '否',
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        'tcp-81-80',
        '81',
        '80',
        'http-82-80',
        'https-83-80',
        'http2-84-80',
        'grpc-85-80',
        'udp-86-80',
        'TCP',
        'UDP',
      ],
      选择器: ['键', '值'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53616 : 单击左导航内部路由-列表页单击创建内部路由-右上角切换到 YAML-输入name/port-点击创建', () => {
    const yamlstring = CommonMethod.readTemplateFile('alauda.service.yaml', {
      service_name: service_name,
      ns_name: namespace_name,
      headless: true,
    });
    page.listPage.addServiceByYaml(yamlstring);
    const expectData = {
      会话保持: '否',
      外网访问: '否',
      '虚拟 IP-headless': true,
      '主机端口-clusterip/headless': true,
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'tcp-80-80',
        'TCP',
        '80',
      ],
      选择器: ['键', '值'],
      容器组: [],
    };
    page.detailPageVerify.verify(expectData);
  });
});
