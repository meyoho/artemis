import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡 https L1自动化', () => {
  const page = new Alb2Page();

  const service_name1 = page.getTestData('forhttps1');
  const service_name2 = page.getTestData('forhttps2');
  const secret_name1 = page.getTestData('forhttps1');
  const secret_name2 = page.getTestData('forhttps2');
  const https_port = '1003';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    page.preparePage.deleteFrontend(alb_name, https_port);
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.preparePage.createService(service_name2, page.namespace1Name);
    page.createSecret(secret_name1, page.namespace1Name);
    page.createSecret(secret_name2, page.namespace1Name);

    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');

    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(https_port);
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteService(service_name1, page.namespace1Name);
    page.preparePage.deleteService(service_name2, page.namespace1Name);
    page.deleteSecret(secret_name1, page.namespace1Name);
    page.deleteSecret(secret_name2, page.namespace1Name);
    page.preparePage.deleteFrontend(alb_name, https_port);
  });

  it('ACP2UI-52447 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口https-没有默认内部路由，创建成功', () => {
    if (isReady) {
      const testData = {
        端口: https_port,
        协议: 'HTTPS',
        默认证书: {
          命名空间: page.namespace1Name,
          保密字典: secret_name1,
        },
      };
      page.createFrontendPage.addPort(testData);
      // 创建后进入frontend详情页
      const expectData = {
        // 面包屑: `网络/负载均衡/${alb_name}/端口/${frontend_name}`,
        名称: frontend_name,
        协议: 'HTTPS',
        默认内部路由: '-',
        默认证书: `${page.namespace1Name}_${secret_name1}`,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53511: 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https-操作栏更新默认内部路由-1个，成功', () => {
    if (isReady) {
      const testData = {
        内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
        会话保持: '源地址哈希',
      };
      page.detailAlbPage.updateDefaultInternalRouting(
        alb_name,
        https_port,
        testData,
      );
      const expectData = {
        // 面包屑: `网络/负载均衡/${alb_name}`,
        侦听器: {
          端口: https_port,
          协议: `HTTPS(证书:${page.namespace1Name}/${secret_name1})`,
          默认内部路由: service_name1,
        },
      };

      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53513: 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https-操作栏删除，删除成功', () => {
    if (isReady) {
      page.detailAlbPage.deletePort(alb_name, https_port);
      const expectData = {
        删除: https_port,
        // 面包屑: `网络/负载均衡/${alb_name}`
      };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54328 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口https，默认证书，1 个默认内部路由， 源地址哈希，创建成功', () => {
    if (isReady) {
      const testData = {
        端口: https_port,
        协议: 'HTTPS',
        默认证书: {
          命名空间: page.namespace1Name,
          保密字典: secret_name1,
        },
        内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
        会话保持: '源地址哈希',
      };
      page.createFrontendPage.addPort(testData);

      const expectData = {
        名称: frontend_name,
        协议: 'HTTPS',
        默认内部路由: service_name1,
        默认证书: `${page.namespace1Name}_${secret_name1}`,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54335 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https名称进入详情-操作-更新默认证书为有效证书（验证只显示tls），成功', () => {
    if (isReady) {
      const testData = {
        默认证书: {
          命名空间: page.namespace1Name,
          保密字典: secret_name2,
        },
      };
      page.detailFrontendPage.updateDefaultCertificate(
        alb_name,
        https_port,
        testData,
      );

      const expectData = {
        名称: frontend_name,
        协议: 'HTTPS',
        默认内部路由: '-',
        默认证书: `${page.namespace1Name}_${secret_name2}`,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53512: 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https名称进入详情-操作-更新默认内部路由-2个，成功', () => {
    if (isReady) {
      const testData = {
        内部路由组: [
          [page.namespace1Name, service_name1, '80', 100],
          [page.namespace2Name, service_name2, '80', 100],
        ],
      };

      page.detailFrontendPage.updateDefaultInternalRouting(
        alb_name,
        https_port,
        testData,
      );

      const expectData = {
        名称: frontend_name,
        协议: 'HTTPS',
        默认内部路由: service_name1,
        默认证书: `${service_name1}: 80 权重 : 100 (50.00%)\n${service_name2}: 80 权重 : 100 (50.00%)`,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53513: 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https名称进入详情-操作-删除，删除成功', () => {
    if (isReady) {
      page.detailFrontendPage.deletePort(alb_name, https_port);
      const expectData = {
        删除: https_port,
      };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
