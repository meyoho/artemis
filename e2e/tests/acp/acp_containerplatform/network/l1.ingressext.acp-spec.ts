import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';
import { IngressPage } from '@e2e/page_objects/acp/ingress/ingress.page';
import { browser } from 'protractor';

describe('用户视图 访问规则L1自动化', () => {
  const page = new IngressPage();
  const ingress_name = page.getTestData('ext-ingress');

  const service_name = page.getTestData('ext-service');
  const secret_name1 = page.getTestData('ext-secret1');
  const secret_name2 = page.getTestData('ext-secret2');
  const domain_name_full = page.getTestData('ext-domian1');
  const domain_name2 = page.getTestData('ext-domain2');
  const domain_name_ext = `*.${domain_name2}`;
  const alb_page = new Alb2Page();
  let isReady, alb_name;

  beforeAll(() => {
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
    page.preparePage.deleteService(service_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteDomain(domain_name2);

    page.createService(service_name, page.namespace1Name);
    page.createSecret(secret_name1, page.namespace1Name);
    page.createSecret(secret_name2, page.namespace1Name);
    page.preparePage.createDomain(domain_name_full, 'full');
    page.preparePage.createDomain(domain_name2, 'extensive');

    page.login();
    page.enterUserView(page.namespace1Name);
    try {
      alb_name = alb_page.preparePage.isReady();
      isReady = true;
    } catch {
      isReady = false;
    }
  });
  afterAll(() => {
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
    page.preparePage.deleteService(service_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteDomain(domain_name2);
  });

  it('ACP2UI-52646 : 单击左导航访问规则，在列表页单击创建访问规则按钮，创建访问规则-泛域名、https，成功', () => {
    const testData = {
      名称: ingress_name,
      域名: domain_name_ext,
      域名前缀: 'add',
      HTTPS: secret_name1,
      规则: [['/uiauto-create', service_name, '80']],
    };
    page.createPage.addIngress(testData);
    const expectData = {
      规则: [
        {
          地址: `add.${domain_name2}/uiauto-create`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书: [{ 保密字典: secret_name1, 域名: domain_name2 }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53587 : 单击左导航访问规则，在列表页单击名称进入详情查看，成功', () => {
    page.listPage.enterDetailIngress(ingress_name);

    const expectData = {
      规则: [
        {
          地址: `add.${domain_name2}/uiauto-create`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书: [{ 保密字典: secret_name1, 域名: domain_name2 }],
    };
    page.detailPageVerify.verify(expectData);
    // 验证详情页的基本信息、规则、HTTPS 证书

    page.viewPage.switchtab('YAML');
    // 验证YAML正确
    const yamlvalue = page.viewPage.alaudaCodeEdit.getYamlValue();
    expect(yamlvalue).toContain('kind: Ingress');
    expect(yamlvalue).toContain(`secretName: ${secret_name1}`);
    expect(yamlvalue).toContain(`host: add.${domain_name2}`);
  });
  it('ACP2UI-53402 : 创建访问规则,选择https证书，验证alb的443端口有规则', () => {
    if (isReady) {
      alb_page.detailFrontendPage.enterDetailPage(alb_name, 443);
      const expectData = {
        规则: [
          `${page.namespace1Name}/${ingress_name}`,
          '域名',
          `add.${domain_name2}`,
          'URL',
          '/uiauto-create',
          `${page.namespace1Name} ${service_name}: 80 权重 : 100 (100.00%)`,
        ],
      };
      alb_page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52652 : 单击左导航访问规则，在列表页单击名称进入详情，操作-更新，成功', () => {
    const testData = {
      域名: domain_name_full,
      HTTPS: secret_name2,
      规则: [
        ['/uiauto-update', service_name, '80'],
        ['/uiauto-add', service_name, '80'],
      ],
    };
    page.viewPage.updateIngressFromDetail(ingress_name, testData);
    // 验证详情页
    const expectData = {
      规则: [
        {
          地址: `${domain_name_full}/uiauto-add`,
          内部路由: service_name,
          服务端口: 80,
        },
        {
          地址: `${domain_name_full}/uiauto-update`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书: [{ 保密字典: secret_name2, 域名: domain_name_full }],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52656 : 创建访问规则,https证书，更新规则，验证alb的443端口规则更新', () => {
    if (isReady) {
      browser.sleep(20000).then(() => {
        alb_page.detailFrontendPage.enterDetailPage(alb_name, 443);
        const expectData = {
          规则: [
            `${page.namespace1Name}/${ingress_name}`,
            '域名',
            domain_name_full,
            'URL',
            '/uiauto-add',
            '/uiauto-update',
            `${page.namespace1Name} ${service_name}: 80 权重 : 100 (100.00%)`,
          ],
        };
        alb_page.detaiFrontendPageVerify.verify(expectData);
      });
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52654 : 单击左导航访问规则，在列表页单击名称进入详情，操作-删除，成功', () => {
    page.viewPage.deleteIngressFromDetail(ingress_name);
    // 验证搜索正确
    page.listPage.searchIngress(ingress_name, 0);
    const expectData = {
      数量: 0,
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-52657 : 创建访问规则，https证书，删除访问规则，验证alb的443端口规则删除', () => {
    if (isReady) {
      alb_page.detailFrontendPage.enterDetailPage(alb_name, 443);
      const expectData = {
        删除规则: [domain_name_full, '/uiauto-add', '/uiauto-update'],
      };
      alb_page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
