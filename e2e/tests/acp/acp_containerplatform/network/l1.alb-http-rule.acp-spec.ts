import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡 http规则 L1自动化', () => {
  const page = new Alb2Page();

  const service_name1 = page.getTestData('forhttprule1');
  const service_name2 = page.getTestData('forhttprule2');
  const full_domain_name = page
    .getTestData('alb.http.rule.full')
    .replace('-', '.');
  const domain_name_ext = page
    .getTestData('alb.http.rule.ext')
    .replace('-', '.');
  const ext_domain_name = `*.${domain_name_ext}`;
  const http_port = '1004';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.preparePage.createService(service_name2, page.namespace1Name);
    page.preparePage.createDomain(full_domain_name, 'full');
    page.preparePage.createDomain(domain_name_ext, 'extensive');

    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');

    try {
      alb_name = page.preparePage.isReady();
      page.preparePage.deleteFrontend(alb_name, http_port);
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(http_port);
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteDomain(full_domain_name);
    page.preparePage.deleteDomain(domain_name_ext);
    page.preparePage.deleteService(service_name1, page.namespace1Name);
    page.preparePage.deleteService(service_name2, page.namespace1Name);
    page.preparePage.deleteFrontend(alb_name, http_port);
  });

  it('ACP2UI-54188 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口http-没有默认内部路由，源地址哈希，不能添加规则。', () => {
    if (isReady) {
      const testData = {
        端口: http_port,
        协议: 'HTTP',
        会话保持: '源地址哈希',
      };
      page.createFrontendPage.addPort(testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        名称: frontend_name,
        协议: 'HTTP',
        默认内部路由: '-',
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-54849 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击添加规则,输入规则，点击取消，验证未添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '验证添加取消',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData, false);

      const expectData = { 删除规则: '验证添加取消' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52423 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击添加规则（只有域名），成功', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '只有域名',
          内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }, { '': full_domain_name }],
            },
          ],
          会话保持: '不会话保持',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '只有域名',
          '域名',
          `rule.${domain_name_ext}`,
          full_domain_name,
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-52433 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-更新规则（所有类型规则）', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '所有类型规则',
          内部路由组: [
            [page.namespace1Name, service_name2, '80', 100],
            [page.namespace1Name, service_name1, '80', 100, true],
          ],
          会话保持: '源地址哈希',
          'URL 重写': 'true',
          重写地址: '/redirect',
          规则: [
            {
              Header: ['域名'],
              Data: [{ '': full_domain_name }, { ruleupdate: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [
                { StartsWith: ['rule.url'] },
                { RegEx: ['^23'] },
                { StartsWith: ['test.url'] },
              ],
            },
            {
              Header: ['IP'],
              Data: [
                { Equal: ['10.10.10.10'] },
                { Range: ['10.0.0.0', '10.255.255.255'] },
              ],
            },
            {
              Header: ['Header', 'header'],
              Data: [
                { Equal: ['header'] },
                { Range: ['1', '3'] },
                { RegEx: ['^y'] },
              ],
            },
            {
              Header: ['Cookie', 'cookie'],
              Data: [{ Equal: ['cookie'] }],
            },
            {
              Header: ['URL Param', 'param'],
              Data: [{ Equal: ['param'] }, { Range: ['a', 'c'] }],
            },
          ],
        },
      };
      page.detailFrontendPage.updateRule(
        alb_name,
        http_port,
        '只有域名',
        testData,
      );

      const expectData = {
        规则: [
          '所有类型规则',
          '域名',
          full_domain_name,
          `ruleupdate.${domain_name_ext}`,
          'URL',
          '/rule.url',
          '^23',
          '/test.url',
          'IP',
          '10.10.10.10',
          '10.0.0.0-10.255.255.255',
          'Header',
          'header:header',
          'header:1-3',
          'header:^y',
          'Cookie',
          'cookie:cookie',
          'URL Param',
          'param:param',
          'param:a-c',
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52435 : L0:单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-删除规则', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(alb_name, http_port, '所有类型规则');
      const expectData = { 删除规则: '所有类型规则' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52423 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击添加规则（域名+URL+Param），成功', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '域名+URL+Param',
          内部路由组: [
            [page.namespace1Name, service_name2, '80', 100],
            [page.namespace1Name, service_name1, '80', 100],
          ],
          会话保持: 'Cookie key',
          会话保持属性: 'RULE',
          'URL 重写': 'false',
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ StartsWith: ['rule.url'] }],
            },
            {
              Header: ['URL Param', 'param'],
              Data: [{ Equal: ['param'] }],
            },
          ],
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '域名+URL+Param',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '/rule.url',
          'URL Param',
          'param:param',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (50.00%)`,
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (50.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53659 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-删除规则(验证多个内部路由时不可删除规则)', () => {
    if (isReady) {
      page.detailFrontendPage.enterDetailPage(alb_name, http_port);

      const expectData = { 不能删除规则: '域名+URL+Param' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54326 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-更新规则，点击取消，验证未更新', () => {
    if (isReady) {
      const testData = {
        规则: {
          内部路由组: [
            [page.namespace1Name, service_name1, '80', 100],
            [page.namespace1Name, service_name2, '80', 100, true],
          ],
        },
      };
      page.detailFrontendPage.updateRule(
        alb_name,
        http_port,
        '域名+URL+Param',
        testData,
        false,
      );
      const expectData = {
        规则: [
          '域名+URL+Param',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '/rule.url',
          'URL Param',
          'param:param',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (50.00%)`,
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (50.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54726 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-更新规则，不更新规则，更新内部路由，验证数据（泛域名）未丢失', () => {
    if (isReady) {
      const testData = {
        规则: {
          内部路由组: [
            [page.namespace1Name, service_name1, '80', 100],
            [page.namespace1Name, service_name2, '80', 100, true],
          ],
        },
      };
      page.detailFrontendPage.updateRule(
        alb_name,
        http_port,
        '域名+URL+Param',
        testData,
      );
      const expectData = {
        规则: [
          '域名+URL+Param',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '/rule.url',
          'URL Param',
          'param:param',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54327 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-删除规则，点击取消，验证未删除', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(
        alb_name,
        http_port,
        '域名+URL+Param',
        false,
      );
      const expectData = {
        规则: [
          '域名+URL+Param',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '/rule.url',
          'URL Param',
          'param:param',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52424 : http端口添加规则-1个内部路由+全域名+url(StartsWith)+不会话保持，无url重写，添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '1个内部路由+全域名+url(StartsWith)+不会话保持，无url重写',
          内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
          规则: [
            {
              Header: ['域名'],
              Data: [{ '': full_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ StartsWith: ['rule.url'] }],
            },
          ],
          会话保持: '不会话保持',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '1个内部路由+全域名+url(StartsWith)+不会话保持，无url重写',
          '域名',
          full_domain_name,
          'URL',
          '/rule.url',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52425 : http端口添加规则-2个内部路由+泛域名+多个url+header(equal)+源地址哈希，无url重写，添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述:
            '2个内部路由+泛域名+多个url+header(equal)+源地址哈希，无url重写',
          内部路由组: [
            [page.namespace1Name, service_name1, '80', 100],
            [page.namespace1Name, service_name2, '80', 100],
          ],
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [
                { StartsWith: ['rule.url'] },
                { RegEx: ['^23'] },
                { StartsWith: ['test.url'] },
              ],
            },
            {
              Header: ['Header', 'header'],
              Data: [{ Equal: ['header'] }],
            },
          ],
          会话保持: '源地址哈希',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '2个内部路由+泛域名+多个url+header(equal)+源地址哈希，无url重写',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '/rule.url',
          '^23',
          '/test.url',
          'Header',
          'header:header',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (50.00%)`,
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (50.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52426 : http端口添加规则-3个内部路由+泛域名+url(RegEx)+header(range)+URL Param+Cookie key，无url重写，添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述:
            '3个内部路由+泛域名+url(RegEx)+header(range)+URL Param+Cookie key，无url重写',
          内部路由组: [
            [page.namespace1Name, service_name1, '80', 100],
            [page.namespace1Name, service_name2, '80', 100],
            [page.namespace1Name, service_name2, '80', 100],
          ],
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ RegEx: ['^23'] }],
            },
            {
              Header: ['Header', 'header'],
              Data: [{ Range: ['1', '3'] }],
            },
            {
              Header: ['URL Param', 'param'],
              Data: [{ Equal: ['param'] }, { Range: ['a', 'c'] }],
            },
          ],
          会话保持: 'Cookie key',
          会话保持属性: 'RULE',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '3个内部路由+泛域名+url(RegEx)+header(range)+URL Param+Cookie key，无url重写',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '^23',
          'Header',
          'header:1-3',
          'URL Param',
          'param:param',
          'param:a-c',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (33.33%)`,
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (33.33%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52427 : http端口添加规则-1个内部路由+泛域名+url+header(RegEx)+URL Param(range)+Cookie(equal)，有url重写，添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述:
            '1个内部路由+泛域名+url+header(RegEx)+URL Param(range)+Cookie(equal)，有url重写',
          内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ RegEx: ['^23'] }],
            },
            {
              Header: ['Header', 'header'],
              Data: [{ RegEx: ['^y'] }],
            },
            {
              Header: ['URL Param', 'param'],
              Data: [{ Range: ['a', 'c'] }],
            },
            {
              Header: ['Cookie', 'cookie'],
              Data: [{ Equal: ['cookie'] }],
            },
          ],
          'URL 重写': 'true',
          重写地址: '/redirect',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '1个内部路由+泛域名+url+header(RegEx)+URL Param(range)+Cookie(equal)，有url重写',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '^23',
          'Header',
          'header:^y',
          'URL Param',
          'param:a-c',
          'Cookie',
          'cookie:cookie',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52428 : http端口添加规则-1个内部路由+泛域名+url+header+URL Param(equal)+Cookie+IP，无url重写，添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述:
            '1个内部路由+泛域名+url+header+URL Param(equal)+Cookie+IP，无url重写',
          内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ RegEx: ['^23'] }],
            },
            {
              Header: ['Header', 'header'],
              Data: [{ RegEx: ['^y'] }],
            },
            {
              Header: ['URL Param', 'param'],
              Data: [{ Equal: ['param'] }],
            },
            {
              Header: ['Cookie', 'cookie'],
              Data: [{ Equal: ['cookie'] }],
            },
            {
              Header: ['IP'],
              Data: [
                { Equal: ['10.10.10.10'] },
                { Range: ['10.0.0.0', '10.255.255.255'] },
              ],
            },
          ],
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);

      //创建成功后自动进入frontend 详情页

      const expectData = {
        规则: [
          '1个内部路由+泛域名+url+header+URL Param(equal)+Cookie+IP，无url重写',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          '^23',
          'Header',
          'header:^y',
          'URL Param',
          'param:param',
          'Cookie',
          'cookie:cookie',
          'IP',
          '10.10.10.10',
          '10.0.0.0-10.255.255.255',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53483 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http名称进入详情页-操作-删除，删除成功 ', () => {
    if (isReady) {
      page.detailFrontendPage.deletePort(alb_name, http_port);
      const expectData = { 删除: http_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
