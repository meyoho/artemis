import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡L0 自动化', () => {
  const page = new Alb2Page();
  const service_name1 = page.getTestData('forl0http1');
  const http_port = '1000';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(http_port);
    } catch {
      isReady = false;
    }
    page.preparePage.deleteFrontend(alb_name, http_port);
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteFrontend(alb_name, http_port);
    page.preparePage.deleteService(service_name1, page.namespace1Name);
  });
  it('ACP2UI-54139 : 单击左导航负载均衡，在列表页，按名称过滤', () => {
    if (isReady) {
      page.listPage.search(alb_name);
      let expectData = {
        检索: 1,
      };
      page.alb2ListPageVerify.verify(expectData);
      // 按大写过滤
      page.listPage.search(alb_name.toUpperCase());
      page.alb2ListPageVerify.verify(expectData);
      // 按不存在的名称过滤
      expectData = {
        检索: 0,
      };
      page.listPage.search('notexist', 0);
      page.alb2ListPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52413 : L0:单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口http-没有默认内部路由，不会话保持，添加', () => {
    if (isReady) {
      const testData = {
        端口: http_port,
        协议: 'HTTP',
      };
      page.createFrontendPage.addPort(testData);
      //创建成功后自动进入frontend 详情页
      const expectData = {
        名称: frontend_name,
        协议: 'HTTP',
        默认内部路由: '-',
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52423 : L0:单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击添加规则，成功 ', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '只有Header',
          内部路由组: [[page.namespace1Name, service_name1, '80', 100]],
          规则: [
            {
              Header: ['Header', 'header'],
              Data: [{ Equal: ['header'] }, { Range: ['1', '3'] }],
            },
          ],
          会话保持: '不会话保持',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.addRule(alb_name, http_port, testData);
      const expectData = {
        规则: [
          '只有Header',
          'Header',
          'header:header',
          'header:1-3',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-52435 : L0:单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http进入详情页-点击规则-操作-删除规则', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(alb_name, http_port, '只有Header');
      const expectData = { 删除规则: '只有Header' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-52438 : L0:单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口http-操作栏删除，删除成功', () => {
    if (isReady) {
      page.detailAlbPage.deletePort(alb_name, http_port);
      const expectData = { 删除: http_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
