import { DomainPage } from '@e2e/page_objects/acp/domain/domain.page';
import { browser } from 'protractor';

describe('管理视图 域名管理L1自动化', () => {
  const page = new DomainPage();
  const domain_name = page.getTestData('extensive');
  const full_domain_name = `*.${domain_name}`;
  const region_name = `${page.clusterName} (${page.clusterDisplayName()})`;
  const project_name1 = `${page.projectName} (UI TEST Project)`;
  const project_name2 = `${page.project2Name} (UI TEST Project)`;
  const expectRowData = new Map<string, string>();

  beforeAll(() => {
    page.preparePage.deleteDomain(domain_name);
    page.login();
  });
  beforeEach(() => {
    page.enterOperationView();
    page.clickLeftNavByText('域名');
    if (process.env.ENV === 'staging') {
      // staging 环境集群列表页有错误提示，等待错误提示消失
      browser.sleep(10000);
    }
    expectRowData.clear();
  });
  afterAll(() => {
    page.preparePage.deleteDomain(domain_name);
  });

  it('ACP2UI-3922 : 单击左导航域名管理，在列表页单击添加域名按钮，创建泛域名，分配集群1，分配项目1，成功', () => {
    const testData = {
      类型: '泛域名',
      域名: domain_name,
      分配集群: region_name,
      分配项目: project_name1,
    };
    page.createPage.addDomain(testData);

    expectRowData.set(
      domain_name,
      `*.${domain_name}泛域名${region_name}${project_name1}`,
    );

    const expectData = {
      泛域名分配集群: expectRowData,
    };

    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(
      full_domain_name,
      page.namespace1Name,
      true,
    );

    page.listPageVerify.checkingress(
      full_domain_name,
      page.project2ns1,
      false,
      page.project2Name,
    );
  });

  it('ACP2UI-3931 : 单击左导航域名管理，在列表页单击泛域名操作列-更新,分配项目 1改为2，成功', () => {
    const testData = {
      分配项目: project_name2,
    };
    page.updatePage.updateDomain(domain_name, '泛域名', testData);

    expectRowData.set(
      domain_name,
      `*.${domain_name}泛域名${region_name}${project_name2}`,
    );

    const expectData = {
      泛域名分配集群: expectRowData,
    };

    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(
      full_domain_name,
      page.namespace1Name,
      false,
    );

    page.listPageVerify.checkingress(
      full_domain_name,
      page.project2ns1,
      true,
      page.project2Name,
    );
  });
  it('ACP2UI-3930 : 单击左导航域名管理，在列表页单击泛域名操作列-更新,不分配集群，成功', () => {
    const testData = {
      分配集群: '不分配',
    };
    page.updatePage.updateDomain(domain_name, '泛域名', testData);
    expectRowData.set(domain_name, `*.${domain_name}泛域名未分配未分配`);

    const expectData = {
      泛域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(
      full_domain_name,
      page.namespace1Name,
      false,
    );

    page.listPageVerify.checkingress(
      full_domain_name,
      page.project2ns1,
      false,
      page.project2Name,
    );
  });
  it('ACP2UI-3933 : 单击左导航域名管理，在列表页单击泛域名操作列-更新,分配集群,分配项目-全部项目，成功', () => {
    const testData = {
      分配集群: region_name,
      分配项目: '全部项目',
    };
    page.updatePage.updateDomain(domain_name, '泛域名', testData);
    expectRowData.set(
      domain_name,
      `*.${domain_name}泛域名${region_name}全部项目`,
    );

    const expectData = {
      泛域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(
      full_domain_name,
      page.namespace1Name,
      true,
    );

    page.listPageVerify.checkingress(
      full_domain_name,
      page.project2ns1,
      true,
      page.project2Name,
    );
  });
  it('ACP2-3936: 单击左导航域名管理，在列表页单击泛域名操作列，删除成功', () => {
    page.deletePage.deleteDomainFromList(full_domain_name, '泛域名');
    const expectData = {
      删除域名: full_domain_name,
    };
    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(
      full_domain_name,
      page.namespace1Name,
      false,
    );

    page.listPageVerify.checkingress(
      full_domain_name,
      page.project2ns1,
      false,
      page.project2Name,
    );
  });
});
