import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';
import { $ } from 'protractor';

describe('管理视图 负载均衡 L1自动化', () => {
  const page = new Alb2Page();
  const region_name = page.clusterName;
  const input_name = page.getTestData('standalone');
  const input_name_high = page.getTestData('high');
  const alb_name = `${region_name}-${input_name}`;
  const alb_high_name = `${region_name}-${input_name_high}`;

  beforeAll(() => {
    page.preparePage.deleteHelmRequest(alb_name);
    page.preparePage.deleteHelmRequest(alb_high_name);
    page.login();
    page.enterOperationView();
    page.clickLeftNavByText('负载均衡');
    page.switchCluster_onAdminView(region_name).then(() => {
      page.waitElementPresent(
        $('.aui-card__content aui-table'),
        '等待 10s后，负载均衡列表页未显示',
        10000,
      );
      page.waitElementNotPresent($('.aui-icon-spinner'), '', 20000);
    });
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteHelmRequest(alb_name);
    page.preparePage.deleteHelmRequest(alb_high_name);
  });

  it('ACP2UI-55152 : l0创建负载均衡-单点：输入名称，类型默认单点，输入地址，副本数默认是 1 置灰，选择单个主机标签，分配项目默认全部项目，点击创建', () => {
    const testData = {
      名称: input_name,
      类型: '单点',
      地址: '3.3.3.3',
      主机标签: ['beta.kubernetes.io/os:linux'],
      分配项目: '全部项目',
    };
    page.createAlbPage.addAlb(testData);
    page.listPage.search(input_name);
    const expectData = {
      检索: 1,
      创建: {
        名称: input_name,
        地址: '3.3.3.3',
        项目: '全部项目',
      },
    };
    page.alb2ListPageVerify.verify(expectData);
  });
  it('ACP2UI-55153 : l0创建负载均衡-高可用：输入名称，类型高可用，输入地址，副本数默认是3，可以点减号到 2，选择单个主机标签，分配项目 选择单个项目，点击创建', () => {
    const testData = {
      名称: input_name_high,
      类型: '高可用',
      地址: 'artemis.test',
      副本数: 2,
      主机标签: [
        'beta.kubernetes.io/arch:amd64',
        'beta.kubernetes.io/os:linux',
      ],
      分配项目: page.project2Name,
    };
    page.createAlbPage.addAlb(testData);
    page.listPage.search(input_name_high);
    const expectData = {
      检索: 1,
      创建: {
        名称: input_name_high,
        地址: 'artemis.test',
        项目: page.project2Name,
      },
    };
    page.alb2ListPageVerify.verify(expectData);
  });

  it('ACP2UI-53864 : 单击左导航负载均衡，在详情页-更新项目，选择所有项目，验证详情页的项目显示正确', () => {
    page.detailAlbPage.updateProject(input_name_high, '全部项目');
    const expectData = {
      名称: input_name_high,
      地址: 'artemis.test',
      分配项目: '全部项目',
    };
    page.detailAlbPageVerify.verify(expectData);
  });
  it('ACP2UI-54080 : 单击左导航负载均衡，在详情页-更新项目，选择单个项目A，验证列表页项目显示正确', () => {
    page.detailAlbPage.updateProject(input_name, page.project2Name);
    const expectData = {
      名称: input_name,
      地址: '3.3.3.3',
      分配项目: page.project2Name,
    };
    page.detailAlbPageVerify.verify(expectData);
  });
  it('ACP2UI-54031 : 单击左导航负载均衡，在列表页-删除（慎重）输入正确的名称，点击删除', () => {
    page.listPage.deleteAlb(input_name);
    page.listPage.search(input_name, 0);
    const expectData = {
      检索: 0,
    };
    page.alb2ListPageVerify.verify(expectData);
  });
  it('ACP2UI-54085 : 单击左导航负载均衡，在详情页-删除（慎重）输入正确的名称，点击删除', () => {
    page.detailAlbPage.deleteAlb(input_name_high);
    page.listPage.search(input_name_high, 0);
    const expectData = {
      检索: 0,
    };
    page.alb2ListPageVerify.verify(expectData);
  });
});
