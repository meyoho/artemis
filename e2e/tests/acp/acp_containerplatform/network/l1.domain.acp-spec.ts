import { DomainPage } from '@e2e/page_objects/acp/domain/domain.page';
import { browser } from 'protractor';

describe('管理视图 域名管理L1自动化', () => {
  const page = new DomainPage();
  const domain_name = page.getTestData('domain');
  const region_name = `${page.clusterName} (${page.clusterDisplayName()})`;
  const project_name1 = `${page.projectName} (UI TEST Project)`;
  const project_name2 = `${page.project2Name} (UI TEST Project)`;
  const expectRowData = new Map<string, string>();

  beforeAll(() => {
    page.preparePage.deleteDomain(domain_name);

    page.login();
  });
  beforeEach(() => {
    page.enterOperationView();
    page.clickLeftNavByText('域名');
    if (process.env.ENV === 'staging') {
      // staging 环境集群列表页有错误提示，等待错误提示消失
      browser.sleep(10000);
    }
    expectRowData.clear();
  });
  afterAll(() => {
    page.preparePage.deleteDomain(domain_name);
  });

  it('ACP2UI-3917 : 创建全域名，分配集群后分配给全部项目', () => {
    const testData = {
      类型: '全域名',
      域名: domain_name,
      分配集群: region_name,
      分配项目: '全部项目',
    };
    page.createPage.addDomain(testData);
    page.listPage.domainNameTable.getColumeTextByName('域名').then(name => {
      expect(name).toContain(domain_name);
    });

    expectRowData.set(
      domain_name,
      `${domain_name}全域名${region_name}全部项目`,
    );

    const expectData = {
      全域名分配集群: expectRowData,
    };

    page.listPageVerify.verify(expectData);
    page.listPageVerify.checkingress(domain_name, page.namespace1Name, true);
    page.listPageVerify.checkingress(
      domain_name,
      page.project2ns1,
      true,
      page.project2Name,
    );
  });

  it('ACP2UI-3929 : 单击左导航域名管理，在列表页单击全域名操作列，更新分配集群，选择集群1- 分配项目1成功', () => {
    // 更新
    const testData = {
      分配集群: region_name,
      分配项目: project_name1,
    };
    page.updatePage.updateDomain(domain_name, '全域名', testData);

    expectRowData.set(
      domain_name,
      `${domain_name}全域名${region_name}${project_name1}`,
    );

    const expectData = {
      全域名不分配集群: expectRowData,
    };

    page.listPageVerify.verify(expectData);
    page.listPageVerify.checkingress(domain_name, page.namespace1Name, true);
  });
  it('ACP2UI-54925 : 单击左导航域名管理，在列表页单击全域名操作列-更新,不分配集群，成功', () => {
    const testData = {
      分配集群: '不分配',
    };
    page.updatePage.updateDomain(domain_name, '全域名', testData);
    expectRowData.set(domain_name, `*.${domain_name}全域名未分配未分配`);

    const expectData = {
      全域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(domain_name, page.namespace1Name, false);

    page.listPageVerify.checkingress(
      domain_name,
      page.project2ns1,
      false,
      page.project2Name,
    );
  });
  it('ACP2UI-54926 : 单击左导航域名管理，在列表页单击全域名操作列-更新,分配集群,分配项目-项目2，成功', () => {
    const testData = {
      分配集群: region_name,
      分配项目: project_name2,
    };
    page.updatePage.updateDomain(domain_name, '全域名', testData);
    expectRowData.set(
      domain_name,
      `*.${domain_name}全域名${region_name}${project_name2}`,
    );

    const expectData = {
      全域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);

    page.listPageVerify.checkingress(domain_name, page.namespace1Name, false);

    page.listPageVerify.checkingress(
      domain_name,
      page.project2ns1,
      true,
      page.project2Name,
    );
  });
  it('ACP2UI-3934 : 验证更新时的取消按钮和关闭按钮', () => {
    const testData = {
      分配集群: region_name,
      分配项目: project_name1,
    };
    page.updatePage.updateDomain(domain_name, '全域名', testData, 'cancel');
    expectRowData.set(
      domain_name,
      `${domain_name}全域名${region_name}${project_name2}`,
    );
    const expectData = {
      全域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);
    page.updatePage.updateDomain(domain_name, '全域名', testData, 'close');
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-3937 : 验证删除时的取消按钮', () => {
    page.deletePage.deleteDomainFromList(domain_name, '全域名', 'cancel');
    expectRowData.set(
      domain_name,
      `*.${domain_name}全域名${region_name}全部项目`,
    );
    const expectData = {
      全域名分配集群: expectRowData,
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2-3935: 单击左导航域名管理，在列表页单击全域名操作列，删除成功', () => {
    page.deletePage.deleteDomainFromList(domain_name, '全域名');
    const expectData = {
      删除域名: domain_name,
    };
    page.listPageVerify.verify(expectData);
    page.listPageVerify.checkingress(domain_name, page.namespace1Name, false);
  });
  it('ACP2UI-3919 : 验证创建时的取消按钮和关闭按钮', () => {
    const testData = {
      类型: '全域名',
      域名: domain_name,
      分配集群: region_name,
      分配项目: '全部项目',
    };
    page.createPage.addDomain(testData, 'cancel');
    const expectData = {
      删除域名: domain_name,
    };
    page.listPageVerify.verify(expectData);
    page.createPage.addDomain(testData, 'close');
    page.listPageVerify.verify(expectData);
  });
});
