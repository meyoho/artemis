import { DomainPage } from '@e2e/page_objects/acp/domain/domain.page';
import { browser } from 'protractor';

describe('管理视图 域名管理L0自动化', () => {
    const page = new DomainPage();
    const domain_name = page.getTestData('domain');

    const expectRowData = new Map<string, string>();

    beforeAll(() => {
        page.preparePage.deleteDomain(domain_name);
        page.login();
        page.enterOperationView();
    });
    beforeEach(() => {
        page.enterOperationView();
        page.clickLeftNavByText('域名');
        if (process.env.ENV === 'staging') {
            // staging 环境集群列表页有错误提示，等待错误提示消失
            browser.sleep(10000);
        }
        expectRowData.clear();
    });
    afterAll(() => {
        page.preparePage.deleteDomain(domain_name);
    });
    it('ACP2UI-3915 : L0:创建全域名，不分配集群，验证任意项目的用户视图ingress都看不到该域名', () => {
        const testData = {
            类型: '全域名',
            域名: domain_name
        };
        page.createPage.addDomain(testData);
        page.listPage.domainNameTable.getColumeTextByName('域名').then(name => {
            expect(name).toContain(domain_name);
        });

        expectRowData.set(domain_name, `${domain_name}全域名未分配未分配`);

        const expectData = {
            全域名不分配集群: expectRowData
        };

        page.listPageVerify.verify(expectData);
        page.listPageVerify.checkingress(
            domain_name,
            page.namespace1Name,
            false
        );
    });
    it('ACP2UI-3935 : L0:删除全域名-验证删除后所有项目看不到域名', () => {
        page.deletePage.deleteDomainFromList(domain_name, '全域名');
        const expectData = {
            删除域名: domain_name
        };
        page.listPageVerify.verify(expectData);
        page.listPageVerify.checkingress(
            domain_name,
            page.namespace1Name,
            false
        );
    });
});
