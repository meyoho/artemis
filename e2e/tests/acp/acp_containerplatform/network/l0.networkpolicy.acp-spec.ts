/* TODO bug: http://jira.alauda.cn/browse/DEV-21290
import { PolicyPage } from '@e2e/page_objects/acp/networkpolicy/policy.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('用户视图 网络策略L0自动化', () => {
  const page = new PolicyPage();
  const region_name = page.calicoClusterName;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  if (!page.networkPolicyIsEnabled(region_name)) {
    return;
  }
  const namespace_name = page.calicoNamespaceName;
  const project_name = page.projectName;
  const policy_name = page.getTestData('l0-policy');

  beforeAll(() => {
    page.login();
    page.enterUserView(namespace_name, project_name, region_name);
    page.listPage.batchDelete();
  });
  beforeEach(() => {
    page.clickLeftNavByText('网络策略');
  });
  afterAll(() => {
    page.listPage.batchDelete();
  });

  it('ACP2UI-57266 : 点击左导航网络策略-点击yaml创建网络策略-跳转到yaml 创建网络策略页面-输入yaml 内容-点击创建-创建成功返回列表页，出现成功提示/点击取消', () => {
    const yamlstring = CommonMethod.readTemplateFile(
      'alauda.networkpolicy.yaml',
      {
        name: policy_name,
        namespace: namespace_name,
        policyType: 'Ingress',
        podSelector: 'true',
      },
    );
    page.createPage.addByYaml(yamlstring, 'cancel');
    page.listPage.searchPolicy(policy_name, 0);
    let expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
    page.createPage.addByYaml(yamlstring);
    page.listPage.searchPolicy(policy_name, 1);
    expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57456 : 点击左导航网络策略-选择一个网络策略-点击名称-弹出该网络策略的yaml 信息', () => {
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'NetworkPolicy',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57271 : 点击左导航网络策略-选择一个网络策略-点击更新-跳转到更新网络策略页面-修改内容-点击更新-更新成功返回列表页，出现成功提示', () => {
    page.listPage.updatePolicy(policy_name);
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'NetworkPolicy',
        metadata: {
          labels: {
            update: 'update',
          },
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57272 : 点击左导航网络策略-选择一个网络策略-点击删除-页面弹出确认删除对话框-点击删除-删除成功', () => {
    page.listPage.deletePolicy(policy_name);
    page.listPage.searchPolicy(policy_name, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});

*/
