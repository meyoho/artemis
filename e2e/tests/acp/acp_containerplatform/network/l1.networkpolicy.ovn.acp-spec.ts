import { PolicyPage } from '@e2e/page_objects/acp/networkpolicy/policy.page';

describe('用户视图 网络策略模板ovn L1自动化', () => {
  const page = new PolicyPage();
  const region_name = page.ovnClusterName;
  if (page.checkRegionNotExist(region_name)) {
    return;
  }
  if (!page.networkPolicyIsEnabled(region_name)) {
    return;
  }
  const namespace_name = page.ovnNamespaceName;
  const project_name = page.projectName;

  beforeAll(() => {
    page.login();
    page.enterUserView(namespace_name, project_name, region_name);
    page.listPage.batchDelete();
  });
  beforeEach(() => {
    page.clickLeftNavByText('网络策略');
  });
  afterAll(() => {
    page.listPage.batchDelete();
  });
  it('ACP2UI-57279|ACP2UI-57264 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板01(全隔离，Namespace 内外的 Pod 都无法访问)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '01.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'default-deny-all';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'NetworkPolicy',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {},
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
  it('ACP2UI-57305 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板02(namespace 内 pod 互通，namespace 外 pod 访问拒绝)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '02.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'deny-other-namespaces';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'NetworkPolicy',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {},
          policyTypes: ['Ingress'],
          ingress: [
            {
              from: [
                {
                  podSelector: {},
                },
              ],
            },
          ],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57307 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板03(允许某个 namespace 访问当前 namespace)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '03.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-other-namespaces';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {},
          ingress: [
            {
              from: [
                {
                  namespaceSelector: {
                    matchLabels: {
                      env: 'production',
                    },
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57308 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板04(允许某个 IP 访问当前 namespace)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '04.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-ip';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {},
          ingress: [
            {
              from: [
                {
                  ipBlock: {
                    cidr: '10.16.0.0/16',
                    except: ['10.16.8.0/24', '10.16.0.2/32'],
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
  it('ACP2UI-57310 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板05(禁止对指定pod的访问)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '05.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'default-deny-all';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
  it('ACP2UI-57312 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板06(namespace 内某些pod可访问指定pod)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '06.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-some-same-namespace';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          ingress: [
            {
              from: [
                {
                  podSelector: {
                    matchLabels: {
                      app: 'mail',
                    },
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57313 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板07(namespace 内都可访问指定pod)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '07.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-same-namespace';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: { name: policy_name, namespace: namespace_name },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          ingress: [
            {
              from: [
                {
                  podSelector: {},
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57314 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板08(某些namespace 的某些pod 可访问指定pod)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '08.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-some-other-namespace';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: { name: policy_name, namespace: namespace_name },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          ingress: [
            {
              from: [
                {
                  podSelector: {
                    matchLabels: {
                      app: 'mail',
                    },
                  },
                  namespaceSelector: {
                    matchLabels: {
                      env: 'production',
                    },
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57315 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板09(某些namespace 内所有pod 可访问指定pod)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '09.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-other-namespace';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: {
          name: policy_name,
          namespace: namespace_name,
        },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          ingress: [
            {
              from: [
                {
                  namespaceSelector: {
                    matchLabels: {
                      env: 'production',
                    },
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-57316 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板10(某些IP可访问指定pod)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '10.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-ip';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: { name: policy_name, namespace: namespace_name },
        spec: {
          podSelector: {
            matchLabels: {
              role: 'db',
            },
          },
          ingress: [
            {
              from: [
                {
                  ipBlock: {
                    cidr: '10.16.0.0/16',
                    except: ['10.16.8.0/24', '10.16.0.2/32'],
                  },
                },
              ],
            },
          ],
          policyTypes: ['Ingress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
  it('ACP2UI-57318 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板11(namespace 里所有 pod 都不允许访问外网)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '11.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'default-deny-all';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        kind: 'NetworkPolicy',
        apiVersion: 'networking.k8s.io/v1',
        metadata: { name: policy_name, namespace: namespace_name },
        spec: {
          podSelector: {},
          egress: [{ to: [{ podSelector: {} }] }],
          policyTypes: ['Egress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
  it('ACP2UI-57317 : 点击左导航网络策略-点击创建-弹出选择网络策略模板页面-选择模板12(指定pod可访问某些IP)-点击确定-网络策略创建成功', () => {
    const testData = {
      模板: '12.',
    };
    page.createPage.addPolicy(testData);
    const policy_name = 'allow-ip';
    page.listPage.viewPolicy(policy_name);
    const expectData = {
      YAML: {
        apiVersion: 'networking.k8s.io/v1',
        kind: 'NetworkPolicy',
        metadata: { name: policy_name, namespace: namespace_name },
        spec: {
          podSelector: { matchLabels: { role: 'db' } },
          egress: [
            {
              to: [
                {
                  ipBlock: {
                    cidr: '10.16.0.0/16',
                    except: ['10.16.8.0/24', '10.16.0.2/32'],
                  },
                },
              ],
            },
          ],
          policyTypes: ['Egress'],
        },
      },
    };
    page.listPageVerify.verify(expectData);
    page.listPage.deletePolicy(policy_name);
  });
});
