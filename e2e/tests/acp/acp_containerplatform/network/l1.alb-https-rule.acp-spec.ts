import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';

describe('用户视图 负载均衡 https规则 L1自动化', () => {
  const page = new Alb2Page();

  const service_name1 = page.getTestData('forhttpsrule1');
  const service_name2 = page.getTestData('forhttpsrule2');
  const secret_name1 = page.getTestData('forhttpsrule1');
  const secret_name2 = page.getTestData('forhttpsrule2');
  const full_domain_name = page
    .getTestData('alb.https.rule.full')
    .replace('-', '.');
  const domain_name_ext = page
    .getTestData('alb.https.rule.ext')
    .replace('-', '.');
  const ext_domain_name = `*.${domain_name_ext}`;
  const https_port = '1005';
  let isReady, alb_name, frontend_name;
  beforeAll(() => {
    page.preparePage.deleteFrontend(alb_name, https_port);
    page.preparePage.createService(service_name1, page.namespace1Name);
    page.preparePage.createService(service_name2, page.namespace1Name);
    page.createSecret(secret_name1, page.namespace1Name);
    page.createSecret(secret_name2, page.namespace1Name);
    page.preparePage.createDomain(full_domain_name, 'full');
    page.preparePage.createDomain(domain_name_ext, 'extensive');

    page.login();
    page.enterUserView(page.namespace1Name, page.projectName);
    page.clickLeftNavByText('负载均衡');

    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
      frontend_name = alb_name + '-' + page.autoComplete(https_port);
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteDomain(full_domain_name);
    page.preparePage.deleteDomain(domain_name_ext);
    page.preparePage.deleteService(service_name1, page.namespace1Name);
    page.preparePage.deleteService(service_name2, page.namespace1Name);
    page.deleteSecret(secret_name1, page.namespace1Name);
    page.deleteSecret(secret_name2, page.namespace1Name);
    page.preparePage.deleteFrontend(alb_name, https_port);
  });
  it('ACP2UI-54876 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击添加监听端口https，默认证书，没有默认内部路由， 源地址哈希，不能添加规则，创建成功', () => {
    if (isReady) {
      const testData = {
        端口: https_port,
        协议: 'HTTPS',
        默认证书: {
          命名空间: page.namespace1Name,
          保密字典: secret_name1,
        },
        会话保持: '源地址哈希',
      };
      page.createFrontendPage.addPort(testData);

      const expectData = {
        名称: frontend_name,
        协议: 'HTTPS',
        默认内部路由: '-',
        默认证书: `${page.namespace1Name}_${secret_name1}`,
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54872 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击添加规则（域名+URL），成功 ', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '域名+URL',
          证书: {
            true: {
              命名空间: page.namespace1Name,
              保密字典: secret_name1,
            },
          },
          内部路由组: [
            [page.namespace1Name, service_name2, '80', 100],
            [page.namespace1Name, service_name1, '80', 100],
          ],

          规则: [
            {
              Header: ['域名'],
              Data: [{ rule: ext_domain_name }, { '': full_domain_name }],
            },
            {
              Header: ['URL'],
              Data: [{ RegEx: ['addurl'] }],
            },
          ],
          会话保持: '不会话保持',
          'URL 重写': 'true',
          重写地址: '/redirect',
        },
      };
      page.detailFrontendPage.addRule(alb_name, https_port, testData);

      const expectData = {
        规则: [
          '域名+URL',
          '域名',
          `rule.${domain_name_ext}`,
          'URL',
          'addurl',
          full_domain_name,
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (50.00%)`,
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (50.00%)`,
        ],
      };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-53663 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-删除规则(验证多个内部路由时不可删除规则)', () => {
    if (isReady) {
      page.detailFrontendPage.enterDetailPage(alb_name, https_port);

      const expectData = { 不能删除规则: '域名+URL' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-52451 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-更新规则(Header+Cookie)', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: 'Header+Cookie',
          内部路由组: [
            [page.namespace1Name, service_name1, '80', 100],
            [page.namespace1Name, service_name2, '80', 100, true],
          ],
          规则: [
            {
              Header: ['Header', 'header'],
              Data: [
                {
                  Equal: ['header'],
                },
              ],
            },
            {
              Header: ['Cookie', 'cookie'],
              Data: [
                {
                  Equal: ['cookie'],
                },
              ],
            },
          ],
          会话保持: '源地址哈希',
          'URL 重写': 'false',
        },
      };
      page.detailFrontendPage.updateRule(
        alb_name,
        https_port,
        '域名+URL',
        testData,
      );

      const expectData = {
        名称: frontend_name,
        协议: 'HTTPS',
        规则: [
          'Header+Cookie',
          'Header',
          'header:header',
          'Cookie',
          'cookie:cookie',
          `${page.namespace1Name} ${service_name1}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53662 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-删除规则', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(alb_name, https_port, 'Header+Cookie');
      const expectData = { 删除规则: 'Header+Cookie' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54872 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击添加规则（只能添加一条规则），成功 ', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '只有IP',
          内部路由组: [[page.namespace1Name, service_name2, '80', 100]],
          规则: [
            {
              Header: ['IP'],
              Data: [
                { Equal: ['10.10.10.10'] },
                { Range: ['10.0.0.0', '10.255.255.255'] },
              ],
            },
          ],
          会话保持: '源地址哈希',
        },
      };
      page.detailFrontendPage.addRule(alb_name, https_port, testData);

      const expectDataIp = {
        规则: [
          '只有IP',
          'IP',
          'IP',
          '10.10.10.10',
          '10.0.0.0-10.255.255.255',
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (100.00%)`,
        ],
      };
      page.detaiFrontendPageVerify.verify(expectDataIp);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54873 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击添加规则,输入规则，点击取消，验证未添加', () => {
    if (isReady) {
      const testData = {
        规则: {
          规则描述: '验证添加取消',
        },
      };
      page.detailFrontendPage.addRule(alb_name, https_port, testData, false);

      const expectData = { 删除规则: '验证添加取消' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54874 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-更新规则，点击取消，验证未更新', () => {
    if (isReady) {
      const testData = {
        规则: {
          内部路由组: [[page.namespace1Name, service_name2, '80', 100, true]],
        },
      };
      page.detailFrontendPage.updateRule(
        alb_name,
        https_port,
        '只有IP',
        testData,
        false,
      );
      const expectData = {
        规则: [
          '只有IP',
          'IP',
          'IP',
          '10.10.10.10',
          '10.0.0.0-10.255.255.255',
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (100.00%)`,
        ],
      };

      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-54342 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-删除规则，点击取消，验证未删除', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(alb_name, https_port, '只有IP', false);
      const expectData = {
        规则: [
          '只有IP',
          'IP',
          'IP',
          '10.10.10.10',
          '10.0.0.0-10.255.255.255',
          `${page.namespace1Name} ${service_name2}: 80 权重 : 100 (100.00%)`,
        ],
      };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-53662 : 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https进入详情页-点击规则-操作-删除规则', () => {
    if (isReady) {
      page.detailFrontendPage.deleteRule(alb_name, https_port, '只有IP');
      const expectData = { 删除规则: '只有IP' };
      page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });

  it('ACP2UI-53514: 单击左导航负载均衡，在列表页点击名称进入详情页，点击监听端口https名称进入详情-操作-删除，删除成功', () => {
    if (isReady) {
      page.detailFrontendPage.deletePort(alb_name, https_port);
      const expectData = { 删除: https_port };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
