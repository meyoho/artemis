import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';
import { IngressPage } from '@e2e/page_objects/acp/ingress/ingress.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { browser } from 'protractor';

describe('用户视图 访问规则L1自动化', () => {
  const page = new IngressPage();
  const ingress_name = page.getTestData('l1-ingress');
  const service_name = page.getTestData('l1-ingress');
  const domain_name_full = page.getTestData('l1-ingress');
  const domain_name2 = page.getTestData('ext-ingress2');
  const domain_name_ext = `*.${domain_name2}`;
  const alb_page = new Alb2Page();
  let isReady, alb_name;

  beforeAll(() => {
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteDomain(domain_name2);
    page.preparePage.deleteService(service_name, page.namespace1Name);

    page.preparePage.createService(service_name, page.namespace1Name);
    page.preparePage.createDomain(domain_name_full, 'full');
    page.preparePage.createDomain(domain_name2, 'extensive');

    page.login();
    page.enterUserView(page.namespace1Name);
    try {
      alb_name = alb_page.preparePage.isReady();
      isReady = true;
    } catch {
      isReady = false;
    }
  });
  afterAll(() => {
    page.preparePage.deleteIngress(ingress_name, page.namespace1Name);
    page.preparePage.deleteDomain(domain_name_full);
    page.preparePage.deleteDomain(domain_name2);
    page.preparePage.deleteService(service_name, page.namespace1Name);
  });
  it('ACP2UI-55011 : 创建访问规则，输入数据后，点击取消，验证未创建', () => {
    const testData = {
      名称: ingress_name,
      域名: domain_name_full,
      规则: [['/uiauto-create', service_name, '80']],
    };
    page.createPage.addIngress(testData, 'cancel');
    page.listPage.searchIngress(ingress_name, 0);
    const expectData = {
      数量: 0,
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-52648 : 创建访问规则-yaml', () => {
    const yamlstring = CommonMethod.readyamlfile('alauda.ingress.yaml', {
      $INGRESS_NAME: ingress_name,
      $NAMESPACE: page.namespace1Name,
      $HOST: domain_name_full,
      $SERVICE_NAME: service_name,
      $PATH: '/uiauto-create-ingress',
    });
    page.createPage.addIngressByYaml(yamlstring);
    const expectData = {
      规则: [
        {
          地址: `${domain_name_full}/uiauto-create-ingress`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52649 : 单击左导航访问规则，在列表页，验证数据，成功', () => {
    page.listPage.searchIngress(ingress_name);
    const temp = new Map();
    temp.set(ingress_name, [`${domain_name_full}/uiauto-create-ingress`]);
    const expectData = { 规则: temp };
    page.listPageVerify.verify(expectData);
  });

  it('ACP2UI-52655 : 创建访问规则-无https证书,验证alb的80端口有规则', () => {
    if (isReady) {
      alb_page.detailFrontendPage.enterDetailPage(alb_name, 80);
      const expectData = {
        规则: [
          `${page.namespace1Name}/${ingress_name}`,
          '域名',
          domain_name_full,
          'URL',
          '/uiauto-create-ingress',
          `${page.namespace1Name} ${service_name}: 80 权重 : 100 (100.00%)`,
        ],
      };
      alb_page.detaiFrontendPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-55012 : 更新访问规则，输入数据后，点击取消，验证未更新', () => {
    const testData = {
      域名: domain_name_full,
      规则: [
        ['/update1', service_name, '80'],
        ['/update2', service_name, '80', true],
      ],
    };
    page.updatePage.updateIngress(ingress_name, testData, 'cancel');
    page.listPage.searchIngress(ingress_name);
    const temp = new Map();
    temp.set(ingress_name, [`${domain_name_full}/uiauto-create-ingress`]);
    const expectData = { 规则: temp };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-52660 : 更新访问规则-无https，多条规则，验证alb的80端口有多条规则', () => {
    const testData = {
      域名: domain_name_ext,
      域名前缀: 'update',
      规则: [
        ['/uiauto-update-ingress-http1', service_name, '80'],
        ['/uiauto-update-ingress-http2', service_name, '80'],
      ],
    };
    page.updatePage.updateIngress(ingress_name, testData);
    const expectData = {
      规则: [
        {
          地址: `update.${domain_name2}/uiauto-update-ingress-http1`,
          内部路由: service_name,
          服务端口: 80,
        },
        {
          地址: `update.${domain_name2}/uiauto-update-ingress-http2`,
          内部路由: service_name,
          服务端口: 80,
        },
      ],
      HTTPS证书无数据: '无HTTPS 证书',
    };
    page.detailPageVerify.verify(expectData);
    if (isReady) {
      browser.sleep(20000).then(() => {
        alb_page.detailFrontendPage.enterDetailPage(alb_name, 80);
        const expectAlbData = {
          规则: [
            `${page.namespace1Name}/${ingress_name}`,
            '域名',
            `update.${domain_name2}`,
            'URL',
            '/uiauto-update-ingress-http1',
            '/uiauto-update-ingress-http2',
            `${page.namespace1Name} ${service_name}: 80 权重 : 100 (100.00%)`,
          ],
        };
        alb_page.detaiFrontendPageVerify.verify(expectAlbData);
      });
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
  it('ACP2UI-55013 : 删除访问规则，点击取消，验证未删除', () => {
    page.deletePage.deleteIngress(ingress_name, 'cancel');
    page.listPage.searchIngress(ingress_name, 1);
    const expectData = {
      数量: 1,
    };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-55014 : 创建访问规则，无https证书，删除访问规则，验证alb的80端口规则删除', () => {
    page.deletePage.deleteIngress(ingress_name);
    // 验证搜索正确
    page.listPage.searchIngress(ingress_name, 0);
    const expectData = {
      数量: 0,
    };
    page.listPageVerify.verify(expectData);
    if (isReady) {
      alb_page.detailFrontendPage.enterDetailPage(alb_name, 80);
      const expectData2 = {
        删除规则: [
          domain_name_full,
          `update.${domain_name2}`,
          '/uiauto-update-ingress-http1',
          '/uiauto-update-ingress-http2',
        ],
      };
      alb_page.detaiFrontendPageVerify.verify(expectData2);
    } else {
      fail(`没有找到负载均衡器，请检查集群${page.clusterName}`);
    }
  });
});
