import { ServicePage } from '@e2e/page_objects/acp/service/service.page';

describe('用户视图 内部路由L1自动化', () => {
  const page = new ServicePage();
  const service_name = page.getTestData('l1-svc');
  const namespace_name = page.namespace1Name;

  beforeAll(() => {
    page.preparePage.deleteService(service_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('内部路由');
  });
  afterAll(() => {
    page.preparePage.deleteService(service_name);
  });
  it('ACP2UI-56826 : 单击左导航内部路由-列表页单击创建内部路由-输入名称-虚拟 IP打开-外网访问打开-端口gRPC-选择器添加不存在的key/value-点击创建', () => {
    const testData = {
      名称: service_name,
      '虚拟 IP': true,
      外网访问: true,
      会话保持: true,
      端口: [['gRPC', 80, 80]],
      选择器: [['key', 'value']],
    };
    //注意： 创建成功后进入的列表页， 以后可能会改成直接进入详情页，
    //      现在更新成功后是进入详情页的
    page.listPage.addService(testData);
    const expectData = {
      '虚拟 IP-clusterip/nodeport': true,
      会话保持: '是',
      外网访问: '是',
      端口: [
        '端口名称',
        '协议',
        '服务端口',
        '容器端口',
        '主机端口',
        'TCP',
        'grpc-80-80',
        '80',
        '80',
      ],
      '主机端口-nodeport': true,
      选择器: ['键', '值', 'key', 'value'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54965 : 单击左导航内部路由，在列表页单击名称进入详情，操作-更新-取消', () => {
    const testData = {
      选择器: [['key', 'updatevalue'], ['updatekey', '']],
    };
    page.detailPage.update(service_name, testData, 'cancel');
    const expectData = {
      选择器: ['键', '值', 'key', 'value'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53623 : 单击左导航内部路由，在列表页单击名称进入详情，操作-更新', () => {
    const testData = {
      端口: [
        ['TCP', 81, 80],
        ['HTTP', 82, 80],
        ['HTTPS', 83, 80],
        ['HTTP2', 84, 80],
        ['gRPC', 85, 90],
        ['UDP', 86, 80],
      ],
      选择器: [['key', 'updatevalue'], ['updatekey', '']],
    };
    page.detailPage.update(service_name, testData);
    const expectData = {
      端口: [
        '端口名称',
        '服务端口',
        '容器端口',
        'tcp-81-80',
        '81',
        '80',
        'http-82-80',
        'https-83-80',
        'http2-84-80',
        'grpc-85-90',
        'udp-86-80',
      ],
      '主机端口-nodeport': true,
      选择器: ['键', '值', 'key', 'updatevalue', 'updatekey'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54966 : 单击左导航内部路由，在列表页单击名称进入详情，更新标签-取消', () => {
    const testData = [['addlabel', 'addlabel'], ['label', 'label']];
    page.detailPage.updateLabel(service_name, testData, 'cancel');
    const expectData = {
      不包含标签: ['addlabel: addlabel', 'label: label'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54969 : 单击左导航内部路由，在列表页单击名称进入详情，更新标签-关闭', () => {
    const testData = [['addlabel', 'addlabel'], ['label', 'label']];
    page.detailPage.updateLabel(service_name, testData, 'close');
    const expectData = {
      不包含标签: ['addlabel: addlabel', 'label: label'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53619 : 单击左导航内部路由，在列表页单击名称进入详情，更新标签 ', () => {
    const testData = [['addlabel', 'addlabel'], ['label', 'label']];
    page.detailPage.updateLabel(service_name, testData);
    const expectData = {
      标签: ['addlabel: addlabel', 'label: label'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54968 : 单击左导航内部路由，在列表页单击名称进入详情，更新注解-取消', () => {
    const testData = [
      ['addannotation', 'addannotation'],
      ['annotation', 'annotation'],
    ];
    page.detailPage.updateAnnotation(service_name, testData, 'cancel');
    const expectData = {
      不包含注解: ['addannotation: addannotation', 'annotation: annotation'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54970 : 单击左导航内部路由，在列表页单击名称进入详情，更新注解-关闭', () => {
    const testData = [
      ['addannotation', 'addannotation'],
      ['annotation', 'annotation'],
    ];
    page.detailPage.updateAnnotation(service_name, testData, 'close');
    const expectData = {
      不包含注解: ['addannotation: addannotation', 'annotation: annotation'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53625 : 单击左导航内部路由，在列表页单击名称进入详情，更新注解', () => {
    const testData = [
      ['addannotation', 'addannotation'],
      ['annotation', 'annotation'],
    ];
    page.detailPage.updateAnnotation(service_name, testData);
    const expectData = {
      注解: ['addannotation: addannotation', 'annotation: annotation'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-52667 : 单击左导航内部路由，在列表页单击名称进入详情', () => {
    page.listPage.viewService(service_name);
    const expectData = {
      标签: ['addlabel: addlabel', 'label: label'],
      注解: ['addannotation: addannotation', 'annotation: annotation'],
      来源: '-',
      端口: [
        '端口名称',
        '服务端口',
        '容器端口',
        'tcp-81-80',
        '81',
        '80',
        'http-82-80',
        'https-83-80',
        'http2-84-80',
        'grpc-85-90',
        'udp-86-80',
      ],
      选择器: ['键', '值', 'key', 'updatevalue', 'updatekey'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53667 : 单击左导航内部路由，在列表页单击名称进入详情,切换到容器组，错误的selector时列表为空', () => {
    page.listPage.viewService(service_name);
    const expectData = { 容器组: [] };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53668 : 单击左导航内部路由，在列表页单击名称进入详情,切换到yaml,yaml正确', () => {
    page.listPage.viewService(service_name);
    const expectData = {
      YAML: {
        kind: 'Service',
        apiVersion: 'v1',
        metadata: {
          name: service_name,
          namespace: namespace_name,
          labels: {
            addlabel: 'addlabel',
            label: 'label',
          },
          annotations: {
            addannotation: 'addannotation',
            annotation: 'annotation',
          },
        },
        spec: {
          ports: [
            {
              name: 'tcp-81-80',
              protocol: 'TCP',
              port: 81,
              targetPort: 80,
            },
            {
              name: 'http-82-80',
              protocol: 'TCP',
              port: 82,
              targetPort: 80,
            },
            {
              name: 'https-83-80',
              protocol: 'TCP',
              port: 83,
              targetPort: 80,
            },
            {
              name: 'http2-84-80',
              protocol: 'TCP',
              port: 84,
              targetPort: 80,
            },
            {
              name: 'grpc-85-90',
              protocol: 'TCP',
              port: 85,
              targetPort: 90,
            },
            {
              name: 'udp-86-80',
              protocol: 'UDP',
              port: 86,
              targetPort: 80,
            },
          ],
          selector: {
            key: 'updatevalue',
            updatekey: '',
          },
          type: 'NodePort',
          sessionAffinity: 'ClientIP',
        },
      },
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54964 : 单击左导航内部路由，在列表页单击名称进入详情，操作-删除-取消', () => {
    page.detailPage.delete(service_name, 'cancel');
    const expectData = {
      注解: ['addannotation: addannotation', 'annotation: annotation'],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53624 : 单击左导航内部路由，在列表页单击名称进入详情，操作-删除', () => {
    page.detailPage.delete(service_name);
    // 验证搜索正确
    page.listPage.searchService(service_name, 0);
    const expectData = { 检索: 0 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53618 : 单击左导航内部路由，在列表页单击创建内部路由按钮，创建内部路由，点击取消按钮', () => {
    const testData = {
      名称: service_name,
      端口: [['TCP', 80, 80]],
      选择器: [['key', 'value']],
    };
    page.listPage.addService(testData, 'cancel');
    page.listPage.searchService(service_name, 0);
    const expectData = { 检索: 0 };
    page.listPageVerify.verify(expectData);
  });
});
