import { Alb2Page } from '@e2e/page_objects/acp/alb2/alb2.page';
import { $ } from 'protractor';

describe('管理视图 负载均衡 L0自动化', () => {
  const page = new Alb2Page();
  const region_name = page.clusterName;

  let alb_name;
  let isReady;
  beforeAll(() => {
    page.login();
    page.enterOperationView();
    page.clickLeftNavByText('负载均衡');
    page.switchCluster_onAdminView(region_name).then(() => {
      page.waitElementPresent(
        $('.aui-card__content aui-table'),
        '等待 10s后，负载均衡列表页未显示',
        10000,
      );
      page.waitElementNotPresent($('.aui-icon-spinner'), '', 20000);
    });
    try {
      alb_name = page.preparePage.isReady();
      isReady = true;
    } catch {
      isReady = false;
    }
  });
  beforeEach(() => {});
  afterAll(() => {});
  it('ACP2UI-54078 : L0:单击左导航负载均衡，在列表页-更新项目，选择全部项目，验证列表页的项目显示正确', () => {
    page.listPage.updateProject(alb_name, '全部项目');
    const expectData = {
      创建: {
        名称: `${alb_name}`,
        分配项目: '全部项目',
      },
    };
    page.alb2ListPageVerify.verify(expectData);
  });
  it('ACP2UI-53520 : 单击左导航负载均衡，在列表页，按名称过滤，成功', () => {
    if (isReady) {
      page.listPage.search(alb_name);
      let expectData = {
        检索: 1,
      };
      page.alb2ListPageVerify.verify(expectData);
      // 按大写过滤
      page.listPage.search(alb_name.toUpperCase());
      page.alb2ListPageVerify.verify(expectData);
      // 按不存在的名称过滤
      expectData = {
        检索: 0,
      };
      page.listPage.search('notexist', 0);
      page.alb2ListPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${region_name}`);
    }
  });
  it('ACP2UI-52444 : 单击左导航负载均衡，在列表页点击名称进入详情页，验证基本信息显示', () => {
    if (isReady) {
      page.listPage.enterAlb2Detail(alb_name);
      const expectData = {
        名称: `${alb_name}`,
        分配项目: '全部项目',
      };
      page.detailAlbPageVerify.verify(expectData);
    } else {
      fail(`没有找到负载均衡器，请检查集群${region_name}`);
    }
  });
});
