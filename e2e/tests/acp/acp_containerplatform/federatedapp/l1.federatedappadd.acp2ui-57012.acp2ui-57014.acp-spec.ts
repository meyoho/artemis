import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { StatefulSetPage } from '@e2e/page_objects/acp/statefulset/statefulset.page';
import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';

describe('用户视图 联邦应用 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_app_name1 = page.getTestData('57012');
  const fed_deploy_name1 = `${fed_app_name1}-deploy`;
  const fed_sts_name1 = `${fed_app_name1}-sts`;
  const fed_app_name2 = page.getTestData('57014');
  const fed_deploy_name2 = `${fed_app_name2}-deploy`;
  const fed_sts_name2 = `${fed_app_name2}-sts`;
  const image = ServerConf.TESTIMAGE;
  const deploy_page = new DeploymentPage();
  const sts_page = new StatefulSetPage();
  const pvc_page = new StoragePage();
  const third_name = `${fed_app_name1}-pvc`;

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    pvc_page.prePareData.deletePvc(third_name, namespace_name, fed_host_name);
    const pvc_data = {
      name: third_name,
      namespace: namespace_name,
    };
    pvc_page.prePareData.createPvcByYaml(pvc_data, fed_host_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name2,
    );
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name1,
    );
    pvc_page.prePareData.deletePvc(third_name, namespace_name, fed_host_name);
  });
  it('ACP2UI-57012 : 创建deployment(pvc)+statefulset(pvc)+无svc+无cm+无secret+差异化（statefulset集群 1 cpu+deployment集群 2 mem）', () => {
    const data = {
      名称: fed_app_name1,

      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy',
          存储卷: [
            {
              名称: 'deploy-volume-pvc',
              类型: '持久卷声明',
              持久卷声明: third_name,
            },
            {
              名称: 'deploy-volume-path',
              类型: '主机路径',
              主机路径: '/tmp',
            },
            {
              名称: 'deploy-volume-empty',
              类型: '空目录',
            },
          ],
          亲和性: [
            {
              亲和性: 'Pod 亲和',
              方式: '高级',
              亲和类型: 'Preferred',
              权重: 3,
              匹配标签: [
                [
                  `service.${ServerConf.LABELBASEDOMAIN}/name`,
                  fed_deploy_name1,
                ],
              ],
            },
          ],
          存储卷挂载: [
            [
              ['持久卷声明', 'deploy-volume-pvc'],
              'l1-deploy-pvc',
              '/test/pvcsub',
            ],
            [['持久卷声明', 'deploy-volume-pvc'], '', '/test/pvc'],
            [['主机路径', 'deploy-volume-path'], 'sub', '/test/path'],
            [['空目录', 'deploy-volume-empty'], '', '/test/empty'],
          ],
        },
      ],
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
          存储卷: [
            {
              名称: 'deploy-volume-pvc',
              类型: '持久卷声明',
              持久卷声明: third_name,
            },
            {
              名称: 'deploy-volume-path',
              类型: '主机路径',
              主机路径: '/tmp',
            },
            {
              名称: 'deploy-volume-empty',
              类型: '空目录',
            },
          ],
          亲和性: [
            {
              亲和性: 'Pod 反亲和',
              方式: '高级',
              亲和类型: 'Preferred',
              权重: 3,
              匹配标签: [
                [
                  `service.${ServerConf.LABELBASEDOMAIN}/name`,
                  fed_deploy_name1,
                ],
              ],
            },
          ],
          存储卷挂载: [
            [
              ['持久卷声明', 'deploy-volume-pvc'],
              'l1-deploy-pvc',
              '/test/pvcsub',
            ],
            [['持久卷声明', 'deploy-volume-pvc'], '', '/test/pvc'],
            [['主机路径', 'deploy-volume-path'], 'sub', '/test/path'],
            [['空目录', 'deploy-volume-empty'], '', '/test/empty'],
          ],
        },
      ],
      差异化: [
        {
          资源类型: '有状态副本集',
          资源: fed_sts_name1,
          差异化: [[fed_host_name, '资源限制-CPU', '20m']],
        },
        {
          资源类型: '部署',
          资源: fed_deploy_name1,
          差异化: [[fed_host_name, '资源限制-内存', '20Mi']],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name1,
        工作负载: [
          '部署',
          '有状态副本集',
          fed_deploy_name1,
          fed_sts_name1,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        差异化: [
          '部署',

          fed_deploy_name1,
          fed_host_name,
          '资源限制-内存',
          '20Mi',
          '有状态副本集',
          fed_sts_name1,
          '资源限制-CPU',
          '20m',
        ],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'deployment',
        fed_deploy_name1,
        namespace_name,
        true,
      );
      page.clickLeftNavByText('部署');
      deploy_page.listPage.enterDetail(fed_deploy_name1);
      const expectDetailData = {
        状态: '运行中',
        实例数: '1',
        资源限制: 'CPU:10m内存:20Mi',
        存储卷: [
          'deploy-volume-pvc',
          '持久卷声明',
          third_name,
          '主机路径',
          '/tmp',
          'deploy-volume-empty',
          '空目录',
          '-',
        ],
        'Pod 亲和': [
          'Pod 亲和',
          'Preferred',
          '3',
          'kubernetes.io/hostname',
          `service.${ServerConf.LABELBASEDOMAIN}/name: ${fed_deploy_name1}`,
        ],
        已挂载存储卷: [
          'deploy-volume-pvc',
          'l1-deploy-pvc',
          '/test/pvcsub',
          '/test/pvc',
          'deploy-volume-path',
          '/test/path',
          'deploy-volume-empty',
          '/test/empty',
        ],
      };
      deploy_page.detailPageVerify.verify(expectDetailData);
      page.preparePage.waitResourceExist(
        'statefulset',
        fed_sts_name1,
        namespace_name,
        true,
      );
      page.clickLeftNavByText('有状态副本集');
      sts_page.listPage.enterDetail(fed_sts_name1);
      const expectStsDetailData = {
        状态: '运行中',
        实例数: '1',
        资源限制: 'CPU:20m内存:10Mi',
        存储卷: [
          'deploy-volume-pvc',
          '持久卷声明',
          third_name,
          '主机路径',
          '/tmp',
          'deploy-volume-empty',
          '空目录',
          '-',
        ],
        'Pod 亲和': [
          'Pod 反亲和',
          'Preferred',
          '3',
          'kubernetes.io/hostname',
          `service.${ServerConf.LABELBASEDOMAIN}/name: ${fed_deploy_name1}`,
        ],
        已挂载存储卷: [
          'deploy-volume-pvc',
          'l1-deploy-pvc',
          '/test/pvcsub',
          '/test/pvc',
          'deploy-volume-path',
          '/test/path',
          'deploy-volume-empty',
          '/test/empty',
        ],
      };
      sts_page.detailPageVerify.verify(expectStsDetailData);
    });
  });
  it('ACP2UI-57014 : 创建deployment+statefulset+无svc+无cm+无secret+无差异化', () => {
    const data = {
      名称: fed_app_name2,
      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy',
          标签: [['deploylabel', 'deploylabel']],
          主机选择器: ['beta.kubernetes.io/os:linux'],
          'Host 模式': 'true',
        },
      ],
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
          实例数量: 2,
          更新策略: [2],
          标签: [['stslabel', 'stslabel']],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name2,
        工作负载: [
          '部署',
          '有状态副本集',
          fed_deploy_name2,
          fed_sts_name2,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'deployment',
        fed_deploy_name2,
        namespace_name,
        true,
      );
      deploy_page.listPage.enterDetail(fed_deploy_name2);
      const expectDetailData = {
        状态: '运行中',
        实例数: '1',
        标签: 'deploylabel:deploylabel',
        主机选择器: 'beta.kubernetes.io/os:linux',
      };
      deploy_page.detailPageVerify.verify(expectDetailData);
      page.preparePage.waitResourceExist(
        'statefulset',
        fed_sts_name2,
        namespace_name,
        true,
      );
      sts_page.listPage.enterDetail(fed_sts_name2);
      const expectStsDetailData = {
        状态: '运行中',
        实例数: '2',
        更新策略: '滚动更新策略 ( partition: 2 )',
      };
      sts_page.detailPageVerify.verify(expectStsDetailData);
    });
  });
});
