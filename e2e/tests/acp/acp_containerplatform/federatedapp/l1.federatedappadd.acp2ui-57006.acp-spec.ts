import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 联邦应用 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_app_name = page.getTestData('57006');
  const fed_deploy_name1 = `${fed_app_name}-deploy1`;
  const fed_deploy_name2 = `${fed_app_name}-deploy2`;
  const fed_sts_name = `${fed_app_name}-sts`;
  const fed_svc_name = `${fed_app_name}-svc`;
  const fed_secret_name = `${fed_app_name}-secret`;
  const image = ServerConf.TESTIMAGE;
  const app_page = new Application();
  const deploy_page = new DeploymentPage();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name,
    );
  });
  it('ACP2UI-57006 : 创建多个deployment+statefulset+svc(nodeport+空)+configmap(导入)+secret(ssh)+差异化(多个deployment)', () => {
    const data = {
      名称: fed_app_name,
      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy1',
          实例数量: 2,
          更新策略: ['20%', '20%'],
          标签: [['deploylabel', 'deploylabel']],
          亲和性: [
            {
              亲和性: 'Pod 反亲和',
              方式: '基本',
              亲和组件: ['部署', fed_deploy_name1],
            },
          ],
        },
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy2',
          启动命令: [['/bin/sh']],
          参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
          环境变量: [['env', 'env']],
          添加存活性健康检查: {
            协议类型: 'HTTP',
            启动时间: 10,
            间隔: 10,
          },
          端口: [['TCP', '80', '']],
          日志文件: [['/var/*.*']],
          排除日志文件: [['/var/hehe.txt']],
          亲和性: [
            {
              亲和性: 'Pod 亲和',
              方式: '基本',
              亲和组件: ['部署', fed_deploy_name2],
            },
          ],
        },
      ],
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
          启动命令: [['/bin/sh']],
          参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
          环境变量: [['env', 'env']],
          添加存活性健康检查: {
            协议类型: 'HTTP',
            启动时间: 10,
            间隔: 10,
          },
          端口: [['TCP', '80', '']],
          日志文件: [['/var/*.*']],
          排除日志文件: [['/var/hehe.txt']],
          亲和性: [
            {
              亲和性: 'Pod 亲和',
              方式: '基本',
              亲和组件: ['有状态副本集', fed_sts_name],
            },
          ],
        },
      ],
      内部路由: [
        {
          名称: fed_svc_name,
          '虚拟 IP': true,
          外网访问: true,
          工作负载: { 资源类型: '有状态副本集', 资源名称: fed_sts_name },
          端口: [['HTTPS', 80, 80]],
          会话保持: false,
        },
      ],
      保密字典: [
        {
          名称: fed_secret_name,
          类型: 'SSH 认证',
          'SSH 私钥': `-----BEGIN OPENSSH PRIVATE KEY-----
      -----END OPENSSH PRIVATE KEY-----`,
        },
      ],
      差异化: [
        {
          资源类型: '部署',
          资源: fed_deploy_name1,
          差异化: [
            [fed_host_name, '实例数量', '0'],
            [fed_host_name, '资源限制-CPU', '20m'],
            [fed_host_name, '资源限制-内存', '20Mi'],
          ],
        },
        {
          资源类型: '部署',
          资源: fed_deploy_name2,
          差异化: [[fed_host_name, '实例数量', '2']],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name,
        工作负载: [
          '部署',
          '有状态副本集',
          fed_deploy_name1,
          fed_deploy_name2,
          fed_sts_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        内部路由: [
          fed_svc_name,
          `有状态副本集: ${fed_sts_name}`,
          '服务端口',
          '容器端口',
          '80',
        ],
        配置: [fed_secret_name, 'SSH 认证'],
        差异化: [
          fed_host_name,
          '实例数量',
          '0',
          '资源限制-CPU',
          '20m',
          '资源限制-内存',
          '20Mi',
        ],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'application',
        fed_app_name,
        namespace_name,
        true,
      );
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        应用状态: {
          app名称: fed_app_name,
          状态: '部分运行',
        },
        Deployment: {
          名称: fed_deploy_name1,
          实例数量: {
            pod名称: fed_deploy_name1,
            pod数量: '0',
          },
          资源限制: 'CPU: 20m内存: 20Mi',
        },
        StatefulSet: {
          名称: fed_sts_name,
          实例数量: {
            pod名称: fed_sts_name,
            pod数量: '1',
          },
          资源限制: 'CPU: 10m内存: 10Mi',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
      page.clickLeftNavByText('部署');
      deploy_page.listPage.enterDetail(fed_deploy_name2);
      const expectDetailData = {
        状态: '运行中',
        实例数: '2',
        资源限制: 'CPU:10m内存:10Mi',
        启动命令: ['/bin/sh'],
        参数: ['-c', 'while true; do sleep 10; echo 123; done'],
        环境变量: ['env'],
      };
      deploy_page.detailPageVerify.verify(expectDetailData);
    });
  });
});
