import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('用户视图 应用联邦化 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const image = ServerConf.TESTIMAGE;
  const app_page = new Application();
  const app_name = app_page.getTestData('58747');
  const container_name = app_page.detailAppPage.getImageName(image);

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
    app_page.prepare.deleteApp(app_name, namespace_name, fed_host_name);
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      app_name,
    );
  });
  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      app_name,
    );
  });
  it('ACP2UI-58747 : 子集群应用联邦化，创建应用包含五种资源，点击应用联邦化，提示资源列表正确，生成联邦应用，主集群详情页的内容正确；其他集群的资源能创建出来；普通应用有联邦化的标记不能再联邦化', () => {
    const configmap_data = {
      name: app_name,
      namespace: namespace_name,
      datas: { key: 'value' },
    };
    const secret_data = {
      name: app_name,
      namespace: namespace_name,
      datatype: 'Opaque',
      datas: {
        key1: 'value1',
      },
    };
    const create_data = {
      选择镜像: {
        方式: '输入',
        镜像地址: image,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'StatefulSet',
      },
      内部路由: {
        名称: app_name,
        '虚拟 IP': 'true',
        会话保持: 'false',
        目标组件: `${app_name}-${container_name}`,
        端口: [['TCP', '80', '80']],
      },
    };
    app_page.createAppPage.createAppNotWaitRunning(create_data, namespace_name);
    const update_data = {
      添加工作负载: [
        {
          选择镜像: {
            方式: '输入',
            镜像地址: image,
          },
          部署模式: 'Deployment',
        },
      ],
      'YAML 添加资源': [
        CommonMethod.readTemplateFile('alauda.configmap.yaml', configmap_data),
        CommonMethod.readTemplateFile('alauda.secret_tpl.yaml', secret_data),
      ],
    };
    app_page.createAppPage.new_update(update_data, app_name, namespace_name);
    app_page.detailAppPage.federatedApp();
    browser.sleep(1).then(() => {
      // 生成联邦应用，主集群详情页的内容正确
      page.preparePage.waitResourceExist(
        'federatedapplication',
        app_name,
        namespace_name,
      );
      page.clickLeftNavByText('联邦应用');
      page.listPage.enterDetail(app_name);
      const verifyData = {
        面包屑: `联邦应用/${app_name}`,
        名称: app_name,
        工作负载: [
          '部署',
          '有状态副本集',
          app_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        配置: [app_name, '', app_name, 'Opaque'],
        内部路由: [
          app_name,
          `有状态副本集: ${app_name}-${container_name}`,
          '服务端口',
          '容器端口',
          '80',
        ],
      };
      page.detailVerify.verify(verifyData);
      // 其他集群的资源能创建出来
      if (detail.Member.length > 0) {
        page.detailPage.enterAppDetail(detail.Member[0]);
        const v_data = {
          标题: `应用/${app_name}`,
          名称: app_name,
          显示名称: '-',
          应用状态: {
            app名称: app_name,
            状态: '运行中',
          },
          // 普通应用有联邦化的标记不能再联邦化
          联邦化标记: true,
          按钮不可点击: ['开始', '停止', '操作'],
          Deployment: {
            名称: app_name,
            状态: {
              pod名称: `${app_name}-${container_name}`,
              pod状态: '运行中',
            },
            实例数量: {
              pod名称: `${app_name}-${container_name}`,
              pod数量: '1',
            },
            镜像: image,
          },
          StatefulSet: {
            名称: app_name,
            状态: {
              pod名称: `${app_name}-${container_name}`,
              pod状态: '运行中',
            },
            实例数量: {
              pod名称: `${app_name}-${container_name}`,
              pod数量: '1',
            },
            镜像: image,
          },
          其他资源: [
            [app_name, 'Service'],
            [app_name, 'ConfigMap'],
            [app_name, 'Secret'],
          ],
        };
        app_page.appDetailVerifyPage.verify(v_data);
      }
    });
  });
});
