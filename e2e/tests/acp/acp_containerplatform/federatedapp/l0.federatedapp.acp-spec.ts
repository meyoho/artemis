import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('用户视图 联邦应用 L0自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_all_name = detail.All;
  const fed_app_name = page.getTestData('l0-fedapp');
  const fed_cm_name = page.getTestData('l0-fedapp-cm');
  const app_page = new Application();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name,
    );
    page.preparePage.deleteFederatedResource(
      'federatedconfigmap',
      namespace_name,
      fed_cm_name,
    );
  });
  it('ACP2UI-56994 : l0点击左导航联邦应用，点击创建联邦应用，输入名称，点击创建；创建成功跳转到详情页，基本信息 （显示名称、创建时间、创建人、状态）、资源显示空、差异化显示空，操作（更新删除）', () => {
    const data = {
      名称: fed_app_name,
      显示名称: fed_app_name,
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'application',
        fed_app_name,
        namespace_name,
        true,
      );
      const verifyData = {
        面包屑: `联邦应用/${fed_app_name}`,
        名称: fed_app_name,
        无差异化: true,
        无资源: true,
      };
      page.detailVerify.verify(verifyData);
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        应用状态: {
          app名称: fed_app_name,
          状态: '无工作负载',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
    });
  });
  it('ACP2UI-56992 : l0点击左导航联邦应用，进入列表：表头名称、所属集群（主集群、从集群都要显示）、创建时间、操作（更新、删除），无数据时，显示无联邦应用', () => {
    page.listPage.search(fed_app_name);
    const expectData = {
      标题: '联邦应用',
      header: ['名称', '所属集群', '创建时间'],
      数量: 1,
      创建: {
        名称: fed_app_name,
        所属集群: fed_all_name,
      },
    };
    page.listVerify.verify(expectData);
    page.listPage.search('fedapperror', 0);
    const expectNoData = {
      数量: 0,
      无联邦应用: true,
    };
    page.listVerify.verify(expectNoData);
  });
  it('ACP2UI-56996 : l0点击左导航联邦应用，点击联邦应用后的操作-更新，添加一个configmap，点击更新，进入详情页资源显示configmap', () => {
    const data = {
      配置字典: [
        {
          名称: fed_cm_name,
          配置项: [{ 键: 'key1', 值: 'value1' }],
        },
      ],
    };
    page.listPage.update(fed_app_name, data);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'configmap',
        fed_cm_name,
        namespace_name,
        true,
      );
      browser.refresh();
      const verifyData = {
        面包屑: `联邦应用/${fed_app_name}`,
        名称: fed_app_name,
        配置: [fed_cm_name, ''],
        应用状态: [fed_host_name],
      };
      page.detailVerify.verify(verifyData);
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        应用状态: {
          app名称: fed_app_name,
          状态: '无工作负载',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
    });
  });
  it('ACP2UI-56997 : l0点击左导航联邦应用，点击联邦应用后的操作-删除，点击确认，停在列表页，按名称搜索不存在', () => {
    page.listPage.delete(fed_app_name);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'federatedapplication',
        fed_app_name,
        namespace_name,
        false,
      );
      // 验证搜索正确
      page.listPage.search(fed_app_name, 0);
      const expectData = { 数量: 0 };
      page.listVerify.verify(expectData);
    });
  });
});
// 配置字典: [
//   {
//     名称: fed_cm_name,
//     配置项: [{ 键: 'key1', 值: 'value1' }],
//   },
// ],
// 保密字典: [
//   {
//     名称: fed_secret_name,
//     类型: '用户名/密码',
//     用户名: `example@alauda.io`,
//     密码: `&&$$.*12L`,
//   },
// ],
// 部署: [
//   {
//     镜像: { 方式: '输入', 镜像地址: ServerConf.TESTIMAGE },
//     名称: 'deploy',
//   },
// ],
// 有状态副本集: [
//   {
//     镜像: { 方式: '输入', 镜像地址: ServerConf.TESTIMAGE },
//     名称: 'sts',
//   },
// ],
// 内部路由: [
//   {
//     名称: fed_svc_name,
//     '虚拟 IP': true,
//     外网访问: true,
//     工作负载: { 资源类型: '部署', 资源名称: fed_deploy_name },
//     端口: [['HTTP', 80, 80]],
//     会话保持: true,
//   },
// ],
// 差异化: [
//   {
//     资源类型: '应用',
//     差异化: [[fed_host_name, '显示名称', 'app_override']],
//   },
// {
//   资源类型: '有状态副本集',
//   资源: fed_sts_name,
//   差异化: [
//     [fed_host_name, '实例数量', '2'],
//     [fed_host_name, '资源限制-CPU', '20m'],
//     [fed_host_name, '资源限制-内存', '20Mi'],
//   ],
// },
// ],
