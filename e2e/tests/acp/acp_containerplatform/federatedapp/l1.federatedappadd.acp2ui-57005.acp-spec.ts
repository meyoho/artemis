import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('用户视图 联邦应用 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_app_name = page.getTestData('57005');
  const fed_sts_name = `${fed_app_name}-sts`;
  const fed_svc_name = `${fed_app_name}-svc`;
  const fed_cm_name = `${fed_app_name}-cm`;
  const fed_secret_name = `${fed_app_name}-secret`;
  const image = ServerConf.TESTIMAGE;
  const app_page = new Application();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name,
    );
  });
  it('ACP2UI-57005|ACP2UI-57000: 创建无deployment+statefulset必填项+svc(headless+当前statefulset)+cm(多组)+secret(tls)+差异化(st+cpu+mem+数量）', () => {
    const data = {
      名称: fed_app_name,
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
        },
      ],
      内部路由: [
        {
          名称: fed_svc_name,
          '虚拟 IP': false,
          工作负载: { 资源类型: '有状态副本集', 资源名称: fed_sts_name },
          端口: [['TCP', 80, 80]],
          会话保持: false,
        },
      ],
      配置字典: [
        {
          名称: fed_cm_name,
          配置项: [
            { 键: 'key1', 值: 'value1' },
            { 键: 'key2', 值: 'value2' },
            { 键: 'key3', 值: 'value3' },
          ],
        },
      ],
      保密字典: [
        {
          名称: fed_secret_name,
          类型: 'TLS',
          证书: `-----BEGIN CERTIFICATE-----
      -----END CERTIFICATE-----`,
          私钥: `-----BEGIN RSA PRIVATE KEY-----
      -----END RSA PRIVATE KEY-----`,
        },
      ],
      差异化: [
        {
          资源类型: '有状态副本集',
          资源: fed_sts_name,
          差异化: [
            [fed_host_name, '实例数量', '0'],
            [fed_host_name, '资源限制-CPU', '20m'],
            [fed_host_name, '资源限制-内存', '20Mi'],
          ],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        面包屑: `联邦应用/${fed_app_name}`,
        名称: fed_app_name,
        配置: [fed_cm_name, '', fed_secret_name, 'TLS'],
        工作负载: [
          '有状态副本集',
          fed_sts_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        内部路由: [
          fed_svc_name,
          `有状态副本集: ${fed_sts_name}`,
          '服务端口',
          '容器端口',
          '80',
        ],
        差异化: [
          fed_host_name,
          '实例数量',
          '0',
          '资源限制-CPU',
          '20m',
          '资源限制-内存',
          '20Mi',
        ],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'application',
        fed_app_name,
        namespace_name,
        true,
      );
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        应用状态: {
          app名称: fed_app_name,
          状态: '已停止',
        },
        StatefulSet: {
          名称: fed_sts_name,
          实例数量: {
            pod名称: fed_sts_name,
            pod数量: '0',
          },
          资源限制: 'CPU: 20m内存: 20Mi',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
    });
  });
});
