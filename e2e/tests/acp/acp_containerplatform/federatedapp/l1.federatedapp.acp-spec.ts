import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';

describe('用户视图 联邦应用 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_app_name = page.getTestData('57002');
  const fed_deploy_name = `${fed_app_name}-deploy`;
  const fed_svc_name = `${fed_app_name}-svc`;
  const fed_cm_name = `${fed_app_name}-cm`;
  const fed_secret_name = `${fed_app_name}-secret`;
  const image = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name,
    );
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name,
    );
  });
  it('ACP2UI-57002 : 创建deployment仅必填项+无statefultset+svc(clusterip+当前deploy)+cm(无配置项)+secret(opaque)+无差异化,验证详情页的资源区显示', () => {
    const data = {
      名称: fed_app_name,
      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy',
        },
      ],
      内部路由: [
        {
          名称: fed_svc_name,
          '虚拟 IP': true,
          外网访问: true,
          工作负载: { 资源类型: '部署', 资源名称: fed_deploy_name },
          端口: [['HTTP', 80, 80]],
          会话保持: true,
        },
      ],
      配置字典: [
        {
          名称: fed_cm_name,
          配置项: [],
        },
      ],
      保密字典: [
        {
          名称: fed_secret_name,
          类型: 'Opaque',
          数据: [{ 键: 'key1', 值: 'value1' }],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name,
        配置: [fed_cm_name, '', fed_secret_name, 'Opaque'],
        工作负载: [
          '部署',
          fed_deploy_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        内部路由: [
          fed_svc_name,
          `部署: ${fed_deploy_name}`,
          '服务端口',
          '容器端口',
          '80',
        ],
      };
      page.detailVerify.verify(verifyData);
    });
  });
  it('ACP2UI-57030 : 详情页更新-删除资源+添加差异化（验证旗下的资源能正常删除）', () => {
    const data = {
      删除资源: [
        { 资源类型: '内部路由', 资源名称: fed_svc_name },
        { 资源类型: '配置', 资源名称: fed_cm_name },
      ],
      差异化: [
        {
          资源类型: '应用',
          差异化: [[fed_host_name, '显示名称', 'app_override']],
        },
      ],
    };
    page.detailPage.update(fed_app_name, data);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'service',
        fed_svc_name,
        namespace_name,
        false,
      );
      browser.refresh();
      const verifyData = {
        名称: fed_app_name,
        配置: [fed_secret_name, 'Opaque'],
        工作负载: [
          '部署',
          fed_deploy_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        差异化: ['应用', fed_host_name, '显示名称', 'app_override'],
      };
      page.detailVerify.verify(verifyData);
    });
  });
  it('ACP2UI-57027 : 详情页更新-更新应用显示名称+添加资源+更新差异化（验证旗下的资源能正常创建、更新）', () => {
    const data = {
      内部路由: [
        {
          名称: fed_svc_name,
          '虚拟 IP': true,
          外网访问: true,
          工作负载: { 资源类型: '部署', 资源名称: fed_deploy_name },
          端口: [['HTTP', 81, 81]],
          会话保持: true,
        },
      ],
      编辑差异化: [
        {
          资源类型: '应用',
          资源名称: '-',
          数据: {
            差异化: [[fed_host_name, '显示名称', 'app_override_update']],
          },
        },
      ],
    };
    page.detailPage.update(fed_app_name, data);
    browser.sleep(1).then(() => {
      const verifyData = {
        内部路由: [
          fed_svc_name,
          `部署: ${fed_deploy_name}`,
          '服务端口',
          '容器端口',
          '81',
        ],
        差异化: ['应用', fed_host_name, '显示名称', 'app_override_update'],
      };
      page.detailVerify.verify(verifyData);
    });
  });
  xit('ACP2UI-57028|ACP2UI-56998 : 详情页更新-更新资源+删除差异化（验证旗下的资源能正常创建、更新）', () => {
    const data = {
      编辑资源: [
        {
          资源类型: '部署',
          资源名称: fed_deploy_name,
          数据: {
            资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
          },
        },
      ],
      删除差异化: [{ 资源类型: '应用', 资源名称: '-' }],
    };
    page.detailPage.update(fed_app_name, data);
    browser.sleep(1).then(() => {
      const verifyData = {
        工作负载: [
          '部署',
          fed_deploy_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 20m; 内存: 20Mi',
        ],
      };
      page.detailVerify.verify(verifyData);
    });
  });
  it('ACP2UI-56999 : 点击左导航联邦应用，点击联邦应用名称进入详情，点击操作-删除，点击确认，返回到列表页，按名称搜索不存在', () => {
    page.detailPage.delete(fed_app_name);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'federatedapplication',
        fed_app_name,
        namespace_name,
        false,
      );
      // http://jira.alauda.cn/browse/ACP-1948
      // 验证搜索正确
      // page.listPage.search(fed_app_name, 0);
      // const expectData = { 数量: 0 };
      // page.listVerify.verify(expectData);
    });
  });
});
