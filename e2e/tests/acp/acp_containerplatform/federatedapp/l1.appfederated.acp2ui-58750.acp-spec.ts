import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';

describe('用户视图 应用联邦化 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const image = ServerConf.TESTIMAGE;
  const app_page = new Application();
  const app_name = app_page.getTestData('58750');
  const container_name = app_page.detailAppPage.getImageName(image);

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
    page.preparePage.deleteFederatedResource(
      'ingress',
      namespace_name,
      app_name,
    );
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      app_name,
    );
    app_page.prepare.deleteApp(app_name, namespace_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      app_name,
    );
    page.preparePage.deleteFederatedResource(
      'ingress',
      namespace_name,
      app_name,
    );
  });
  it('ACP2UI-58750 : 子集群应用联邦化，创建应用包含非五种资源，点击应用联邦化，提示资源列表为空，生成联邦应用，主集群详情页的资源为空；其他集群的资源未创建；普通应用有联邦化的标记不能再联邦化', () => {
    const create_data = {
      选择镜像: {
        方式: '输入',
        镜像地址: image,
      },
      应用: {
        名称: app_name,
        显示名称: app_name,
      },
      计算组件: {
        部署模式: 'Deployment',
      },
      内部路由: {
        名称: app_name,
        端口: [['TCP', '81', '81']],
      },
      访问规则: {
        名称: app_name,
        规则: [['/c', app_name, '81']],
      },
    };
    app_page.createAppPage.create(create_data, namespace_name);
    app_page.detailAppPage.federatedApp();
    browser.sleep(1).then(() => {
      // 生成联邦应用，主集群详情页的内容正确
      page.preparePage.waitResourceExist(
        'federatedapplication',
        app_name,
        namespace_name,
      );
      page.clickLeftNavByText('联邦应用');
      page.listPage.enterDetail(app_name);
      const verifyData = {
        面包屑: `联邦应用/${app_name}`,
        名称: app_name,
        工作负载: [
          '部署',
          app_name,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        内部路由: [
          app_name,
          `部署: ${app_name}-${container_name}`,
          '服务端口',
          '容器端口',
          '81',
        ],
      };
      page.detailVerify.verify(verifyData);

      page.detailPage.enterAppDetail(fed_host_name);
      const v_data = {
        标题: `应用/${app_name}`,
        名称: app_name,
        显示名称: app_name,
        应用状态: {
          app名称: app_name,
          状态: '运行中',
        },
        联邦化标记: true,
        按钮不可点击: ['开始', '停止', '操作'],
        Deployment: {
          名称: app_name,
          状态: {
            pod名称: `${app_name}-${container_name}`,
            pod状态: '运行中',
          },
        },
        其他资源: [[app_name, 'Service']],
        其他资源不存在: [[app_name, 'Ingress']],
      };
      app_page.appDetailVerifyPage.verify(v_data);
    });
  });
});
