import { FederatedAppPage } from '@e2e/page_objects/acp/federatedapp/federatedapp.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { StatefulSetPage } from '@e2e/page_objects/acp/statefulset/statefulset.page';

describe('用户视图 联邦应用 L1自动化', () => {
  const page = new FederatedAppPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-ns';
  const detail = page.preparePage.fedRegionDetail();
  const fed_host_name = detail.Host;
  const fed_app_name1 = page.getTestData('57009');
  const fed_deploy_name1 = `${fed_app_name1}-deploy`;
  const fed_sts_name1 = `${fed_app_name1}-sts`;
  const fed_secret_name1 = `${fed_app_name1}-secret1`;
  const fed_secret_name2 = `${fed_app_name1}-secret2`;
  const fed_cm_name1 = `${fed_app_name1}-cm1`;
  const fed_cm_name2 = `${fed_app_name1}-cm2`;
  const fed_app_name2 = page.getTestData('57008');
  const fed_deploy_name2 = `${fed_app_name2}-deploy`;
  const fed_sts_name2 = `${fed_app_name2}-sts`;
  const image = ServerConf.TESTIMAGE;
  const app_page = new Application();
  const deploy_page = new DeploymentPage();
  const sts_page = new StatefulSetPage();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.createFederatedNs(namespace_name, project_name);
    page.login();
    page.enterUserView(namespace_name, project_name, fed_host_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦应用');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name2,
    );
    page.preparePage.deleteFederatedResource(
      'federatedapplication',
      namespace_name,
      fed_app_name1,
    );
  });
  it('ACP2UI-57009|ACP2UI-57013 : 创建deployment(引用当前cm/secret)+statefulset(引用当前cm/secret)+无svc+多个cm+secret(镜像服务)+差异化（应用的显示名称）', () => {
    const data = {
      名称: fed_app_name1,
      配置字典: [
        {
          名称: fed_cm_name1,
          配置项: [{ 键: 'key1', 值: 'value1' }],
        },
        {
          名称: fed_cm_name2,
          配置项: [{ 键: 'key2', 值: 'value2' }],
        },
      ],
      保密字典: [
        {
          名称: fed_secret_name1,
          类型: '用户名/密码',
          用户名: `example@alauda.io`,
          密码: `&&$$.*12L`,
        },
        {
          名称: fed_secret_name2,
          类型: '镜像服务',
          地址: 'index.alauda.cn',
          用户名: `claas`,
          密码: `YWxhdWRhCg`,
          邮箱: 'example@alauda.io',
        },
      ],
      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy',
          存储卷: [
            {
              名称: 'deploy-volume-cm',
              类型: '配置字典',
              配置字典: fed_cm_name1,
            },
            {
              名称: 'deploy-volume-secret',
              类型: '保密字典',
              保密字典: fed_secret_name1,
            },
          ],
          环境变量添加引用: [
            ['envfromcm', ['配置字典', fed_cm_name1], 'key1'],
            ['envfromsecret', ['保密字典', fed_secret_name1], 'username'],
          ],
          配置引用: [
            ['配置字典', fed_cm_name1],
            ['保密字典', fed_secret_name1],
          ],
          存储卷挂载: [
            [['配置字典', 'deploy-volume-cm'], 'key1', '/test/cmsub'],
            [['配置字典', 'deploy-volume-cm'], '', '/test/cm', true],
            [
              ['保密字典', 'deploy-volume-secret'],
              'username',
              '/test/secretsub',
            ],
            [['保密字典', 'deploy-volume-secret'], '', '/test/secret'],
          ],
          凭据: [fed_secret_name2],
        },
      ],
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
          存储卷: [
            {
              名称: 'deploy-volume-cm',
              类型: '配置字典',
              配置字典: fed_cm_name1,
            },
            {
              名称: 'deploy-volume-secret',
              类型: '保密字典',
              保密字典: fed_secret_name1,
            },
          ],
          环境变量添加引用: [
            ['envfromcm', ['配置字典', fed_cm_name1], 'key1'],
            ['envfromsecret', ['保密字典', fed_secret_name1], 'username'],
          ],
          配置引用: [
            ['配置字典', fed_cm_name1],
            ['保密字典', fed_secret_name1],
          ],
          存储卷挂载: [
            [['配置字典', 'deploy-volume-cm'], 'key1', '/test/cmsub'],
            [['配置字典', 'deploy-volume-cm'], '', '/test/cm', true],
            [
              ['保密字典', 'deploy-volume-secret'],
              'username',
              '/test/secretsub',
            ],
            [['保密字典', 'deploy-volume-secret'], '', '/test/secret'],
          ],
          凭据: [fed_secret_name2],
        },
      ],
      差异化: [
        {
          资源类型: '应用',
          差异化: [[fed_host_name, '显示名称', 'app_override']],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name1,
        工作负载: [
          '部署',
          '有状态副本集',
          fed_deploy_name1,
          fed_sts_name1,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        配置: [
          fed_secret_name1,
          '用户名/密码',
          fed_secret_name2,
          '镜像服务',
          fed_cm_name1,
          fed_cm_name2,
        ],
        差异化: ['应用', fed_host_name, '显示名称', 'app_override'],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'application',
        fed_app_name1,
        namespace_name,
        true,
      );
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        显示名称: 'app_override',
        应用状态: {
          app名称: fed_app_name1,
          状态: '运行中',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
      page.clickLeftNavByText('部署');
      deploy_page.listPage.enterDetail(fed_deploy_name1);
      const expectDetailData = {
        状态: '运行中',
        实例数: '1',
        凭据: fed_secret_name2,
        存储卷: [
          'deploy-volume-cm',
          '配置字典',
          fed_cm_name1,
          'deploy-volume-secret',
          '保密字典',
          fed_secret_name1,
        ],
        资源限制: 'CPU:10m内存:10Mi',
        已挂载存储卷: [
          'deploy-volume-cm',
          'key1',
          '/test/cmsub',
          '/test/cm',
          'deploy-volume-secret',
          'username',
          '/test/secretsub',
          '/test/secret',
        ],
        环境变量: [
          'envfromcm',
          `配置字典 ${fed_cm_name1} : key1`,
          'envfromsecret',
          `保密字典 ${fed_secret_name1} : username`,
        ],
        配置引用: ['配置字典', fed_cm_name1, '保密字典', fed_secret_name1],
      };
      deploy_page.detailPageVerify.verify(expectDetailData);
    });
  });
  it('ACP2UI-57008 : 创建deployment(引用已有cm/secret)+statefulset(引用已有cm/secret)+无svc+cm(多组)+secret(user)+差异化（statefulset）', () => {
    const data = {
      名称: fed_app_name2,
      部署: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'deploy',
          存储卷: [
            {
              名称: 'deploy-volume-cm',
              类型: '配置字典',
              配置字典: fed_cm_name2,
            },
            {
              名称: 'deploy-volume-secret',
              类型: '保密字典',
              保密字典: fed_secret_name2,
            },
          ],
          环境变量添加引用: [
            ['envfromcm', ['配置字典', fed_cm_name2], 'key2'],
            ['envfromsecret', ['保密字典', fed_secret_name1], 'password'],
          ],
          配置引用: [
            ['配置字典', fed_cm_name2],
            ['保密字典', fed_secret_name1],
          ],
          存储卷挂载: [
            [['配置字典', 'deploy-volume-cm'], 'key2', '/test/cmsub'],
            [['配置字典', 'deploy-volume-cm'], '', '/test/cm', true],
            [
              ['保密字典', 'deploy-volume-secret'],
              '.dockerconfigjson',
              '/test/secretsub',
            ],
            [['保密字典', 'deploy-volume-secret'], '', '/test/secret'],
          ],
        },
      ],
      有状态副本集: [
        {
          镜像: { 方式: '输入', 镜像地址: image },
          名称: 'sts',
          存储卷: [
            {
              名称: 'deploy-volume-cm',
              类型: '配置字典',
              配置字典: fed_cm_name2,
            },
            {
              名称: 'deploy-volume-secret',
              类型: '保密字典',
              保密字典: fed_secret_name1,
            },
          ],
          环境变量添加引用: [
            ['envfromcm', ['配置字典', fed_cm_name2], 'key2'],
            ['envfromsecret', ['保密字典', fed_secret_name1], 'username'],
          ],
          配置引用: [
            ['配置字典', fed_cm_name2],
            ['保密字典', fed_secret_name1],
          ],
          存储卷挂载: [
            [['配置字典', 'deploy-volume-cm'], 'key2', '/test/cmsub'],
            [['配置字典', 'deploy-volume-cm'], '', '/test/cm', true],
            [
              ['保密字典', 'deploy-volume-secret'],
              'username',
              '/test/secretsub',
            ],
            [['保密字典', 'deploy-volume-secret'], '', '/test/secret'],
          ],
        },
      ],
      差异化: [
        {
          资源类型: '有状态副本集',
          资源: fed_sts_name2,
          差异化: [[fed_host_name, '资源限制-内存', '20Mi']],
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      const verifyData = {
        名称: fed_app_name2,
        工作负载: [
          '部署',
          '有状态副本集',
          fed_deploy_name2,
          fed_sts_name2,
          1,
          '镜像',
          image,
          '容器大小',
          'CPU: 10m; 内存: 10Mi',
        ],
        差异化: [fed_host_name, '资源限制-内存', '20Mi'],
      };
      page.detailVerify.verify(verifyData);
      page.preparePage.waitResourceExist(
        'application',
        fed_app_name2,
        namespace_name,
        true,
      );
      page.detailPage.enterAppDetail(fed_host_name);
      const v_data_1 = {
        显示名称: '-',
        应用状态: {
          app名称: fed_app_name2,
          状态: '运行中',
        },
      };
      app_page.appDetailVerifyPage.verify(v_data_1);
      sts_page.listPage.enterDetail(fed_sts_name2);
      const expectDetailData = {
        状态: '运行中',
        实例数: '1',
        存储卷: [
          'deploy-volume-cm',
          '配置字典',
          fed_cm_name2,
          'deploy-volume-secret',
          '保密字典',
          fed_secret_name1,
        ],
        资源限制: 'CPU:10m内存:20Mi',
        已挂载存储卷: [
          'deploy-volume-cm',
          'key2',
          '/test/cmsub',
          '/test/cm',
          'deploy-volume-secret',
          'username',
          '/test/secretsub',
          '/test/secret',
        ],
        环境变量: [
          'envfromcm',
          `配置字典 ${fed_cm_name2} : key2`,
          'envfromsecret',
          `保密字典 ${fed_secret_name1} : username`,
        ],
        配置引用: ['配置字典', fed_cm_name2, '保密字典', fed_secret_name1],
      };
      sts_page.detailPageVerify.verify(expectDetailData);
    });
  });
});
