import { PSPPage } from '@e2e/page_objects/acp/psp/psp.page';

describe('用户视图 容器组 L0自动化', () => {
  const page = new PSPPage();
  beforeAll(() => {
    page.preparePage.deletePSP();
    page.login();
    page.enterOperationView().then(() => {
      page.switchCluster_onAdminView(page.clusterName);
      page.detailPage.waitRegionSwitch(page.clusterName);
    });
  });
  beforeEach(() => {
    page.clickLeftNavByText('Pod 安全策略');
  });
  afterAll(() => {
    page.preparePage.deletePSP();
  });
  it('ACP2UI-60669 : 管理视图->点击pod安全策略->点击立即配置->点击配置按钮->保存成功，安全策略为默认安全策略', () => {
    const create_data = {};
    page.editPage.create(create_data).then(() => {
      const v_data = {
        基本策略: {
          '以特权模式运行（privileged）': true,
          '提升特权（allowPrivilegeEscalation）': true,
          默认允许: true,
          默认禁止: false,
          '主机 PID（hostPID）': true,
          '主机 IPC（hostIPC）': true,
          '主机网络（hostNetwork）': true,
          '只读文件系统（readOnlyRootFilesystem）': false,
        },
        '主机路径策略（allowedHostPaths）': {
          范围: '所有主机路径',
        },
        '主机端口策略（hostPorts）': {
          范围: '所有主机端口',
        },
        '用户运行策略（runAsUser）': {
          范围: '所有用户',
        },
      };
      page.detailVerifyPage.verify(v_data);
    });
  });
  it('ACP2UI-60687 : pod安全策略详情页->点击删除->查看确认信息,确认删除<集群名称>集群下的Pod安全策略吗?', () => {
    const v_data = {
      删除提示: `确定删除“${page.clusterName}”集群下的 Pod 安全策略吗？`,
    };
    page.detailVerifyPage.verify(v_data);
  });
  it('ACP2UI-60686 : pod安全策略详情页->点击删除->点击确认->成功删除', () => {
    page.detailPage.deletePSP().then(() => {
      const v_data = {
        帮助信息:
          'Pod 安全策略（Pod Security Policy）是集群级别的资源，它能够控制 Pod 安全敏感方面的运行行为。 Pod Security Policy 对象定义了一组条件和默认值，指示 Pod 必须按系统所能接受的方式运行。若需启用请点击下方按钮。',
      };
      page.detailVerifyPage.verify(v_data);
    });
  });
});
