import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';

describe('模板仓库UI自动化测试 L1', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('58352');
  const repo = chart_repo.createPage.analysisRepo(
    ServerConf.GIT_CHART_REPO_URI,
  );
  const repo_uri = repo.uri;
  const user = repo.user;
  const password = repo.password;
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.delete_chart_repo(repo_name);
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.delete_chart_repo(repo_name);
  });
  it('ACP2UI-58352 : l1 添加模板仓库--选择类型-Git，输入名称、描述、仓库地址-选择http、目录、分配项目、用户名、密码，点击确定按钮页面跳转到详情页', () => {
    const data = {
      名称: repo_name,
      描述: repo_name.toLocaleUpperCase(),
      类型: 'Git',
      代码仓库地址: repo_uri,
      目录: '/',
      分配项目: chart_repo.projectName,
      用户名: user,
      密码: password,
    };
    chart_repo.createPage.create(data);
    const v_data = {
      名称: repo_name,
      详情信息: {
        类型: 'Git',
        描述: repo_name.toLocaleUpperCase(),
        分配项目: chart_repo.projectName,
        地址: repo_uri,
        目录: '/',
      },
    };
    chart_repo.detailPageVerify.verify(v_data);
  });
});
