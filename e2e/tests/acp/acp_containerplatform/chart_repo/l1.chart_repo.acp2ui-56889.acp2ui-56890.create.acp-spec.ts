import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';
import { browser } from 'protractor';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

describe('模板仓库UI自动化测试 L1', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('56889');
  const repo = chart_repo.createPage.analysisRepo(
    ServerConf.HELM_CHART_REPO_URI,
  );
  const user = repo.user;
  const password = repo.password;
  const repo_uri = ServerConf.HELM_CHART_REPO_URI;
  const ns_name = ServerConf.GLOBAL_NAMESPCE;
  const data = {
    name: repo_name,
    ns_name: ns_name,
    uri: repo_uri,
  };
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.prepare.createChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    browser.sleep(5000);
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
  });
  it('ACP2UI-56890 : 把一个没有用户名密码的仓库更新为有用户名密码的仓库，检查secret是否创建', () => {
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name);
    // chart_repo.listPage.chartRepoTable.clickResourceNameByRow([repo_name]);
    const u_data = {
      描述: `U${repo_name.toLocaleUpperCase()}`,
      // 仓库地址: repo_uri_1,
      分配项目: chart_repo.projectName,
      用户名: user,
      密码: password,
    };
    chart_repo.detailPage.update(u_data, repo_name);
    const v_data = {
      名称: repo_name,
      详情信息: {
        类型: 'Chart',
        仓库地址: repo_uri,
        描述: `U${repo_name.toLocaleUpperCase()}`,
        分配项目: chart_repo.projectName,
      },
    };
    chart_repo.detailPageVerify.verify(v_data);
    browser.sleep(1).then(() => {
      const repr = new RegExp('^(E|e)rror.?');
      const v_data_2 = CommonKubectl.execKubectlCommand(
        `kubectl get secret -n ${ServerConf.GLOBAL_NAMESPCE} ${repo_name} -o yaml`,
      );
      console.log(v_data_2);
      expect(repr.test(v_data_2)).toBe(false);
    });
  });
  it('ACP2UI-56889 : 把一个有用户名密码的仓库更新为没有用户名密码的仓库，检查secret是否删掉', () => {
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name);
    // chart_repo.listPage.chartRepoTable.clickResourceNameByRow([repo_name]);
    const u_data_1 = {
      // 仓库地址: repo_uri,
      用户名: '',
      密码: '',
    };
    chart_repo.detailPage.update(u_data_1, repo_name);
    const v_data = {
      名称: repo_name,
      分配项目: chart_repo.projectName,
      仓库地址: repo_uri,
      描述: `U${repo_name.toLocaleUpperCase()}`,
    };
    chart_repo.detailPageVerify.verify(v_data);
    browser.sleep(5000).then(() => {
      const repr = new RegExp('^(E|e)rror.?');
      const v_data_2 = CommonKubectl.execKubectlCommand(
        `kubectl get secret -n ${ServerConf.GLOBAL_NAMESPCE} ${repo_name} -o yaml`,
      );
      console.log(v_data_2);
      expect(repr.test(v_data_2)).toBe(true);
    });
  });
});
