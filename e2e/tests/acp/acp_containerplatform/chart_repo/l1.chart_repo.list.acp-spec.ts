import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';

describe('模板仓库UI自动化测试 L1', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('repol');
  const repo_uri = ServerConf.HELM_CHART_REPO_URI;
  const ns_name = ServerConf.GLOBAL_NAMESPCE;
  const data = {
    name: repo_name,
    ns_name: ns_name,
    uri: repo_uri,
  };
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.prepare.createChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
  });
  it('ACP2UI-54844 : l1 检查模板仓库列表页', () => {
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name);
    const v_data = {
      面包屑: '模板仓库',
      表头: ['名称', '类型', '分配项目', '创建时间', ''],
      数据: { keys: [repo_name], exists: true },
    };
    chart_repo.listPageVerify.verify(v_data);
  });
});
