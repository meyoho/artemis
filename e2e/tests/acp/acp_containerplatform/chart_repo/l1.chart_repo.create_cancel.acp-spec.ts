import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';

describe('模板仓库UI自动化测试 L1', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('repocc');
  const ns_name = ServerConf.GLOBAL_NAMESPCE;
  const repo = chart_repo.createPage.analysisRepo(
    ServerConf.HELM_CHART_REPO_URI,
  );
  const repo_uri = repo.uri;
  const user = repo.user;
  const password = repo.password;
  const data = {
    name: repo_name,
    ns_name: ns_name,
    uri: repo_uri,
    username: user,
    password: password,
  };
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
  });
  it('ACP2UI-54840 : 添加模板仓库，输入参数后，点击取消', () => {
    const data = {
      名称: repo_name,
      描述: repo_name.toLocaleUpperCase(),
      仓库地址: repo_uri,
      分配项目: chart_repo.projectName,
      用户名: user,
      密码: password,
    };
    chart_repo.createPage.create_cancle(data);
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name, 0);
    const v_data = {
      数据: { keys: [repo_name], exists: false },
    };
    chart_repo.listPageVerify.verify(v_data);
  });
});
