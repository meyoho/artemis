import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';

describe('模板仓库UI自动化测试 L1', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('repos');
  const repo_uri = ServerConf.HELM_CHART_REPO_URI;
  const ns_name = ServerConf.GLOBAL_NAMESPCE;
  const data = {
    name: repo_name,
    ns_name: ns_name,
    uri: repo_uri,
  };
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.prepare.createChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
  });
  it('ACP2UI-54895 : 搜索', () => {
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name, 1);
    const v_data = {
      数据: { keys: [repo_name], exists: true },
    };
    chart_repo.listPageVerify.verify(v_data);
    chart_repo.listPage.chartRepoTable.searchByResourceName(
      repo_name.slice(0, repo_name.length - 1),
      0,
    );
    const v_data_1 = {
      数据: { keys: [repo_name], exists: false },
    };
    chart_repo.listPageVerify.verify(v_data_1);
  });
});
