import { ServerConf } from '@e2e/config/serverConf';
import { ChartRepoPage } from '@e2e/page_objects/acp/chart_repo/chartrepo.page';

describe('模板仓库UI自动化测试 L0', () => {
  const chart_repo = new ChartRepoPage();
  const repo_name = chart_repo.getTestData('repo');
  const repo_uri = ServerConf.HELM_CHART_REPO_URI;
  const ns_name = ServerConf.GLOBAL_NAMESPCE;
  const data = {
    name: repo_name,
    ns_name: ns_name,
    uri: repo_uri,
  };
  if (!chart_repo.prepare.appmarketIsEnabled) {
    return;
  }
  beforeAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
    chart_repo.login();
    chart_repo.enterOperationView();
  });
  beforeEach(() => {
    chart_repo.clickLeftNavByText('模板仓库');
  });
  afterAll(() => {
    chart_repo.prepare.deleteChartRepoByTemplate(
      'alauda.chart_repo.yaml',
      data,
    );
  });
  it('ACP2UI-54837 : 添加模板仓库---只输入正确的必需参数', () => {
    const data = {
      名称: repo_name,
      仓库地址: repo_uri,
    };
    chart_repo.createPage.create(data);
    const v_data = {
      面包屑: `模板仓库/${repo_name}`,
      名称: repo_name,
      类型: 'Chart',
      仓库地址: repo_uri,
    };
    chart_repo.detailPageVerify.verify(v_data);
  });
  it('ACP2UI-55442 : L0 模板仓库列表->点击操作->点击删除->输入名称->点击删除->成功删除', () => {
    chart_repo.listPage.delete(repo_name);
    chart_repo.listPage.chartRepoTable.searchByResourceName(repo_name, 0);
    const v_data = {
      数据: { keys: [repo_name], exists: false },
    };
    chart_repo.listPageVerify.verify(v_data);
  });
});
