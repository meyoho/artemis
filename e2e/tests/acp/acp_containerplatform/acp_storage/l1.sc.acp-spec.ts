import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';
import { CommonMethod } from '@e2e/utility/common.method';
// 存储类新版本有问题，先注释掉
describe('存储类测试', () => {
  const page = new StoragePage();
  const defaultScs = page.prePareData.defaultScs();
  const sc_name = page.getTestData('storage-class');
  const sc_yaml = CommonMethod.readTemplateFile('alauda.storage_class.yaml', {
    sc_name: sc_name,
  });

  beforeAll(() => {
    if (defaultScs.length > 0) {
      defaultScs.forEach(defaultSc => {
        page.prePareData.unsetDefaultClass(defaultSc);
      });
    }
    page.prePareData.deleteStorageClass(sc_name);
    page.login();
    page.enterOperationView();
    page.clickLeftNavByText('存储类');
    page.switchCluster_onAdminView(page.clusterName);
  });
  afterAll(() => {
    page.prePareData.deleteStorageClass(sc_name);
    if (defaultScs.length > 0) {
      defaultScs.forEach(defaultSc => {
        page.prePareData.setAssignDefaultClass(defaultSc);
      });
    }
  });
  beforeEach(() => {});
  afterEach(() => {});
  it('ACP2UI-52686 : L1 存储类列表页->创建存储类->成功创建', () => {
    const testData = {
      集群: page.clusterName,
      YAML: sc_yaml,
    };
    page.sc_create.create(testData);
    // 创建成功后，页面跳转到详情页

    const expectValue = {
      标题: sc_name,
      名称: sc_name,
      // 类型: 'rbd',
    };
    page.sc_detailVerify.verify(expectValue);
  });
  it('ACP2UI-52687 : L1 存储类列表页->查看存储类列表->页面功能正常，数据展示正确', () => {
    page.clickLeftNavByText('存储类');
    const expectValue = {
      header: ['名称', '类型', '项目', '创建时间', ''],
      名称: [sc_name],
      // 类型: 'rbd',
    };
    page.sc_listVerify.verify(expectValue);
  });
  it('ACP2UI-52688 : L1 存储类列表页->查看存储类详情->前端页面内容展示正确，功能正常 ', () => {
    // 单击列表页中名称进入详情页
    page.sc_list.scTable.clickResourceNameByRow([sc_name]);
    // 验证详情页内容展示正确
    const expectValue = {
      标题: sc_name,
      名称: sc_name,
      // 类型: 'rbd',
    };
    page.sc_detailVerify.verify(expectValue);
  });
  it('ACP2UI-52689 : L1 存储类列表页->设置默认存储类->成功设置', () => {
    page.clickLeftNavByText('存储类');
    // 设置存储类为默认类
    page.sc_list.setDefault(sc_name);
    const expectValue = {
      名称: [sc_name],
      默认: '默认',
    };
    page.sc_listVerify.verify(expectValue);
  });
  it('ACP2UI-52690 : L1 存储类列表->取消默认存储类->成功被取消', () => {
    // 取消默认存储类
    const defaultScs_ = page.prePareData.defaultScs();
    if (defaultScs_.includes(sc_name)) {
      page.sc_list.unSetDefault(sc_name);
      const expectValue = {
        名称: [sc_name],
        默认: '非默认',
      };
      page.sc_listVerify.verify(expectValue);
    } else {
      expect(`${sc_name} 是默认存储类`).toBe(`${sc_name} 不是默认存储类`);
    }
  });
  it('ACP2UI-52691 : L1 存储类列表->删除存储类->能成功被删除', () => {
    // 删除存储类
    page.clickLeftNavByText('存储类');
    page.sc_list.delete(sc_name).then(() => {
      const expectValue = {
        删除: sc_name,
      };
      // 删除后验证列表页找不到该存储类
      page.sc_listVerify.verify(expectValue);
    });
  });
});
