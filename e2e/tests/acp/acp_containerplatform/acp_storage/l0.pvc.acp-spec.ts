import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';

describe('PVC L0 case', () => {
  const page = new StoragePage();

  const hostpath_pvc_name = page.getTestData('hostpath-pvc');

  beforeAll(() => {
    page.prePareData.deletePvc(hostpath_pvc_name, page.namespace1Name);
    page.prePareData.deletePv(hostpath_pvc_name);
    page.prePareData.createPv(hostpath_pvc_name);

    page.login();
    page.enterUserView(page.namespace1Name);
  });
  afterAll(() => {
    page.prePareData.deletePvc(hostpath_pvc_name, page.namespace1Name);
    page.prePareData.deletePv(hostpath_pvc_name);
  });
  beforeEach(() => {});
  it('ACP2UI-52678 : 用户视图->创建pvc 不使用存储类->成功创建pvc hostpath', () => {
    const testData = {
      名称: hostpath_pvc_name,
      大小: '1',
      访问模式: '读写',
      高级: 'true',
      标签: [['type', 'hostpath'], ['name', hostpath_pvc_name]],
      注解: [['type', 'hostpath'], ['name', hostpath_pvc_name]],
      选择器: [['type', 'hostpath'], ['name', hostpath_pvc_name]],
    };
    page.pvc_create.create(testData).then(() => {
      const expectVaue = {
        标题: hostpath_pvc_name,
        名称: hostpath_pvc_name,
        大小: '1Gi',
        //状态: '绑定',
        访问模式: '读写',
        容器组: '无关联资源',
      };
      // 创建成功后页面跳转到详情页
      page.pvc_detailVerify.verify(expectVaue);
    });
  });
});
