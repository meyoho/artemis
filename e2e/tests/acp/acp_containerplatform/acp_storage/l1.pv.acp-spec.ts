import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';

describe('持久卷 L1 case', () => {
  const page = new StoragePage();
  const nfs_pv_name = page.getTestData('l1-nfs-pv');

  const host_path = '/tmp/pv';

  beforeAll(() => {
    page.prePareData.deletePv(nfs_pv_name);

    page.login();
    page.enterOperationView();
    page.clickLeftNavByText('持久卷');
    page.switchCluster_onAdminView(page.clusterName);
  });
  afterAll(() => {
    page.prePareData.deletePv(nfs_pv_name);
  });
  beforeEach(() => {});

  it('ACP2UI-52770 : pv列表页->创建nfs-pv->pv能成功被创建', () => {
    const testData = {
      名称: nfs_pv_name,
      大小: '1',
      类型: 'nfs',
      服务地址: '192.168.0.1',
      路径: host_path,
      访问模式: '共享',
      回收策略: '保留',
      高级: 'true',
      标签: [['type', 'hostpath'], ['name', nfs_pv_name]],
      注解: [['type', 'hostpath'], ['name', nfs_pv_name]],
    };
    page.pv_create.create(testData);

    const expectVaue = {
      标题: nfs_pv_name,
      名称: nfs_pv_name,
      大小: '1Gi',
      访问模式: '共享',
      回收策略: '保留',
    };
    // 创建成功后页面跳转到详情页
    page.pv_detailVerify.verify(expectVaue);
  });
  it('ACP2UI-53179 : pv列表页->更新pv->能成功更新pv', () => {
    const testData = {
      大小: '2',
      // 路径: host_path,
      访问模式: '只读',
      回收策略: '删除',
      高级: 'true',
      标签: [['test', 'test'], ['test1', nfs_pv_name]],
      注解: [['test', 'test'], ['test1', nfs_pv_name]],
    };
    page.pv_update.update(nfs_pv_name, testData);

    const expectVaue = {
      标题: nfs_pv_name,
      名称: nfs_pv_name,
      大小: '2Gi',
      访问模式: '只读',
      回收策略: '删除',
    };
    //  更新成功后页面跳转到详情页
    page.pv_detailVerify.verify(expectVaue);
  });
  it('ACP2UI-53182 : pv列表页->查看pv详情->pv详情页能正常展示', () => {
    //从pv列表页进入pv详情页
    page.pv_list.viewDatail(nfs_pv_name);

    const expectVaue = {
      标题: nfs_pv_name,
      名称: nfs_pv_name,
      大小: '2Gi',
      访问模式: '只读',
      回收策略: '删除',
    };
    //  更新成功后页面跳转到详情页
    page.pv_detailVerify.verify(expectVaue);
  });

  it('ACP2UI-53180 : pv列表页->删除pv->pv成功被删除（未绑定pvc）', () => {
    // 不支持模糊搜索，暂时先用精确搜索
    page.pv_list.delete(nfs_pv_name);

    const expectVaue = {
      数量: 0,
    };

    page.pv_list.search(nfs_pv_name, 0);
    page.pv_listVerify.verify(expectVaue);
  });
});
