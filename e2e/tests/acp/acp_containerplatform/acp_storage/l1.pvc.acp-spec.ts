import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';

describe('PVC L1 级别case', () => {
  const page = new StoragePage();

  const nfs_pvc_name = page.getTestData('nfs-pvc');

  const ceph_sc_name = 'cephfs';
  const ceph_pvc_name = page.getTestData('ceph');
  const default_pvc_name = page.getTestData('default');

  beforeAll(() => {
    page.prePareData.deletePvc(nfs_pvc_name, page.namespace1Name);
    page.prePareData.deletePvc(ceph_pvc_name, page.namespace1Name);
    page.prePareData.deletePvc(default_pvc_name, page.namespace1Name);
    page.prePareData.deletePv(nfs_pvc_name);
    page.prePareData.createnfsPv(nfs_pvc_name);
    // page.prePareData.setDefaultClass();
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  afterAll(() => {
    page.prePareData.deletePvc(nfs_pvc_name, page.namespace1Name);
    page.prePareData.deletePvc(ceph_pvc_name, page.namespace1Name);
    page.prePareData.deletePvc(default_pvc_name, page.namespace1Name);
    page.prePareData.deletePv(nfs_pvc_name);
  });
  beforeEach(() => {});

  it('ACP2UI-52678 : L0:用户视图->创建pvc 不使用存储类->成功创建pvc nfs', () => {
    const testData = {
      名称: nfs_pvc_name,
      大小: '1',
      访问模式: '读写',
    };
    page.pvc_create.create(testData);

    const expectVaue = {
      标题: nfs_pvc_name,
      名称: nfs_pvc_name,
      大小: '1Gi',
      //状态: '绑定',
      访问模式: '读写',
      容器组: '无关联资源',
    };
    // 创建成功后页面跳转到详情页
    // page.pvc_list.viewDatail(nfs_pvc_name);
    page.pvc_detailVerify.verify(expectVaue);
  });
  it('ACP2UI-52679 : L1 用户视图->创建pvc-使用存储类->pv能成功创建', () => {
    const testData = {
      名称: ceph_pvc_name,
      大小: '1',
      访问模式: '只读',
      存储类: ceph_sc_name,
    };
    page.pvc_create.create(testData);

    const expectVaue = {
      标题: ceph_pvc_name,
      名称: ceph_pvc_name,
      大小: '1Gi',
      //状态: '绑定',
      访问模式: '只读',
      容器组: '无关联资源',
    };
    // 创建成功后页面跳转到详情页
    // page.pvc_list.viewDatail(ceph_pvc_name);
    page.pvc_detailVerify.verify(expectVaue);
  });
  it('ACP2UI-52680 : L1 用户视图->创建pvc-使用默认存储类->pvc能成功被创建', () => {
    const testData = {
      名称: default_pvc_name,
      大小: '1',
      访问模式: '共享',
      存储类: '使用默认',
    };
    page.pvc_create.create(testData);

    const expectVaue = {
      标题: default_pvc_name,
      名称: default_pvc_name,
      大小: '1Gi',
      //状态: '绑定',
      访问模式: '共享',
      容器组: '无关联资源',
    };
    // 创建成功后页面跳转到详情页
    // page.pvc_list.viewDatail(default_pvc_name);
    page.pvc_detailVerify.verify(expectVaue);
  });
  it('ACP2UI-53185 : L1 用户视图->更新pvc->pvc能成功被更新', () => {
    //从pvc列表页进入pvc详情页

    const testData = {
      高级: 'true',
      标签: [['name', nfs_pvc_name]],
    };
    page.pvc_update.update(nfs_pvc_name, testData);

    const expectVaue = {
      标题: nfs_pvc_name,
      名称: nfs_pvc_name,
      大小: '1Gi',
      //状态: '绑定',
      访问模式: '读写',
      容器组: '无关联资源',
    };
    // 创建成功后页面跳转到详情页
    // page.pvc_list.viewDatail(nfs_pvc_name);
    page.pvc_detailVerify.verify(expectVaue);
  });

  it('ACP2UI-52683 : L1 用户视图->查看pvc列表->页面功能正常', () => {
    page.clickLeftNavByText('存储');

    const expectVaue = {
      header: [
        '持久卷声明名称',
        '状态',
        '关联持久卷',
        '大小',
        '创建时间',
        '操作',
      ],
    };

    page.pvc_listVerify.verify(expectVaue);
  });
  it('ACP2UI-53186 : L1 用户视图->查看pvc详情->pvc详情页内容正确，功能按钮正常', () => {
    //从pv列表页进入pv详情页
    page.pvc_list.viewDatail(nfs_pvc_name);
    page.pvc_detail.clickTab('YAML');
    const expectVaue = {
      yaml: '1Gi',
    };

    page.pvc_detailVerify.verify(expectVaue);
  });

  it('ACP2UI-52684 : L1 用户视图->pvc列表页->搜索pvc->搜索功能正常', () => {
    //从pv列表页进入pv详情页
    page.pvc_list.search(nfs_pvc_name.toLocaleUpperCase());
    const expectVaue = {
      数量: 1,
    };

    page.pvc_listVerify.verify(expectVaue);
  });
  it('ACP2UI-52682 : L1 用户视图->删除pvc-使用存储类->pvc能成功被删除(未被挂载) ', () => {
    // 不支持模糊搜索，暂时先用精确搜索
    // pv_list_page.pvTable.searchByResourceName(nfs_pv_name.slice(0,-5),2);
    page.pvc_list.delete(nfs_pvc_name);

    const expectVaue = {
      数量: 0,
    };

    page.pvc_list.search(nfs_pvc_name, 0);
    page.pvc_listVerify.verify(expectVaue);
  });
});
