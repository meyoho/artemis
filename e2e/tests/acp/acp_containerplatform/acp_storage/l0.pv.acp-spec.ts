import { ServerConf } from '@e2e/config/serverConf';
import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';

describe('持久卷 L0 case', () => {
  const page = new StoragePage();
  const hostpath_pv_name = page.getTestData('hostpath-pv');

  const host_path = '/tmp/pv';

  beforeAll(() => {
    page.prePareData.deletePv(hostpath_pv_name);
    page.login();
    page.enterOperationView();
    page.clickLeftNavByText('持久卷');
    page.switchCluster_onAdminView(ServerConf.REGIONNAME);
  });
  afterAll(() => {
    page.prePareData.deletePv(hostpath_pv_name);
  });
  beforeEach(() => {});
  it('ACP2UI-52771 : pv列表页->创建hostpath-pv->pv能成功被创建', () => {
    const testData = {
      名称: hostpath_pv_name,
      大小: '1',
      路径: host_path,
      高级: 'true',
      标签: [['type', 'hostpath'], ['name', hostpath_pv_name]],
      注解: [['type', 'hostpath'], ['name', hostpath_pv_name]],
    };
    page.pv_create.create(testData);

    const expectVaue = {
      标题: hostpath_pv_name,
      名称: hostpath_pv_name,
      大小: '1Gi',
      访问模式: '读写',
      // 回收策略: '保留'
    };
    // 创建成功后页面跳转到详情页
    page.pv_detailVerify.verify(expectVaue);
  });
});
