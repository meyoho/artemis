import { ServerConf } from '@e2e/config/serverConf';
import { StatefulSetPage } from '@e2e/page_objects/acp/statefulset/statefulset.page';

describe('用户视图 有状态副本集L0自动化', () => {
  const page = new StatefulSetPage();
  const statefulset_name = page.getTestData('statefulset');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(statefulset_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('有状态副本集');
  });
  afterAll(() => {
    page.preparePage.delete(statefulset_name);
  });
  it('ACP2UI-54672 : L0:创建有状态副本集-输入必填项-点击【确定】-创建成功', () => {
    const testData = {
      名称: statefulset_name,
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/有状态副本集/${statefulset_name}`,
      主机选择器: '-',
      更新策略: '滚动更新策略 ( partition: 0 )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54681 : 计算-有状态副本集-列表页字段验证', () => {
    page.listPage.waitStatus(statefulset_name, '运行中');
    const expectCreateData = {
      标题: '计算/有状态副本集',
      header: ['名称', '状态', '所属应用', '创建时间'],
      数量: 1,
      创建: {
        名称: statefulset_name,
        状态: '运行中(1/1)',
        所属应用: '-',
      },
    };
    page.listPageVerify.verify(expectCreateData);
  });
  it('ACP2UI-54203 : 计算-有状态副本集-列表页-操作-点击【删除】-有状态副本集删除成功', () => {
    page.listPage.delete(statefulset_name);
    // 验证搜索正确
    page.listPage.search(statefulset_name, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
