import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name_base = page.getTestData('l1-deploy-base');
  const deployment_name_anti = page.getTestData('l1-deploy-aff1');
  const deployment_name_aff = page.getTestData('l1-deploy-aff2');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  beforeAll(() => {
    page.preparePage.delete(deployment_name_base);
    page.preparePage.delete(deployment_name_anti);
    page.preparePage.delete(deployment_name_aff);

    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name_base);
    page.preparePage.delete(deployment_name_anti);
    page.preparePage.delete(deployment_name_aff);
  });
  it('ACP2UI-53912 : 创建部署-输入必填项-（基本信息）显示名称-增加实例数量为2-（部署高级）更新策略-组件标签（容器高级）启动命令和参数-无主机端口-（容器组高级）容器组标签-主机选择器-打开host-点击确定-创建成功', () => {
    const testData = {
      名称: deployment_name_base,
      显示名称: deployment_name_base,
      实例数量: 2,
      更新策略: ['20%', '20%'],
      组件标签: [['deploylabel', 'deploylabel']],
      资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
      启动命令: [['/bin/sh']],
      参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
      环境变量: [['env', 'env']],
      端口: [['TCP', '80', '']],
      容器组标签: [['podlabel', 'podlabel']],
      主机选择器: ['beta.kubernetes.io/os:linux'],
      'Host 模式': 'true',
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '2',
      显示名称: deployment_name_base,
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53886 : 计算-部署-列表页-点击名称-进入部署详情页', () => {
    page.listPage.enterDetail(deployment_name_base);
    const expectDetailData = {
      显示名称: deployment_name_base,
      // 容器组标签: 'podlabel:podlabel',
      主机选择器: 'beta.kubernetes.io/os:linux',
      更新策略: '滚动更新策略 ( 最大可超出数: 20%, 最多不可用数: 20% )',
      镜像: imageAddress,
      资源限制: 'CPU:20m内存:20Mi',
      启动命令: ['/bin/sh'],
      参数: ['-c', 'while true; do sleep 10; echo 123; done'],
      端口: ['TCP', '80'],
      环境变量: ['env'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-53914 : 创建部署-输入必填项-（容器高级）-健康检查-（容器组高级）添加反亲和-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name_anti,
      亲和性: [
        {
          亲和性: 'Pod 反亲和',
          方式: '基本',
          亲和组件: ['部署', deployment_name_base],
        },
      ],
      添加存活性健康检查: {
        协议类型: 'HTTP',
        启动时间: 10,
        间隔: 10,
      },
      添加可用性健康检查: {
        协议类型: 'TCP',
        启动时间: 10,
        间隔: 10,
        正常阈值: 1,
      },
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      'Pod 亲和': [
        'Pod 反亲和',
        'Required',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: deployment-${deployment_name_base}`,
      ],
      存活性健康检查: {
        健康检查类型: '存活性',
        协议类型: 'HTTP',
        启动时间: 10,
        间隔: 10,
        超时时长: 30,
        正常阈值: 1,
        不正常阈值: 5,
        协议: 'HTTP',
        端口: 80,
        路径: '/',
        请求头: '无请求头',
      },
      可用性健康检查: {
        健康检查类型: '可用性',
        协议类型: 'TCP',
        启动时间: 10,
        间隔: 10,
        超时时长: 30,
        正常阈值: 1,
        不正常阈值: 5,
        端口: 80,
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-53917 : 创建部署-输入必填项-（容器高级）-日志文件-（容器组高级）添加亲和性-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name_aff,
      资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
      亲和性: [
        {
          亲和性: 'Pod 亲和',
          方式: '高级',
          亲和类型: 'Preferred',
          权重: 3,
          匹配标签: [
            [
              `service.${ServerConf.LABELBASEDOMAIN}/name`,
              `deployment-${deployment_name_base}`,
            ],
          ],
        },
      ],
      日志文件: [['/var/*.*']],
      排除日志文件: [['/var/hehe.txt']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53958 : 详情信息-亲和性，日志文件', () => {
    page.listPage.enterDetail(deployment_name_aff);
    const expectDetailData = {
      'Pod 亲和': [
        'Pod 亲和',
        'Preferred',
        '3',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: deployment-${deployment_name_base}`,
      ],
      日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53988 : 详情-日志-容器组/容器信息显示正确', () => {
    page.listPage.enterDetail(deployment_name_base);
    const expectDetailData = {
      日志: '123',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
