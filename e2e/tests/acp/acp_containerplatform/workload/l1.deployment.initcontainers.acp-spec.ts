import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';

describe('用户视图 部署初始化容器L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('init');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const cm_page = new ConfigPage();
  const secret_page = new SecretPage();
  const third_name = page.getTestData('l1-init');

  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
    const configmap_data = {
      name: third_name,
      namespace: namespace_name,
      datas: { 'l1-deploy-cm': 'l1-deploy-configmap' },
    };
    cm_page.prepareData.create(configmap_data);
    const secret_data = {
      name: third_name,
      namespace: namespace_name,
      datatype: 'Opaque',
      datas: {
        'l1-deploy-sc': 'l1-deploy-secret',
      },
    };
    secret_page.prepareData.create(secret_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
  });
  it('ACP2UI-58739 : 创建部署-输入必填项-添加初始化容器-点击【确定】-创建成功，验证详情页的容器显示', () => {
    const testData = {
      名称: deployment_name,
      容器名称: 'container1',
      存储卷: [
        {
          名称: 'deploy-volume-empty',
          类型: '空目录',
        },
      ],
      存储卷挂载: [[['空目录', 'deploy-volume-empty'], '', '/tmp']],
      添加初始化容器: [
        {
          镜像: imageAddress,
          容器名称: 'init-container2',
          资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
          启动命令: [['/bin/sh']],
          参数: [['-c'], ['env;env>/tmp/container2.env']],
          环境变量添加引用: [
            ['envfromcm', ['配置字典', third_name], 'l1-deploy-cm'],
            ['envfromsecret', ['保密字典', third_name], 'l1-deploy-sc'],
          ],
          配置引用: [['配置字典', third_name], ['保密字典', third_name]],
          端口: [['TCP', '80', '']],
          日志文件: [['/var/*.*']],
          排除日志文件: [['/var/hehe.txt']],
        },
        {
          镜像: imageAddress,
          容器名称: 'init-container3',
          启动命令: [['/bin/sh']],
          参数: [['-c'], ['env;env>/tmp/container3.env']],
          环境变量: [['env', 'env']],
          存储卷挂载: [[['空目录', 'deploy-volume-empty'], '', '/tmp']],
        },
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      存储卷: ['deploy-volume-empty', '空目录', '-'],
      容器: [
        {
          容器名称: 'container1',
          镜像: imageAddress,
          资源限制: 'CPU:10m内存:10Mi',
          启动命令: ['-'],
          参数: ['-'],
          已挂载存储卷: ['deploy-volume-empty', '/tmp'],
        },
        {
          容器名称: 'init-container2',
          镜像: imageAddress,
          资源限制: 'CPU:20m内存:20Mi',
          启动命令: ['/bin/sh'],
          参数: ['-c', 'env;env>/tmp/container2.env'],
          端口: ['TCP', '80'],
          日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
        },
        {
          容器名称: 'init-container3',
          启动命令: ['/bin/sh'],
          参数: ['-c', 'env;env>/tmp/container3.env'],
          已挂载存储卷: ['deploy-volume-empty', '/tmp'],
        },
      ],
      验证容器日志: [
        {
          容器: 'init-container2',
          日志: [
            'envfromcm=l1-deploy-configmap',
            'envfromsecret=l1-deploy-secret',
            'l1-deploy-cm=l1-deploy-configmap',
            'l1-deploy-sc=l1-deploy-secret',
          ],
        },
        { 容器: 'init-container3', 日志: ['env=env'] },
        { 容器: 'container1', 日志: ['/var/hehe.txt'] },
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
