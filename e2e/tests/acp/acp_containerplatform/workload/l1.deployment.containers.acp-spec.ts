import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署添加容器L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('container');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name);
  });
  it('ACP2UI-57661 : 创建部署-输入必填项-添加容器-点击【确定】-创建成功，验证详情页的容器显示', () => {
    const testData = {
      名称: deployment_name,
      容器名称: 'container1',
      存储卷: [
        {
          名称: 'deploy-volume-empty',
          类型: '空目录',
        },
      ],
      添加容器: [
        {
          镜像: imageAddress,
          容器名称: 'container2',
          资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
          启动命令: [['/bin/sh']],
          参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
          环境变量: [['env', 'env']],
          端口: [['TCP', '80', '']],
          日志文件: [['/var/*.*']],
          排除日志文件: [['/var/hehe.txt']],
        },
        {
          镜像: imageAddress,
          容器名称: 'container3',
          添加存活性健康检查: {
            协议类型: 'HTTP',
            启动时间: 10,
            间隔: 10,
          },
          添加可用性健康检查: {
            协议类型: 'TCP',
            启动时间: 10,
            间隔: 10,
            正常阈值: 1,
          },
          存储卷挂载: [[['空目录', 'deploy-volume-empty'], '', '/test/empty']],
        },
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      存储卷: ['deploy-volume-empty', '空目录', '-'],
      容器: [
        {
          容器名称: 'container1',
          镜像: imageAddress,
          资源限制: 'CPU:10m内存:10Mi',
          启动命令: ['-'],
          参数: ['-'],
        },
        {
          容器名称: 'container2',
          镜像: imageAddress,
          资源限制: 'CPU:20m内存:20Mi',
          启动命令: ['/bin/sh'],
          参数: ['-c', 'while true; do sleep 10; echo 123; done'],
          端口: ['TCP', '80'],
          日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
        },
        {
          容器名称: 'container3',
          已挂载存储卷: ['deploy-volume-empty', '/test/empty'],
          存活性健康检查: {
            健康检查类型: '存活性',
            协议类型: 'HTTP',
            启动时间: 10,
            间隔: 10,
            超时时长: 30,
            正常阈值: 1,
            不正常阈值: 5,
            协议: 'HTTP',
            端口: 80,
            路径: '/',
            请求头: '无请求头',
          },
          可用性健康检查: {
            健康检查类型: '可用性',
            协议类型: 'TCP',
            启动时间: 10,
            间隔: 10,
            超时时长: 30,
            正常阈值: 1,
            不正常阈值: 5,
            端口: 80,
          },
        },
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
