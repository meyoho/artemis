import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署定时扩缩容L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('cronhpa');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  if (!cronhpa_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.preparePage.deleteCronHpa(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
    const testData = {
      名称: deployment_name,
    };
    page.createPage.create(testData, imageAddress);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteCronHpa(deployment_name);
    page.preparePage.delete(deployment_name);
  });
  it('ACP2UI-55786 : 部署详情页->点击更新->点击自动扩缩容编辑弹窗的更新按钮->更新成功，页面提示无自动扩缩容', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55808 : 部署详情页->查看不调节hpa详情->显示无自动扩缩容', () => {
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55788 : 部署详情页->点击更新->选择定时调节->规则默认勾选每天时间0:00->输入目标实例数3->点击更新->能正常保存', () => {
    const testData = {
      定时调节: [['时间', ['每天', ''], 3]],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天00:00触发3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55802 : 部署详情页->查看定时调节hpa详情->页面展示触发规则和目标实例数，名称和deployment同名', () => {
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天00:00触发3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55790 : 部署详情页->点击更新->选择定时调节->类型选择自定义->输入触发规则1 12 * * * ->目标实例数输入2->点击更新->能正常保存', () => {
    const testData = {
      定时调节: [['自定义', '1 12 * * *', 2]],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天12:01触发2',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55791 : 部署详情页->点击更新->选择定时调节->输入目标实例数3->点击取消->没有保存，没有后端网络请求', () => {
    const testData = {
      定时调节: [['自定义', '10 12 * * *', 3]],
    };
    page.detailPage.updateHpa(deployment_name, testData, 'cancel');
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数每天12:01触发2',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55792 : 部署详情页->点击更新->选择定时调节->输入目标实例数3->添加一条调节规则->点击更新->能正常保存', () => {
    const testData = {
      定时调节: [
        ['自定义', '30 18 * * 3,6', 3],
        ['时间', ['星期一,星期二', '20:50'], 0],
      ],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容:
        '触发规则目标实例数星期三、星期六18:30触发3星期一、星期二20:50触发0',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55804 : 部署详情页->点击更新->选择定时调节->输入目标实例数->选择不调节->点击更新->保存成功，没有创建定时调节', () => {
    const testData = {
      定时调节: [['自定义', '1 12 * * *', 3]],
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55803 : 已存在一个定时调节hpa->点击更新->输入目标实例数6，选择周一周日，1:01触发->点击"X"->弹窗成功关闭，修改没有保存', () => {
    const testData = {
      定时调节: [['时间', ['星期一,星期日', '01:01'], 6]],
    };
    page.detailPage.updateHpa(deployment_name, testData, 'close');
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55798 : 已存在一个定时调节hpa->点击更新->输入目标实例数6，选择周一周日，1:01触发->点击更新->保存成功，值成功被修改', () => {
    const testData = {
      定时调节: [
        ['时间', ['星期一,星期日', '01:01'], 6],
        ['自定义', '1 2 3 * *', 0],
      ],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '触发规则目标实例数星期日、星期一01:01触发6123**0',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55794 : 已存在一个定时调节hpa->点击更新->选择不设置->点击更新->保存成功，原来的hpa被删除', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
