import { ServerConf } from '@e2e/config/serverConf';
import { DaemonSetPage } from '@e2e/page_objects/acp/daemonset/daemonset.page';

describe('用户视图 守护进程集L0自动化', () => {
  const page = new DaemonSetPage();
  const daemonset_name = page.getTestData('daemonset');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(daemonset_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('守护进程集');
  });
  afterAll(() => {
    page.preparePage.delete(daemonset_name);
  });
  it('ACP2UI-54691 : L0:创建守护进程集-输入必填项-点击【确定】-创建成功', () => {
    const testData = {
      名称: daemonset_name,
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/守护进程集/${daemonset_name}`,
      主机选择器: '-',
      更新策略: '滚动更新策略 ( 最多不可用数: 25% )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54698 : 计算-守护进程集-列表页字段验证', () => {
    page.listPage.waitStatus(daemonset_name, '运行中');
    const expectCreateData = {
      标题: '计算/守护进程集',
      header: ['名称', '状态', '所属应用', '创建时间'],
      数量: 1,
      创建: {
        名称: daemonset_name,
        // 状态: '运行中',
        所属应用: '-',
      },
    };
    page.listPageVerify.verify(expectCreateData);
  });
  it('ACP2UI-54034 : 计算-守护进程集-列表页-操作-点击【删除】-守护进程集删除成功', () => {
    page.listPage.delete(daemonset_name);
    // 验证搜索正确
    page.listPage.search(daemonset_name, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
