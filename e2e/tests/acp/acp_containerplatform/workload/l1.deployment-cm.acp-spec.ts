import { ServerConf } from '@e2e/config/serverConf';
import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name_cm = page.getTestData('l1-deploy-cm');

  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const cm_page = new ConfigPage();
  const secret_page = new SecretPage();
  const third_name = page.getTestData('l1-deploy');
  beforeAll(() => {
    page.preparePage.delete(deployment_name_cm);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
    const configmap_data = {
      name: third_name,
      namespace: namespace_name,
      datas: { 'l1-deploy-cm': 'l1-deploy-configmap' },
    };
    cm_page.prepareData.create(configmap_data);
    const secret_data = {
      name: third_name,
      namespace: namespace_name,
      datatype: 'Opaque',
      datas: {
        'l1-deploy-sc': 'l1-deploy-secret',
      },
    };
    secret_page.prepareData.create(secret_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name_cm);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
  });

  it('ACP2UI-53914 : 创建部署-输入必填项-更新大小-（容器高级）环境变量-配置引用-存储卷挂载cm/secret-端口-（容器组高级）存储卷-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name_cm,
      资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
      存储卷: [
        {
          名称: 'deploy-volume-cm',
          类型: '配置字典',
          配置字典: third_name,
        },
        {
          名称: 'deploy-volume-secret',
          类型: '保密字典',
          保密字典: third_name,
        },
      ],

      环境变量添加引用: [
        ['envfromcm', ['配置字典', third_name], 'l1-deploy-cm'],
        ['envfromsecret', ['保密字典', third_name], 'l1-deploy-sc'],
      ],
      配置引用: [['配置字典', third_name], ['保密字典', third_name]],
      存储卷挂载: [
        [['配置字典', 'deploy-volume-cm'], 'l1-deploy-cm', '/test/cmsub'],
        [['配置字典', 'deploy-volume-cm'], '', '/test/cm', true],
        [
          ['保密字典', 'deploy-volume-secret'],
          'l1-deploy-sc',
          '/test/secretsub',
        ],
        [['保密字典', 'deploy-volume-secret'], '', '/test/secret'],
      ],
      端口: [['TCP', '80', '40080']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      存储卷: [
        'deploy-volume-cm',
        '配置字典',
        third_name,
        'deploy-volume-secret',
        '保密字典',
        third_name,
      ],
      已挂载存储卷: [
        'deploy-volume-cm',
        'l1-deploy-cm',
        '/test/cmsub',
        '/test/cm',
        'deploy-volume-secret',
        'l1-deploy-sc',
        '/test/secretsub',
        '/test/secret',
      ],
      端口: ['TCP', '80', '40080'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-53985 : 查看配置tab显示，环境变量（键，值/引用值），配置引用（类型，名称），操作-更新', () => {
    page.listPage.enterDetail(deployment_name_cm);
    const expectDetailData = {
      环境变量: [
        'envfromcm',
        `配置字典 ${third_name} : l1-deploy-cm`,
        'envfromsecret',
        `保密字典 ${third_name} : l1-deploy-sc`,
      ],
      配置引用: ['配置字典', third_name, '保密字典', third_name],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53986 : 环境变量，点击更新，弹出更新环境变量页面-添加环境变量', () => {
    const testData = {
      环境变量: [
        ['envfromcm', ['配置字典', third_name], 'l1-deploy-cm'],
        ['envfromsecret', ['保密字典', third_name], 'l1-deploy-sc'],
        ['addenv', 'addenv'],
      ],
    };
    page.detailPage.updateEnv(deployment_name_cm, testData);
    const expectDetailData = {
      环境变量: ['addenv'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53986 : 环境变量，点击更新，弹出更新环境变量页面-更新环境变量', () => {
    const testData2 = {
      环境变量添加引用: [
        ['envupdatecm', ['配置字典', third_name], 'l1-deploy-cm'],
        ['envupdatesecret', ['保密字典', third_name], 'l1-deploy-sc'],
        ['updateenv', 'updateenv'],
      ],
    };
    page.detailPage.updateEnv(deployment_name_cm, testData2);
    const expectDetailData2 = {
      环境变量: [
        'updateenv',
        'envupdatecm',
        `配置字典 ${third_name} : l1-deploy-cm`,
        'envupdatesecret',
        `保密字典 ${third_name} : l1-deploy-sc`,
      ],
    };
    page.detailPageVerify.verify(expectDetailData2);
  });

  it('ACP2UI-53987 : 配置引用，点击更新，弹出更新配置引用页面', () => {
    const testData = {
      删除配置引用: [`配置字典: ${third_name}`, `保密字典: ${third_name}`],
    };
    page.detailPage.updateConfig(deployment_name_cm, testData);
    const expectDetailData = {
      无配置引用: 'true',
    };
    page.detailPageVerify.verify(expectDetailData);
    const testData2 = {
      配置引用: [['配置字典', third_name], ['保密字典', third_name]],
    };
    page.detailPage.updateConfig(deployment_name_cm, testData2);
    const expectDetailData2 = {
      配置引用: ['配置字典', third_name, '保密字典', third_name],
    };
    page.detailPageVerify.verify(expectDetailData2);
  });
});
