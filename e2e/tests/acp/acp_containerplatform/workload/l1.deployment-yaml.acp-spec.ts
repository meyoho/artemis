import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('用户视图 部署L1自动化', () => {
  const page = new DeploymentPage();

  const deployment_name_yaml = page.getTestData('l1-deploy-yaml');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(deployment_name_yaml);

    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name_yaml);
  });

  it('ACP2UI-53945 : YAML创建部署-输入正确的yaml格式内容-点击【创建】-部署创建成功', () => {
    const yamlstring = CommonMethod.readyamlfile('alauda.deployment.yaml', {
      '${NAME}': deployment_name_yaml,
      '${NAMESPACE}': namespace_name,
      '${IMAGE}': imageAddress,
    });
    page.createPage.createByYaml(yamlstring);
    const expectDetailData = {
      面包屑: `计算/部署/${deployment_name_yaml}`,
      状态: '运行中',
      实例数: '1',
      // 容器组标签: `app:${deployment_name_yaml}`,
      主机选择器: '-',
      更新策略: '滚动更新策略 ( 最大可超出数: 25%, 最多不可用数: 25% )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53889 : 计算-部署-列表页-搜索框里输入名称(精确)-信息显示正确', () => {
    page.listPage.search(deployment_name_yaml.toUpperCase());
    const expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53889 : 计算-部署-列表页-搜索框里输入名称(大小写)-信息显示正确', () => {
    page.listPage.search(deployment_name_yaml.replace('yaml', 'YAML'));
    const expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53889 : 计算-部署-列表页-搜索框里输入名称(模糊)-信息显示正确', () => {
    const expectData = { 数量: 1 };
    page.listPage.search(deployment_name_yaml.substring(2));
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53889 : 计算-部署-列表页-搜索框里输入名称(不存在)-信息显示正确', () => {
    page.listPage.search(deployment_name_yaml.concat('notexist'), 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53975 : 详情信息-开始/停止', () => {
    page.detailPage.stop(deployment_name_yaml);
    const expectStopData = {
      状态: '已停止',
      实例数: '0',
    };
    page.detailPageVerify.verify(expectStopData);
    page.detailPage.start(deployment_name_yaml);
    const expectStartData = {
      状态: '运行中',
      实例数: '1',
    };
    page.detailPageVerify.verify(expectStartData);
  });
  it('ACP2UI-53972 : 详情信息-点击pod个数的“+”“-”按钮，实现pod个数的增加和减少', () => {
    page.detailPage.scaleUp(deployment_name_yaml);
    const expectUpData = {
      状态: '运行中',
      实例数: '2',
    };
    page.detailPageVerify.verify(expectUpData);
    page.detailPage.scaleDown(deployment_name_yaml);
    const expectDownData = {
      状态: '运行中',
      实例数: '1',
    };
    page.detailPageVerify.verify(expectDownData);
  });
  xit('ACP2UI-53949 : 详情信息-基本信息显示正确-更新容器组标签', () => {
    let testData = [['app', deployment_name_yaml], ['podlabel', 'addlabel']];
    page.detailPage.updateLabel(deployment_name_yaml, testData);
    let expectData = {
      容器组标签: `app:${deployment_name_yaml};podlabel:addlabel`,
    };
    page.detailPageVerify.verify(expectData);
    testData = [['app', deployment_name_yaml], ['podlabel', 'updatelabel']];
    page.detailPage.updateLabel(deployment_name_yaml, testData);
    expectData = {
      容器组标签: `app:${deployment_name_yaml};podlabel:updatelabel`,
    };
    page.detailPageVerify.verify(expectData);
    testData = [
      ['app', deployment_name_yaml],
      ['podlabel', 'deletelabel', 'true'],
    ];
    page.detailPage.updateLabel(deployment_name_yaml, testData);
    expectData = {
      容器组标签: `app:${deployment_name_yaml}`,
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53959 : 详情信息-容器基本信息-更新镜像', () => {
    page.detailPage.updateImage(deployment_name_yaml, 'latest');
    const expectImageData = {
      镜像: imageAddress.replace('helloworld', 'latest'),
    };
    page.detailPageVerify.verify(expectImageData);
  });
  it('ACP2UI-53959 : 详情信息-容器基本信息-更新资源限制', () => {
    const testData = [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }];
    page.detailPage.updateLimit(deployment_name_yaml, testData);
    const expectData = {
      资源限制: 'CPU:20m内存:20Mi',
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-53973 : 详情信息-点击【更新】-进入到更新部署页面', () => {
    const testData = {
      显示名称: deployment_name_yaml,
      实例数量: 2,
      更新策略: ['20%', '20%'],
      镜像地址: imageAddress,
      资源限制: [{ CPU: 10, 单位: 'm' }, { 内存: 10, 单位: 'Mi' }],
      环境变量: [['env', 'env']],
    };
    page.detailPage.update(deployment_name_yaml, testData);
    const expectDetailData = {
      状态: '运行中',
      实例数: '2',
      显示名称: deployment_name_yaml,
      更新策略: '滚动更新策略 ( 最大可超出数: 20%, 最多不可用数: 20% )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      环境变量: ['env'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-53887 : 计算-部署-列表页-操作-点击【更新】-进入部署更新页', () => {
    const testData = {
      显示名称: deployment_name_yaml,
      实例数量: 1,
    };
    page.listPage.update(deployment_name_yaml, testData);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53974 : 详情信息-点击【删除】-页面弹出删除部署确定框', () => {
    page.detailPage.delete(deployment_name_yaml, 'cancel');
    const expectDetailData = {
      显示名称: deployment_name_yaml,
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54035 : 计算-部署-列表页-操作-点击【删除】-部署删除成功', () => {
    page.listPage.delete(deployment_name_yaml, 'cancel');
    page.listPage.search(deployment_name_yaml);
    let expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);

    page.detailPage.delete(deployment_name_yaml);
    page.listPage.search(deployment_name_yaml, 0);
    expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
