// TODO: statefulset 将会重构，case 先暂时注释掉了

// import { ServerConf } from '@e2e/config/serverConf';
// import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
// import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';
// import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';
// import { CommonMethod } from '@e2e/utility/common.method';
// import { StatefulSetPage } from '@e2e/page_objects/acp/statefulset/statefulset.page';

// describe('用户视图 有状态副本集L1自动化', () => {
//   const page = new StatefulSetPage();
//   const statefulset_name_base = page.getTestData('l1-sts-base');
//   const statefulset_name_cm = page.getTestData('l1-sts-cm');
//   const statefulset_name_vol = page.getTestData('l1-sts-vol');
//   const statefulset_name_yaml = page.getTestData('l1-sts-yaml');
//   const namespace_name = page.namespace1Name;
//   const imageAddress = ServerConf.TESTIMAGE;
//   const cm_page = new ConfigPage();
//   const secret_page = new SecretPage();
//   const pvc_page = new StoragePage();
//   const third_name = page.getTestData('l1-sts');
//   beforeAll(() => {
//     page.preparePage.delete(statefulset_name_base);
//     page.preparePage.delete(statefulset_name_cm);
//     page.preparePage.delete(statefulset_name_vol);
//     page.preparePage.delete(statefulset_name_yaml);
//     cm_page.prepareData.delete(third_name);
//     secret_page.prepareData.delete(third_name);
//     pvc_page.prePareData.deletePvc(third_name, page.namespace1Name);
//     const configmap_data = {
//       name: third_name,
//       namespace: namespace_name,
//       datas: { 'l1-statefulset-cm': 'l1-statefulset-configmap' },
//     };
//     cm_page.prepareData.create(configmap_data);
//     const secret_data = {
//       name: third_name,
//       namespace: namespace_name,
//       datatype: 'Opaque',
//       datas: {
//         'l1-statefulset-sc': 'l1-statefulset-secret',
//       },
//     };
//     secret_page.prepareData.create(secret_data);

//     const pvc_data = {
//       name: third_name,
//       namespace: namespace_name,
//     };
//     pvc_page.prePareData.createPvcByYaml(pvc_data);

//     page.login();
//     page.enterUserView(namespace_name);
//   });
//   beforeEach(() => {
//     page.clickLeftNavByText('有状态副本集');
//   });
//   afterAll(() => {
//     page.preparePage.delete(statefulset_name_base);
//     page.preparePage.delete(statefulset_name_cm);
//     page.preparePage.delete(statefulset_name_vol);
//     page.preparePage.delete(statefulset_name_yaml);
//     cm_page.prepareData.delete(third_name);
//     secret_page.prepareData.delete(third_name);
//     pvc_page.prePareData.deletePvc(third_name, page.namespace1Name);
//   });
//   it('ACP2UI-54815 : 创建有状态副本集-输入必填项-增加实例数量为2-组件标签-启动命令和参数-容器组标签-主机选择器-打开host网络模式-点击【确定】-创建成功', () => {
//     const testData = {
//       名称: statefulset_name_base,
//       显示名称: statefulset_name_base,
//       实例数量: 2,
//       更新策略: [2],
//       组件标签: [['deploylabel', 'deploylabel']],
//       启动命令: [['/bin/sh']],
//       参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
//       环境变量: [['env', 'env']],
//       端口: [['TCP', '80', '']],
//       容器组标签: [['podlabel', 'podlabel']],
//       主机选择器: ['beta.kubernetes.io/os:linux'],
//       'Host 模式': 'true',
//     };
//     page.createPage.create(testData, imageAddress);
//     const expectDetailData = {
//       状态: '运行中',
//       实例数: '2',
//       显示名称: statefulset_name_base,
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54198 : 计算-有状态副本集-列表页-点击名称-进入有状态副本集详情页', () => {
//     page.listPage.enterDetail(statefulset_name_base);
//     const expectDetailData = {
//       显示名称: statefulset_name_base,
//       容器组标签: 'podlabel:podlabel',
//       主机选择器: 'beta.kubernetes.io/os:linux',
//       更新策略: '滚动更新策略 ( partition: 2 )',
//       镜像: imageAddress,
//       资源限制: 'CPU:10m内存:10Mi',
//       启动命令: ['/bin/sh'],
//       参数: ['-c', 'while true; do sleep 10; echo 123; done'],
//       环境变量: ['env'],
//       YAML: {
//         spec: {
//           template: {
//             spec: {
//               containers: [
//                 {
//                   ports: [
//                     {
//                       containerPort: 80,
//                       protocol: 'TCP',
//                     },
//                   ],
//                 },
//               ],
//             },
//           },
//         },
//       },
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });

//   xit('ACP2UI-54675 : 创建有状态副本集-输入必填项-环境变量-配置引用-添加健康检查-添加反亲和-点击【确定】-创建成功', () => {
//     const testData = {
//       名称: statefulset_name_cm,
//       存储卷: [
//         {
//           名称: 'statefulset-volume-cm',
//           类型: '配置字典',
//           配置字典: third_name,
//         },
//         {
//           名称: 'statefulset-volume-secret',
//           类型: '保密字典',
//           保密字典: third_name,
//         },
//       ],
//       亲和性: [
//         {
//           亲和性: 'Pod 反亲和',
//           方式: '基本',
//           亲和组件: ['有状态副本集', statefulset_name_base],
//         },
//       ],
//       环境变量添加引用: [
//         ['envfromcm', ['配置字典', third_name], 'l1-statefulset-cm'],
//         ['envfromsecret', ['保密字典', third_name], 'l1-statefulset-sc'],
//       ],
//       配置引用: [['配置字典', third_name], ['保密字典', third_name]],
//       添加存活性健康检查: {
//         协议类型: 'HTTP',
//         启动时间: 10,
//         间隔: 10,
//       },
//       添加可用性健康检查: {
//         协议类型: 'TCP',
//         启动时间: 10,
//         间隔: 10,
//         正常阈值: 1,
//       },
//       存储卷挂载: [
//         [
//           ['配置字典', 'statefulset-volume-cm'],
//           'l1-statefulset-cm',
//           '/test/cmsub',
//         ],
//         [['配置字典', 'statefulset-volume-cm'], '', '/test/cm', true],
//         [
//           ['保密字典', 'statefulset-volume-secret'],
//           'l1-statefulset-sc',
//           '/test/secretsub',
//         ],
//         [['保密字典', 'statefulset-volume-secret'], '', '/test/secret'],
//       ],
//       端口: [['TCP', '80', '40090']],
//     };
//     page.createPage.create(testData, imageAddress);
//     const expectDetailData = {
//       状态: '运行中',
//       实例数: '1',
//       存储卷: [
//         'statefulset-volume-cm',
//         '配置字典',
//         third_name,
//         'statefulset-volume-secret',
//         '保密字典',
//         third_name,
//       ],
//       'Pod 亲和': [
//         'Pod 反亲和',
//         'Required',
//         'kubernetes.io/hostname',
//         `service.${ServerConf.LABELBASEDOMAIN}/name: statefulset-${statefulset_name_base}`,
//       ],
//       存活性健康检查: {
//         健康检查类型: '存活性',
//         协议类型: 'HTTP',
//         启动时间: 10,
//         间隔: 10,
//         超时时长: 30,
//         正常阈值: 1,
//         不正常阈值: 5,
//         协议: 'HTTP',
//         端口: 80,
//         路径: '/',
//         请求头: '无请求头',
//       },
//       可用性健康检查: {
//         健康检查类型: '可用性',
//         协议类型: 'TCP',
//         启动时间: 10,
//         间隔: 10,
//         超时时长: 30,
//         正常阈值: 1,
//         不正常阈值: 5,
//         端口: 80,
//       },
//       已挂载存储卷: [
//         'statefulset-volume-cm',
//         'l1-statefulset-cm',
//         '/test/cmsub',
//         '/test/cm',
//         'statefulset-volume-secret',
//         'l1-statefulset-sc',
//         '/test/secretsub',
//         '/test/secret',
//       ],
//       环境变量: [
//         'envfromcm',
//         `配置字典 ${third_name}: l1-statefulset-cm`,
//         'envfromsecret',
//         `保密字典 ${third_name}: l1-statefulset-sc`,
//       ],
//       配置引用: ['配置字典', third_name, '保密字典', third_name],
//       YAML: {
//         spec: {
//           template: {
//             spec: {
//               containers: [
//                 {
//                   ports: [
//                     { hostPort: 40090, containerPort: 80, protocol: 'TCP' },
//                   ],
//                 },
//               ],
//             },
//           },
//         },
//       },
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });

//   it('ACP2UI-54676 : 创建有状态副本集-输入必填项-存储卷-存储卷挂载-日志文件-添加容器-添加亲和性-点击【确定】-创建成功', () => {
//     const testData = {
//       名称: statefulset_name_vol,
//       存储卷: [
//         {
//           名称: 'statefulset-volume-pvc',
//           类型: '持久卷声明',
//           持久卷声明: third_name,
//         },
//         {
//           名称: 'statefulset-volume-path',
//           类型: '主机路径',
//           主机路径: '/tmp',
//         },
//         {
//           名称: 'statefulset-volume-empty',
//           类型: '空目录',
//         },
//       ],
//       亲和性: [
//         {
//           亲和性: 'Pod 亲和',
//           方式: '高级',
//           亲和类型: 'Preferred',
//           权重: 3,
//           匹配标签: [
//             [
//               `service.${ServerConf.LABELBASEDOMAIN}/name`,
//               `statefulset-${statefulset_name_base}`,
//             ],
//           ],
//         },
//       ],
//       存储卷挂载: [
//         [
//           ['持久卷声明', 'statefulset-volume-pvc'],
//           'l1-statefulset-pvc',
//           '/test/pvcsub',
//         ],
//         [['持久卷声明', 'statefulset-volume-pvc'], '', '/test/pvc'],
//         [['主机路径', 'statefulset-volume-path'], 'sub', '/test/path'],
//         [['空目录', 'statefulset-volume-empty'], '', '/test/empty'],
//       ],
//       端口: [['TCP', '80', '40091'], ['UDP', '81', '40092']],
//       日志文件: [['/var/*.*']],
//       排除日志文件: [['/var/hehe.txt']],
//     };
//     page.createPage.create(testData, imageAddress);
//     const expectDetailData = {
//       状态: '运行中',
//       实例数: '1',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54680 : 详情信息-亲和性，已挂载存储卷，健康检查，日志文件', () => {
//     page.listPage.enterDetail(statefulset_name_vol);
//     const expectDetailData = {
//       存储卷: [
//         'statefulset-volume-pvc',
//         '持久卷声明',
//         third_name,
//         '主机路径',
//         '/tmp',
//         'statefulset-volume-empty',
//         '空目录',
//         '-',
//       ],
//       'Pod 亲和': [
//         'Pod 亲和',
//         'Preferred',
//         '3',
//         // 'kubernetes.io/hostname',
//         `service.${ServerConf.LABELBASEDOMAIN}/name: statefulset-${statefulset_name_base}`,
//       ],
//       已挂载存储卷: [
//         'statefulset-volume-pvc',
//         'l1-statefulset-pvc',
//         '/test/pvcsub',
//         '/test/pvc',
//         'statefulset-volume-path',
//         '/test/path',
//         'statefulset-volume-empty',
//         '/test/empty',
//       ],
//       日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
//       YAML: {
//         spec: {
//           template: {
//             spec: {
//               containers: [
//                 {
//                   ports: [
//                     {
//                       hostPort: 40091,
//                       containerPort: 80,
//                       protocol: 'TCP',
//                     },
//                     {
//                       hostPort: 40092,
//                       containerPort: 81,
//                       protocol: 'UDP',
//                     },
//                   ],
//                 },
//               ],
//             },
//           },
//         },
//       },
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54269 : YAML创建有状态副本集-输入正确的yaml格式内容-点击【创建】-有状态副本集创建成功', () => {
//     const yamlstring = CommonMethod.readyamlfile('alauda.statefulset.yaml', {
//       '${NAME}': statefulset_name_yaml,
//       '${NAMESPACE}': namespace_name,
//       '${IMAGE}': imageAddress,
//     });
//     page.createPage.createByYaml(yamlstring);
//     const expectDetailData = {
//       面包屑: `计算/有状态副本集/${statefulset_name_yaml}`,
//       状态: '运行中',
//       实例数: '1',
//       容器组标签: `app:${statefulset_name_yaml}`,
//       主机选择器: '-',
//       更新策略: '滚动更新策略 ( partition: 0 )',
//       镜像: imageAddress,
//       资源限制: 'CPU:10m内存:10Mi',
//       启动命令: ['-'],
//       参数: ['-'],
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54293 : 详情信息-开始/停止', () => {
//     page.detailPage.stop(statefulset_name_yaml);
//     const expectStopData = {
//       状态: '已停止',
//       实例数: '0',
//     };
//     page.detailPageVerify.verify(expectStopData);
//     page.detailPage.start(statefulset_name_yaml);
//     const expectStartData = {
//       状态: '运行中',
//       实例数: '1',
//     };
//     page.detailPageVerify.verify(expectStartData);
//   });
//   xit('ACP2UI-54290 : 详情信息-点击pod个数的“+”“-”按钮，实现pod个数的增加和减少', () => {
//     page.detailPage.scaleUp(statefulset_name_yaml);
//     const expectUpData = {
//       状态: '运行中',
//       实例数: '2',
//     };
//     page.detailPageVerify.verify(expectUpData);
//     page.detailPage.scaleDown(statefulset_name_yaml);
//     const expectDownData = {
//       状态: '运行中',
//       实例数: '1',
//     };
//     page.detailPageVerify.verify(expectDownData);
//   });
//   it('ACP2UI-54678 : 详情信息-基本信息显示正确-更新容器组标签', () => {
//     let testData = [['app', statefulset_name_yaml], ['podlabel', 'addlabel']];
//     page.detailPage.updateLabel(statefulset_name_yaml, testData);
//     let expectData = {
//       容器组标签: `app:${statefulset_name_yaml};podlabel:addlabel`,
//     };
//     page.detailPageVerify.verify(expectData);
//     testData = [['app', statefulset_name_yaml], ['podlabel', 'updatelabel']];
//     page.detailPage.updateLabel(statefulset_name_yaml, testData);
//     expectData = {
//       容器组标签: `app:${statefulset_name_yaml};podlabel:updatelabel`,
//     };
//     page.detailPageVerify.verify(expectData);
//     testData = [
//       ['app', statefulset_name_yaml],
//       ['podlabel', 'deletelabel', 'true'],
//     ];
//     page.detailPage.updateLabel(statefulset_name_yaml, testData);
//     expectData = {
//       容器组标签: `app:${statefulset_name_yaml}`,
//     };
//     page.detailPageVerify.verify(expectData);
//   });
//   it('ACP2UI-54679 : 详情信息-容器基本信息-更新镜像-更新资源限制', () => {
//     page.detailPage.updateImage(statefulset_name_yaml, 'latest');
//     const expectImageData = {
//       镜像: imageAddress.replace('helloworld', 'latest'),
//     };
//     page.detailPageVerify.verify(expectImageData);
//     const testData = [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }];
//     page.detailPage.updateLimit(statefulset_name_yaml, testData);
//     const expectData = {
//       资源限制: 'CPU:20m内存:20Mi',
//     };
//     page.detailPageVerify.verify(expectData);
//   });
//   it('ACP2UI-54303 : 环境变量，点击更新，弹出更新环境变量页面', () => {
//     const testData = {
//       环境变量: [
//         ['envfromcm', ['配置字典', third_name], 'l1-statefulset-cm'],
//         ['envfromsecret', ['保密字典', third_name], 'l1-statefulset-sc'],
//         ['addenv', 'addenv'],
//       ],
//     };
//     page.detailPage.updateEnv(statefulset_name_cm, testData);
//     const expectDetailData = {
//       环境变量: ['addenv'],
//     };
//     page.detailPageVerify.verify(expectDetailData);
//     const testData2 = {
//       环境变量添加引用: [
//         ['envupdatecm', ['配置字典', third_name], 'l1-statefulset-cm'],
//         ['envupdatesecret', ['保密字典', third_name], 'l1-statefulset-sc'],
//         ['updateenv', 'updateenv'],
//       ],
//     };
//     page.detailPage.updateEnv(statefulset_name_cm, testData2);
//     const expectDetailData2 = {
//       环境变量: [
//         'updateenv',
//         'envupdatecm',
//         `配置字典 ${third_name}: l1-statefulset-cm`,
//         'envupdatesecret',
//         `保密字典 ${third_name}: l1-statefulset-sc`,
//       ],
//     };
//     page.detailPageVerify.verify(expectDetailData2);
//   });
//   it('ACP2UI-54304 : 配置引用，点击更新，弹出更新配置引用页面', () => {
//     const testData = {
//       删除配置引用: [`配置字典: ${third_name}`, `保密字典: ${third_name}`],
//     };
//     page.detailPage.updateConfig(statefulset_name_cm, testData);
//     const expectDetailData = {
//       无配置引用: 'true',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//     const testData2 = {
//       配置引用: [['配置字典', third_name], ['保密字典', third_name]],
//     };
//     page.detailPage.updateConfig(statefulset_name_cm, testData2);
//     const expectDetailData2 = {
//       配置引用: ['配置字典', third_name, '保密字典', third_name],
//     };
//     page.detailPageVerify.verify(expectDetailData2);
//   });
//   it('ACP2UI-53988 : 详情-日志-容器组/容器信息显示正确', () => {
//     page.listPage.enterDetail(statefulset_name_base);
//     const expectDetailData = {
//       日志: '123',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54291 : 详情信息-点击【更新】-进入到更新有状态副本集页面', () => {
//     const testData = {
//       显示名称: statefulset_name_yaml,
//       实例数量: 2,
//       镜像地址: imageAddress,
//       资源限制: [{ CPU: 10, 单位: 'm' }, { 内存: 10, 单位: 'Mi' }],
//       环境变量: [['env', 'env']],
//     };
//     page.detailPage.update(statefulset_name_yaml, testData);
//     const expectDetailData = {
//       状态: '运行中',
//       实例数: '2',
//       显示名称: statefulset_name_yaml,
//       镜像: imageAddress,
//       资源限制: 'CPU:10m内存:10Mi',
//       环境变量: ['env'],
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54292 : 详情信息-点击【删除】-页面弹出删除有状态副本集确定框', () => {
//     page.detailPage.delete(statefulset_name_yaml, 'cancel');
//     const expectDetailData = {
//       显示名称: statefulset_name_yaml,
//     };
//     page.detailPageVerify.verify(expectDetailData);
//     page.detailPage.delete(statefulset_name_yaml);
//     page.listPage.search(statefulset_name_yaml, 0);
//     const expectData = { 数量: 0 };
//     page.listPageVerify.verify(expectData);
//   });
//   xit('ACP2UI-54199 : 计算-有状态副本集-列表页-操作-点击【更新】-进入有状态副本集更新页', () => {
//     const testData = {
//       显示名称: statefulset_name_base,
//       实例数量: 1,
//     };
//     page.listPage.update(statefulset_name_base, testData);
//     const expectDetailData = {
//       状态: '运行中',
//       实例数: '1',
//     };
//     page.detailPageVerify.verify(expectDetailData);
//   });
//   it('ACP2UI-54203 : 计算-有状态副本集-列表页-操作-点击【删除】-有状态副本集删除成功', () => {
//     page.listPage.delete(statefulset_name_vol, 'cancel');
//     page.listPage.search(statefulset_name_vol);
//     let expectData = { 数量: 1 };
//     page.listPageVerify.verify(expectData);
//     page.detailPage.delete(statefulset_name_vol);
//     page.listPage.search(statefulset_name_vol, 0);
//     expectData = { 数量: 0 };
//     page.listPageVerify.verify(expectData);
//   });
//   it('ACP2UI-54201 : 计算-有状态副本集-列表页-搜索框里输入名称(精确/模糊/大小写)-信息显示正确', () => {
//     page.listPage.search(statefulset_name_base.toUpperCase());
//     let expectData = { 数量: 1 };
//     page.listPageVerify.verify(expectData);
//     page.listPage.search(statefulset_name_base.replace('base', 'BASE'));
//     page.listPageVerify.verify(expectData);
//     page.listPage.search(statefulset_name_base.substring(2));
//     page.listPageVerify.verify(expectData);
//     page.listPage.search(statefulset_name_base.concat('notexist'), 0);
//     expectData = { 数量: 0 };
//     page.listPageVerify.verify(expectData);
//   });
// });
