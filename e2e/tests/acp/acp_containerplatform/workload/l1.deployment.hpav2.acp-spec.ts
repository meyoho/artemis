import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署新指标扩缩容L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('hpav2');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const hpav2_is_enabled = page.featureIsEnabled('hpav2');
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  if (!hpav2_is_enabled) {
    return;
  }

  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.preparePage.deleteHpa(deployment_name);
    page.preparePage.deleteCronHpa(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
    const testData = {
      名称: deployment_name,
    };
    page.createPage.create(testData, imageAddress);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteHpa(deployment_name);
    page.preparePage.deleteCronHpa(deployment_name);
    page.preparePage.delete(deployment_name);
  });

  it('ACP2UI-59064 : hpav2功能开关打开，添加新的指标调节，添加成功，验证详情页显示正确', () => {
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 2,
        触发策略: [['CPU 利用率', 10]],
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1最大实例数2触发策略指标目标阈值CPU利用率10%',
    };
    page.detailPageVerify.verify(expectHpaData);
  });

  it('ACP2UI-59069 : hpav2功能开关打开，添加多条触发策略（验证同一指标只能添加一次），添加成功，验证详情页显示正确', () => {
    const testData = {
      指标调节: {
        最小实例数: 2,
        最大实例数: 3,
        触发策略: [
          ['CPU 利用率', 11],
          ['内存利用率', 12],
          ['网络入流量', 13],
          ['网络出流量', 14],
          ['存储读流量', 15],
          ['存储写流量', 16],
        ],
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容:
        '最小实例数2最大实例数3触发策略指标目标阈值内存利用率12%网络入流量13KiB/s网络出流量14KiB/s存储读流量15KiB/s存储写流量16KiB/sCPU利用率11%',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-59079 : hpav2功能开关打开，已存在一个指标调节hpa->点击更新->选择不设置->点击更新->保存成功，原来的hpa被删除', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-59080 : hpav2打开，选择新指标调节->输入最大实例数3，最小实例数1，cpu阈值10->选择定时调节->输入目标实例数2->点击更新按钮->能正常保存，指标调节没有创建，定时调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        触发策略: [['内存利用率', 10]],
      },
      定时调节: [['自定义', '1 2 3 * *', 0]],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = { 自动扩缩容: '触发规则目标实例数123**0' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-59081 : hpav2打开，选择定时调节->输入目标实例数2->选择新指标调节->输入最大最小实例数和cpu阈值->点击更新->保存成功，没有创建定时调节，指标调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '1 2 3 4 5', 2]],
      指标调节: {
        最小实例数: 1,
        最大实例数: 1,
        触发策略: [['网络入流量', 10]],
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1最大实例数1触发策略指标目标阈值网络入流量10KiB/s',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
