import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署旧指标扩缩容L1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('hpa');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const hpav2_is_enabled = page.featureIsEnabled('hpav2');
  const cronhpa_is_enabled = page.preparePage.cronhpaIsEnabled;
  if (hpav2_is_enabled) {
    return;
  }
  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.preparePage.deleteHpa(deployment_name);
    page.preparePage.deleteCronHpa(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
    const testData = {
      名称: deployment_name,
    };
    page.createPage.create(testData, imageAddress);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteHpa(deployment_name);
    page.preparePage.deleteCronHpa(deployment_name);
    page.preparePage.delete(deployment_name);
  });

  it('ACP2UI-55787 : 部署详情页->点击更新->选择指标调节->输入最大实例数3，最小实例数1，cpu阈值10->点击更新按钮->指标扩缩容能正常保存', () => {
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率10%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55801 : 部署详情页->查看指标调节hpa详情->页面显示最大最小实例数和cpu阈值，名称和deployment同名', () => {
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率10%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55797 : 已存在一个指标调节hpa->点击更新->输入最大实例3，最小实例3，cpu阈值20->点击更新->保存成功，值成功修改', () => {
    const testData = {
      指标调节: {
        最小实例数: 3,
        最大实例数: 3,
        'CPU 利用率': 20,
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数3CPU利用率20%最大实例数3',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55793 : 已存在一个指标调节hpa->点击更新->选择不设置->点击更新->保存成功，原来的hpa被删除', () => {
    const testData = {
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '无自动扩缩容',
    };
    page.detailPageVerify.verify(expectHpaData);
  });

  it('ACP2UI-55805 : 部署详情页->点击更新->选择指标调节->输入最大实例数3，最小实例数1，cpu阈值10->选择不调节->点击更新按钮->能正常保存，指标调节没有创建', () => {
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
      类型: '不设置',
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = { 自动扩缩容: '无自动扩缩容' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55806 : 部署详情页->点击更新->选择旧指标调节->输入最大实例数3，最小实例数1，cpu阈值10->选择定时调节->输入目标实例数2->点击更新按钮->能正常保存，指标调节没有创建，定时调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      指标调节: {
        最小实例数: 1,
        最大实例数: 3,
        'CPU 利用率': 10,
      },
      定时调节: [['自定义', '1 2 3 * *', 0]],
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = { 自动扩缩容: '触发规则目标实例数123**0' };
    page.detailPageVerify.verify(expectHpaData);
  });
  it('ACP2UI-55807 : 部署详情页->点击更新->选择定时调节->输入目标实例数2->选择旧指标调节->输入最大最小实例数和cpu阈值->点击更新->保存成功，没有创建定时调节，指标调节成功创建', () => {
    if (!cronhpa_is_enabled) {
      return;
    }
    const testData = {
      定时调节: [['自定义', '1 2 3 4 5', 2]],
      指标调节: {
        最小实例数: 1,
        最大实例数: 1,
        'CPU 利用率': 1,
      },
    };
    page.detailPage.updateHpa(deployment_name, testData);
    const expectHpaData = {
      自动扩缩容: '最小实例数1CPU利用率1%最大实例数1',
    };
    page.detailPageVerify.verify(expectHpaData);
  });
});
