import { ServerConf } from '@e2e/config/serverConf';
import { ConfigPage } from '@e2e/page_objects/acp/container/conifgmap/configmap.page';
import { SecretPage } from '@e2e/page_objects/acp/container/secret/secret.page';
import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { DaemonSetPage } from '@e2e/page_objects/acp/daemonset/daemonset.page';

describe('用户视图 守护进程集L1自动化', () => {
  const page = new DaemonSetPage();
  const daemonset_name_base = page.getTestData('l1-daemonset-base');
  const daemonset_name_cm = page.getTestData('l1-daemonset-cm');
  const daemonset_name_vol = page.getTestData('l1-daemonset-vol');
  const daemonset_name_yaml = page.getTestData('l1-daemonset-yaml');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;
  const cm_page = new ConfigPage();
  const secret_page = new SecretPage();
  const pvc_page = new StoragePage();
  const third_name = page.getTestData('l1-daemonset');
  beforeAll(() => {
    page.preparePage.delete(daemonset_name_base);
    page.preparePage.delete(daemonset_name_cm);
    page.preparePage.delete(daemonset_name_vol);
    page.preparePage.delete(daemonset_name_yaml);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
    pvc_page.prePareData.deletePvc(third_name, page.namespace1Name);
    const configmap_data = {
      name: third_name,
      namespace: namespace_name,
      datas: { 'l1-daemonset-cm': 'l1-daemonset-configmap' },
    };
    cm_page.prepareData.create(configmap_data);
    const secret_data = {
      name: third_name,
      namespace: namespace_name,
      datatype: 'Opaque',
      datas: {
        'l1-daemonset-sc': 'l1-daemonset-secret',
      },
    };
    secret_page.prepareData.create(secret_data);

    const pvc_data = {
      name: third_name,
      namespace: namespace_name,
    };
    pvc_page.prePareData.createPvcByYaml(pvc_data);

    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('守护进程集');
  });
  afterAll(() => {
    page.preparePage.delete(daemonset_name_base);
    page.preparePage.delete(daemonset_name_cm);
    page.preparePage.delete(daemonset_name_vol);
    page.preparePage.delete(daemonset_name_yaml);
    cm_page.prepareData.delete(third_name);
    secret_page.prepareData.delete(third_name);
    pvc_page.prePareData.deletePvc(third_name, page.namespace1Name);
  });
  it('ACP2UI-54813 : 创建守护进程集-输入必填项-组件标签-启动命令和参数-容器组标签-主机选择器-打开host网络模式-点击【确定】-创建成功', () => {
    const testData = {
      名称: daemonset_name_base,
      显示名称: daemonset_name_base,
      更新策略: [10],
      组件标签: [['deploylabel', 'deploylabel']],
      启动命令: [['/bin/sh']],
      参数: [['-c'], ['while true; do sleep 10; echo 123; done']],
      环境变量: [['env', 'env']],
      端口: [['TCP', '80', '']],
      // 容器组标签: [['podlabel', 'podlabel']],
      主机选择器: ['node-role.kubernetes.io/master:'],
      // 'Host 模式': 'true',
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      显示名称: daemonset_name_base,
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54026 : 计算-守护进程集-列表页-点击名称-进入守护进程集详情页', () => {
    page.listPage.enterDetail(daemonset_name_base);
    const expectDetailData = {
      显示名称: daemonset_name_base,
      // 容器组标签: 'podlabel:podlabel',
      主机选择器: 'node-role.kubernetes.io/master',
      更新策略: '滚动更新策略 ( 最多不可用数: 10 )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['/bin/sh'],
      参数: ['-c', 'while true; do sleep 10; echo 123; done'],
      环境变量: ['env'],
      YAML: {
        spec: {
          template: {
            spec: {
              containers: [
                {
                  ports: [
                    {
                      containerPort: 80,
                      protocol: 'TCP',
                    },
                  ],
                },
              ],
            },
          },
        },
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-54694 : 创建守护进程集-输入必填项-环境变量-配置引用-添加健康检查-添加反亲和-点击【确定】-创建成功', () => {
    const testData = {
      名称: daemonset_name_cm,
      更新策略: [10],
      存储卷: [
        {
          名称: 'daemonset-volume-cm',
          类型: '配置字典',
          配置字典: third_name,
        },
        {
          名称: 'daemonset-volume-secret',
          类型: '保密字典',
          保密字典: third_name,
        },
      ],
      主机选择器: ['node-role.kubernetes.io/node:'],
      亲和性: [
        {
          亲和性: 'Pod 反亲和',
          方式: '基本',
          亲和组件: ['守护进程集', daemonset_name_base],
        },
      ],
      环境变量添加引用: [
        ['envfromcm', ['配置字典', third_name], 'l1-daemonset-cm'],
        ['envfromsecret', ['保密字典', third_name], 'l1-daemonset-sc'],
      ],
      配置引用: [['配置字典', third_name], ['保密字典', third_name]],
      添加存活性健康检查: {
        协议类型: 'HTTP',
        启动时间: 10,
        间隔: 10,
      },
      添加可用性健康检查: {
        协议类型: 'TCP',
        启动时间: 10,
        间隔: 10,
        正常阈值: 1,
      },
      存储卷挂载: [
        [['配置字典', 'daemonset-volume-cm'], 'l1-daemonset-cm', '/test/cmsub'],
        [['配置字典', 'daemonset-volume-cm'], '', '/test/cm', true],
        [
          ['保密字典', 'daemonset-volume-secret'],
          'l1-daemonset-sc',
          '/test/secretsub',
        ],
        [['保密字典', 'daemonset-volume-secret'], '', '/test/secret'],
      ],
      端口: [['TCP', '80', '40070']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      存储卷: [
        'daemonset-volume-cm',
        '配置字典',
        third_name,
        'daemonset-volume-secret',
        '保密字典',
        third_name,
      ],
      'Pod 亲和': [
        'Pod 反亲和',
        'Required',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: daemonset-${daemonset_name_base}`,
      ],
      存活性健康检查: {
        健康检查类型: '存活性',
        协议类型: 'HTTP',
        启动时间: 10,
        间隔: 10,
        超时时长: 30,
        正常阈值: 1,
        不正常阈值: 5,
        协议: 'HTTP',
        端口: 80,
        路径: '/',
        请求头: '无请求头',
      },
      可用性健康检查: {
        健康检查类型: '可用性',
        协议类型: 'TCP',
        启动时间: 10,
        间隔: 10,
        超时时长: 30,
        正常阈值: 1,
        不正常阈值: 5,
        端口: 80,
      },
      已挂载存储卷: [
        'daemonset-volume-cm',
        'l1-daemonset-cm',
        '/test/cmsub',
        '/test/cm',
        'daemonset-volume-secret',
        'l1-daemonset-sc',
        '/test/secretsub',
        '/test/secret',
      ],
      环境变量: [
        'envfromcm',
        `配置字典 ${third_name} : l1-daemonset-cm`,
        'envfromsecret',
        `保密字典 ${third_name} : l1-daemonset-sc`,
      ],
      配置引用: ['配置字典', third_name, '保密字典', third_name],
      YAML: {
        spec: {
          template: {
            spec: {
              containers: [
                {
                  ports: [
                    { hostPort: 40070, containerPort: 80, protocol: 'TCP' },
                  ],
                },
              ],
            },
          },
        },
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-54695 : 创建守护进程集-输入必填项-存储卷-存储卷挂载-日志文件-添加容器-添加亲和性-点击【确定】-创建成功', () => {
    const testData = {
      名称: daemonset_name_vol,
      存储卷: [
        {
          名称: 'daemonset-volume-pvc',
          类型: '持久卷声明',
          持久卷声明: third_name,
        },
        {
          名称: 'daemonset-volume-path',
          类型: '主机路径',
          主机路径: '/tmp',
        },
        {
          名称: 'daemonset-volume-empty',
          类型: '空目录',
        },
      ],
      主机选择器: ['node-role.kubernetes.io/master:'],
      亲和性: [
        {
          亲和性: 'Pod 亲和',
          方式: '高级',
          亲和类型: 'Preferred',
          权重: 3,
          匹配标签: [
            [
              `service.${ServerConf.LABELBASEDOMAIN}/name`,
              `daemonset-${daemonset_name_base}`,
            ],
          ],
        },
      ],
      存储卷挂载: [
        [
          ['持久卷声明', 'daemonset-volume-pvc'],
          'l1-daemonset-pvc',
          '/test/pvcsub',
        ],
        [['持久卷声明', 'daemonset-volume-pvc'], '', '/test/pvc'],
        [['主机路径', 'daemonset-volume-path'], 'sub', '/test/path'],
        [['空目录', 'daemonset-volume-empty'], '', '/test/empty'],
      ],
      日志文件: [['/var/*.*']],
      排除日志文件: [['/var/hehe.txt']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54689 : 详情信息-亲和性，已挂载存储卷，健康检查，日志文件', () => {
    page.listPage.enterDetail(daemonset_name_vol);
    const expectDetailData = {
      存储卷: [
        'daemonset-volume-pvc',
        '持久卷声明',
        third_name,
        '主机路径',
        '/tmp',
        'daemonset-volume-empty',
        '空目录',
        '-',
      ],
      'Pod 亲和': [
        'Pod 亲和',
        'Preferred',
        '3',
        'kubernetes.io/hostname',
        `service.${ServerConf.LABELBASEDOMAIN}/name: daemonset-${daemonset_name_base}`,
      ],
      已挂载存储卷: [
        'daemonset-volume-pvc',
        'l1-daemonset-pvc',
        '/test/pvcsub',
        '/test/pvc',
        'daemonset-volume-path',
        '/test/path',
        'daemonset-volume-empty',
        '/test/empty',
      ],
      日志文件: ['日志文件', '/var/*.*', '排除日志文件', '/var/hehe.txt'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54098 : YAML创建守护进程集-输入正确的yaml格式内容-点击【创建】-守护进程集创建成功', () => {
    const yamlstring = CommonMethod.readyamlfile('alauda.daemonset.yaml', {
      '${NAME}': daemonset_name_yaml,
      '${NAMESPACE}': namespace_name,
      '${IMAGE}': imageAddress,
    });
    page.createPage.createByYaml(yamlstring);
    const expectDetailData = {
      面包屑: `计算/守护进程集/${daemonset_name_yaml}`,
      状态: '运行中',
      // 容器组标签: `app:${daemonset_name_yaml}`,
      主机选择器: '-',
      更新策略: '滚动更新策略 ( 最多不可用数: 1 )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  xit('ACP2UI-54687 : 详情信息-基本信息显示正确-更新容器组标签', () => {
    let testData = [['app', daemonset_name_yaml], ['podlabel', 'addlabel']];
    page.detailPage.updateLabel(daemonset_name_yaml, testData);
    let expectData = {
      容器组标签: `app:${daemonset_name_yaml};podlabel:addlabel`,
    };
    page.detailPageVerify.verify(expectData);
    testData = [['app', daemonset_name_yaml], ['podlabel', 'updatelabel']];
    page.detailPage.updateLabel(daemonset_name_yaml, testData);
    expectData = {
      容器组标签: `app:${daemonset_name_yaml};podlabel:updatelabel`,
    };
    page.detailPageVerify.verify(expectData);
    testData = [
      ['app', daemonset_name_yaml],
      ['podlabel', 'deletelabel', 'true'],
    ];
    page.detailPage.updateLabel(daemonset_name_yaml, testData);
    expectData = {
      容器组标签: `app:${daemonset_name_yaml}`,
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54688 : 详情信息-容器基本信息-更新镜像-更新资源限制', () => {
    page.detailPage.updateImage(daemonset_name_yaml, 'latest');
    const expectImageData = {
      镜像: imageAddress.replace('helloworld', 'latest'),
    };
    page.detailPageVerify.verify(expectImageData);
    const testData = [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }];
    page.detailPage.updateLimit(daemonset_name_yaml, testData);
    const expectData = {
      资源限制: 'CPU:20m内存:20Mi',
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54133 : 环境变量，点击更新，弹出更新环境变量页面', () => {
    const testData = {
      环境变量: [
        ['envfromcm', ['配置字典', third_name], 'l1-daemonset-cm', false],
        ['envfromsecret', ['保密字典', third_name], 'l1-daemonset-sc', false],
        ['addenv', 'addenv'],
      ],
    };
    page.detailPage.updateEnv(daemonset_name_cm, testData);
    const expectDetailData = {
      环境变量: ['addenv'],
    };
    page.detailPageVerify.verify(expectDetailData);
    const testData2 = {
      环境变量添加引用: [
        ['updateenvfromcm', ['配置字典', third_name], 'l1-daemonset-cm'],
        ['updateenvfromsecret', ['保密字典', third_name], 'l1-daemonset-sc'],
        ['updateenv', 'updateenv'],
      ],
    };
    page.detailPage.updateEnv(daemonset_name_cm, testData2);
    const expectDetailData2 = {
      环境变量: [
        'updateenv',
        'updateenvfromcm',
        `配置字典 ${third_name} : l1-daemonset-cm`,
        'updateenvfromsecret',
        `保密字典 ${third_name} : l1-daemonset-sc`,
      ],
    };
    page.detailPageVerify.verify(expectDetailData2);
  });
  it('ACP2UI-54134 : 配置引用，点击更新，弹出更新配置引用页面', () => {
    const testData = {
      删除配置引用: [`配置字典: ${third_name}`, `保密字典: ${third_name}`],
    };
    page.detailPage.updateConfig(daemonset_name_cm, testData);
    const expectDetailData = {
      无配置引用: 'true',
    };
    page.detailPageVerify.verify(expectDetailData);
    const testData2 = {
      配置引用: [['配置字典', third_name], ['保密字典', third_name]],
    };
    page.detailPage.updateConfig(daemonset_name_cm, testData2);
    const expectDetailData2 = {
      配置引用: ['配置字典', third_name, '保密字典', third_name],
    };
    page.detailPageVerify.verify(expectDetailData2);
  });
  it('ACP2UI-54118 : 详情-日志-容器组/容器信息显示正确', () => {
    page.listPage.enterDetail(daemonset_name_base);
    const expectDetailData = {
      日志: '123',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  // 经常报错请求冲突，先注释
  // it('ACP2UI-54121 : 详情信息-点击【更新】-进入到更新守护进程集页面', () => {
  //   const testData = {
  //     显示名称: daemonset_name_yaml,
  //     更新策略: [20],
  //     镜像地址: imageAddress,
  //     资源限制: [{ CPU: 10, 单位: 'm' }, { 内存: 10, 单位: 'Mi' }],
  //     // 环境变量: [['env', 'env']],
  //   };
  //   page.detailPage.update(daemonset_name_yaml, testData);
  //   const expectDetailData = {
  //     状态: '运行中',
  //     显示名称: daemonset_name_yaml,
  //     更新策略: '滚动更新策略 ( 最多不可用数: 20 )',
  //     镜像: imageAddress,
  //     资源限制: 'CPU:10m内存:10Mi',
  //     // 环境变量: ['env'],
  //   };
  //   page.detailPageVerify.verify(expectDetailData);
  // });
  it('ACP2UI-54122 : 详情信息-点击【删除】-页面弹出删除守护进程集确定框', () => {
    page.detailPage.delete(daemonset_name_yaml, 'cancel');
    const expectDetailData = {
      显示名称: daemonset_name_yaml,
    };
    page.detailPageVerify.verify(expectDetailData);
    page.detailPage.delete(daemonset_name_yaml);
    page.listPage.search(daemonset_name_yaml, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });

  it('ACP2UI-54027 : 计算-守护进程集-列表页-操作-点击【更新】-进入守护进程集更新页', () => {
    const testData = {
      显示名称: daemonset_name_base,
      更新策略: [20],
    };
    page.listPage.update(daemonset_name_base, testData);
    const expectDetailData = {
      状态: '运行中',
      更新策略: '滚动更新策略 ( 最多不可用数: 20 )',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54034 : 计算-守护进程集-列表页-操作-点击【删除】-守护进程集删除成功', () => {
    page.listPage.delete(daemonset_name_vol, 'cancel');
    page.listPage.search(daemonset_name_vol);
    let expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);
    page.detailPage.delete(daemonset_name_vol);
    page.listPage.search(daemonset_name_vol, 0);
    expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-54029 : 计算-守护进程集-列表页-搜索框里输入名称(精确/模糊/大小写)-信息显示正确', () => {
    page.listPage.search(daemonset_name_base.toUpperCase());
    let expectData = { 数量: 1 };
    page.listPageVerify.verify(expectData);
    page.listPage.search(daemonset_name_base.replace('base', 'BASE'));
    page.listPageVerify.verify(expectData);
    page.listPage.search(daemonset_name_base.substring(2));
    page.listPageVerify.verify(expectData);
    page.listPage.search(daemonset_name_base.concat('notexist'), 0);
    expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
