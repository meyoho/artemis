import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署L0自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('deployment');
  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name);
  });
  it('ACP2UI-53894 : L0:创建部署-输入必填项-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name,
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      面包屑: `计算/部署/${deployment_name}`,
      主机选择器: '-',
      更新策略: '滚动更新策略 ( 最大可超出数: 25%, 最多不可用数: 25% )',
      镜像: imageAddress,
      资源限制: 'CPU:10m内存:10Mi',
      启动命令: ['-'],
      参数: ['-'],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-54454 : 计算-部署-列表页字段验证', () => {
    page.listPage.waitStatus(deployment_name, '运行中');
    const expectCreateData = {
      标题: '计算/部署',
      header: ['名称', '状态', '所属应用', '创建时间'],
      数量: 1,
      创建: {
        名称: deployment_name,
        状态: '运行中(1/1)',
        所属应用: '-',
      },
    };
    page.listPageVerify.verify(expectCreateData);
  });
  it('ACP2UI-54035 : 计算-部署-列表页-操作-点击【删除】-部署删除成功', () => {
    page.listPage.delete(deployment_name);
    // 验证搜索正确
    page.listPage.search(deployment_name, 0);
    const expectData = { 数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
