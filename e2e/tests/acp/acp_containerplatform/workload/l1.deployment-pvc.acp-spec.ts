import { ServerConf } from '@e2e/config/serverConf';
import { StoragePage } from '@e2e/page_objects/acp/container/storage/storage.page';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署L1自动化', () => {
  const page = new DeploymentPage();

  const deployment_name_vol = page.getTestData('l1-deploy-vol');

  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  const pvc_page = new StoragePage();
  const third_name = page.getTestData('l1-deploy');
  beforeAll(() => {
    page.preparePage.delete(deployment_name_vol);
    pvc_page.prePareData.deletePvc(third_name, namespace_name);
    const pvc_data = {
      name: third_name,
      namespace: namespace_name,
    };
    pvc_page.prePareData.createPvcByYaml(pvc_data);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name_vol);
    pvc_page.prePareData.deletePvc(third_name, namespace_name);
  });

  it('ACP2UI-53917 : 创建部署-输入必填项-（容器高级）存储卷挂载pvc/host/empty-多个端口-（容器组高级）存储卷-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name_vol,
      资源限制: [{ CPU: 20, 单位: 'm' }, { 内存: 20, 单位: 'Mi' }],
      存储卷: [
        {
          名称: 'deploy-volume-pvc',
          类型: '持久卷声明',
          持久卷声明: third_name,
        },
        {
          名称: 'deploy-volume-path',
          类型: '主机路径',
          主机路径: '/tmp',
        },
        {
          名称: 'deploy-volume-empty',
          类型: '空目录',
        },
      ],
      存储卷挂载: [
        [['持久卷声明', 'deploy-volume-pvc'], 'l1-deploy-pvc', '/test/pvcsub'],
        [['持久卷声明', 'deploy-volume-pvc'], '', '/test/pvc'],
        [['主机路径', 'deploy-volume-path'], 'sub', '/test/path'],
        [['空目录', 'deploy-volume-empty'], '', '/test/empty'],
      ],
      端口: [['TCP', '80', '40081'], ['UDP', '81', '40082']],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-53958 : 详情信息-已挂载存储卷', () => {
    page.listPage.enterDetail(deployment_name_vol);
    const expectDetailData = {
      存储卷: [
        'deploy-volume-pvc',
        '持久卷声明',
        third_name,
        '主机路径',
        '/tmp',
        'deploy-volume-empty',
        '空目录',
        '-',
      ],
      已挂载存储卷: [
        'deploy-volume-pvc',
        'l1-deploy-pvc',
        '/test/pvcsub',
        '/test/pvc',
        'deploy-volume-path',
        '/test/path',
        'deploy-volume-empty',
        '/test/empty',
      ],
      端口: ['TCP', '80', '40081', 'UDP', '81', '40082'],
      YAML: {
        spec: {
          template: {
            spec: {
              containers: [
                {
                  ports: [
                    {
                      hostPort: 40081,
                      containerPort: 80,
                      protocol: 'TCP',
                    },
                    {
                      hostPort: 40082,
                      containerPort: 81,
                      protocol: 'UDP',
                    },
                  ],
                },
              ],
            },
          },
        },
      },
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
