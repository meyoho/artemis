import { ServerConf } from '@e2e/config/serverConf';
import { DeploymentPage } from '@e2e/page_objects/acp/deployment/deployment.page';

describe('用户视图 部署fieldRefL1自动化', () => {
  const page = new DeploymentPage();
  const deployment_name = page.getTestData('fieldref');

  const namespace_name = page.namespace1Name;
  const imageAddress = ServerConf.TESTIMAGE;

  beforeAll(() => {
    page.preparePage.delete(deployment_name);
    page.login();
    page.enterUserView(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('部署');
  });
  afterAll(() => {
    page.preparePage.delete(deployment_name);
  });

  it('ACP2UI-58310 : 创建部署-输入必填项-（容器高级）环境变量fieldRef resourceFieldRef-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name,
      环境变量添加引用: [
        ['name', ['fieldRef', ''], 'metadata.name'],
        ['namespace', ['fieldRef', ''], 'metadata.namespace'],
        ['nodename', ['fieldRef', ''], 'spec.nodeName'],
        ['saname', ['fieldRef', ''], 'spec.serviceAccountName'],
        ['limits.cpu', ['resourceFieldRef', ''], 'limits.cpu'],
        ['limits.memory', ['resourceFieldRef', ''], 'limits.memory'],
      ],
    };
    page.createPage.create(testData, imageAddress);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      环境变量: [
        'name',
        'fieldRef: metadata.name',
        'namespace',
        'fieldRef: metadata.namespace',
        'nodename',
        'fieldRef: spec.nodeName',
        'saname',
        'fieldRef: spec.serviceAccountName',
        'resourceFieldRef: limits.cpu',
        'resourceFieldRef: limits.memory',
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
  it('ACP2UI-58311 : 更新部署-（容器高级）环境变量fieldRef resourceFieldRef-点击【确定】-创建成功', () => {
    const testData = {
      名称: deployment_name,
      环境变量添加引用: [
        ['updatename', ['fieldRef', ''], 'metadata.name'],
        ['updatenamespace', ['fieldRef', ''], 'metadata.namespace'],
        ['nodename', ['fieldRef', ''], 'spec.nodeName'],
        ['updatesaname', ['fieldRef', ''], 'spec.serviceAccountName'],
        ['updatelimits.cpu', ['resourceFieldRef', ''], 'limits.cpu'],
        ['limits.memory', ['resourceFieldRef', ''], 'limits.memory', true],
      ],
    };
    page.listPage.update(deployment_name, testData);
    const expectDetailData = {
      状态: '运行中',
      实例数: '1',
      环境变量: [
        'updatename',
        'fieldRef: metadata.name',
        'updatenamespace',
        'fieldRef: metadata.namespace',
        'nodename',
        'fieldRef: spec.nodeName',
        'updatesaname',
        'fieldRef: spec.serviceAccountName',
        'updatelimits.cpu',
        'resourceFieldRef: limits.cpu',
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });

  it('ACP2UI-58312 : 环境变量，点击更新，弹出更新环境变量页面 fieldRef resourceFieldRef', () => {
    const testData = {
      环境变量添加引用: [
        ['name', ['fieldRef', ''], 'metadata.name'],
        ['namespace', ['fieldRef', ''], 'metadata.namespace'],
        ['nodename', ['fieldRef', ''], 'spec.nodeName'],
        ['saname', ['fieldRef', ''], 'spec.serviceAccountName'],
        ['hostip', ['fieldRef', ''], 'status.hostIP'],
        ['podip', ['fieldRef', ''], 'status.podIP'],
        ['limits.cpu', ['resourceFieldRef', ''], 'limits.cpu'],
        ['limits.memory', ['resourceFieldRef', ''], 'limits.memory'],
        ['requests.cpu', ['resourceFieldRef', ''], 'requests.cpu'],
        ['requests.memory', ['resourceFieldRef', ''], 'requests.memory'],
      ],
    };
    page.detailPage.updateEnv(deployment_name, testData);
    const expectDetailData = {
      环境变量: [
        'name',
        'fieldRef: metadata.name',
        'namespace',
        'fieldRef: metadata.namespace',
        'nodename',
        'fieldRef: spec.nodeName',
        'saname',
        'fieldRef: spec.serviceAccountName',
        'hostip',
        'fieldRef: status.hostIP',
        'podip',
        'fieldRef: status.podIP',
        'resourceFieldRef: limits.cpu',
        'resourceFieldRef: limits.memory',
        'resourceFieldRef: requests.cpu',
        'resourceFieldRef: requests.memory',
      ],
    };
    page.detailPageVerify.verify(expectDetailData);
  });
});
