// /**
//  * Created by zhangjiao on 2020/2/14.
//  */

// import { browser } from 'protractor';

// import { ServerConf } from '../../../config/serverConf';
// import { UserPage } from '../../../page_objects/platform/user/user.page';

// const page = new UserPage();
// const ldapIsReady = ServerConf.LDAP_ISREADY;
// const localusername = ServerConf.ADMIN_USER;
// const localpassword = ServerConf.ADMIN_PASSWORD;
// const ldapusername = ServerConf.LDAP_USER;
// const ldappassword = ServerConf.LDAP_PASSWORD;
// const products = ['Service Mesh', 'DevOps', 'Container Platform', 'platform'];

// describe('用户登录L0级别case', () => {
//   beforeAll(() => {
//     browser.get(`${ServerConf.BASE_URL}/console-acp/`);
//   });

//   beforeEach(() => {});

//   afterAll(() => {});
//   it('ACP2UI-4158 : 【本地用户】登录页面-选择管理员登录-输入用户名-输入密码-登录成功-页面跳转至项目管理视图', () => {
//     page.localUserLogin(localusername, localpassword);
//     page.goToChinese('acp');

//     products.forEach(key => {
//       if (key === 'platform') {
//         page.acpAccountMenu.select('项目管理');
//         page.switchWindow(1);
//       } else {
//         page.projects.select(key);
//       }
//       page.verifyHomePageLoginUsername(key).then(name => {
//         expect(name).toBe(localusername);
//       });
//       if (key === 'platform') {
//         browser.close();
//       }
//       page.switchWindow(0);
//     });
//   });
//   it('ACP2UI-4163 : 【本地用户】退出登录-点击头像操作-退出登录-登出成功-页面跳转至用户登录页', () => {
//     expect(page.logout()).toContain('dex/auth');
//   });
//   it('ACP2UI-4165 : 【ldap用户】登录页面-选择ldap-输入用户名-输入密码-点击登录-登录成功', () => {
//     if (ldapIsReady === 'true') {
//       browser.get(`${ServerConf.BASE_URL}/console-acp/`);
//       browser.sleep(500);
//       page.ldapUserLogin(ldapusername, ldappassword);

//       products.forEach(key => {
//         if (key === 'platform') {
//           page.acpAccountMenu.select('项目管理');
//           page.switchWindow(1);
//         } else {
//           page.projects.select(key);
//         }
//         page.verifyHomePageLoginUsername(key).then(name => {
//           expect(name).toBe(ldapusername);
//         });
//         if (key === 'platform') {
//           browser.close();
//         }
//         page.switchWindow(0);
//       });
//     } else {
//       console.log('Ldap不可用，不执行使用Ldap用户登陆');
//     }
//   });
//   it('ACP2UI-4177 : 【ldap用户】退出登录-点击头像操作-退出登录-页面跳转至登录页', () => {
//     if (ldapIsReady === 'true') {
//       expect(page.logout()).toContain('dex/auth');
//     } else {
//       console.log('Ldap不可用，不执行使用Ldap用户退出登陆');
//     }
//   });
//   // it('ACP2UI-4179 : 【oidc用户】登录页面-选择oidc-打开第三方登录页面-输入认证信息-登录-页面跳转回首页', () => {
//   //   browser.get(`${ServerConf.BASE_URL}/console-asm/`);
//   //   page.keycloakUserLogin(keycloakusername, keycloakpassword);

//   //   products.forEach(key => {
//   //     if (products[key] === 'platform') {
//   //       page.acpAccountMenu.select('项目管理');
//   //       page.switchWindow(1);
//   //     } else {
//   //       page.projects.select(products[key]);
//   //     }
//   //     page.verifyHomePageLoginUsername(products[key]).then(name => {
//   //       expect(name).toBe(keycloakusername);
//   //     });
//   //     if (products[key] === 'platform') {
//   //       browser.close();
//   //     }
//   //     page.switchWindow(0);
//   //   });
//   // });
//   // it('ACP2UI-4180 : 【oidc用户】退出登录-点击头像操作-退出登录-页面跳转至登录页', () => {
//   //   expect(page.logout()).toContain('dex/auth');
//   // });
//   it('ACP2UI-53608 : 【用户登录】【用户列表】第三方用户邮箱包含特殊字符，大写字母等-使用用户登录平台-退出登录-使用平台管理员登录-进入用户列表-查看用户', () => {});
// });
