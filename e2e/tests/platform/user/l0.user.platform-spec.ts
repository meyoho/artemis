/**
 * Created by zhangjiao on 2020/2/12.
 */

import { UserPage } from '@e2e/page_objects/platform/user/user.page';
import { ServerConf } from '@e2e/config/serverConf';
import { browser } from 'protractor';

describe('用户管理L0用例', () => {
  const page = new UserPage();
  const ldapIsReady = ServerConf.LDAP_ISREADY;
  let useName = '';
  const email = 'qa@alauda.io';
  const lastName = 'test';
  beforeAll(() => {
    page.preparePage.deleteUser(email);
    if (ldapIsReady === 'true') {
      useName = page.preparePage.createLdapUser(email, lastName);
    }
    page.login();
    page.enterPlatformView('平台管理');
    page.clickLeftNavByText('用户角色管理');
  });

  beforeEach(() => {
    page.clickLeftNavByText('用户管理');
  });
  afterAll(() => {
    page.preparePage.deleteUser(email);
  });

  it('ACP2UI-4182 : 【增加用户】ldap中添加用户-用户管理页-同步用户-用户列表展示ldap中新增的用户', () => {
    if (ldapIsReady === 'true') {
      page.listPage.syncUser();
      page.listPage.searchUser(email, 1);
      const expectData = { 状态: '有效', 来源: 'ldap' };
      page.listPageVerify.verify(expectData);
    } else {
      console.log('Ldap不可用');
    }
  });

  it('ACP2UI-4365 : 【添加角色】用户列表页-操作-管理角色-选择平台级角色-点击添加-角色添加成功', () => {
    if (ldapIsReady === 'true') {
      page.listPage.enterDetail(email);
      page.addRole('acp-platform-admin (平台管理员)');
      page.detaiPage.rolesTable.waitRowCountChangeto(1);
      page.detaiPage.rolesTable.getRowCount().then(count => {
        expect(count).toBe(1);
      });
    } else {
      console.log('Ldap不可用');
    }
  });
  it('ACP2UI-4375 : 【删除角色】用户列表页-用户操作-角色管理-点击角色右侧垃圾桶-提示：是否删除角色-确定-用户角色删除', () => {
    if (ldapIsReady === 'true') {
      page.listPage.enterDetail(email);
      page.deleteRole('acp-platform-admin');
      page.detaiPage.rolesTable.waitRowCountChangeto(0);
    } else {
      console.log('Ldap不可用');
    }
  });
  it('ACP2UI-111 : 【删除用户】用户管理页-用户列表删除用户', () => {
    if (ldapIsReady === 'true') {
      page.listPage.deleteUser(email); //删除用户
      page.listPage.searchUser(email, 0); // 验证用户删除
    } else {
      console.log('Ldap不可用');
    }
  });

  it('ACP2UI-4197 : 【删除用户】ldap同步-ldap中删除的用户在平台上变为无效状态-用户列表页-点击用户操作-点击“清理用户”-提示是否清理用户-确定-清理成功', () => {
    if (ldapIsReady === 'true') {
      page.listPage.syncUser(); // 同步用户;
      browser.sleep(50);
      page.preparePage.deleteLdapUser(useName); // ldap 删除用户
      browser.sleep(1000);

      browser.navigate().to(browser.baseUrl);
      page.switchSetting('平台管理');
      page.clickLeftNavByText('用户管理');

      page.listPage.syncUser(); // 同步用户;
      //验证用户状态无效
      page.listPage.searchUser(email, 1);
      const expectData = { 状态: '无效' };
      page.listPageVerify.verify(expectData);

      page.listPage.clear([email]); //清理无效用户

      // 验证用户删除
      page.listPage.searchUser(email, 0);
      const expectData1 = { 数量: 0 };
      page.listPageVerify.verify(expectData1);
    } else {
      console.log('Ldap不可用');
    }
  });
});
