/**
 * Created by zhangjiao on 2018/3/20.
 */

import { $ } from 'protractor';

import { UserPage } from '../../../page_objects/platform/user/user.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';

const page = new UserPage();
const localusername = ServerConf.ADMIN_USER;

describe('用户管理L1用例', () => {
  beforeAll(() => {
    page.login();
    page.enterPlatformView('平台管理');
    page.clickLeftNavByText('用户角色管理');
  });

  beforeEach(() => {
    page.clickLeftNavByText('用户管理');
  });

  afterAll(() => {
    CommonKubectl.deleteResource('clusterroles', 'platformclusterrole');
    CommonKubectl.deleteResource(
      'clusterrolebindings',
      'platformclusterrolebindings',
    );
  });
  it('ACP2UI-4186 : 【搜索用户】进入用户列表-选择“用户名”-输入用户名-点击搜索-搜索出所有来源中符合条件的数据', () => {
    page.searchUser(localusername);
    // const expectData = { 状态: '有效', 来源: 'local' };  //有时候获取到来源是 -
    const expectData = { 状态: '有效' };
    page.listPageVerify.verify(expectData);
  });
  // it('ACP2UI-4183 : 【增加用户】oidc新增用户-使用新增用户登录平台-使用管理员查看用户列表-用户列表中有新增的oidc的用户', () => {
  //   page.searchUser(keycloakusername);
  //   const expectData = { 状态: '有效', 来源: 'oidc' };
  //   page.listPageVerify.verify(expectData);
  // });
  it('ACP2UI-4190|ACP2UI-4191 : 【个人信息】点击头像中操作-个人信息-进入个人信息页面 | 查看token', () => {
    page.accountMenu.select('个人信息');
    expect(page.detaiPage.getElementByText('显示名称').getText()).toBe('admin');
    page.getButtonByText('平台 token').click();
    page.waitElementPresent($('.input input'), 'token按钮未出现');
  });
});
