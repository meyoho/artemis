import { browser } from 'protractor';
import { FederatedNsPage } from '@e2e/page_objects/platform/federatedns/federatedns.page';
import { ServerConf } from '@e2e/config/serverConf';
import { ProjectPage } from '@e2e/page_objects/platform/project/project.page';

describe('用户视图 联邦命名空间 L1自动化', () => {
  const page = new FederatedNsPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-l1';
  const region_name = ServerConf.FEDERATIONREGIONNAME;
  const detail = page.preparePage.fedRegionDetail();
  const fed_all_name = detail.All;
  const fed_host_name = detail.Host;
  const fed_member_name = detail.Member;
  const project_page = new ProjectPage();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.deleteFederatedResource(
      'federatednamespace',
      namespace_name,
      namespace_name,
    );
    page.preparePage.deleteFederatedResource(
      'namespace',
      namespace_name,
      namespace_name,
    );
    project_page.login();
    project_page.switchSetting('项目管理');
    project_page.listPage.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦命名空间');
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatednamespace',
      namespace_name,
      namespace_name,
    );
    page.preparePage.deleteFederatedResource(
      'namespace',
      namespace_name,
      namespace_name,
    );
  });
  it('ACP2UI-57083 : 点击左导航联邦命名空间-点击创建联邦命名空间-选择联邦集群-输入联邦命名空间名称-输入资源配置-输入容器限额-点击取消按钮', () => {
    const data = {
      联邦集群: region_name,
      名称: 'l1',
      差异化配置: true,
      资源配额: [],
      容器限额: [],
    };
    fed_all_name.forEach(region => {
      data.资源配额.push({
        集群: region,
        CPU: '1',
        内存: '1',
        存储: '1',
        'Pods 数': '1',
        'PVC 数': '1',
      });
      data.容器限额.push({
        集群: region,
        CPU: {
          默认值: '5',
          最大值: '5',
        },
        内存: {
          默认值: '10',
          最大值: '10',
        },
      });
    });
    page.listPage.create(data, 'cancel');
    page.listPage.search(namespace_name, 0);
    const expectNoData = { 数量: 0 };
    page.listVerify.verify(expectNoData);
  });
  it('ACP2UI-58055|ACP2UI-57092 : 点击左导航联邦命名空间-点击创建联邦命名空间-选择联邦集群-输入联邦命名空间名称-输入显示名称-差异化配置按钮开启-有几个联邦化的集群就展示几个区域，分别配置各自配额和限额信息-带出项目上配置的各集群', () => {
    const data = {
      联邦集群: region_name,
      名称: 'l1',
      差异化配置: true,
      资源配额: [],
      容器限额: [],
    };
    fed_all_name.forEach(region => {
      data.资源配额.push({
        集群: region,
        CPU: '1',
        内存: '1',
        存储: '1',
        'Pods 数': '1',
        'PVC 数': '1',
      });
      data.容器限额.push({
        集群: region,
        CPU: {
          默认值: '5',
          最大值: '5',
        },
        内存: {
          默认值: '10',
          最大值: '10',
        },
      });
    });
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'limitrange',
        'default',
        namespace_name,
        true,
      );
      page.preparePage.waitResourceExist(
        'resourcequota',
        'default',
        namespace_name,
        true,
      );
      browser.refresh();
      const v_Data = {
        面包屑: `联邦命名空间/${namespace_name}`,
        名称: namespace_name,
        显示名称: '-',
        所属联邦集群: region_name,
        容器限额: [],
        资源配额: [],
      };
      fed_all_name.forEach(region => {
        v_Data.容器限额.push({
          集群: region,
          cpu: {
            默认值: '5m',
            最大值: '5m',
          },
          memory: {
            默认值: '10Mi',
            最大值: '10Mi',
          },
        });
        v_Data.资源配额.push({
          集群: region,
          CPU: '1 核',
          内存: '1 Gi',
          存储: '1 Gi',
          'Pods 数': '1 个',
          'PVC 数': '1 个',
        });
      });
      page.detailVerify.verify(v_Data);
    });
  });
  it('ACP2UI-57102 : 点击左导航联邦命名空间-进入联邦命名空间列表-点击命名空间名称-进入详情页-点击显示名称的编译图标-更新显示名称-点击取消-更新取消', () => {
    const data = { 显示名称: 'updatedisplayname' };
    page.detailPage.updatedisplayname(namespace_name, data, 'cancel');
    const v_data = { 显示名称: '-' };
    page.detailVerify.verify(v_data);
  });
  it('ACP2UI-57089|ACP2UI-57101 : 点击左导航联邦命名空间-进入联邦命名空间列表-点击命名空间名称-进入详情页-显示详情信息-更新显示名称', () => {
    const data = { 显示名称: 'updatedisplayname' };
    page.detailPage.updatedisplayname(namespace_name, data);
    page.detailVerify.verify(data);
  });
  it('ACP2UI-57087 : 点击左导航联邦命名空间-选择联邦命名空间点击操作-更新联邦命名空间-选择集群名称-更新限额/配额-点击取消按钮，更新取消', () => {
    const data = {
      差异化配置: true,
      资源配额: [],
      容器限额: [],
    };
    data.资源配额.push({
      集群: fed_host_name,
      CPU: '2',
      内存: '2',
      存储: '2',
      'Pods 数': '2',
      'PVC 数': '2',
    });
    data.容器限额.push({
      集群: fed_host_name,
      CPU: {
        默认值: '20',
        最大值: '1000',
      },
      内存: {
        默认值: '20',
        最大值: '1024',
      },
    });
    page.detailPage.update(namespace_name, data, 'cancel');
    page.listPage.enterDetail(namespace_name);
    const v_Data = {
      容器限额: [],
      资源配额: [],
    };
    v_Data.容器限额.push({
      集群: fed_host_name,
      cpu: {
        默认值: '5m',
        最大值: '5m',
      },
      memory: {
        默认值: '10Mi',
        最大值: '10Mi',
      },
    });
    v_Data.资源配额.push({
      集群: fed_host_name,
      CPU: '1 核',
      内存: '1 Gi',
      存储: '1 Gi',
      'Pods 数': '1 个',
      'PVC 数': '1 个',
    });
    page.detailVerify.verify(v_Data);
  });
  it('ACP2UI-57100 : 点击左导航联邦命名空间-进入联邦命名空间列表-点击命名空间名称-进入详情页-操作-删除-弹出删除确认框-输入联邦命名空间名称-点击取消-删除取消', () => {
    page.detailPage.delete(namespace_name, 'cancel');
    page.listPage.search(namespace_name, 1);
    const expectData = { 数量: 1 };
    page.listVerify.verify(expectData);
  });
  it('ACP2UI-57095|ACP2UI-58056: 点击左导航联邦命名空间-进入联邦命名空间列表-点击命名空间名称-进入详情页-操作-更新资源配额/容器限额', () => {
    const data = {
      差异化配置: true,
      资源配额: [],
      容器限额: [],
    };
    data.资源配额.push({
      集群: fed_host_name,
      CPU: '2',
      内存: '2',
      存储: '2',
      'Pods 数': '2',
      'PVC 数': '2',
    });
    data.容器限额.push({
      集群: fed_host_name,
      CPU: {
        默认值: '20',
        最大值: '1000',
      },
      内存: {
        默认值: '20',
        最大值: '1024',
      },
    });
    page.detailPage.update(namespace_name, data);
    const v_Data = {
      资源配额: [],
      容器限额: [],
    };
    v_Data.资源配额.push({
      集群: fed_host_name,
      CPU: '2',
      内存: '2',
      存储: '2',
      'Pods 数': '2',
      'PVC 数': '2',
    });
    fed_member_name.forEach(region => {
      v_Data.资源配额.push({
        集群: region,
        CPU: '1',
        内存: '1',
        存储: '1',
        'Pods 数': '1',
        'PVC 数': '1',
      });
    });
    v_Data.容器限额.push({
      集群: fed_host_name,
      cpu: {
        默认值: '20m',
        最大值: '1 核',
      },
      memory: {
        默认值: '20Mi',
        最大值: '1Gi',
      },
    });
    fed_member_name.forEach(region => {
      v_Data.容器限额.push({
        集群: region,
        cpu: {
          默认值: '5m',
          最大值: '5m',
        },
        memory: {
          默认值: '10Mi',
          最大值: '10Mi',
        },
      });
    });
    page.detailVerify.verify(v_Data);
  });
  it('ACP2UI-58057 : 点击左导航联邦命名空间-选择联邦命名空间点击操作-更新联邦命名空间-差异化配置开启更新为关闭-显示主集群下命名空间的【资源配额】和【容器限额】', () => {
    const data = {
      差异化配置: false,
    };
    page.listPage.update(namespace_name, data);
    const v_Data = {
      资源配额: [],
      容器限额: [],
    };
    fed_all_name.forEach(region => {
      v_Data.资源配额.push({
        集群: region,
        CPU: '2',
        内存: '2',
        存储: '2',
        'Pods 数': '2',
        'PVC 数': '2',
      });
      v_Data.容器限额.push({
        集群: region,
        cpu: {
          默认值: '20m',
          最大值: '1 核',
        },
        memory: {
          默认值: '20Mi',
          最大值: '1Gi',
        },
      });
    });
    page.detailVerify.verify(v_Data);
  });
});
