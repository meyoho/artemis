import { browser } from 'protractor';
import { FederatedNsPage } from '@e2e/page_objects/platform/federatedns/federatedns.page';
import { ServerConf } from '@e2e/config/serverConf';
import { ProjectPage } from '@e2e/page_objects/platform/project/project.page';

describe('用户视图 联邦命名空间 L0自动化', () => {
  const page = new FederatedNsPage();
  if (page.checkRegionNotExist(ServerConf.FEDERATIONREGIONNAME, 'clusterfed')) {
    return;
  }
  if (!page.preparePage.kubefedIsEnabled) {
    return;
  }
  const project_name = 'uiauto-acp-fed';
  const namespace_name = 'uiauto-acp-fed-l0';
  const region_name = ServerConf.FEDERATIONREGIONNAME;
  const detail = page.preparePage.fedRegionDetail();
  const fed_all_name = detail.All;
  const project_page = new ProjectPage();

  beforeAll(() => {
    page.preparePage.createFederatedPro(project_name);
    page.preparePage.deleteFederatedResource(
      'federatednamespace',
      namespace_name,
      namespace_name,
    );
    page.preparePage.deleteFederatedResource(
      'namespace',
      namespace_name,
      namespace_name,
    );
    project_page.login();
    project_page.switchSetting('项目管理');
    project_page.listPage.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('联邦命名空间');
    browser.sleep(1000);
  });
  afterAll(() => {
    page.preparePage.deleteFederatedResource(
      'federatednamespace',
      namespace_name,
      namespace_name,
    );
    page.preparePage.deleteFederatedResource(
      'namespace',
      namespace_name,
      namespace_name,
    );
  });
  it('ACP2UI-57081|ACP2UI-57092 : L0:点击左导航联邦命名空间-点击创建联邦命名空间-选择联邦集群-输入联邦命名空间名称-输入显示名称-差异化配置按钮默认关闭-输入资源配置-输入容器限额-点击创建按钮-联邦命名空间创建成功', () => {
    const data = {
      联邦集群: region_name,
      名称: 'l0',
      差异化配置: false,
      资源配额: [
        {
          CPU: '10',
          内存: '10',
          存储: '10',
          'Pods 数': '10',
          'PVC 数': '10',
        },
      ],
      容器限额: [
        {
          CPU: {
            默认值: '5',
            最大值: '5',
          },
          内存: {
            默认值: '10',
            最大值: '10',
          },
        },
      ],
    };
    page.listPage.create(data);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'limitrange',
        'default',
        namespace_name,
        true,
      );
      page.preparePage.waitResourceExist(
        'resourcequota',
        'default',
        namespace_name,
        true,
      );
      browser.refresh();
      browser.sleep(1000);
      const v_Data = {
        面包屑: `联邦命名空间/${namespace_name}`,
        名称: namespace_name,
        显示名称: '-',
        所属联邦集群: region_name,
        容器限额: [],
        资源配额: [],
      };
      fed_all_name.forEach(region => {
        v_Data.容器限额.push({
          集群: region,
          cpu: {
            默认值: '5m',
            最大值: '5m',
          },
          memory: {
            默认值: '10Mi',
            最大值: '10Mi',
          },
        });
        v_Data.资源配额.push({
          集群: region,
          CPU: '10',
          内存: '10',
          存储: '10',
          'Pods 数': '10',
          'PVC 数': '10',
        });
      });
      page.detailVerify.verify(v_Data);
    });
  });
  it('ACP2UI-57041 : 点击左导航联邦命名空间-选择联邦命名空间点击操作-更新联邦命名空间-选择集群名称-更新配额-点击更新按钮，更新成功', () => {
    const data = {
      差异化配置: false,
      资源配额: [
        {
          CPU: '1',
          内存: '1',
          存储: '1',
          'Pods 数': '1',
          'PVC 数': '1',
        },
      ],
    };
    page.listPage.update(namespace_name, data);
    browser.sleep(100);
    const v_Data = {
      资源配额: [],
    };
    fed_all_name.forEach(region => {
      v_Data.资源配额.push({
        集群: region,
        CPU: '1',
        内存: '1',
        存储: '1',
        'Pods 数': '1',
        'PVC 数': '1',
      });
    });
    page.detailVerify.verify(v_Data);
  });
  it('ACP2UI-57086 : 点击左导航联邦命名空间-选择联邦命名空间点击操作-更新联邦命名空间-选择集群名称-更新限额-点击更新按钮，更新成功', () => {
    const data = {
      差异化配置: false,
      容器限额: [
        {
          CPU: {
            默认值: '20',
            最大值: '1000',
          },
          内存: {
            默认值: '20',
            最大值: '1024',
          },
        },
      ],
    };
    page.listPage.update(namespace_name, data);
    const v_Data = {
      容器限额: [],
    };
    fed_all_name.forEach(region => {
      v_Data.容器限额.push({
        集群: region,
        cpu: {
          默认值: '20m',
          最大值: '1 核',
        },
        memory: {
          默认值: '20Mi',
          最大值: '1Gi',
        },
      });
    });
    page.detailVerify.verify(v_Data);
  });
  it('ACP2UI-57040 : 点击左导航联邦命名空间-进入列表：按名称搜索(不存在、已存在、模糊、大小写)', () => {
    page.listPage.search(namespace_name, 1);
    const expectData = {
      标题: '联邦命名空间',
      header: ['名称', '所属集群', '创建时间'],
      数量: 1,
      创建: {
        名称: namespace_name,
        联邦集群: region_name,
      },
    };
    page.listVerify.verify(expectData);
    page.listPage.search(namespace_name.replace('l0', 'L0'));
    page.listVerify.verify(expectData);
    page.listPage.search(namespace_name.substring(2));
    page.listVerify.verify(expectData);
    page.listPage.search(namespace_name.concat('notexist'), 0);
    const expectNoData = { 数量: 0 };
    page.listVerify.verify(expectNoData);
  });
  it('ACP2UI-57098 : L0:点击左导航联邦命名空间-进入联邦命名空间列表-点击命名空间名称-进入详情页-操作-删除-弹出删除确认框-输入联邦命名空间名称-点击删除-删除成功', () => {
    page.detailPage.delete(namespace_name);
    browser.sleep(1).then(() => {
      page.preparePage.waitResourceExist(
        'federatednamespace',
        namespace_name,
        namespace_name,
        false,
      );
      page.listPage.search(namespace_name, 0);
      const expectNoData = { 数量: 0 };
      page.listVerify.verify(expectNoData);
    });
  });
});
