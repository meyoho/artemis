/**
 * Created by zhouchangjian on 2019/6/15.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ProjectPage } from '../../../page_objects/platform/project/project.page';

describe('项目L0级别UI自动化case', () => {
  const page = new ProjectPage();
  const project_name: string = page.getTestData('l0-project');
  const cluster = ServerConf.REGIONNAME;

  const cpu_quota = '2';
  const mem_quota = '2';
  const volum_quota = '2';
  const pvc_num = '2';
  const pods_num = '2';

  const display_name = '测试';
  const project_desc = 'this project is used for e2e';

  const create_project_data = {
    名称: project_name,
    显示名称: display_name,
    描述: project_desc,
    // 所属集群: `${cluster} (${page.clusterDisplayName()})`,
    所属集群: cluster,
  };
  const project_quota = {
    CPU: cpu_quota,
    内存: mem_quota,
    存储: volum_quota,
    'PVC 数': pvc_num,
    'Pods 数': pods_num,
  };
  const namespace_data = {
    所属集群: cluster,
    命名空间: 'ns',
    CPU: cpu_quota,
    内存: mem_quota,
    存储: volum_quota,
    'PVC 数': pvc_num,
    'Pods 数': pods_num,
    容器限额: '1',
  };

  beforeAll(() => {
    page.login();
    page.switchSetting('项目管理');
    browser.sleep(100);
    page.preparePage.deleteProject(project_name);
  });

  it('L0:ACP2UI-4386 : 【创建项目】项目管理视图-项目列表页-点击“创建项目”-输入项目名称、显示名称、描述-选择集群-点击下一步-输入配额-点击创建-项目创建成功', () => {
    page
      .createPlatformProject(create_project_data, project_quota)
      .then(messageSuccess => {
        const expectData = new Map<string, string>();
        expectData.set(messageSuccess, 'detail');
        //项目创建成功后，cueeentUrl 包含detail 关键字
        page.createPageVerify.verify(expectData);
      });
  });
  it('L0:ACP2UI-4402 : 【搜索项目】项目列表页-选择名称-输入名称-搜索出名称与输入名称相同的数据', () => {
    page.switchSetting('项目管理');
    page.listPage.searchContext().search(project_name);
    const expectData = {
      名称: project_name,
      显示名称: display_name,
      状态: '正常',
      '集群(CPU/内存/存储)':
        `${cluster} (${page.clusterDisplayName()})` +
        '\n:\n' +
        cpu_quota +
        '核\n' +
        mem_quota +
        'Gi\n' +
        volum_quota +
        'Gi',
      创建时间: '',
    };
    page.listPageVerify.verify(expectData);
  });
  it('L0:ACP2UI-4409 : 【查看项目】项目列表页-点击项目名称-查看项目详情页信息', () => {
    page.listPage.enterProject(project_name).then(() => {
      const basicInfo = new Map();
      basicInfo.set('显示名称', display_name);
      //basicInfo.set('项目管理员', '');
      basicInfo.set('创建时间', '');
      basicInfo.set('描述', project_desc);
      basicInfo.set('更新时间', '-');
      basicInfo.set('创建人', ServerConf.ADMIN_USER);

      // const projectquotas = new Map();
      // projectquotas.set(`${cluster} (${page.clusterDisplayName()})`, {
      //   CPU: ['0\n%', '0 / ' + cpu_quota + ' 核'],
      //   内存: ['0\n%', '0 / ' + mem_quota + ' Gi'],
      //   存储: ['0\n%', '0 / ' + volum_quota + ' Gi'],
      //   'PVC 数': ['0\n%', '0 / ' + pvc_num + ' 个'],
      //   'Pods 数': ['0\n%', '0 / ' + pods_num + ' 个'],
      // });

      // const expectData = { 基本信息: basicInfo, 项目配额: projectquotas };

      // page.detailPageVerify.verify(expectData);
    });
  });

  it('L0:ACP2UI-4412 : 【查看项目】项目有namespace使用配额-项目详情页-展示已经使用的配额占配额的占比', () => {
    //创建一个命名空间，使用了项目的全部配额
    page.createPlatformNamespace(namespace_data).then(text => {
      expect(text).toBe('命名空间创建成功');
    });

    page.clickLeftNavByText('项目详情');

    // const projectquotas = new Map();
    // projectquotas.set(`${cluster} (${page.clusterDisplayName()})`, {
    //   CPU: ['100\n%', cpu_quota + ' / ' + cpu_quota + ' 核'],
    //   内存: ['100\n%', mem_quota + ' / ' + mem_quota + ' Gi'],
    //   存储: ['100\n%', volum_quota + ' / ' + volum_quota + ' Gi'],
    //   'PVC 数': ['100\n%', pvc_num + ' / ' + pvc_num + ' 个'],
    //   'Pods 数': ['100\n%', pods_num + ' / ' + pods_num + ' 个'],
    // });

    // const expectData = { 项目配额: projectquotas };
    // browser.refresh();
    // page.detailPageVerify.verify(expectData);
  });

  it('L0:ACP2UI-4424 : 【删除项目】项目列表页-进入项目详情页-点击操作-点击删除项目-输入项目名称-点击删除-项目删除成功-项目下所有的clusterrolebindings和namespace都删除成功', () => {
    // expect(page.deleteProject(project_name)).toBe('项目删除成功')
    page.deleteProject(project_name);
    page.listPage.searchContext().search(project_name);
    page.listPage.projectsTable.getRowCount().then(count => {
      expect(count).toBe(0);
    });
  });
});
