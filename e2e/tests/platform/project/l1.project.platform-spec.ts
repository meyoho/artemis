/**
 * Created by zhouchangjian on 2019/6/15.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ProjectPage } from '../../../page_objects/platform/project/project.page';

xdescribe('项目L1级别UI自动化case', () => {
  const page = new ProjectPage();
  const project_name: string = page.getTestData('l1-project');
  const cluster = ServerConf.REGIONNAME;
  const second_cluster = ServerConf.SECONDREGIONNAME;
  const cpu_quota = '2';
  const mem_quota = '2';
  const volum_quota = '2';
  const pvc_num = '2';
  const pods_num = '2';
  const update_cpu_quota = '4';
  const update_mem_quota = '4';
  const update_volum_quota = '4';
  const update_pvc_num = '4';
  const update_pods_num = '4';
  const display_name = '测试';
  const project_desc = 'this project is used for e2e';
  const username = ServerConf.ADMIN_USER;
  const verifyusername = `${ServerConf.ADMIN_USER}\nadmin`;
  const projectrolename = '项目管理员';
  const namespacerolename = ['命名空间管理员', '开发人员'];
  const create_project_data = {
    名称: project_name,
    显示名称: display_name,
    描述: project_desc,
    所属集群: cluster,
  };
  const project_quota = {
    CPU: cpu_quota,
    内存: mem_quota,
    存储: volum_quota,
    'PVC 数': pvc_num,
    'Pods 数': pods_num,
  };
  const addcluster = {
    集群: second_cluster,
    CPU: cpu_quota,
    内存: mem_quota,
    存储: volum_quota,
    'PVC 数': pvc_num,
    'Pods 数': pods_num,
  };
  const update_quota_data = {
    CPU: update_cpu_quota,
    内存: update_mem_quota,
    存储: update_volum_quota,
    'PVC 数': update_pvc_num,
    'Pods 数': update_pods_num,
  };
  const namespace_data = {
    所属集群: cluster,
    命名空间: 'ns',
    CPU: cpu_quota,
    内存: mem_quota,
    存储: volum_quota,
    'PVC 数': pvc_num,
    'Pods 数': pods_num,
    容器限额: '1',
  };
  const ns = project_name + '-' + namespace_data.命名空间;
  beforeAll(() => {
    page.preparePage.deleteProject(project_name);
    page.preparePage.deleteNamespace(ns, cluster);
    page.login();
    page.switchSetting('项目管理');
    browser.sleep(100);
  });

  beforeEach(() => {});

  afterAll(() => {
    page.deleteProject(project_name);
    page.preparePage.deleteNamespace(ns, cluster);
  });

  it('L1:ACP2UI-53497 : 【添加命名空间成员】项目业务视图-点击左导航命名空间-点击命名空间-点击成员管理-点击添加成员-输入成员名称-选择角色-点击添加-成员添加成功 ', () => {
    page.createPlatformProject(create_project_data, project_quota);

    page.createPlatformNamespace(namespace_data).then(text => {
      expect(text).toBe('命名空间创建成功');
    });

    page
      .importNamespaceMember(
        username,
        namespacerolename[0],
        project_name + '-' + namespace_data.命名空间,
      )
      .then(text => {
        expect(text).toBe('导入成功');
      });
    page.namespaceMemberPage.getUserCount().then(count => {
      expect(count).toBe(2);
    });
    page.namespaceMemberPage.getUserRoleByRowIndex(1).then(rolename => {
      expect(rolename).toBe(namespacerolename[0]);
    });
    page.namespaceMemberPage.getUserByRowIndex(1).then(username => {
      expect(username).toBe(verifyusername);
    });
  });
  it('L1:ACP2UI-53502 : 【命名空间删除成员】项目业务视图-点击左导航命名空间-点击命名空间-点击成员管理-点击用户操作-移除-弹框确认“确定移除成员“XXX”吗”-确定-成员移除成功-成员列表不展示移除的成员', () => {
    // expect(page.deleteNamespaceMember(username, namespace_data.命名空间名称)).toBe('移除成功');
    page.deleteNamespaceMember(username, namespace_data.命名空间);
    page.namespaceMemberPage.getUserCount().then(count => {
      expect(count).toBe(1);
    });
  });

  it('L1:ACP2UI-4416 : 【更新项目】项目列表页-进入项目详情页-更新项目基本信息-输入显示名称，描述-点击保存-更新成功', () => {
    const update_description = 'update project description';
    const update_display_name = '更新测试';
    const data = {
      显示名称: update_display_name,
      描述: update_description,
    };
    // expect(page.updateDetail(data)).toBe('项目更新成功')
    page.updateDetail(data);

    const expectData = {
      基本信息: {
        显示名称: update_display_name,
        // 项目管理员: '',
        // 创建时间: '',
        描述: update_description,
        创建人: ServerConf.ADMIN_USER,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  // TODO: staging 环境项目添加global 集群，设定CPU，内存 ... 大小， 页面上仍然显示不限制
  it('L1:ACP2UI-4420 : 【添加集群】项目列表页-进入项目详情页-点击操作-点击添加集群-集群列表页显示未添加的集群-选择集群-填写配额-集群添加成功-在详情页展示添加的集群', () => {
    if (second_cluster !== 'undefined') {
      // 项目添加第二个集群
      expect(page.addCluster(addcluster)).toBe('添加集群成功');

      const projectquotas = new Map();
      projectquotas.set(`${second_cluster} (${second_cluster})`, {
        CPU: ['0\n%', '0 / ' + cpu_quota + ' 核'],
        内存: ['0\n%', '0 / ' + mem_quota + ' Gi'],
        存储: ['0\n%', '0 / ' + volum_quota + ' Gi'],
        'PVC 数': ['0\n%', '0 / ' + pvc_num + ' 个'],
        'Pods 数': ['0\n%', '0 / ' + pods_num + ' 个'],
      });

      const expectData = { 项目配额: projectquotas };
      browser.refresh();
      page.detailPageVerify.verify(expectData);
    }
  });
  it('L1:ACP2UI-4422 : 【移除集群】项目列表页-进入项目详情页-项目有2个以上集群-点击操作-点击移除集群-输入需要移除的集群的名称-点击移除-集群移除成功-项目详情页不展示移除的集群', () => {
    if (second_cluster !== 'undefined') {
      // expect(page.removeCluster(second_cluster)).toBe('集群移除成功');
      page.removeCluster(second_cluster);

      page.checkClusterNotExist(second_cluster).then(rusult => {
        expect(rusult).toBe(false);
      });
    }
  });
  it('L1:ACP2UI-4417 : 【更新项目】项目列表页-进入项目详情页-更新项目配额-选择集群-输入大于等于当前已使用的配额的数值-点击保存-配额更新成功', () => {
    page.updateQuota(update_quota_data).then(rusult => {
      expect(rusult).toBe('项目更新成功');
    });

    const projectquotas = new Map();
    projectquotas.set(`${cluster} (${cluster})`, {
      CPU: ['50\n%', cpu_quota + ' / ' + update_cpu_quota + ' 核'],
      内存: ['50\n%', mem_quota + ' / ' + update_mem_quota + ' Gi'],
      存储: ['50\n%', volum_quota + ' / ' + update_volum_quota + ' Gi'],
      'PVC 数': ['50\n%', pvc_num + ' / ' + update_pvc_num + ' 个'],
      'Pods 数': ['50\n%', pods_num + ' / ' + update_pods_num + ' 个'],
    });

    const expectData = { 项目配额: projectquotas };
    browser.refresh();
    page.detailPageVerify.verify(expectData);
  });
  it('L1:ACP2UI-4428 : 【导入项目成员】项目业务视图-点击成员管理-点击导入成员-选择组-选择成员-选择角色-点击导入-成员导入成功', () => {
    page.importProjectMember(username, projectrolename).then(rusult => {
      expect(rusult).toBe('导入成功');
    });
    page.projectMemberPage
      .memberList()
      .getRowCount()
      .then(count => {
        expect(count).toBe(1);
      });
    expect(page.projectMemberPage.getUserByRowIndex(0)).toContain(
      verifyusername,
    );
    expect(
      page.projectMemberPage.getUserRoleByRowIndex(verifyusername),
    ).toContain(projectrolename);
  });
  it('L1:ACP2UI-4430 : 【删除成员】项目业务视图-点击成员管理-点击用户操作-移除-弹框确认“确定移除成员“XXX”吗”-确定-成员移除成功-成员列表不展示移除的成员', () => {
    // expect(page.deleteProjectMember(verifyusername)).toBe('移除成功');
    page.deleteProjectMember(verifyusername);
    page.projectMemberPage
      .memberList()
      .getRowCount()
      .then(count => {
        expect(count).toBe(0);
      });
  });
});
