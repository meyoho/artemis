import { ResourceManagementPage } from '@e2e/page_objects/acp/resource_management/resource.management';
import { browser } from 'protractor';

describe('管理视图 资源管理L1自动化', () => {
  const page = new ResourceManagementPage();
  const region_name = page.clusterName;
  const ingress_name = page.getTestData('resource');

  const namespace_name = page.namespace1Name;
  beforeAll(() => {
    page.preparePage.deleteIngress(ingress_name, namespace_name);
    page.login();
    page.listPage.openResourceListPage();
    page.switchCluster_onAdminView(region_name);
  });

  afterAll(() => {
    page.preparePage.deleteIngress(ingress_name, namespace_name);
  });

  it('ACP2UI-53312 : 单击左导航资源管理，选择命名空间资源-点击创建资源对象', () => {
    page.createPage.create(ingress_name, 'Ingress', namespace_name);
    browser.sleep(1000);
    const expectData = { 资源数量: 1 };
    page.listPage.resource.search('Ingress');
    page.listPage.searchResource(ingress_name, namespace_name);
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53313 : 单击左导航资源管理，按名称搜索命名空间相关资源对象', () => {
    //命名空间相关资源
    page.listPage.resource.search('Ingress');

    page.listPage.searchResource('notexist', namespace_name);
    const expectData = { 资源数量: 0 };
    page.listPageVerify.verify(expectData);
  });

  it('ACP2UI-53689 : 单击左导航资源管理，选择命名空间资源-点击资源对象名称-查看yaml', () => {
    page.listPage.resource.search('Ingress');
    page.listPage.searchResource(ingress_name, namespace_name);
    page.listPage.clickview(ingress_name);
    const expectData = { 详情YAML: 'kind: Ingress' };
    page.detailPageVeriy.verify(expectData);
  });
  it('ACP2UI-53690 : 单击左导航资源管理，选择命名空间资源-点击资源对象-操作-更新', () => {
    page.updatePage.updateIngress(ingress_name, namespace_name);
    page.listPage.resource.search('Ingress');
    page.listPage.searchResource(ingress_name, namespace_name);
    browser.sleep(100);
    const expectData = { 标签: ingress_name };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53311 : 单击左导航资源管理，选择命名空间资源-点击资源对象-操作-删除', () => {
    page.deletePage.deleteResource(ingress_name, namespace_name);

    const expectData = { 资源数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
