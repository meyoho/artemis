import { ResourceManagementPage } from '@e2e/page_objects/acp/resource_management/resource.management';

describe('管理视图 资源管理L0自动化', () => {
  const page = new ResourceManagementPage();
  beforeAll(() => {
    page.login();
    page.listPage.openResourceListPage();
    page.listPage.switchCluster_onAdminView(page.clusterName);
  });

  it('ACP2UI-53305 : 单击左导航资源管理，按名称过滤资源类型', () => {
    //命名空间相关资源
    let expectData = {
      命名空间相关: '命名空间相关 (1)',
      集群相关: '集群相关 (0)',
    };
    page.listPage.resource.onlysearch('Endpoints');
    page.listPageVerify.verify(expectData);
    //集群相关资源
    page.listPage.resource.onlysearch('APIService');
    expectData = {
      命名空间相关: '命名空间相关 (0)',
      集群相关: '集群相关 (1)',
    };
    page.listPageVerify.verify(expectData);
    // 不存在的资源
    page.listPage.resource.onlysearch('notexist');
    expectData = {
      命名空间相关: '命名空间相关 (0)',
      集群相关: '集群相关 (0)',
    };
    page.listPageVerify.verify(expectData);
  });
});
