import { ResourceManagementPage } from '@e2e/page_objects/acp/resource_management/resource.management';
import { ServerConf } from '@e2e/config/serverConf';

describe('管理视图 资源管理L1自动化', () => {
  const page = new ResourceManagementPage();
  const name = page.getTestData('resource');

  const namespace_name = page.getTestData('ns-resource');
  const ns = `${page.projectName}-${namespace_name}`;
  beforeAll(() => {
    page.preparePage.deleteNs(ns);
    page.login();
    page.listPage.openResourceListPage();
    page.switchCluster_onAdminView(ServerConf.REGIONNAME);
  });
  beforeEach(() => {});
  afterAll(() => {
    page.preparePage.deleteNs(ns);
  });

  it('ACP2UI-53692 : 单击左导航资源管理，选择集群资源-点击创建资源对象', () => {
    page.createPage.create(name, 'Namespace', namespace_name);
    page.listPage.searchCategory('Namespace');
    page.listPage.searchResource(ns);
    const expectData = { 资源数量: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53693 : 单击左导航资源管理，按名称搜索集群相关资源对象', () => {
    //  集群相关资源
    page.listPage.searchCategory('Namespace');
    page.listPage.searchResource('notexist');
    const expectData = { 资源数量: 0 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53694 : 单击左导航资源管理，选择集群资源-点击资源对象名称-查看yaml', () => {
    page.listPage.searchCategory('Namespace');
    page.listPage.searchResource(ns);
    page.listPage.clickview(ns);
    const expectData = { 详情YAML: 'Namespace' };
    page.detailPageVeriy.verify(expectData);
  });
  it('ACP2UI-53695 : 单击左导航资源管理，选择集群资源-点击资源对象-操作-更新', () => {
    page.updatePage.updateNs(ns);
    page.listPage.searchCategory('Namespace');
    page.listPage.searchResource(ns);
    const expectData = { 命名空间标签: ns };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-53697 : 单击左导航资源管理，选择集群资源-点击资源对象-操作-删除', () => {
    page.deletePage.deleteResource(ns, namespace_name, 'Namespace');
    const expectData = { 资源数量: 0 };
    page.listPageVerify.verify(expectData);
  });
});
