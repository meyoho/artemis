import { ClusterPage } from '@e2e/page_objects/platform/cluster/cluster.page';

describe('cluster自动化测试', () => {
  const page = new ClusterPage();
  const clustername = page.getTestData('cluster2');

  beforeAll(() => {
    page.login();
    page.enterPlatformView('平台管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('集群管理');
  });
  afterAll(() => {});

  it('ACP2UI-58197 : 创建集群-填写参数-全部正确参数-取消-返回列表页', () => {
    page.getButtonByText('创建集群').click();
    const testData = {
      名称: clustername,
      显示名称: clustername,
      集群地址: { 'IP 地址/域名': '1.0.0.80', 端口: '6443' },
      控制节点: '1.0.0.80',
      'SSH 端口': '22',
      用户名: 'root',
      密码: 'root',
      'Cluster CIDR': '10.3.0.0/16',
      'Service CIDR': '10.4.0.0/16',
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('取消').click(); // 点击取消按钮返回列表页
  });

  it('ACP2UI-58176 : 接入集群-全部参数-点击接入-点击取消-返回参数页面', () => {
    page.getButtonByText('接入集群').click();
    const testData = {
      名称: clustername,
      显示名称: clustername,
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('取消').click(); // 点击取消按钮返回列表页
  });

  xit('ACP2UI-53255 : 详情页删除集群-取消', () => {
    page.listPage.enterDetail('cls-w5xrsf62');
    page.detailPage.detailPage_operate('删除集群');
    page.getButtonByText('取消').click();
  });
});
