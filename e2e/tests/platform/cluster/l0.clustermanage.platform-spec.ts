import { ClusterPage } from '@e2e/page_objects/platform/cluster/cluster.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';

describe('集群 L0自动化测试', () => {
  const page = new ClusterPage();
  const clustername = page.getTestData('cluster1');

  function getIssueUrl() {
    const data = CommonMethod.parseYaml(
      CommonKubectl.execKubectlCommand(
        'kubectl get configmap -n alauda-system dex-configmap -o yaml',
      ),
    );

    return CommonMethod.parseYaml(String(data.data['config.yaml'])).issuer;
  }

  beforeAll(() => {
    page.login();
    page.enterPlatformView('平台管理');
    page.clickLeftNavByText('集群管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('集群');
  });
  afterAll(() => {});

  it('ACP2UI-53255 : 创建集群-填写全部参数-创建成功', () => {
    page.getButtonByText('创建集群').click();
    // 创建集群页填写参数;
    const testData = {
      名称: clustername,
      显示名称: clustername,
      集群地址: { 'IP 地址/域名': '1.0.0.80', 端口: '6443' },
      控制节点: '1.0.0.80',
      'SSH 端口': '22',
      用户名: 'root',
      密码: 'root',
      'Cluster CIDR': '10.3.0.0/16',
      'Service CIDR': '10.4.0.0/16',
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('创建').click();

    // page.scriptInfoDialogVerify.verify(expectData);
  });
  it('ACP2UI-53340 : 接入集群-正常跳转页面-验证面包屑显示/跳转', () => {
    //多集群管理页点击接入集群
    page.getButtonByText('接入集群').click();
    const expectData = {
      警示信息: {
        信息:
          '请严格按照如下参数调整 K8S API Server 相关配置信息，否则可能会遇到权限不同步等问题，导致您无法正常使用平台。',
        '--oidc-issuer-url': `--oidc-issuer-url=${getIssueUrl()}`,
        '--oidc-client-id':
          '--oidc-client-id=alauda-auth；OIDC 客户端的唯一标识',
        '--oidc-ca-file':
          '--oidc-ca-file=/etc/kubernetes/pki/apiserver-dex-ca.crt',
        '--oidc-username-claim': '--oidc-username-claim=email',
        '--oidc-groups-claim': '--oidc-groups-claim=groups',
      },
      // 面包屑: '集群/集群管理/接入',
    };
    page.accessPageVerify.verify(expectData);

    page.getButtonByText('接入').click();
    // page.accessPageVerify.verify({ 必填项: 5 });
  });
});
