import { ClusterPage } from '@e2e/page_objects/platform/cluster/cluster.page';
import { browser } from 'protractor';

describe('cluster自动化测试', () => {
  const page = new ClusterPage();
  const clustername1 = 'uiauto-cluster1';
  const clustername =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrstuvwxyz26-0606';
  const displayName = 'uiauto-集群测试1';
  const CIDR = '10.33.0.0/16';

  beforeAll(() => {
    page.login();
    page.enterPlatformView('平台管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('集群管理');
  });
  afterAll(() => {});

  it('ACP2UI-58178 : 创建集群-填写参数-名称校验-长度/特殊字符校验-其余参数正确填写-点击创建-创建成功', () => {
    page.getButtonByText('创建集群').click();
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', clustername + 'duoyuzifu', ''); // 名称63位
    page.getButtonByText('创建').click(); // 点击创建按钮

    browser.sleep(1000);
    const testData = {
      显示名称: displayName,
      集群地址: { 'IP 地址/域名': '1.0.0.80', 端口: '6443' },
      控制节点: '1.0.0.80',
      'SSH 端口': '22',
      用户名: 'root',
      密码: 'root',
      'Cluster CIDR': '10.3.0.0/16',
      'Service CIDR': '10.4.0.0/16',
    };
    page.createPage.fillForm(testData);
  });

  xit('创建集群-名称重复校验-点击创建时提示名称已存在', () => {
    // 点击表头进入详情页面
    page.getButtonByText('创建集群').click();
    const testData = {
      名称: 'global',
      显示名称: displayName,
      集群地址: { 'IP 地址/域名': '1.0.0.80', 端口: '6443' },
      控制节点: '1.0.0.80',
      端口: '22',
      用户名: 'root',
      密码: 'root',
      CIDR: CIDR,
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('创建').click(); // 点击创建按钮
    //由于集群名称已存在
    // expect(page.createPage.errorAlert().checkTextIsPresent()).toBeTruthy();
    // page.createPage.errorAlert_close.click();
    page.createPage.inputByText('名称', clustername1); //修改名称
    page.getButtonByText('创建').click(); // 点击创建按钮
  });
  it('ACP2UI-58117 : 点击左导航集群/集群管理-管理集群列表页展示正常-搜索框支持名称搜索-刷新按钮正常', () => {
    page.listPage.search('global', 1);
    const expectData = { 检索数量: 1 };
    page.listPageVerify.verify(expectData);
  });
});
