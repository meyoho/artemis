/**
 * Created by dmchang on 2019/6/18.
 */
import { RolePage } from '../../../page_objects/platform/roles/role.page';
import { browser } from 'protractor';

describe('角色页面检查五个默认角色', () => {
  const page = new RolePage();

  beforeAll(async () => {
    page.login();
    page.enterPlatformView('平台管理');
    await browser.sleep(200);
    page.clickLeftNavByText('用户角色管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('角色管理');
  });

  it('ACP2UI-4378:【角色】管理视图-角色管理-查看系统角色-5个系统角色-平台管理员-平台审计员-项目管理员-命名空间管理员-命名空间开发员', () => {
    const expectData = {
      'acp-platform-admin': {
        类型: '平台角色',
        属性: '系统角色',
        描述: '具备平台所有业务及资源的全部权限。',
        创建人: 'system',
      },
      'acp-platform-auditor': {
        类型: '平台角色',
        属性: '系统角色',
        描述:
          '负责整个平台的审计工作，可查看平台所有资源与操作记录，没有查看外的其他操作权限。',
        创建人: 'system',
      },
      'acp-project-admin': {
        类型: '项目角色',
        属性: '系统角色',
        描述: '负责绑定/解绑命名空间管理员，以及管理命名空间配额。',
        创建人: 'system',
      },
      'acp-namespace-admin': {
        类型: '命名空间角色',
        属性: '系统角色',
        描述: '负责给命名空间添加成员以及设置角色。',
        创建人: 'system',
      },
      'acp-namespace-developer': {
        类型: '命名空间角色',
        属性: '系统角色',
        描述: '负责在该命名空间下开发、部署以及维护应用。',
        创建人: 'system',
      },
    };
    page.roleListPageVerify.verify(expectData);
  });
  it('ACP2UI-4379 :【角色】角色管理-点击平台管理员-验证角色基本信息-权限-角色描述', () => {
    // 点击表头进入详情页面
    page.listPage.enterDetail('平台管理员');
    const expectData = {
      基本信息: {
        显示名称: '平台管理员',
        类型: '平台角色',
        属性: '系统角色',
        描述: '具备平台所有业务及资源的全部权限。',
        创建人: 'system',
      },
    };
    page.roleDetailPageVerify.verify(expectData);
  });
});
