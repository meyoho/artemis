/**
 * Created by zhangjiao on 2020/01/22.
 */

import { RolePage } from '../../../page_objects/platform/roles/role.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';

describe('自定义角色L1级别测试用例', () => {
  const page = new RolePage();
  const rolename =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrstuvwxyz26-0606';
  const displayName = 'uiauto-自定义角色l1-1';
  const resourceName = 'uiauto.resourcel1';
  const rolename1 = 'uiatu-rolel1';
  const displayName1 = 'uiauto-自定义角色l1-2';

  beforeAll(() => {
    page.preparePage.deleteRoleTemplate(rolename);
    page.preparePage.deleteRoleTemplate(rolename1);
    browser.sleep(500);
    page.login();
    page.enterPlatformView('平台管理');
    page.clickLeftNavByText('用户角色管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('角色管理');
  });
  afterAll(() => {
    page.preparePage.deleteRoleTemplate(rolename);
    page.preparePage.deleteRoleTemplate(rolename1);
  });

  /**
   * ACP2UI-55625 该用例包含了所有创建角色时名称的校验，显示名称为空的校验以及输入显示名称，描述为空，权限全部不选的情况
   *  */
  it('ACP2UI-55625|ACP2UI-55626|ACP2UI-55627|ACP2UI-55628|ACP2UI-55629|ACP2UI-55630|ACP2UI-55631|ACP2UI-55632|ACP2UI-55633|ACP2UI-55634|ACP2UI-55635|ACP2UI-55636|ACP2UI-55662 : 创建角色-角色名称校验', () => {
    // 点击表头进入详情页面
    page.getButtonByText('创建角色').click();
    page.getButtonByText('下一步').click(); // 点击下一步按钮
    page.createPage.inputByText('名称', '', '名称是必填项');
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    // page.createPage.inputByText('名称', '55shuzi');
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', rolename + 'duoyuzifu', ''); // 名称63位

    page.getButtonByText('下一步').click(); // 点击下一步按钮
    // 显示名称校验
    page.createPage.inputByText('显示名称', '', '显示名称是必填项');
    page.createPage.inputByText('显示名称', displayName, '');
    page.getButtonByText('下一步').click(); // 点击下一步按钮
    page.getButtonByText('创建').click(); // 点击创建按钮

    browser.sleep(1000);
    const expectData = {
      基本信息: {
        显示名称: displayName,
        类型: '平台角色',
        属性: '自定义角色',
        创建人: ServerConf.ADMIN_USER,
      },
    };
    page.roleDetailPageVerify.verify(expectData);
  });

  /**
   * 校验名称重复，校验自定义资源的名称校验
   */
  it('ACP2UI-56944 : 创建角色-角色名称校验-重复校验-点击创建时提示名称已存在', () => {
    // 点击表头进入详情页面
    page.getButtonByText('创建角色').click();
    const testData2 = {
      名称: rolename,
      显示名称: displayName1,
      描述: '通过UI自动化创建的角色，不选择任何权限创建',
    };
    page.createPage.fillForm(testData2);
    page.getButtonByText('下一步').click(); // 点击下一步按钮

    const testData = {
      添加自定义资源: 'click',
      'API 组': '自定义',
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('添加').click(); // 点击创建按钮
    page.createPage.inputByText1('组名称', '', '组名称是必填项');
    // 校验组名称
    page.createPage.inputByText1('组名称', 'Daxiezimu');
    page.createPage.inputByText1('组名称', '_teshuzifu');
    page.createPage.inputByText1('组名称', '中文aaaaaaaa');
    page.createPage.inputByText1('组名称', 'aaaa_aaaaa');
    page.createPage.inputByText1('组名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText1('组名称', 'aaaateDAXIE');
    page.createPage.inputByText1('组名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText1('组名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText1('组名称', resourceName, '');
    browser.sleep(500);
    // 校验资源名称
    page.getButtonByText('添加').click(); // 点击添加按钮
    page.createPage.inputByText1('资源名称', '', '资源名称是必填项');
    page.createPage.inputByText1('资源名称', 'Daxiezimu');
    page.createPage.inputByText1('资源名称', '_teshuzifu');
    page.createPage.inputByText1('资源名称', '中文aaaaaaaa');
    page.createPage.inputByText1('资源名称', 'aaaa_aaaaa');
    page.createPage.inputByText1('资源名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText1('资源名称', 'aaaateDAXIE');
    page.createPage.inputByText1('资源名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText1('资源名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText1('资源名称', 'aa.aa', ''); //特殊字符开头
    page.getButtonByText('添加').click(); // 点击创建按钮

    const testData1 = {
      权限: resourceName,
    };
    page.createPage.fillForm(testData1);
    page.getButtonByText('创建').click(); // 点击创建按钮
    //由于角色名称已存在
    expect(page.createPage.errorAlert().checkTextIsPresent()).toBeTruthy();
    page.createPage.errorAlert_close.click();
    page.getButtonByText('上一步').click(); // 点击上一步按钮
    page.createPage.inputByText('名称', rolename1, ''); //修改名称
    page.getButtonByText('下一步').click(); // 点击下一步按钮
    page.getButtonByText('创建').click(); // 点击创建按钮

    browser.sleep(500);
    const expectData = {
      基本信息: {
        显示名称: displayName1,
        类型: '平台角色',
        属性: '自定义角色',
        创建人: ServerConf.ADMIN_USER,
        描述: '通过UI自动化创建的角色，不选择任何权限创建',
      },
    };
    page.roleDetailPageVerify.verify(expectData);
  });
});
