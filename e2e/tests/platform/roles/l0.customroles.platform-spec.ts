/**
 * Created by zhangjiao on 2020/02/04.
 */

import { RolePage } from '../../../page_objects/platform/roles/role.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';

describe('自定义角色L0级别测试用例', () => {
  const page = new RolePage();
  const rolename = 'acp-platform-admin-uiautocopy1';
  const roledisplayname = '平台管理员副本-uiauto111';
  const discription = '具备平台所有业务及资源的全部权限-描述描述描述描述描述';

  beforeAll(() => {
    page.preparePage.deleteRoleTemplate(rolename);
    browser.sleep(500);
    page.login();
    page.enterPlatformView('平台管理');
    page.clickLeftNavByText('用户角色管理');
  });
  beforeEach(() => {
    page.clickLeftNavByText('角色管理');
  });
  afterAll(() => {
    page.preparePage.deleteRoleTemplate(rolename);
  });

  it('ACP2UI-55714 : L0-复制为新角色-完全继承', () => {
    // 点击表头进入详情页面
    page.listPage.enterDetail('平台管理员');
    page.detailPage.detailPage_operate('复制为新角色');
    browser.sleep(1000);
    const testData = {
      名称: rolename,
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('下一步').click(); // 点击下一步按钮
    page.getButtonByText('创建').click(); // 点击创建按钮
    const expectData = {
      基本信息: {
        显示名称: '平台管理员副本',
        类型: '平台角色',
        属性: '自定义角色',
        描述: '具备平台所有业务及资源的全部权限。',
        创建人: ServerConf.ADMIN_USER,
      },
    };
    page.roleDetailPageVerify.verify(expectData);
    browser.sleep(1000);
  });

  it('ACP2UI-55718 : L0-自定义角色-更新基本信息', () => {
    // 点击表头进入详情页面
    page.listPage.enterDetail(rolename);
    page.detailPage.detailPage_operate('更新基本信息');
    browser.sleep(500);
    const testData = {
      显示名称: roledisplayname,
      描述: discription,
    };
    page.createPage.fillForm(testData);
    page.getButtonByText('更新').click(); // 点击更新按钮
    browser.sleep(1000);
    const expectData = {
      基本信息: {
        显示名称: roledisplayname,
        类型: '平台角色',
        属性: '自定义角色',
        描述: discription,
        创建人: ServerConf.ADMIN_USER,
      },
    };
    page.roleDetailPageVerify.verify(expectData);
  });

  it('ACP2UI-55724 : L0-自定义角色-删除', () => {
    // 点击表头进入详情页面
    page.listPage.enterDetail(rolename);
    page.detailPage.detailPage_operate('删除');
    page.detailPage.confirmDialog_name.clickDelete(rolename);
  });
});
