import { browser } from 'protractor';
import { LicensePage } from '@e2e/page_objects/platform/licensemanage/license.page';

describe('许可证管理L1级别UI自动化case', () => {
  const page = new LicensePage();

  beforeAll(() => {
    page.login();
    page.licenseListPage.toPlatformManagePage().then(() => {
      page.clickLeftNavByText('许可证管理');
      page.licenseListPage.waitPagePresent();
    });
    browser.sleep(100);
  });

  it('ACP2UI-58093 : 平台管理员登陆平台->进入许可证管理列表页面->查看页面展示', () => {
    const v_data = {
      面包屑: '许可证管理',
      提示信息: `许可证以证书的形式，对产品的授权用户、使用期限、集群规模等进行管控。
 获取证书方式如下：
下载 平台信息（Cluster Feature Code）并发给厂商，厂商根据购买情况生成许可证
点击【导入许可证】，将厂商提供许可证导入，激活产品`,
      表头: ['许可产品', '状态', '版本类型', '生效时间', '截止时间', ''],
    };
    page.licenseVeryfyPage.verify(v_data);
  });
});
