import { browser } from 'protractor';
import { LicensePage } from '@e2e/page_objects/platform/licensemanage/license.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { ServerConf } from '@e2e/config/serverConf';

describe('许可证管理L1级别UI自动化case', () => {
  const page = new LicensePage();

  beforeAll(() => {
    page.licensePreparePage.createLicense();
    page.licensePreparePage.deleteLicense(
      ServerConf.TESTDATAPATH + 'valid.lic',
    );
    page.licensePreparePage.importLicense(
      ServerConf.TESTDATAPATH + 'valid.lic',
    );
    page.login();
    page.licenseListPage.toPlatformManagePage().then(() => {
      page.clickLeftNavByText('许可证管理');
      page.licenseListPage.waitPagePresent();
    });
    browser.sleep(100);
  });

  beforeEach(() => {});

  afterAll(() => {
    page.licensePreparePage.deleteLicense(
      ServerConf.TESTDATAPATH + 'valid.lic',
    );
  });
  it('ACP2UI-58113 : 许可证管理页面->点击导入许可证->输入已存在的许可证密钥->点击导入->导入失败，弹窗不关闭，提示许可证存在', () => {
    const data = CommonMethod.readyamlfile('valid.lic');
    page.licenseListPage.importLicense(data).then(() => {
      const v_data = {
        存在导入License弹窗: true,
        存在数据: [['AutoTest', '试用版', '已激活']],
      };
      page.licenseVeryfyPage.verify(v_data);
    });
  });
});
