import { browser } from 'protractor';
import { LicensePage } from '@e2e/page_objects/platform/licensemanage/license.page';

describe('许可证管理L1级别UI自动化case', () => {
  const page = new LicensePage();

  beforeAll(() => {
    page.login();
    page.licenseListPage.toPlatformManagePage().then(() => {
      page.clickLeftNavByText('许可证管理');
      page.licenseListPage.waitPagePresent();
    });
    browser.sleep(100);
  });

  beforeEach(() => {});

  afterAll(() => {});

  it('ACP2UI-58099 : 许可证管理页面->点击导入许可证->输入错误的的许可证密钥->点击导入->导入失败，弹窗不关闭，提示许可证无效', () => {
    const data = 'abc';
    page.licenseListPage.importLicense(data);
    const v_data = { 存在导入License弹窗: true };
    page.licenseVeryfyPage.verify(v_data);
  });
});
