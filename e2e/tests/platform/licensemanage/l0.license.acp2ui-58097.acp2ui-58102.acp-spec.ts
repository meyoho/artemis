import { browser } from 'protractor';
import { LicensePage } from '@e2e/page_objects/platform/licensemanage/license.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { ServerConf } from '@e2e/config/serverConf';

describe('许可证管理L0级别UI自动化case', () => {
  const page = new LicensePage();

  beforeAll(() => {
    page.licensePreparePage.createLicense();
    page.licensePreparePage.deleteLicense(
      ServerConf.TESTDATAPATH + 'valid.lic',
    );
    page.login();
    page.licenseListPage.toPlatformManagePage().then(() => {
      page.clickLeftNavByText('许可证管理');
      page.licenseListPage.waitPagePresent();
    });
    browser.sleep(100);
  });

  beforeEach(() => {});

  afterAll(() => {
    page.licensePreparePage.deleteLicense(
      ServerConf.TESTDATAPATH + 'valid.lic',
    );
  });

  it('ACP2UI-58097 : 许可证管理页面->点击导入许可证->输入正确的许可证密钥->点击导入->导入成功，弹窗关闭，产品已被激活 ', () => {
    const data = CommonMethod.readyamlfile('valid.lic');
    page.licenseListPage.importLicense(data).then(() => {
      const v_data = {
        存在数据: [['AutoTest', '试用版', '已激活']],
      };
      page.licenseVeryfyPage.verify(v_data);
    });
  });
  it('ACP2UI-58102 : 许可证管理页面->许可证列表->选择一行点击操作->点击删除->点击确认->成功删除许可证', () => {
    page.licenseListPage.deleteLicense(['AutoTest', '已激活']).then(() => {
      const v_data = {
        不存在数据: [['AutoTest', '试用版', '已激活']],
      };
      page.licenseVeryfyPage.verify(v_data);
    });
  });
});
