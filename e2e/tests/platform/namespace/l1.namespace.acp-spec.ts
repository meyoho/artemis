import { ServerConf } from '@e2e/config/serverConf';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';

describe('项目管理 命名空间L1自动化', () => {
  const page = new NamespacePage();
  const project = page.projectName;
  const ns_name = page.getTestData('ns-l1');
  const name = `${project}-${ns_name}`;
  const testCreateData = {
    基本信息: {
      所属集群: ServerConf.REGIONNAME,
      命名空间: ns_name,
      显示名称: ns_name.toUpperCase(),
    },
    容器限额: {
      CPU: {
        默认值: '5',
        最大值: '2000',
      },
      内存: {
        默认值: '10',
        最大值: '4096',
      },
    },
  };

  beforeAll(() => {
    page.nsPrepare.deleleNs(name);
    page.login();
    page.switchSetting('项目管理');
    page.projectlistPage.enterProject(project);
    page.nsCreatePage.create_ns(testCreateData);
  });
  beforeEach(() => {
    page.clickLeftNavByText('命名空间');
  });
  afterAll(() => {
    page.nsPrepare.deleleNs(name);
  });

  it('ACP2UI-53779 : 单击左导航命名空间，在列表页单击操作列-更新资源配额，成功', () => {
    const testData = {
      CPU: '2',
      内存: '1',
      存储: '1',
      'PVC 数': '1',
      'Pods 数': '10',
    };
    page.nsListPage.updatenamespacequota_list(name, testData);
    page.nsListPage.enterDetail(name);
    const v_data = {
      资源配额: testData,
    };
    page.nsDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-53780 : 单击左导航命名空间，点击名称进入详情页，验证详细信息，成功', () => {
    page.nsListPage.enterDetail(name);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        显示名称: ns_name.toUpperCase(),
        所属集群: ServerConf.REGIONNAME,
      },
      容器限额: {
        CPU: {
          默认值: '5m',
          最大值: '2 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '4Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
  it('ACP2UI-53781 : 单击左导航命名空间，点击名称进入详情页，更新注解，成功', () => {
    const testDataAnn = [
      [`resource.${ServerConf.LABELBASEDOMAIN}/status`, 'Initializing'],
      ['add', 'updateannotation'],
    ];
    page.nsDetailPage.updateannotation(name, testDataAnn);
    const expectData = {
      注解: testDataAnn,
    };
    page.nsDetailVerifyPage.verify(expectData);
  });
  it('ACP2UI-53782 : 单击左导航命名空间，点击名称进入详情页，更新标签，成功', () => {
    const testDataLabel = [
      [`${ServerConf.LABELBASEDOMAIN}/cluster`, ServerConf.REGIONNAME],
      [`${ServerConf.LABELBASEDOMAIN}/project`, project],
      ['add', 'updatelabel'],
    ];
    page.nsDetailPage.updatelabel(name, testDataLabel);
    const expectData = {
      标签: testDataLabel,
    };
    page.nsDetailVerifyPage.verify(expectData);
  });

  it('ACP2UI-53784 : 单击左导航命名空间，点击名称进入详情页，点击操作-更新容器限额，成功', () => {
    const testData = {
      容器限额: {
        CPU: {
          默认值: '20',
          最大值: '1000',
        },
        内存: {
          默认值: '10',
          最大值: '1024',
        },
      },
    };

    page.nsDetailPage.updateContainerLimitRange(
      `${project}-${ns_name}`,
      testData,
    );

    const expectData = {
      容器限额: {
        CPU: {
          默认值: '20m',
          最大值: '1 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '1Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(expectData);
  });
  it('ACP2UI-53163 : 命名空间详情页-删除，输入正确的名称，确定后删除成功，可以再次创建同名称的命名空间', () => {
    page.nsListPage.deletenamespace(`${project}-${ns_name}`);
    // page.nsListPage.searchnamespace(`${project}-${ns_name}`, 0);
    page.nsCreatePage.create_ns(testCreateData);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        显示名称: ns_name.toUpperCase(),
        所属集群: ServerConf.REGIONNAME,
      },
      容器限额: {
        CPU: {
          默认值: '5m',
          最大值: '2 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '4Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
});
