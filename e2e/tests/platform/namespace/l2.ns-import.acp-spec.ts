import { ServerConf } from '@e2e/config/serverConf';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';

xdescribe('项目管理 命名空间导入L2自动化', () => {
  const page = new NamespacePage();
  const project = page.projectName;

  const name = page.getTestData('ns-import');

  beforeAll(() => {
    page.nsPrepare.deleleNs(name);
    const createData = {
      name: name,
      limitrange: true,
    };
    page.nsPrepare.createNs(createData);
    page.login();
    page.switchSetting('项目管理');
    page.projectlistPage.enterProject(project);
  });
  beforeEach(() => {
    page.clickLeftNavByText('命名空间');
  });
  afterAll(() => {
    page.nsPrepare.deleleNs(name);
  });
  it('ACP2UI-55284 : 导入命名空间，该命名空间的名字不是以项目名开头', () => {
    const testData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name,
        显示名称: name.toUpperCase(),
      },
    };
    page.nsImportPage.import_ns(testData);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        显示名称: name.toUpperCase(),
        所属集群: ServerConf.REGIONNAME,
      },
      容器限额: {
        CPU: {
          默认值: '10m',
          最大值: '1 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '1Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
  it('ACP2UI-55278 : 导入命名空间后，对该命名空间的详情页面测试', () => {
    page.nsListPage.enterDetail(name);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        显示名称: name.toUpperCase(),
        所属集群: ServerConf.REGIONNAME,
      },
      容器限额: {
        CPU: {
          默认值: '10m',
          最大值: '1 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '1Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
  it('ACP2UI-55264 : 对纳管命名空间进行更新-更新资源配额，成功', () => {
    const testData = {
      CPU: '2',
      内存: '1',
      存储: '1',
      'PVC 数': '1',
      'Pods 数': '10',
    };
    page.nsListPage.updatenamespacequota_detail(name, testData);

    page.nsListPage.enterDetail(name);

    const v_data = {
      资源配额: testData,
    };
    page.nsDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-55264 : 对纳管命名空间进行更新-更新容器限额，成功', () => {
    const testData = {
      容器限额: {
        CPU: {
          默认值: '20',
          最大值: '1000',
        },
        内存: {
          默认值: '10',
          最大值: '1024',
        },
      },
    };

    page.nsDetailPage.updateContainerLimitRange(name, testData);

    const expectData = {
      容器限额: {
        CPU: {
          默认值: '20m',
          最大值: '1 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '1Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(expectData);
  });
  it('ACP2UI-55264 : 对纳管命名空间进行更新-更新注解，成功', () => {
    const testDataAnn = [
      [`resource.${ServerConf.LABELBASEDOMAIN}/status`, 'Initializing'],
      ['add', 'updateannotation'],
    ];
    page.nsDetailPage.updateannotation(name, testDataAnn);
    const expectData = {
      注解: testDataAnn,
    };
    page.nsDetailVerifyPage.verify(expectData);
  });
  it('ACP2UI-55264 : 对纳管命名空间进行更新-更新标签，成功', () => {
    const testDataLabel = [
      [`${ServerConf.LABELBASEDOMAIN}/cluster`, ServerConf.REGIONNAME],
      [`${ServerConf.LABELBASEDOMAIN}/project`, project],
      ['add', 'updatelabel'],
    ];
    page.nsDetailPage.updatelabel(name, testDataLabel);
    const expectData = {
      标签: testDataLabel,
    };
    page.nsDetailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55265 : 对纳管命名空间进行移除纳管操作', () => {
    page.nsListPage.removenamespace(name);
    // 验证搜索正确
    page.nsListPage.searchnamespace(name, 0);
    page.nsListVerifyPage.verify({
      数量: 0,
    });
  });
  it('ACP2UI-55283 : 移除命名空间后，再次重新导入该命名空间', () => {
    const testData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name,
        显示名称: name.toUpperCase(),
      },
      容器限额: {
        CPU: {
          默认值: '5',
          最大值: '5',
        },
        内存: {
          默认值: '10',
          最大值: '10',
        },
      },
    };
    page.nsImportPage.import_ns(testData);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        显示名称: name.toUpperCase(),
        所属集群: ServerConf.REGIONNAME,
      },
      容器限额: {
        CPU: {
          默认值: '5m',
          最大值: '5m',
        },
        内存: {
          默认值: '10Mi',
          最大值: '10Mi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
  it('ACP2UI-55266 : 对纳管后的命名空间进行删除操作', () => {
    page.nsListPage.deletenamespace(name);
    // 验证搜索正确
    page.nsListPage.searchnamespace(name, 1);
    page.nsListVerifyPage.verify({
      数量: 1,
      状态: { 名称: name, 状态: '删除中' },
    });
  });
});
