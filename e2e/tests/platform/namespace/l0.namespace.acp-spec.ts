import { ServerConf } from '@e2e/config/serverConf';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';
import { browser } from 'protractor';

describe('项目管理 命名空间L0自动化', () => {
  const page = new NamespacePage();
  const project = page.projectName;
  const ns_name = page.getTestData('ns-l0');
  const namespace_name = `${project}-${ns_name}`;
  const ns_display_name = ns_name.toLocaleUpperCase();
  beforeAll(() => {
    page.nsPrepare.deleleNs(namespace_name);
    page.login();
    page.switchSetting('项目管理');
    page.projectlistPage.enterProject(project);
  });
  afterAll(() => {
    page.nsPrepare.deleleNs(namespace_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('命名空间');
  });
  it('ACP2UI-53777 : 单击左导航命名空间，在列表页单击创建命名空间按钮，创建命名空间,只填必填项，成功', () => {
    const testData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: ns_name,
        显示名称: ns_display_name,
      },
      资源配额: {
        CPU: '10',
        内存: '10',
        存储: '10',
        'PVC 数': '10',
        'Pods 数': '10',
      },
      容器限额: {
        CPU: {
          默认值: '5',
          最大值: '5',
        },
        内存: {
          默认值: '10',
          最大值: '10',
        },
      },
    };
    page.nsCreatePage.create_ns(testData);
    browser.sleep(1).then(() => {
      page.waitResourceExist('resourcequota', 'default', namespace_name, true);
      page.waitResourceExist('limitrange', 'default', namespace_name, true);
      browser.refresh();
      const v_Data = {
        基本信息: {
          命名空间名称: namespace_name,
          显示名称: ns_display_name,
          所属集群: ServerConf.REGIONNAME,
        },
        资源配额: {
          CPU: '10',
          内存: '10',
          存储: '10',
          'PVC 数': '10',
          'Pods 数': '10',
        },
        容器限额: {
          CPU: {
            默认值: '5m',
            最大值: '5m',
          },
          内存: {
            默认值: '10Mi',
            最大值: '10Mi',
          },
        },
      };
      page.nsDetailVerifyPage.verify(v_Data);
    });
  });
  it('ACP2UI-53778 : 单击左导航命名空间，在列表页按名称搜索，成功', () => {
    page.nsListPage.searchnamespace(namespace_name, 1);
    page.nsListVerifyPage.verify({
      数量: 1,
    });
  });
  it('ACP2UI-53783 : 单击左导航命名空间，点击名称进入详情页，点击操作-更新资源配额，成功', () => {
    const testData = {
      CPU: '2',
      内存: '1',
      存储: '1',
      'PVC 数': '1',
      'Pods 数': '10',
    };
    page.nsListPage.updatenamespacequota_detail(namespace_name, testData);

    page.nsDetailPage.switchtab('命名空间成员');
    page.nsDetailPage.switchtab('详情信息');
    const v_data = {
      资源配额: testData,
    };
    page.nsDetailVerifyPage.verify(v_data);
  });
  it('ACP2UI-53785 : 单击左导航命名空间，点击名称进入详情页，点击操作-删除，成功', () => {
    page.nsListPage.deletenamespace(namespace_name);
    page.nsListVerifyPage.verify({
      状态: {
        名称: namespace_name,
        状态: '删除中',
      },
    });
    browser.sleep(1).then(() => {
      page.waitResourceExist('namespace', namespace_name, '', false);
      browser.refresh();
      // 验证搜索正确
      page.nsListPage.searchnamespace(namespace_name, 0);
      page.nsListVerifyPage.verify({
        数量: 0,
      });
    });
  });
});
