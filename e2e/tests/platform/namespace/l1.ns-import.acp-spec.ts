import { ServerConf } from '@e2e/config/serverConf';
import { NamespacePage } from '@e2e/page_objects/platform/namespace/namespace.page';

xdescribe('项目管理 命名空间导入L1自动化', () => {
  const page = new NamespacePage();
  const project = page.projectName;
  const name = page.getTestData('all-import');
  const name_only = page.getTestData('only-ns');
  const name_unit1 = page.getTestData('unit1');
  const name_unit2 = page.getTestData('unit2');
  const name_quota = page.getTestData('quota');
  const name_quota2 = page.getTestData('no-quota');
  const name_display = page.getTestData('display');
  const name_label = page.getTestData('label');
  const name_label1 = page.getTestData('add-label');
  const name_label2 = page.getTestData('update-abel');
  const name_label3 = page.getTestData('del-label');

  beforeAll(() => {
    page.nsPrepare.deleleNs(name);
    page.nsPrepare.deleleNs(name_only);
    page.nsPrepare.deleleNs(name_unit1);
    page.nsPrepare.deleleNs(name_unit2);
    page.nsPrepare.deleleNs(name_quota);
    page.nsPrepare.deleleNs(name_quota2);
    page.nsPrepare.deleleNs(name_display);
    page.nsPrepare.deleleNs(name_label);
    page.nsPrepare.deleleNs(name_label1);
    page.nsPrepare.deleleNs(name_label2);
    page.nsPrepare.deleleNs(name_label3);
    page.login();
    page.switchSetting('项目管理');
    page.projectlistPage.enterProject(project);
  });
  beforeEach(() => {
    page.clickLeftNavByText('命名空间');
  });
  afterAll(() => {
    page.nsPrepare.deleleNs(name);
    page.nsPrepare.deleleNs(name_only);
    page.nsPrepare.deleleNs(name_unit1);
    page.nsPrepare.deleleNs(name_unit2);
    page.nsPrepare.deleleNs(name_quota);
    page.nsPrepare.deleleNs(name_quota2);
    page.nsPrepare.deleleNs(name_display);
    page.nsPrepare.deleleNs(name_label);
    page.nsPrepare.deleleNs(name_label1);
    page.nsPrepare.deleleNs(name_label2);
    page.nsPrepare.deleleNs(name_label3);
  });
  it('ACP2UI-55252 : 导入有资源配额和容器限额的命名空间-限额名字都是default', () => {
    const createAllData = {
      name: name,
      limitrange: true,
      resourcequota: true,
    };
    page.nsPrepare.createNs(createAllData);
    const testData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name,
      },
    };
    page.nsImportPage.import_ns(testData);
    const v_Data = {
      基本信息: {
        命名空间名称: name,
        所属集群: ServerConf.REGIONNAME,
      },
      资源配额: {
        CPU: '1',
        内存: '1',
        存储: '1',
        'PVC 数': '1',
        'Pods 数': '1',
      },
      容器限额: {
        CPU: {
          默认值: '10m',
          最大值: '1 核',
        },
        内存: {
          默认值: '10Mi',
          最大值: '1Gi',
        },
      },
    };
    page.nsDetailVerifyPage.verify(v_Data);
  });
  it('ACP2UI-55279 : 导入命名空间后，检查命名空间列表有该命名空间的数据存在', () => {
    page.nsListPage.searchnamespace(name, 1);
    page.nsListVerifyPage.verify({
      数量: 1,
    });
  });
  // //jira.alauda.cn/browse/ACP-1079
  // http: it('ACP2UI-55256 : 导入没有资源配额和容器限额的命名空间', () => {
  //   const name_only = page.getTestData('only-ns');
  //   const createOnlyData = {
  //     name: name_only,
  //   };
  //   page.nsPrepare.createNs(createOnlyData);
  //   const testData = {
  //     基本信息: {
  //       所属集群: ServerConf.REGIONNAME,
  //       命名空间: name_only,
  //     },
  //     容器限额: {
  //       CPU: {
  //         默认值: '10',
  //         最大值: '1000',
  //       },
  //       内存: {
  //         默认值: '10',
  //         最大值: '1024',
  //       },
  //     },
  //   };
  //   page.nsImportPage.import_ns(testData);
  //   const v_Data = {
  //     基本信息: {
  //       命名空间名称: name_only,
  //       所属集群: ServerConf.REGIONNAME,
  //     },
  //     容器限额: {
  //       CPU: {
  //         默认值: '10m',
  //         最大值: '1 核',
  //       },
  //       内存: {
  //         默认值: '10Mi',
  //         最大值: '1Gi',
  //       },
  //     },
  //   };
  //   page.nsDetailVerifyPage.verify(v_Data);
  // });
  it('ACP2UI-55263 : 导入命名空间，检查前端获取到的命名空间列表是正确的', () => {
    //已经关联到项目的ns
    const v1_Data = {
      验证命名空间不存在: {
        命名空间: 'uiauto-acp2-ns1',
        所属集群: ServerConf.REGIONNAME,
      },
    };
    page.nsDetailVerifyPage.verify(v1_Data);
    //系统级别的ns
    const s_Data = {
      验证命名空间不存在: {
        命名空间: `${ServerConf.GLOBAL_NAMESPCE}`,
        所属集群: ServerConf.REGIONNAME,
      },
    };
    page.nsDetailVerifyPage.verify(s_Data);
    const v2_Data = {
      验证命名空间存在: {
        命名空间: 'cert-manager',
        所属集群: ServerConf.REGIONNAME,
      },
    };
    page.nsDetailVerifyPage.verify(v2_Data);
  });
  it('ACP2UI-55268 : 导入命名空间，检查前端获取到的命名空间配额信息是正确的,资源的单位需要特别关注', () => {
    const createData = {
      name: name_unit1,
      limitrange: true,
      resourcequota: true,
      cpu: '1',
      memory: '1Gi',
      storage: '1Gi',
    };
    page.nsPrepare.createNs(createData);
    const testGData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name_unit1,
      },
    };
    page.nsImportPage.import_ns(testGData);
    const g_Data = {
      资源配额: {
        CPU: '1 核',
        内存: '1 Gi',
        存储: '1 Gi',
      },
    };
    page.nsDetailVerifyPage.verify(g_Data);
    const createTData = {
      name: name_unit2,
      limitrange: true,
      resourcequota: true,
      memory: '1Ti',
      storage: '1Ti',
    };
    page.nsPrepare.createNs(createTData);
    const testTData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name_unit2,
      },
    };
    page.nsImportPage.import_ns(testTData);
    const t_Data = {
      资源配额: {
        内存: '1024 Gi',
        存储: '1024 Gi',
      },
    };
    page.nsDetailVerifyPage.verify(t_Data);
  });
  it('ACP2UI-55273 : 导入命名空间时，可以修改原有的资源配额-只支持正整数-配额校验正确', () => {
    const createData = {
      name: name_quota,
      limitrange: true,
      resourcequota: true,
    };
    page.nsPrepare.createNs(createData);
    const testGData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name_quota,
      },
      资源配额: {
        CPU: '2',
        内存: '2',
        存储: '2',
        'Pods 数': '2',
        'PVC 数': '2',
      },
    };
    page.nsImportPage.import_ns(testGData);
    const q_Data = {
      资源配额: {
        CPU: '2 核',
        内存: '2 Gi',
        存储: '2 Gi',
        'PVC 数': '2 个',
        'Pods 数': '2 个',
      },
    };
    page.nsDetailVerifyPage.verify(q_Data);
  });
  it('ACP2UI-55274 : 导入命名空间时，如果没有配额，可以手动新增配额-支持正整数-配额校验正确', () => {
    const createData = {
      name: name_quota2,
      limitrange: true,
      resourcequota: false,
    };
    page.nsPrepare.createNs(createData);
    const testGData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name_quota2,
      },
      资源配额: {
        CPU: '2',
        内存: '2',
        存储: '2',
        'Pods 数': '2',
        'PVC 数': '2',
      },
    };
    page.nsImportPage.import_ns(testGData);
    const q_Data = {
      资源配额: {
        CPU: '2 核',
        内存: '2 Gi',
        存储: '2 Gi',
        'PVC 数': '2 个',
        'Pods 数': '2 个',
      },
    };
    page.nsDetailVerifyPage.verify(q_Data);
  });
  it('ACP2UI-55272 : 导入命名空间时，可以新增显示名称', () => {
    const createData = {
      name: name_display,
      limitrange: true,
    };
    page.nsPrepare.createNs(createData);
    const testGData = {
      基本信息: {
        所属集群: ServerConf.REGIONNAME,
        命名空间: name_display,
        显示名称: name_display.toUpperCase(),
      },
    };
    page.nsImportPage.import_ns(testGData);
    const q_Data = {
      基本信息: {
        显示名称: name_display.toUpperCase(),
      },
    };
    page.nsDetailVerifyPage.verify(q_Data);
  });
  // http://jira.alauda.cn/browse/ACP-1099
  // it('ACP2UI-55267 : 导入命名空间-检查命名空间的标签和注解显示正确', () => {
  //   const createData = {
  //     name: name_label,
  //     limitrange: true,
  //     labels: true,
  //     annotations: true,
  //     displayname: true,
  //   };
  //   page.nsPrepare.createNs(createData);
  //   const testGData = {
  //     基本信息: {
  //       所属集群: ServerConf.REGIONNAME,
  //       命名空间: `${name_label} (uiauto-display-name)`,
  //     },
  //   };
  //   page.nsImportPage.import_ns(testGData);
  //   const testDataAnn = [['uiauto-annotations', 'uiauto-annotations']];
  //   const testDataLabel = [['uiauto-label', 'uiauto-label']];
  //   const q_Data = {
  //     基本信息: {
  //       显示名称: 'uiauto-display-name',
  //     },
  //     注解: testDataAnn,
  //     标签: testDataLabel,
  //   };
  //   page.nsDetailVerifyPage.verify(q_Data);
  // });
  // it('ACP2UI-55269 : 导入命名空间时，手动添加新的label和注解', () => {
  //   const createData = {
  //     name: name_label1,
  //     limitrange: true,
  //     labels: true,
  //     annotations: true,
  //     displayname: true,
  //   };
  //   page.nsPrepare.createNs(createData);
  //   const testGData = {
  //     基本信息: {
  //       所属集群: ServerConf.REGIONNAME,
  //       命名空间: `${name_label1} (uiauto-display-name)`,
  //     },
  //     更多配置: {
  //       标签: [
  //         ['', ''],
  //         ['', ''],
  //         ['uiauto-label', 'uiauto-label'],
  //         ['uiauto-add-label', 'uiauto-add-label'],
  //       ],
  //       注解: [
  //         ['', ''],
  //         ['uiauto-annotations', 'uiauto-annotations'],
  //         ['uiauto-add-annotations', 'uiauto-add-annotations'],
  //       ],
  //     },
  //   };
  //   page.nsImportPage.import_ns(testGData);
  //   const testDataAnn = [
  //     ['uiauto-add-annotations', 'uiauto-add-annotations'],
  //     ['uiauto-annotations', 'uiauto-annotations'],
  //   ];
  //   const testDataLabel = [
  //     ['uiauto-add-label', 'uiauto-add-label'],
  //     ['uiauto-label', 'uiauto-label'],
  //   ];
  //   const q_Data = {
  //     基本信息: {
  //       显示名称: 'uiauto-display-name',
  //     },
  //     注解: testDataAnn,
  //     标签: testDataLabel,
  //   };
  //   page.nsDetailVerifyPage.verify(q_Data);
  // });
  // it('ACP2UI-55270 : 导入命名空间时，可以对默认标签和注解进行更新', () => {
  //   const createData = {
  //     name: name_label2,
  //     limitrange: true,
  //     labels: true,
  //     annotations: true,
  //     displayname: true,
  //   };
  //   page.nsPrepare.createNs(createData);
  //   const testGData = {
  //     基本信息: {
  //       所属集群: ServerConf.REGIONNAME,
  //       命名空间: `${name_label2} (uiauto-display-name)`,
  //     },
  //     更多配置: {
  //       标签: [
  //         ['', ''],
  //         ['', ''],
  //         ['uiauto-update-label', 'uiauto-update-label'],
  //       ],
  //       注解: [
  //         ['', ''],
  //         ['uiauto-update-annotations', 'uiauto-update-annotations'],
  //       ],
  //     },
  //   };
  //   page.nsImportPage.import_ns(testGData);
  //   const testDataAnn = [
  //     ['uiauto-update-annotations', 'uiauto-update-annotations'],
  //   ];
  //   const testDataLabel = [['uiauto-update-label', 'uiauto-update-label']];
  //   const q_Data = {
  //     注解: testDataAnn,
  //     标签: testDataLabel,
  //   };
  //   page.nsDetailVerifyPage.verify(q_Data);
  // });
  // it('ACP2UI-55270 : 导入命名空间时，可以对默认标签和注解进行删除', () => {
  //   const createData = {
  //     name: name_label3,
  //     limitrange: true,
  //     labels: true,
  //     annotations: true,
  //     displayname: true,
  //   };
  //   page.nsPrepare.createNs(createData);
  //   const testGData = {
  //     基本信息: {
  //       所属集群: ServerConf.REGIONNAME,
  //       命名空间: `${name_label3} (uiauto-display-name)`,
  //     },
  //     更多配置: {
  //       标签: [['', ''], ['', ''], ['uiauto-label', 'uiauto-label', true]],
  //       注解: [['', ''], ['uiauto-annotations', 'uiauto-annotations', true]],
  //     },
  //   };
  //   page.nsImportPage.import_ns(testGData);
  //   const testDataAnn = [['uiauto-annotations', 'uiauto-annotations']];
  //   const testDataLabel = [['uiauto-label', 'uiauto-label']];
  //   const q_Data = {
  //     不包含注解: testDataAnn,
  //     不包含标签: testDataLabel,
  //   };
  //   page.nsDetailVerifyPage.verify(q_Data);
  // });
});
