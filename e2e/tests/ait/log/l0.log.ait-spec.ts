// import { LogPage } from '@e2e/page_objects/ait/log/log.page';
// import { browser } from 'protractor';

// describe('管理视图日志 L0 case', () => {
//   const page = new LogPage();
//   const appName = page.getTestData('logapp');
//   const queryString = `application: ${appName}.${page.namespace1Name}`;

//   beforeAll(() => {
//     page.preparePage.deleteApp(appName, page.namespace1Name);
//     page.preparePage.createApp(appName, page.namespace1Name);
//     page.login();
//     page.enterPlatformView('运维中心');
//     browser.sleep(1000);
//   });
//   afterAll(() => {
//     page.preparePage.deleteApp(appName, page.namespace1Name);
//   });
//   beforeEach(() => {});
//   it('ACP2UI-4098 : L0:验证日志面板', () => {
//     page.listPage.search(queryString);
//     const exceptData = {
//       日志: ['hehe'],
//       时间范围: [
//         '过去30分钟',
//         '过去1小时',
//         '过去6小时',
//         '过去1天',
//         '过去3天',
//         '过去7天',
//         '自定义时间',
//       ],
//       柱状图: 31,
//       日志个数: 31,
//     };
//     page.listPageVerify.verify(exceptData);
//   });
// });
