import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';
import { browser } from 'protractor';

describe('管理视图告警模板 L0 case', () => {
  const page = new AlarmPage();
  const alarmtmp_clusterquota_name = page.getTestData('alarmtmpl0-cluster1');
  const alarmtmp_nodequota_name = page.getTestData('alarmtmpl0-node1');
  const alarmtmp_workloadquota_name = page.getTestData('alarmtmpl0-workload1');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_clusterquota_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_nodequota_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workloadquota_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
    browser.sleep(500);
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_clusterquota_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_nodequota_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workloadquota_name);
  });

  // 创建集群的指标告警
  it('ACP2UI-55412 : 创建告警模板-资源类型选择集群-选择指标告警-输入阈值其他默认-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_clusterquota_name,
        描述: alarmtmp_clusterquota_name.toUpperCase(),
        资源类型: '集群',
      },
      告警规则1: {
        阈值: ['>', '98'],
        验证规则: [
          'cluster.alerts.firing > 98 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_clusterquota_name,
        描述: alarmtmp_clusterquota_name.toUpperCase(),
        资源类型: '集群',
      },
      告警规则: [
        [
          'cluster.alerts.firing > 98 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55413 : 创建告警模板-资源类型选择主机-选择指标告警-输入阈值其他默认-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_nodequota_name,
        描述: alarmtmp_nodequota_name.toUpperCase(),
        资源类型: '主机',
      },
      告警规则1: {
        阈值: ['>', '90'],
        验证规则: [
          'node.cpu.utilization > 90% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_nodequota_name,
        描述: alarmtmp_nodequota_name.toUpperCase(),
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.cpu.utilization > 90% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55414 : 创建告警模板-资源类型选择计算组件-选择指标告警-输入阈值其他默认-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workloadquota_name,
        描述: alarmtmp_workloadquota_name.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        阈值: ['>', '88.8865'],
        验证规则: [
          'container.cpu.utilization > 88.8865% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workloadquota_name,
        描述: alarmtmp_workloadquota_name.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'container.cpu.utilization > 88.8865% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });
});
