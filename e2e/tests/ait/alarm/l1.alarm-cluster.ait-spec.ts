import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警 L1 case', () => {
  const page = new AlarmPage();
  const alarm_cluster_name1 = page.getTestData('alarml1-cluster1');
  const alarm_cluster_name2 = page.getTestData('alarml1-cluster2');
  const alarm_cluster_name3 = page.getTestData('alarml1-cluster3');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarm(alarm_cluster_name1);
    page.preparePage.deleteAlarm(alarm_cluster_name2);
    page.preparePage.deleteAlarm(alarm_cluster_name3);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
    page.switchCluster_onAdminView(page.clusterName);
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarm(alarm_cluster_name1);
    page.preparePage.deleteAlarm(alarm_cluster_name2);
    page.preparePage.deleteAlarm(alarm_cluster_name3);
  });

  it('ACP2UI-57340 : 创建告警-资源类型选择集群-添加1条指标告警(选择有聚合类型的指标,修改各项值)-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarm_cluster_name1,
        描述: alarm_cluster_name1.toUpperCase(),
        资源类型: '集群',
      },
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'cluster.cpu.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_cluster_name1,
        描述: alarm_cluster_name1.toUpperCase(),
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55454 : 创建告警-资源类型选择集群-添加3条指标告警规则(修改各项值)并分别添加标签和注解-选择多个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarm_cluster_name2,
        资源类型: '集群',
      },
      告警规则1: {
        指标: 'cluster.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '3分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '5分钟',
        高级: {
          标签: [['aaaa1', 'aaaaaa1'], ['vvv1', 'vvvvv1']],
          注解: [['aaaa1', 'aaaaaa1'], ['vvv1', 'vvvvv1']],
        },
        验证规则: [
          'cluster.memory.utilization > 99.999% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则2: {
        指标: 'cluster.kube.apiserver.health',
        告警等级: 'Critical',
        阈值: ['<=', '90'],
        持续时间: '5分钟',
        高级: {
          标签: [['aaaa2', 'aaaaaa2'], ['vvv2', 'vvvvv2']],
          注解: [['aaaa2', 'aaaaaa2'], ['vvv2', 'vvvvv2']],
        },
        验证规则: [
          'cluster.kube.apiserver.health <= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则3: {
        指标: 'cluster.kube.apiserver.request.count.2xx',
        告警等级: 'Low',
        阈值: ['==', '20'],
        持续时间: '3分钟',
        高级: {
          标签: [['aaaa3', 'aaaaa3'], ['vvv3', 'vvvvv3']],
          注解: [['aaaa3', 'aaaaaa3'], ['vvv3', 'vvvvv3']],
        },
        验证规则: [
          'cluster.kube.apiserver.request.count.2xx == 20 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_cluster_name2,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.memory.utilization > 99.999% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
        [
          '',
          'cluster.kube.apiserver.health <= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
        [
          '',
          'cluster.kube.apiserver.request.count.2xx == 20 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55448 : 创建告警-资源类型选择集群-添加1条自定义告警(输入各项值，添加标签和注解)-点击创建创建成功 ', () => {
    const data = {
      基本信息: {
        名称: alarm_cluster_name3,
        描述: alarm_cluster_name3.toUpperCase(),
        资源类型: '集群',
      },
      告警规则1: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'core',
        告警等级: 'Low',
        阈值: ['<=', '1000'],
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_cluster_name3,
        描述: alarm_cluster_name3.toUpperCase(),
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55512 : 更新告警-选择资源类型是集群的指标告警更新-列表页点击更新-更新描述-更新原有的告警规则-点击更新更新成功', () => {
    page.listPage.update(alarm_cluster_name1);
    const testData = {
      基本信息: {
        描述: 'a',
      },
      更新告警规则1: {
        聚合时间: '5分钟',
        聚合方式: '平均值',
        告警等级: 'Medium',
        阈值: ['>', '99'],
        持续时间: '5分钟',
        验证规则: [
          'cluster.cpu.utilization > 99% 且持续时间达到 5 分钟',
          '指标告警',
          'Medium',
        ],
      },
    };
    page.updateAlarm(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    page.listPage.enterDetail(alarm_cluster_name1);
    const expectData1 = {
      基本信息: {
        名称: alarm_cluster_name1,
        描述: 'a',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization > 99% 且持续时间达到 5 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });

  it('ACP2UI-57334 : 更新告警-选择资源类型是集群的告警-详情页点击更新-删除1条老的告警规则-添加2条新的告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail(alarm_cluster_name2);
    page.detailPage.detailPage_operate('更新');
    const testData = {
      基本信息: {
        描述: alarm_cluster_name2.toUpperCase(),
      },
      删除告警规则1: 'click',
      告警规则3: {
        指标: 'cluster.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '10分钟',
        聚合方式: '最小值',
        告警等级: 'Critical',
        阈值: ['>', '99.998'],
        持续时间: '10分钟',
        验证规则: [
          'cluster.memory.utilization > 99.998% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则4: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'core',
        告警等级: 'Low',
        阈值: ['<=', '1000'],
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      },
    };
    page.updateAlarm(testData);
    const expectData1 = {
      基本信息: {
        名称: alarm_cluster_name2,
        描述: alarm_cluster_name2.toUpperCase(),
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.kube.apiserver.health <= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
        [
          '',
          'cluster.kube.apiserver.request.count.2xx == 20 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
        [
          '',
          'cluster.memory.utilization > 99.998% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
        [
          '',
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });
});
