import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';
import { browser } from 'protractor';

describe('管理视图告警模板+使用模板创建告警 L0 case', () => {
  const page = new AlarmPage();
  const alarmtmp_cluster_name = page.getTestData('alarmtmpl0-cluster2');
  const alarmtmp_node_name = page.getTestData('alarmtmpl0-node2');
  const alarmtmp_workload_name = page.getTestData('alarmtmpl0-workload2');

  const alarm_cluster_name = page.getTestData('usetmp-alarml0-1');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_cluster_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name);
    page.preparePage.deleteAlarm(alarm_cluster_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
    browser.sleep(500);
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_cluster_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name);
    page.preparePage.deleteAlarm(alarm_cluster_name);
  });
  // 前三个用例是测试模板创建告警，为后面使用模板创建告警而用
  it('ACP2UI-55415 : 创建告警模板-资源类型选择集群-添加2条告警规则分别是指标告警和自定义告警(修改各项值)-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_cluster_name,
        资源类型: '集群',
      },
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则2: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'core',
        告警等级: 'Low',
        阈值: ['<=', '1000'],
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_cluster_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
        [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55416 : 创建告警模板-资源类型选择主机-添加2条告警规则分别是指标告警和自定义告警(修改各项值)-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_node_name,
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.memory.utilization',
        告警等级: 'Critical',
        阈值: ['>=', '97.7658'],
        持续时间: '5分钟',
        验证规则: [
          'node.memory.utilization >= 97.7658% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则2: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'byte/second',
        阈值: ['!=', '1000'],
        验证规则: [
          'sum_over_time != 1000byte/second 且持续时间达到 1 分钟',
          '自定义告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_node_name,
        描述: '-',
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.memory.utilization >= 97.7658% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
        [
          'sum_over_time != 1000byte/second 且持续时间达到 1 分钟',
          '自定义告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-55417 : 创建告警模板-资源类型选择计算组件-添加4条告警规则分别是指标告警，日志告警，事件告警，自定义告警(修改各项值)-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name,
        资源类型: '工作负载',
      },
      告警规则1: {
        指标: 'workload.pod.status.phase.running',
        数据类型: '聚合值',
        聚合时间: '2分钟',
        告警等级: 'High',
        阈值: ['>', '10'],
        持续时间: '5分钟',
        验证规则: [
          'workload.pod.status.phase.running > 10 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则2: {
        告警类型: '日志告警',
        日志内容: '错误',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '1 分钟内，包含 错误 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
      告警规则3: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '10'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 10 条',
          '事件告警',
          'Medium',
        ],
      },
      告警规则4: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'kb',
        阈值: ['!=', '100'],
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time != 100kb 且持续时间达到 5 分钟',
          '自定义告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name,
        描述: '-',
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'workload.pod.status.phase.running > 10 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        ['1 分钟内，包含 错误 信息的日志 > 20 条', '日志告警', 'High'],
        ['1 分钟内，包含 Failed 信息的事件 > 10 条', '事件告警', 'Medium'],
        [
          'sum_over_time != 100kb 且持续时间达到 5 分钟',
          '自定义告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  // 下面是使用告警模板创建告警
  it('ACP2UI-54350 : 模板创建告警-告警名称-选择类型是集群的模板名称-点击创建', () => {
    page.clickLeftNavByText('告警');
    page.switchCluster_onAdminView(page.clusterName);
    browser.sleep(500);
    const data = {
      告警名称: alarm_cluster_name,
      模板名称: alarmtmp_cluster_name,
      资源类型: '集群',
      资源名称: page.clusterName,
      告警规则: [
        [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
        [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
    };
    page.createAlarm_useTmp(data);
    const expectData = {
      基本信息: {
        名称: alarm_cluster_name,
        描述: '-',
        资源类型: '集群',
        资源名称: page.clusterName,
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
        [
          '',
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });
});
