import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';

describe('管理视图告警模板+使用模板创建告警 L0 case', () => {
  const page = new AlarmPage();
  const app_page = new Application();
  const deploy_list_page = new DeploymentListPage();
  const appName = 'auto-ait-appdeploy1';
  const deployName = `${appName}-qaimages`;
  const alarm_app_name1 = page.getTestData('app-alarml1-1'); // 2条指标告警
  const alarm_app_name2 = page.getTestData('app-alarml1-2'); // 日志告警
  const alarm_app_name3 = page.getTestData('app-alarml1-3'); // 事件告警
  const alarm_app_name4 = page.getTestData('app-alarml1-4'); // 自定义告警
  const alarm_app_name5 = page.getTestData('app-alarml1-5'); // 多条告警规则
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarm(alarm_app_name1, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name2, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name3, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name4, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name5, page.namespace1Name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    page.preparePage.deleteAlarm(alarm_app_name1, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name2, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name3, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name4, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name5, page.namespace1Name);
  });

  it('ACP2UI-55427 : 部署方式为部署-创建告警-名称-添加2条指标告警规则-数据类型选择原始值和聚合值-聚合时间选择1分钟-输入阈值-持续时间2分钟-点击创建并创建成功', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name1,
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        指标: 'pod.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'pod.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则2: {
        指标: 'pod.memory.utilization',
        数据类型: '聚合值',
        告警等级: 'Low',
        阈值: ['>=', '90'],
        持续时间: '5分钟',
        验证规则: [
          'pod.memory.utilization >= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name1,
        描述: '-',
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'pod.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
        [
          '',
          'pod.memory.utilization >= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });

  it('ACP2UI-55428 : 部署方式为部署-创建告警-名称,描述-添加日志告警-输入日志内容-告警等级选择High-输入阈值-添加多个标签-点击创建并成功', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name2,
        描述: alarm_app_name2.toUpperCase(),
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        告警类型: '日志告警',
        时间范围: '2分钟',
        日志内容: '非常严重的错误Pod出错啦集群出错啦节点出错啦',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '2 分钟内，包含 非常严重的错误Pod出错啦集群出错啦节点出错啦 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name2,
        描述: alarm_app_name2.toUpperCase(),
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          '2 分钟内，包含 非常严重的错误Pod出错啦集群出错啦节点出错啦 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });

  it('ACP2UI-55429 : 部署方式为部署-创建告警-名称-添加事件告警-时间范围选择2分钟-输入事件内容+low+‘《=’+输入阈值+High-添加多个注解-点击添加-点击创建', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name3,
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '11'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 11 条',
          '事件告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name3,
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        ['', '1 分钟内，包含 Failed 信息的事件 > 11 条', '事件告警', 'Medium'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });

  it('ACP2UI-55430 : 部署方式为部署-创建告警-名称-添加自定义告警规则-输入指标-输入或选择单位-输入阈值-持续时间选择5分钟-点击添加-添加一个通知-点击创建', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name4,
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: '%',
        告警等级: 'Low',
        阈值: ['>', '1000'],
        持续时间: '2分钟',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name4,
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });

  it('ACP2UI-55431 : 部署方式为部署-创建告警-名称-添加多个告警规则-点击创建', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name5,
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        指标: 'workload.cpu.utilization',
        数据类型: '聚合值',
        聚合时间: '5分钟',
        告警等级: 'High',
        阈值: ['>=', '80'],
        验证规则: [
          'workload.cpu.utilization >= 80% 且持续时间达到 1 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则2: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '11'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 11 条',
          '事件告警',
          'Medium',
        ],
      },
      告警规则3: {
        告警类型: '日志告警',
        时间范围: '2分钟',
        日志内容: '错误',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '2 分钟内，包含 错误 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
      告警规则4: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: '%',
        告警等级: 'Low',
        阈值: ['>', '1000'],
        持续时间: '2分钟',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name5,
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'workload.cpu.utilization >= 80% 且持续时间达到 1 分钟',
          '指标告警',
          'High',
        ],
        ['', '1 分钟内，包含 Failed 信息的事件 > 11 条', '事件告警', 'Medium'],
        ['', '2 分钟内，包含 错误 信息的日志 > 20 条', '日志告警', 'High'],
        [
          '',
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });

  // 更新告警
  it('ACP2UI-55526 : 计算组件详情页-更新告警-选择指标告警更新-列表页点击更新-更新描述,告警规则,指标,数据类型,持续时间,高级-点击更新并更新成功', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.update(alarm_app_name1);
    const testData = {
      基本信息: {
        描述: alarm_app_name1.toUpperCase(),
        资源类型: '工作负载',
        资源名称: deployName,
      },
      更新告警规则1: {
        持续时间: '5分钟',
        验证规则: [
          'pod.cpu.utilization >= 98% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
    };
    page.updateAlarmApp(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    page.listPage.enterDetail_AlarmappList(alarm_app_name1);
    const expectData1 = {
      基本信息: {
        名称: alarm_app_name1,
        描述: alarm_app_name1.toUpperCase(),
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'pod.cpu.utilization >= 98% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        [
          '',
          'pod.memory.utilization >= 90% 且持续时间达到 5 分钟',
          '指标告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData1);
  });
  it('ACP2UI-55527 : 计算组件详情页-更新告警-选择自定义告警更新-详情页点击更新-更新告警规则,指标,单位,告警等级-更新操作指令-点击更新按钮并更新成功', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.enterDetail_AlarmappList(alarm_app_name4);
    page.detailPage.detailPage_operate('更新');
    const testData = {
      基本信息: {
        描述: alarm_app_name4.toUpperCase(),
        资源类型: '工作负载',
        资源名称: deployName,
      },
      更新告警规则1: {
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      },
    };
    page.updateAlarmApp(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    const expectData1 = {
      基本信息: {
        名称: alarm_app_name4,
        描述: alarm_app_name4.toUpperCase(),
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'sum_over_time > 1000% 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData1);
  });
  it('ACP2UI-55528 : 应用详情页-更新告警-选择日志告警更新-列表页点击更新-更新告警规则,事件范围,日志内容,阈值-点击更新并更新成', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.update(alarm_app_name2);
    const testData = {
      基本信息: {
        描述: alarm_app_name2.toUpperCase(),
        资源类型: '工作负载',
        资源名称: deployName,
      },
      更新告警规则1: {
        时间范围: '5分钟',
        验证规则: [
          '5 分钟内，包含 非常严重的错误Pod出错啦集群出错啦节点出错啦 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
    };
    page.updateAlarmApp(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    page.listPage.enterDetail_AlarmappList(alarm_app_name2);
    const expectData1 = {
      基本信息: {
        名称: alarm_app_name2,
        描述: alarm_app_name2.toUpperCase(),
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          '5 分钟内，包含 非常严重的错误Pod出错啦集群出错啦节点出错啦 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData1);
  });

  it('ACP2UI-55529 : 应用详情页-更新告警-选择事件告警更新-详情页点击更新-更新告警规则,时间范围,事件内容,阈值-点击更新并更新成功', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.enterDetail_AlarmappList(alarm_app_name3);
    page.detailPage.detailPage_operate('更新');
    const testData = {
      基本信息: {
        描述: alarm_app_name3.toUpperCase(),
        资源类型: '工作负载',
        资源名称: deployName,
      },
      更新告警规则1: {
        时间范围: '2分钟',
        验证规则: [
          '2 分钟内，包含 Failed 信息的事件 > 11 条',
          '事件告警',
          'Medium',
        ],
      },
    };
    page.updateAlarmApp(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    const expectData1 = {
      基本信息: {
        名称: alarm_app_name3,
        描述: alarm_app_name3.toUpperCase(),
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        ['', '2 分钟内，包含 Failed 信息的事件 > 11 条', '事件告警', 'Medium'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData1);
  });

  it('ACP2UI-55532 : 计算组件详情页-删除告警-列表页页点击删除-确认删除框点击确认按钮', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.delete_AlarmappList(alarm_app_name1);
    page.listPage.search_AlarmappList(alarm_app_name1, 0);
    const expectData = { 无数据: true };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-55534 : 应用详情页-删除告警-详情页点击删除-确认删除框点击确定按钮', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    page.listPage.enterDetail_AlarmappList(alarm_app_name2);
    page.detailPage.detailPage_operate('删除');
    page.confirmDialog.clickConfirm(); // 点击删除确认框确定按钮
    // 删除后跳转回列表页
    page.listPage.search_AlarmappList(alarm_app_name2, 0);
    const expectData = { 无数据: true };
    page.listPageVerify.verify(expectData);
  });
});
