import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警模板 L3 case', () => {
  const page = new AlarmPage();
  const alarmtmp_clusterdeall_name = page.getTestData('alarmtmpl3-cluster1');
  // const alarmtmp_nodecustom_name = page.getTestData('node-alarmt2');
  // const alarmtmp_workloadall_name = page.getTestData('workload-alarmt1');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_clusterdeall_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_clusterdeall_name);
  });

  // 创建集群的指标告警
  it('创建告警模板，选择所有指标', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_clusterdeall_name,
        描述: alarmtmp_clusterdeall_name.toUpperCase(),
        资源类型: '集群',
      },
      告警规则1: {
        告警类型: '指标告警',
        指标: 'cluster.cpu.utilization',
        告警等级: 'High',
        阈值: ['>', '90'],
      },
      告警规则2: {
        指标: 'cluster.kube.apiserver.health',
        告警等级: 'Critical',
        阈值: ['<', '90'],
        持续时间: '1分钟',
      },
      告警规则3: {
        指标: 'cluster.kube.apiserver.request.count',
        告警等级: 'Low',
        阈值: ['>', '800000000'],
        持续时间: '5分钟',
      },
      告警规则4: {
        指标: 'cluster.kube.apiserver.request.count.2xx',
        阈值: ['>', '1000'],
      },
      告警规则5: {
        指标: 'cluster.kube.apiserver.request.count.2xx.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则6: {
        指标: 'cluster.kube.apiserver.request.count.4xx',
        阈值: ['>', '1000'],
      },
      告警规则7: {
        指标: 'cluster.kube.apiserver.request.count.4xx.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则8: {
        指标: 'cluster.kube.apiserver.request.count.5xx',
        阈值: ['>', '1000'],
      },
      告警规则9: {
        告警类型: '指标告警',
        指标: 'cluster.kube.apiserver.request.count.5xx.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则10: {
        指标: 'cluster.kube.apiserver.request.count.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则11: {
        指标: 'cluster.kube.apiserver.request.error.rate',
        阈值: ['>', '1000'],
      },
      告警规则12: {
        指标: 'cluster.kube.apiserver.request.error.rate.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则13: {
        指标: 'cluster.kube.apiserver.request.latency',
        阈值: ['>', '1000'],
      },
      告警规则14: {
        指标: 'cluster.kube.apiserver.request.latency.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则15: {
        指标: 'cluster.kube.apiserver.request.latency.per.verb',
        阈值: ['>', '1000'],
      },
      告警规则16: {
        指标: 'cluster.kube.apiserver.up',
        阈值: ['>', '1000'],
      },
      告警规则17: {
        指标: 'cluster.kube.controller.manager.health',
        阈值: ['>', '1000'],
      },
      告警规则18: {
        指标: 'cluster.kube.controller.manager.up',
        阈值: ['>', '1000'],
      },
      告警规则19: {
        指标: 'cluster.kube.dns.health',
        阈值: ['>', '1000'],
      },
      告警规则20: {
        指标: 'cluster.kube.dns.up',
        阈值: ['>', '1000'],
      },
      告警规则21: {
        指标: 'cluster.kube.etcd.active.lease.stream',
        阈值: ['>', '1000'],
      },
      告警规则22: {
        指标: 'cluster.kube.etcd.active.watch.stream',
        阈值: ['>', '1000'],
      },
      告警规则23: {
        指标: 'cluster.kube.etcd.client.traffic.in',
        阈值: ['>', '1000'],
      },
      告警规则24: {
        指标: 'cluster.kube.etcd.client.traffic.in.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则25: {
        指标: 'cluster.kube.etcd.client.traffic.out',
        阈值: ['>', '1000'],
      },
      告警规则26: {
        指标: 'cluster.kube.etcd.client.traffic.out.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则27: {
        指标: 'cluster.kube.etcd.db.fsync.duration.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则28: {
        指标: 'cluster.kube.etcd.db.size',
        阈值: ['>', '1000'],
      },
      告警规则29: {
        指标: 'cluster.kube.etcd.db.size.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则30: {
        指标: 'cluster.kube.etcd.has.leader',
        阈值: ['>', '1000'],
      },
      告警规则31: {
        指标: 'cluster.kube.etcd.health',
        阈值: ['>', '1000'],
      },
      告警规则32: {
        指标: 'cluster.kube.etcd.leader.elections',
        阈值: ['>', '1000'],
      },
      告警规则33: {
        指标: 'cluster.kube.etcd.members',
        阈值: ['>', '1000'],
      },
      告警规则34: {
        指标: 'cluster.kube.etcd.peer.traffic.in',
        阈值: ['>', '1000'],
      },
      告警规则35: {
        指标: 'cluster.kube.etcd.peer.traffic.in.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则36: {
        指标: 'cluster.kube.etcd.peer.traffic.out',
        阈值: ['>', '1000'],
      },
      告警规则37: {
        指标: 'cluster.kube.etcd.peer.traffic.out.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则38: {
        指标: 'cluster.kube.etcd.raft.proposal.applied.rate',
        阈值: ['>', '1000'],
      },
      告警规则39: {
        指标: 'cluster.kube.etcd.raft.proposal.committed.rate',
        阈值: ['>', '1000'],
      },
      告警规则40: {
        指标: 'cluster.kube.etcd.raft.proposal.failed.rate',
        阈值: ['>', '1000'],
      },
      告警规则41: {
        指标: 'cluster.kube.etcd.raft.proposal.pending.rate',
        阈值: ['>', '1000'],
      },
      告警规则42: {
        指标: 'cluster.kube.etcd.rpc.failed.rate',
        阈值: ['>', '1000'],
      },
      告警规则43: {
        指标: 'cluster.kube.etcd.rpc.rate',
        阈值: ['>', '1000'],
      },
      告警规则44: {
        指标: 'cluster.kube.etcd.up',
        阈值: ['>', '1000'],
      },
      告警规则45: {
        指标: 'cluster.kube.etcd.wal.fsync.duration.per.instance',
        阈值: ['>', '1000'],
      },
      告警规则46: {
        指标: 'cluster.kube.kubelet.health',
        阈值: ['>', '1000'],
      },
      告警规则47: {
        指标: 'cluster.kube.kubelet.up',
        阈值: ['>', '1000'],
      },
      告警规则48: {
        指标: 'cluster.kube.proxy.health',
        阈值: ['>', '1000'],
      },
      告警规则49: {
        指标: 'cluster.kube.proxy.up',
        阈值: ['>', '1000'],
      },
      告警规则50: {
        指标: 'cluster.kube.scheduler.health',
        阈值: ['>', '1000'],
      },
      告警规则51: {
        指标: 'cluster.kube.scheduler.up',
        阈值: ['>', '1000'],
      },
      告警规则52: {
        指标: 'cluster.memory.utilization',
        阈值: ['>', '1000'],
      },
      告警规则53: {
        指标: 'cluster.node.health',
        阈值: ['>', '1000'],
      },
      告警规则54: {
        指标: 'cluster.node.network.ipvs.active.connections',
        阈值: ['>', '1000'],
      },
      告警规则55: {
        指标: 'cluster.node.not.ready.count',
        阈值: ['>', '1000'],
      },
      告警规则56: {
        指标: 'cluster.node.ready',
        阈值: ['>', '1000'],
      },
      告警规则57: {
        指标: 'cluster.pod.restarted.count',
        阈值: ['>', '1000'],
      },
      告警规则58: {
        指标: 'cluster.pod.restarted.total',
        阈值: ['>', '1000'],
      },
      告警规则59: {
        指标: 'cluster.pod.status.phase.failed',
        阈值: ['>', '1000'],
      },
      告警规则60: {
        指标: 'cluster.pod.status.phase.not.running',
        阈值: ['>', '1000'],
      },
      告警规则61: {
        指标: 'cluster.pod.status.phase.pending',
        阈值: ['>', '1000'],
      },
      告警规则62: {
        指标: 'cluster.pod.status.phase.running',
        阈值: ['>', '1000'],
      },
      告警规则63: {
        指标: 'cluster.pod.status.phase.succeeded',
        阈值: ['>', '1000'],
      },
      告警规则64: {
        指标: 'cluster.pod.status.phase.unknown',
        阈值: ['>', '1000'],
      },
      告警规则65: {
        指标: 'cluster.resource.request.cpu.utilization',
        阈值: ['>', '1000'],
      },
      告警规则66: {
        指标: 'cluster.resource.request.memory.utilization',
        阈值: ['>', '1000'],
      },
      告警规则67: {
        告警类型: '指标告警',
        指标: 'cluster.alerts.firing',
        阈值: ['>', '10'],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);

    // const expectData = {
    //   基本信息: {
    //     名称: alarmtmp_clusterdefault_name,
    //     描述: alarmtmp_clusterdefault_name.toUpperCase(),
    //     资源类型: '集群',
    //   },
    //   告警规则: {
    //     告警类型: '指标告警',
    //     指标: 'cluster.cpu.utilization',
    //     告警等级: 'Medium',
    //     阈值: ['>', '0'],
    //     持续时间: '1 分钟',
    //   },
    //   操作指令: [['通知', [name_notification]]],
    // };
    // page.detailPageVerify.verify(expectData);
  });
});
