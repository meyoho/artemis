import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警模板 L0 case', () => {
  const page = new AlarmPage();
  const alarmtmp_workload_name1 = page.getTestData('alarmtmpl1-workload1');
  const alarmtmp_workload_name2 = page.getTestData('alarmtmpl1-workload2');
  const alarmtmp_workload_name3 = page.getTestData('alarmtmpl1-workload3');
  const alarmtmp_workload_name4 = page.getTestData('alarmtmpl1-workload4');
  const alarmtmp_workload_name5 = page.getTestData('alarmtmpl1-workload5');
  const alarmtmp_workload_name6 = page.getTestData('alarmtmpl1-workload6');
  const alarmtmp_workload_name7 = page.getTestData('alarmtmpl1-workload7');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name1);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name2);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name3);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name4);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name5);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name6);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name7);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name1);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name2);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name3);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name4);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name5);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name6);
    page.preparePage.deleteAlarmTemplate(alarmtmp_workload_name7);
  });
  it('ACP2UI-57281 : 创建告警模板-资源类型选择计算组件-添加数据类型有聚合值的指标(修改各项值)-添加一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name1,
        资源类型: '工作负载',
      },
      告警规则1: {
        指标: 'container.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name1,
        描述: '-',
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57283 : 创建告警模板-资源类型选择计算组件-添加3个指标告警分别添加标签和注解-添加多个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name2,
        资源类型: '工作负载',
      },
      告警规则1: {
        指标: 'container.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则2: {
        指标: 'pod.network.receive_bytes',
        告警等级: 'High',
        阈值: ['>', '8000'],
        持续时间: '5分钟',
        验证规则: [
          'pod.network.receive_bytes > 8000bytes/second 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则3: {
        指标: 'workload.replicas.available',
        告警等级: 'Low',
        阈值: ['==', '5'],
        持续时间: '3分钟',
        验证规则: [
          'workload.replicas.available == 5 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name2,
        描述: '-',
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
        [
          'pod.network.receive_bytes > 8000bytes/second 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        [
          'workload.replicas.available == 5 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55422 : 创建告警模板-资源类型选择计算组件-添加1条日志告警-输入多条日志内容-输入阈值-添加1个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name3,
        描述: alarmtmp_workload_name3.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        告警类型: '日志告警',
        日志内容: 'error',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '1 分钟内，包含 error 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name3,
        描述: alarmtmp_workload_name3.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 error 信息的日志 > 20 条', '日志告警', 'High'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57285 : 创建告警模板-资源类型选择计算组件-添加2条日志告警(1条输入1个日志内容，另外一条输入多个日志内容)-分别输入阈值-添加多个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name4,
        描述: alarmtmp_workload_name4.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        告警类型: '日志告警',
        日志内容: 'error',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '1 分钟内，包含 error 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
      告警规则2: {
        告警类型: '日志告警',
        日志内容: '错误',
        告警等级: 'Low',
        阈值: ['>', '18'],
        验证规则: ['1 分钟内，包含 错误 信息的日志 > 18 条', '日志告警', 'Low'],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name4,
        描述: alarmtmp_workload_name4.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 error 信息的日志 > 20 条', '日志告警', 'High'],
        ['1 分钟内，包含 错误 信息的日志 > 18 条', '日志告警', 'Low'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55423 : 创建告警模板-资源类型选择计算组件-添加1条事件告警-输入1个事件内容-输入阈值-添加1个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name5,
        描述: alarmtmp_workload_name5.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '10'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 10 条',
          '事件告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name5,
        描述: alarmtmp_workload_name5.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 Failed 信息的事件 > 10 条', '事件告警', 'Medium'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57287 : 创建告警模板-资源类型选择计算组件-添加2条事件告警(1条输入1个事件内容，另外一条输入多个事件内容)-分别输入阈值-添加多个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name6,
        描述: alarmtmp_workload_name6.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '10'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 10 条',
          '事件告警',
          'Medium',
        ],
      },
      告警规则2: {
        告警类型: '事件告警',
        事件原因: 'FailedKillPod',
        告警等级: 'High',
        阈值: ['>=', '8'],
        验证规则: [
          '1 分钟内，包含 FailedKillPod 信息的事件 >= 8 条',
          '事件告警',
          'High',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name6,
        描述: alarmtmp_workload_name6.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 Failed 信息的事件 > 10 条', '事件告警', 'Medium'],
        ['1 分钟内，包含 FailedKillPod 信息的事件 >= 8 条', '事件告警', 'High'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55424 : 创建告警模板-资源类型选择计算组件-添加1条自定义告警(修改各项默认值)-添加多条标签和注解-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_workload_name7,
        描述: alarmtmp_workload_name7.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则1: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: '%',
        告警等级: 'Low',
        阈值: ['>', '1000'],
        持续时间: '2分钟',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name7,
        描述: alarmtmp_workload_name7.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['sum_over_time > 1000% 且持续时间达到 2 分钟', '自定义告警', 'Low'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55522 : 更新告警模板-选择资源类型是计算组件的指标告警更新-列表页点击更新-更新描述-更新原有的告警规则-点击更新更新成功', () => {
    page.listPage.update(alarmtmp_workload_name1);
    const testData = {
      基本信息: {
        描述: 'a',
      },
      更新告警规则1: {
        聚合时间: '5分钟',
        聚合方式: '平均值',
        告警等级: 'High',
        阈值: ['>', '99.998'],
        持续时间: '5分钟',
        验证规则: [
          'container.memory.utilization > 99.998% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
    };
    page.updateAlarmTemplate(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name1);
    const expectData1 = {
      基本信息: {
        名称: alarmtmp_workload_name1,
        描述: 'a',
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'container.memory.utilization > 99.998% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });

  it('ACP2UI-57300 : 更新告警模板-选择资源类型是计算组件的指标告警更新-列表页点击更新-更新描述-删除一条原有的告警规则再增加一条新的告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name2);
    page.detailPage.detailPage_operate('更新');
    const testData = {
      基本信息: {
        描述: alarmtmp_workload_name2.toUpperCase(),
      },
      删除告警规则1: 'click',
      告警规则3: {
        指标: 'container.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
    };
    page.updateAlarmTemplate(testData);
    const expectData1 = {
      基本信息: {
        名称: alarmtmp_workload_name2,
        描述: alarmtmp_workload_name2.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        [
          'pod.network.receive_bytes > 8000bytes/second 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        [
          'workload.replicas.available == 5 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
        [
          'container.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });

  it('ACP2UI-55523 : 更新告警模板-选择资源类型是计算组件的日志告警更新-详情页点击更新-更新告警规则并增加一条新的事件告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name3);
    page.detailPage.detailPage_operate('更新');
    const data = {
      更新告警规则1: {
        告警等级: 'Medium',
        阈值: ['>', '15'],
        验证规则: [
          '1 分钟内，包含 error 信息的日志 > 15 条',
          '日志告警',
          'Medium',
        ],
      },
      告警规则2: {
        告警类型: '日志告警',
        日志内容: '你好呀',
        告警等级: 'High',
        阈值: ['>', '20'],
        验证规则: [
          '1 分钟内，包含 你好呀 信息的日志 > 20 条',
          '日志告警',
          'High',
        ],
      },
    };
    page.updateAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name3,
        描述: alarmtmp_workload_name3.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 error 信息的日志 > 15 条', '日志告警', 'Medium'],
        ['1 分钟内，包含 你好呀 信息的日志 > 20 条', '日志告警', 'High'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57304 : 更新告警模板-选择资源类型是计算组件的事件告警更新-详情页点击更新-删除老的告警规则并增加一条新的事件告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name5);
    page.detailPage.detailPage_operate('更新');
    const data = {
      删除告警规则1: 'click',
      告警规则1: {
        告警类型: '事件告警',
        事件原因: 'Failed',
        阈值: ['>', '80'],
        验证规则: [
          '1 分钟内，包含 Failed 信息的事件 > 80 条',
          '事件告警',
          'Medium',
        ],
      },
    };
    page.updateAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name5,
        描述: alarmtmp_workload_name5.toUpperCase(),
        资源类型: '工作负载',
      },
      告警规则: [
        ['1 分钟内，包含 Failed 信息的事件 > 80 条', '事件告警', 'Medium'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55524 : 更新告警模板-选择资源类型是计算组件的自定义告警-更新告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name7);
    page.detailPage.detailPage_operate('更新');
    const data = {
      基本信息: {
        描述: 'cc',
      },
      更新告警规则1: {
        告警等级: 'Medium',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.updateAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_workload_name7,
        描述: 'cc',
        资源类型: '工作负载',
      },
      告警规则: [
        ['sum_over_time > 1000% 且持续时间达到 2 分钟', '自定义告警', 'Medium'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-4067 : 告警模板搜索是否正常', () => {
    page.listPage.search_AlarmtmpList(alarmtmp_workload_name1, 1);
    const expectData = { 检索数量: 1 };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-4059 : 删除告警模板-任意选择一个告警模板-列表页点击删除-确认后删除成功', () => {
    page.listPage.delete_AlarmtmpList(alarmtmp_workload_name1);
    page.listPage.search_AlarmtmpList(alarmtmp_workload_name1, 0);
    const expectData = { 无数据: true };
    page.listPageVerify.verify(expectData);
  });
  it('ACP2UI-4060 : 删除告警模板-任意选择一个告警模板-详情页点击删除-确认后删除成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_workload_name2);
    page.detailPage.detailPage_operate('删除');
    page.confirmDialog.clickConfirm(); // 点击删除确认框的确定按钮
    // 删除后跳转回列表页
    page.listPage.search_AlarmtmpList(alarmtmp_workload_name2, 0);
    const expectData = { 无数据: true };
    page.listPageVerify.verify(expectData);
  });
});
