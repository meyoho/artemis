import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';
import { browser } from 'protractor';

describe('管理视图告警 L0 case', () => {
  const page = new AlarmPage();
  const alarm_clusterquota_name = page.getTestData('alarml0-cluster1');
  const alarm_nodequota_name = page.getTestData('alarml0-node1');
  const alarm_cluster_name = page.getTestData('alarml0-cluster2');
  const alarm_node_name = page.getTestData('alarml0-node2');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarm(alarm_clusterquota_name);
    page.preparePage.deleteAlarm(alarm_nodequota_name);
    page.preparePage.deleteAlarm(alarm_cluster_name);
    page.preparePage.deleteAlarm(alarm_node_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('告警');
    page.switchCluster_onAdminView(page.clusterName);
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警');
    browser.sleep(500);
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarm(alarm_clusterquota_name);
    page.preparePage.deleteAlarm(alarm_nodequota_name);
    page.preparePage.deleteAlarm(alarm_cluster_name);
    page.preparePage.deleteAlarm(alarm_node_name);
  });

  it('ACP2UI-55445 : 创建告警-资源类型选择集群-选择指标告警(选择一个指标,输入阈值其他默认)-选择一个通知-点击创建创建成功-并验证是否会触发告警', () => {
    const data = {
      基本信息: {
        名称: alarm_clusterquota_name,
        描述: alarm_clusterquota_name.toUpperCase(),
        资源类型: '集群',
      },
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        阈值: ['>', '98'],
        验证规则: [
          'cluster.cpu.utilization > 98% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_clusterquota_name,
        描述: alarm_clusterquota_name.toUpperCase(),
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization > 98% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55453 : 创建告警-资源类型选择主机-全部主机-添加1条指标告警(选择一个指标，输入阈值其他默认)-点击创建创建成功-并验证是否会触发告警', () => {
    const data = {
      基本信息: {
        名称: alarm_nodequota_name,
        描述: alarm_nodequota_name.toUpperCase(),
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.cpu.utilization',
        阈值: ['>', '90'],
        验证规则: [
          'node.cpu.utilization > 90% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_nodequota_name,
        描述: alarm_nodequota_name.toUpperCase(),
        资源类型: '主机',
      },
      告警规则: [
        [
          '',
          'node.cpu.utilization > 90% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55449 : 创建告警-资源类型选择集群-添加2条告警规则分别是指标告警和自定义告警(修改各项值)-选择一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarm_cluster_name,
        资源类型: '集群',
      },
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则2: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'core',
        告警等级: 'Low',
        阈值: ['<=', '1000'],
        持续时间: '5分钟',
        验证规则: [
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_cluster_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
        [
          '',
          'sum_over_time <= 1000core 且持续时间达到 5 分钟',
          '自定义告警',
          'Low',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55457 : 创建告警-资源类型选择主机-全部主机-添加2条告警规则分别是指标告警和自定义告警(输入各项值)-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarm_node_name,
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.memory.utilization',
        告警等级: 'Critical',
        阈值: ['>=', '97.7658'],
        持续时间: '5分钟',
        验证规则: [
          'node.memory.utilization >= 97.7658% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则2: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: 'byte/second',
        阈值: ['!=', '1000'],
        验证规则: [
          'sum_over_time != 1000byte/second 且持续时间达到 1 分钟',
          '自定义告警',
          'Medium',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarm(data);
    const expectData = {
      基本信息: {
        名称: alarm_node_name,
        描述: '-',
        资源类型: '主机',
      },
      告警规则: [
        [
          '',
          'node.memory.utilization >= 97.7658% 且持续时间达到 5 分钟',
          '指标告警',
          'Critical',
        ],
        [
          '',
          'sum_over_time != 1000byte/second 且持续时间达到 1 分钟',
          '自定义告警',
          'Medium',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });
});
