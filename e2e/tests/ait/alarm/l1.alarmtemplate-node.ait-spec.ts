import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警模板-创建主机类型的告警模板 L1 case', () => {
  const page = new AlarmPage();
  const alarmtmp_node_name1 = page.getTestData('alarmtmpl1-node1');
  const alarmtmp_node_name2 = page.getTestData('alarmtmpl1-node2');
  const alarmtmp_node_name3 = page.getTestData('alarmtmpl1-node3');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name1);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name2);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name3);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name1);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name2);
    page.preparePage.deleteAlarmTemplate(alarmtmp_node_name3);
  });

  it('ACP2UI-55420 : 创建告警模板-资源类型选择主机-添加数据类型有聚合值的指标(修改各项值)-添加一个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_node_name1,
        描述: alarmtmp_node_name1.toUpperCase(),
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.resource.request.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_node_name1,
        描述: alarmtmp_node_name1.toUpperCase(),
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55421 : 创建告警模板-资源类型选择主机-添加3个指标告警分别添加标签和注解-添加多个通知-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_node_name2,
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.resource.request.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
      告警规则2: {
        指标: 'node.disk.utilization',
        告警等级: 'High',
        阈值: ['>=', '99'],
        持续时间: '5分钟',
        验证规则: [
          'node.disk.utilization >= 99% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
      告警规则3: {
        指标: 'node.load.15',
        告警等级: 'Low',
        阈值: ['==', '20'],
        持续时间: '3分钟',
        验证规则: [
          'node.load.15 == 20 且持续时间达到 3 分钟',
          '指标告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_node_name2,
        描述: '-',
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
        [
          'node.disk.utilization >= 99% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        ['node.load.15 == 20 且持续时间达到 3 分钟', '指标告警', 'Low'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57289 : 创建告警模板-资源类型选择主机-添加1条自定义告警(修改各项默认值)-添加多条标签和注解-点击创建创建成功', () => {
    const data = {
      基本信息: {
        名称: alarmtmp_node_name3,
        描述: alarmtmp_node_name3.toUpperCase(),
        资源类型: '主机',
      },
      告警规则1: {
        告警类型: '自定义告警',
        自定义指标: 'sum_over_time',
        单位: '%',
        告警等级: 'Low',
        阈值: ['>', '1000'],
        持续时间: '2分钟',
        验证规则: [
          'sum_over_time > 1000% 且持续时间达到 2 分钟',
          '自定义告警',
          'Low',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmTemplate(data);
    const expectData = {
      基本信息: {
        名称: alarmtmp_node_name3,
        描述: alarmtmp_node_name3.toUpperCase(),
        资源类型: '主机',
      },
      告警规则: [
        ['sum_over_time > 1000% 且持续时间达到 2 分钟', '自定义告警', 'Low'],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57302 : 更新告警模板-选择资源类型是主机的指标告警更新-列表页点击更新-更新描述-更新原有的告警规则-点击更新更新成功', () => {
    page.listPage.update(alarmtmp_node_name1);
    const testData = {
      基本信息: {
        描述: 'a',
      },
      更新告警规则1: {
        聚合时间: '5分钟',
        聚合方式: '平均值',
        告警等级: 'High',
        阈值: ['>', '99.998'],
        持续时间: '5分钟',
        验证规则: [
          'node.resource.request.memory.utilization > 99.998% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      },
    };
    page.updateAlarmTemplate(testData); // 更新成功后跳转到列表页，需点击模板名称到详情页校验更新是否成功
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_node_name1);
    const expectData1 = {
      基本信息: {
        名称: alarmtmp_node_name1,
        描述: 'a',
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.resource.request.memory.utilization > 99.998% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });

  it('ACP2UI-57303 : 更新告警模板-选择资源类型是主机的指标告警更新-详情页点击更新-更新描述-删除一个原有的告警规则再增加一条新的告警规则-点击更新更新成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtmp_node_name2);
    page.detailPage.detailPage_operate('更新');
    const testData = {
      基本信息: {
        描述: alarmtmp_node_name2.toUpperCase(),
      },
      删除告警规则1: 'click',
      告警规则3: {
        指标: 'node.resource.request.memory.utilization',
        数据类型: '聚合值',
        聚合时间: '30分钟',
        聚合方式: '最大值',
        告警等级: 'Critical',
        阈值: ['>', '99.999'],
        持续时间: '10分钟',
        验证规则: [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      },
    };
    page.updateAlarmTemplate(testData);
    const expectData1 = {
      基本信息: {
        名称: alarmtmp_node_name2,
        描述: alarmtmp_node_name2.toUpperCase(),
        资源类型: '主机',
      },
      告警规则: [
        [
          'node.disk.utilization >= 99% 且持续时间达到 5 分钟',
          '指标告警',
          'High',
        ],
        ['node.load.15 == 20 且持续时间达到 3 分钟', '指标告警', 'Low'],
        [
          'node.resource.request.memory.utilization > 99.999% 且持续时间达到 10 分钟',
          '指标告警',
          'Critical',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailPageVerify.verify(expectData1);
  });
});
