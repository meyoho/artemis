import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';

describe('业务视图-应用下创建告警 L0 case', () => {
  const page = new AlarmPage();
  const app_page = new Application();
  const deploy_list_page = new DeploymentListPage();
  const appName = 'auto-ait-appdeploy1';
  const deployName = `${appName}-qaimages`;
  const alarm_app_name = page.getTestData('app-alarml0-1');
  const name_notification = 'uiauto-notification-email';

  beforeAll(() => {
    page.preparePage.deleteApp(appName, page.namespace1Name);
    page.preparePage.createApp(appName, page.namespace1Name);
    page.preparePage.deleteAlarm(alarm_app_name, page.namespace1Name);
    page.login();
    page.enterUserView(page.namespace1Name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('应用');
  });
  afterAll(() => {
    page.preparePage.deleteAlarm(alarm_app_name, page.namespace1Name);
  });

  it('ACP2UI-55426 : 部署模式为部署-创建告警-名称-添加告警规则-选择指标-输入阈值-点击添加-点击创建', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('告警').click();
    const data = {
      基本信息: {
        名称: alarm_app_name,
        资源类型: '工作负载',
        资源名称: deployName,
      },
      告警规则1: {
        指标: 'pod.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'pod.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
      操作指令: [['通知', [name_notification]]],
    };
    page.createAlarmApp(data);
    const expectData = {
      基本信息: {
        名称: alarm_app_name,
        描述: '-',
        资源类型: '部署',
        资源名称: deployName,
      },
      告警规则: [
        [
          '',
          'pod.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
      操作指令: [['通知', [name_notification]]],
    };
    page.detailAppPageVerify.verify(expectData);
  });
});
