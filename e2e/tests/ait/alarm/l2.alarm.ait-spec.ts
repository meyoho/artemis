import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警 L2 case', () => {
  const page = new AlarmPage();
  const alarm_name =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrstuvwxyz1-alarm';

  beforeAll(() => {
    page.preparePage.deleteAlarm(alarm_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
    page.switchCluster_onAdminView(page.clusterName);
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarm(alarm_name);
  });

  // 创建集群的指标告警
  it('ACP2UI-54032 : 创建告警-名称特殊字符校验-最长字符校验-必填项校验', () => {
    page.getButtonByText('创建告警').click();
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', alarm_name + 'duoyuzifu', ''); // 名称63位

    page.getButtonByText('创建').click(); // 点击创建按钮
    // 提示“告警至少需要一条告警规则”
    expect(
      page.createPage
        .errorAlert('告警至少需要一条告警规则')
        .checkTextIsPresent(),
    ).toBeTruthy();
    page.createPage.errorAlert_close.click();

    const data = {
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
    };
    page.createAlarm_add(data);
    const expectData = {
      基本信息: {
        名称: alarm_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54014|ACP2UI-54010 : 创建告警-名称同名校验（包含创建输入信息后点击取消）', () => {
    const data = {
      基本信息: {
        名称: alarm_name,
        描述: alarm_name.toUpperCase(),
        资源类型: '主机',
      },
      告警规则1: {
        指标: 'node.cpu.utilization',
        阈值: ['>', '98'],
        验证规则: [
          'node.cpu.utilization > 98% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
    };
    page.createAlarm(data);
    //由于告警模板已存在
    // expect(
    //   page.createPage.errorAlert('资源已存在').checkTextIsPresent(),
    // ).toBeTruthy();
    // page.createPage.errorAlert_close.click();
    page.getButtonByText('取消').click(); // 点击取消按钮后返回列表页
  });

  it('ACP2UI-55518 : 更新告警-任意选择一个告警到更新页-更新描述和更新告警规则后-更新页点击取消按钮-取消成功', () => {
    page.listPage.enterDetail(alarm_name);
    page.detailPage.detailPage_operate('更新');
    page.getButtonByText('取消').click(); // 点击取消按钮后返回详情页
    const expectData = {
      基本信息: {
        名称: alarm_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-55519 : 删除告警-任意选择一个告警点击删除-在删除确认框中点击取消按钮-取消成功', () => {
    page.listPage.enterDetail(alarm_name);
    page.detailPage.detailPage_operate('删除');
    page.getButtonByText('取消').click();
    const expectData = {
      基本信息: {
        名称: alarm_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          '',
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
