import { AlarmPage } from '@e2e/page_objects/ait/alarm/alarm.page';

describe('管理视图告警模板 L2 case', () => {
  const page = new AlarmPage();
  const alarmtemplate_name =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrs-alarmtemplate';

  beforeAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtemplate_name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText_nowait('告警');
  });
  beforeEach(() => {
    page.clickLeftNavByText('告警模板');
  });
  afterEach(() => {});
  afterAll(() => {
    page.preparePage.deleteAlarmTemplate(alarmtemplate_name);
  });

  it('ACP2UI-54349 : 创建告警模板-名称特殊字符校验-最长字符校验-必填项校验', () => {
    page.getButtonByText('创建告警模板').click();
    page.createTmpPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createTmpPage.inputByText('名称', '_teshuzifu');
    page.createTmpPage.inputByText('名称', '中文aaaaaaaa');
    page.createTmpPage.inputByText('名称', 'aaaa_aaaaa');
    page.createTmpPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createTmpPage.inputByText('名称', 'aaaateDAXIE');
    page.createTmpPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createTmpPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createTmpPage.inputByText(
      '名称',
      alarmtemplate_name + 'duoyuzifu',
      '',
    ); // 名称63位

    page.getButtonByText('创建').click(); // 点击创建按钮
    // 提示“告警模板至少需要一条告警规则”
    expect(
      page.createTmpPage
        .errorAlert('告警模板至少需要一条告警规则')
        .checkTextIsPresent(),
    ).toBeTruthy();
    page.createTmpPage.errorAlert_close.click();

    const data = {
      告警规则1: {
        指标: 'cluster.cpu.utilization',
        告警等级: 'High',
        阈值: ['>=', '98'],
        持续时间: '2分钟',
        验证规则: [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      },
    };
    page.createAlarmTemplate_add(data);
    const expectData = {
      基本信息: {
        名称: alarmtemplate_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57309|ACP2UI-57298 : 创建告警模板-名称同名校验（包含创建输入信息后点击取消）', () => {
    const data = {
      基本信息: {
        名称: alarmtemplate_name,
        资源类型: '主机',
      },
      告警规则1: {
        阈值: ['>', '98'],
        验证规则: [
          'node.cpu.utilization > 98% 且持续时间达到 1 分钟',
          '指标告警',
          'Medium',
        ],
      },
    };
    page.createAlarmTemplate(data);
    //由于告警模板已存在
    // expect(
    //   page.createTmpPage.errorAlert('资源已存在').checkTextIsPresent(),
    // ).toBeTruthy();
    // page.createTmpPage.errorAlert_close.click();
    page.getButtonByText('取消').click(); // 点击取消按钮后返回列表页
  });

  it('ACP2UI-4058 : 更新告警模板-更新描述和更新告警规则后-点击更新页的取消按钮-取消成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtemplate_name);
    page.detailPage.detailPage_operate('更新');
    page.getButtonByText('取消').click(); // 点击取消按钮后返回详情页
    const expectData = {
      基本信息: {
        名称: alarmtemplate_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-55525 : 删除告警模板-任意选择一个告警模板点击删除-删除确认对话框点击取消-取消成功', () => {
    page.listPage.enterDetail_AlarmtmpList(alarmtemplate_name);
    page.detailPage.detailPage_operate('删除');
    page.getButtonByText('取消').click();
    const expectData = {
      基本信息: {
        名称: alarmtemplate_name,
        描述: '-',
        资源类型: '集群',
      },
      告警规则: [
        [
          'cluster.cpu.utilization >= 98% 且持续时间达到 2 分钟',
          '指标告警',
          'High',
        ],
      ],
    };
    page.detailPageVerify.verify(expectData);
  });
});
