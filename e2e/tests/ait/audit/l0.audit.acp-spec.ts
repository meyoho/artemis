// import { ServerConf } from '@e2e/config/serverConf';
// import { AuditPage } from '@e2e/page_objects/ait/audit/audit.page';
// import { Application } from '@e2e/page_objects/acp/appcore/application.page';
// import { element, by } from 'protractor';

// describe('平台管理审计 L0 case', () => {
//   const page = new AuditPage();
//   const app_page = new Application();
//   const app_name = page.getTestData('l0-auditapp');

//   beforeAll(() => {
//     page.preparePage.deleteApp(app_name, page.namespace1Name);
//     page.login();
//     app_page.enterUserView(app_page.namespace1Name);

//     //创建一个应用

//     const data = {
//       选择镜像: {
//         方式: '输入',
//         镜像地址: ServerConf.TESTIMAGE,
//       },
//       应用: {
//         名称: app_name,
//         显示名称: app_name,
//       },
//       计算组件: {
//         部署模式: 'Deployment',
//         实例数量: '1',
//         容器组: {
//           名称: app_name,
//           资源限制: [{ CPU: '10', 单位: 'm' }, { 内存: '10', 单位: 'Mi' }],
//         },
//       },
//     };
//     app_page.createAppPage.create(data, page.namespace1Name);
//   });
//   afterAll(() => {
//     page.preparePage.deleteApp(app_name, page.namespace1Name);
//   });

//   beforeEach(() => {});
//   it('ACP2UI-3957 : L0:在搜索框中输入操作对象名称，按回车搜索，查看搜索结果是否正常', () => {
//     // 进入平台管理
//     page.switchSetting('平台管理');
//     page.waitElementPresent(
//       element(
//         by.xpath(
//           '//button/descendant-or-self::*[normalize-space(text()) ="同步用户"]',
//         ),
//       ),
//       '平台管理页面没有打开',
//     );
//     const searchData = {
//       时间范围: '过去30分钟',
//       操作类型: '更新',
//       对象类型: '应用',
//       操作对象: app_name,
//     };
//     // 检索审计
//     page.listPage.search(searchData).then(() => {
//       const expectData = {
//         请求URI: `/kubernetes/${page.clusterName}/apis/app.k8s.io/v1beta1/namespaces/${page.namespace1Name}/applications/${app_name}/status`,
//       };

//       // 验证第一条审计包含 请求URI
//       page.listPage.expandByIndex(0);
//       page.listPageVerify.verify(expectData);
//     });
//   });
// });
