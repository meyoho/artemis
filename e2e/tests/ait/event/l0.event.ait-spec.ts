import { EventPage } from '@e2e/page_objects/ait/event/event.page';
import { browser } from 'protractor';

describe('管理视图事件 L0 case', () => {
  const page = new EventPage();
  const appName = page.getTestData('eventapp');
  const queryString = `kind: Deployment`;

  beforeAll(() => {
    page.preparePage.deleteApp(appName, page.namespace1Name);
    page.preparePage.createApp(appName, page.namespace1Name);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('事件');
    page.switchCluster_onAdminView(page.clusterName);
  });
  afterAll(() => {
    page.preparePage.deleteApp(appName, page.namespace1Name);
  });
  beforeEach(() => {});
  it('ACP2UI-4154 : L0:查看k8s事件收集是否成功', () => {
    page.listPage.search(queryString);
    page.listPage.timeRange.select('过去10分钟');
    const exceptData = {
      事件: [page.namespace1Name, 'Deployment', appName],
      时间范围: [
        '自定义时间',
        '过去10分钟',
        '过去30分钟',
        '过去1小时',
        '过去3小时',
        '过去6小时',
        '过去12小时',
        '过去1天',
        '过去2天',
        '过去3天',
        '过去5天',
        '过去7天',
      ],
    };

    page.listPageVerify.verify(exceptData);
  });
  it('ACP2UI-4156 : 查看分页跳转是否成功', () => {
    page.listPage.clearSearch();
    page.listPage.timeRange.select('过去1天');
    page.listPage.listPage_getEventTotal.checkTextIsPresent();
    page.listPage.listPage_getEventTotal.getText().then(function(text) {
      const reg = /[^0-9]/gi;
      const a = Number(text.replace(reg, ''));
      if (a > 50) {
        page.alaudaElement.scrollToBoWindowBottom();
        page.listPage.listPage_nextPage_button.click(); //点击下一页
        browser.sleep(1000);
        // page.listPage.listPage_fistPage_button.click(); //点击第一页
        page.listPage.listPage_pageNum.select('50');
        page.listPage.listPage_nextPage_button.click(); //点击下一页
      }
    });
  });
  it('ACP2UI-58936 : 事件列表页-查看事件详情', () => {
    page.listPage.timeRange.select('过去30分钟');
    browser.sleep(1000);
    page.listPage.listPage_eventDetaillink.click(); //点击事件详情
    page.listPage.listPage_eventDialogClose.click();
  });
});
