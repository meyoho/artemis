import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知模板L2 自动化', () => {
  const page = new NotificationPage();
  const noticetmpName =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrstuvw-noticetmp';
  const noticitmpDisplayName = page.getTestData('通知模板l2-1');

  beforeAll(() => {
    page.preparePage.deleteNotifacationTmp(noticetmpName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知模板');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationTmp(noticetmpName);
  });

  it('ACP2UI-54939 : 管理视图，创建通知模板，校验名称规则和必填。', () => {
    page.getButtonByText('创建通知模板').click();
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', noticetmpName + 'duoyuzifu', ''); // 名称63位

    page.getButtonByText('创建').click(); // 点击创建按钮
    // 显示名称校验
    page.createPage.inputByText('显示名称', '', '显示名称是必填项');
    page.createPage.inputByText('显示名称', noticitmpDisplayName, '');
    page.getButtonByText('创建').click(); // 点击创建按钮
    const expectData = {
      基本信息: {
        名称: noticetmpName,
        显示名称: noticitmpDisplayName,
        描述: '-',
        类型: '邮箱',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  xit('ACP2UI-54940|ACP2UI-57039 : 管理视图，创建通知模板，校验名称同名是否正常。（包含创建页取消按钮）', () => {
    const testData = {
      名称: noticetmpName,
      显示名称: noticetmpName.toUpperCase(),
    };
    page.createNotificationtmp(testData);
    //由于通知服务器已存在
    expect(page.createPage.errorAlert().checkTextIsPresent()).toBeTruthy();
    page.createPage.errorAlert_close.click();
    page.getButtonByText('取消').click(); // 点击取消按钮后返回列表页
    page.listPage.enterDetail(noticetmpName);
  });

  it('ACP2UI-54935 : 管理视图，更新通知模板，输入更新信息，点击取消按钮，验证更新取消通知模板是否正常。', () => {
    page.listPage.enterDetail(noticetmpName);
    page.detailPage_operate('更新');
    page.getButtonByText('取消').click(); // 点击取消按钮后返回详情页
    const expectData = {
      基本信息: {
        名称: noticetmpName,
        显示名称: noticitmpDisplayName,
        描述: '-',
        类型: '邮箱',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54937 : 管理视图，删除通知模板，删除对话框点击取消，验证删除通知模板取消是否正常。', () => {
    page.listPage.delete_operate(noticetmpName);
    page.getButtonByText('取消').click();
    page.listPage.enterDetail(noticetmpName);
  });
});
