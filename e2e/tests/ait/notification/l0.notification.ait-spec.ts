import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';

describe('管理视图 通知L0 自动化', () => {
  const page = new NotificationPage();
  const emailName = page.getTestData('email1');
  const emailReceiver = 'UIAUTO邮箱(test@alauda.io)';
  const emailTmp = 'UIAUTO-Email的模板(uiauto-emailtemplate)';
  const emailSender = ServerConf.EMAILSENDER_NAME2;

  const notificationName = page.getTestData('multil1');
  const smsReceiver = 'UIAUTO短信(13436500533)';
  const smsTmp = 'UIAUTO-短信的模板(uiauto-smstemplate)';
  const smsSender = 'default-sms-sender';

  const webhookReceiver = 'UIAUTOWebhook(http://webhook123.com)';
  const otherTmp = 'UIAUTO-Webhook/企业微信/钉钉的模板(uiauto-othertemplate)';
  const ispublic = ServerConf.IS_PUBLIC;

  beforeAll(() => {
    page.preparePage.deleteNotifacation(emailName);
    page.preparePage.deleteNotifacation(notificationName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacation(emailName);
    page.preparePage.deleteNotifacation(notificationName);
  });
  it('ACP2UI-54963 : 管理视图，创建通知，类型选择邮件，创建成功并跳转到详情页', () => {
    if (ispublic === 'true') {
      const testData = {
        名称: emailName,
        显示名称: emailName.toUpperCase(),
        通知方式1: {
          类型: '邮箱',
          通知对象: emailReceiver,
          通知模板: emailTmp,
          发送人: emailSender,
        },
      };
      page.createNotification(testData);

      const expectData = {
        基本信息: {
          名称: emailName,
          显示名称: emailName.toUpperCase(),
          描述: '-',
        },
        通知方式1: {
          类型: '邮箱',
          通知对象: emailReceiver,
          // 通知模板: emailTmp,
          发送人: emailSender,
        },
      };
      page.detailPageVerify.verify(expectData);
    } else {
      console.log('私有环境,不执行创建通知类型邮箱的case');
    }
  });

  it('ACP2UI-54972 : 管理视图，创建通知，选择多个不同类型通知方式，创建成功并跳转到详情页 ', () => {
    const testData = {
      名称: notificationName,
      显示名称: notificationName.toUpperCase(),
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        通知模板: smsTmp,
        发送人: smsSender,
      },
      通知方式2: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        通知模板: otherTmp,
      },
    };
    page.createNotification(testData);

    const expectData = {
      基本信息: {
        名称: notificationName,
        显示名称: notificationName.toUpperCase(),
        描述: '-',
      },
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        // 通知模板: smsTmp,
        发送人: smsSender,
      },
      通知方式2: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54977 : 管理视图，更新类型是邮件的通知，列表页点击更新，更新信息，点击更新按钮，更新成功跳转到详情页', () => {
    page.listPage.update(notificationName);
    const testData = {
      显示名称: '更新显示名称',
      描述: '添加描述',
    };
    page.updateNotification(testData);

    const expectData = {
      基本信息: {
        名称: notificationName,
        显示名称: '更新显示名称',
        描述: '添加描述',
      },
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        // 通知模板: smsTmp,
        发送人: smsSender,
      },
      通知方式2: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-57049 : 管理视图，删除通知，详情页点击删除，对话框输入确认名称后点击删除，删除成功', () => {
    page.listPage.enterDetail(notificationName);
    page.detailPage_operate('删除');
    page.confirmDialog_name.clickDelete(notificationName); // 输入绑定名称后点击删除按钮
  });
});
