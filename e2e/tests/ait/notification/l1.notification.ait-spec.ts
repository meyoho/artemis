import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知L1 自动化', () => {
  const page = new NotificationPage();

  const smsName = page.getTestData('sms1');
  const smsReceiver = 'UIAUTO短信(13436500533)';
  const smsTmp = 'UIAUTO-短信的模板(uiauto-smstemplate)';
  const smsSender = 'default-sms-sender';

  const dingtalkName = page.getTestData('dingtalk1');
  const dingtalkReceiver = 'UIAUTO-钉钉(http://dingtalk.com)';
  const otherTmp = 'UIAUTO-Webhook/企业微信/钉钉的模板(uiauto-othertemplate)';

  const wechatName = page.getTestData('wechat1');
  const wechatReceiver = 'UIAUTO-企业微信(http://wechat.com)';

  const webhookName = page.getTestData('webhook1');
  const webhookReceiver = 'UIAUTOWebhook(http://webhook123.com)';

  beforeAll(() => {
    page.preparePage.deleteNotifacation(smsName);
    page.preparePage.deleteNotifacation(dingtalkName);
    page.preparePage.deleteNotifacation(wechatName);
    page.preparePage.deleteNotifacation(webhookName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacation(smsName);
    page.preparePage.deleteNotifacation(dingtalkName);
    page.preparePage.deleteNotifacation(wechatName);
    page.preparePage.deleteNotifacation(webhookName);
  });

  it('ACP2UI-54967 : 管理视图，创建通知，类型选择短信，创建成功并跳转到详情页', () => {
    const testData = {
      名称: smsName,
      显示名称: smsName.toUpperCase(),
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        通知模板: smsTmp,
        发送人: smsSender,
      },
    };
    page.createNotification(testData);

    const expectData = {
      基本信息: {
        显示名称: smsName.toUpperCase(),
        描述: '-',
      },
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        // 通知模板: smsTmp,
        发送人: smsSender,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54971 : 管理视图，创建通知，类型选择钉钉，创建成功并跳转到详情页', () => {
    const testData = {
      名称: dingtalkName,
      显示名称: dingtalkName.toUpperCase(),
      通知方式1: {
        类型: '钉钉',
        通知对象: dingtalkReceiver,
        通知模板: otherTmp,
      },
    };
    page.createNotification(testData);

    const expectData = {
      基本信息: {
        名称: dingtalkName,
        显示名称: dingtalkName.toUpperCase(),
        描述: '-',
      },
      通知方式1: {
        类型: '钉钉',
        通知对象: dingtalkReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55007 : 管理视图，创建通知，类型选择企业微信，创建成功并跳转到详情页', () => {
    const testData = {
      名称: wechatName,
      显示名称: wechatName.toUpperCase(),
      通知方式1: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        通知模板: otherTmp,
      },
    };
    page.createNotification(testData);

    const expectData = {
      基本信息: {
        名称: wechatName,
        显示名称: wechatName.toUpperCase(),
        描述: '-',
      },
      通知方式1: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55008 : 管理视图，创建通知，类型选择webhook，创建成功并跳转到详情页', () => {
    const testData = {
      名称: webhookName,
      显示名称: webhookName.toUpperCase(),
      通知方式1: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        通知模板: otherTmp,
      },
    };
    page.createNotification(testData);

    const expectData = {
      基本信息: {
        名称: webhookName,
        显示名称: webhookName.toUpperCase(),
        描述: '-',
      },
      通知方式1: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54978 : 管理视图，更新类型是短信的通知，详情页点击更新，更新信息，并多添加一个通知类型，点击更新按钮，更新成功跳转到详情页', () => {
    page.listPage.enterDetail(smsName);
    page.detailPage_operate('更新');
    const testData = {
      描述: '添加描述',
      通知方式2: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        通知模板: otherTmp,
      },
    };
    page.updateNotification(testData);

    const expectData = {
      基本信息: {
        名称: smsName,
        显示名称: smsName.toUpperCase(),
        描述: '添加描述',
      },
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        // 通知模板: smsTmp,
        发送人: smsSender,
      },
      通知方式2: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54979 : 管理视图，更新类型是钉钉的通知，更新信息，并再添加两个通知类型，点击更新按钮，更新成功并跳转到详情页', () => {
    page.listPage.enterDetail(dingtalkName);
    page.detailPage_operate('更新');
    const testData = {
      描述: '添加描述，增加两个通知方式～～～---，原通知方式是钉钉。',
      通知方式2: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        通知模板: otherTmp,
      },
      通知方式3: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        通知模板: otherTmp,
      },
    };
    page.updateNotification(testData);

    const expectData = {
      基本信息: {
        名称: dingtalkName,
        显示名称: dingtalkName.toUpperCase(),
        描述: '添加描述，增加两个通知方式～～～---，原通知方式是钉钉。',
      },
      通知方式1: {
        类型: '钉钉',
        通知对象: dingtalkReceiver,
        // 通知模板: otherTmp,
      },
      通知方式2: {
        类型: '企业微信',
        通知对象: wechatReceiver,
        // 通知模板: otherTmp,
      },
      通知方式3: {
        类型: 'webhook',
        通知对象: webhookReceiver,
        // 通知模板: otherTmp,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55009 : 管理视图，更新类型是企业微信的通知，更新信息，删除一个通知类型，点击更新按钮，更新成功并跳转到详情页', () => {
    page.listPage.enterDetail(smsName);
    page.detailPage_operate('更新');
    const testData = {
      描述: '删除通知方式添加描述',
      删除通知方式2: 'click', // 删除通知方式
    };
    page.updateNotification(testData);

    const expectData = {
      基本信息: {
        名称: smsName,
        显示名称: smsName.toUpperCase(),
        描述: '删除通知方式添加描述',
      },
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        // 通知模板: smsTmp,
        发送人: smsSender,
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  //   it('列表页，搜索通知', () => {
  //     page.listPage.search(smsName);
  //   });

  it('ACP2UI-54983 : 管理视图，删除通知，列表页点击删除，对话框输入确认名称后点击删除，删除成功', () => {
    page.listPage.delete_operate(smsName);
    page.confirmDialog_name.clickDelete(smsName); // 输入绑定名称后点击删除按钮
  });
});
