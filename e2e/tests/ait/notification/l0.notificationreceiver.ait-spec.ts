import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知对象L0 自动化', () => {
  const page = new NotificationPage();

  const receiverEmail_Name = 'uiauto012533@alauda.io';
  const receiverEmail_Displayname = page.getTestData('Receiver的邮件');
  const receiverSms_Name = '13111012533';
  const receiverSms_Displayname = page.getTestData('Receiver的短信');
  const receiverWebhook_Name = 'http://webhook012533.com';
  const receiverWebhook_Displayname = page.getTestData('Receiver的Webhook');
  const receiverDing_Name = 'http://dingding012533.com';
  const receiverDing_Displayname = page.getTestData('Receiver的钉钉');
  const receiverWechat_Name = 'http://wechat012533.com';
  const receiverWechat_Displayname = page.getTestData('Receiver的微信');

  const receiverEmail_Name_new = 'uiauto0633@alauda.io';
  const receiverSms_Name_new = '13111110633';
  const receiverWebhook_Name_new = 'http://webhook012633.com';
  const receiverDing_Name_new = 'http://dingding012633.com';
  const receiverWechat_Name_new = 'http://wechat012633.com';

  beforeAll(() => {
    page.preparePage.deleteNotifacationReceiver('012533');
    page.preparePage.deleteNotifacationReceiver('012633');
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知对象');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationReceiver('012533');
    page.preparePage.deleteNotifacationReceiver('012633');
  });

  it('ACP2UI-54943 : 添加类型是邮件的通知对象，验证是否正常', () => {
    const testData = {
      添加通知对象: [['邮箱', receiverEmail_Displayname, receiverEmail_Name]],
    };
    page.createNotificationReceiver(testData);
  });
  it('ACP2UI-54944 : 添加类型是短信的通知对象，验证是否正常', () => {
    const testData = {
      添加通知对象: [['短信', receiverSms_Displayname, receiverSms_Name]],
    };
    page.createNotificationReceiver(testData);
  });
  it('ACP2UI-54945 : 添加类型是企业微信的通知对象，验证是否正常', () => {
    const testData = {
      添加通知对象: [
        ['企业微信', receiverWechat_Displayname, receiverWechat_Name],
      ],
    };
    page.createNotificationReceiver(testData);
  });
  it('ACP2UI-54946 : 添加类型是钉钉的通知对象，验证是否正常', () => {
    const testData = {
      添加通知对象: [['钉钉', receiverDing_Displayname, receiverDing_Name]],
    };
    page.createNotificationReceiver(testData);
  });
  it('ACP2UI-54947 : 添加类型是webhook的通知对象，验证是否正常', () => {
    const testData = {
      添加通知对象: [
        ['webhook', receiverWebhook_Displayname, receiverWebhook_Name],
      ],
    };
    page.createNotificationReceiver(testData);
  });

  it('ACP2UI-54948 : 添加多个类型的通知对象，验证添加是否正常', () => {
    const testData = {
      添加通知对象: [
        ['邮箱', receiverEmail_Displayname, receiverEmail_Name],
        ['webhook', receiverWebhook_Displayname, receiverWebhook_Name],
        ['企业微信', receiverWechat_Displayname, receiverWechat_Name],
      ],
    };
    page.createNotificationReceiver(testData);
  });

  it('ACP2UI-54951 : 点击添加通知对象按钮，添加11条通知对象，点击添加按钮，验证是否正常。', () => {
    const testData = {
      添加通知对象: [
        ['短信', receiverSms_Displayname, receiverSms_Name],
        ['邮箱', receiverEmail_Displayname, receiverEmail_Name],
        ['webhook', receiverWebhook_Displayname, receiverWebhook_Name],
        ['企业微信', receiverWechat_Displayname, receiverWechat_Name],
        ['钉钉', receiverDing_Displayname, receiverDing_Name],
        ['短信', receiverSms_Displayname, receiverSms_Name],
        ['短信', receiverSms_Displayname, receiverSms_Name],
        ['邮箱', receiverEmail_Displayname, receiverEmail_Name],
        ['webhook', receiverWebhook_Displayname, receiverWebhook_Name],
        ['企业微信', receiverWechat_Displayname, receiverWechat_Name],
        ['钉钉', receiverDing_Displayname, receiverDing_Name],
      ],
    };
    page.createNotificationReceiver(testData);
  });

  // 更新
  it('ACP2UI-54952 : 选择一个类型是邮件的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.update(receiverEmail_Name);
    // 更新页修改信息
    const testData = {
      添加通知对象: [
        ['邮箱', receiverEmail_Displayname, receiverEmail_Name_new],
      ],
    };
    page.updateNotificationReceiver(testData);
  });
  it('ACP2UI-54953 : 选择一个类型是短信的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.update(receiverSms_Name);
    // 更新页修改信息
    const testData = {
      添加通知对象: [['短信', receiverSms_Displayname, receiverSms_Name_new]],
    };
    page.updateNotificationReceiver(testData);
  });
  it('ACP2UI-54954 : 选择一个类型是企业微信的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.update(receiverWechat_Name);
    const testData = {
      添加通知对象: [
        ['企业微信', receiverWechat_Displayname, receiverWechat_Name_new],
      ],
    };
    page.updateNotificationReceiver(testData);
  });
  it('ACP2UI-54955 : 选择一个类型是钉钉的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.update(receiverDing_Name);
    const testData = {
      添加通知对象: [['钉钉', receiverDing_Displayname, receiverDing_Name_new]],
    };
    page.updateNotificationReceiver(testData);
  });
  it('ACP2UI-54956 : 选择一个类型是webhook的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.update(receiverWebhook_Name);
    const testData = {
      添加通知对象: [
        ['webhook', receiverWebhook_Displayname, receiverWebhook_Name_new],
      ],
    };
    page.updateNotificationReceiver(testData);
  });

  // 删除
  it('ACP2UI-54955 : 选择一个类型是钉钉的通知对象，点击列表页更新按钮。验证更新是否正常', () => {
    page.listPage.searchCount(receiverDing_Name_new, 1);
    page.listPage.delete(receiverDing_Name_new);
    page.listPage.searchCount(receiverDing_Name_new, 0);
  });
});
