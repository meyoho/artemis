import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知L2 自动化', () => {
  const page = new NotificationPage();

  const notificationName =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrstuvwxyz-notice';
  const smsReceiver = 'UIAUTO短信(13436500533)';
  const smsTmp = 'UIAUTO-短信的模板(uiauto-smstemplate)';
  const smsSender = 'default-sms-sender';

  beforeAll(() => {
    page.preparePage.deleteNotifacation(notificationName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacation(notificationName);
  });

  it('ACP2UI-54988 : 管理视图，创建通知，校验名称规则和必填（包含通知方式为必填ACP2UI-54962）', () => {
    page.getButtonByText('创建通知').click();
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', notificationName + 'duoyuzifu', ''); // 名称63位

    page.getButtonByText('创建').click(); // 点击创建按钮
    // 显示名称校验
    page.createPage.inputByText('显示名称', '', '显示名称是必填项');
    page.createPage.inputByText('显示名称', notificationName.toUpperCase(), '');
    const testData = {
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        通知模板: smsTmp,
        发送人: smsSender,
      },
    };
    page.createNotification_add(testData);
    const expectData = {
      基本信息: {
        名称: notificationName,
        显示名称: notificationName.toUpperCase(),
        描述: '-',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54987|ACP2UI-57048 : 管理视图，创建通知，校验同名是否正常（包含创建输入信息后点击取消）', () => {
    const testData = {
      名称: notificationName,
      显示名称: notificationName.toUpperCase(),
      通知方式1: {
        类型: '短信',
        通知对象: smsReceiver,
        通知模板: smsTmp,
        发送人: smsSender,
      },
    };
    page.createNotification(testData);
    //由于通知服务器已存在
    expect(page.createPage.errorAlert().checkTextIsPresent()).toBeTruthy();
    page.createPage.errorAlert_close.click();
    page.getButtonByText('取消').click(); // 点击取消按钮后返回列表页
    page.listPage.enterDetail(notificationName);
  });

  it('ACP2UI-54982 : 管理视图，更新通知页，输入更新信息，点击取消按钮，取消更新通知成功', () => {
    page.listPage.enterDetail(notificationName);
    page.detailPage_operate('更新');
    page.getButtonByText('取消').click(); // 点击取消按钮后返回详情页
    const expectData = {
      基本信息: {
        名称: notificationName,
        显示名称: notificationName.toUpperCase(),
        描述: '-',
      },
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54984 : 管理视图，删除通知，对话框输入确认名称后点击取消，取消成功', () => {
    page.listPage.delete_operate(notificationName);
    page.getButtonByText('取消').click();
    page.listPage.enterDetail(notificationName);
  });
});
