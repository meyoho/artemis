import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知模板L0 自动化', () => {
  const page = new NotificationPage();
  const noticeEmailName = page.getTestData('noticetmpemail1');
  const noticeEmailDisplayName = page.getTestData('UI自动化Email模板');
  const noticeEmailDescription = page.getTestData(
    '在UI自动化中添加的一个Email模板！！！',
  );

  beforeAll(() => {
    page.preparePage.deleteNotifacationTmp(noticeEmailName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知模板');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationTmp(noticeEmailName);
  });
  it('ACP2UI-54930 : 管理视图，创建通知模板，模板类型选择邮件，输入信息，点击创建按钮，验证创建通知模板是否正常。', () => {
    const testData = {
      名称: noticeEmailName,
      显示名称: noticeEmailName.toUpperCase(),
    };
    page.createNotificationtmp(testData);

    const expectData = {
      基本信息: {
        名称: noticeEmailName,
        显示名称: noticeEmailName.toUpperCase(),
        描述: '-',
        类型: '邮箱',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54934 : 管理视图，更新通知模板，列表页点击更新，更新显示名称和描述，点击更新按钮，更新成功后跳转到详情页且数据正确。', () => {
    page.listPage.updateTmp(noticeEmailName);
    // 更新页修改信息
    const testData1 = {
      显示名称: noticeEmailDisplayName,
      模板描述: noticeEmailDescription,
    };
    page.updateNotificationtmp(testData1);

    const expectData = {
      基本信息: {
        名称: noticeEmailName,
        显示名称: noticeEmailDisplayName,
        描述: noticeEmailDescription,
        类型: '邮箱',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54936 : 管理视图，删除通知模板，列表页点击删除，通知模板删除成功。', () => {
    page.listPage.delete(noticeEmailName);
    // page.listPage.search(noticeEmailName, 0);
    // expect(page.listPage.noData.isPresent()).toBeTruthy();
  });
});
