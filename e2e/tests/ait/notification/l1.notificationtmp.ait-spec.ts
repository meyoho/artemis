import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知模板L1 自动化', () => {
  const page = new NotificationPage();
  const noticeSmsName = page.getTestData('noticetmpsms1');
  const noticeOtherName = page.getTestData('noticetmpother1');

  beforeAll(() => {
    page.preparePage.deleteNotifacationTmp(noticeSmsName);
    page.preparePage.deleteNotifacationTmp(noticeOtherName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知模板');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationTmp(noticeSmsName);
    page.preparePage.deleteNotifacationTmp(noticeOtherName);
  });

  it('ACP2UI-54931 : 管理视图，创建通知模板，模板类型选择短信，输入信息，点击创建按钮，验证创建通知模板是否正常。', () => {
    const testData = {
      名称: noticeSmsName,
      显示名称: noticeSmsName.toUpperCase(),
      模板类型: '短信',
    };
    page.createNotificationtmp(testData);

    const expectData = {
      基本信息: {
        名称: noticeSmsName,
        显示名称: noticeSmsName.toUpperCase(),
        描述: '-',
        类型: '短信',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54932 : 管理视图，创建通知模板，模板类型选择企业微信+钉钉+webhook，输入信息，点击创建按钮，验证创建通知模板是否正常。', () => {
    const testData = {
      名称: noticeOtherName,
      显示名称: noticeOtherName.toUpperCase(),
      模板类型: '企业微信/钉钉/webhook',
    };
    page.createNotificationtmp(testData);

    const expectData = {
      基本信息: {
        名称: noticeOtherName,
        显示名称: noticeOtherName.toUpperCase(),
        描述: '-',
        类型: '企业微信/钉钉/webhook',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55760 : 管理视图，更新通知模板，详情页点击更新，更新模板类型，查看通知内容样例，点击更新按钮，更新成功后跳转到详情页且数据正确。', () => {
    page.listPage.enterDetail(noticeSmsName);
    page.detailPage_operate('更新');
    // 更新页修改信息
    const testData1 = {
      模板类型: '邮箱',
      // 查看样例: 'click',
    };
    page.updateNotificationtmp(testData1);

    const expectData = {
      基本信息: {
        名称: noticeSmsName,
        显示名称: noticeSmsName.toUpperCase(),
        描述: '-',
        类型: '邮箱',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55761 : 管理视图，删除通知模板，详情页点击删除，通知模板删除成功。', () => {
    page.listPage.enterDetail(noticeSmsName);
    page.detailPage_operate('删除');
    page.confirmDialog.clickConfirm();
  });
});
