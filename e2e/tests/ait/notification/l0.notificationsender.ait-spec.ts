import { NotificationPage } from '@e2e/page_objects/ait/notification/notification.page';
import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';

describe('管理视图 通知发送人L0 自动化', () => {
  const page = new NotificationPage();
  const noticeServerName = 'uiauto-email163-server';
  const noticeEmailName = ServerConf.EMAILSENDER_NAME1;
  const noticeEmailSecret = ServerConf.EMAILSENDER_PWD1;
  const ispublic = ServerConf.IS_PUBLIC;

  beforeAll(() => {
    page.preparePage.deleteNotifacationSender(noticeEmailName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知发送人');
  });
  afterAll(() => {});
  it('ACP2UI-54912 : 点击添加发送人按钮，选择邮件服务器，输入邮箱地址、密码，点击添加按钮。验证添加发送人是否正常。', () => {
    if (ispublic === 'true') {
      const testData = {
        邮件服务器: `${noticeServerName}(${noticeServerName})`,
        邮箱: noticeEmailName,
        密码: noticeEmailSecret,
      };
      page.createNotificationSender(testData);
    } else {
      console.log('私有环境,不执行创建通知发送人的case');
    }
  });

  it('ACP2UI-54920 : 任意选择一个发送人，点击列表页删除按钮，验证删除发送人是否正常', () => {
    if (ispublic === 'true') {
      page.listPage.delete(noticeEmailName);
    } else {
      console.log('私有环境,不执行删除通知人发送人的case');
    }
  });
});
