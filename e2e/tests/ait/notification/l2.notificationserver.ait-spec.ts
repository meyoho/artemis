/**
 * Created by zhangjiao on 2019/10/31.
 */

import { NotificationPage } from '../../../page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知服务器L2 自动化', () => {
  const page = new NotificationPage();
  const noticeServerName =
    'abcdefghijklmnopqrstuvwxyz26-0abcdefghijklmnopqrst-noticeserver';
  const noticeServerDisplayname1 = page.getTestData('通知服务器11');
  const noticeServerUrl = 'aaaaaaa';
  const noticeServerPort = '9000';

  beforeAll(() => {
    page.preparePage.deleteNotifacationServer(noticeServerName);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知服务器');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationServer(noticeServerName);
  });

  it('ACP2UI-54908 : 管理视图，创建通知服务器，校验名称规则和必填', () => {
    page.getButtonByText('创建通知服务器').click();
    page.createPage.inputByText('名称', 'Daxiezimu'); //输入不合法的名称
    page.createPage.inputByText('名称', '_teshuzifu');
    page.createPage.inputByText('名称', '中文aaaaaaaa');
    page.createPage.inputByText('名称', 'aaaa_aaaaa');
    page.createPage.inputByText('名称', 'aaaajiewei-ss&*');
    page.createPage.inputByText('名称', 'aaaateDAXIE');
    page.createPage.inputByText('名称', 'aaaajiewei-'); // 以- 结尾
    page.createPage.inputByText('名称', '.sdffffffff'); //特殊字符开头
    page.createPage.inputByText('名称', noticeServerName + 'duoyuzifu', ''); // 名称63位

    page.getButtonByText('创建').click(); // 点击创建按钮
    // 显示名称校验
    page.createPage.inputByText('显示名称', '', '显示名称是必填项');
    page.createPage.inputByText('显示名称', noticeServerDisplayname1, '');
    browser.sleep(100);
    page.createPage.inputByText('服务器地址', '', '服务器地址是必填项');
    page.createPage.inputByText('服务器地址', noticeServerUrl, '');
    browser.sleep(100);
    page.createPage.inputByText('端口', '', '端口是必填项');
    page.createPage.inputByText('端口', 'rrr', '端口格式错误');
    page.createPage.inputByText('端口', noticeServerPort, '');
    page.getButtonByText('创建').click(); // 点击创建按钮
    // 判断是否创建成功
    const expectData = {
      基本信息: {
        名称: noticeServerName,
        显示名称: noticeServerDisplayname1,
        描述: '-',
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '否',
        是否跳过非安全验证: '否',
      },
    };
    page.detailPageVerify.verify(expectData);
  });
  /**
   * 以下包含两个用例
   * ACP2UI-54991 : 管理视图，创建通知服务器，校验同名，验证是否正常。
   * ACP2UI-54865 : 管理视图，创建通知服务器，输入信息，点击取消按钮，验证取消创建是否正常。
   */
  xit('ACP2UI-54991|ACP2UI-54865 : 管理视图，创建通知服务器，校验同名，验证是否正常。（包含）', () => {
    const testData = {
      名称: noticeServerName,
      显示名称: noticeServerName.toUpperCase(),
      描述: noticeServerDisplayname1,
      服务器地址: noticeServerUrl,
      端口: noticeServerPort,
    };
    page.createNotificationServer(testData);
    //由于通知服务器已存在
    expect(page.createPage.errorAlert().checkTextIsPresent()).toBeTruthy();
    page.createPage.errorAlert_close.click();
    page.getButtonByText('取消').click(); // 点击取消按钮后返回列表页
    page.listPage.enterDetail(noticeServerName);
  });

  it('ACP2UI-54868 : 管理视图，更新通知服务器，到详情页点击更新，更新信息，点击取消按钮，验证取消更新是否成功。 ', () => {
    page.listPage.enterDetail(noticeServerName);
    page.detailPage_operate('更新');
    page.getButtonByText('取消').click(); // 点击取消按钮后返回详情页
    const expectData = {
      基本信息: {
        名称: noticeServerName,
        显示名称: noticeServerDisplayname1,
        描述: '-',
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '否',
        是否跳过非安全验证: '否',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54906 : 管理视图，删除通知服务器，到详情页点击删除，点击取消按钮，验证取消删除是否正常。', () => {
    page.listPage.enterDetail(noticeServerName);
    page.detailPage_operate('删除');
    page.getButtonByText('取消').click();
    const expectData = {
      基本信息: {
        名称: noticeServerName,
        显示名称: noticeServerDisplayname1,
        描述: '-',
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '否',
        是否跳过非安全验证: '否',
      },
    };
    page.detailPageVerify.verify(expectData);
  });
});
