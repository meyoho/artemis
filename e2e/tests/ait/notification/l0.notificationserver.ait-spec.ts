/**
 * Created by zhangjiao on 2019/10/31.
 */

import { NotificationPage } from '../../../page_objects/ait/notification/notification.page';
import { browser } from 'protractor';

describe('管理视图 通知服务器L0 自动化', () => {
  const page = new NotificationPage();
  const noticeServerName1 = page.getTestData('noticeserver1');
  const noticeServerDisplayname1 = page.getTestData('通知服务器1');
  const noticeServerDescription = '为自动化添加的通知服务其添加描述11aa!';
  const noticeServerUrl = 'aaaaaaa';
  const noticeServerPort = '9000';
  const noticeServerUrl_new = 'acacacaca';
  const noticeServerPort_new = '35';

  const noticeServerName2 = page.getTestData('noticeserver2');
  const noticeServerDisplayname2 = page.getTestData('通知服务器2');

  beforeAll(() => {
    page.preparePage.deleteNotifacationServer(noticeServerName1);
    page.preparePage.deleteNotifacationServer(noticeServerName2);
    browser.sleep(2000);
    page.login();
    page.enterPlatformView('运维中心');
    page.clickLeftNavByText('通知');
  });
  beforeEach(() => {
    page.clickLeftNavByText('通知服务器');
  });
  afterAll(() => {
    page.preparePage.deleteNotifacationServer(noticeServerName1);
    page.preparePage.deleteNotifacationServer(noticeServerName2);
  });

  it('ACP2UI-54863 : 管理视图，创建通知服务器，输入信息，点击创建按钮，验证是否创建成功。', () => {
    const testData = {
      名称: noticeServerName1,
      显示名称: noticeServerName1.toUpperCase(),
      描述: noticeServerDisplayname1,
      服务器地址: noticeServerUrl,
      端口: noticeServerPort,
    };
    page.createNotificationServer(testData);

    const expectData = {
      基本信息: {
        名称: noticeServerName1,
        显示名称: noticeServerName1.toUpperCase(),
        描述: noticeServerDisplayname1,
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '否',
        是否跳过非安全验证: '否',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54864 : 管理视图，创建通知服务器，输入信息(勾选“使用SSL”,勾选“跳过非安全验证”)，点击创建按钮，验证创建是否成功。', () => {
    const testData = {
      名称: noticeServerName2,
      显示名称: noticeServerName2.toUpperCase(),
      描述: noticeServerDisplayname2,
      服务器地址: noticeServerUrl,
      端口: noticeServerPort,
      使用SSL: 'check',
      跳过非安全验证: 'check',
    };
    page.createNotificationServer(testData);

    const expectData = {
      基本信息: {
        名称: noticeServerName2,
        显示名称: noticeServerName2.toUpperCase(),
        描述: noticeServerDisplayname2,
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '是',
        是否跳过非安全验证: '是',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-54867 : 管理视图，更新通知服务器，到详情页点击更新，更新信息，点击更新按钮，验证更新是否成功。', () => {
    page.listPage.enterDetail(noticeServerName1);
    page.detailPage_operate('更新');
    const testData = {
      显示名称: noticeServerDisplayname1,
      描述: noticeServerDescription,
      服务器地址: noticeServerUrl_new,
      端口: noticeServerPort_new,
      使用SSL: 'check',
      跳过非安全验证: 'check',
    };
    page.updateNotificationServer(testData);

    const expectData = {
      基本信息: {
        名称: noticeServerName1,
        显示名称: noticeServerDisplayname1,
        描述: noticeServerDescription,
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl_new,
        端口: noticeServerPort_new,
        是否使用SSL: '是',
        是否跳过非安全验证: '是',
      },
    };
    page.detailPageVerify.verify(expectData);
  });

  it('ACP2UI-55785 : 管理视图，更新通知服务器，到详情页点击更新，更新信息(取消勾选"使用SSL"，取消勾选"跳过非安全验证")，点击更新按钮，验证更新是否成功。', () => {
    page.listPage.enterDetail(noticeServerName2);
    page.detailPage_operate('更新');
    const testData = {
      显示名称: noticeServerDisplayname2,
      描述: noticeServerDescription,
      使用SSL: 'uncheck',
      跳过非安全验证: 'uncheck',
    };
    page.updateNotificationServer(testData);

    const expectData = {
      基本信息: {
        名称: noticeServerName2,
        显示名称: noticeServerDisplayname2,
        描述: noticeServerDescription,
        类型: '邮箱',
      },
      服务器配置: {
        主机: noticeServerUrl,
        端口: noticeServerPort,
        是否使用SSL: '否',
        是否跳过非安全验证: '否',
      },
    };
    page.detailPageVerify.verify(expectData);
  });
  it('ACP2UI-54869 : 管理视图，删除通知服务器，到详情页点击删除，输入名称确认，点击删除，验证删除是否正常。', () => {
    page.listPage.enterDetail(noticeServerName2);
    page.detailPage_operate('删除');
    page.confirmDialog_name.clickDelete(noticeServerName2); // 输入绑定名称后点击删除按钮
  });
});
