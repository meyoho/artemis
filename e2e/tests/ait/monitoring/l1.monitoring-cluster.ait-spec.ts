import { ClusterPage } from '@e2e/page_objects/platform/cluster/cluster.page';
import { MonitoringDetail } from '@e2e/page_objects/ait/monitoring/monitoring.page';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

describe('管理视图集群下的监控 L1 case', () => {
  const clusterpage = new ClusterPage();
  const monitoringpage = new MonitoringDetail();

  const clusterglobal = 'global';
  const clusterregion = ServerConf.REGIONNAME;
  const clusterregion_node = CommonKubectl.execKubectlCommand(
    `kubectl get cluster ${clusterregion} -o custom-columns='':.spec.machines[0].ip`,
  ).trim();

  beforeAll(() => {
    clusterpage.login();
    clusterpage.enterPlatformView('平台管理');
    clusterpage.clickLeftNavByText('集群管理');
  });
  beforeEach(() => {
    clusterpage.clickLeftNavByText('集群');
  });
  afterAll(() => {});
  it('ACP2UI-4073|ACP2UI-4074|ACP2UI-4075 : 管理视图，集群详情页，监控tab页，聚合方式选择平均值，查看各个时间范围内的集群监控数据是否正确', () => {
    clusterpage.listPage.enterDetail(clusterglobal);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.value_dropdown('最大值');
    monitoringpage.monitoring_view(`${clusterglobal}:最大值`, 'CPU利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:最大值`, '内存利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:最大值`, 'API Server响应');
    monitoringpage.monitoring_view(`${clusterglobal}:最大值`, 'ETCD客户端流量');
    monitoringpage.monitoring_line(`${clusterglobal}:最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.monitoring_view(`${clusterglobal}:最小值`, 'CPU利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:最小值`, '内存利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:最小值`, 'API Server响应');
    monitoringpage.monitoring_view(`${clusterglobal}:最小值`, 'ETCD客户端流量');
    monitoringpage.monitoring_line(`${clusterglobal}:最小值`);
    monitoringpage.value_dropdown('平均值');
    monitoringpage.monitoring_view(`${clusterglobal}:平均值`, 'CPU利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:平均值`, '内存利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:平均值`, 'API Server响应');
    monitoringpage.monitoring_view(`${clusterglobal}:平均值`, 'ETCD客户端流量');
    monitoringpage.monitoring_line(`${clusterglobal}:平均值`);
    monitoringpage.time_dropdown('过去1小时');
    monitoringpage.monitoring_view(`${clusterglobal}:1小时`, 'CPU利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:1小时`, '内存利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:1小时`, 'API Server响应');
    monitoringpage.monitoring_view(`${clusterglobal}:1小时`, 'ETCD客户端流量');
    monitoringpage.monitoring_line(`${clusterglobal}:1小时`);
    monitoringpage.time_dropdown('过去1天');
    monitoringpage.monitoring_view(`${clusterglobal}:1天`, 'CPU利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:1天`, '内存利用率');
    monitoringpage.monitoring_view(`${clusterglobal}:1天`, 'API Server响应');
    monitoringpage.monitoring_view(`${clusterglobal}:1天`, 'ETCD客户端流量');
    monitoringpage.monitoring_line(`${clusterglobal}:1天`);
  });

  it('ACP2UI-4077|ACP2UI-4078|ACP2UI-4079 : 管理视图，选择一个集群再点击到主机详情页，监控tab页，聚合方式选择平均值，查看各个时间范围内主机监控数据是否正确', () => {
    clusterpage.listPage.enterDetail(clusterregion);
    clusterpage.detailPage.enterNodeDetail(clusterregion_node);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.value_dropdown('最大值');
    monitoringpage.monitoring_view(`${clusterregion_node}:最大值`, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(`${clusterregion_node}:最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.monitoring_view(`${clusterregion_node}:最小值`, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(`${clusterregion_node}:最小值`);
    monitoringpage.value_dropdown('平均值');
    monitoringpage.monitoring_view(`${clusterregion_node}:平均值`, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(`${clusterregion_node}:平均值`);
    monitoringpage.time_dropdown('过去6小时');
    monitoringpage.monitoring_view(`${clusterregion_node}:3小时`, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(`${clusterregion_node}:6小时`);
    monitoringpage.time_dropdown('过去7天');
    monitoringpage.monitoring_view(`${clusterregion_node}:7天`, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(`${clusterregion_node}:7天`);
  });
});
