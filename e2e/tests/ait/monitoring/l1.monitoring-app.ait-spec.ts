import { MonitoringDetail } from '@e2e/page_objects/ait/monitoring/monitoring.page';
import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DaemonSetListPage } from '@e2e/page_objects/acp/daemonset/list.page';
import { StatefulSetListPage } from '@e2e/page_objects/acp/statefulset/list.page';
import { PodsPage } from '@e2e/page_objects/acp/pods/pods.page';
import { DetailDeploymentPage } from '@e2e/page_objects/acp/deployment/detail.page';

describe('业务视图应用下的监控 L1 case', () => {
  const app_page = new Application();
  const deploy_list_page = new DeploymentListPage();
  const daemonset_list_page = new DaemonSetListPage();
  const statefulset_list_page = new StatefulSetListPage();
  const podpage = new PodsPage();
  const deploy_detail_page = new DetailDeploymentPage();
  const monitoringpage = new MonitoringDetail();

  const appdeployname = 'auto-ait-appdeploy1';
  const deployName = `${appdeployname}-qaimages`;
  const appstatefulesetname = 'auto-ait-appstatefulset1';
  const statefulsetName = `${appstatefulesetname}-qaimages`; //有状态副本集
  const appdaemonsetname = 'auto-ait-appdaemonset1';
  const daemonsetName = `${appdaemonsetname}-qaimages`;

  beforeAll(() => {
    monitoringpage.login();
    monitoringpage.enterUserView(monitoringpage.namespace1Name);
  });
  beforeEach(() => {
    monitoringpage.clickLeftNavByText('应用');
  });
  afterAll(() => {});
  it('ACP2UI-59118 : 创建部署模式是deployment的应用，查看deployment的详情页监控，按分组方式，聚合方式，时间范围查看监控数据', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.group_dropdown('Pod');
    monitoringpage.monitoring_view(`${deployName}:Pod平均值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:Pod平均值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:Pod平均值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:Pod平均值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:Pod平均值`);
    monitoringpage.value_dropdown('最大值');
    monitoringpage.time_dropdown('过去1小时');
    monitoringpage.monitoring_view(`${deployName}:Pod最大值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:Pod最大值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:Pod最大值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:Pod最大值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:Pod最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.time_dropdown('过去1小时');
    monitoringpage.monitoring_view(`${deployName}:最小值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:最小值`);
    monitoringpage.group_dropdown('容器');
    monitoringpage.monitoring_view(`${deployName}:容器`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:容器`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:容器`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:容器`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:容器`);
    monitoringpage.group_dropdown('工作负载');
    monitoringpage.time_dropdown('过去1天');
    monitoringpage.monitoring_view(`${deployName}:工作负载1天`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:工作负载1天`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:工作负载1天`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:工作负载1天`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:工作负载1天`);
    monitoringpage.value_dropdown('最大值');
    monitoringpage.monitoring_view(`${deployName}:最大值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:最大值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:最大值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:最大值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.monitoring_view(`${deployName}:最小值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:最小值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:最小值`);
    monitoringpage.value_dropdown('平均值');
    monitoringpage.monitoring_view(`${deployName}:平均值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:平均值`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:平均值`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:平均值`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:平均值`);
    monitoringpage.time_dropdown('过去30分钟');
    monitoringpage.monitoring_view(`${deployName}:过去30分钟`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${deployName}:过去30分钟`, '内存利用率');
    monitoringpage.monitoring_view(`${deployName}:过去30分钟`, '发送字节');
    monitoringpage.monitoring_view(`${deployName}:过去30分钟`, '接收字节');
    monitoringpage.monitoring_line(`${deployName}:过去30分钟`);
  });
  it('ACP2UI-59117 : 创建部署模式是daemonset的应用，查看daemonset的详情页监控，按分组方式，聚合方式，时间范围查看监控数据', () => {
    daemonset_list_page.enterDetail(daemonsetName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.group_dropdown('Pod');
    monitoringpage.monitoring_view(`${daemonsetName}:Pod`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:Pod`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:Pod`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:Pod`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:Pod`);
    monitoringpage.group_dropdown('容器');
    monitoringpage.monitoring_view(`${daemonsetName}:容器`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:容器`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:容器`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:容器`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:容器`);
    monitoringpage.group_dropdown('工作负载');
    monitoringpage.monitoring_view(`${daemonsetName}:工作负载`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:工作负载`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:工作负载`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:工作负载`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:工作负载`);
    monitoringpage.value_dropdown('最大值');
    monitoringpage.monitoring_view(`${daemonsetName}:最大值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:最大值`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:最大值`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:最大值`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.monitoring_view(`${daemonsetName}:最小值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:最小值`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:最小值`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:最小值`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:最小值`);
    monitoringpage.value_dropdown('平均值');
    monitoringpage.monitoring_view(`${daemonsetName}:平均值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:平均值`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:平均值`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:平均值`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:平均值`);
    monitoringpage.time_dropdown('过去6小时');
    monitoringpage.monitoring_view(`${daemonsetName}:过去6小时`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:过去6小时`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:过去6小时`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:过去6小时`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:过去6小时`);
    monitoringpage.time_dropdown('过去3天');
    monitoringpage.monitoring_view(`${daemonsetName}:过去3天`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:过去3天`, '内存利用率');
    monitoringpage.monitoring_view(`${daemonsetName}:过去3天`, '发送字节');
    monitoringpage.monitoring_view(`${daemonsetName}:过去3天`, '接收字节');
    monitoringpage.monitoring_line(`${daemonsetName}:过去3天`);
  });
  it('ACP2UI-59119 : 创建部署模式是statefulset的应用，查看statefulset的详情页监控，按分组方式，聚合方式，时间范围查看监控数据', () => {
    statefulset_list_page.enterDetail(statefulsetName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.group_dropdown('Pod');
    monitoringpage.monitoring_view(`${statefulsetName}:Pod`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:Pod`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:Pod`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:Pod`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:Pod`);
    monitoringpage.group_dropdown('容器');
    monitoringpage.monitoring_view(`${statefulsetName}:容器`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:容器`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:容器`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:容器`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:容器`);
    monitoringpage.group_dropdown('工作负载');
    monitoringpage.monitoring_view(`${statefulsetName}:工作负载`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:工作负载`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:工作负载`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:工作负载`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:工作负载`);
    monitoringpage.value_dropdown('最大值');
    monitoringpage.monitoring_view(`${statefulsetName}:最大值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:最大值`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:最大值`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:最大值`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:最大值`);
    monitoringpage.value_dropdown('最小值');
    monitoringpage.monitoring_view(`${statefulsetName}:最小值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:最小值`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:最小值`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:最小值`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:最小值`);
    monitoringpage.value_dropdown('平均值');
    monitoringpage.monitoring_view(`${statefulsetName}:平均值`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:平均值`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:平均值`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:平均值`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:平均值`);
    monitoringpage.time_dropdown('自定义时间');
    monitoringpage.monitoring_view(`${statefulsetName}:自定义`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:自定义`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:自定义`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:自定义`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:自定义`);
    monitoringpage.time_dropdown('过去7天');
    monitoringpage.monitoring_view(`${statefulsetName}:过去7天`, 'CPU 利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:过去7天`, '内存利用率');
    monitoringpage.monitoring_view(`${statefulsetName}:过去7天`, '发送字节');
    monitoringpage.monitoring_view(`${statefulsetName}:过去7天`, '接收字节');
    monitoringpage.monitoring_line(`${statefulsetName}:过去7天`);
  });

  it('ACP2UI-59120 : 查看deployment的容器组下的监控，按照分组方式，聚合方式，时间范围查看监控数据', () => {
    app_page.appListPage.toAppDetail(appdeployname);
    app_page.detailAppPage.getTab('容器组').click();
    deploy_detail_page.getPodNameByRowIndex(0).then(pod_name => {
      podpage.podListPage.podListTable.clickResourceNameByRow([pod_name]);
      app_page.detailAppPage.getTab('监控').click();
      monitoringpage.monitoring_view(`${pod_name}:Pod`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:Pod`);
      monitoringpage.group_dropdown('容器');
      monitoringpage.monitoring_view(`${pod_name}:容器`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:容器`);
    });
  });
  it('ACP2UI-59121 : 查看deamonset的容器组下的监控，按照分组方式，聚合方式，时间范围查看监控数据', () => {
    app_page.appListPage.toAppDetail(appdaemonsetname);
    app_page.detailAppPage.getTab('容器组').click();
    deploy_detail_page.getPodNameByRowIndex(2).then(pod_name => {
      podpage.podListPage.podListTable.clickResourceNameByRow([pod_name]);
      app_page.detailAppPage.getTab('监控').click();
      monitoringpage.monitoring_view(`${pod_name}:Pod`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:Pod`);
      monitoringpage.group_dropdown('容器');
      monitoringpage.monitoring_view(`${pod_name}:容器`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:容器`);
    });
  });
  it('ACP2UI-59122 : 查看statefulset的容器组下的监控，按照分组方式，聚合方式，时间范围查看监控数据', () => {
    app_page.appListPage.toAppDetail(appstatefulesetname);
    app_page.detailAppPage.getTab('容器组').click();
    deploy_detail_page.getPodNameByRowIndex(0).then(pod_name => {
      podpage.podListPage.podListTable.clickResourceNameByRow([pod_name]);
      app_page.detailAppPage.getTab('监控').click();
      monitoringpage.monitoring_view(`${pod_name}:Pod`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:Pod`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:Pod`);
      monitoringpage.group_dropdown('容器');
      monitoringpage.monitoring_view(`${pod_name}:容器`, 'CPU 利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '内存利用率');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '发送字节');
      monitoringpage.monitoring_view(`${pod_name}:容器`, '接收字节');
      monitoringpage.monitoring_line(`${pod_name}:容器`);
    });
  });
});
