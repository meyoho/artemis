import { MonitoringDetail } from '@e2e/page_objects/ait/monitoring/monitoring.page';
import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';
import { Application } from '@e2e/page_objects/acp/appcore/application.page';
import { DaemonSetListPage } from '@e2e/page_objects/acp/daemonset/list.page';
import { StatefulSetListPage } from '@e2e/page_objects/acp/statefulset/list.page';

describe('业务视图应用下的监控 L0 case', () => {
  const app_page = new Application();
  const deploy_list_page = new DeploymentListPage();
  const daemonset_list_page = new DaemonSetListPage();
  const statefulset_list_page = new StatefulSetListPage();
  const monitoringpage = new MonitoringDetail();

  const appdeployname = 'auto-ait-appdeploy1';
  const deployName = `${appdeployname}-qaimages`;
  const appstatefulesetname = 'auto-ait-appstatefulset1';
  const statefulsetName = `${appstatefulesetname}-qaimages`; //有状态副本集
  const appdaemonsetname = 'auto-ait-appdaemonset1';
  const daemonsetName = `${appdaemonsetname}-qaimages`;

  beforeAll(() => {
    monitoringpage.login();
    monitoringpage.enterUserView(monitoringpage.namespace1Name);
  });
  beforeEach(() => {
    monitoringpage.clickLeftNavByText('应用');
  });
  afterAll(() => {});

  it('ACP2UI-4080 : 用户视图-应用管理-应用-监控tab页-监控信息', () => {
    app_page.appListPage.toAppDetail(appdeployname);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.monitoring_view(appdeployname, 'CPU 利用率');
    monitoringpage.monitoring_view(appdeployname, '内存利用率');
    monitoringpage.monitoring_view(appdeployname, '发送字节');
    monitoringpage.monitoring_view(appdeployname, '接收字节');
    monitoringpage.monitoring_line(appdeployname);
  });
  it('ACP2UI-4081 : 创建部署模式是deployment的应用，查看应用详情页的监控和deployment详情页的监控数据是否正确', () => {
    deploy_list_page.enterDetail(deployName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.monitoring_view(deployName, 'CPU 利用率');
    monitoringpage.monitoring_view(deployName, '内存利用率');
    monitoringpage.monitoring_view(deployName, '发送字节');
    monitoringpage.monitoring_view(deployName, '接收字节');
    monitoringpage.monitoring_line(deployName);
  });
  it('ACP2UI-4095 : 创建部署模式是daemonset的应用，查看应用详情页的监控和daemonset详情页的监控数据是否正确', () => {
    daemonset_list_page.enterDetail(daemonsetName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.monitoring_view(daemonsetName, 'CPU 利用率');
    monitoringpage.monitoring_view(daemonsetName, '内存利用率');
    monitoringpage.monitoring_view(daemonsetName, '发送字节');
    monitoringpage.monitoring_view(daemonsetName, '接收字节');
    monitoringpage.monitoring_line(daemonsetName);
  });
  it('ACP2UI-4096 : 创建部署模式是statefulset的应用，查看应用详情页的监控数据和statefulset详情页的监控是否正确', () => {
    statefulset_list_page.enterDetail(statefulsetName);
    app_page.detailAppPage.getTab('监控').click();
    monitoringpage.monitoring_view(statefulsetName, 'CPU 利用率');
    monitoringpage.monitoring_view(statefulsetName, '内存利用率');
    monitoringpage.monitoring_view(statefulsetName, '发送字节');
    monitoringpage.monitoring_view(statefulsetName, '接收字节');
    monitoringpage.monitoring_line(statefulsetName);
  });
});
