import { ClusterPage } from '@e2e/page_objects/platform/cluster/cluster.page';
import { MonitoringDetail } from '@e2e/page_objects/ait/monitoring/monitoring.page';
import { ServerConf } from '@e2e/config/serverConf';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

describe('管理视图集群下的监控 L0 case', () => {
  const clusterpage = new ClusterPage();
  const monitoringpage = new MonitoringDetail();

  const clusterglobal = 'global';
  const clusterglobal_node = CommonKubectl.execKubectlCommand(
    `kubectl get node | grep master | awk '{print $1}'`,
  )
    .split('\n')[0]
    .trim();
  const clusterregion = ServerConf.REGIONNAME;
  const clusterregion_node = CommonKubectl.execKubectlCommand(
    `kubectl get cluster ${clusterregion} -o custom-columns='':.spec.machines[0].ip`,
  ).trim();

  beforeAll(() => {
    clusterpage.login();
    clusterpage.enterPlatformView('平台管理');
    clusterpage.clickLeftNavByText('集群管理');
  });
  beforeEach(() => {
    clusterpage.clickLeftNavByText('集群');
  });
  afterAll(() => {});

  it('ACP2UI-57719 : 管理视图，集群列表页，已部署Prometheus的集群显示CPU和内存已使用占比', () => {
    clusterpage.listPage.list_monitoring(clusterglobal);
    clusterpage.listPage.list_monitoring(clusterregion);
  });
  it('ACP2UI-57720 : 管理视图，集群详情页，主机列表中资源占比列会显示CPU和内存的已使用占比', () => {
    clusterpage.listPage.enterDetail(clusterglobal);
    clusterpage.detailPage.nodetable_monitoring(clusterglobal_node);
    clusterpage.detailPage.clickCrumb(
      '/console-platform/manage-platform/cluster',
    ); //点击面包屑的集群管理到列表页
    clusterpage.listPage.enterDetail(clusterregion);
    clusterpage.detailPage.nodetable_monitoring(clusterregion_node);
  });
  it('ACP2UI-4072 : 验证global集群监控详情', () => {
    clusterpage.listPage.enterDetail(clusterglobal);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.monitoring_view(clusterglobal, 'CPU利用率');
    monitoringpage.monitoring_view(clusterglobal, '内存利用率');
    monitoringpage.monitoring_view(clusterglobal, 'API Server响应');
    monitoringpage.monitoring_view(clusterglobal, 'ETCD客户端流量');
    monitoringpage.monitoring_line(clusterglobal);
  });
  it('ACP2UI-4076 : 验证global集群的主机监控详情', () => {
    clusterpage.listPage.enterDetail(clusterglobal);
    clusterpage.detailPage.enterNodeDetail(clusterglobal_node);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.monitoring_view(clusterglobal_node, 'CPU利用率');
    monitoringpage.monitoring_view(clusterglobal_node, '内存利用率');
    monitoringpage.monitoring_view(clusterglobal_node, '系统负载');
    monitoringpage.monitoring_view(clusterglobal_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterglobal_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterglobal_node, '网络流量');
    monitoringpage.monitoring_line(clusterglobal_node);
  });
  it('ACP2UI-59111 : 验证业务集群监控详情', () => {
    clusterpage.listPage.enterDetail(clusterregion);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.monitoring_view(clusterregion, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion, '内存利用率');
    monitoringpage.monitoring_view(clusterregion, 'API Server响应');
    monitoringpage.monitoring_view(clusterregion, 'ETCD客户端流量');
    monitoringpage.monitoring_line(clusterregion);
  });
  it('ACP2UI-59112 : 验证业务集群的主机监控详情', () => {
    clusterpage.listPage.enterDetail(clusterregion);
    clusterpage.detailPage.enterNodeDetail(clusterregion_node);
    clusterpage.detailPage.clickTab('监控');
    monitoringpage.monitoring_view(clusterregion_node, 'CPU利用率');
    monitoringpage.monitoring_view(clusterregion_node, '内存利用率');
    monitoringpage.monitoring_view(clusterregion_node, '系统负载');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘利用率');
    monitoringpage.monitoring_view(clusterregion_node, '磁盘读写');
    monitoringpage.monitoring_view(clusterregion_node, '网络流量');
    monitoringpage.monitoring_line(clusterregion_node);
  });
});
