// import { ServiceMeshPage } from '@e2e/page_objects/asm/admin_view_service_mesh/service_mesh.page';
// import { VerifyServiceMeshListPage } from '@e2e/page_verify/asm/admin_view_service_mesh/service_mesh_list_verify.page';
// import { ServerConf } from '@e2e/config/serverConf';
// import { VerifyServiceMeshDetailPage } from '@e2e/page_verify/asm/admin_view_service_mesh/service_mesh_detail_verify.page';

// describe('服务网格 L1相关case', () => {
//   const servicemesh = new ServiceMeshPage();
//   const verifyListPage = new VerifyServiceMeshListPage();
//   const verifyDetailPage = new VerifyServiceMeshDetailPage();
//   const service_mesh_name = servicemesh.serviceMeshName;
//   const displayName = servicemesh.getTestData('UI自动化更新的显示名称');
//   // const traceSampling = '55.44';
//   beforeAll(() => {
//     servicemesh.login();
//     servicemesh.enterAdminView();
//     servicemesh.clickLeftNavByText('服务网格');
//   });

//   it('ACP2UI-57823 : L1:列表页展示', () => {
//     const testData = {
//       名称: service_mesh_name,
//       集群: ServerConf.REGIONNAME,
//       状态: '成功',
//     };
//     verifyListPage.verifyListInfo(testData, [service_mesh_name]);
//   });

//   it('ACP2UI-57813 : L1:更新服务网格-更新显示名称，更新成功', () => {
//     servicemesh.listPage.serviceMeshTable.clickResourceNameByRow([
//       service_mesh_name,
//     ]);
//     const testData = {
//       显示名称: displayName,
//     };

//     servicemesh.detailPage.updateServiceMesh(testData);
//     const expectData = {
//       显示名称: displayName,
//     };
//     verifyDetailPage.verifyDetailInfo(expectData);
//   });

//   // it('ACP2UI-57808 : L0:更新服务网格-更新采样率为55.44，更新成功', () => {
//   //   const testData = {
//   //     采样率: traceSampling,
//   //   };

//   //   servicemesh.detailPage.updateServiceMesh(testData);
//   //   const expectData = {
//   //     采样率: traceSampling,
//   //   };
//   //   verifyDetailPage.verifyDetailInfo(expectData);
//   // });
// });
