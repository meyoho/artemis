import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';

describe('负载均衡 L1case测试 ', () => {
  const loadbalancer = new MicroServicePage();
  const expectMethod = new MicroDetailPageVerify();

  const microservice_name = 'loadbalancer';
  const namespace_name = loadbalancer.namespace1Name;
  const service = 'lb-deploy';
  const version = 'v1';
  beforeAll(() => {
    loadbalancer.login();
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      loadbalancer.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      loadbalancer.clusterName,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': loadbalancer.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.lb-policy' + String(new Date().getMilliseconds()),
      loadbalancer.clusterName,
    );
    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.outlierdetecttion' + String(new Date().getMilliseconds()),
      loadbalancer.clusterName,
    );
    loadbalancer.enterUserView(
      loadbalancer.projectName,
      loadbalancer.clusterName,
      loadbalancer.namespace1Name,
    );
    loadbalancer.clickLeftNavByText('服务列表');
    //进入微服务详情页
    loadbalancer.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击拓扑图service节点 和 点击策略tab页
    //loadbalancer.microServiceDetailPage._serviceNode.click();
    loadbalancer.microServiceDetailPage._strategyTab.click();
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      loadbalancer.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      loadbalancer.clusterName,
    );
    loadbalancer.deleteResource('service', service, namespace_name);
  });

  it('创建负载均衡--取消', () => {
    loadbalancer.microServiceDetailPage.getButtonByText('创建策略').click();
    loadbalancer.microServiceDetailPage.getButtonByText('负载均衡').click();
    loadbalancer.microServiceDetailPage._dialog.cancel();
    expectMethod.verifyStrategyNum(0);
  });

  it('ACP2UI-858 : L1: 服务列表-微服务详情页（已添加服务入口）-策略-创建策略-负载均衡-轮询', () => {
    const testData = {
      负载均衡策略: '轮询',
    };

    loadbalancer.microServiceDetailPage.createLoadBalancer(testData);
    const expectData = {
      策略类型: ['负载均衡'],
      策略详情: ['轮询'],
    };

    expectMethod.verifyStrategyInfo(expectData);
    expectMethod.verifyStrategyNum(1);
  });

  it('ACP2UI-4167 : L1: 服务列表-微服务详情页（已创建负载均衡）-策略-更新负载均衡', () => {
    const testData = {
      负载均衡策略: '最小请求负载',
    };

    loadbalancer.microServiceDetailPage.updateStrategy(testData);
    const expectData = {
      策略类型: ['负载均衡'],
      策略详情: ['最小请求负载'],
    };

    expectMethod.verifyStrategyInfo(expectData);
  });

  it('ACP2UI-862 : L1: 服务列表-微服务详情页（已创建负载均衡）-策略-删除负载均衡', () => {
    loadbalancer.microServiceDetailPage.deleteStrategy();
    expectMethod.verifyStrategyNum(0);
  });
});
