import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('连接池设置 L1 case测试', () => {
  const connectionpool = new MicroServicePage();
  const expectMethod = new MicroDetailPageVerify();
  const expectYaml = new YamlVerify();

  const microservice_name = 'connectionpool';
  const namespace_name = connectionpool.namespace1Name;
  const service = 'cp-deploy';
  const version = 'v1';
  beforeAll(() => {
    connectionpool.login();
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      connectionpool.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      connectionpool.clusterName,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': connectionpool.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.cp-policy' + String(new Date().getMilliseconds()),
      connectionpool.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.outlierdetecttion' + String(new Date().getMilliseconds()),
      connectionpool.clusterName,
    );
    connectionpool.enterUserView(
      connectionpool.projectName,
      connectionpool.clusterName,
      connectionpool.namespace1Name,
    );
    connectionpool.clickLeftNavByText('服务列表');
    //进入微服务详情页
    connectionpool.microServiceListPage.serviceListTable.clickResourceNameByRow(
      [microservice_name],
    );
    // 点击拓扑图service节点 和 点击策略tab页
    //connectionpool.microServiceDetailPage._serviceNode.click();
    connectionpool.microServiceDetailPage._strategyTab.click();
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      connectionpool.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      connectionpool.clusterName,
    );
    connectionpool.deleteResource('service', service, namespace_name);
  });

  it('创建连接池设置--取消', () => {
    connectionpool.microServiceDetailPage.getButtonByText('创建策略').click();
    connectionpool.microServiceDetailPage.getButtonByText('连接池设置').click();
    connectionpool.microServiceDetailPage.getButtonByText('HTTP/HTTP2').click();
    connectionpool.microServiceDetailPage._dialog.cancel();
    expectMethod.verifyStrategyNum(0);
  });

  it('ACP2UI-54822 : L1:创建连接池设置策略并生效-HTTP| ACP2UI-54841 : L1:创建连接池设置策略并生效-HTTP2', () => {
    const testData = {
      最大连接数: 10,
      最大请求重试数: 4,
      每连接最大请求数: 10,
      最大等待请求数: 100,
      最大请求数: 1000,
    };

    connectionpool.microServiceDetailPage.createConnectionPoolSettings(
      testData,
    );

    const expectData = {
      策略类型: ['连接池设置\n(HTTP/HTTP2)'],
      策略详情: [
        '最大连接数：10\n最大请求重试数：4\n每连接最大请求数：10\n最大等待请求数：100\n最大请求数：1000',
      ],
    };
    const expectCpJson = {
      spec: {
        host: service,
        http: {
          httpMaxPendingRequests: 100,
          httpMaxRequests: 1000,
          maxRequestsPerConnection: 10,
          maxRetries: 4,
        },
        tcp: {
          maxConnections: 10,
        },
      },
    };
    const expectDestinationrulesJson = {
      spec: {
        host: service,
        trafficPolicy: {
          connectionPool: {
            http: {
              http1MaxPendingRequests: 100,
              http2MaxRequests: 1000,
              maxRequestsPerConnection: 10,
              maxRetries: 4,
            },
            tcp: {
              maxConnections: 10,
            },
          },
        },
      },
    };

    expectMethod.verifyStrategyInfo(expectData);
    expectMethod.verifyStrategyNum(1).then(() => {
      expectYaml.verifyYaml(
        'destinationrule',
        `asm-${service}`,
        namespace_name,
        expectDestinationrulesJson,
      );
      expectYaml.verifyYaml('cp', service, namespace_name, expectCpJson);
    });
  });

  it('ACP2UI-54823 : L1:更新连接池设置策略', () => {
    const testData = {
      最大连接数: 20,
      最大请求重试数: 10,
      每连接最大请求数: 9,
      最大等待请求数: 19,
      最大请求数: 40,
    };

    connectionpool.microServiceDetailPage.updateStrategy(testData);
    const expectData = {
      策略类型: ['连接池设置\n(HTTP/HTTP2)'],
      策略详情: [
        '最大连接数：20\n最大请求重试数：10\n每连接最大请求数：9\n最大等待请求数：19\n最大请求数：40',
      ],
    };
    const expectCpJson = {
      spec: {
        host: service,
        http: {
          httpMaxPendingRequests: 19,
          httpMaxRequests: 40,
          maxRequestsPerConnection: 9,
          maxRetries: 10,
        },
        tcp: {
          maxConnections: 20,
        },
      },
    };
    const expectDestinationrulesJson = {
      spec: {
        host: service,
        trafficPolicy: {
          connectionPool: {
            http: {
              http1MaxPendingRequests: 19,
              http2MaxRequests: 40,
              maxRequestsPerConnection: 9,
              maxRetries: 10,
            },
            tcp: {
              maxConnections: 20,
            },
          },
        },
      },
    };
    expectMethod.verifyStrategyInfo(expectData).then(() => {
      expectYaml.verifyYaml(
        'destinationrule',
        `asm-${service}`,
        namespace_name,
        expectDestinationrulesJson,
      );
      expectYaml.verifyYaml('cp', service, namespace_name, expectCpJson);
    });
  });

  it('ACP2UI-54825 : L1:删除连接池设置策略', () => {
    connectionpool.microServiceDetailPage.deleteStrategy();
    expectMethod.verifyStrategyNum(0).then(() => {
      expect(expectYaml.isDelete('cp', service, namespace_name)).toBe(true);
    });
  });
});
