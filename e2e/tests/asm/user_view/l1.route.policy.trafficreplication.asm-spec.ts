import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('ASM 路由策略-流量复制 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const name = 'route-traffic';
  const service = 'route-traffic';
  const microservice_name = 'route-traffic';
  const version = 'v1';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deployments.apps', service, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-trffic' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.route-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建路由
    CommonKubectl.createResource(
      'alauda.asm.virtualservice.yaml',
      {
        '${service}': service,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${name}': name,
        '${namespace}': namespace_name,
      },
      'L1.virtualservice-copy' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    routePage.waitDeployReady(service);

    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    routePage.clickLeftNavByText('服务列表');

    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击路由tab页
    routePage.microServiceDetailPage._route.click();
  });

  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);
  });

  it('ACP2UI-1102 : L1:service-a调用service-b，在service-b 路由权重上配置流量复制，验证配置生效', () => {
    const testData = {
      目标服务: service,
      目标服务版本: 'v1',
      目标服务端口: 80,
    };
    routePage.policyPage.createPolicy('权重规则', '流量复制', testData);

    const expectData = {
      权重规则: `流量复制\n目标服务:${service}\n目标服务版本:v1\n目标服务端口:80`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1115 : L1:service-a调用service-b，更新service-b 路由权重上配置流量复制，验证配置生效', () => {
    const testData = {
      目标服务: service,
      目标服务版本: 'v1',
      目标服务端口: 80,
    };

    routePage.policyPage.updatePolicy('权重规则', testData);
    const expectData = {
      权重规则: `流量复制\n目标服务:${service}\n目标服务版本:v1\n目标服务端口:80`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1113 : L1:service-a调用service-b，在service-b 路由权重上配置流量复制，删除流量复制策略，验证配置生效', () => {
    routePage.policyPage.deletePolicy('权重规则');
    routePage.policyPage.policyCardIsPresent('权重规则').then(number => {
      expect(number).toBe(0);
    });
  });

  it('ACP2UI-1104 : L1:service-a调用service-b，在service-b 路由规则上配置流量复制，验证配置生效', () => {
    const testData = {
      目标服务: service,
      目标服务版本: 'v1',
      目标服务端口: 80,
    };
    routePage.policyPage.createPolicy('条件规则', '流量复制', testData);

    const expectData = {
      条件规则: `流量复制\n目标服务:${service}\n目标服务版本:v1\n目标服务端口:80`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1116 : L1:service-a调用service-b，更新service-b 路由规则上配置流量复制，验证配置生效', () => {
    const testData = {
      目标服务: service,
      目标服务版本: 'v1',
      目标服务端口: 80,
    };

    routePage.policyPage.updatePolicy('条件规则', testData);
    const expectData = {
      条件规则: `流量复制\n目标服务:${service}\n目标服务版本:v1\n目标服务端口:80`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1114 : L1:service-a调用service-b，在service-b 路由规则上配置流量复制，删除流量复制策略，验证配置生效', () => {
    routePage.policyPage.deletePolicy('条件规则');
    routePage.policyPage.policyCardIsPresent('条件规则').then(number => {
      expect(number).toBe(0);
    });
  });
});
