import { CommonKubectl } from '@e2e/utility/common.kubectl';

import { ServerConf } from '../../../config/serverConf';
import { GatewayPage } from '../../../page_objects/asm/gateway/gateway.page';
import { GatewayDetailVerify } from '@e2e/page_verify/asm/gateway/gateway_detail_verify.page';
import { GatewayListVerify } from '@e2e/page_verify/asm/gateway/gateway_list_verify.page';

describe('服务网关 L0级别case', () => {
  const gatewayPage = new GatewayPage();
  const verifyPage = new GatewayDetailVerify();
  const listVerify = new GatewayListVerify();
  const project_name = gatewayPage.projectName;
  const namespace_name = gatewayPage.namespace1Name;
  const name = 'gateway-only';
  const updateDisplayName = 'update-name';

  const service = 'gateway-deploy';
  const microservice_name = 'gateway-only';
  const version = 'test-gateway';
  const port = '80';

  const gateway_entry = gatewayPage.getTestData('entry.only');

  beforeAll(() => {
    gatewayPage.login();
    gatewayPage.deleteResource('deploy', service, namespace_name);
    gatewayPage.deleteResource('service', service, namespace_name);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
    gatewayPage.deleteResource('gateway', name, namespace_name);
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': gatewayPage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.gateway-only' + String(new Date().getMilliseconds()),
      gatewayPage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.gateway-only' + String(new Date().getMilliseconds()),
      gatewayPage.clusterName,
    );

    gatewayPage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    gatewayPage.clickLeftNavByText('服务网关');
  });

  afterAll(() => {
    gatewayPage.deleteResource('deploy', service, namespace_name);
    gatewayPage.deleteResource('service', service, namespace_name);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
    gatewayPage.deleteResource('gateway', name, namespace_name);
  });

  it('ACP2UI-57729 : L1:创建网关-取消', () => {
    gatewayPage.createPage.getButtonByText('创建网关').click();
    gatewayPage.createPage.getButtonByText('取消').click();
    listVerify.verifyNum(0, name);
  });
  it('ACP2UI-57482 : L0:创建网关-选择微服务-选择端口-填写名称、显示名称-填写网关入口-创建成功', () => {
    const testData = {
      微服务: [microservice_name, port],
      名称: name,
      显示名称: name.toUpperCase(),
      网关入口: gateway_entry,
    };

    gatewayPage.createPage.createGateway(testData);

    const expectData = {
      显示名称: name.toUpperCase(),
      网关入口: 'http://' + gateway_entry + ':30666',
      微服务: `${microservice_name} ( 端口 : ${port})`,
    };

    verifyPage.verify(expectData);
  });

  it('ACP2UI-57511 : L1:更新服务网关-更新显示名称-更新成功', () => {
    const testData = {
      显示名称: updateDisplayName,
    };

    gatewayPage.detailPage.update(testData);

    const expectData = {
      显示名称: updateDisplayName,
    };

    verifyPage.verify(expectData);
  });

  it('ACP2UI-57512 : L1:更新服务网关-更新微服务端口-更新成功', () => {
    const testData = {
      微服务: ['90'],
    };

    gatewayPage.detailPage.update(testData);

    const expectData = {
      微服务: `${microservice_name} ( 端口 : 90)`,
    };

    verifyPage.verify(expectData);
  });

  it('ACP2UI-1119 : L1:单击左导航Istio 网关，在列表页，单击名称进入详情页，选择操作->删除，验证删除成功', () => {
    gatewayPage.detailPage.delete();
    listVerify.verifyNum(0, name);
  });
});
