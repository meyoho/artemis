import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';

describe('熔断 L1case 测试', () => {
  const outlierDetection = new MicroServicePage();
  const expectMethod = new MicroDetailPageVerify();

  const microservice_name = 'outlierdetection';
  const namespace_name = outlierDetection.namespace1Name;
  const service = 'od-deploy';
  const version = 'v1';
  beforeAll(() => {
    outlierDetection.login();
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      outlierDetection.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      outlierDetection.clusterName,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': outlierDetection.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.od-policy' + String(new Date().getMilliseconds()),
      outlierDetection.clusterName,
    );
    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.outlierdetecttion' + String(new Date().getMilliseconds()),
      outlierDetection.clusterName,
    );
    outlierDetection.enterUserView(
      outlierDetection.projectName,
      outlierDetection.clusterName,
      outlierDetection.namespace1Name,
    );
    outlierDetection.clickLeftNavByText('服务列表');
    //进入微服务详情页
    outlierDetection.microServiceListPage.serviceListTable.clickResourceNameByRow(
      [microservice_name],
    );
    // 点击拓扑图service节点 和 点击策略tab页
    //outlierDetection.microServiceDetailPage._serviceNode.click();
    outlierDetection.microServiceDetailPage._strategyTab.click();
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      outlierDetection.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      outlierDetection.clusterName,
    );
    outlierDetection.deleteResource('service', service, namespace_name);
  });

  it('创建熔断策略--取消', () => {
    outlierDetection.microServiceDetailPage.getButtonByText('创建策略').click();
    outlierDetection.microServiceDetailPage.getButtonByText('熔断').click();
    outlierDetection.microServiceDetailPage._dialog.cancel();
    expectMethod.verifyStrategyNum(0);
  });

  it('ACP2UI-54761 : L1:创建熔断策略', () => {
    const testData = {
      连续错误响应个数: 4,
      检查周期: [20, 'm'],
      实例最短隔离时间: [30, 'm'],
      实例最大隔离比例: 30,
    };

    outlierDetection.microServiceDetailPage.createOutlierDetection(testData);

    const expectData = {
      策略类型: ['熔断'],
      策略详情: [
        '连续错误响应个数：4\n检查周期：20m\n实例最短隔离时间：30m\n实例最大隔离比例：30%',
      ],
    };

    expectMethod.verifyStrategyInfo(expectData);
    expectMethod.verifyStrategyNum(1);
  });

  it('ACP2UI-54762 : L1:更新熔断策略', () => {
    const testData = {
      连续错误响应个数: 5,
      检查周期: [10, 's'],
      实例最短隔离时间: [20, 's'],
      实例最大隔离比例: 10,
    };

    outlierDetection.microServiceDetailPage.updateStrategy(testData);
    const expectData = {
      策略类型: ['熔断'],
      策略详情: [
        '连续错误响应个数：5\n检查周期：10s\n实例最短隔离时间：20s\n实例最大隔离比例：10%',
      ],
    };
    expectMethod.verifyStrategyInfo(expectData);
  });

  it('ACP2UI-54763 : L1:删除熔断策略', () => {
    outlierDetection.microServiceDetailPage.deleteStrategy();
    expectMethod.verifyStrategyNum(0);
  });
});
