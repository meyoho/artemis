import { GatewayPage } from '@e2e/page_objects/asm/gateway/gateway.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { GatewayDetailVerify } from '@e2e/page_verify/asm/gateway/gateway_detail_verify.page';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('服务网关 网关路由 L0case', () => {
  const gatewayPage = new GatewayPage();
  const verifyPage = new GatewayDetailVerify();
  const yamlVerify = new YamlVerify();
  const project_name = gatewayPage.projectName;
  const region_name = gatewayPage.clusterName;
  const namespace_name = gatewayPage.namespace1Name;

  const deploy_service = 'gateway-route';
  const microservice_name = 'gateway-route-mi';
  const version = 'gateway-route';
  const gateway_name = 'gateway-route';
  const gateway_entry = 'gateway.route';
  const gateway_route_name = 'gateway-route-only';
  beforeAll(() => {
    gatewayPage.login();
    gatewayPage.deleteResource('deploy', deploy_service, namespace_name);
    gatewayPage.deleteResource('svc', deploy_service, namespace_name);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
    gatewayPage.deleteResource('gateway', gateway_name, namespace_name);
    gatewayPage.deleteResource(
      'virtualservice',
      gateway_route_name,
      namespace_name,
    );
    // 创建应用
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': region_name,
        '${namespace_name}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${app_name}': deploy_service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.gateway-route' + String(new Date().getMilliseconds()),
      region_name,
    );

    //创建微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${namespace}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${deployment}': deploy_service,
        '${version}': version,
      },
      'L1.gateway-route' + String(new Date().getMilliseconds()),
      region_name,
    );

    //创建网关
    CommonKubectl.createResource(
      'alauda.asm.gateway.yaml',
      {
        '${name}': gateway_name,
        '${desplay_name}': gateway_name.toUpperCase(),
        '${gateway_entry}': gateway_entry,
        '${microservice}': microservice_name,
        '${namespace}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
      },
      'L1.gateway-route' + String(new Date().getMilliseconds()),
      region_name,
    );

    gatewayPage.waitDeployReady(deploy_service);
    gatewayPage.enterUserView(project_name, region_name, namespace_name);
    gatewayPage.clickLeftNavByText('服务网关');
    gatewayPage.listPage.gatewayTable.clickResourceNameByRow([gateway_name]);
  });
  afterAll(() => {
    gatewayPage.deleteResource('deploy', deploy_service, namespace_name);
    gatewayPage.deleteResource('svc', deploy_service, namespace_name);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
    gatewayPage.deleteResource('gateway', gateway_name, namespace_name);
    gatewayPage.deleteResource(
      'virtualservice',
      gateway_route_name,
      namespace_name,
    );
  });
  it('ACP2UI-57728 : L1:创建网关路由-取消 ', () => {
    gatewayPage.detailPage.getButtonByText('创建路由').click();
    gatewayPage.detailPage.getButtonByText('取消').click();
    expect(
      yamlVerify.isDelete('virtualservice', gateway_route_name, namespace_name),
    ).toBe(true);
  });
  it('ACP2UI-57485 : L0:创建网关路由-填写名称、显示名称-输入权重-创建成功', () => {
    const testData = {
      名称: gateway_route_name,
      显示名称: gateway_route_name,
      权重规则: [['无数据', '无数据', '100']],
    };

    gatewayPage.detailPage.createGatewayRoute(testData);

    const expectData = {
      状态: '已启用',
      权重规则: [[deploy_service], [version], ['100%']],
    };
    verifyPage.verifyGatewayRouteInfo(expectData);
  });

  it('ACP2UI-57505 : L1:停用网关路由-生效', () => {
    gatewayPage.detailPage.stop();
    const expectData = {
      状态: '未启用',
    };
    verifyPage.verifyGatewayRouteInfo(expectData);
  });
  it('ACP2UI-57506 : L1:启用网关路由-生效', () => {
    gatewayPage.detailPage.start();
    const expectData = {
      状态: '已启用',
    };
    verifyPage.verifyGatewayRouteInfo(expectData);
  });
  it('ACP2UI-57727 : L1:更新网关路由-修改权重规则处的权重', () => {
    const testData = {
      权重规则: [['无数据', '无数据', '100']],
    };
    gatewayPage.detailPage.updateGatewayRoute(testData);
    const expectData = {
      权重规则: [[deploy_service], [version], ['100%']],
    };
    verifyPage.verifyGatewayRouteInfo(expectData);
  });
  it('ACP2UI-57507 : L0:删除网关路由', () => {
    gatewayPage.detailPage.stop();
    gatewayPage.detailPage.deleteDatewayRoute().then(() => {
      yamlVerify.isDelete('virtualservice', gateway_route_name, namespace_name);
    });
  });
});
