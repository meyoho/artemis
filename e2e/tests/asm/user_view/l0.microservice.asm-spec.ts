import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroServiceListPageVerify } from '@e2e/page_verify/asm/microservice/microservice_list_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('服务列表 L1 case测试', () => {
  const servicePage = new MicroServicePage();
  const verifyListPage = new MicroServiceListPageVerify();
  const verifyYaml = new YamlVerify();
  const project_name = servicePage.projectName;
  const namespace_name = servicePage.namespace1Name;
  const microservice_name = 'microservice1';
  const deploy_name1 = 'micro-deploy';
  beforeAll(() => {
    servicePage.login();
    servicePage.deleteResource('deploy', deploy_name1, namespace_name);
    servicePage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': servicePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': deploy_name1,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.cp-policy' + String(new Date().getMilliseconds()),
      servicePage.clusterName,
    );
    servicePage.enterUserView(
      project_name,
      servicePage.clusterName,
      namespace_name,
    );
    servicePage.clickLeftNavByText('服务列表');
  });

  afterAll(() => {
    servicePage.deleteResource('deploy', deploy_name1, namespace_name);
    servicePage.deleteResource('service', deploy_name1, namespace_name);
    servicePage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
  });

  it('ACP2UI-53679 : L1:点击左导航服务列表-必选项创建微服务-创建成功', () => {
    const testData = {
      微服务名称: microservice_name,
      显示名称: microservice_name.toUpperCase(),
      服务版本: [[deploy_name1 + `\n1个服务入口: ${deploy_name1}`, 'v1']],
      //
    };
    servicePage.createMicroService.createMicroService(testData);
    servicePage.clickLeftNavByText('服务列表');

    verifyListPage.verifyNum(1, microservice_name);
    const expectData = {
      名称: microservice_name,
      工作负载: `${deploy_name1} (v1)`,
    };
    const expectJson = {
      // kind: 'MicroService',
      metadata: {
        name: microservice_name,
        namespace: namespace_name,
      },
      spec: {
        deployments: [
          {
            name: deploy_name1,
            version: 'v1',
          },
        ],
        services: [
          {
            name: deploy_name1,
          },
        ],
      },
    };
    verifyListPage.verifyParam(expectData, [microservice_name]).then(() => {
      verifyYaml.verifyYaml(
        'microservices',
        microservice_name,
        namespace_name,
        expectJson,
      );
    });
  });

  it('ACP2UI-53702 : L1:删除微服务', () => {
    servicePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);

    servicePage.microServiceDetailPage.delete();

    verifyListPage.verifyNum(0, microservice_name).then(() => {
      expect(
        verifyYaml.isDelete('microservices', microservice_name, namespace_name),
      ).toBe(true);
    });
  });
});
