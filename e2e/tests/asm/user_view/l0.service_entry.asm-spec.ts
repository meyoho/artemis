import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';

describe('服务入口 L1 case测试', () => {
  const micro = new MicroServicePage();
  const verifyServiceEntry = new MicroDetailPageVerify();
  const yamlVerify = new YamlVerify();

  const service = 'service-entry';
  const microservice_name = 'service-entry';
  const namespace_name = micro.namespace1Name;
  const version = 'v1';

  const svc_name = 'e2e-create';
  beforeAll(() => {
    micro.login();
    micro.deleteResource('deploy', service, namespace_name);
    micro.deleteResource('svc', service, namespace_name);
    micro.deleteResource('svc', svc_name, namespace_name);
    micro.deleteResource('microservice', microservice_name, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': micro.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.service-entry' + String(new Date().getMilliseconds()),
      micro.clusterName,
    );

    // 创建一个不带svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.service-entry' + String(new Date().getMilliseconds()),
      micro.clusterName,
    );
    micro.enterUserView(
      micro.projectName,
      micro.clusterName,
      micro.namespace1Name,
    );
    micro.clickLeftNavByText('服务列表');
    micro.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
  });

  afterAll(() => {
    micro.deleteResource('deploy', service, namespace_name);
    micro.deleteResource('svc', svc_name, namespace_name);
    micro.deleteResource('svc', service, namespace_name);
    micro.deleteResource('microservice', microservice_name, namespace_name);
  });

  it('ACP2UI-60731 : L1: 微服务详情页-创建服务入口-取消', () => {
    micro.microServiceDetailPage.createServiceEntryButton.click();
    micro.microServiceDetailPage._dialog.cancel().then(() => {
      expect(yamlVerify.isDelete('svc', svc_name, namespace_name)).toBe(true);
    });
  });

  it('ACP2UI-53952 : L0:微服务详情页-管理服务入口-创建服务入口', () => {
    const testData = {
      名称: svc_name,
      端口: [['HTTP', '80', '80']],
    };
    micro.microServiceDetailPage.createServiceEntry(testData);

    const expectData = {
      服务入口: 'HTTP\n80\n80',
    };
    verifyServiceEntry.verifyServiceEntryInfo(expectData);
  });

  it('ACP2UI-53954 : L1:微服务详情页-管理服务入口，更新服务入口', () => {
    const testData = {
      端口: [['gRPC', '90', '90']],
    };
    micro.microServiceDetailPage.updateServiceEntry(testData);
    const expectData = {
      服务入口: 'gRPC\n90\n90',
    };
    verifyServiceEntry.verifyServiceEntryInfo(expectData);
  });

  it('ACP2UI-53960 : L0:微服务详情页-管理服务入口，删除服务入口', () => {
    micro.microServiceDetailPage.deleteServiceEntry().then(() => {
      expect(yamlVerify.isDelete('svc', svc_name, namespace_name)).toBe(true);
    });
  });
});
