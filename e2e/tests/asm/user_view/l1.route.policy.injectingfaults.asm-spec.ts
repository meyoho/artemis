import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('ASM 路由策略-错误注入 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const name = 'route-fault';
  const service = 'route-fault';
  const microservice_name = 'route-fault';
  const version = 'v1';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.route-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建路由
    CommonKubectl.createResource(
      'alauda.asm.virtualservice.yaml',
      {
        '${service}': service,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${name}': name,
        '${namespace}': namespace_name,
      },
      'L1.virtualservice-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    routePage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击拓扑图service节点 和 点击路由tab页
    //routePage.microServiceDetailPage._serviceNode.click();
    routePage.microServiceDetailPage._route.click();
  });

  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);
  });

  it('L1：service-a 调用service-b， 在service-b 权重规则上配置错误注入（99%），验证配置生效', () => {
    const testData = {
      'HTTP 状态码': 400,
      错误比例: 88.9,
    };
    routePage.policyPage.createPolicy('权重规则', '错误注入', testData);

    const expectData = {
      权重规则: '错误注入\nHTTP 状态码:400\n错误比例:88.9%',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-867 : L1：service-a 调用service-b， 在service-b 权重规则上更新错误注入配置，验证配置生效', () => {
    const testData = {
      'HTTP 状态码': 503,
      错误比例: 66.33,
    };

    routePage.policyPage.updatePolicy('权重规则', testData);
    const expectData = {
      权重规则: '错误注入\nHTTP 状态码:503\n错误比例:66.33%',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1153 : L1：service-a 调用service-b， 在service-b 权重规则上删除错误注入配置，验证配置生效', () => {
    routePage.policyPage.deletePolicy('权重规则');
    routePage.policyPage.policyCardIsPresent('权重规则').then(number => {
      expect(number).toBe(0);
    });
  });

  it('ACP2UI-864 : L1：service-a 调用service-b， 在service-b 上路由规则上配置错误注入（99%），验证配置生效', () => {
    const testData = {
      'HTTP 状态码': 400,
      错误比例: 88.9,
    };
    routePage.policyPage.createPolicy('条件规则', '错误注入', testData);

    const expectData = {
      条件规则: '错误注入\nHTTP 状态码:400\n错误比例:88.9%',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1152 : L1：service-a 调用service-b， 在service-b 上路由规则上配置错误注入（99%）， 更新错误注入，验证配置生效 ', () => {
    const testData = {
      'HTTP 状态码': 503,
      错误比例: 66.33,
    };

    routePage.policyPage.updatePolicy('条件规则', testData);
    const expectData = {
      条件规则: '错误注入\nHTTP 状态码:503\n错误比例:66.33%',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-866 : L1：service-a 调用service-b， 在service-b 上路由规则上配置错误注入（99%）， 删除错误注入，验证配置生效', () => {
    routePage.policyPage.deletePolicy('条件规则');
    routePage.policyPage.policyCardIsPresent('条件规则').then(number => {
      expect(number).toBe(0);
    });
  });
});
