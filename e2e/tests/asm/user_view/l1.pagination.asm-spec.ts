import { AsmPageBase } from '@e2e/page_objects/asm/asm.page.base';
import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';
import { GatewayPage } from '@e2e/page_objects/asm/gateway/gateway.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('路由/网关/安全策略 分页case', () => {
  const asm_page = new AsmPageBase();
  const gateway_page = new GatewayPage();
  const servicePage = new MicroServicePage();
  const project_name = asm_page.paginationProJectName;
  const namespace_name = asm_page.paginationNs;
  const name = 'pagination';
  const gateway_ownname = 'gateway';

  let gateway_entry = 'gateway.pagi';
  let gateway_name = '';

  let microservice_name = '';
  const version = 'v1';

  beforeAll(() => {
    for (let i = 0; i < 21; i++) {
      gateway_entry = gateway_entry + i;
      gateway_name = gateway_ownname + i;

      const deployment_name = name + i;
      microservice_name = name + i;

      // 创建21个应用
      CommonKubectl.createResource(
        'alauda.asm.service.yaml',
        {
          '${cluster_name}': servicePage.clusterName,
          '${label}': ServerConf.LABELBASEDOMAIN,
          '${namespace_name}': namespace_name,
          '${app_name}': microservice_name,
          '${IMAGE}': ServerConf.TESTIMAGE,
        },
        'L1.deploy-pagination' + String(new Date().getMilliseconds()),
        servicePage.clusterName,
      );

      //创建21个微服务
      CommonKubectl.createResource(
        'alauda.asm.microservice.yaml',
        {
          '${name}': microservice_name,
          '${label}': ServerConf.LABELBASEDOMAIN,
          '${namespace}': namespace_name,
          '${deployment}': deployment_name,
          '${version}': version,
        },
        'L1.microservice-pagination' + String(new Date().getMilliseconds()),
        servicePage.clusterName,
      );

      //创建21个网关
      CommonKubectl.createResource(
        'alauda.asm.gateway.yaml',
        {
          '${name}': gateway_name,
          '${label}': ServerConf.LABELBASEDOMAIN,
          '${desplay_name}': gateway_name.toUpperCase(),
          '${gateway_entry}': gateway_entry,
          '${microservice}': microservice_name,
          '${namespace}': namespace_name,
        },
        'L1.gateway-pagination' + String(new Date().getMilliseconds()),
        servicePage.clusterName,
      );
    }

    asm_page.login();
    asm_page.enterUserView(
      project_name,
      servicePage.clusterName,
      namespace_name,
    );
  });

  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice --all -n ${namespace_name}`,
      asm_page.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete gateway --all -n ${namespace_name}`,
      asm_page.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete service --all -n ${namespace_name}`,
      asm_page.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy --all -n ${namespace_name}`,
      asm_page.clusterName,
    );
  });

  it('ACP2UI-57716 : L1:网关列表页搜索及加载更多', () => {
    asm_page.clickLeftNavByText('服务网关');
    browser.sleep(500);
    gateway_name = gateway_ownname + '0';

    gateway_page.listPage.gatewayTable.getRowCount().then(number => {
      expect(number).toBe(20);
    });
    // 加载更多
    gateway_page.listPage.loadMoreButton.click();
    gateway_page.listPage.gatewayTable.getRowCount().then(number => {
      expect(number).toBe(21);
    });

    gateway_page.listPage.gatewayTable.searchByResourceName(gateway_name);
    gateway_page.listPage.gatewayTable.getRowCount().then(number => {
      expect(number).toBe(1);
    });
  });

  it('ACP2UI-53684 : L1:服务列表搜索及加载更多', () => {
    asm_page.clickLeftNavByText('服务列表');
    browser.sleep(500);

    microservice_name = name + '0';
    // 默认显示20条
    servicePage.microServiceListPage.serviceListTable
      .getRowCount()
      .then(number => {
        expect(number).toBe(20);
      });

    // 加载更多
    servicePage.microServiceListPage.loadMoreButton.click();
    servicePage.microServiceListPage.serviceListTable
      .getRowCount()
      .then(number => {
        expect(number).toBe(21);
      });

    // 搜索一条数据
    servicePage.microServiceListPage.serviceListTable.searchByResourceName(
      microservice_name,
    );
    servicePage.microServiceListPage.serviceListTable
      .getRowCount()
      .then(number => {
        expect(number).toBe(1);
      });
  });
});
