import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('访问控制-白名单 L1case测试', () => {
  const whitelist = new MicroServicePage();
  const expectPage = new MicroDetailPageVerify();
  const expectYaml = new YamlVerify();
  const microservice_name = 'whitelist1';
  const microservice_name2 = 'whitelist2';
  const version = 'v1';
  const namespace_name = whitelist.namespace1Name;
  const service = 'white1';
  const service2 = 'white2';
  beforeAll(() => {
    whitelist.login();
    whitelist.deleteResource('whitelist', service, namespace_name);
    whitelist.deleteResource('whitelist', service2, namespace_name);
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service2} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name2} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': whitelist.clusterName,
        '${namespace_name}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.whitelist1-policy' + String(new Date().getMilliseconds()),
      whitelist.clusterName,
    );
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': whitelist.clusterName,
        '${namespace_name}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${app_name}': service2,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.whitelist2-policy' + String(new Date().getMilliseconds()),
      whitelist.clusterName,
    );
    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${namespace}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.whitelist1' + String(new Date().getMilliseconds()),
      whitelist.clusterName,
    );
    // 创建一个有服务入口的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name2,
        '${namespace}': namespace_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${deployment}': service2,
        '${version}': version,
      },
      'L1.whitelist2' + String(new Date().getMilliseconds()),
      whitelist.clusterName,
    );
    whitelist.enterUserView(
      whitelist.projectName,
      whitelist.clusterName,
      whitelist.namespace1Name,
    );
    whitelist.clickLeftNavByText('服务列表');
    //进入微服务详情页
    whitelist.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击策略tab页
    whitelist.microServiceDetailPage._whitelistTab.click();
  });
  afterAll(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service2} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete deploy ${service} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    CommonKubectl.execKubectlCommand(
      `kubectl delete microservice ${microservice_name2} -n ${namespace_name}`,
      whitelist.clusterName,
    );
    whitelist.deleteResource('whitelist', service, namespace_name);
    whitelist.deleteResource('whitelist', service2, namespace_name);
    whitelist.deleteResource('service', service, namespace_name);
    whitelist.deleteResource('service', service2, namespace_name);
  });
  it('添加白名单--取消', () => {
    browser.sleep(100);
    whitelist.getButtonByText('添加白名单').click();
    whitelist.getButtonByText('取消').click();
    expectPage.verifyWhiteListNum(0);
  });

  it('ACP2UI-55247 : L1:添加白名单-一个白名单', () => {
    const testData = {
      白名单: [[microservice_name, version]],
    };
    whitelist.microServiceDetailPage.createWhiteList(testData);
    expectPage.verifyWhiteListNum(1);
    const expectData = {
      微服务: [microservice_name],
      版本: [version],
    };
    const expectJson = {
      spec: {
        allowlist: [
          {
            app: microservice_name,
            version: version,
          },
        ],
        destmatch: {
          namespace: namespace_name,
          service: service,
        },
      },
    };
    expectPage.verifyWhiteListInfo(expectData).then(() => {
      expectYaml.verifyYaml('whitelist', service, namespace_name, expectJson);
    });
  });

  it('ACP2UI-55249 : L1:多次添加白名单', () => {
    const testData = {
      白名单: [[microservice_name2, version]],
    };
    whitelist.microServiceDetailPage.createWhiteList(testData).then(() => {
      expectPage.verifyWhiteListNum(2);
    });

    const expectData = {
      微服务: [microservice_name, microservice_name2],
      版本: [version, version],
    };

    const expectJson = {
      spec: {
        allowlist: [
          {
            app: microservice_name,
            version: version,
          },
          {
            app: microservice_name2,
            version: version,
          },
        ],
        destmatch: {
          namespace: namespace_name,
          service: service,
        },
      },
    };
    expectPage.verifyWhiteListInfo(expectData).then(() => {
      expectYaml.verifyYaml('whitelist', service, namespace_name, expectJson);
    });
  });

  it('ACP2UI-55251 : L1:多个服务入口，添加了同一个微服务及版本的白名单', () => {
    whitelist.clickLeftNavByText('服务列表');
    //进入微服务详情页
    whitelist.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name2,
    ]);
    // 点击拓扑图service节点 和 点击策略tab页
    whitelist.microServiceDetailPage._serviceNode.click();
    whitelist.microServiceDetailPage._whitelistTab.click();
    const testData = {
      白名单: [[microservice_name, version]],
    };
    whitelist.microServiceDetailPage.createWhiteList(testData);
    expectPage.verifyWhiteListNum(1);
    const expectData = {
      微服务: [microservice_name],
      版本: [version],
    };

    const expectJson = {
      spec: {
        allowlist: [
          {
            app: microservice_name,
            version: version,
          },
        ],
        destmatch: {
          namespace: namespace_name,
          service: service2,
        },
      },
    };
    expectPage.verifyWhiteListInfo(expectData).then(() => {
      expectYaml.verifyYaml('whitelist', service2, namespace_name, expectJson);
    });
  });
  it('ACP2UI-55257 : L1:多个白名单--删除一个白名单', () => {
    whitelist.clickLeftNavByText('服务列表');
    //进入微服务详情页
    whitelist.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击策略tab页
    whitelist.microServiceDetailPage._whitelistTab.click();

    whitelist.microServiceDetailPage.deleteWhiteList(microservice_name);
    const expectJson = {
      spec: {
        allowlist: [
          {
            app: microservice_name2,
            version: version,
          },
        ],
        destmatch: {
          namespace: namespace_name,
          service: service,
        },
      },
    };
    expectPage.verifyWhiteListNum(1).then(() => {
      expectYaml.verifyYaml('whitelist', service, namespace_name, expectJson);
    });
  });

  it('ACP2UI-55260 : L1:删除全部白名单', () => {
    whitelist.microServiceDetailPage.deleteWhiteList(microservice_name2);
    browser.refresh();
    whitelist.microServiceDetailPage._whitelistTab.click();
    expectPage.verifyWhiteListNum(0).then(() => {
      expect(expectYaml.isDelete('whitelist', service, namespace_name)).toBe(
        true,
      );
    });
  });

  it('ACP2UI-55248 : L1:添加白名单-多个白名单', () => {
    const testData = {
      白名单: [[microservice_name, version], [microservice_name2, version]],
    };
    whitelist.microServiceDetailPage.createWhiteList(testData);
    expectPage.verifyWhiteListNum(2);
    const expectData = {
      微服务: [microservice_name, microservice_name2],
      版本: [version, version],
    };
    const expectJson = {
      spec: {
        allowlist: [
          {
            app: microservice_name,
            version: version,
          },
          {
            app: microservice_name2,
            version: version,
          },
        ],
        destmatch: {
          namespace: namespace_name,
          service: service,
        },
      },
    };
    expectPage.verifyWhiteListInfo(expectData).then(() => {
      expectYaml.verifyYaml('whitelist', service, namespace_name, expectJson);
    });
  });
});
