import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';

describe('ASM 路由管理 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();
  const verifyPage = new MicroDetailPageVerify();
  const verifyYaml = new YamlVerify();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const microservice_name = 'route-only';
  const version = 'V-2-3';
  const name = 'route-only';
  const service = 'route-only';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-only' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.route-only' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    routePage.waitDeployReady(service);

    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    routePage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击拓扑图service节点 和 点击路由tab页
    //routePage.microServiceDetailPage._serviceNode.click();
    routePage.microServiceDetailPage._route.click();
  });

  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);
  });

  it('ACP2UI-56028 : L0:创建路由-添加名称、显示名称，权重规则处的工作负载及版本是当前微服务下所有工作负载及版本，可以修改权重，创建成功且生效', () => {
    const testData = {
      名称: name,
      显示名称: name,
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRoute(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
    };
    // 验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-57678 : L1:路由-停用', () => {
    routePage.detailPage.stop();
    const expectData = {
      状态: '未启用',
    };
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-57679 : L1:路由-启用', () => {
    routePage.detailPage.start();
    const expectData = {
      状态: '已启用',
    };
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-56030 : L1:更新路由-可以更新显示名称、权重规则的权重、条件规则', () => {
    const update_name = name + 'update';
    const testData = {
      显示名称: update_name,
      权重规则: [['无数据', '无数据', '100']],
    };

    routePage.detailPage.update(testData);

    const expectData = {
      显示名称: update_name,
      状态: '已启用',
      路由入口: service,
      权重规则: [[service], [version], ['100%']],
    };
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-56031 : L0:删除路由', () => {
    routePage.detailPage.stop();
    routePage.detailPage.delete().then(() => {
      expect(verifyYaml.isDelete('virtualservice', name, namespace_name)).toBe(
        true,
      );
    });
  });
});
