import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';

describe('ASM 路由管理条件规则 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();
  const verifyPage = new MicroDetailPageVerify();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const name = 'route-rule';
  const service = 'route-rule';
  const microservice_name = 'route-rule';
  const version = 'V-rule';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-only' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.route-only' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );
    routePage.waitDeployReady(service);
    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
  });

  beforeEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete virtualservice ${name} -n ${namespace_name}`,
      routePage.clusterName,
    );
    routePage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击拓扑图service节点 和 点击路由tab页
    //routePage.microServiceDetailPage._serviceNode.click();
    routePage.microServiceDetailPage._route.click();
  });
  afterEach(() => {
    CommonKubectl.execKubectlCommand(
      `kubectl delete virtualservice ${name} -n ${namespace_name}`,
      routePage.clusterName,
    );
  });
  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);
  });

  it('ACP2UI-1111 : L1: 单击路由tab， 为服务版本设定uri条件规则，验证条件规则创建成功', () => {
    const testData = {
      名称: name,
      显示名称: name,
      条件规则: [
        {
          工作负载: service,
          规则: [['uri', 'exact', '-', '/test']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    //创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['uri'], ['exact'], ['-'], ['/test']],
    };
    //验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-4148 : L1:单击路由tab，为服务版本设定scheme条件规则，验证条件规则生效', () => {
    // 测试数据
    const testData = {
      名称: name,
      显示名称: name,
      权重规则: [['无数据', '无数据', '100']],
      条件规则: [
        {
          工作负载: service,
          规则: [['scheme', 'regex', '-', '^https.*']],
        },
      ],
    };

    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['scheme'], ['regex'], ['-'], ['^https.*']],
    };
    // 验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-1110 : L1:单击路由tab，为服务版本设定method条件规则，验证条件规则创建成功', () => {
    const testData = {
      名称: name,
      显示名称: name,
      条件规则: [
        {
          工作负载: service,
          规则: [['method', 'exact', '-', 'GET']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['method'], ['exact'], ['-'], ['GET']],
    };
    // 验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });

  it('ACP2UI-4149 : L1:单击路由tab，为服务版本设定headers条件规则，验证条件规则创建成功', () => {
    const testData = {
      名称: name,
      显示名称: name,
      条件规则: [
        {
          工作负载: service,
          规则: [['headers', 'prefix', 'key', 'value']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['headers'], ['prefix'], ['key'], ['value']],
    };
    // 验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });

  //   it('ACP2UI-4147 : L1:单击路由tab，为服务版本设定port条件规则,验证条件规则创建成功', () => {
  //     const testData = {
  //       名称: name,
  //       显示名称: name,
  //       条件规则: [
  //         {
  //           工作负载: service,
  //           规则: [['port', '-', '-', '80']],
  //         },
  //       ],
  //       权重规则: [['无数据', '无数据', '100']],
  //     };
  //     routePage.createPage.createRouteRule(testData);

  //     // 创建成功后，跳到详情页
  //     const expectData = {
  //       显示名称: name,
  //       状态: '已启用',
  //       权重规则: [[service], [version], ['100%']],
  //       条件规则: [['port'], ['-'], ['-'], ['80']],
  //     };
  //     // 验证详情页显示正确
  //     verifyPage.verifyRouteInfo(expectData);
  //   });

  it('ACP2UI-4150 : L1: 单击路由tab，为服务版本设定sourceLabels条件规则,验证条件规则创建成功', () => {
    // 测试数据
    const testData = {
      名称: name,
      显示名称: name,
      权重规则: [['无数据', '无数据', '100']],
      条件规则: [
        {
          工作负载: service,
          规则: [['sourceLabels', '-', 'app', 'reviews']],
        },
      ],
    };

    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: name,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['sourceLabels'], ['-'], ['app'], ['reviews']],
    };
    // 验证详情页显示正确
    verifyPage.verifyRouteInfo(expectData);
  });
});
