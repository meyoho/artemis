import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('ASM 路由策略-请求重写 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const name = 'route-rewrite';
  const service1 = 'route-rewrite1';
  const service2 = 'route-rewrite2';

  const microservice_name = 'route-rewrite';
  const version = 'v1';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service1, namespace_name);
    routePage.deleteResource('deploy', service1, namespace_name);
    routePage.deleteResource('svc', service2, namespace_name);
    routePage.deleteResource('deploy', service2, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service1,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-rewrite1' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service2,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-rewrite2' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service1,
        '${version}': version,
      },
      'L1.route-rewrite' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建路由
    CommonKubectl.createResource(
      'alauda.asm.virtualservice.yaml',
      {
        '${service}': service1,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${name}': name,
        '${namespace}': namespace_name,
      },
      'L1.virtualservice-rewrite' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    routePage.waitDeployReady(service1);
    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    routePage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击路由tab页
    routePage.microServiceDetailPage._route.click();
  });

  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service1, namespace_name);
    routePage.deleteResource('deploy', service1, namespace_name);
    routePage.deleteResource('svc', service2, namespace_name);
    routePage.deleteResource('deploy', service2, namespace_name);
  });

  it('ACP2UI-1101 : L1:service-a调用service-b，在service-b 路由权重上配置请求重写，验证配置生效', () => {
    const testData = {
      '重写 host': service2,
      '重写 uri': '/de',
    };
    routePage.policyPage.createPolicy('权重规则', '请求重写', testData);

    const expectData = {
      权重规则: `请求重写\n重写 host:${service2}\n重写 uri:/de`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1121 : L1:service-a调用service-b，更新service-b 路由权重上配置请求重写，验证配置生效', () => {
    const testData = {
      '重写 uri': '/update',
    };

    routePage.policyPage.updatePolicy('权重规则', testData);
    const expectData = {
      权重规则: `请求重写\n重写 host:${service2}\n重写 uri:/update`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1123 : L1:service-a调用service-b，在service-b 路由权重上配置请求重写，删除请求重写策略，验证配置生效', () => {
    routePage.policyPage.deletePolicy('权重规则');
    routePage.policyPage.policyCardIsPresent('权重规则').then(number => {
      expect(number).toBe(0);
    });
  });

  it('ACP2UI-1099 : L1:service-a调用service-b，在service-b 路由规则上配置请求重写，验证配置生效', () => {
    const testData = {
      '重写 host': service2,
      '重写 uri': '/de',
    };
    routePage.policyPage.createPolicy('条件规则', '请求重写', testData);

    const expectData = {
      条件规则: `请求重写\n重写 host:${service2}\n重写 uri:/de`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1122 : L1:service-a调用service-b，更新service-b 路由规则上配置请求重写，验证配置生效', () => {
    const testData = {
      '重写 uri': '/update',
    };

    routePage.policyPage.updatePolicy('条件规则', testData);
    const expectData = {
      条件规则: `请求重写\n重写 host:${service2}\n重写 uri:/update`,
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1124 : L1:service-a调用service-b，在service-b 路由规则上配置请求重写，删除请求重写策略，验证配置生效', () => {
    routePage.policyPage.deletePolicy('条件规则');
    routePage.policyPage.policyCardIsPresent('条件规则').then(number => {
      expect(number).toBe(0);
    });
  });
});
