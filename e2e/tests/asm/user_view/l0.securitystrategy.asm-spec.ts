import { CommonKubectl } from '@e2e/utility/common.kubectl';

import { ServerConf } from '../../../config/serverConf';
// import { YamlVerify } from '@e2e/page_verify/asm/yaml_verify.page';
import { MicroDetailPageVerify } from '@e2e/page_verify/asm/microservice/microservice_detail_verify.page';
import { MicroServicePage } from '@e2e/page_objects/asm/servicelist/microservice.page';

describe('ASM 安全策略 L1 级别case', () => {
  const securityPage = new MicroServicePage();
  const expectMethod = new MicroDetailPageVerify();
  // const expectYaml = new YamlVerify();

  const project_name = securityPage.projectName;
  const namespace_name = securityPage.namespace1Name;
  const service = 'security';
  const microservice_name = 'security-test';
  const version = 'V-security';

  beforeAll(() => {
    securityPage.login();
    securityPage.deleteResource('policy', service, namespace_name);
    securityPage.deleteResource('deploy', service, namespace_name);
    securityPage.deleteResource('svc', service, namespace_name);
    securityPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': securityPage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.security' + String(new Date().getMilliseconds()),
      securityPage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.security' + String(new Date().getMilliseconds()),
      securityPage.clusterName,
    );
    securityPage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    securityPage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    securityPage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击拓扑图service节点 和 点击路由tab页
    //securityPage.microServiceDetailPage._serviceNode.click();
    securityPage.microServiceDetailPage._strategyTab.click();
  });

  afterAll(() => {
    securityPage.deleteResource('policy', service, namespace_name);
    securityPage.deleteResource('deploy', service, namespace_name);
    securityPage.deleteResource('svc', service, namespace_name);
    securityPage.deleteResource(
      'microservice',
      microservice_name,
      namespace_name,
    );
  });

  it('L1: 创建安全策略-取消', () => {
    securityPage.microServiceDetailPage.getButtonByText('创建策略').click();
    securityPage.microServiceDetailPage.getButtonByText('安全').click();
    securityPage.microServiceDetailPage._dialog.getButtonByText('取消').click();
    expectMethod.verifyStrategyNum(0);
  });
  it('ACP2UI-1106 : L0:创建安全规则（严格模式）， 验证创建成功', () => {
    const testData = {
      规则: '严格模式（tls）',
    };

    securityPage.microServiceDetailPage.createSecurityStrategy(testData);
    const expectData = {
      策略类型: ['安全'],
      策略详情: ['严格模式（tls）'],
    };
    expectMethod.verifyStrategyNum(1);
    expectMethod.verifyStrategyInfo(expectData);
  });

  it('ACP2UI-1109 : L0:选择一个安全规则， 验证更新成功', () => {
    const testData = {
      规则: '兼容模式（tls 和 text）',
    };

    securityPage.microServiceDetailPage.updateStrategy(testData);
    const expectData = {
      策略类型: ['安全'],
      策略详情: ['兼容模式（tls 和 text）'],
    };
    expectMethod.verifyStrategyInfo(expectData);
  });

  it('ACP2UI-1107 : L1:选择一个安全规则， 验证删除成功', () => {
    securityPage.microServiceDetailPage.deleteStrategy();
    expectMethod.verifyStrategyNum(0);
  });

  it('ACP2UI-1108 : L1:创建安全规则（兼容模式）， 验证创建成功', () => {
    const testData = {
      规则: '兼容模式（tls 和 text）',
    };
    securityPage.microServiceDetailPage.createSecurityStrategy(testData);
    expectMethod.verifyStrategyNum(1);
    const expectData = {
      策略类型: ['安全'],
      策略详情: ['兼容模式（tls 和 text）'],
    };
    expectMethod.verifyStrategyInfo(expectData);
    securityPage.microServiceDetailPage.deleteStrategy();
  });
});
