import { ServerConf } from '../../../config/serverConf';
import { RoutePage } from '../../../page_objects/asm/route/route.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('ASM 路由策略-超时和重试 L1 级别的case', () => {
  const routePage: RoutePage = new RoutePage();

  const project_name = routePage.projectName;
  const namespace_name = routePage.namespace1Name;
  const name = 'route-timeout';
  const service = 'route-timeout';
  const microservice_name = 'route-timeout';
  const version = 'v1';

  beforeAll(() => {
    routePage.login();
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);

    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': routePage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespace_name,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.route-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespace_name,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.route-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    // 创建路由
    CommonKubectl.createResource(
      'alauda.asm.virtualservice.yaml',
      {
        '${service}': service,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${name}': name,
        '${namespace}': namespace_name,
      },
      'L1.virtualservice-delay' + String(new Date().getMilliseconds()),
      routePage.clusterName,
    );

    routePage.enterUserView(
      project_name,
      ServerConf.REGIONNAME,
      namespace_name,
    );
    routePage.clickLeftNavByText('服务列表');
    //进入微服务详情页
    routePage.microServiceListPage.serviceListTable.clickResourceNameByRow([
      microservice_name,
    ]);
    // 点击路由tab页
    routePage.microServiceDetailPage._route.click();
  });

  afterAll(() => {
    routePage.deleteResource('virtualservice', name, namespace_name);
    routePage.deleteResource('microservice', microservice_name, namespace_name);
    routePage.deleteResource('svc', service, namespace_name);
    routePage.deleteResource('deploy', service, namespace_name);
  });

  it('ACP2UI-875 : L1：service-a 调用service-b， 在service-b 上路由权重上配置全局超时时间和重试，验证配置生效', () => {
    const testData = {
      全局超时时间: 4000,
      重试次数: 2,
      重试超时时间: 1000,
    };
    routePage.policyPage.createPolicy('权重规则', '超时和重试', testData);

    const expectData = {
      权重规则:
        '超时和重试\n全局超时时间:4000ms\n重试次数:2\n重试超时时间:1000ms',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-877 : L1：service-a 调用service-b， 在service-b 上路由权重上配置全局超时时间，更新，验证配置生效', () => {
    const testData = {
      全局超时时间: 5000,
      重试次数: 1,
      重试超时时间: 1002,
    };

    routePage.policyPage.updatePolicy('权重规则', testData);
    const expectData = {
      权重规则:
        '超时和重试\n全局超时时间:5000ms\n重试次数:1\n重试超时时间:1002ms',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1125 : L1：service-a 调用service-b， 在service-b 上路由权重上配置全局超时时间和重试， 删除超时和重试策略，验证删除后配置生效', () => {
    routePage.policyPage.deletePolicy('权重规则');
    routePage.policyPage.policyCardIsPresent('权重规则').then(number => {
      expect(number).toBe(0);
    });
  });

  it('ACP2UI-874 : L1：service-a 调用service-b， 在service-b 上路由规则上配置全局超时时间和重试，验证配置生效', () => {
    const testData = {
      全局超时时间: 4000,
      重试次数: 2,
      重试超时时间: 1000,
    };
    routePage.policyPage.createPolicy('条件规则', '超时和重试', testData);

    const expectData = {
      条件规则:
        '超时和重试\n全局超时时间:4000ms\n重试次数:2\n重试超时时间:1000ms',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-1126 : L1：service-a 调用service-b， 在service-b 上路由规则上配置全局超时时间，更新，验证配置生效', () => {
    const testData = {
      全局超时时间: 5000,
      重试次数: 1,
      重试超时时间: 1002,
    };

    routePage.policyPage.updatePolicy('条件规则', testData);
    const expectData = {
      条件规则:
        '超时和重试\n全局超时时间:5000ms\n重试次数:1\n重试超时时间:1002ms',
    };
    routePage.policyPage.verifyPolicy(expectData);
  });

  it('ACP2UI-876 : L1：service-a 调用service-b， 在service-b 上路由规则上配置全局超时时间和重试， 删除超时和重试策略，验证删除后配置生效', () => {
    routePage.policyPage.deletePolicy('条件规则');
    routePage.policyPage.policyCardIsPresent('条件规则').then(number => {
      expect(number).toBe(0);
    });
  });
});
