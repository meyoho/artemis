import { ServerConf } from '../../../config/serverConf';
import { GatewayPage } from '../../../page_objects/asm/gateway/gateway.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { RoutePage } from '@e2e/page_objects/asm/route/route.page';
import { GatewayDetailVerify } from '@e2e/page_verify/asm/gateway/gateway_detail_verify.page';

describe('Istio网关条件规则 L1级别case', () => {
  const gatewayPage = new GatewayPage();
  const verifyPage = new GatewayDetailVerify();
  const routePage = new RoutePage();
  const projectName = gatewayPage.projectName;
  const namespaceName = gatewayPage.namespace1Name;
  const gatewayName = 'gateway-rule';
  const gatewayEntry = 'gateway.entry';
  const service = 'gateway-rule';

  const microservice_name = 'gateway-rule';
  const version = 'v1';

  beforeAll(() => {
    gatewayPage.login();
    gatewayPage.deleteResource('deploy', service, namespaceName);
    gatewayPage.deleteResource('svc', service, namespaceName);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespaceName,
    );
    gatewayPage.deleteResource('gateway', gatewayName, namespaceName);
    // 创建一个带有svc的deployment
    CommonKubectl.createResource(
      'alauda.asm.service.yaml',
      {
        '${cluster_name}': gatewayPage.clusterName,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace_name}': namespaceName,
        '${app_name}': service,
        '${IMAGE}': ServerConf.TESTIMAGE,
      },
      'L1.gateway-rule' + String(new Date().getMilliseconds()),
      gatewayPage.clusterName,
    );

    // 创建一个带有svc的微服务
    CommonKubectl.createResource(
      'alauda.asm.microservice_svc.yaml',
      {
        '${name}': microservice_name,
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${namespace}': namespaceName,
        '${deployment}': service,
        '${version}': version,
      },
      'L1.gateway-rule' + String(new Date().getMilliseconds()),
      gatewayPage.clusterName,
    );

    //创建网关
    CommonKubectl.createResource(
      'alauda.asm.gateway.yaml',
      {
        '${name}': gatewayName,
        '${desplay_name}': gatewayName.toUpperCase(),
        '${gateway_entry}': gatewayEntry,
        '${microservice}': microservice_name,
        '${namespace}': namespaceName,
        '${label}': ServerConf.LABELBASEDOMAIN,
      },
      'L1.gateway-route' + String(new Date().getMilliseconds()),
      gatewayPage.clusterName,
    );

    gatewayPage.waitDeployReady(service);

    gatewayPage.enterUserView(
      projectName,
      ServerConf.REGIONNAME,
      namespaceName,
    );
  });
  beforeEach(() => {
    gatewayPage.deleteResource('virtualservice', gatewayName, namespaceName);

    gatewayPage.clickLeftNavByText('服务网关');
    gatewayPage.listPage.gatewayTable.clickResourceNameByRow([gatewayName]);
  });
  afterEach(() => {
    gatewayPage.deleteResource('virtualservice', gatewayName, namespaceName);
  });

  afterAll(() => {
    gatewayPage.deleteResource('deploy', service, namespaceName);
    gatewayPage.deleteResource('svc', service, namespaceName);
    gatewayPage.deleteResource(
      'microservice',
      microservice_name,
      namespaceName,
    );
    gatewayPage.deleteResource('gateway', gatewayName, namespaceName);
  });

  it('ACP2UI-4201 : L1: 单击Istio网关， 条件规则设定uri验证路由创建成功', () => {
    const testData = {
      名称: gatewayName,
      显示名称: gatewayName,
      条件规则: [
        {
          工作负载: service,
          规则: [['uri', 'exact', '-', '/test']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: gatewayName,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['uri'], ['exact'], ['-'], ['/test']],
    };
    // 验证详情页显示正确
    verifyPage.verifyGatewayRouteInfo(expectData);
  });

  it('ACP2UI-4202 : L1:单击Istio网关，条件规则设定scheme验证路由创建成功', () => {
    // 测试数据
    const testData = {
      名称: gatewayName,
      显示名称: gatewayName,
      权重规则: [['无数据', '无数据', '100']],
      条件规则: [
        {
          工作负载: service,
          规则: [['scheme', 'regex', '-', '^https.*']],
        },
      ],
    };

    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: gatewayName,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['scheme'], ['regex'], ['-'], ['^https.*']],
    };
    // 验证详情页显示正确
    verifyPage.verifyGatewayRouteInfo(expectData);
  });

  it('ACP2UI-4203 : L1:单击Istio网关，条件规则设定method验证路由创建成功', () => {
    const testData = {
      名称: gatewayName,
      显示名称: gatewayName,
      条件规则: [
        {
          工作负载: service,
          规则: [['method', 'exact', '-', 'GET']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: gatewayName,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['method'], ['exact'], ['-'], ['GET']],
    };
    // 验证详情页显示正确
    verifyPage.verifyGatewayRouteInfo(expectData);
  });

  it('ACP2UI-4204 : L1:单击Istio网关，条件规则设定headers验证路由创建成功', () => {
    const testData = {
      名称: gatewayName,
      显示名称: gatewayName,
      条件规则: [
        {
          工作负载: service,
          规则: [['headers', 'prefix', 'key', 'value']],
        },
      ],
      权重规则: [['无数据', '无数据', '100']],
    };
    routePage.createPage.createRouteRule(testData);

    // 创建成功后，跳到详情页
    const expectData = {
      显示名称: gatewayName,
      状态: '已启用',
      权重规则: [[service], [version], ['100%']],
      条件规则: [['headers'], ['prefix'], ['key'], ['value']],
    };
    // 验证详情页显示正确
    verifyPage.verifyGatewayRouteInfo(expectData);
  });

  // it('ACP2UI-4205 : L1:单击Istio网关，条件规则设定port,验证路由创建成功', () => {
  //     const testData = {
  // 		名称: gatewayName,
  // 		显示名称: gatewayName,
  // 		网关入口: service,
  // 		条件规则: [
  // 			{
  // 				规则: [['port', '-', '-', '80']],
  // 				微服务: [[service, 'all', '80', '100']],
  // 			},
  // 		],
  // 		权重规则: [[service, 'all', '80', '100']],
  // 	};
  // 	gatewayPage.createPage.createGateway(testData);

  // 	// 创建成功后，跳到详情页
  // 	const expectData = {
  // 		显示名称: gatewayName,
  // 		状态: '已启用',
  // 		网关入口: service,
  // 		权重规则: `${service}\nall\n80\n100%`,
  // 		'条件规则(优先级1)': [`port\n-\n-\n80`, `${service}\nall\n80\n100%`],
  // 	};
  // 	// 验证详情页显示正确
  // 	gatewayPage.detailPage.verify(expectData);
  // });
});
