import { AsmProjectPage } from '@e2e/page_objects/asm/admin_view_project/asm_project.page';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { ServerConf } from '@e2e/config/serverConf';
import { AsmProjectListVerifyPage } from '@e2e/page_verify/asm/admin_view_project/project_list_verify.page';
import { AsmProjectDetailVerifyPage } from '@e2e/page_verify/asm/admin_view_project/project_detail_verify.page';

describe('管理视图 项目L1case', () => {
  const projectpage = new AsmProjectPage();
  const verifyListPage = new AsmProjectListVerifyPage();
  const verifyDetailPage = new AsmProjectDetailVerifyPage();
  const projectName = 'ui-asm-test';
  const displayName = 'asm-ui-test-displayName';
  const description = 'asm-ui-test-description';
  const clusterName = projectpage.globalClusterName;
  beforeAll(() => {
    //创建一个项目
    if (
      projectpage.isNotExist('global', `kubectl get project ${projectName}`)
    ) {
      // 如果项目不存在，创建项目, 创建项目是在global 集群上创建的
      CommonKubectl.createResource(
        'alauda.asm.project.yaml',
        {
          '${project_description}': description,
          '${project_displayname}': displayName,
          '${project_name}': projectName,
          '${cluster_name}': clusterName,
          '${label}': ServerConf.LABELBASEDOMAIN,
        },
        'L1.asm-project' + String(new Date().getMilliseconds()),
        projectpage.globalClusterName,
      );
    }
    projectpage.login();
    projectpage.enterAdminView();
    projectpage.clickLeftNavByText('项目');
  });

  afterAll(() => {
    console.log(
      CommonKubectl.execKubectlCommand(
        `kubectl delete project ${projectName}`,
        this.globalClusterName,
      ),
    );
  });
  it('ACP2UI-3942 : L1:单击左导航项目，显示项目列表，列表展示', () => {
    projectpage.listPage.projectTable.searchByResourceName(projectName);

    const expectData = {
      名称: projectName,
      关联集群: clusterName,
    };
    verifyListPage.verifyListInfo(expectData, [projectName]);
  });

  it('ACP2UI-3946 : L1:单击左导航项目，点击项目名称进入项目详情页，详情页展示', () => {
    // 点击项目名称进入项目详情页
    projectpage.listPage.projectTable.clickResourceNameByRow([projectName]);
    const expectData = {
      名称: projectName,
      显示名称: displayName,
      关联集群: clusterName,
      描述: description,
    };
    const namespaceExpectData = {
      名称: projectName,
      关联集群: clusterName,
    };
    verifyDetailPage.verifyBasicDetailInfo(expectData);
    verifyDetailPage.verifyNamespaceInfo(namespaceExpectData);
  });

  it('ACP2UI-3947 : L1:单击左导航项目，点击项目名称进入项目详情页，命名空间搜索', () => {
    projectpage.detailPage.namespaceTable.searchByResourceName(projectName);
    projectpage.detailPage.namespaceTable.getRowCount().then(number => {
      expect(number).toBe(1);
    });
  });
});
