import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { AsmProjectPage } from '@e2e/page_objects/asm/admin_view_project/asm_project.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('管理视图 项目列表搜索及加载更多', () => {
  const projectpage = new AsmProjectPage();
  let projectName = 'asm-ui-';

  beforeAll(() => {
    // 创建21个项目
    for (let i = 0; i < 21; i++) {
      projectName = projectName + i;

      if (
        projectpage.isNotExist('global', `kubectl get project ${projectName}`)
      ) {
        // 如果项目不存在，创建项目, 创建项目是在global 集群上创建的
        CommonKubectl.createResource(
          'alauda.asm.project.yaml',
          {
            '${project_description}': 'asm-uitest',
            '${project_displayname}': 'asm-uitest',
            '${project_name}': projectName,
            '${cluster_name}': projectpage.globalClusterName,
            '${label}': ServerConf.LABELBASEDOMAIN,
          },
          'L1.asm-project-pagination' + String(new Date().getMilliseconds()),
          projectpage.globalClusterName,
        );
      }
    }
    projectpage.login();
    projectpage.enterAdminView();
  });
  beforeEach(() => {
    projectpage.clickLeftNavByText('项目');
  });
  afterAll(() => {
    let projectName = 'asm-ui-';
    // 删除创建出的21个项目
    for (let i = 0; i < 21; i++) {
      projectName = projectName + i;
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl delete project ${projectName}`,
          this.globalClusterName,
        ),
      );
    }
  });
  it('ACP2UI-3944 : L1:单击左导航项目，显示项目列表，加载更多', () => {
    //默认20
    projectpage.listPage.projectTable.getRowCount().then(number => {
      expect(number).toBe(20);
    });

    projectpage.listPage.loadMoreButton.click();
    projectpage.listPage.projectTable.getRowCount().then(number => {
      expect(number).toBeGreaterThanOrEqual(21);
    });
  });

  it('ACP2UI-3943 : L1:单击左导航项目，显示项目列表，搜索 ', () => {
    projectpage.listPage.projectTable.searchByResourceName(projectName);
    projectpage.listPage.projectTable.getRowCount().then(number => {
      expect(number).toBe(1);
    });
  });
});
