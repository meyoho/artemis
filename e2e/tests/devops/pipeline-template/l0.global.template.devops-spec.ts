/**
 * Created by wangyanzhao on 2019/12/24.
 */

import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { GlobalTemplatePage } from '../../../page_objects/devops_new/pipeline-template/global.template.page';

describe('全局自定义模版 L0级别UI自动化case', () => {
  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  const page = new GlobalTemplatePage();
  if (!page.globalfeatureIsEnabled('devops-template-allnamespace')) {
    console.log('没有开启全局自定义模版功能');
    return '没有开启全局自定义模版功能';
  }
  beforeAll(() => {
    browser.refresh();
    page.login();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.clickLeftNavByText('全局自定义模版');
  });

  afterAll(() => {
    page.preparePage.deleteclusterpipelinetemplatesyncs();
    page.preparePage.deleteglobaltemplate();
  });

  it('ACP2UI-57968 : 代码仓库输入公有仓库-非master分支-不带凭据-模版导入成功', () => {
    const testData = {
      配置模版仓库: {
        代码仓库地址: gitlab_public,
        代码分支: 'cluster-template',
      },
    };
    page.detailPage.enterConfig('配置模版仓库');
    page.configTemplateRepo.create(testData);
    page.officialTemplate.getTemplatecount().then(count => {
      expect(count).toBe(1);
    });
  });
});
