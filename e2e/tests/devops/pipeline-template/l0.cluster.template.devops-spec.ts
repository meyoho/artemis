/**
 * Created by wangyanzhao on 2019/12/24.
 */

import { browser } from 'protractor';

import { OfficialTemplate } from '../../../page_objects/devops_new/pipeline-template/official.template.page';
// import { CommonPage } from '../../../utility/common.page';

describe('官方模版 L0级别UI自动化case', () => {
  const page = new OfficialTemplate();
  beforeAll(() => {
    browser.refresh();
    page.login();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.clickLeftNavByText('官方模版');
  });

  it('ACP2UI-60659 : 官方模版总数是16', () => {
    page.getTemplatecount().then(count => {
      expect(count).toBe(10);
    });

    page.clickNextPage(2);

    browser.sleep(2000);

    page.getTemplatecount().then(count => {
      expect(count).toBe(6);
    });
  });

  it('ACP2UI-60660 : 官方模版支持搜索', () => {
    page.searchTemplate('golang');

    browser.sleep(2000);

    page.getTemplatecount().then(count => {
      expect(count).toBe(2);
    });
  });
});
