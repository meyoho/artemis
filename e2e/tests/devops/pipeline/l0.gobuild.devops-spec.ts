/**
 * Created by zhangjiao on 2019/6/14.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('使用<Golang 构建>创建流水线-前提只绑定Jenkins L0级别UI自动化case', () => {
  // const page = new PipelineTemplatePage();
  const page = new PipelinePage();

  //const pipelinePage = new PipelineJenkinsfilePage();

  const project_name = page.projectName1;

  const pipeline_name_go1 = page.getTestData('gobuild01');

  // 代码仓库和镜像仓库相关参数
  const gitlabsecret_private = page.gitlabsecretPrivate1;
  const harborsecret_private = page.harborsecretPrivate1;

  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_go1, project_name);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_go1, project_name);
  });

  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-57918 : golang构建-gitlab-输入-http公有root仓库-输入master分支-流水线执行成功', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Golang 构建',
      基本信息: {
        名称: pipeline_name_go1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: 'master',
        构建命令: 'cd go-test-public; go build',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
        },
      },
    };
    page.createPage.create(testData);
    browser.sleep(1000);

    const expectData = {
      名称: pipeline_name_go1,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_public}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
        `credentialId = "${project_name}-${harborsecret_private}"`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });

  it('ACP2UI-58595 : 更新Golang构建流水线-执行成功', () => {
    const testData = {
      基本信息: {
        执行方式: '并发执行',
      },

      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_private,
          凭据: gitlabsecret_private,
        },
        分支: 'master',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
        },
      },
      触发器: {
        定时扫描: '每 24 小时 检查新提交',
        // 定时触发器: ['星期一', '星期二'],
      },
    };

    page.detailPage.update(testData);
    browser.sleep(1000);
    page.listPage.enterDetail(pipeline_name_go1);

    const expectData = {
      名称: pipeline_name_go1,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
      执行方式: '并发执行',
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
        `credentialId = "${project_name}-${harborsecret_private}"`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });

  // 删除 <Golang 构建>创建的流水线
  it('ACP2UI-58596 : golang流水线详情页删除成功 ', () => {
    page.detailPage.delete();
    page.listPage.search(pipeline_name_go1);
    browser.sleep(1000);
    page.listPage.noResult.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });
});
