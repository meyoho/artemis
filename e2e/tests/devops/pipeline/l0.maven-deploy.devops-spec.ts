/**
 * Created by zgSu on 2019/12/30.
 * 需要用到Nexus, 需要搭建Jenkins
 */

import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('使用<Maven 构建并分发到仓库>-使用global的凭据为项目绑定好代码仓库和制品仓库 L0级别UI自动化case', () => {
  const page = new PipelinePage();

  // base info
  const project_name = page.projectName2;
  const pipeline_name_maven_l0_1 = page.getTestData('maven-l0-1');
  const pipeline_name_maven_l0_2 = page.getTestData('maven-l0-2');

  // pipeline info
  const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  const gitlab_branch = ServerConf.GITLAB_BRANCH;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harbor_repo = ServerConf.HARBOR_REPO;
  const harbor_repo_tag = ServerConf.HARBOR_TAG;

  const dependencyRepo = page.mavenbind2;
  // jenkins info
  const jenkinsBinding = page.jenkinsbind2;

  beforeAll(() => {
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('持续交付');
  });

  beforeEach(() => {
    page.preparePage.deletePipelineconfig(
      pipeline_name_maven_l0_1,
      project_name,
    );
    page.preparePage.deletePipelineconfig(
      pipeline_name_maven_l0_2,
      project_name,
    );
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(
      pipeline_name_maven_l0_1,
      project_name,
    );
    page.preparePage.deletePipelineconfig(
      pipeline_name_maven_l0_2,
      project_name,
    );
  });

  it('ACP2UI-57341:使用<Maven 构建并分发到仓库>创建流水线-选择模板创建-选择mave构建并分发到仓库-名称-Jenkins实例-选择代码仓库-选择分支-输入maven构建命令-分发仓库选择Snapshot的-选择依赖仓库-镜像仓库-创建-执行', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Maven 构建并分发到仓库',
      基本信息: {
        名称: pipeline_name_maven_l0_1,
        Jenkins: jenkinsBinding,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: 'master',
        构建命令: 'cd java; mvn clean deploy',
        依赖仓库: dependencyRepo,
        分发仓库: dependencyRepo,
        镜像仓库: {
          方式: '选择',
          镜像仓库: `${ServerConf.HARBOR_PROJECT}/${harbor_repo}`,
          版本: [harbor_repo_tag],
        },
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_maven_l0_1);

    const expectData = {
      名称: pipeline_name_maven_l0_1,
      创建方式: '模版创建',
      Jenkins: jenkinsBinding,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    const expectYaml = {
      yamlvalue: ['agent {label "java"}', 'cd java; mvn clean deploy'],
    };

    page.detailVerifyPage.verify(expectYaml);
  });

  it('ACP2UI-57349:使用<Maven 构建并分发到仓库>创建流水线-选择模板创建-选择mave构建-名称-Jenkins实例-选择代码仓库-选择分支-输入maven构建命令-选择依赖仓库-镜像仓库-创建-执行', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Maven 构建并分发到仓库',
      基本信息: {
        名称: pipeline_name_maven_l0_2,
        Jenkins: jenkinsBinding,
        执行方式: '并发执行',
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: gitlab_branch,
        构建命令: 'cd java; mvn clean deploy',
        依赖仓库: dependencyRepo,
        分发仓库: dependencyRepo,
        镜像仓库: {
          方式: '选择',
          镜像仓库: `${ServerConf.HARBOR_PROJECT}/${harbor_repo}`,
          版本: [harbor_repo_tag],
        },
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_maven_l0_2);

    const expectData = {
      名称: pipeline_name_maven_l0_2,
      创建方式: '模版创建',
      Jenkins: jenkinsBinding,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });
});
