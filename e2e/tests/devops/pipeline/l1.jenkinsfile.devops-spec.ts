/**
 * Created by zhangjiao on 2019/6/14.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { PipelineJenkinsfilePage } from '../../../page_objects/devops/pipeline/jenkinsfile.page';
import { CommonMethod } from '../../../utility/common.method';
import { CommonPage } from '../../../utility/common.page';
import { CommonApi } from '../../../utility/common.api';
import { alauda_type_pipeline } from '../../../utility/resource.type.k8s';

describe('流水线 L1级别UI自动化case', () => {
  const page = new PipelineJenkinsfilePage();

  const project_name = page.projectName3;
  const jenkinsbind = page.jenkinsbind3;

  const pipeline_name1 = page.getTestData('pipelinel11');
  const pipeline_name2 = page.getTestData('pipelinel12');
  const pipeline_name3 = page.getTestData('pipelinel13');
  const pipeline_name4 = page.getTestData('pipelinel14');

  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  beforeAll(() => {
    page.deletePipelineconfig(pipeline_name1, project_name);
    page.deletePipelineconfig(pipeline_name2, project_name);
    page.deletePipelineconfig(pipeline_name3, project_name);
    page.deletePipelineconfig(pipeline_name4, project_name);
    CommonApi.createResource(
      'alauda.pipelineconfig.yaml',
      {
        '${NAME}': pipeline_name4,
        '${PROJECT_NAME}': project_name,
        '${JENKINSBIND_NAME}': jenkinsbind,
        '${PIPELINE_NAME}': pipeline_name4,
        '${SOURCEGIT}': gitlab_public,
        '${GIT_SECRET}': '',
      },
      alauda_type_pipeline,
      null,
    );
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('持续交付');
  });

  beforeEach(() => {
    page.navigationButton();
    browser.sleep(500);
  });

  afterAll(() => {
    page.deletePipelineconfig(pipeline_name1, project_name);
    page.deletePipelineconfig(pipeline_name2, project_name);
    page.deletePipelineconfig(pipeline_name3, project_name);
    page.deletePipelineconfig(pipeline_name4, project_name);
  });

  it('ACP2UI-52845:脚本创建流水线-通过页面编写Jenkinsfile-成功创建流水线-执行结果正确', () => {
    const testData = {
      名称: pipeline_name1,
      显示名称: 'AUTO_JENKINSFILE1',
      Jenkins: jenkinsbind,
      'Jenkinsfile 位置': '页面编写',
    };
    page.createPipiline_byscript_first(testData);
    page.createPage_yaml_textarea.checkEditorIsPresent();

    // 将yaml文件拷贝至yaml编辑框
    expect(page.createPage_yaml_textarea.checkEditorIsPresent()).toBeTruthy();
    const yamlFile = CommonMethod.readyamlfile('alauda.jenkinsfile.yaml');
    page.createPage_yaml_textarea.setYamlValue(yamlFile);
    page.createPage_nextButton.click();
    // 第三步骤-直接点击创建按钮
    page.createPage_createButton.click();

    // 创建成功过后详情页检查
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleEle(),
      pipeline_name1,
    );
    page.detailPage_Content
      .getElementByText('Jenkins')
      .getText()
      .then(text => {
        expect(text).toBe(jenkinsbind);
      });

    page.detailPage_Content
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toBe('AUTO_JENKINSFILE1');
      });

    page.detailPage_executeHistory_Table.getRowCount().then(count => {
      expect(count).toBe(0); // 执行记录为0
    });

    // 流水线设计展示Jenkinsfile
    page.detailPage_TabItem('流水线设计').click(); // 点击详情页的Jenkinsfile的tab
    browser.sleep(500);
    expect(
      page.detailPage_PipelineTab_jenkins.checkTextIsPresent(),
    ).toBeTruthy();
  });

  it('AldDevops-2464:脚本创建的流水线2484-详情页-执行并成功-执行记录检查(点击执行ID-到执行记录详情页)', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineName(project_name, pipeline_name1)
      .clickNameInlistPage();
    expect(page.detailPage_operate_all.isPresent()).toBeTruthy();
    page.detailPage_operate_all.click();
    page.detailPage_operate_button('执行').click();
    CommonPage.waitStateChangeTo(
      page.detail_executeHistory_status('1').getText(),
      '执行成功',
    );
    let num = 1;
    page
      .detail_executeHistory_status('1')
      .getText()
      .then(function(text) {
        if (text === '执行失败') {
          // 再执行一次
          page.detailPage_operate_all.click();
          page.detailPage_operate_button('执行').click();
          CommonPage.waitStateChangeTo(
            page.detail_executeHistory_status('2').getText(),
            '执行成功',
          );
          num = 2;
          console.log('一共执行了两次');
        }
      });
    expect(page.detail_executeHistory_status(num).getText()).toBe('执行成功');
    // 详情页中查看执行记录概览
    expect(page.detailPage_historyTable_logview.isPresent()).toBeTruthy();
    page.detailPage_historyTable_logview.click();
    expect(
      page.logView_dialog_header(pipeline_name1).checkTextIsPresent(),
    ).toBeTruthy();
    page.logView_dialog_cancel.click();
  });

  it('ACP2UI-60733 : 列表页-搜索功能-按名称搜索/按显示名称搜索', () => {
    expect(page.listPage_nameFilter_input.isPresent()).toBeTruthy(); // 检查搜索框是否存在

    page.resourceTable.searchByResourceName('autopipeline_bucunzai', 0); // 在搜索框输入不存在的流水线名称
    page.resourceTable.getRowCount().then(count => {
      expect(count).toBe(0);
    });

    page.resourceTable.aloSearch.auiSearch.clear(); // 清空检索框
    page.resourceTable.searchByResourceName(pipeline_name4, 1); // 在搜索框输入要检索的流水线名称
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name4)
        .checkNameInListPage(),
    ).toBeTruthy();
    page.resourceTable.getRowCount().then(count => {
      expect(count).toBe(1);
    });
  });

  it('ACP2UI-60734 : 流水线列表页-执行-更新-复制-删除-几个操作验证', () => {
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name4)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name4, '更新')
      .clickOperate(); // 列表页更新操作
    // 到更新页
    CommonPage.waitElementTextChangeTo(
      page.updatePage_Content.getElementByText('名称'),
      pipeline_name4,
    );
    page.updatePage_Content
      .getElementByText('名称')
      .getText()
      .then(text => {
        expect(text).toBe(pipeline_name4);
      });

    page.updatePage_Content
      .getElementByText('Jenkins')
      .getText()
      .then(text => {
        expect(text).toBe(jenkinsbind);
      });

    // 点击取消更新
    expect(page.updatePage_cancelButton.isPresent()).toBeTruthy();
    page.updatePage_cancelButton.click(); // 点击“取消”按钮

    // 点击复制
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name4)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name4, '复制')
      .clickOperate(); // 列表页复制操作
    // 到复制页，Jenkinsfile默认选中代码仓库
    page.createPipeline_location.getText().then(text => {
      expect(text).toBe('代码仓库'); // 判断选中的是否是“代码仓库”
    });

    page.updatePage_cancelButton.click(); // 点击“取消”按钮

    // 删除
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name4)
        .checkNameInListPage(),
    ).toBeTruthy();
    page
      .listPage_pipelineOperate(project_name, pipeline_name4, '删除')
      .clickOperate(); // 列表页删除操作
    page.listPage_confirmdelete_button().click();
    expect(
      page
        .listPage_pipelineName(project_name, pipeline_name4)
        .checkNameNotInListPage(),
    ).toBeFalsy();
  });
});
