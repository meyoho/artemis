/**
 * Created by liuzongyao on 2020/2/18.
 */

import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('流水线支持jmeter', () => {
  // const page = new PipelineTemplatePage();
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const pipeline_name_jmeter1 = page.getTestData('jmeter1');
  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_jmeter1, project_name);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  beforeEach(() => {});

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_jmeter1, project_name);
  });

  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-58316 : 代码仓库输入公有仓库-master分支-不带凭据-输入jmeter命令-流水线执行成功', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'JMeter 自动化测试',
      基本信息: {
        名称: pipeline_name_jmeter1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: 'master',
        'JMeter 命令':
          'cd jmeter; jmeter -n -t jmeter.jmx -l jmeter.jtl -j jmeter.log -e -o jmeter',
        产出物路径: 'jmeter/*',
      },
    };
    page.createPage.create(testData);
    browser.sleep(1000);

    // 执行流水线
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
