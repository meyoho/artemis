/**
 * Created by wangyanzhao on 2020/3/17.
 * 需要搭建Jenkins
 */

import { browser } from 'protractor';

// import { CommonPage } from '../../../utility/common.page';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';
import { ServerConf } from '@e2e/config/serverConf';

describe('使用<更新应用>创建流水线-前提只绑定Jenkins,在不同的集群创建好应用 L0级别UI自动化case', () => {
  const page = new PipelinePage();

  const project_name = page.projectName1;
  const region_name = page.clusterDisplayName(ServerConf.REGIONNAME);
  const namespace = 'UI TEST NS';

  const pipeline_name_update_deploy1 = page.getTestData('updatedeploy01');
  const application_name = page.devopsApp1;

  // 镜像仓库相关参数
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}:${ServerConf.HARBOR_TAG}`;
  const harborsecret_private = page.harborsecretPrivate1;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;

  beforeAll(() => {
    page.preparePage.deletePipelineconfig(
      pipeline_name_update_deploy1,
      project_name,
    );
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
  });

  beforeEach(() => {
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(
      pipeline_name_update_deploy1,
      project_name,
    );
  });

  it('ACP2UI-57939 : 创建流水线-选择模版创建-选择更新应用模版-输入名称-选择jenkins实例-选择业务集群、命名空间、应用、容器、镜像仓库-打开回滚按钮-创建-执行', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: '更新应用',
      基本信息: {
        名称: pipeline_name_update_deploy1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
        镜像仓库: {
          方式: '输入',
          镜像地址: harbor_image_url,
          凭据: harborsecret_private,
        },
        失败后回滚: 'true',
      },
    };

    page.createPage.create(testData);
    page.listPage.enterDetail(pipeline_name_update_deploy1);

    const expectData = {
      名称: pipeline_name_update_deploy1,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    browser.sleep(100);

    const expectYaml = {
      yamlvalue: [
        `agent {label "tools"}`,
        `newImage['name'] = "${harbor_image}`,
        `container.name == "${application_name}"`,
        `def shouldUpdate = true`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });
});
