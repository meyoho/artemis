/**
 * Created by liuzongyao on 2020/2/18.
 */

import { browser } from 'protractor';
// import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('流水线支持jmeter选择代码仓库', () => {
  // const page = new PipelineTemplatePage();
  const page = new PipelinePage();
  const project_name = page.projectName2;
  const pipeline_name_jmeter2 = page.getTestData('jmeter2');
  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind2;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_jmeter2, project_name);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  beforeEach(() => {});

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_jmeter2, project_name);
  });

  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-58640 : 代码仓库选择仓库-test分支-输入jmeter命令-流水线执行成功', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'JMeter 自动化测试',
      基本信息: {
        名称: pipeline_name_jmeter2,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: 'root/devops-ui-private',
        },
        分支: 'test/v1.1',
        'JMeter 命令':
          'cd jmeter; jmeter -n -t jmeter.jmx -l jmeter.jtl -j jmeter.log -e -o jmeter',
        产出物路径: 'jmeter/*',
      },
    };
    page.createPage.create(testData);
    browser.sleep(1000);

    // 执行流水线
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
