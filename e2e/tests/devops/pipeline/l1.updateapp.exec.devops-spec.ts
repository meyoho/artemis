/**
 * Created by liuzongyao on 2020/5/7.
 */

import { browser } from 'protractor';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('更新应用模版', () => {
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const region_name = page.clusterDisplayName(ServerConf.REGIONNAME);
  const namespace = 'UI TEST NS';
  const pipeline_name01 = page.getTestData('updateapp01');
  const application_name = page.devopsApp1;
  const harborsecret_private = page.harborsecretPrivate1;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name01, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('流水线');
  });
  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name01, project_name);
  });
  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-57940 : 创建流水线-选择模版创建-选择更新应用模版-输入名称-选择jenkins实例-选择业务集群、命名空间、应用、容器、镜像仓库-输入启动命令和参数-打开回滚按钮-创建-执行', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: '更新应用',
      基本信息: {
        名称: pipeline_name01,
        Jenkins: jenkinsbind,
      },
      模版参数设置: {
        镜像仓库: {
          方式: '输入',
          镜像地址: harbor_image_url.concat(':${imageTag}'),
          凭据: harborsecret_private,
        },
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
      },
    };
    page.createPage.create(testData);
    const expectData = {
      显示名称: '-',
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };
    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');
    const expectYaml = {
      yamlvalue: ['containerName = "devops-app1"'],
    };
    page.detailVerifyPage.verify(expectYaml);

    // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executeParamPipeline('dont-change-e2e-use');
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
