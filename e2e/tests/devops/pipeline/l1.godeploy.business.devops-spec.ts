/**
 * Created by zhangjiao on 2019/6/14.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('使用<Golang 构建并部署应用>创建流水线-前提只绑定Jenkins,在不同的集群创建好应用 L0级别UI自动化case', () => {
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const region_name = page.clusterDisplayName(ServerConf.REGIONNAME);
  const namespace = 'UI TEST NS';
  const pipeline_name_gou1 = page.getTestData('godeploy01');
  const pipeline_name_gou2 = page.getTestData('godeploy02');
  const application_name = page.devopsApp1;
  // 代码仓库和镜像仓库相关参数
  const gitlabsecret_private = page.gitlabsecretPrivate1;
  const harborsecret_private = page.harborsecretPrivate1;
  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  const gitlab_branch = `${ServerConf.GITLAB_BRANCH}`;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_gou2, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('流水线');
  });
  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_gou2, project_name);
  });
  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-57894 : 创建流水线-选择模版创建-go构建并更新应用模版-填写正确的名称、jenkins实例、代码仓库、镜像仓库-选择业务集群、命名空间、应用、容器-通知-创建-执行', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Golang 构建并部署应用',
      基本信息: {
        名称: pipeline_name_gou1,
        Jenkins: jenkinsbind,
      },
      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: 'master',
        构建命令: 'cd go; go build',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
          版本: ['latest', '${GIT_COMMIT}'],
        },
        'Docker 构建': '展开高级选项',
        构建参数: '--force-rm',
        Dockerfile: 'go/Dockerfile',
        重试次数: '2',
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
      },
    };
    page.createPage.create(testData);
    const expectData = {
      显示名称: '-',
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };
    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');
    const expectYaml = {
      yamlvalue: [
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };
    page.detailVerifyPage.verify(expectYaml);

    // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
  it('ACP2UI-57700: 输入git私有仓库地址,选择凭据,输入其他存在分支-其他正确输入-创建成功', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Golang 构建并部署应用',
      基本信息: {
        名称: pipeline_name_gou2,
        Jenkins: jenkinsbind,
      },
      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: gitlab_branch,
        构建命令: 'cd go; go build',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
          版本: ['gobuild', '${GIT_COMMIT}'],
        },
        'Docker 构建': '展开高级选项',
        构建参数: '--force-rm',
        Dockerfile: 'go/Dockerfile',
        重试次数: '2',
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
      },
    };
    page.createPage.create(testData);
    const expectData = {
      显示名称: '-',
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };
    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');
    const expectYaml = {
      yamlvalue: [
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };
    page.detailVerifyPage.verify(expectYaml);

    // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
  // 更新 <Golang 构建并更新服务>创建的流水线
  it('ACP2UI-57701: 更新57700的流水线-选择git仓库凭据-更新成功-流水线执行成功', async () => {
    page.listPage.enterDetail(pipeline_name_gou2);
    const testData = {
      基本信息: {
        执行方式: '并发执行',
      },
      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_private,
          凭据: gitlabsecret_private,
        },
        分支: 'master',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
        },
      },
      触发器: {
        定时扫描: '每 24 小时 检查新提交',
        // 定时触发器: ['星期一', '星期二'],
      },
    };
    page.detailPage.update(testData);
    browser.sleep(1000);
    page.listPage.enterDetail(pipeline_name_gou2);
    const expectData = {
      名称: pipeline_name_gou2,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
      执行方式: '并发执行',
    };
    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');
    const expectYaml = {
      yamlvalue: [
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };
    page.detailVerifyPage.verify(expectYaml);

    // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
