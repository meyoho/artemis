/**
 * Created by liuzongyao on 2020/4/18.
 */
import { browser } from 'protractor';
import { ServerConf } from '../../../config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('多分支创建流水线 L0级别UI自动化case', () => {
  const page = new PipelinePage();
  if (!page.createPage.isReady('devops-pipeline-multibranch')) {
    console.log('没有开启多分支流水线功能');
    return '没有开启多分支流水线功能';
  }

  const project_name = page.projectName3;
  const jenkinsbind = page.jenkinsbind3;

  const pipeline_name3 = page.getTestData('multibranch3');

  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name3, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('持续交付');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name3, project_name);
  });

  it('ACP2UI-56301 : 输入名称-选择实例-选择gitlab仓库-其他默认-执行成功', async () => {
    const testData = {
      创建类型: '多分支流水线',
      基本信息: {
        名称: pipeline_name3,
        Jenkins: jenkinsbind,
      },

      分支设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
      },
    };
    page.createPage.create_multibranch(testData);
    browser.sleep(10000);

    // 执行流水线
    await page.detailPage.scanPipeline();
    page.detailPage.clickTab('执行记录');
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
