/**
 * Created by liuzongyao on 2020/2/18.
 */

import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('使用<Golang 构建>创建流水线-前提只绑定Jenkins L0级别UI自动化case', () => {
  // const page = new PipelineTemplatePage();
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const pipeline_name_nodejs1 = page.getTestData('nodejs1');
  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  const gitlab_branch = ServerConf.GITLAB_BRANCH;
  const harborsecret_private = page.harborsecretPrivate1;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_nodejs1, project_name);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_nodejs1, project_name);
  });

  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-58591 : nodejs 构建 命令 node -v', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Nodejs 10 构建',
      基本信息: {
        名称: pipeline_name_nodejs1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: gitlab_branch,
        构建命令: 'node -v',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
        },
      },
    };
    page.createPage.create(testData);
    browser.sleep(1000);

    // 执行流水线
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
