/**
 * Created by zhangjiao on 2019/6/14.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('使用<Golang 构建>创建流水线-前提只绑定Jenkins,在不同的集群创建好应用 L1级别UI自动化case', () => {
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const pipeline_name_gou1 = page.getTestData('gobuild04');
  const harborsecret_private = page.getTestData('harbors1');
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    page.preparePage.deleteSecret(harborsecret_private, project_name, 'global');
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('流水线');
  });
  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    page.preparePage.deleteSecret(harborsecret_private, project_name, 'global');
  });
  // ------------在创建流水线过程中创建镜像凭据------------
  it('ACP2UI-57696 : 模版创建流水线-在第二步模版参数-Docker 构建-点击镜像仓库<选择>-输入-添加凭据(类型只有镜像仓库)-凭据添加成功', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Golang 构建',
      基本信息: {
        名称: pipeline_name_gou1,
        Jenkins: jenkinsbind,
      },
      模版参数设置: {
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          添加凭据: {
            凭据名称: harborsecret_private,
            //类型: '镜像服务',
            镜像服务地址: ServerConf.HARBOR_HOST,
            用户名: ServerConf.HARBOR_USER,
            密码: ServerConf.HARBOR_PWD,
            邮箱地址: 'jiaozhang@alauda.io',
          },
        },
      },
    };
    page.createPage.getButtonByText('创建流水线').click();
    page.createPage.dialogCreateType.select(testData['创建类型']);
    page.createPage.createTemplateSelectorPage.select(testData['选择模版']);
    page.createPage.basicInfo.fill(testData['基本信息']);
    page.createPage.templateParamaterConfig.input(testData['模版参数设置']);
  });
});
