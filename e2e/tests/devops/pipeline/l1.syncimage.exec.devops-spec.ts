/**
 * Created by liuzongyao on 2020/5/7.
 */

import { browser } from 'protractor';
import { ServerConf } from '@e2e/config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('同步镜像流水线', () => {
  // const page = new PipelineTemplatePage();
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const pipeline_name_1 = page.getTestData('syncimage1');
  const harborsecret_private = page.harborsecretPrivate1;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_1, project_name);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_1, project_name);
  });

  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-60780 : 同步镜像目标tag参数化', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: '同步镜像',
      基本信息: {
        名称: pipeline_name_1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        源镜像仓库地址: harbor_image_url,
        源镜像凭据: harborsecret_private,
        源镜像信息: '展开高级选项',
        源镜像标签: 'dont-change-e2e-use',
        目标镜像仓库地址: harbor_image_url,
        目标镜像凭据: harborsecret_private,
      },
    };
    page.createPage.create(testData);
    browser.sleep(1000);

    // 执行流水线
    await page.detailPage.executeParamPipeline('v100');
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
