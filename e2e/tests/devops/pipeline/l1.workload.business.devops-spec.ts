/**
 * Created by liuzongyao on 2020/4/21.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';
import { ServerConf } from '@e2e/config/serverConf';
describe('使用<Golang 构建并部署应用>创建流水线-workload', () => {
  const page = new PipelinePage();
  const project_name = page.projectName1;
  const region_name = page.clusterDisplayName(ServerConf.REGIONNAME);
  const namespace = 'UI TEST NS';
  const project1ns1 = page.project1ns1;
  const appname1 = 'app';
  const pipeline_name_gou1 = page.getTestData('workload01');
  const harborsecret_private = page.harborsecretPrivate1;
  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  // const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  // const gitlab_branch = `${ServerConf.GITLAB_BRANCH}`;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harbor_image_url = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind1;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
  });
  beforeEach(() => {
    page.clickLeftNavByText('流水线');
  });
  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_gou1, project_name);
    page.preparePage.deleteApp(appname1, project1ns1);
  });
  // ------------------------下面是在project1中创建流水线，通过输入方式的测试用例-------------------
  it('ACP2UI-57063 : 创建流水线-模版创建-选择go构建并部署应用-填写名称-选择实例-选择代码仓库-选择分支-选择镜像仓库-打开创建应用按钮-选择业务集群、命名空间-修改部署配置目录-填写容器名称-执行', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Golang 构建并部署应用',
      基本信息: {
        名称: pipeline_name_gou1,
        Jenkins: jenkinsbind,
      },
      模版参数设置: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        分支: 'master',
        构建命令: 'cd workload; go build',
        镜像仓库: {
          方式: '输入',
          镜像仓库: harbor_image_url,
          凭据: harborsecret_private,
          版本: ['${GIT_COMMIT}', 'latest'],
        },
        'Docker 构建': '展开高级选项',
        构建参数: '--force-rm',
        Dockerfile: 'go/Dockerfile',
        重试次数: '2',
        创建应用: true,
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        // ------------------------配置目录是相对代码仓库根目录-------------------
        部署配置目录: 'workload/app',
        部署容器: 'app-test',
      },
    };
    page.createPage.create(testData);
    const expectData = {
      显示名称: '-',
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };
    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');
    const expectYaml = {
      yamlvalue: [
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };
    page.detailVerifyPage.verify(expectYaml);
    // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
