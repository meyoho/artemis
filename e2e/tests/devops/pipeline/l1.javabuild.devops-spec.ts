/**
 * Created by zhangjiao on 2019/6/14.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('java构建相关用例', () => {
  const page = new PipelinePage();

  const project_name = page.projectName2;
  const pipeline_name_javau1 = page.getTestData('javabuild01');


  // 代码仓库和镜像仓库相关参数
  const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harborrepo_repo = ServerConf.HARBOR_REPO;
  const harborrepo_tag = ServerConf.HARBOR_TAG;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind2;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_javau1, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_javau1, project_name);
  });

  it('ACP2UI-58592 : java构建 命令 java -version mvn -version', async () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Java 构建',
      基本信息: {
        名称: pipeline_name_javau1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: 'master',
        构建命令: 'cd java; java -version; mvn -version',
        镜像仓库: {
          方式: '选择',
          镜像仓库: `${ServerConf.HARBOR_PROJECT}/${harborrepo_repo}`,
          版本: [harborrepo_tag],
        }
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_javau1);

    const expectData = {
      名称: pipeline_name_javau1,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    browser.sleep(100);

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
        // 执行流水线
    page.detailPage.clickTab('详情信息');
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
