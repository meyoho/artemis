/**
 * Created by zhangjiao on 2019/6/14.
 */

import { browser } from 'protractor';
import { ServerConf } from '../../../config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('脚本创建流水线 L0级别UI自动化case', () => {
  const page = new PipelinePage();

  const project_name = page.projectName3;
  const jenkinsbind = page.jenkinsbind3;

  const pipeline_name1 = page.getTestData('pipelinel11');

  const gitlab_public = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PUBLIC}`;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name1, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('持续交付');
  });

  beforeEach(() => {});

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name1, project_name);
  });

  it('ACP2UI-55905 : 输入名称-选择实例-代码仓库-串行-选择代码仓库-master分支-脚本路径默认-无触发器-执行成功', async () => {
    const testData = {
      创建类型: '脚本创建',
      基本信息: {
        名称: pipeline_name1,
        Jenkins: jenkinsbind,
      },

      Jenkinsfile: {
        代码仓库: {
          方式: '输入',
          代码仓库地址: gitlab_public,
        },
        代码分支: 'master',
      },
    };
    page.createPage.create_script(testData);
    browser.sleep(1000);

    // 执行流水线
    await page.detailPage.executePipeline();
    const result = await page.detailPage.listPage_pipelineExecuteResult();
    expect(result).toBe('执行成功');
  });
});
