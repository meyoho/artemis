/**
 * Created by zhangjiao on 2019/6/14.
 * 需要用到外网Github, 需要搭建Jenkins
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { PipelinePage } from '@e2e/page_objects/devops_new/pipeline/pipeline.page';

describe('使用<Java 构建并部署应用>-使用global的凭据为项目分配好代码仓库和制品仓库 L0级别UI自动化case', () => {
  const page = new PipelinePage();

  const project_name = page.projectName2;
  const region_name = page.clusterDisplayName(ServerConf.REGIONNAME);
  const namespace = 'UI TEST NS';

  const pipeline_name_javau1 = page.getTestData('javadepoy01');
  const pipeline_name_javau2 = page.getTestData('javadepoy02');
  const pipeline_name_javau3 = page.getTestData('javadepoy03');
  const application_name = page.devopsApp2;

  // 代码仓库和镜像仓库相关参数
  const gitlab_private = `${ServerConf.GITLAB_URL}/${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`;
  const harbor_image = `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}/${ServerConf.HARBOR_REPO}`;
  const harborrepo_repo = ServerConf.HARBOR_REPO;
  const harborrepo_tag = ServerConf.HARBOR_TAG;
  const devops_email_name = ServerConf.DEVOPS_EMAIL_NAME;

  // Jenkins相关参数
  const jenkinsbind = page.jenkinsbind2;
  beforeAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_javau1, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_javau2, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_javau3, project_name);
    browser.sleep(1000);
    page.login();
    page.enterProjectDashboard();
    page.enterProject(project_name);
    page.clickLeftNavByText('流水线');
  });

  afterAll(() => {
    page.preparePage.deletePipelineconfig(pipeline_name_javau1, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_javau2, project_name);
    page.preparePage.deletePipelineconfig(pipeline_name_javau3, project_name);
  });

  it('ACP2UI-53588:选择已绑定代码仓库-选择镜像仓库地址-其他默认正确输入-创建并执行成功', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Java 构建并部署应用',
      基本信息: {
        名称: pipeline_name_javau1,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: 'master',
        构建命令: 'cd java; mvn clean package',
        镜像仓库: {
          方式: '选择',
          镜像仓库: `${ServerConf.HARBOR_PROJECT}/${harborrepo_repo}`,
          版本: [harborrepo_tag],
        },
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_javau1);

    const expectData = {
      名称: pipeline_name_javau1,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    browser.sleep(100);

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });

  it('ACP2UI-57703 : java构建并更新应用-选择仓库-master分支-新建镜像仓库-更新应用', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Java 构建并部署应用',
      基本信息: {
        名称: pipeline_name_javau2,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: 'master',
        构建命令: 'cd java; mvn clean package',
        镜像仓库: {
          方式: '新建',
          镜像仓库: [
            `${ServerConf.HARBOR_HOST}/${ServerConf.HARBOR_PROJECT}`,
            `${ServerConf.HARBOR_REPO}`,
          ],
        },
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_javau2);

    const expectData = {
      名称: pipeline_name_javau2,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });

  it('ACP2UI-57847 : 创建流水线-模板创建-选择模板-Java构建并部署应用-填写基本信息、模板参数设置、触发器（可以使流水线成功的参数）-开启通知-选择一个通知-创建-执行-流水线结束后收到执行成功的通知', () => {
    const testData = {
      创建类型: '模版创建',
      选择模版: 'Java 构建并部署应用',
      基本信息: {
        名称: pipeline_name_javau3,
        Jenkins: jenkinsbind,
      },

      模版参数设置: {
        代码仓库: {
          方式: '选择',
          代码仓库: `${ServerConf.GITLAB_USER}/${ServerConf.GITLAB_PRIVATE}`,
        },
        分支: 'master',
        构建命令: 'cd java; mvn clean package',
        镜像仓库: {
          方式: '选择',
          镜像仓库: `${ServerConf.HARBOR_PROJECT}/${harborrepo_repo}`,
          版本: [harborrepo_tag],
        },
        集群: region_name,
        命名空间: namespace.toLocaleUpperCase(),
        应用: application_name + '/deployment/' + application_name,
        容器: application_name,
        开启通知: 'true',
        通知: devops_email_name,
      },
    };

    page.createPage.create(testData);

    page.listPage.enterDetail(pipeline_name_javau3);

    const expectData = {
      名称: pipeline_name_javau3,
      创建方式: '模版创建',
      Jenkins: jenkinsbind,
    };

    page.detailVerifyPage.verify(expectData);
    page.detailPage.clickTab('流水线设计');

    browser.sleep(100);

    const expectYaml = {
      yamlvalue: [
        `codeRepository = "${gitlab_private}"`,
        `def repositoryAddr = '${harbor_image}'.replace("http://","").replace("https://","")`,
      ],
    };

    page.detailVerifyPage.verify(expectYaml);
  });
});
