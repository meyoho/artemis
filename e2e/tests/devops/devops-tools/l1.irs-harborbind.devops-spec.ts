/**
 * Created by zhangjiao on 2019/6/17.
 */

import { browser, $ } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ProjectPage } from '@e2e/page_objects/devops_new/project/project.page';
import { CommonKubectl } from '../../../utility/common.kubectl';

describe('Devops工具链-HarborBinding L1级别UI自动化case', () => {
  const page = new ProjectPage();
  const preparePage = page.devopsToolPage.preparePage;
  const project_name = page.projectName4;

  const harbor_name = ServerConf.HARBOR_NAME;
  const harborsecret_private = page.harborsecretPrivate4;
  const harborsecret_global = page.harborsecretGlobal;
  const harborsecret_error = page.gitlabsecretPrivate4;
  const harborbind_name1 = page.getTestData('harborbind1');
  const harborbind_name2 = page.getTestData('harborbind2');
  const harborbind_name3 = page.getTestData('harborbind4');
  const harborbind_api3 = page.getTestData('harborbind3');
  beforeAll(() => {
    preparePage.deleteimageRepoBinding(harborbind_name1, project_name);
    preparePage.deleteimageRepoBinding(harborbind_name2, project_name);
    preparePage.deleteimageRepoBinding(harborbind_name3, project_name);
    preparePage.deleteimageRepoBinding(harborbind_api3, project_name);
    browser.sleep(1000);

    this.testdata1 = CommonKubectl.createResourceByTemplate(
      'alauda.imageregistry-bindnorepo.yaml',
      {
        REGISTRY_NAME: harbor_name,
        REGISTRYBIND_NAME: harborbind_api3,
        NAMESPACE: project_name,
        HARBOR_SECRET: harborsecret_private,
        SECRET_NS: project_name,
        REGISTRY_TYPE: 'Harbor',
      },
      'l1.harborregistry.testdata1',
    );
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    preparePage.deleteimageRepoBinding(harborbind_name1, project_name);
    preparePage.deleteimageRepoBinding(harborbind_name2, project_name);
    preparePage.deleteimageRepoBinding(harborbind_name3, project_name);
    preparePage.deleteimageRepoBinding(harborbind_api3, project_name);
  });

  it('ACP2UI-56049 : harbor绑定-输入名称-描述-选择凭据-点击绑定账号-选择镜像仓库-点击添加地址-分配仓库', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_name1,
      描述: 'bind Harbor registry service description1111!',
      凭据: harborsecret_private,
      选择镜像repo: 'e2e-automation',
      点击添加地址: 'click',
    };

    page.detailPage.bind(testData);
    browser.sleep(20000);

    const expectData = {
      '类型：': '制品仓库 / Harbor 私有版',
      '凭据：': harborsecret_private,
      '描述：': 'bind Harbor registry service description1111!',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyHarborBind(expectData);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toBeGreaterThan(0);
    });
    browser.sleep(100);
  });

  it('ACP2UI-56088 : harbor绑定-输入的名称-输入描述信息（中文-长度-格式）-选择凭据-点击绑定账号-分配仓库页面点击取消按钮', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_name3,
      描述:
        '格式中文-长度-格式：测试描述信息过长{描述信息中包括特殊字符类似*、|898193801930」}vhfjvfkjvkJIFVOIOWPWOVUFYVIFVFVFVFVJVKFJVFVFVFV',
      凭据: harborsecret_private,
    };

    page.detailPage.bind(testData);
    page.detailPage.bindIntergratePage.primaryButton.click();
    browser.sleep(1000);

    const expectData = {
      '类型：': '制品仓库 / Harbor 私有版',
      '凭据：': harborsecret_private,
      '描述：':
        '格式中文-长度-格式：测试描述信息过长{描述信息中包括特殊字符类似*、|898193801930」}vhfjvfkjvkJIFVOIOWPWOVUFYVIFVFVFVFVJVKFJVFVFVFV',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyHarborBind(expectData);
    browser.sleep(100);
  });

  it('ACP2UI-56050 : harbor绑定-输入名称-输入描述-选择凭据-点击绑定账号-选择镜像仓库-添加地址-删除地址-分配仓库', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_name2,
      描述: 'bind Harbor registry service description222',
      凭据: harborsecret_global,
      选择镜像repo: 'e2e-automation',
      点击添加地址: 'click',
      删除已添加的地址: ['e2e-automation'],
    };

    page.detailPage.bind(testData);
    browser.sleep(1000);

    const expectData = {
      '类型：': '制品仓库 / Harbor 私有版',
      '凭据：': harborsecret_global,
      '描述：': 'bind Harbor registry service description222',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyHarborBind(expectData);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toEqual(0);
    });
    browser.sleep(100);
  });

  it('ACP2UI-56080 : 项目详情页-点击Devops工具链-点击制品仓库-任意选择一个仓库，点击名称进入详情页-点击分配仓库-选择镜像仓库-点击添加地址按钮-点击更新按钮', () => {
    const testData = {
      项目名称: project_name,
      名称: harborbind_name2,
      选择镜像repo: 'e2e-automation',
      点击添加地址: 'click',
    };
    page.detailPage.update_assignBind(testData);
    browser.sleep(20000);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toBeGreaterThan(0);
    });
    browser.sleep(100);
  });

  it('ACP2UI-56096 : 项目详情页-点击Devops工具链-名称框中输入名称-按名称过滤检查', () => {
    page.listPage.enterDetail(project_name); // 选择项目进入详情页
    page.detailPage.clickByName('DevOps 工具链'); // 单击 "DevOps 工具链 " Tab
    // 等待状态消失
    page.waitElementNotPresent(
      $('div[class*="empty"]'),
      '【数据加载中...】的提示没有消失',
    );
    page.devopsToolPage.updateDialogPage.input_search.input('bucunzai');
    expect(
      page.devopsToolPage.updateDialogPage.bindListPage_empty.checkTextIsPresent(),
    ).toBeTruthy();
    page.devopsToolPage.updateDialogPage.input_search.input(harborbind_name1);
    expect(
      page.devopsToolPage.updateDialogPage
        .bindListPage_Exist(harborbind_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
  });

  it('ACP2UI-56086 : harbor绑定-输入名称-输入描述-选择错误用户名密码的凭据-点击绑定账号', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_name2,
      描述: 'bind Harbor registry service description1111!',
      凭据: harborsecret_error,
    };
    page.detailPage.fillBindInfo(testData);
    console.log('page.detailPage.bindIntergratePage.primaryButton2.click();');
    page.detailPage.bindIntergratePage.primaryButton.click();

    //由于Secret错误绑定失败
    page.detailPage.errorDialog.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
      // 等待6秒，错误框自动消失
      browser.sleep(6000);
    });
  });

  it('ACP2UI-56078 : 项目详情页-点击Devops工具链-点击制品仓库-任意选择一个仓库，点击名称进入详情页-点击更新按钮-更新描述-更新认证方式-点击更新按钮', () => {
    const testData = {
      项目名称: project_name,
      名称: harborbind_name2,
      描述: 'update binding',
      认证方式: '无认证',
    };
    page.detailPage.updateBind(testData);
    browser.sleep(1000);

    const expectData = {
      类型: '制品仓库 / Harbor 私有版',
      描述: 'update binding',
      凭据: '无认证',
    };

    // 更新绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyHarborBind(expectData);
  });

  it('ACP2UI-56093 : 项目详情页-点击Devops工具链-点击制品仓库-选择一个已分配仓库的仓库，点击名称进入详情页-点击解除绑定', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_name1,
    };
    page.detailPage.unbind(testData);

    // 验证解绑成功
    page.detailPage.isBind(testData).then(isBind => {
      expect(isBind).toBeFalsy();
    });
  });

  it('ACP2UI-56094 : 项目详情页-点击Devops工具链-点击制品仓库-选择一个未分配仓库的仓库，点击名称进入详情页-点击解除绑定', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: harbor_name,
      名称: harborbind_api3,
    };

    page.detailPage.unbind2(testData);

    // 验证解绑成功
    page.detailPage.isBind(testData).then(isBind => {
      expect(isBind).toBeFalsy();
    });
  });
});
