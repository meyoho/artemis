/**
 * Created by zhangjiao on 2019/6/17.
 */
import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { DevopsToolPage } from '@e2e/page_objects/devops_new/devops-tools/devops.tools.page';

describe('Devops工具链-Harbor L1级别UI自动化case', () => {
  const page = new DevopsToolPage();
  const preparePage = page.preparePage;
  const harbor_name = ServerConf.HARBOR_NAME;
  const project_name = page.projectName4;
  const harbor_secret = page.harborsecretPrivate4;
  const harbor_url = `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`;
  const harbor_name1 = page.getTestData('harbor1');
  const harbor_url1 = 'http://www.' + harbor_name1 + '1.com';
  const harbor_url2 = 'http://www.' + harbor_name1 + '2.com';
  const bind_name1 = 'inter-ui-bind1';
  const bind_name2 = 'inter-ui-bind2';
  beforeAll(() => {
    preparePage.deleteimageRepoBinding(bind_name1, project_name);
    preparePage.deleteimageRepoBinding(bind_name2, project_name);
    page.preparePage.deleteharbor(harbor_name1);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    preparePage.deleteimageRepoBinding(bind_name1, project_name);
    preparePage.deleteimageRepoBinding(bind_name2, project_name);
    page.preparePage.deleteharbor(harbor_name1);
  });

  it('ACP2UI-55940 : 点击Devops工具链-集成-制品仓库-harbor-staging-输入名称-输入访问地址-输入无效的API地址-点击集成按钮', () => {
    const testData = {
      集成: 'Harbor Registry 私有版',
      名称: harbor_name1,
      访问地址: harbor_url1,
      'API 地址': harbor_url1,
    };
    page.intergratePage.integrate(testData);

    const expectData = {
      类型: '制品仓库 / Harbor 私有版',
      访问地址: harbor_url1,
      'API 地址': harbor_url1,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55941 : 点击Devops工具链-集成-制品仓库-harbor-registry-输入名称-输入访问地址-输入API地址-点击取消按钮', () => {
    const testData = {
      集成: 'Harbor Registry 私有版',
      名称: 'harbor-cancel',
      访问地址: 'https://harbor.test',
      'API 地址': 'https://harbor.test',
    };
    page.intergratePage.fillForm(testData);
    page.intergratePage.intergrateDialog.clickCancelButton();

    //验证取消集成后 集成按钮在列表页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-55944 : 点击Devops工具链-点击制品仓库-任意选择一个harbor名称点击进入详情页-点击更新按钮-更新访问地址-更新为有效的API地址-点击更新按钮', () => {
    const testData = {
      工具链名称: harbor_name1,
      'API 地址': harbor_url2,
      访问地址: harbor_url2,
    };
    page.detailPage.updateBasicInfo(testData);

    const expectData = {
      类型: '制品仓库 / Harbor 私有版',
      访问地址: harbor_url2,
      'API 地址': harbor_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55958 : 点击Devops工具链-点击制品仓库-任意选择一个harbor名称点击进入详情页-点击更新按钮-更新访问地址-更新为已存在的API地址-点击更新按钮', () => {
    const testData = {
      工具链名称: harbor_name1,
      'API 地址': harbor_url, // 第一次改地址：API地址改成已存在的地址
      访问地址: harbor_url,
    };
    page.detailPage.fillUpdateBasicInfo(testData);
    page.detailPage.updateDialog.clickUpdateButton();
    page.detailPage.errorDialog.isPresent().then(isPresent => {
      // 验证更新失败弹窗
      expect(isPresent).toBeTruthy();

      // 输出错误信息
      page.detailPage.errorDialog.message.getText().then(text => {
        console.log(text);
      });
    });

    // 等待错误弹框消失
    browser.sleep(6000);

    // 点击取消按钮
    page.detailPage.updateDialog.clickCancelButton();

    const expectData = {
      类型: '制品仓库 / Harbor 私有版',
      访问地址: harbor_url2,
      'API 地址': harbor_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55946 : 点击Devops工具链-点击制品仓库-任意选择一个harbor名称点击进入详情页-点击更新按钮-更新访问地址-更新API地址-点击取消按钮', () => {
    const testData = {
      工具链名称: harbor_name1,
      访问地址: 'https://update.harbor', // 修改服务地址
    };
    page.detailPage.fillUpdateBasicInfo(testData);

    // 点击取消按钮
    page.detailPage.updateDialog.clickCancelButton();

    //取消更新后页面还在详情页
    const expectData = {
      类型: '制品仓库 / Harbor 私有版',
      访问地址: harbor_url2,
      'API 地址': harbor_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55948 : 点击Devops工具链-点击制品仓库-任意选择一个harbor进入详情页-点击绑定-选择项目-输入名称-输入描述-选择凭据-点击绑定账号-点击取消按钮', () => {
    // 绑定账号
    const testData = {
      工具链名称: harbor_name,
      项目名称: project_name,
      名称: bind_name1,
      描述: 'harbor集成详情绑定项目',
      凭据: harbor_secret,
    };
    page.detailPage.bind(testData);
    //分配仓库页面点击取消按钮
    page.detailPage.bindIntergratePage.cancelButton.click();

    // 绑定成功后跳转到集成详情页
    const expectData = {
      名称: bind_name1,
      项目名称: project_name,
      凭据: harbor_secret,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55949 : 点击Devops工具链-点击制品仓库-任意选择一个harbor进入详情页-点击绑定-选择项目-输入名称-添加凭据-点击绑定账号-选择镜像仓库-点击添加地址-点击分配仓库', () => {
    // 绑定账号
    const testData = {
      工具链名称: harbor_name,
      项目名称: project_name,
      名称: bind_name2,
      描述: 'harbor集成详情绑定项目并分配镜像仓库',
      凭据: harbor_secret,
      选择镜像repo: 'e2e-automation',
      点击添加地址: 'click',
    };
    page.detailPage.bind(testData);

    // 绑定成功后跳转到集成详情页
    const expectData = {
      名称: bind_name2,
      项目名称: project_name,
      凭据: harbor_secret,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55957 : 点击Devops工具链-点击制品仓库-选择一个有绑定的harbor点击进入详情页-任意选择一个未分配仓库的绑定账号-点击解除绑定按钮', () => {
    const testData = {
      项目名称: project_name,
      名称: bind_name1,
    };
    // 集成详情页的列表上解除绑定
    page.detailPage.unbind(harbor_name, testData);

    page.detailPage.isPresent(harbor_name).then(isPresent => {
      // 解绑成功后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-55955 : 点击Devops工具链-点击制品仓库-选择一个有绑定的harbor点击进入详情页-任意选择一个已分配仓库的绑定账号-点击解除绑定按钮', () => {
    const testData = {
      项目名称: project_name,
      名称: bind_name2,
    };
    // 集成详情页的列表上解除绑定
    page.detailPage.unAssignbind(harbor_name, testData);

    page.detailPage.isPresent(harbor_name).then(isPresent => {
      // 解绑成功后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-55956 : 点击Devops工具链-单击制品仓库-选择一个无绑定的harbor点击进入详情页-点击删除按钮', () => {
    //删除未绑定的harbor
    page.detailPage.delete(harbor_name1);

    // 删除成功后跳转到Devops工具链的首页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });

    page.listPage.isExist(harbor_name1).then(isExist => {
      expect(isExist).toBeFalsy();
    });
  });

  it('ACP2UI-55954 : 点击Devops工具链-点击制品仓库-选择一个有绑定的harbor点击进入详情页-点击删除按钮', () => {
    page.listPage.enterDetail(harbor_name);
    page.detailPage.menuTrigger.select('删除');
    //判断有删除按钮后点击取消
    page.detailPage.deleteDialog.buttonCancel.click();

    // 取消删除后页面还停留在详情页
    page.detailPage.isPresent(harbor_name).then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });
});
