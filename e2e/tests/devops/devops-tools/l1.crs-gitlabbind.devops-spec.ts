/**
 * Created by zhangjiao on 2019/6/17.
 * 需要搭建私有Gitlab,并且可用
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';

import { CommonKubectl } from '../../../utility/common.kubectl';
import { ProjectPage } from '@e2e/page_objects/devops_new/project/project.page';

describe('Devops工具链-GitlabBinding L1级别UI自动化case', () => {
  const page = new ProjectPage();
  const preparePage = page.devopsToolPage.preparePage;
  const project_name = page.projectName4;
  // gitlab相关参数
  const gitlab_name = ServerConf.GITLAB_NAME;
  const gitlab_repository = ServerConf.GITLAB_PUBLIC;
  const gitlabbind_name = page.getTestData('labbind');
  const gitlabbind_name2 = page.getTestData('labbind3');
  const labsecret_name = page.gitlabsecretPrivate4;
  const labsecret_name_error = page.harborsecretPrivate4;
  const gitlabbind_api1 = page.getTestData('labbind1');
  const gitlabbind_api2 = page.getTestData('labbind2');
  //let labbindIsTrue = false;

  beforeAll(() => {
    try {
      preparePage.deleteCodeRepoBinding(gitlabbind_name, project_name);
      preparePage.deleteCodeRepoBinding(gitlabbind_name2, project_name);
      preparePage.deleteCodeRepoBinding(gitlabbind_api1, project_name);
      preparePage.deleteCodeRepoBinding(gitlabbind_api2, project_name);
      browser.sleep(1000);

      // 绑定Gitlab，用于测试集成详情页删除绑定
      this.testdata5 = CommonKubectl.createResourceByTemplate(
        'devops.coderepobinding.yaml',
        {
          CODESERVICEBIND_NAME: gitlabbind_api1,
          NAMESPACE: project_name,
          CODESERVICE_NAME: gitlab_name,
          CODESERVICE_TYPE: 'gitlab',
          CODESERVICE_SECRET: labsecret_name,
          SECRET_NS: project_name,
          // ACCOUNT_NAME: 'root',
        },
        'l1.gitlabrepository.testdata5',
      );
      this.testdata6 = CommonKubectl.createResourceByTemplate(
        'alauda.coderepobinding-norepo.yaml',
        {
          CODESERVICEBIND_NAME: gitlabbind_api2,
          NAMESPACE: project_name,
          CODESERVICE_NAME: gitlab_name,
          CODESERVICE_TYPE: 'gitlab',
          CODESERVICE_SECRET: labsecret_name,
          SECRET_NS: project_name,
          ACCOUNT_NAME: 'root',
        },
        'l1.gitlabrepository.testdata6',
      );
    } catch (ex) {
      console.log('\n=====================');
      console.log(ex);
      console.log('=====================\n');
    }

    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    preparePage.deleteCodeRepoBinding(gitlabbind_name, project_name);
    preparePage.deleteCodeRepoBinding(gitlabbind_name2, project_name);
    preparePage.deleteCodeRepoBinding(gitlabbind_api1, project_name);
    preparePage.deleteCodeRepoBinding(gitlabbind_api2, project_name);
  });

  it('ACP2UI-57751 : Gitlab绑定-输入名称-搜素凭据-选择root凭据-绑定-勾root全部-所有仓库可以同步', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: gitlab_name,
      名称: gitlabbind_name,
      描述: 'auto bind gitlab service description22222!',
      凭据: labsecret_name,
      分配代码仓库: { 自动同步全部仓库: 'true' },
    };

    page.detailPage.bind(testData);

    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      凭据: labsecret_name,
      描述: 'auto bind gitlab service description22222!',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyProjectBind(expectData);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toBeGreaterThan(0);
    });
    browser.sleep(100);
  });

  it('ACP2UI-57753 : Gitlab绑定-输入名称-搜素凭据-选择root凭据-绑定-勾root部分仓库-部分仓库可以同步', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: gitlab_name,
      名称: gitlabbind_name2,
      描述: 'select 镜像仓库',
      凭据: labsecret_name,
      分配代码仓库: { 请选择仓库: [gitlab_repository] },
    };
    page.detailPage.bind(testData);

    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      凭据: labsecret_name,
      描述: 'select 镜像仓库',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyProjectBind(expectData);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toBeGreaterThan(0);
    });
    browser.sleep(100);
  });
  it('ACP2UI-57770 : Gitlab绑定-输入名称-选择错误的凭据-点击绑定账号-错误提醒', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: gitlab_name,
      名称: gitlabbind_name,
      描述: 'auto bind gitlab service description22222!',
      凭据: labsecret_name_error,
    };

    page.detailPage.fillBindInfo(testData);
    page.detailPage.bindIntergratePage.primaryButton.click();

    //由于Secret错误绑定失败
    page.detailPage.errorDialog.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
      // 等待6秒，错误框自动消失
      browser.sleep(6000);
    });
  });

  it('ACP2UI-57766 : 绑定详情-无代码仓库显示正常-点击分配仓库-可以分配仓库并同步', () => {
    const testData = {
      项目名称: project_name,
      名称: gitlabbind_api1,
      请选择仓库: [gitlab_repository],
    };
    page.detailPage.update_assignBind(testData);

    page.devopsToolPage.detailPage.bindTable.getRowCount().then(number => {
      expect(number).toBeGreaterThan(0);
    });
    browser.sleep(100);
  });

  it('ACP2UI-59194 : 未分配仓库Gitlab绑定详情-解除绑定-解绑成功', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: gitlab_name,
      名称: gitlabbind_name,
    };

    page.detailPage.unbind(testData);

    // 验证解绑成功
    page.detailPage.isBind(testData).then(isBind => {
      expect(isBind).toBeFalsy();
    });
  });
});
