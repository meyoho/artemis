/**
 * Created by zhangjiao on 2019/6/17.
 * 公有Github需要用到外网
 */

import { $, browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { DevopsBindPage } from '../../../page_objects/devops/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/devops/devops-tools/devopstools.page';
import { ProjectPage } from '../../../page_objects/devops/project/project.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

describe('Devops工具链-GithubBinding L1级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const page = new ProjectPage();
  // github相关参数
  let github_name = 'github';
  const github_host = 'https://api.github.com';
  const project_name = toolsPage.projectName4;

  const githubbind_name1 = bindPage.getTestData('hubbind1');
  const githubbind_name2 = bindPage.getTestData('hubbind2');
  const githubbind_name3 = bindPage.getTestData('hubbind3');
  const githubbind_name4 = bindPage.getTestData('hubbind4');
  const githubbind_nameerror = bindPage.getTestData('hubbinderror');

  const githubsecret_name = bindPage.getTestData('githubs1');
  const githubsecret_name_error = page.harborsecretPrivate4;
  const ispublic = ServerConf.IS_PUBLIC;

  beforeAll(() => {
    try {
      // 删除绑定
      page.deleteToolsBind('crb', githubbind_name1, project_name);
      page.deleteToolsBind('crb', githubbind_name2, project_name);
      page.deleteToolsBind('crb', githubbind_name3, project_name);
      page.deleteToolsBind('crb', githubbind_name4, project_name);
      page.deleteSecret(githubsecret_name, project_name);
      // 集成一个github
      const hub = CommonKubectl.getServiceNameByHost('crs', github_host);
      if (hub === 'false') {
        this.testdata2 = CommonKubectl.createResource(
          'alauda.codereposervice.yaml',
          {
            '${NAME}': github_name,
            '${CODEREPO_NAME}': github_name,
            '${CODEREPO_HOST}': github_host,
            '${TYPE}': 'Github',
            '${PUBLIC}': 'true',
          },
          'l1.githubrepository.testdata2',
        );
      } else {
        github_name = hub;
      }

      // 创建github的凭据
      const secret_data = {
        name: githubsecret_name,
        namespace: project_name,
        datatype: 'kubernetes.io/basic-auth',
        datas: {
          username: ServerConf.GITLAB_USER,
          password: ServerConf.GITHUB_TOKEN1,
        },
      };

      page.preparePage.createSecret(secret_data);
      browser.sleep(1000);
      // 绑定Github，用于测试集成详情页删除绑定
      this.testdata6 = CommonKubectl.createResourceByTemplate(
        'devops.coderepobinding.yaml',
        {
          CODESERVICEBIND_NAME: githubbind_name3,
          NAMESPACE: project_name,
          CODESERVICE_NAME: github_name,
          CODESERVICE_TYPE: 'github',
          CODESERVICE_SECRET: githubsecret_name,
          SECRET_NS: project_name,
          ACCOUNT_NAME: 'jiaozhang1',
        },
        'l1.githubrepository.testdata6',
      );
      this.testdata7 = CommonKubectl.createResourceByTemplate(
        'alauda.coderepobinding-norepo.yaml',
        {
          CODESERVICEBIND_NAME: githubbind_name4,
          NAMESPACE: project_name,
          CODESERVICE_NAME: github_name,
          CODESERVICE_TYPE: 'github',
          CODESERVICE_SECRET: githubsecret_name,
          SECRET_NS: project_name,
          ACCOUNT_NAME: 'jiaozhang1',
        },
        'l1.githubrepository.testdata7',
      );
    } catch (ex) {
      console.log('\n========================');
      console.log(ex);
      console.log('========================\n');
    }

    browser.refresh();
    toolsPage.login();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    page.waitProgressBarNotPresent();
    page.clickLeftNavByText('项目');
    page.waitProgressBarNotPresent();
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
  });

  afterAll(() => {
    // 删除绑定
    page.deleteToolsBind('crb', githubbind_name1, project_name);
    page.deleteToolsBind('crb', githubbind_name2, project_name);
    page.deleteToolsBind('crb', githubbind_name3, project_name);
    page.deleteToolsBind('crb', githubbind_name4, project_name);
    page.deleteSecret(githubsecret_name, project_name);
  });

  // 公有的Github，需要用到外网
  it('ACP2UI-52900 : 项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-并成功分配代码仓库', () => {
    if (ispublic === 'true') {
      try {
        CommonPage.waitElementTextChangeTo(
          page.detailPage_Content.getTitleText(),
          project_name,
        ); // 检查基本信息中项目名称是否正确
        page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

        expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
        page.detailPage_bindButton.click(); // 点击绑定按钮
        browser.sleep(500);
        bindPage.bindDialog_tools_type('代码仓库').click();
        expect(
          bindPage.bindDialog_tools_card(github_name).isPresent(),
        ).toBeTruthy();
        bindPage.bindDialog_tools_card(github_name).click();

        // 开始绑定账号-第一步
        const testData = {
          名称: githubbind_name1,
          描述: 'auto bind coderepo service description!',
          凭据: githubsecret_name,
        };
        const max_retries = 3;
        for (let i = 0; i < max_retries; ++i) {
          try {
            bindPage.bindPage_inputbox(testData);
            break;
          } catch (ex) {
            if (i + 1 == max_retries) throw ex;
            console.warn('bindPage_inputbox error occurs..., ' + ex);
          }
        }
        // 等待状态消失
        bindPage.waitElementNotPresent(
          $('div .status aui-icon'),
          '【代码仓库获取中...】的提示没有消失',
        );
        // 分配代码仓库-第二步

        bindPage.waitProgressBarNotPresent();
        browser.sleep(2000);
        bindPage.bind_secondStep_createButton.click();
        bindPage.waitProgressBarNotPresent();
        bindPage.toast.getMessage().then(message => {
          console.log(message);
        });

        // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
        CommonPage.waitElementTextChangeTo(
          bindPage.binddetailPage_Content.getTitleText(),
          githubbind_name1,
        ).then(isTextChangeTo => {
          if (!isTextChangeTo) {
            bindPage.bind_secondStep_createButton.click();
            bindPage.waitProgressBarNotPresent();
            bindPage.toast.getMessage().then(message => {
              console.log(message);
            });
          }
        });
        bindPage
          .getElementByText('类型')
          .getText()
          .then(text => {
            expect(text).toBe('类型 代码仓库 / Github 公有版');
          });

        bindPage.binddetailPage_Content
          .getElementByText('凭据')
          .getText()
          .then(text => {
            expect(text).toBe(githubsecret_name);
          });

        bindPage.binddetailPage_Content
          .getElementByText('描述')
          .getText()
          .then(text => {
            expect(text).toBe('auto bind coderepo service description!');
          });

        // 列表等已分配的代码仓库出现
        // bindPage
        //   .binddetailPage_assignTable_repoName(
        //     ServerConf.GITHUB_USER,
        //     'hello-world-java',
        //   )
        //   .checkTextAllIsPresent(0)
        //   .then(isPresent => {
        //     expect(isPresent).toBeTruthy();
        //   });
      } catch (ex) {
        console.warn('catch error..., ' + ex);
      }
    } else {
      console.log('私有环境,不执行该case');
    }
  });

  // 公有的Github，需要用到外网
  it('ACP2UI-52901 : 项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-分配仓库选择自动同步全部仓库-分配成功', () => {
    if (ispublic === 'true') {
      CommonPage.waitElementTextChangeTo(
        page.detailPage_Content.getTitleText(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

      expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
      page.detailPage_bindButton.click(); // 点击绑定按钮
      browser.sleep(500);
      bindPage.bindDialog_tools_type('代码仓库').click();
      expect(
        bindPage.bindDialog_tools_card(github_name).isPresent(),
      ).toBeTruthy();
      bindPage.bindDialog_tools_card(github_name).click();

      // 开始绑定账号-第一步
      const testData = {
        名称: githubbind_name2,
        描述: 'auto bind coderepo service description!',
        凭据: githubsecret_name,
      };
      bindPage.bindPage_inputbox(testData);
      // 分配代码仓库-第二步，勾选自动同步全部仓库
      expect(
        bindPage.bindCode_secondStep_autoCheckbox.isPresent(),
      ).toBeTruthy();
      bindPage.bindCode_secondStep_autoCheckbox.click();
      expect(bindPage.bind_secondStep_createButton.isPresent()).toBeTruthy();
      bindPage.bind_secondStep_createButton.click();
      // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content.getTitleText(),
        githubbind_name2,
      );
      bindPage
        .getElementByText('类型')
        .getText()
        .then(text => {
          expect(text).toBe('类型 代码仓库 / Github 公有版');
        });

      bindPage.binddetailPage_Content
        .getElementByText('凭据')
        .getText()
        .then(text => {
          expect(text).toBe(githubsecret_name);
        });

      bindPage.binddetailPage_Content
        .getElementByText('描述')
        .getText()
        .then(text => {
          expect(text).toBe('auto bind coderepo service description!');
        });
    } else {
      console.log('私有环境,不执行AldDevops-2436');
    }
  });

  it('ACP2UI-52902 : 项目详情-DevOps工具链-绑定Github-使用Token认证方式绑定-选择错误的凭据-分配仓库表单验证(异常)', () => {
    if (ispublic === 'true') {
      CommonPage.waitElementTextChangeTo(
        page.detailPage_Content.getTitleText(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签
      expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
      page.detailPage_bindButton.click(); // 点击绑定按钮
      browser.sleep(500);
      bindPage.bindDialog_tools_type('代码仓库').click();
      expect(
        bindPage.bindDialog_tools_card(github_name).isPresent(),
      ).toBeTruthy();
      bindPage.bindDialog_tools_card(github_name).click();

      // 开始绑定账号-第一步
      const testData = {
        名称: githubbind_nameerror,
        描述: 'auto bind coderepo service description!',
        凭据: githubsecret_name_error,
      };
      bindPage.bindPage_inputbox(testData);

      expect(
        bindPage.bindCode_firstStep_erroralert.checkTextIsPresent(),
      ).toBeTruthy();
      bindPage.bindPage_closeButton.click();
    } else {
      console.log('私有环境,不执行AldDevops-2438');
    }
  });

  it('ACP2UI-52903 : 项目详情-DevOps工具链-已绑定Github详情页-更新基本信息-更新成功', () => {
    if (ispublic === 'true') {
      CommonPage.waitElementTextChangeTo(
        page.detailPage_Content.getTitleText(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      page.detailPage_TabItem('DevOps 工具链').click();

      // 点击已经绑定过的一个代码仓库服务到详情页
      expect(
        bindPage.bindListPage_toolName(githubbind_name1).checkNameInListPage(),
      ).toBeTruthy();
      bindPage.bindListPage_toolName(githubbind_name1).clickNameInlistPage();

      // 先检查是否到详情页
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content.getTitleText(),
        githubbind_name1,
      );
      bindPage.binddetailPage_operate.click(); //点击操作
      expect(bindPage.binddetailPage_updateButton.isPresent()).toBeTruthy();
      bindPage.binddetailPage_updateButton.click(); // 点击更新操作

      // 更新dialog
      CommonPage.waitElementTextChangeTo(
        bindPage.bindUpdateDialog_name.getText(),
        githubbind_name1,
      );
      bindPage.bindUpdateDialog_desc.input(
        'new auto bind coderepo service description!',
      );
      bindPage.bindUpdateDialog_updateButton.click();
      // 更新成功后验证
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content.getElementByText('描述'),
        'new auto bind coderepo service description!',
      );
    } else {
      console.log('私有环境,不执行AldDevops-2423');
    }
  });

  it('ACP2UI-52904 : 项目详情-DevOps工具链-已绑定Github详情页-分配代码仓库-成功', () => {
    if (ispublic === 'true') {
      CommonPage.waitElementTextChangeTo(
        page.detailPage_Content.getTitleText(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

      // 点击已经绑定过的一个代码仓库服务到详情页
      expect(
        bindPage.bindListPage_toolName(githubbind_name1).checkNameInListPage(),
      ).toBeTruthy();
      bindPage.bindListPage_toolName(githubbind_name1).clickNameInlistPage();

      // 先检查是否到详情页
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content.getTitleText(),
        githubbind_name1,
      );
      expect(
        bindPage.bindCodedetailPage_assign_button.isPresent(),
      ).toBeTruthy();
      bindPage.click_bindCodedetailPage_assign_button(); //点击分配代码仓库按钮

      expect(
        bindPage.bindCode_assigndialog_accounts.checkTextAllIsPresent(0),
      ).toBeTruthy();
      bindPage.bindCode_assigndialog_selectcode('alauda').select('golang-http');
      bindPage.bindCode_assigndialog_title.click();
      expect(
        bindPage.bindCode_assigndialog_upateButton.isPresent(),
      ).toBeTruthy();
      bindPage.bindCode_assigndialog_upateButton.click();
      browser.sleep(2000);
    } else {
      console.log('私有环境,不执行AldDevops-2425');
    }
  });

  it('ACP2UI-52905 : 项目详情-DevOps工具链-已绑定Github详情页--解除绑定-成功', () => {
    if (ispublic === 'true') {
      CommonPage.waitElementTextChangeTo(
        page.detailPage_Content.getTitleText(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

      // 点击已经绑定过的一个代码仓库服务到详情页
      expect(
        bindPage.bindListPage_toolName(githubbind_name1).checkNameInListPage(),
      ).toBeTruthy();
      bindPage.bindListPage_toolName(githubbind_name1).clickNameInlistPage();

      // 先检查是否到详情页
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content.getTitleText(),
        githubbind_name1,
      );
      bindPage.binddetailPage_operate.click(); //点击操作
      expect(bindPage.binddetailPage_remove_button.isPresent()).toBeTruthy();
      bindPage.binddetailPage_remove_button.click(); // 点击解除绑定操作
      expect(bindPage.binddetailPage_remove_name.isPresent()).toBeTruthy();
      bindPage.binddetailPage_remove_name.input(githubbind_name1); // 输入绑定名称
      bindPage.binddetailPage_removedialog_remove.click();
    } else {
      console.log('私有环境,不执行AldDevops-2424');
    }
  });

  // 集成工具详情页解除绑定
  it('ACP2UI-58304 : 已集成Github详情页-选择一个已分配仓库的解除绑定-选择一个未分配仓库的绑定解除绑定', () => {
    if (ispublic === 'true') {
      toolsPage.navigationButton();
      expect(
        toolsPage.listPage_serviceName(github_name).checkNameInListPage(),
      ).toBeTruthy();
      toolsPage.listPage_serviceName(github_name).clickNameInlistPage();
      // 判断是否到详情页
      CommonPage.waitElementTextChangeTo(
        toolsPage.detailPage_Content.getTitleEle(),
        github_name,
      );
      toolsPage.detailPage_bind_table.clickOperationButtonByRow(
        [githubbind_name3],
        '解除绑定',
      );
      expect(bindPage.binddetailPage_remove_name.isPresent()).toBeTruthy();
      bindPage.binddetailPage_remove_name.input(githubbind_name3); // 输入绑定名称
      bindPage.binddetailPage_removedialog_remove.click();
      //   expect(bindPage.toast.getMessage()).toBe(`解绑成功`);
      // 解绑成功后留在当前集成详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(github_name);
      });

      //解除未分配仓库的绑定
      toolsPage.detailPage_bind_table.clickOperationButtonByRow(
        [githubbind_name4],
        '解除绑定',
      );
      expect(
        bindPage.binddetailPage_removedialog_remove1.isPresent(),
      ).toBeTruthy();
      bindPage.binddetailPage_removedialog_remove1.click(); // 点击确认框中的“解除绑定”
      //   expect(bindPage.toast.getMessage()).toBe(`解绑成功`);
      // 解绑成功后留在当前集成详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(github_name);
      });
    } else {
      console.log('内网环境,不执行AldDevops-2709');
    }
  });

  it('ACP2UI-58305 : 删除Github集成-已绑定过-输入Github集成名称删除成功', () => {
    if (ispublic === 'true') {
      toolsPage.navigationButton();
      expect(
        toolsPage.listPage_serviceName(github_name).checkNameInListPage(),
      ).toBeTruthy();
      toolsPage.listPage_serviceName(github_name).clickNameInlistPage();
      // 判断是否到详情页
      CommonPage.waitElementTextChangeTo(
        toolsPage.detailPage_Content.getTitleText(),
        github_name,
      );

      toolsPage.detailPage_svgButton.click();
      toolsPage.detailPage_operateButton('删除').click();
      expect(toolsPage.deleteConfirm_name.isPresent()).toBeTruthy();
      toolsPage.deleteConfirm_cancelButton1.click();
    } else {
      console.log('私有环境,不执行AldDevops-2524');
    }
  });
});
