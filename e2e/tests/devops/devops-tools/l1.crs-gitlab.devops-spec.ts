/**
 * Created by zhangjiao on 2019/6/17.
 * 需要搭建私有Gitlab,并且可用
 */

import { ServerConf } from '../../../config/serverConf';

import { DevopsToolPage } from '@e2e/page_objects/devops_new/devops-tools/devops.tools.page';
import { browser } from 'protractor';

describe('Devops工具链-Gitlab L1级别UI自动化case', () => {
  const page = new DevopsToolPage();

  const project_name = page.projectName4;
  const gitlab_secret = page.gitlabsecretPrivate4;
  // gitlab相关参数
  const gitlab_name = ServerConf.GITLAB_NAME;
  const gitlab_url = ServerConf.GITLAB_URL;
  const gitlab_name1 = page.getTestData('lab1');
  const gitlab_url1 = 'https://api.' + gitlab_name1 + '1.com';
  const gitlab_url2 = 'https://api.' + gitlab_name1 + '2.com';
  const bind_name = 'ui-bind-gitlab11';
  beforeAll(() => {
    page.preparePage.deleteCodeRepoService(gitlab_name1);
    page.preparePage.deleteCodeRepoBinding(bind_name, project_name);

    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    page.preparePage.deleteCodeRepoService(gitlab_name1);
    page.preparePage.deleteCodeRepoBinding(bind_name, project_name);
  });

  it('ACP2UI-56111 : Gitlab集成-输入名称-输入不正确的API地址-点击集成按钮-集成成功-》有感叹号提示', () => {
    const testData = {
      集成: 'Gitlab 私有版',
      名称: gitlab_name1,
      访问地址: gitlab_url1,
      'API 地址': gitlab_url1,
    };
    page.intergratePage.integrate(testData);

    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      访问地址: gitlab_url1,
      'API 地址': gitlab_url1,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-57745 : Gitlab集成-输入正确信息-取消-检查集成没有被创建', () => {
    // 类型1：取消删除未绑定的jenkins集成
    const testData = {
      集成: 'Gitlab 私有版',
      名称: 'gitlab1111',
      访问地址: 'https://gitlab.test',
      'API 地址': 'https://gitlab.test',
    };
    page.intergratePage.fillForm(testData);
    page.intergratePage.intergrateDialog.clickCancelButton();

    //验证取消集成后 集成按钮在列表页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-59191 : gitlab集成-更新-更新api地址-api地址被更新', () => {
    const testData = {
      工具链名称: gitlab_name1,
      'API 地址': gitlab_url2,
      访问地址: gitlab_url2,
    };
    page.detailPage.updateBasicInfo(testData);

    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      访问地址: gitlab_url2,
      'API 地址': gitlab_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-56490 : Gitlab集成-已存在名称-已存在API地址-前后端校验', () => {
    const testData = {
      工具链名称: gitlab_name1,
      'API 地址': gitlab_url, // 第一次改地址：API地址改成已存在的地址
      访问地址: gitlab_url,
    };
    page.detailPage.fillUpdateBasicInfo(testData);
    page.detailPage.updateDialog.clickUpdateButton();
    page.detailPage.errorDialog.isPresent().then(isPresent => {
      // 验证更新失败弹窗
      expect(isPresent).toBeTruthy();

      // 输出错误信息
      page.detailPage.errorDialog.message.getText().then(text => {
        console.log(text);
      });
    });

    // 等待错误弹框消失
    browser.sleep(6000);

    // 点击取消按钮
    page.detailPage.updateDialog.clickCancelButton();

    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      访问地址: gitlab_url2,
      'API 地址': gitlab_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-59192 : gitlab集成-更新-更新api地址-取消', () => {
    const testData = {
      工具链名称: gitlab_name1,
      访问地址: 'https://update.a.a.a.a', // 修改服务地址
    };
    page.detailPage.fillUpdateBasicInfo(testData);

    // 点击取消按钮
    page.detailPage.updateDialog.clickCancelButton();

    //取消更新后页面还在详情页
    const expectData = {
      类型: '代码仓库 / Gitlab 私有版',
      访问地址: gitlab_url2,
      'API 地址': gitlab_url2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-59193 : gitlab绑定过程点击取消回到集成详情页', () => {
    // 绑定账号
    const testData = {
      工具链名称: gitlab_name,
      项目名称: project_name,
      名称: bind_name,
      描述: 'a jenkins binding',
      凭据: gitlab_secret,
    };
    page.detailPage.bind(testData);
    browser.sleep(3000);
    //分配仓库页面点击取消按钮
    page.detailPage.bindIntergratePage.cancelButton.click();

    // 绑定成功后跳转到集成详情页
    const expectData = {
      名称: bind_name,
      项目名称: project_name,
      凭据: gitlab_secret,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-57747 : Gitlab集成详情-解绑未分配仓库绑定-输入确认信息-解绑成功', () => {
    const testData = {
      项目名称: project_name,
      名称: bind_name,
    };
    // 未分配仓库的绑定-解除绑定
    // 集成详情页的列表上解除绑定
    page.detailPage.unbind(gitlab_name, testData);

    page.detailPage.isPresent(gitlab_name).then(isPresent => {
      // 解绑成功后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-57763 : Gitlab集成-删除取消-工具不会被删除', () => {
    //只判断弹框，不删除
    page.listPage.enterDetail(gitlab_name);
    page.detailPage.menuTrigger.select('删除');
    // 单击取消按钮
    page.detailPage.deleteDialog.buttonCancel.click();

    page.detailPage.isPresent(gitlab_name).then(isPresent => {
      // 取消后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-57742 : Gitlab集成-不存在绑定-直接删除', () => {
    //删除
    page.detailPage.delete(gitlab_name1);

    //删除成功后，判断是否存在
    page.listPage.isExist(gitlab_name1).then(isExist => {
      expect(isExist).toBeFalsy();
    });
  });
});
