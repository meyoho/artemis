/**
 * Created by zhangjiao on 2019/6/14.
 * 任何环境都可以
 */

import { ServerConf } from '../../../config/serverConf';
import { DevopsToolPage } from '@e2e/page_objects/devops_new/devops-tools/devops.tools.page';

describe('Devops工具链-Jenkins L1级别UI自动化case', () => {
  const page = new DevopsToolPage();
  const project_name = page.projectName4;
  const jenkins_name = ServerConf.JENKINS_NAME;
  const jenkins_host = ServerConf.JENKINS_HOST;
  const jenkins_name1 = page.getTestData('jenkinsl1');
  const jenkins_host3 = 'http://' + jenkins_name1 + '3.com';
  const jsecret_private = page.jenkinssecretPrivate4;
  const bind_name = 'jenkins-uibinding';
  beforeAll(() => {
    page.preparePage.deleteJenkins(jenkins_name1);
    page.preparePage.deleteJenkinsBinding(bind_name, project_name);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    page.preparePage.deleteJenkins(jenkins_name1);
    page.preparePage.deleteJenkinsBinding(bind_name, project_name);
  });

  it('ACP2UI-58284 : 点击Devops工具链-点击集成-点击持续集成-点击Jenkins-点击集成按钮', () => {
    const testData = {
      集成: 'Jenkins 私有版',
    };
    page.intergratePage.fillForm(testData);
    page.intergratePage.intergrateDialog.clickIntegrateButton(); //点击集成按钮

    //API地址输入框提示必填项不能为空
    page.waitElementTextChangeTo(
      page.intergratePage.integrate_input_content.getHintByText('API 地址'),
      '必填项不能为空',
    );
    const message = page.intergratePage.integrate_input_content
      .getHintByText('API 地址')
      .getText();
    message.then(text => {
      expect(text).toBe('必填项不能为空');
    });
    //点击集成页面取消按钮
    page.intergratePage.intergrateDialog.clickCancelButton();
    //验证取消集成后 集成按钮在列表页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-57778 : jenkins集成-输入名称-api地址-取消', () => {
    const testData = {
      集成: 'Jenkins 私有版',
      名称: 'test1111',
      访问地址: 'https://12.12.12.12',
      'API 地址': 'https://12.12.12.12',
    };
    page.intergratePage.fillForm(testData);
    page.intergratePage.intergrateDialog.clickCancelButton();

    //验证取消集成后 集成按钮在列表页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-58285 : 更新jenkins集成 -更新api地址-取消', () => {
    const testData = {
      工具链名称: jenkins_name,
      访问地址: jenkins_host3, // 修改服务地址
    };
    page.detailPage.fillUpdateBasicInfo(testData);

    // 点击取消按钮
    page.detailPage.updateDialog.clickCancelButton();

    //取消更新后页面还在详情页
    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      'API 地址': jenkins_host,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-57780 : jenkins绑定-jenkins集成详情页-绑定-选择项目-输入绑定名称-选择凭据-绑定账号', () => {
    const testData = {
      工具链名称: jenkins_name,
      项目名称: project_name,
      名称: bind_name,
      描述: 'a jenkins binding',
      凭据: jsecret_private,
    };
    page.detailPage.bind(testData);

    // 绑定成功后跳转到集成详情页
    const expectData = {
      名称: bind_name,
      项目名称: project_name,
      凭据: jsecret_private,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-58287 : jenkins解绑取消-jenkins集成详情页-解除绑定-取消', () => {
    const testData = {
      项目名称: project_name,
      名称: bind_name,
    };
    // 未分配仓库的绑定-解除绑定
    page.detailPage.unbind_cancel(jenkins_name, testData);

    page.detailPage.isPresent(jenkins_name).then(isPresent => {
      // 解绑成功后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-57793 : jenkins解绑-jenkins集成详情页-解除绑定', () => {
    const testData = {
      项目名称: project_name,
      名称: bind_name,
    };
    // 未分配仓库的绑定-解除绑定
    page.detailPage.unbind(jenkins_name, testData);

    page.detailPage.isPresent(jenkins_name).then(isPresent => {
      // 解绑成功后留在当前集成详情页
      expect(isPresent).toBeTruthy();
    });
  });
});
