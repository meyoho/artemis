/**
 * Created by wangyanzhao on 2019/12/24.
 * 任何环境都可以
 */
import { ServerConf } from '../../../config/serverConf';
import { DevopsToolPage } from '@e2e/page_objects/devops_new/devops-tools/devops.tools.page';

describe('Devops工具链-Jenkins L0级别UI自动化case', () => {
  const page = new DevopsToolPage();

  const jenkins_name = ServerConf.JENKINS_NAME;
  const jenkins_name1 = page.getTestData('jenkinsl1');
  const jenkins_host1 = 'http://' + jenkins_name1 + '1.com';
  const jenkins_host2 = 'http://' + jenkins_name1 + '2.com';
  beforeAll(() => {
    page.preparePage.deleteJenkins(jenkins_name1);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    page.preparePage.deleteJenkins(jenkins_name1);
  });

  it('ACP2UI-57774 : jenkins集成-输入名称-输入不可访问的api地址-集成', () => {
    const testData = {
      集成: 'Jenkins 私有版',
      名称: jenkins_name1,
      访问地址: jenkins_host1,
      'API 地址': jenkins_host1,
    };
    page.intergratePage.integrate(testData);

    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      访问地址: jenkins_host1,
      'API 地址': jenkins_host1,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-57777 : 更新jenkins集成 -更新访问地址和api地址', () => {
    const testData = {
      工具链名称: jenkins_name1,
      'API 地址': jenkins_host2,
      访问地址: jenkins_host2,
    };
    page.detailPage.updateBasicInfo(testData);

    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      访问地址: jenkins_host2,
      'API 地址': jenkins_host2,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-58286 : jenkins集成-删除jenkins集成-取消', () => {
    // 类型1：取消删除未绑定的jenkins集成
    page.listPage.enterDetail(jenkins_name1);
    page.detailPage.menuTrigger.select('删除');
    page.detailPage.confirmDialog.buttonCancel.click();

    // 取消删除后页面还停留在详情页
    page.detailPage.isPresent(jenkins_name1).then(isPresent => {
      expect(isPresent).toBeTruthy();
    });

    //类型2：取消删除已绑定的jenkins集成
    page.listPage.enterDetail(jenkins_name);
    page.detailPage.menuTrigger.select('删除');
    page.detailPage.deleteDialog.buttonCancel.click();

    // 取消删除后页面还停留在详情页
    page.detailPage.isPresent(jenkins_name).then(isPresent => {
      expect(isPresent).toBeTruthy();
    });
  });

  it('ACP2UI-57779 : jenkins集成-删除jenkins集成', () => {
    // 类型1：删除未绑定的jenkins集成
    page.detailPage.delete(jenkins_name1);

    // 删除成功后跳转到Devops工具链的首页
    page.listPage.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
    });

    page.listPage.isExist(jenkins_name1).then(isExist => {
      expect(isExist).toBeFalsy();
    });
  });
});
