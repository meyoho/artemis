/**
 * Created by zhangjiao on 2019/6/14.
 */

import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { ProjectPage } from '@e2e/page_objects/devops_new/project/project.page';

describe('Devops工具链-Jenkins L1级别UI自动化case', () => {
  const page = new ProjectPage();
  const preparePage = page.devopsToolPage.preparePage;
  const project_name = page.projectName4;

  const jenkins_name = ServerConf.JENKINS_NAME;
  const jsecret_private = page.jenkinssecretPrivate4;
  const jsecret_global = page.jenkinssecretGlobal;
  const jsecret_error = page.gitlabsecretPrivate4;
  const jenkinsbind_name1 = page.getTestData('jenkinsbind1');
  const jenkinsbind_name2 = page.getTestData('jenkinsbind2');
  // const jenkins_user = ServerConf.JENKINS_USER;
  // const jenkins_pwd = ServerConf.JENKINS_PWD;
  beforeAll(() => {
    preparePage.deleteJenkinsBinding(jenkinsbind_name1, project_name);
    preparePage.deleteJenkinsBinding(jenkinsbind_name2, project_name);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    preparePage.deleteJenkinsBinding(jenkinsbind_name1, project_name);
    preparePage.deleteJenkinsBinding(jenkinsbind_name2, project_name);
  });

  it('ACP2UI-57782 : jenkins绑定-项目-devops工具链-绑定-输入绑定名称-描述-选择项目下凭据-绑定账号', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name1,
      描述: 'create a jenkinsbinding in project4!',
      凭据: jsecret_private,
    };

    page.detailPage.bind(testData);

    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      凭据: jsecret_private,
      描述: 'create a jenkinsbinding in project4!',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyProjectBind(expectData);
  });

  it('ACP2UI-57786 : jenkins绑定-项目-devops工具链-绑定-选择jenkins-输入名称-选择公共凭据-绑定账号', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name2,
      描述: 'jenkins detail page binding!',
      凭据: jsecret_global,
    };

    page.detailPage.bind(testData);

    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      凭据: jsecret_global,
      描述: 'jenkins detail page binding!',
    };

    // 绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyProjectBind(expectData);
  });

  it('ACP2UI-57785 : jenkins绑定-项目-devops工具链-绑定-输入绑定名称-选择错误凭据-绑定账号', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name2,
      描述: 'jenkins bingding error',
      凭据: jsecret_error,
    };

    page.detailPage.fillBindInfo(testData);
    page.detailPage.bindIntergratePage.primaryButton.click();

    //由于Secret错误绑定失败
    page.detailPage.errorDialog.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy();
      // 等待6秒，错误框自动消失
      browser.sleep(6000);
    });
  });

  it('ACP2UI-57789 : jenkins绑定更新-jenkins集成详情页-绑定详情页-更新-更新描述和凭据', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name1,
      描述: 'update binding',
      凭据: jsecret_global,
    };
    page.detailPage.updateBind(testData);
    browser.sleep(1000);

    const expectData = {
      类型: '持续集成 / Jenkins 私有版',
      描述: 'update binding',
      凭据: jsecret_global,
    };

    // 更新绑定成功后到项目详情-绑定代码仓库详情页-基本信息检查
    page.devopsToolPage.detailVerifyPage.verifyProjectBind(expectData);
  });

  it('ACP2UI-58293 : jenkins解绑-jenkins绑定详情页-解除绑定-取消', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name2,
    };
    //点击取消绑定按钮
    page.detailPage.unbind2_cancel(testData);

    // 验证还在绑定详情页
    page.detailPage.isBind(testData).then(isBind => {
      expect(isBind).toBeTruthy();
    });
  });

  it('ACP2UI-57794 : jenkins解绑-jenkins绑定详情页-解除绑定', () => {
    const testData = {
      项目名称: project_name,
      工具链名称: jenkins_name,
      名称: jenkinsbind_name2,
    };

    page.detailPage.unbind2(testData);

    // 验证解绑成功
    page.detailPage.isBind(testData).then(isBind => {
      expect(isBind).toBeFalsy();
    });
  });
});
