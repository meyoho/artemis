/**
 * Created by zhangjiao on 2019/6/17.
 * 需要部署dockerhubregistry
 */

import { $ } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { DevopsBindPage } from '../../../page_objects/devops/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/devops/devops-tools/devopstools.page';
import { ProjectPage } from '../../../page_objects/devops/project/project.page';
import { CommonKubectl } from '../../../utility/common.kubectl';
import { CommonPage } from '../../../utility/common.page';

describe('Devops工具链-DockerHub Registry L1级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const projectPage = new ProjectPage();
  const project_name = toolsPage.projectName1;

  let dockerhub_name = 'dockerhub-registry';
  const dockerhub_host = 'https://hub.docker.com';
  const dockerhubsecret_name = toolsPage.getTestData('dockerhubsecret1');

  let dockerhubbind_name1 = toolsPage.getTestData('registrybind1');
  dockerhubbind_name1 = dockerhubbind_name1.substring(
    dockerhubbind_name1.length - 30,
    dockerhubbind_name1.length,
  );
  const dockerhub_repo = 'jiaozhang';
  const ispublic = ServerConf.IS_PUBLIC;
  beforeAll(() => {
    // 删除绑定
    CommonKubectl.execKubectlCommand(
      `kubectl delete ImageRegistryBinding -n ${project_name} ${dockerhubbind_name1}`,
    );

    const registry = CommonKubectl.getServiceNameByHost(
      'imageregistry',
      dockerhub_host,
    );
    if (registry === 'false') {
      this.testdata1 = CommonKubectl.createResourceByTemplate(
        'alauda.imageregistry.yaml',
        {
          NAME: dockerhub_name,
          REGISTRY_NAME: dockerhub_name,
          REGISTRY_HOST: dockerhub_host,
          REGISTRY_TYPE: 'DockerHub',
        },
        'L1.dockerregistry.testdata1',
      );
    } else {
      dockerhub_name = registry;
    }
    // 创建dockerhub的凭据

    const secret_data = {
      name: dockerhubsecret_name,
      namespace: project_name,
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: 'jiaozhang',
        password: 'ZhangJiao123',
      },
    };
    toolsPage.preparePage.createSecret(secret_data);

    toolsPage.login();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.navigationButton();
  });

  afterAll(() => {
    // 删除绑定
    CommonKubectl.execKubectlCommand(
      `kubectl delete ImageRegistryBinding -n ${project_name} ${dockerhubbind_name1}`,
    );
  });

  it('ACP2UI-58306 : 集成-制品仓库-DockerHub Registry-点击<绑定>打开选择项目弹框', () => {
    if (ispublic === 'true') {
      expect(
        toolsPage.listPage_serviceName(dockerhub_name).checkNameInListPage(),
      ).toBeTruthy();
      toolsPage.listPage_serviceName(dockerhub_name).clickNameInlistPage();
      // 判断是否到详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(dockerhub_name);
      });

      toolsPage.detailPage_bindButton.click();
      toolsPage.selectProjectTable.searchByResourceName(project_name, 1); // 在搜索框输入项目名称
      toolsPage.selectProject_project(project_name).click();
      // // 到绑定页，开始绑定账号-第一步
      const testData = {
        名称: dockerhubbind_name1,
        描述: 'auto bind registry service description1111!!!',
        凭据: dockerhubsecret_name,
      };
      bindPage.bindPage_inputbox(testData);
      // 分配镜像仓库-第二步
      expect(bindPage.bind_secondStep_createButton.isPresent()).toBeTruthy();
      bindPage.bind_secondStep_createButton.click();
      //   expect(bindPage.toast.getMessage()).toBe(`分配仓库成功`);
      // 在集成详情页绑定成功后停留在集成详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(dockerhub_name);
      });
    } else {
      console.log('私有环境,不执行该case');
    }
  });
  it('ACP2UI-58307 : 集成详情页-点击已绑定DockerHub Registry-未分配仓库-解除绑定-成功', () => {
    if (ispublic === 'true') {
      expect(
        toolsPage.listPage_serviceName(dockerhub_name).checkNameInListPage(),
      ).toBeTruthy();
      toolsPage.listPage_serviceName(dockerhub_name).clickNameInlistPage();
      // 判断是否到详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(dockerhub_name);
      });
      // 解除绑定
      toolsPage.detailPage_bind_table.clickOperationButtonByRow(
        [dockerhubbind_name1],
        '解除绑定',
      );
      expect(
        bindPage.binddetailPage_removedialog_remove1.isPresent(),
      ).toBeTruthy();
      bindPage.binddetailPage_removedialog_remove1.click(); // 点击确认框中的“解除绑定”
      // 解绑成功后留在当前集成详情页
      toolsPage.detailPage_Content.getTitleText().then(text => {
        expect(text).toBe(dockerhub_name);
      });

      // // 点击已经绑定过的一个ACE Registry到详情页
      // toolsPage.detailPage_bind_table.clickResourceNameByRow([
      //   dockerhubbind_name1,
      // ]);
      // // 先检查是否到详情页
      // bindPage.binddetailPage_Content1.getTitleText().then(text => {
      //   expect(text).toBe(dockerhubbind_name1);
      // });

      // bindPage.binddetailPage_operate.click(); //点击操作
      // expect(bindPage.binddetailPage_remove_button.isPresent()).toBeTruthy();
      // bindPage.binddetailPage_remove_button.click(); // 点击解除绑定操作
      // expect(
      //   bindPage.binddetailPage_removedialog_remove1.isPresent(),
      // ).toBeTruthy();
      // bindPage.binddetailPage_removedialog_remove1.click(); // 点击确认框中的“解除绑定”
      // //   expect(bindPage.toast.getMessage()).toBe(`解绑成功`);
      // // 解除绑定后跳转到项目详情页
      // expect(projectPage.detailPage_Content.getTitleText()).toBeTruthy(
      //   project_name,
      // );
    } else {
      console.log('私有环境,不执行该case');
    }
  });

  it('ACP2UI-58308 : 项目详情-DevOps工具链-绑定DockerHub Registry-输入正确的绑定账号-成功分配镜像仓库', () => {
    if (ispublic === 'true') {
      projectPage.clickLeftNavByText('项目');
      projectPage.enterProject(project_name); // 在列表页点击项目名称到项目详情页
      CommonPage.waitElementTextChangeTo(
        projectPage.detailPage_Content.getTitleEle(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签
      expect(projectPage.detailPage_bindButton.isPresent()).toBeTruthy();
      projectPage.detailPage_bindButton.click(); // 点击绑定按钮
      bindPage.bindDialog_tools_type('制品仓库').click();
      expect(
        bindPage.bindDialog_tools_card(dockerhub_name).isPresent(),
      ).toBeTruthy();
      bindPage.bindDialog_tools_card(dockerhub_name).click();

      // 开始绑定账号-第一步
      const testData = {
        名称: dockerhubbind_name1,
        描述: 'auto bind registry service description1111!!!',
        凭据: dockerhubsecret_name,
      };
      bindPage.bindPage_inputbox(testData);

      // 分配镜像仓库-第二步
      expect(bindPage.bind_secondStep_createButton.isPresent()).toBeTruthy();
      // 选择仓库
      const testData1 = {
        选择镜像repo: dockerhub_repo,
        点击添加地址: 'click',
      };
      bindPage.bindPage_sencondinput(testData1);
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content1.getTitleEle(),
        dockerhubbind_name1,
      );
      bindPage.binddetailPage_Content1
        .getElementByText('集成名称')
        .getText()
        .then(text => {
          expect(text).toBe('dockerhub-registry');
        });

      bindPage.binddetailPage_Content1
        .getElementByText('凭据')
        .getText()
        .then(text => {
          expect(text).toBe(dockerhubsecret_name);
        });

      bindPage
        .getElementByText_harbor('类型：')
        .getText()
        .then(text => {
          expect(text).toContain('制品仓库 / DockerHub 公有版');
        });

      const child = $('.name-cell__name a');
      expect(child.isPresent());
    } else {
      console.log('私有环境,不执行该case');
    }
  });

  it('ACP2UI-58309 : 项目详情-DevOps工具链-已绑定DockerHub Registry详情页-已分配仓库-解除绑定-输入绑定名称-成功', () => {
    if (ispublic === 'true') {
      projectPage.clickLeftNavByText('项目');
      projectPage.enterProject(project_name); // 在列表页点击项目名称到项目详情页
      CommonPage.waitElementTextChangeTo(
        projectPage.detailPage_Content.getTitleEle(),
        project_name,
      ); // 检查基本信息中项目名称是否正确
      projectPage.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

      // 点击已经绑定过的一个DockerHub Registry到详情页
      bindPage.bindListPage_toolName(dockerhubbind_name1).clickNameInlistPage();

      // 先检查是否到详情页
      CommonPage.waitElementTextChangeTo(
        bindPage.binddetailPage_Content1.getTitleEle(),
        dockerhubbind_name1,
      );
      bindPage.binddetailPage_operate.click(); //点击操作
      expect(bindPage.binddetailPage_remove_button.isPresent()).toBeTruthy();
      bindPage.binddetailPage_remove_button.click(); // 点击解除绑定操作
      expect(bindPage.binddetailPage_remove_name.isPresent()).toBeTruthy();
      bindPage.binddetailPage_remove_name.input(dockerhubbind_name1); // 输入绑定名称
      bindPage.binddetailPage_removedialog_remove.click();
      //   expect(bindPage.toast.getMessage()).toBe(`解绑成功`);
      // 解除绑定后跳转到项目详情页
      expect(projectPage.detailPage_Content.getTitleText()).toBeTruthy(
        project_name,
      );
    } else {
      console.log('私有环境,不执行该case');
    }
  });
});
