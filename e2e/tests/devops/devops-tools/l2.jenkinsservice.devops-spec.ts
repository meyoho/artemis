/**
 * Created by zhangjiao on 2019/6/17.
 * 任何环境都可以
 */

import { DevopsBindPage } from '../../../page_objects/devops/devops-tools/devopsbind.page';
import { DevopsToolsPage } from '../../../page_objects/devops/devops-tools/devopstools.page';
import { ProjectPage } from '../../../page_objects/devops/project/project.page';
import { SecretPage } from '../../../page_objects/devops/secret/secret.page';
import { CommonApi } from '../../../utility/common.api';
import { CommonPage } from '../../../utility/common.page';
import {
  alauda_type_jenkins,
  alauda_type_jenkinsbinding,
} from '../../../utility/resource.type.k8s';
import { CommonKubectl } from '@e2e/utility/common.kubectl';

describe('Devops工具链-Jenkins L2级别UI自动化case', () => {
  const toolsPage = new DevopsToolsPage();
  const bindPage = new DevopsBindPage();
  const page = new ProjectPage();
  const secretPage = new SecretPage();
  const project_name = toolsPage.projectName4;
  const jenkins_name = toolsPage.getTestData('jenkinsl2');
  const jenkins_host = 'https://' + jenkins_name + '.com'; // Jenkins服务地址
  const jenkinsbind_name = toolsPage.getTestData('jenkinsbind1');
  const jsecret_name = toolsPage.getTestData('jsecret3');

  beforeAll(() => {
    CommonApi.deleteResource(jenkins_name, alauda_type_jenkins, null);
    page.deleteToolsBind('JenkinsBinding', jenkinsbind_name, project_name);
    page.deleteSecret(jsecret_name, project_name);
    // 创建Jenkins的Secret
    const secret_data = {
      name: jsecret_name,
      namespace: project_name,
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: 'test',
        password: 'test',
      },
    };
    page.preparePage.createSecret(secret_data);

    CommonKubectl.createResourceByTemplate(
      'alauda.jenkins.yaml',
      {
        JENKINS_NAME: jenkins_name,
        JENKINS_HOST: jenkins_host,
      },
      page.randomGetTestData(alauda_type_jenkins),
    );
    CommonKubectl.createResourceByTemplate(
      'alauda.jenkinsbinding.yaml',
      {
        JENKINSBIND_NAME: jenkinsbind_name,
        JENKINS_NAME: jenkins_name,
        PROJECT_NAME: project_name,
        JENKINSSECRET_NAME: jsecret_name,
        SECRET_NS: project_name,
      },
      page.randomGetTestData(alauda_type_jenkinsbinding),
    );
    toolsPage.login();
    toolsPage.enterAdminDashboard();
  });

  beforeEach(() => {
    toolsPage.navigationButton();
  });

  afterAll(() => {
    CommonApi.deleteResource(jenkins_name, alauda_type_jenkins, null);
    page.deleteToolsBind('JenkinsBinding', jenkinsbind_name, project_name);
    page.deleteSecret(jsecret_name, project_name);
  });

  it('ACP2UI-57775 : jenkins集成-名称校验', () => {
    expect(toolsPage.listPage_createButton.isPresent()).toBeTruthy();
    toolsPage.listPage_createButton.click();
    toolsPage.createDialog_tools_type('持续集成').click();
    toolsPage.createDialog_tools_card('Jenkins 私有版').click();

    toolsPage.createDialog_createButton.click(); // 点击集成按钮
    CommonPage.waitElementTextChangeTo(
      toolsPage.getHintByText('API 地址'),
      '必填项不能为空',
    );
    toolsPage
      .getHintByText('API 地址')
      .getText()
      .then(text => {
        expect(text).toBe('必填项不能为空');
      });

    toolsPage.inputByText('名称', 'DAXIEZIMU'); // 输入不合法的服务名称
    toolsPage
      .getHintByText('名称')
      .getText()
      .then(text => {
        expect(text).toBe('以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -');
      });

    toolsPage.inputByText('名称', 'aaa_aaa'); // 输入不合法的服务名称
    toolsPage
      .getHintByText('名称')
      .getText()
      .then(text => {
        expect(text).toBe('以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -');
      });

    toolsPage.inputByText('API 地址', 'aaa_aaa'); // 输入不合法的API地址
    toolsPage
      .getHintByText('API 地址')
      .getText()
      .then(text => {
        expect(text).toBe(
          '请输入以 "http://" 或 "https://" 开头的域名或 IP 地址',
        );
      });

    toolsPage.inputByText('名称', jenkins_name); // 输入合法的服务名称
    toolsPage.inputByText('API 地址', jenkins_host); // 输入合法的API地址
    toolsPage.createDialog_cancelButton.click(); // 点击取消关闭当前弹窗
  });

  it('ACP2UI-58300 : jenkins绑定-绑定详情页-表单合法验证', () => {
    page.navigationButton();
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleText(),
      project_name,
    ); // 检查基本信息中项目名称是否正确
    page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的服务标签

    expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
    page.detailPage_bindButton.click(); // 点击绑定按钮
    bindPage.bindDialog_tools_type('持续集成').click();
    bindPage.bindDialog_tools_card(jenkins_name).click();

    expect(bindPage.bindJenkinsDialog_bindButton.isPresent()).toBeTruthy();
    bindPage.bindJenkinsDialog_bindButton.click(); //点击“绑定账号”按钮，验证必填项
    bindPage.bindJenkins_inputcontent
      .getHintByText('名称')
      .getText()
      .then(text => {
        expect(text).toBe('必填项不能为空');
      });

    bindPage.bindJenkins_inputcontent
      .getHintByText('凭据')
      .getText()
      .then(text => {
        expect(text).toBe('必填项不能为空');
      });
  });

  it('ACP2UI-58301 : Jenkins服务地址错误-绑定-绑定失败', () => {
    page.navigationButton();
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('DevOps 工具链').click();
    expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
    page.detailPage_bindButton.click(); // 点击绑定按钮
    expect(bindPage.bindJenkins_inputcontent.getTitleText()).toContain('绑定');

    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name).isPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name).click();
    // 检查是否到绑定页
    expect(bindPage.bindPage_header_text.getText()).toContain('绑定');
    const testData = {
      名称: jenkinsbind_name,
      描述: jenkinsbind_name,
      凭据: jsecret_name,
    };
    bindPage.bindJenkins_inputbox(testData);
    //由于Jenkins地址错误绑定失败
    expect(bindPage.bindJenkins_erroralert.checkTextIsPresent()).toBeTruthy();
    bindPage.bindPage_closeButton.click();
  });

  xit('ACP2UI-58302 : 绑定详情页-检查绑定信息链接跳转正确', () => {
    expect(
      toolsPage.listPage_serviceName(jenkins_name).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getTitleText(),
      jenkins_name,
    );
    toolsPage.resourceTable.getRowCount().then(count => {
      expect(count).toBe(1);
    });

    toolsPage.detailPage_acount_table
      .getCell('项目', [project_name])
      .then(function(elem) {
        CommonPage.waitElementPresent(elem);
        elem.click();
        CommonPage.waitElementTextChangeTo(
          page.detailPage_Content.getTitleText(),
          project_name,
        );
      });
    toolsPage.navigationButton(); // 再返回到集成详情页
    expect(
      toolsPage.listPage_serviceName(jenkins_name).checkNameInListPage(),
    ).toBeTruthy();
    toolsPage.listPage_serviceName(jenkins_name).clickNameInlistPage(); // 在列表页点击一个jenkins name到详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      toolsPage.detailPage_Content.getTitleText(),
      jenkins_name,
    );
    toolsPage.resourceTable.getRowCount().then(count => {
      expect(count).toBe(1);
    });

    toolsPage.detailPage_acount_table
      .getCell('凭据', [jsecret_name])
      .then(function(elem) {
        CommonPage.waitElementPresent(elem);
        elem.click();
        secretPage.detailPage_Content.getTitleText().then(text => {
          expect(text).toBe(jsecret_name);
        });
      });
  });

  /**
   * 在"绑定Jenkins服务"对话框中点击取消按钮，取消绑定，页面返回到服务tab的列表页。
   */
  it('ACP2UI-58303 : jenkins绑定-绑定取消', () => {
    page.navigationButton();
    // expect(
    //   page.listPage_projectName(project_name).checkNameInListPage(),
    // ).toBeTruthy();
    // page.listPage_projectName(project_name).clickNameInlistPage();
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
    expect(
      page.detailPage_TabItem('DevOps 工具链').checkTabClickIsPresent(),
    ).toBeTruthy();
    page.detailPage_TabItem('DevOps 工具链').click();
    expect(page.detailPage_bindButton.isPresent()).toBeTruthy();
    page.detailPage_bindButton.click(); // 点击绑定按钮
    expect(bindPage.bindJenkins_inputcontent.getTitleText()).toContain('绑定');

    bindPage.bindDialog_tools_type('持续集成').click();
    expect(
      bindPage.bindDialog_tools_card(jenkins_name).isPresent(),
    ).toBeTruthy();
    bindPage.bindDialog_tools_card(jenkins_name).click();

    expect(toolsPage.bindPage_cancelButton.isPresent()).toBeTruthy();
    toolsPage.bindPage_cancelButton.click(); // 点击取消按钮
    // expect(
    //     toolsPage.bindPage_cancelButton.checkButtonIsNotPresent()
    // ).toBeFalsy();
  });
});
