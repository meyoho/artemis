// // import { browser } from 'protractor';
// import { ConfigPage } from '../../../page_objects/devops/configmap/configmap.page';

// describe('DEVOPS配置字典 L0级别UI自动化case', () => {
//   const page = new ConfigPage();
//   const name = page.getTestData('config');
//   const project_name = page.projectName2;
//   const isready = page.prepareData.isReady();
//   if (!isready) {
//     return '没有开启devops application 功能';
//   }

//   beforeAll(() => {
//     page.prepareData.delete(name, page.project2ns1);
//     page.login();
//     page.enterProjectDashboard();
//     page.enterProject(project_name);
//   });
//   beforeEach(() => {
//     page.clickLeftNavByText('配置字典');
//   });
//   afterEach(() => {});
//   afterAll(() => {});
//   it('ACP2UI-52945 : 配置字典-创建配置字典-输入名称-输入单个数据-创建-成功', () => {
//     const testData = {
//       名称: name,
//       显示名称: 'description',
//       数据: [{ 键: 'key1', 值: 'value1' }],
//     };
//     page.createPage.create(testData);
//     const expectData = {
//       名称: name,
//       显示名称: 'description',
//       数据: [{ 键: 'key1', 值: 'value1' }],
//     };
//     page.detailPageVerify.verify(expectData);
//   });

//   it('ACP2UI-53703 : 配置字典-搜索功能正常', () => {
//     page.listPage.search(name, 1);
//     const expectVaue = {
//       数量: 1,
//     };

//     page.listPageVerify.verify(expectVaue);
//   });
//   it('ACP2UI-52951 : 配置字典列表-某一个配置字典-删除-成功', () => {
//     page.listPage.delete(name);
//     page.listPage.search(name, 0);
//     const expectVaue = {
//       数量: 0,
//     };

//     page.listPageVerify.verify(expectVaue);
//   });
// });
