// /**
//  * Created by zhangjiao on 2018/3/20.
//  */

// import { HomePage } from '../../../page_objects/devops/home/home.page';

// describe('Home页 L1级别UI自动化case', () => {
//   const page = new HomePage();
//   const prefix = page.getTestData('project');
//   const project_name1 = prefix + '-1';
//   const project_name2 = prefix + '-2';

//   beforeAll(() => {
//     // 删除已经存在的项目
//     page.deleteProject(project_name1);
//     page.deleteProject(project_name2);

//     //创建项目
//     page.createProject(project_name1);
//     page.createProject(project_name2);

//     page.login();
//     page.enterProjectDashboard();
//   });

//   beforeEach(() => {
//     // browser.refresh();
//   });

//   afterAll(() => {
//     page.deleteProject(project_name1);
//     page.deleteProject(project_name2);
//   });

//   it('l0:项目列表页-列表数据正确-到项目管理中增删项目列表数', () => {
//     page.projectTable.getRowCount().then(function(count) {
//       if (count === 0) {
//         expect(page.getButtonByText('创建项目').isPresent()).toBeTruthy(); // Home页应该有"创建项目"按钮
//         page.noData.getText().then(text => {
//           expect(text).toBe(
//             '暂无项目，你可以通过上方“创建项目“按钮或切换至平台管理下创建项目',
//           );
//         });

//         page.getButtonByText('创建项目').click(); // 点击"创建项目"按钮
//         expect(page.getButtonByText('创建项目').isPresent()).toBeFalsy();
//       } else {
//         console.log(
//           'Home页有项目，一共有' +
//             count +
//             '个项目，有项目时没有“创建项目”按钮！',
//         );
//       }
//     });
//   });

//   it('ACP2UI-4313 : l0:项目列表页-按名称搜索项目-输入存在项目查出对应结果-输入不存在项目查不到结果(不支持模糊查询)', () => {
//     page.noData.isPresent().then(isPresent => {
//       if (!isPresent) {
//         page.projectTable.searchByResourceName(project_name1);
//         page.projectTable.getRowCount().then(count => {
//           expect(count).toBe(1);
//         });

//         page.projectTable.searchByResourceName(project_name2);
//         page.projectTable.getRowCount().then(count => {
//           expect(count).toBe(1);
//         });

//         page.projectTable.searchByResourceName('aotuprojectbucunzai', 0); // 在搜索框输入不存在的项目名称
//         page.projectTable.getRowCount().then(count => {
//           expect(count).toBe(0);
//         });
//       }
//     });
//   });

//   it('ACP2UI-4317 : 项目详情页-检查详情页数据，并切换tab页-数据正确', () => {
//     page.projectTable.searchByResourceName(project_name1, 1);
//     page.projectTable.clickResourceNameByRow([project_name1]);
//     // 跳转到概览页
//     page.breadcrumb.getText().then(text => {
//       expect(text).toBe('概览');
//     });
//   });
// });
