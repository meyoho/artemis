/* TODO: 私有2.8 封装没跑过，先暂时注释掉了
// /**
//  * Created by zongyao on 2019/6/14.
//  */

// // import { browser } from 'protractor';

// import { SecretPage } from '../../../page_objects/devops/config-secret/secret.page';

// describe('用户视图保密字典 L0级别UI自动化case', () => {
//   const page = new SecretPage();
//   const project_name = page.projectName2;
//   const secret_name_user = page.getTestData('l0-user');
//   const secret_displayname_user = secret_name_user.toLocaleUpperCase();
//   const secret_username_user = 'zhangjiao';
//   const secret_password_user = 'zhangjiao';
//   const isready = page.prepareData.isReady();
//   if (!isready) {
//     return '没有开启devops application 功能';
//   }

//   beforeAll(() => {
//     page.prepareData.deleteSecret(secret_name_user, page.project2ns1);
//     page.login();
//     page.enterProjectDashboard();
//     page.enterProject(project_name);
//   });

//   beforeEach(() => {});

//   afterAll(() => {
//     page.prepareData.deleteSecret(secret_name_user, page.project2ns1);
//   });

//   /**
//    * 创建Secret,类型选择"用户名／密码"，其余各项条件输入合法，创建成功。
//    */
//   it('ACP2UI-52999 : 保密字典-创建保密字典-用户名密码-用户名/密码-成功', () => {
//     const testData = {
//       类型: '用户名密码',
//       名称: secret_name_user,
//       显示名称: secret_displayname_user,
//       用户名: secret_username_user,
//       密码: secret_password_user,
//     };
//     page.createPage.create(testData);

//     const expectData = {
//       显示名称: secret_displayname_user,
//       类型: '用户名密码',
//       用户名: secret_username_user,
//       密码: secret_password_user,
//     };

//     page.detailPageVerify.verify(expectData);
//   });

//   it('ACP2UI-53003 : 保密字典-列表显示-搜索-条数-正常', () => {
//     page.listPage.search(secret_name_user, 1);
//     const expectVaue = {
//       数量: 1,
//     };

//     page.listPage.verify(expectVaue);
//   });
//   it('ACP2UI-54682 : 保密字典-列表-删除某一个保密字典-成功', () => {
//     page.listPage.delete(secret_name_user);
//     const expectVaue = {
//       数量: 0,
//     };

//     page.listPage.verify(expectVaue);
//   });
// });
