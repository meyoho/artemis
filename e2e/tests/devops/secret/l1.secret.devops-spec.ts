/**
 * Created by zhangjijao on 2019/6/14.
 */

import { SecretPage } from '../../../page_objects/devops_new/secret/secret.page';

describe('Secret L1级别UI自动化case', () => {
  const page = new SecretPage();
  const project_name = page.randomGetTestData();

  const secret_name_user = page.getTestData('user');
  const secret_displayname_user = secret_name_user.toLocaleUpperCase();
  const secret_displayname_user_update = page.getTestData('user-update');
  const secret_username_user = 'zhangjiao';
  const secret_password_user = 'zhangjiao';

  const secret_name_registry = page.getTestData('registry');
  const secret_displayname_registry = secret_name_registry.toLocaleUpperCase();
  const secret_global_registry = page.getTestData('gregistry');
  const secret_global_displayname_registry = secret_name_registry.toLocaleUpperCase();
  const secret_displayname_registry_update = 'registry-update';
  const secret_address_registry = 'inputregistry.com';
  const secret_address_registry_update = 'inputregistry.com.update';
  const secret_username_registry = 'inputregistryusername';
  const secret_username_registry_update = 'inputregistryusername-uptdae';
  const secret_password_registry = 'inputregistrypassword';
  const secret_password_registry_update = 'inputregistrypassword-update';
  const secret_email_registry = 'input@email.com';
  const secret_email_registry_update = 'input@email.com.update';

  const secret_name_ssh = page.getTestData(`ssh`);
  const secret_displayname_ssh = secret_name_ssh.toLocaleUpperCase();
  const secret_global_ssh = page.getTestData(`gssh`);
  const secret_global_displayname_ssh = secret_global_ssh.toLocaleUpperCase();
  const secret_displayname_ssh_update = 'ssh-update';
  const secret_value_ssh = 'sshsshsshsshssh';
  const secret_value_ssh_update = 'sshsshsshsshssh-update';

  const secret_name_oauth = page.getTestData(`oauth`);
  const secret_displayname_oauth = secret_name_oauth.toLocaleUpperCase();
  const secret_global_oauth = page.getTestData(`goauth`);
  const secret_global_displayname_oauth = secret_global_oauth.toLocaleUpperCase();
  const secret_displayname_oauth_update = 'oauth-update';
  const secret_client_id =
    '99aacc7f529f14b9f8deb0ed7a393dee13fcca584f55d135a0a8cc52e9a6c6bb';
  const secret_client_id_update = 'id-update';
  const secret_client_secret =
    '5b4e02b4d367fccef88e2a69c8d50464ae06b0e6d700caf373553c41f0166d03';
  const secret_client_secret_update = 'secret-update';

  const secret_name_global = page.getTestData('global');
  const secret_username_user_update = 'zhangjiao_update';
  const secret_password_user_update = 'zhangjiao_update';
  const someArray = [
    secret_global_registry,
    secret_global_ssh,
    secret_global_oauth,
    secret_name_global,
  ];

  beforeAll(() => {
    page.deleteProject(project_name);
    for (const entry of someArray) {
      page.devopspreparePage.deleteGlobalSecret(entry);
    }
    page.createProject(project_name);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    page.devopspreparePage.deleteProject(project_name);
    for (const entry of someArray) {
      page.devopspreparePage.deleteGlobalSecret(entry);
    }
  });

  /**
   * 创建Secret,使用域是项目私有, 类型选择"用户名／密码"，其余各项条件输入合法，创建成功。
   */
  it('L1:ACP2UI-53653:点击凭据-点击创建按钮-输入凭据名称-使用域选择项目私有-选择项目-输入用户名和密码-点击创建按钮-创建成功-跳转到详情页检验数据', () => {
    const testData = {
      凭据名称: secret_name_user,
      显示名称: secret_displayname_user,
      使用域: '项目私有',
      所属项目: project_name,
      类型: '用户名/密码',
      用户名: secret_username_user,
      密码: secret_password_user,
    };
    page.create(testData);

    const expectData = {
      名称: secret_name_user,
      显示名称: secret_displayname_user,
      使用域: project_name,
      类型: '用户名/密码',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建Secret,使用域是项目私有,类型选择"镜像服务"，其余各项条件输入合法，创建成功。
   */
  it('L1:ACP2UI-55767:点击凭据-点击创建按钮-输入凭据名称-使用域选择项目私有-选择项目-类型选择镜像服务-输入镜像服务地址-用户名-密码-邮箱地址-点击创建按钮-创建成功-跳转到详情页检验数据', () => {
    const testData = {
      凭据名称: secret_name_registry,
      显示名称: secret_displayname_registry,
      使用域: '项目私有',
      所属项目: project_name,
      类型: '镜像服务',
      镜像服务地址: secret_address_registry,
      用户名: secret_username_registry,
      密码: secret_password_registry,
      邮箱地址: secret_email_registry,
    };
    page.create(testData);

    const expectData = {
      名称: secret_name_registry,
      显示名称: secret_displayname_registry,
      使用域: project_name,
      类型: '镜像服务',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建Secret,使用域是项目私有,类型选择"SSH"，其余各项条件输入合法，创建成功。
   */
  it('L1:ACP2UI-4354:点击凭据-点击创建按钮-输入凭据名称-使用域选择项目-选择项目-类型选择ssh-输入ssh私钥-点击创建按钮-创建成功-跳转到详情页检验数据', () => {
    const testData = {
      凭据名称: secret_name_ssh,
      显示名称: secret_displayname_ssh,
      使用域: '项目私有',
      所属项目: project_name,
      类型: 'SSH',
      'SSH 私钥': secret_value_ssh,
    };
    page.create(testData);

    const expectData = {
      名称: secret_name_ssh,
      显示名称: secret_displayname_ssh,
      使用域: project_name,
      类型: 'SSH',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建Secret,使用域是项目私有,类型选择"OAuth2"，其余各项条件输入合法，创建成功。
   */
  it('L1:ACP2UI-55763:点击凭据-点击创建按钮-输入凭据名称-使用域选择项目私有-选择项目-类型选择OAuth2-输入client id secret-点击创建按钮-创建成功-跳转到详情页检验数据', () => {
    const testData = {
      凭据名称: secret_name_oauth,
      显示名称: secret_displayname_oauth,
      使用域: '项目私有',
      所属项目: project_name,
      类型: 'OAuth2',
      'Client ID': secret_client_id,
      'Client Secret': secret_client_secret,
    };
    page.create(testData);

    const expectData = {
      名称: secret_name_oauth,
      显示名称: secret_displayname_oauth,
      使用域: project_name,
      类型: 'OAuth2',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建使用域是平台公共，类型是OAuth2的凭据
   */
  it('ACP2UI-55762: 点击凭据-点击创建按钮-输入凭据名称-类型选择OAuth2-点击创建按钮', () => {
    const testData = {
      凭据名称: secret_global_oauth,
      显示名称: secret_global_displayname_oauth,
      使用域: '平台公共',
      类型: 'OAuth2',
      'Client ID': secret_client_id,
      'Client Secret': secret_client_secret,
    };
    page.create(testData);

    const expectData = {
      名称: secret_global_oauth,
      显示名称: secret_global_displayname_oauth,
      使用域: '平台公共',
      类型: 'OAuth2',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建使用域是平台公共，类型是SSH的凭据
   */
  it('ACP2UI-55764 : 点击凭据-点击创建按钮-输入凭据名称-类型选择ssh-输入ssh私钥-点击创建按钮', () => {
    const testData = {
      凭据名称: secret_global_ssh,
      显示名称: secret_global_displayname_ssh,
      使用域: '平台公共',
      类型: 'SSH',
      'SSH 私钥': secret_value_ssh,
    };
    page.create(testData);

    const expectData = {
      名称: secret_global_ssh,
      显示名称: secret_global_displayname_ssh,
      使用域: '平台公共',
      类型: 'SSH',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 创建使用域是平台公共，类型是镜像服务的凭据
   */
  it('ACP2UI-55766 : 点击凭据-点击创建按钮-输入凭据名称-类型选择镜像服务-输入镜像服务地址-用户名-密码-邮箱地址-点击创建按钮', () => {
    const testData = {
      凭据名称: secret_global_registry,
      显示名称: secret_global_displayname_registry,
      使用域: '平台公共',
      类型: '镜像服务',
      镜像服务地址: secret_address_registry,
      用户名: secret_username_registry,
      密码: secret_password_registry,
      邮箱地址: secret_email_registry,
    };
    page.create(testData);

    const expectData = {
      名称: secret_global_registry,
      显示名称: secret_global_displayname_registry,
      使用域: '平台公共',
      类型: '镜像服务',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 详情页更新显示名称，类型是用户名密码，Secret更新成功。
   */
  it('ACP2UI-55768:点击凭据-选择一个使用域是项目私有-类型是用户名密码-详情页-点击更新显示名称-更新显示名称-点击更新按钮-更新并成功', () => {
    const testData = {
      显示名称: secret_displayname_user_update,
    };
    page.secretdetailUpdateDisplay(secret_name_user, testData);

    const expectData = {
      名称: secret_name_user,
      显示名称: secret_displayname_user_update,
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 详情页更新数据信息，类型是用户名密码，Secret更新成功。
   */
  it('ACP2UI-4359:点击凭据-选择一个使用域是项目私有-类型是用户名密码-详情页-点击更新数据信息-更新用户名密码-点击更新按钮-更新并成功', () => {
    const testData = {
      用户名: secret_username_user_update,
      密码: secret_password_user_update,
    };
    page.secretdetailUpdateData(secret_name_user, testData);

    const expectData = {
      名称: secret_name_user,
      类型: '用户名/密码',
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-54655:点击凭据-选择一个使用域是平台公共-类型是镜像服务-详情页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_registry_update,
    };
    page.secretdetailUpdateDisplay(secret_global_registry, testData);

    const expectData = {
      名称: secret_global_registry,
      显示名称: secret_displayname_registry_update,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-54654 : 点击凭据-选择一个使用域是平台公共-类型是镜像服务-详情页-点击更新数据信息-更新镜像服务地址、用户名、密码、邮箱地址-点击更新按钮', () => {
    const testData = {
      镜像服务地址: secret_address_registry_update,
      用户名: secret_username_registry_update,
      密码: secret_password_registry_update,
      邮箱地址: secret_email_registry_update,
    };
    page.secretdetailUpdateData(secret_global_registry, testData);

    const expectData = {
      名称: secret_global_registry,
      类型: '镜像服务',
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55331 : 点击凭据-选择一个使用域是项目私有-类型是镜像服务-列表页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_registry_update,
    };
    page.secretlistUpdateDisplay(secret_name_registry, testData);
  });

  it('ACP2UI-55332 : 点击凭据-选择一个使用域是项目私有-类型是镜像服务-列表页-点击更新数据信息-更新镜像服务地址-用户名-密码-邮箱地址-点击更新按钮', () => {
    const testData = {
      镜像服务地址: secret_address_registry_update,
      用户名: secret_username_registry_update,
      密码: secret_password_registry_update,
      邮箱地址: secret_email_registry_update,
    };
    page.secretlistUpdateData(secret_name_registry, testData);
  });

  it('ACP2UI-55337 : 点击凭据-选择一个使用域是项目私有-类型是ssh-详情页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_ssh_update,
    };
    page.secretdetailUpdateDisplay(secret_name_ssh, testData);

    const expectData = {
      名称: secret_name_ssh,
      显示名称: secret_displayname_ssh_update,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55338 : 点击凭据-选择一个使用域是项目私有-类型是ssh-详情页-点击更新数据信息-更新ssh私钥-点击更新按钮', () => {
    const testData = {
      'SSH 私钥': secret_value_ssh_update,
    };
    page.secretdetailUpdateData(secret_name_ssh, testData);

    const expectData = {
      名称: secret_name_ssh,
      类型: 'SSH',
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55334 : 点击凭据-选择一个使用域是平台公共-类型是ssh-列表页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_ssh_update,
    };
    page.secretlistUpdateDisplay(secret_global_ssh, testData);
  });

  it('ACP2UI-55335 : 点击凭据-选择一个使用域是平台公共-类型是ssh-列表页-点击更新数据信息-更新ssh私钥-点击更新按钮', () => {
    const testData = {
      'SSH 私钥': secret_value_ssh_update,
    };
    page.secretlistUpdateData(secret_global_ssh, testData);
  });

  it('ACP2UI-55340 : 点击凭据-选择一个使用域是平台公共-类型是OAuth2-详情页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_oauth_update,
    };
    page.secretdetailUpdateDisplay(secret_global_oauth, testData);

    const expectData = {
      名称: secret_global_oauth,
      显示名称: secret_displayname_oauth_update,
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('ACP2UI-55341 : 点击凭据-选择一个使用域是平台公共-类型是OAuth2-详情页-点击更新数据信息-更新client id secret-点击更新按钮', () => {
    const testData = {
      'Client ID': secret_client_id_update,
      'Client Secret': secret_client_secret_update,
    };
    page.secretdetailUpdateData(secret_global_oauth, testData);

    const expectData = {
      名称: secret_global_oauth,
      类型: 'OAuth2',
    };
    page.detailVerifyPage.verify(expectData);
  });

  it('L1:ACP2UI-56748 : 点击凭据-选择一个使用域是项目私有-类型是OAuth2-列表页-点击更新显示名称-更新显示名称-点击更新按钮', () => {
    const testData = {
      显示名称: secret_displayname_oauth_update,
    };
    page.secretlistUpdateDisplay(secret_name_oauth, testData);
  });

  it('ACP2UI-56749 : 点击凭据-选择一个使用域是项目私有-类型是OAuth2-列表页-点击更新数据信息-更新client id secret-点击更新按钮', () => {
    const testData = {
      'Client ID': secret_client_id_update,
      'Client Secret': secret_client_secret_update,
    };
    page.secretlistUpdateData(secret_name_oauth, testData);
  });

  it('ACP2UI-57197:点击凭据-选择一个使用域是项目私有-类型是用户名密码-详情页-点击删除按钮-确认后删除成功', () => {
    page.secretdetaillDelete(secret_name_user); //详情页删除凭据
  });

  it('L1:ACP2UI-54653 : 点击凭据-选择一个使用域是平台公共-类型是镜像服务-详情页-点击删除按钮-确认框点击删除按钮', () => {
    page.secretdetaillDelete(secret_global_registry);
  });

  it('ACP2UI-55339 : 点击凭据-选择一个使用域是项目私有-类型是ssh-详情页-点击删除按钮-确认框点击删除按钮', () => {
    page.secretdetaillDelete(secret_name_ssh);
  });

  it('ACP2UI-56747 : 点击凭据-选择一个使用域是平台公共-类型是OAuth2-详情页-点击删除按钮', () => {
    page.secretdetaillDelete(secret_global_oauth);
  });

  it('ACP2UI-55333 : 点击凭据-选择一个使用域是项目私有-类型是镜像服务-列表页-点击删除按钮-确认框点击删除按钮', () => {
    page.secretlistDelete(secret_name_registry); //列表页删除凭据
  });

  it('ACP2UI-55336 : 点击凭据-选择一个使用域是平台公共-类型是ssh-列表页-点击删除按钮-确认框点击删除按钮', () => {
    page.secretlistDelete(secret_global_ssh); //列表页删除凭据
  });

  it('ACP2UI-56750 : 点击凭据-选择一个使用域是项目私有-类型是OAuth2-列表页-点击删除按钮-确认框点击删除按钮', () => {
    page.secretlistDelete(secret_name_oauth); //列表页删除凭据
  });
});
