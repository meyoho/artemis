/**
 * Created by zhangjiao on 2019/6/14.
 */

import { browser } from 'protractor';

import { SecretPage } from '../../../page_objects/devops/secret/secret.page';
import { CommonPage } from '../../../utility/common.page';

describe('Secret L2级别UI自动化case', () => {
  const page = new SecretPage();

  const project_name = page.randomGetTestData();
  const secret_name1 = 'artemis-auto-secret1';
  const secret_name2 = 'artemis-auto-secret2';
  const secret_username2 = 'zhangjiao2';
  const secret_password2 = 'zhangjiao2';

  const secret_createSecret_text = '创建凭据';

  beforeAll(() => {
    browser.sleep(100).then(() => {
      page.createProject(project_name);
    });

    browser.refresh();
    page.login();
    page.enterAdminDashboard();

    browser.sleep(1000).then(() => {
      const secret_image_data = {
        name: secret_name1,
        namespace: project_name,
        datatype: 'kubernetes.io/dockerconfigjson',
        datas: {
          '.dockerconfigjson': `{"auths":{"index.alauda.cn":{"username":"test","password":"test","email":"test@alauda.io"}}}`,
        },
      };

      page.preparePage.createSecret(secret_image_data);

      const secret_data = {
        name: secret_name2,
        namespace: project_name,
        datatype: 'kubernetes.io/basic-auth',
        datas: {
          username: secret_username2,
          password: secret_password2,
        },
      };

      page.preparePage.createSecret(secret_data);
    });
  });

  beforeEach(() => {
    page.navigationButton();
  });
  afterAll(() => {
    page.deleteProject(project_name);
  });

  /**
   * 在Secret列表页，按名称搜索Secret
   */
  it('ACP2UI-56752 : 点击凭据-列表页名称中输入已存在的名称搜索-输入不存在的名称搜索', () => {
    // 按名称搜索凭据
    page.resourceTable.searchByResourceName('zjsecret_bucunzai', 0); // 在搜索框输入不存在的Secret名称
    page.resourceTable.searchByResourceName(secret_name1, 1); // 在搜索框输入要检索的Secret名称
    expect(
      page
        .listPage_SecretName(project_name, secret_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    // page.resourceTable.clearSearchBox(); //清空检索框

    page.listPage_SecretName(project_name, secret_name1).clickNameInlistPage(); // 到Secret详情页检查通过API创建的数据是否正确
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleText(),
      secret_name1,
    );
    page.detailPage_Content.getTitleText().then(text => {
      expect(text).toBe(secret_name1);
    });

    page.detailPage_Content
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toBe('-');
      });

    page.detailPage_Content
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toBe('镜像服务');
      });
  });

  /**
   * 创建Secret, 各项值的合法验证。
   */
  it('ACP2UI-53084 : 点击凭据-点击创建凭据按钮-点击创建按钮-创建页表单数据合法验证', () => {
    page.clickLeftNavByText('凭据');
    page.listPage_createButton.click(); // 列表页点击'创建Secret'到创建Secret页

    page.waitElementPresent(
      page.createPage_createButton.button,
      '创建页面没有打开',
    );
    page.createPage_createButton.click(); // 点击"创建"按钮 （此时什么都没有输入）
    page.createpage_input_content
      .getHintByText('凭据名称')
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          browser.sleep(1000);
          page.createPage_createButton.click();
        } else {
          CommonPage.waitElementTextChangeTo(
            page.createpage_input_content.getHintByText('凭据名称'),
            '必填项不能为空',
          );
          page.createpage_input_content
            .getHintByText('凭据名称')
            .getText()
            .then(text => {
              expect(text).toBe('必填项不能为空');
            });

          page.createpage_input_content
            .getHintByText('用户名')
            .getText()
            .then(text => {
              expect(text).toBe('必填项不能为空');
            });

          page.createpage_input_content
            .getHintByText('密码')
            .getText()
            .then(text => {
              expect(text).toBe('必填项不能为空');
            });

          page.createPage_secretName_inputbox.input('自动—secretname'); // Secret名称输入不合法，中文。
          page.createpage_input_content
            .getHintByText('凭据名称')
            .getText()
            .then(text => {
              expect(text).toContain(
                '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
              );
            });

          page.createPage_secretName_inputbox.input('SECRETNAME'); // Secret名称输入不合法，大写字母。
          page.createpage_input_content
            .getHintByText('凭据名称')
            .getText()
            .then(text => {
              expect(text).toContain(
                '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
              );
            });

          page.createPage_secretName_inputbox.input('secret_name'); // Secret名称输入不合法，有下划线。
          page.createpage_input_content
            .getHintByText('凭据名称')
            .getText()
            .then(text => {
              expect(text).toContain(
                '以 a-z, 0-9 开头结尾，支持使用 a-z, 0-9, -',
              );
            });
        }
      });
  });

  /**
   * 创建Secret页，点击取消按钮跳转回Secret列表页。
   */
  it('ACP2UI-4352:点击凭据-点击创建按钮-输入凭据名称-输入显示名称-输入用户名和密码-点击取消按钮-返回到列表页', () => {
    expect(page.listPage_createButton.isPresent()).toBeTruthy();
    page.listPage_createButton.click();
    expect(page.createPage_headerTitle_Text.checkTextIsPresent()).toBeTruthy();
    page.createPage_headerTitle_Text.getText().then(text => {
      expect(text).toBe(secret_createSecret_text);
    });

    expect(page.createPage_cancelButton.isPresent()).toBeTruthy();
    page.createPage_cancelButton.click(); // 点击取消按钮
    page.listPage_createButton.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy(); // 点击取消操作后返回列表页，判断列表页的创建Secret按钮是否存在。
    });

    page.listPage_createButton.getText().then(text => {
      expect(text).toBe(secret_createSecret_text);
    });
  });

  /**
   * 更新Secret页，修改显示名称和描述，点击取消按钮，页面返回至Secret详情页显示名称和描述还是原来的值。
   */
  it('ACP2UI-4358 : 点击凭据-选择一个使用域是平台公共-类型是用户名密码-详情页-点击更新显示名称-更新显示名称-点击取消按钮', () => {
    page.resourceTable.searchByResourceName(secret_name1, 1);
    expect(
      page
        .listPage_SecretName(project_name, secret_name1)
        .checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_SecretName(project_name, secret_name1).clickNameInlistPage(); // 在Secret列表页找到SecretName，点击到详情页。
    page.detailPage_triggerButton.click();

    page.getButtonByText('更新数据信息').click(); // 点击更新按钮
    expect(
      page.updateDialog_SecretName(secret_name1).checkTextIsPresent(),
    ).toBeTruthy(); // 判断更新页的SecretName
    page.createPage_cancelButton.click();

    // 取消更新后，验证取消成功，并且到Secret详情页。
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleText(),
      secret_name1,
    );
    page.detailPage_Content.getTitleText().then(text => {
      expect(text).toBe(secret_name1);
    });

    page.detailPage_Content
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toBe('-');
      });

    page.detailPage_Content
      .getElementByText('类型')
      .getText()
      .then(text => {
        expect(text).toBe('镜像服务');
      });
  });

  it('ACP2UI-56754:点击凭据-查看列表页分页跳转是否正常-翻页-翻页后搜索-结果正确(自动化先判断数据是否大于20条，否则需要造数据', () => {
    page.listPage_getSecretTotal.checkTextIsPresent();
    page.listPage_getSecretTotal.getText().then(function(text) {
      const reg = /[^0-9]/gi;
      let a = Number(text.replace(reg, ''));
      while (a < 22) {
        const name = 'artemis-auto-secret' + a;
        const secret_data = {
          name: name,
          namespace: project_name,
          datatype: 'kubernetes.io/basic-auth',
          datas: {
            username: secret_username2,
            password: secret_password2,
          },
        };

        page.preparePage.createSecret(secret_data);
        a++;
      }
      browser.refresh();
      page.listPage_nextPage_button.click();
      page.resourceTable.searchByResourceName(secret_name1, 1);
    });
  });
});
