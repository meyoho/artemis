/**
 * Created by wangyanzhao on 2019/12/24.
 */

import { SecretPage } from '../../../page_objects/devops_new/secret/secret.page';

describe('Secret L0级别UI自动化case', () => {
  const page = new SecretPage();

  const secret_name_global = page.getTestData('global0');
  const secret_displayname_global = secret_name_global.toLocaleUpperCase();
  const secret_displayname_global_update = page.getTestData('display-update');
  const secret_username_user = 'uitest_create';
  const secret_password_user = 'uitest_create';
  const secret_username_user_update = 'uitest_update';
  const secret_password_user_update = 'uitest_update';

  beforeAll(() => {
    page.devopspreparePage.deleteGlobalSecret(secret_name_global);
    page.login();
    page.enterAdminDashboard();
  });

  afterAll(() => {
    page.devopspreparePage.deleteGlobalSecret(secret_name_global);
  });

  /**
   * 创建使用域是平台公共，类型是用户名/密码的凭据
   */
  it('ACP2UI-4351 : l0:点击凭据-点击创建按钮-输入凭据名称-输入显示名称-输入用户名和密码-点击创建按钮-创建成功-跳转到详情页检验数据', () => {
    const testData = {
      凭据名称: secret_name_global,
      显示名称: secret_displayname_global,
      使用域: '平台公共',
      类型: '用户名/密码',
      用户名: secret_username_user,
      密码: secret_password_user,
    };
    page.create(testData);

    const expectData = {
      名称: secret_name_global,
      显示名称: secret_displayname_global,
      使用域: '平台公共',
      类型: '用户名/密码',
    };
    page.detailVerifyPage.verify(expectData);
  });

  /**
   * 列表页更新显示名称，类型是用户名密码，Secret更新成功。
   */
  it('ACP2UI-53082 : l0:点击凭据-选择一个使用域是平台公共-类型是用户名密码-列表页-点击更新显示名称-更新显示名称-点击更新按钮-更新并成功', () => {
    const testData = {
      显示名称: secret_displayname_global_update,
    };
    page.secretlistUpdateDisplay(secret_name_global, testData);
  });

  /**
   * 列表页更新数据信息，类型是用户名密码，Secret更新成功。
   */
  it('ACP2UI-4357 : l0:点击凭据-选择一个使用域是平台公共-类型是用户名密码-列表页-点击更新数据信息-更新用户名密码-点击更新按钮-更新并成功', () => {
    const testData = {
      用户名: secret_username_user_update,
      密码: secret_password_user_update,
    };
    page.secretlistUpdateData(secret_name_global, testData);
  });

  it('ACP2UI-57198 : l0:点击凭据-选择一个使用域是平台公共-类型是用户名密码-列表页-点击删除按钮-确认后删除成功', () => {
    page.secretlistDelete(secret_name_global); //列表页删除凭据
  });
});
