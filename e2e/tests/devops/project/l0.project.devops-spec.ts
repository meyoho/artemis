// TODO 这个文件会卡死流水线，暂时先注释掉，后边再跟进吧
// /**
//  * Created by zhangjiao on 2019/6/11
//  */

// import { ProjectPage } from '../../../page_objects/devops/project/project.page';
// import { browser } from 'protractor';
// import { ServerConf } from '@e2e/config/serverConf';
// import { CommonKubectl } from '@e2e/utility/common.kubectl';

// describe('项目 L0级别UI自动化case', () => {
//   const page = new ProjectPage();
//   const project_name = page.getTestData('projectl1');
//   const project_displayname = page.getTestData('测试PROJECTL1');
//   const projectDescription = 'create the first project!';

//   beforeAll(() => {
//     // 通过kubectl 创建项目
//     try {
//       CommonKubectl.execKubectlCommand(
//         `kubectl delete project ${project_name}`,
//       );

//       page.createProject(
//         project_name,
//         ServerConf.REGIONNAME,
//         project_displayname,
//         projectDescription,
//       );
//     } catch (ex) {
//       console.error(ex);
//     }
//     page.login();
//     page.enterAdminDashboard();
//   });

//   beforeEach(() => {
//     page.navigationButton();
//   });

//   afterAll(() => {
//     CommonKubectl.execKubectlCommand(
//       `kubectl delete project ${project_name}`,
//       ServerConf.REGIONNAME,
//     );
//   });

//   it('ACP2UI-4312 : l0:项目列表页-列表数据正确-到项目管理中增删项目列表数据也可以正常显示。', () => {
//     browser.sleep(1);
//     expect(
//       page.listPage_projectName(project_name).checkNameInListPage(),
//     ).toBeTruthy();
//     page.projectTable.searchByResourceName(project_name, 1);
//   });

//   it('ACP2UI-4313 : l0:项目列表页-按名称搜索项目-输入存在项目查出对应结果-输入不存在项目查不到结果(不支持模糊查询)', () => {
//     page.listPage_nameFilter_input.isPresent().then(isPresent => {
//       expect(isPresent).toBeTruthy(); // 检查搜索框是否存在
//     });

//     page.projectTable.searchByResourceName('zjprojectbucunzai', 0);
//     page.projectTable.getRowCount().then(count => {
//       expect(count).toBe(0);
//     });

//     // 按名称搜索项目
//     page.projectTable.searchByResourceName(project_name, 1);
//     page.projectTable.getRowCount().then(count => {
//       expect(count).toBe(1);
//     });

//     page.listPage_projectName(project_name).clickNameInlistPage(); // 到project详情页检查通过API创建的数据是否正确
//     page.detailPage_Content.getTitleText().then(text => {
//       expect(text).toBe(project_name);
//     });

//     page.detailPage_Content
//       .getElementByText('显示名称')
//       .getText()
//       .then(text => {
//         expect(text).toBe(project_displayname);
//       });

//     page.detailPage_Content
//       .getElementByText('描述')
//       .getText()
//       .then(text => {
//         expect(text).toBe(projectDescription);
//       });
//   });
// });
