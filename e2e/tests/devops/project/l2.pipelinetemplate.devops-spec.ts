/**
 * Created by zhangjiao on 2019/6/14.
 */

import { browser } from 'protractor';

import { PipelineTemplate } from '../../../page_objects/devops/project/pipelinetemplate.page';
import { ProjectPage } from '../../../page_objects/devops/project/project.page';
import { SecretPage } from '../../../page_objects/devops/secret/secret.page';
import { CommonMethod } from '@e2e/utility/common.method';

describe('项目-流水线模版 L2级别UI自动化case', () => {
  const page = new ProjectPage();
  const secretPage = new SecretPage();
  const templatePage = new PipelineTemplate();
  const project_name = CommonMethod.random_generate_testData('imtmpl2');

  const secret_name = page.getTestData('secretl2');
  const secret_user_name = page.getTestData('user1');
  const secret_image_name = page.getTestData('image1');

  beforeAll(() => {
    page.deleteProject(project_name);
    page.preparePage.deleteGlobalSecret(secret_name);
    browser.sleep(1000).then(() => {
      page.createProject(project_name);
    });

    page.login();
    page.enterAdminDashboard();
    browser.sleep(1000).then(() => {
      page.createProject(project_name);
      const secret_data = {
        name: secret_user_name,
        namespace: project_name,
        datatype: 'kubernetes.io/basic-auth',
        datas: {
          username: 'test',
          password: 'test',
        },
      };

      page.preparePage.createSecret(secret_data);
      const secret_image_data = {
        name: secret_image_name,
        namespace: project_name,
        datatype: 'kubernetes.io/dockerconfigjson',
        datas: {
          '.dockerconfigjson': `{"auths":{"index.alauda.cn":{"username":"test","password":"test","email":"test@alauda.io"}}}`,
        },
      };

      page.preparePage.createSecret(secret_image_data);
    });
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    page.deleteProject(project_name);
    secretPage.preparePage.deleteGlobalSecret(secret_name);
  });

  it('ACP2UI-55093 : 项目详情-配置流水线模版-输入方式-搜索凭据正常工作', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
    templatePage.detailTab_operateButton.click();
    templatePage.detailTab_configButton_oprate.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();
    // 点击输入
    templatePage.configDialog_selectType('输入');
    // expect(
    //   templatePage.configDialog_secret.checkDataIsNotInDropdown('ssssss'), // 这里现在有bug, image的secret不会在列表显示（secret_image_name）
    // ).toBeFalsy();
    templatePage.configDialog_secret.input(secret_user_name);
    templatePage.configDialog_secretInput.getText().then(text => {
      expect(text).toBe(secret_user_name);
    });

    templatePage.configDialog_cancelButton.click();
  });

  it('ACP2UI-52796 : 项目详情-流水线模板-配置模板仓库-输入-点击添加凭据-凭据类型是"用户/密码"-输入各项后添加凭据成功-并显示在下拉框中', () => {
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
    templatePage.detailTab_operateButton.click();
    templatePage.detailTab_configButton_oprate.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();

    templatePage.configDialog_selectType('输入'); // 点击输入

    // 添加凭据
    const testData = {
      凭据名称: secret_name,
      类型: '用户名/密码',
      用户名: 'jiaozhang',
      密码: 'aaaaasa',
    };
    secretPage.addSecrets(testData);
    // 创建凭据后，选中的是当前凭据
    templatePage.configDialog_secretInput.getText().then(text => {
      expect(text).toBe(secret_name);
    });

    templatePage.configDialog_secret.select(secret_name);
    templatePage.configDialog_cancelButton.click();
  });

  it('ACP2UI-52797 : 左导航官方模板-搜索功能-输入存在模板搜索到结果-输入不存在模板搜索不到结果', () => {
    page.clickLeftNavByText('官方模版');
    templatePage.templateTableofficial.searchByResourceName(
      'templatebucunzai',
      0,
      '',
    );
    templatePage.templateTableofficial.searchByResourceName('golang', 2, '');
    templatePage.templateTableofficial.searchByResourceName(
      '构建并部署',
      5,
      '',
    );
  });

  it('ACP2UI-52798 : 左导航官方模板名称-打开模板详情dialog', () => {
    page.clickLeftNavByText('官方模版');
    templatePage.detailTab_templateName('Nodejs 10 构建并部署应用').click();
    browser.sleep(500);
    templatePage.detailDialog_title.getText().then(text => {
      expect(text).toBe('Nodejs 10 构建并部署应用');
    });
    templatePage.detailDialog_close.click();
    expect(templatePage.detailDialog_title.checkTextIsNotPresent()).toBeFalsy();
  });
});
