/**
 * Created by zhangjiao on 2019/6/14.
 */

import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { browser } from 'protractor';

import { ServerConf } from '../../../config/serverConf';
import { PipelineTemplate } from '../../../page_objects/devops/project/pipelinetemplate.page';
import { ProjectPage } from '../../../page_objects/devops/project/project.page';

describe('项目-流水线模版 L1级别UI自动化case', () => {
  const page = new ProjectPage();
  const templatePage = new PipelineTemplate();
  const project_name1 = page.randomGetTestData();
  const project_name2 = page.projectName2;

  const githubsecret = page.getTestData('hubsecret1');
  const github_tmp_public = `https://github.com/${ServerConf.GITHUB_USER}/template-public.git`;
  const github_tmp_private = `https://github.com/${ServerConf.GITHUB_USER}/template-private.git`;
  let gitlab_tmp_private = ServerConf.GITLAB_TMP_PRIVATE;
  const ispublic = ServerConf.IS_PUBLIC;

  beforeAll(() => {
    page.deleteSecret(githubsecret, project_name1);
    page.deleteProject(project_name1);
    CommonKubectl.execKubectlCommand(
      `kubectl delete pipelinetemplatesyncs -n ${project_name1} TemplateSync`,
    );
    // 通过kubectl 创建项目
    browser.sleep(1000).then(() => {
      page.createProject(project_name1);
    });

    browser.sleep(3000).then(() => {
      const secret_data = {
        name: githubsecret,
        namespace: project_name1,
        datatype: 'kubernetes.io/basic-auth',
        datas: {
          username: ServerConf.GITLAB_USER,
          password: ServerConf.GITLAB_TOKEN,
        },
      };
      page.preparePage.createSecret(secret_data);
    });
    page.login();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {
    page.deleteSecret(githubsecret, project_name1);
    page.deleteProject(project_name1);
    CommonKubectl.execKubectlCommand(
      `kubectl delete pipelinetemplatesyncs -n ${project_name1} TemplateSync`,
    );
  });

  it('ACP2UI-52784 : 项目详情-流水线模板-未配置模板仓库时-自定义模板配置中有配置模板仓库的入口-自定义模板列表为0', () => {
    expect(
      page.listPage_projectName(project_name1).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name1).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page
      .detailPage_TabItem('')
      .getActiveText()
      .then(text => {
        expect(text).toBe('详情信息'); // 判断选中的是否是详情信息tab
      });

    page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
    // 新创建的项目，还未配置模版仓库地址
    expect(templatePage.detailTab_configButton.isPresent()).toBeTruthy(); // 还未配置时，检查"配置模版仓库"按钮是否存在
    templatePage.detailTab_configButton.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();
    templatePage.configDialog_cancelButton.click();
    expect(templatePage.configDialog_title.checkTextIsNotPresent()).toBeFalsy();

    // 检查自定义数据是0
    templatePage.noData.getText().then(text => {
      expect(text).toBe('无项目自定义模版');
    });
  });

  //  ----- GITLAB的代码仓库 ，在project2
  it('ACP2UI-52787 : 项目详情-流水线模版-配置模版仓库-选择已分配的代码仓库-配置且同步成功-同步结果检查', () => {
    gitlab_tmp_private = `${ServerConf.GITLAB_NAME}-${ServerConf.GITLAB_USER}-${ServerConf.GITLAB_TMP_PRIVATE}`;
    expect(
      page.listPage_projectName(project_name2).checkNameInListPage(),
    ).toBeTruthy();
    page.listPage_projectName(project_name2).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
    page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
    templatePage.detailTab_operateButton.click();
    templatePage.detailTab_configButton_oprate.click();
    expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();
    templatePage.confirmDialog_type('选择').click();
    templatePage.configDialog_repo.isPresent().then(isPresent => {
      if (isPresent) {
        templatePage.configDialog_cancelButton.click();
      } else {
        const testData = {
          方式: '选择',
          代码仓库: gitlab_tmp_private,
          代码分支: 'master',
        };
        templatePage.configDialog_inputbox(testData);

        // 配置结果检查
        templatePage.detailTab_Content
          .getElementByText('模版仓库地址')
          .getText()
          .then(text => {
            expect(text).toBe(gitlab_tmp_private);
          });

        templatePage.detailTab_Content
          .getElementByText('代码分支')
          .getText()
          .then(text => {
            expect(text).toBe('master');
          });

        templatePage.waitElementNotPresent(
          templatePage.detailTab_syncing.button,
          '模版仓库同步中...',
          60000,
        );
        // expect(templatePage.detailTab_successNum.getText()).toBe('9');
        // expect(templatePage.detailTab_failureNum.getText()).toBe('0');
        // expect(templatePage.detailTab_skipNum.getText()).toBe('0');

        // 添加自定义模版搜索功能（ACP-2443:项目详情-流水线模版-自定义模版列表搜索功能）
        templatePage.templateTable.searchByResourceName(
          'templatebucunzai',
          0,
          '',
        );
        // templatePage.templateTable.searchByResourceName('ZJ', 2, '');
        // templatePage.templateTable.searchByResourceName(
        //     '构建并更新服务',
        //     1,
        //     ''
        // );
        // expect(
        //     templatePage.detailTab_templateSourceText.getText()
        // ).toBe('自定义 (1)');
      }
    });
  });

  //  ----- GITHUB的代码仓库 ，在project1
  it('ACP2UI-57201 : 项目详情-流水线模板-配置模板仓库-输入公有Git代码仓库地址,master分支,不选择凭据-配置成功-同步成功', () => {
    if (ispublic === 'true') {
      expect(
        page.listPage_projectName(project_name1).checkNameInListPage(),
      ).toBeTruthy();
      page.listPage_projectName(project_name1).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
      page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
      templatePage.detailTab_operateButton.click();
      templatePage.detailTab_configButton_oprate.click();
      expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();

      const testData = {
        方式: '输入',
        代码仓库地址: github_tmp_private,
        代码分支: 'master',
      };
      templatePage.configDialog_inputbox(testData);

      // 配置结果检查
      templatePage.detailTab_Content
        .getElementByText('模版仓库地址')
        .getText()
        .then(text => {
          expect(text).toBe(github_tmp_private);
        });

      templatePage.detailTab_Content
        .getElementByText('代码分支')
        .getText()
        .then(text => {
          expect(text).toBe('master');
        });

      templatePage.waitElementNotPresent(
        templatePage.detailTab_syncing.button,
        '模版仓库同步中...',
        60000,
      );
      //检查同步结果
      // expect(templatePage.detailTab_successNum.getText()).toBe('0');
      // expect(templatePage.detailTab_failureNum.getText()).toBe('0');
      // expect(templatePage.detailTab_skipNum.getText()).toBe('0');

      // 再修改-选择凭据
      templatePage.detailTab_operateButton.click();
      templatePage.detailTab_configButton_oprate.click();
      expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();

      const testData1 = {
        方式: '输入',
        代码仓库地址: github_tmp_private,
        凭据: githubsecret,
      };
      templatePage.configDialog_inputbox(testData1);

      // 配置结果检查
      templatePage.detailTab_Content
        .getElementByText('模版仓库地址')
        .getText()
        .then(text => {
          expect(text).toBe(github_tmp_private);
        });

      templatePage.detailTab_Content
        .getElementByText('代码分支')
        .getText()
        .then(text => {
          expect(text).toBe('master');
        });

      templatePage.waitElementNotPresent(
        templatePage.detailTab_syncing.button,
        '模版仓库同步中...',
        60000,
      );
      //检查同步结果
      // expect(templatePage.detailTab_successNum.getText()).toBe('9');
      // expect(templatePage.detailTab_failureNum.getText()).toBe('0');
      // expect(templatePage.detailTab_skipNum.getText()).toBe('0');
    } else {
      console.log('内网环境无法访问Github，没有模版代码仓库');
    }
  });

  it('ACP2UI-57685 : 项目详情-流水线模版-配置模版仓库-输入公有Git代码仓库地址,其他不存在分支,不选择凭据-配置成功-同步失败', () => {
    if (ispublic === 'true') {
      expect(
        page.listPage_projectName(project_name1).checkNameInListPage(),
      ).toBeTruthy();
      page.listPage_projectName(project_name1).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
      page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
      templatePage.detailTab_operateButton.click();
      templatePage.detailTab_configButton_oprate.click();
      expect(templatePage.configDialog_title.checkTextIsPresent()).toBeTruthy();

      const testData1 = {
        方式: '输入',
        代码仓库地址: github_tmp_public,
        代码分支: 'auto/testv1.1',
      };
      templatePage.configDialog_inputbox(testData1);

      // 配置结果检查
      templatePage.detailTab_Content
        .getElementByText('模版仓库地址')
        .getText()
        .then(text => {
          expect(text).toBe(github_tmp_public);
        });

      templatePage.detailTab_Content
        .getElementByText('代码分支')
        .getText()
        .then(text => {
          expect(text).toBe('auto/testv1.1');
        });

      templatePage.waitElementNotPresent(
        templatePage.detailTab_syncing.button,
        '模版仓库同步中...',
        60000,
      );
      //检查同步结果
      // expect(templatePage.detailTab_successNum.getText()).toBe('5');
      // expect(templatePage.detailTab_failureNum.getText()).toBe('0');
      // expect(templatePage.detailTab_skipNum.getText()).toBe('0');
      // 5个成功状态是新增
    } else {
      console.log('内网环境无法访问Github，没有模版代码仓库');
    }
  });

  xit('ACP2UI-52791 : 项目详情-流水线模板-同步模板仓库-同步成功-同步结果检查', () => {
    if (ispublic === 'true') {
      expect(
        page.listPage_projectName(project_name1).checkNameInListPage(),
      ).toBeTruthy();
      page.listPage_projectName(project_name1).clickNameInlistPage(); // 在列表页点击项目名称到项目详情页
      page.detailPage_TabItem('流水线模版').click(); // 点击详情页的“流水线模版”tab
      templatePage.detailTab_operateButton.click();
      templatePage.detailTab_syncButton.click();
      templatePage.waitElementNotPresent(
        templatePage.detailTab_syncing.button,
        '同步配置一直在同步中...',
      );
      templatePage.detailTab_resultLink.click();
      expect(templatePage.resultDialog_title.checkTextIsPresent()).toBeTruthy();
      templatePage.resultDialog_close.click();
    } else {
      console.log('GITLAB没有准备好，没有模版代码仓库');
    }
  });
});
