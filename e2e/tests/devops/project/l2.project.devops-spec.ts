/**
 * Created by zhangjiao on 2019/6/11
 */

import { ProjectPage } from '../../../page_objects/devops/project/project.page';
import { CommonPage } from '../../../utility/common.page';

describe('项目 L2级别UI自动化case', () => {
  const page = new ProjectPage();
  const project_name = page.projectName1;

  beforeAll(() => {
    page.login();
    page.enterAdminDashboard();
  });

  beforeEach(() => {
    page.navigationButton();
  });

  afterAll(() => {});

  it('ACP2UI-4317 : 项目详情页-检查详情页数据，并切换tab页-数据正确', () => {
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleEle(),
      project_name,
    );
    page
      .detailPage_TabItem('')
      .getActiveText()
      .then(text => {
        expect(text).toBe('详情信息'); // 判断选中的是否是详情信息tab
      });

    page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的“DevOps工具链”tab
    page.detailPage_bindButton.isPresent().then(isPresent => {
      expect(isPresent).toBeTruthy(); // 检查"绑定"按钮是否存在
    });

    page.detailPage_TabItem('详情信息').click(); // 点击详情页的详情信息tab
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleText(),
      project_name,
    ); // 检查基本信息中项目名称是否正确

    page.detailPage_TabItem('DevOps 工具链').click(); // 点击详情页的“DevOps工具链”tab
    expect(page.detailPage_bindButton.isPresent()).toBeTruthy(); // 检查"绑定jenkins服务按钮"是否存在

    page.detailPage_TabItem('详情信息').click(); // 点击详情页的详情信息tab
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleText(),
      project_name,
    );
    page.detailPage_Content
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toBe('UI TEST Project');
      });

    page.detailPage_Content
      .getElementByText('描述')
      .getText()
      .then(text => {
        expect(text).toBe('UI TEST Project');
      });
  });

  it('ACP2UI-54699 : 项目详情页-点击面包屑<项目>-返回到项目列表页', () => {
    page.enterProject(project_name); // 在列表页点击项目名称到项目详情页
    // 判断是否到详情页
    CommonPage.waitElementTextChangeTo(
      page.detailPage_Content.getTitleEle(),
      project_name,
    );
    page
      .detailPage_TabItem('')
      .getActiveText()
      .then(text => {
        expect(text).toBe('详情信息'); // 判断选中的是否是详情信息tab
      });

    page.detailPage_Content
      .getElementByText('显示名称')
      .getText()
      .then(text => {
        expect(text).toBe('UI TEST Project');
      });

    page.detailPage_Breadcrumb.click(); // 点击面包屑“项目”后返回到列表页
    expect(
      page.listPage_projectName(project_name).checkNameInListPage(),
    ).toBeTruthy();
  });
});
