/**
 * 资源列表的页面类, 包含单击资源名进入详情页， 单击右侧选择操作，验证排序等功能
 * Created by liuwei on 2018/3/1.
 *
 */
import {
  $,
  $$,
  ElementFinder,
  browser,
  by,
  element,
  promise,
} from 'protractor';

import { AloSearch } from '../element_objects/alauda.aloSearch';
import { AlaudaAuiTable } from '../element_objects/alauda.aui_table';
import { AlaudaDropdown } from '../element_objects/alauda.dropdown';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class AuiTableComponent extends AlaudaAuiTable {
  private _searchBox: AloSearch;

  constructor(
    root: ElementFinder, // 包含检索框和aui-table 控件的元素
    tableSelector = 'aui-table',
    searchBoxSelector = '.alo-search', // 检索框的父元素
    bodyCellSelector = 'aui-table-cell',
    rowCellSelector = 'aui-table-row',
    headerCellSelector = 'aui-table-header-cell',
  ) {
    super(
      root.$(tableSelector),
      headerCellSelector,
      bodyCellSelector,
      rowCellSelector,
    );
    super.waitElementPresent(root);
    this._searchBox = new AloSearch(root.$(searchBoxSelector));
  }

  /**
   * 数据加载中的控件
   */
  get loading(): ElementFinder {
    return $('.list-status-loading');
  }

  /**
   * 重写父类的方法，单击表头后等待提示消失
   * @param headername 表头列名
   */
  clickHeaderByName(headername): promise.Promise<void> {
    super.clickHeaderByName(headername);
    this.waitElementPresent(this.loading, 60000);
    return browser.sleep(100);
  }

  /**
   * 单击资源名称进入详情页
   * @param keys 根据关键字keys, 找到资源列表的一行
   * @param coulmeName 列名
   */
  clickResourceNameByRow(keys, coulmeName = '名称'): promise.Promise<void> {
    this.getCell(coulmeName, keys).then(cellElem => {
      const nameItem = cellElem.$('a');
      this.waitElementPresent(nameItem);
      nameItem.click();
    });

    return browser.sleep(100);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行， 根据columeName 找到所在列的单元格里面的标签
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any}
   */
  getCellLabel(keys, columeName = '名称'): promise.Promise<string> {
    return this.getCell(columeName, keys).then(elem => {
      return elem.$$(`.plain-container__label`).getText();
    });
  }

  /**
   * 根据关键字keys, 找到资源列表的一行，单击操作按钮
   * @param keys [podname2317, namespace2317, 'Pod']
   * @param actionName 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除',['EXEC','<容器名称>']
   * @return {any}
   */
  clickOperationButtonByRow(
    keys,
    actionName,
    iconButtonSelector = 'alo-menu-trigger aui-icon',
    itemListSelector = 'aui-menu-item button',
  ): promise.Promise<void> {
    const row = this.getRow(keys);
    const option = new AlaudaDropdown(
      row.$(iconButtonSelector),
      $$(itemListSelector),
    );
    if (Array.isArray(actionName)) {
      return option.select(actionName[0], actionName[1]);
    } else {
      return option.select(actionName);
    }
  }
  clickOperationButtonOnly(
    keys,
    iconButtonSelector = 'alo-menu-trigger aui-icon',
    itemListSelector = 'aui-menu-item button',
  ): promise.Promise<void> {
    const row = this.getRow(keys);
    const option = new AlaudaDropdown(
      row.$(iconButtonSelector),
      $$(itemListSelector),
    );
    return new AlaudaButton(option.root).click();
  }
  /**
   * 重写父类的方法 getColumeTextByName, 验证排序的方法会依赖这个
   * @param name 表格的列的名称
   */
  getColumeTextByName(name): promise.Promise<string> {
    return this.getColumeIndexByColumeName(name).then(tableInfo => {
      const index = (tableInfo.columeIndex + 1).toString();
      switch (name) {
        case '名称':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//a'))
            .getText();
        case '创建时间':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//span'))
            .getAttribute('title');
        default:
          return super.getColumeTextByName(name);
      }
    });
  }

  /**
   * 表格右上角的检索框，子类不一样时需要重载
   */
  get aloSearch() {
    return this._searchBox;
  }

  /**
   * 检索资源
   * @param name 检索的资源名称
   * @param rowCount 期望检索到的资源数量
   */
  searchByResourceName(
    name: string,
    rowCount = 1,
    searchType = '名称',
  ): promise.Promise<void> {
    this.aloSearch.search(name, searchType);
    browser.driver.wait(
      () => {
        return this.getRowCount().then(count => {
          return count === rowCount;
        });
      },
      5000,
      `检索后，表格中的数据与期望值(${rowCount})不匹配`,
    );
    return browser.sleep(100);
  }
  /**
   * 检索资源
   * @param name 检索的资源名称
   * @returns 行数
   */
  searchResource(name: string, searchType = '名称') {
    this.aloSearch.search(name, searchType);
    return this.getRowCount();
  }
  /**
   * 检索资源，适用于没有搜索按钮的搜索框
   * @param name 检索的资源名称
   * @param rowCount 期望检索到的资源数量
   */
  searchByResourceNameNotClick(
    name: string,
    rowCount = 1,
    searchType = '名称',
  ): promise.Promise<void> {
    this.aloSearch.onlySearch(name, searchType);
    browser.driver.wait(
      () => {
        return this.getRowCount().then(count => {
          return count === rowCount;
        });
      },
      5000,
      `检索后，表格中的数据与期望值(${rowCount})不匹配`,
    );
    return browser.sleep(100);
  }

  /**
   * 表格为空时的显示, 子类不一样时需要重载
   */
  get noResult(): ElementFinder {
    return $('.aui-card__content .no-data span');
  }
}
