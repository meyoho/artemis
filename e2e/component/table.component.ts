/**
 * 资源列表的页面类, 包含单击资源名进入详情页， 单击右侧选择操作，验证排序等功能
 * Created by liuwei on 2018/3/1.
 *
 */
import { $, $$, browser, by, element, promise } from 'protractor';

import { AloSearch } from '../element_objects/alauda.aloSearch';
import { AlaudaTable } from '../element_objects/alauda.table';
import { CommonPage } from '../utility/common.page';

export class TableComponent extends AlaudaTable {
  private _parent_selector;
  private _body_cell_selector;
  private _body_row_selector;
  private _searchBox;
  private _noData_selector;

  constructor(
    parent_selector: string = 'aui-card',
    searchBox_selector: any = `.list-header .alo-search`,
    header_cell_selector: string = '.aui-table__header-cell',
    body_cell_selector: string = '.aui-table__cell',
    body_row_selector: string = '.aui-table__row',
    nodata_selector: string = '.no-data span',
  ) {
    super(
      `${header_cell_selector}`,
      `${body_cell_selector}`,
      `${body_row_selector}`,
    );
    this._searchBox = $(`${parent_selector} ${searchBox_selector}`);
    this._body_cell_selector = `${parent_selector} ${body_cell_selector}`;
    this._body_row_selector = `${parent_selector} ${body_row_selector}`;
    this._parent_selector = parent_selector;
    this._noData_selector = nodata_selector;
  }

  /**
   * 重写父类的方法，单击表头后等待提示消失
   * @param headername 表头列名
   */
  clickHeaderByName(headername) {
    super.clickHeaderByName(headername);
    return browser.sleep(100);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行,单击资源名称进入详情页
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any}
   */
  clickResourceNameByRow(keys): promise.Promise<void> {
    const nameItem = this.getRow(keys).$(`${this._body_cell_selector} a`);
    CommonPage.waitElementPresent(nameItem);
    nameItem.click();
    return browser.sleep(500);
  }

  /**
   * 根据关键字keys, 找到资源列表的一行， 根据columeName 找到所在列的单元格里面的标签
   * @param keys [podname2317, namespace2317, 'Pod']
   * @return {any}
   */
  getCellLabel(keys, columeName = '名称') {
    const body_cell_selector = this._body_cell_selector;
    return this.getCell(columeName, keys).then(function(elem) {
      return elem.$$(`${body_cell_selector} .plain-container__label`).getText();
    });
  }

  /**
   * 根据关键字keys, 找到资源列表的一行，单击操作按钮
   * @param keys [podname2317, namespace2317, 'Pod']
   * @param actionName 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除'
   * @return {any}
   */
  clickOperationButtonByRow(
    keys,
    actionName,
    iconSelector = 'alo-menu-trigger aui-icon',
    itemSelector = 'aui-menu-item button',
  ) {
    this.getRow(keys)
      .$(iconSelector)
      .click();
    CommonPage.waitElementPresent(element(by.css(itemSelector)));
    browser.sleep(100);
    return this._selectOperation(actionName, itemSelector);
  }

  /**
   * 操作列表的所有元素
   */
  _operationList(itemSelector = 'aui-menu-item button') {
    return element.all(by.css(itemSelector));
  }

  /**
   * 单击操作按钮，或从table 的某行单击操作列按钮后，选择一个操作
   * @parameter {menu_item} string 类型, 操作的名字，例如 '更新'， '更新标签'， '更新注解'， '删除'
   *
   */
  _selectOperation(menu_item, itemSelector = 'aui-menu-item button') {
    CommonPage.waitElementPresent(this._operationList(itemSelector).first());
    const textList = this._operationList(itemSelector).getText();
    this._operationList(itemSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text.indexOf(menu_item) !== -1;
        });
      })
      .first()
      .click();
    return textList;
  }

  /**
   * 重写父类的方法 getColumeTextByName, 验证排序的方法会依赖这个
   * @param name 表格的列的名称
   */
  getColumeTextByName(name) {
    return this._getColumeIndexByColumeName(name).then(tableInfo => {
      const index = (tableInfo.columeIndex + 1).toString();
      switch (name) {
        case '名称':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//a'))
            .getText();
        case '创建时间':
          return element
            .all(by.xpath('//aui-table-cell[' + index + ']//span'))
            .getAttribute('title');
        default:
          return super.getColumeTextByName(name);
      }
    });
  }

  get aloSearch() {
    return new AloSearch(this._searchBox);
  }

  /**
   * 按名称检索
   * @param name 名称
   */
  search(name: string, rowCount: number = 1) {
    this.aloSearch.auiSearch.search(name);
    if (rowCount > 0) {
      CommonPage.waitElementPresent($$(this._body_row_selector).first());
    }
    CommonPage.waitRowcountTextChangeTo($$(this._body_row_selector), rowCount);
    browser.sleep(1000);
  }
  enterSearch(name: string, rowCount: number = 1) {
    this.aloSearch.auiSearch.enterSearch(name);
    if (rowCount > 0) {
      CommonPage.waitElementPresent($$(this._body_row_selector).first());
    }
    CommonPage.waitRowcountTextChangeTo($$(this._body_row_selector), rowCount);
    browser.sleep(1000);
  }

  /**
   * 检索资源
   * @param name 检索的资源名称
   * @param rowCount 期望检索到的资源数量
   */
  searchByResourceName(
    name: string,
    rowCount: number = 1,
    searchType: string = '名称',
  ) {
    if (this._parent_selector === 'aui-card') {
      CommonPage.waitElementPresent(this.aloSearch.auiSearch.inputbox);
      this.aloSearch.search(name, searchType);
    } else {
      // 按名称过滤
      const searchBox = $(`alo-card input`);
      CommonPage.waitElementPresent(searchBox);
      searchBox.clear();
      searchBox.sendKeys(name);
      browser.sleep(100);
    }

    if (rowCount > 0) {
      CommonPage.waitElementPresent($$(this._body_row_selector).first());
    }
    CommonPage.waitRowcountTextChangeTo($$(this._body_row_selector), rowCount);
    browser.sleep(1000);
  }

  /**
   * 表格为空时的显示
   */
  get noResult() {
    return $(this._parent_selector).$(this._noData_selector);
  }

  /**
   * 检查列表页搜索框是否显示
   */
  checkSearchBoxIsPresent() {
    CommonPage.waitElementPresent(this.aloSearch.auiSearch.inputbox);
    return this._searchBox.isPresent();
  }
}
