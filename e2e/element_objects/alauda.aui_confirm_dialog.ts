/**
 * aui-dailog 控件
 * Created by liuwei on 2018/10/19.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';
import { AlaudaInputbox } from './alauda.inputbox';

export class AuiConfirmDialog extends AlaudaElementBase {
  private _root: ElementFinder;
  private _icon: string;
  private _content: string;
  constructor(auiDialog: ElementFinder) {
    super();
    this._root = auiDialog;
    this._icon = '.aui-confirm-dialog__title aui-icon';
    this._content = '.aui-confirm-dialog__content';
  }

  /**
   * 获得dialog控件
   */
  get auiConfirmDialog(): ElementFinder {
    this.waitElementPresent(this._root);
    return this._root;
  }

  /**
   * Dialog 控件的标题
   */
  get title(): ElementFinder {
    return this.auiConfirmDialog.$('.aui-confirm-dialog__title');
  }

  get aui_icon(): AuiIcon {
    return new AuiIcon(this.auiConfirmDialog.$(this._icon));
  }

  /**
   * Dialog 控件的内容
   */
  get content(): ElementFinder {
    return this.auiConfirmDialog.$(this._content);
  }

  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(
      this.auiConfirmDialog.$('.aui-confirm-dialog__confirm-button'),
    );
  }

  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton(
      this.auiConfirmDialog.$('.aui-confirm-dialog__cancel-button'),
    );
  }

  /**
   * 单击确定按钮
   */
  clickConfirm(): promise.Promise<void> {
    this.buttonConfirm.click();
    this.waitElementNotPresent(this._root).then(isNotPresent => {
      if (!isNotPresent) {
        throw new Error('单击确认按钮后，确认对框框没有关闭');
      }
    });
    return browser.sleep(1);
  }

  get deleteDialog_name(): AlaudaInputbox {
    return new AlaudaInputbox(this.auiConfirmDialog.$('input'));
  }
  /**
   * 删除按钮
   */
  get buttonDelete(): AlaudaButton {
    return new AlaudaButton(
      this.auiConfirmDialog.$('.aui-dialog__footer .aui-button--danger'),
    );
  }
  /**
   * 单击确定按钮
   */
  clickDelete(name): promise.Promise<void> {
    this.deleteDialog_name.input(name);
    browser.sleep(100);
    this.buttonDelete.click();
    this.waitElementNotPresent(this._root).then(isNotPresent => {
      if (!isNotPresent) {
        throw new Error('单击删除按钮后，确认对框框没有关闭');
      }
    });
    return browser.sleep(1);
  }

  /**
   * 单击取消按钮
   */
  clickCancel(): promise.Promise<void> {
    this.buttonCancel.click();
    return browser.sleep(100);
  }

  isPresent(): promise.Promise<boolean> {
    return this._root.isPresent();
  }

  WaitDialogNotPresent(): promise.Promise<void> {
    this.waitElementNotPresent(this._root, 30000).then(isNotPresent => {
      if (!isNotPresent) {
        throw new Error('单击确认按钮后，确认对框框没有关闭');
      }
    });
    return browser.sleep(1);
  }
}
