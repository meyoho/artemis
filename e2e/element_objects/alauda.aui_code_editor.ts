/**
 * 编辑YAML 的控件
 * Created by liuwei on 2018/10/19.
 */
import {
  ElementArrayFinder,
  ElementFinder,
  browser,
  promise,
} from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaAuiCodeEditor extends AlaudaElementBase {
  private _root;
  private _toolbarselector;

  /**
   * 编辑器
   *
   * @parameter {codeEditor} code 编辑器
   * @parameter {toolbarselector} code 编辑器上面的 toolbar
   *
   */
  constructor(
    codeEditor: ElementFinder,
    toolbarSelector = '.aui-code-editor-toolbar__control-button',
  ) {
    super();
    this._root = codeEditor;
    this._toolbarselector = toolbarSelector;
  }

  /**
   * 编辑器控件，根据此元素可以找到下面的所有元素
   *
   * @return ElementFinder 对象
   */
  get codeEditor(): ElementFinder {
    this.waitElementPresent(this.codeEditor);
    return this._root;
  }

  /**
   * 控件是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this.codeEditor.isPresent();
  }

  /**
   * 编辑器上面的toolbar,
   *
   * @return element list 对象, 获得toolbar 上的所有按钮
   */
  get toolbar(): ElementArrayFinder {
    return this.codeEditor.$$(this._toolbarselector);
  }

  /**
   * 按照名称单击工具栏上的按钮
   * @param name 工具栏按钮上的名称
   * @example clickToolbarByName('日间')
   */
  clickToolbarByName(name: string): promise.Promise<void> {
    this.toolbar
      .filter(elem => {
        return elem.getText().then(function(text) {
          return text === name;
        });
      })
      .first()
      .click();
    return browser.sleep(100);
  }

  /**
   * 获取编辑器里面的yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @return Promise 对象
   * @example   getYamlValue().then((yaml) => {
   *                console.log(yaml)
   *            })
   */
  getYamlValue(): promise.Promise<string> {
    browser.sleep(1000);
    const queryscript =
      'return monaco.editor.getModels().find(model => model.id === document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).getValue()';
    return browser.executeScript(queryscript);
  }

  /**
   * 编辑器里面的输入 yaml 值
   * 注意: 这里的javascript 只使用于发行版的项目，如果移到其它项目需要重写这个javascript
   *
   * @parameter {stringYaml} yaml 的值
   * @example let yamlstring = CommonMethod.readyamlfile('configmap.yaml', { '$NAME' : 'liuwei', '$NAMESPACE' : 'dddddd'});
   *          setYamlValue(yamlstring);
   *
   */
  setYamlValue(stringYaml: string): promise.Promise<void> {
    const setscript =
      'return monaco.editor.getModels().find(model => model.id === document.querySelector("aui-code-editor [model-id]").attributes["model-id"].value).setValue(`' +
      stringYaml +
      '`)';
    browser.executeScript(setscript);
    return browser.sleep(500);
  }
}
