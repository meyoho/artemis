/**
 * AuiDropdownButton 下拉框列表控件
 * Created by liuwei on 2018/9/13.
 */
import { ElementFinder, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiToolTip } from './alauda.auiToolTip';
import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';

export class AuiDropdownButton extends AlaudaElementBase {
    private _auiDropdownButton: ElementFinder;
    private _auiToolTip: ElementFinder;

    /**
     * 构造函数
     * @param auiDropdownButton ElementFinder 类型，auiSelect 页面元素
     * @param auiToolTip ElementFinder 类型, auiToolTip 页面元素
     */
    constructor(auiDropdownButton: ElementFinder, auiToolTip: ElementFinder) {
        super();
        this._auiDropdownButton = auiDropdownButton;
        this._auiToolTip = auiToolTip;
    }

    /**
     * 内容按钮
     */
    get contentButton(): AlaudaButton {
        return new AlaudaButton(
            this._auiDropdownButton.$('.aui-dropdown-button__content')
        );
    }

    /**
     * Aui-select 控件的aui-icon 控件
     */
    get auiIcon(): AuiIcon {
        return new AuiIcon(
            this._auiDropdownButton.$('.aui-dropdown-button__trigger aui-icon')
        );
    }

    /**
     * Aui-select 控件的下拉框
     */
    get auiToolTip(): AuiToolTip {
        return new AuiToolTip(this._auiToolTip);
    }
    isPresent(): promise.Promise<boolean> {
        return this.contentButton.isPresent();
    }

    /**
     * 获得文本框中的输入值
     */
    get text(): promise.Promise<string> {
        return this.contentButton.getText();
    }

    /**
     * 单击内容按钮
     */
    click(): promise.Promise<void> {
        return this.contentButton.click();
    }

    /**
     * 单击 icon, 从下拉框中选择一个item
     * @param innerText 要选择的item
     */
    select(innerText: string): promise.Promise<void> {
        this.auiIcon.click();
        return this.auiToolTip.selectAui_menu_item(innerText);
    }
}
