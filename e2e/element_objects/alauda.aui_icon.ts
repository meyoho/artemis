/**
 * aui-icon 控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AuiIcon extends AlaudaElementBase {
  private _auiIcon: ElementFinder;
  constructor(auiIcon: ElementFinder) {
    super();
    this._auiIcon = auiIcon;
  }

  /**
   * 获得开关控件
   */
  get auiIcon(): ElementFinder {
    this.waitElementPresent(this._auiIcon);
    return this._auiIcon;
  }

  /**
   * 单击
   */
  click(): promise.Promise<void> {
    this.scrollToView(this.auiIcon).then(() => {
      this.auiIcon.click();
    });
    return browser.sleep(100);
  }
}
