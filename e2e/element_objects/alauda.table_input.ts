/**
 * Created by 李俊磊 on 2019/7/4.
 * 列表类型输入框 如pv创建页的标签、注释
 */
import { ElementFinder, by, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';
import { AlaudaInputbox } from './alauda.inputbox';

interface HeaderCell {
  index: number;
  name: string;
}

export class TableInput extends AlaudaElementBase {
  private _auiTable;
  private _headerCellSelector: string;
  private _cellSelector: string;
  private _rowSelector: string;
  constructor(
    tableElement: ElementFinder,
    headerCellSelector = 'th',
    bodyCellSelector = 'td',
    rowCellSelector = 'tr[rowtemplateindex="0"]',
  ) {
    super();
    this._auiTable = tableElement;
    this._headerCellSelector = headerCellSelector;
    this._cellSelector = bodyCellSelector;
    this._rowSelector = rowCellSelector;
  }

  get auiTable(): ElementFinder {
    this.waitElementPresent(this._auiTable);
    return this._auiTable;
  }
  /**
   * 根据列名获得表格共几列，{name}列在第几列
   *
   * @parameter {name} 列名， string 类型
   */
  getColumeIndexByColumeName(name) {
    const headerCell = this.auiTable
      .$$(this._headerCellSelector)
      .map((elem, index) => {
        return {
          index: index,
          name: elem.getText(),
        };
      });
    // 遍历表头，返回{name}列在第几列，共几列
    return headerCell.then(headerCellList => {
      let columeIndex = 0;
      headerCellList.forEach((item: HeaderCell) => {
        if (item.name === name) {
          columeIndex = item.index;
          return;
        }
      });
      return {
        columeCount: headerCellList.length,
        columeIndex: columeIndex,
      };
    });
  }
  /**
   * 获得表格的表头所有列名
   *
   */
  getHeaderText(): promise.Promise<string> {
    return this.auiTable.$$(this._headerCellSelector).getText();
  }
  /**
   * 获取第几行，行号从0开始
   * @param index 行号
   */
  getRowByIndex(index: number): ElementFinder {
    const rowlist = this.auiTable.$$(this._rowSelector);
    return rowlist.get(index);
  }
  /**
     * 根据列名和关键字获得一个单元格
     *
     * @parameter {name} 列名
     * @parameter {row_index} 数字类型，根据行索引找到唯一行
    });

    * @return 返回 {row_index} 找到的唯一行，所在{name} 列的单元格
    */
  getCell(name, row_index): promise.Promise<ElementFinder> {
    const body_cell_selector = this._cellSelector;
    // 根据索引找到行
    const row = this.getRowByIndex(row_index);
    // 根据列名，返回第几列单元格
    return this.getColumeIndexByColumeName(name).then(colume => {
      return row.$$(body_cell_selector).get(colume.columeIndex);
    });
  }
  /**
   *
   * @param colum_name 列名
   * @param row_index 行索引
   * @param value 要输入的值
   */
  inputValue(colum_name: string, row_index: number, value: string) {
    this.getCell(colum_name, row_index).then(ele => {
      const input = new AlaudaInputbox(ele.$('input'));
      input.value.then(v => {
        console.log(`TableInput.inputValue ${v}:${value}`);
        if (v != value) {
          console.log(`TableInput.inputValue ${v}:${value}`);
          input.input(value);
        }
      });
    });
  }
  /**
   *  批量输入数据
   * @param values 数据 [{"colum_name":"键","row_index":0,"value":"key"},{"colum_name":"值","row_index":0,"value":"value"}...]
   */
  enterValues(values: Array<Map<string, any>>) {
    for (let i = 0; i < values.length; i++) {
      const arg = values[i];
      this.inputValue(
        arg.get('colum_name'),
        arg.get('row_index'),
        arg.get('value'),
      );
    }
  }
  /**
   * 输入列表数据
   * @param table_data 列表类型数据{"键":["key1","key2","key3"...],"值":["value1","value2","value3"...]}
   */
  inputTableValues(table_data: Record<string, any>) {
    for (const key in table_data) {
      const value = table_data[key];
      for (let i = 0; i < value.length; i++) {
        this.inputValue(key, i, value[i]);
      }
    }
  }
  /**
   * 删除指定的行
   * @param row_index 行索引
   */
  del_row(row_index: number) {
    this.getRowByIndex(row_index)
      .$('aui-icon')
      .click();
  }
  /**
   * 增加行
   */
  add_row() {
    this._auiTable.element(
      by.xpath('//td/descendant::*[contains(text(),"添加 ")]'),
    );
  }
}
