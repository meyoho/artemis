/**
 * auiSearch控件
 * Created by liuwei on 2018/8/10.
 */
import { ElementFinder, browser, promise, protractor } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './element.base';
import { AlaudaButton } from './alauda.button';

export class AuiSearch extends AlaudaElementBase {
  private _auiSearch: ElementFinder;
  private _inputboxSelector;
  private _clearIconSelector;
  private _searchButtonSelector;

  constructor(
    auiSearch: ElementFinder,
    inputboxSelector = '.aui-search input',
    clearIconSelector = 'div[class*="clear"] aui-icon',
    searchButtonSelector = 'div[class*="icon"] aui-icon',
  ) {
    super();
    this._auiSearch = auiSearch;
    this._inputboxSelector = inputboxSelector;
    this._clearIconSelector = clearIconSelector;
    this._searchButtonSelector = searchButtonSelector;
  }

  private get _root(): ElementFinder {
    this.waitElementPresent(this._auiSearch).then(isPresent => {
      if (!isPresent) {
        throw new Error(`auiSearch ${this._auiSearch.locator()} 控件没出现`);
      }
    });
    return this._auiSearch;
  }

  /**
   * 文本框
   */
  get inputbox(): ElementFinder {
    this.waitElementPresent(this._root.$(this._inputboxSelector));
    this.waitElementClickable(this._root.$(this._inputboxSelector));
    return this._root.$(this._inputboxSelector);
  }

  /**
   * 清除的小图标
   */
  get iconClear(): ElementFinder {
    return this._root.$(this._clearIconSelector);
  }

  /**
   * 检索的小图标
   */
  get iconSearch(): AlaudaButton {
    this.waitElementPresent(this._root.$(this._searchButtonSelector));
    return new AlaudaButton(this._root.$(this._searchButtonSelector));
  }

  /**
   * 在文本框中输入值
   * @param text 文本框中要输入的值
   */
  _input(text: string): promise.Promise<void> {
    this.iconClear.isDisplayed().then(isDisplayed => {
      if (isDisplayed) {
        new AlaudaButton(this.iconClear).click();
      }
    });
    this.inputbox.clear();
    this.inputbox.sendKeys(text);
    return browser.sleep(1);
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }

  /**
   * 检索
   * @param value 要检索的值
   */
  search(value: string) {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('css 选择器:' + this._inputboxSelector);
        console.log('aui-search 控件没有正常加载');
      }
      this._input(value);
      this.waitElementClickable(this.iconSearch.button);
      this.iconSearch.click();
    });

    return browser.sleep(1);
  }

  /**
   * 适用于没有搜索按钮的搜索框
   * @param value 要检索的值
   */
  onlySearch(value: string): promise.Promise<void> {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('css 选择器:' + this._inputboxSelector);
        console.log('aui-search 控件没有正常加载');
      }
      this._input(value);
    });

    return browser.sleep(1);
  }
  /**
   * 适用于没有搜索按钮的点击回车的搜索框
   * @param value 要检索的值
   */
  enterSearch(value: string): promise.Promise<void> {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('css 选择器:' + this._inputboxSelector);
        console.log('aui-search 控件没有正常加载');
      }
      this._input(value);
      this.inputbox.sendKeys(protractor.Key.ENTER);
    });

    return browser.sleep(1);
  }

  /**
   * 清空检索框
   */
  clear(): promise.Promise<void> {
    CommonPage.waitElementPresent(this.inputbox).then(isPresent => {
      if (!isPresent) {
        console.log('aui-search 控件没有正常加载');
      }
    });
    this.iconClear.click();
    return browser.sleep(1);
  }
}
