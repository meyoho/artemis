/**
 * alo-search控件, 由dropdown 控件，AuiSearch 控件组成
 * 管理
 * Created by liuwei on 2018/8/10.
 */
import { $, $$, ElementFinder, browser, promise } from 'protractor';

import { AuiSearch } from './alauda.auiSearch';
import { AlaudaDropdown } from './alauda.dropdown';

export class AloSearch {
    private _aloSearch: ElementFinder;
    private _dropdown_iconSelector: string;
    private _dropdown_itemListSelector: string;
    private _auiSearchSelector: string;

    constructor(
        aloSearch: ElementFinder = $('.alo-search'),
        auiSearchSelector: string = 'aui-search',
        iconButton: string = 'button aui-icon', // dropdown 的icon
        itemList: string = 'aui-tooltip aui-menu-item button' // dropdown 的下拉框的items
    ) {
        this._aloSearch = aloSearch;
        this._dropdown_iconSelector = iconButton;
        this._dropdown_itemListSelector = itemList;
        this._auiSearchSelector = auiSearchSelector;
    }

    /**
     * 检索框左侧的dropdwon 控件
     */
    get dropdown() {
        return new AlaudaDropdown(
            this._aloSearch.$(this._dropdown_iconSelector),
            $$(this._dropdown_itemListSelector)
        );
    }

    /**
     * 检索框控件
     */
    get auiSearch() {
        return new AuiSearch(this._aloSearch.$(this._auiSearchSelector));
    }

    /**
     * 按照检索类型，检索
     * @param searchValue 检索内容
     * @param selectType 检索类型
     * @example 按照名称检索 search('asm-demo'),
     * @example 按照显示名称检索 search('asm-demo')
     */
    search(
        searchValue: string,
        selectType: string = '名称'
    ): promise.Promise<void> {
        return this.dropdown.isPresent().then(isPresent => {
            if (isPresent) {
                // 如果 dropdown 控件存在，选择一个选项
                if (selectType !== '名称') {
                    this.dropdown.select(selectType);
                    browser.sleep(1);
                }
                // 在检索框中输入内容检索
                this.auiSearch.search(searchValue);
                return browser.sleep(1);
            } else {
                // 在检索框中输入内容检索
                this.auiSearch.search(searchValue);
                return browser.sleep(1);
            }
        });
    }

    /**
     * 适用于没有搜索按钮的搜索框
     */
    onlySearch(
        searchItem: string,
        selectItem: string = '名称'
    ): promise.Promise<void> {
        return this.dropdown.isPresent().then(isPresent => {
            if (isPresent) {
                // 如果 dropdown 控件存在，选择一个选项
                if (selectItem !== '名称') {
                    this.dropdown.select(selectItem);
                    browser.sleep(100);
                }
                // 在检索框中输入内容检索
                this.auiSearch.onlySearch(searchItem);
                return browser.sleep(100);
            } else {
                // 在检索框中输入内容检索
                this.auiSearch.onlySearch(searchItem);
                return browser.sleep(100);
            }
        });
    }
}
