/**
 * Created by liuwei on 2019/8/15.
 */

import { $, ElementArrayFinder, ElementFinder } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaLogEvents extends AlaudaElementBase {
    private _root: ElementFinder;
    private _logCountcssSelector: string;
    private _highchartSeriesSelector: string;

    constructor(
        logResult: ElementFinder = $('.rc-log-dashboard-events'),
        logCountcssSelector: string = '.logCount',
        highchartSeriesSelector: string = 'highcharts-chart g rect'
    ) {
        super();
        this._root = logResult;
        this._logCountcssSelector = logCountcssSelector;
        this._highchartSeriesSelector = highchartSeriesSelector;
    }

    get root(): ElementFinder {
        this.waitElementPresent(this._root);
        return this._root;
    }

    /**
     * 左上角日志个数
     */
    get logCount() {
        this.waitElementPresent(this.root.$(this._logCountcssSelector));
        return this.root.$(this._logCountcssSelector);
    }

    /**
     * 柱状图
     */
    get highchartsSeries(): ElementArrayFinder {
        const loglist = this.root.$$(this._highchartSeriesSelector);
        this.waitElementPresent(loglist.first()).then(isPresent => {
            if (!isPresent) {
                throw new Error('日志面板里面没有日志的柱状图');
            }
        });
        return loglist;
    }
}
