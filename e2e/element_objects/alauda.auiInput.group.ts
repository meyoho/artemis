/**
 * 文本框控件
 * 创建应用页面的容器大小部分，请求值，限制值用到了这个控件
 * Created by liuwei on 2018/9/10.
 */

import { $, ElementFinder, browser, promise } from 'protractor';

import { AuiSelect } from './alauda.auiSelect';
import { AlaudaElementBase } from './element.base';

/**
 * 复合控件，label + inputbox + aui-select
 * 左侧是提示文字， 中间是inputbox，右侧是单位aui-select
 */
export class AlaudaInputGroup extends AlaudaElementBase {
  private _inputGroup: ElementFinder;
  private _labelSelector: string;
  private _inputSelector: string;

  constructor(
    inputGroup: ElementFinder = $('aui-input-group'),
    labelSelector = '.aui-input-group div:nth-child(1) span',
    inputSelector = '.aui-input-group div:nth-child(1) .aui-input',
  ) {
    super();
    this._inputGroup = inputGroup;
    this._labelSelector = labelSelector;
    this._inputSelector = inputSelector;
  }

  get auiSelect(): AuiSelect {
    return new AuiSelect(
      this._inputGroup.$('aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }

  get label(): ElementFinder {
    return this._inputGroup.$(this._labelSelector);
  }

  /**
   * 获得文本框
   */
  get inputbox(): ElementFinder {
    const input = this._inputGroup.$(this._inputSelector);
    this.waitElementPresent(input);
    return input;
  }

  /**
   * 判断Inputbox是否显示
   */
  isPresent(): promise.Promise<boolean> {
    return this.inputbox.isPresent();
  }

  /**
   * 在文本框中输入一个值， 选择一个与该值对应的单位
   * @param inputValue 要输入的值
   * @param unitValue 选择一个单位
   */
  input(inputValue: string, unitValue?: string) {
    this.scrollToView(this.inputbox);
    this.inputbox.clear();
    this.inputbox.sendKeys(inputValue);
    browser.sleep(100);
    this.auiSelect.isPresent().then(isPresent => {
      if (isPresent) {
        return this.auiSelect.select(unitValue);
      }
    });
  }

  /**
   * 获取文本框的值
   */
  getText(): promise.Promise<string> {
    return this.inputbox.getText();
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }
}
