/**
 * aui_radio_group 按钮控件
 * 应用的创建页，添加计算组件页，（列表，YAML）有使用这个控件
 * Created by liuwei on 2018/8/27.
 */

import { $, ElementFinder } from 'protractor';

import { AlaudaButton } from '../element_objects/alauda.button';
import { AlaudaElementBase } from '../element_objects/element.base';
export class AlaudaRadioButton extends AlaudaElementBase {
    private _root: ElementFinder;
    private _contentSelector: string;

    constructor(
        root: ElementFinder = $('aui-radio-group'),
        contentSelector: string = '.aui-radio-button__content'
    ) {
        super();
        this._root = root;
        this._contentSelector = contentSelector;
    }

    /**
     * aui_radio_group 控件
     */
    get aui_radio_group(): ElementFinder {
        this.waitElementPresent(this._root).then(isPresent => {
            if (!isPresent) {
                console.log('alauda.radioButton 控件没有出现');
            }
        });
        return this._root;
    }

    /**
     * 根据名称单击单选按钮
     * @param name 单选按钮的名称
     * clickByName('YAML')
     */
    clickByName(name: string) {
        const temp = this.aui_radio_group
            .$$(`${this._contentSelector}`)
            .filter(elem => {
                return elem.getText().then(text => {
                    return text.trim() === name;
                });
            })
            .first();
        temp.isPresent().then(isPresent => {
            if (!isPresent) {
                this.aui_radio_group.getText().then(text => {
                    throw new Error(
                        `输入的单选按钮选项 '${name}' 写错了，\n页面上的单选按钮选项是: ${text}`
                    );
                });
            }
        });
        const button = new AlaudaButton(temp);
        return button.click();
    }
}
