/**
 * alo-params-input 控件
 * 应用创建页面的容器高级部分，参数控件
 * Created by liuwei on 2018/9/14.
 */
import { ElementFinder, promise } from 'protractor';

import { AlaudaButton } from './alauda.button';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';

export class AloParamsInput extends AlaudaElementBase {
  private _aloParamsInput: ElementFinder;
  private _inputSelector: string;
  private _addbuttonSelector: string;

  /**
   * 构造函数
   * @param inputbox ElementFinder 类型，inputbox 页面元素
   */
  constructor(
    aloPatamsInput: ElementFinder,
    inputSelector = 'input',
    addbuttonSelector = '.add-button',
  ) {
    super();
    this._aloParamsInput = aloPatamsInput;
    this._inputSelector = inputSelector;
    this._addbuttonSelector = addbuttonSelector;
  }

  /**
   * 获得 alo-params-input 控件
   */
  get aloPatamsInput(): ElementFinder {
    this.waitElementPresent(this._aloParamsInput);
    return this._aloParamsInput;
  }

  /**
   * 填写参数
   * @param parameters [‘bash’, '-c', '(http-server & ) && sleep 60'],
   */
  input(parameters: Array<string>) {
    this.aloPatamsInput
      .$$(this._inputSelector)
      .count()
      .then(count => {
        if (parameters.length > count) {
          const temp = parameters.length - count;
          let i = 0;
          for (i = 0; i < temp; i++) {
            this.click();
          }
        }
      });

    this.aloPatamsInput.$$(this._inputSelector).each((elem, index) => {
      const inputbox = new AlaudaInputbox(elem);
      inputbox.input(parameters[index]);
    });
  }

  /**
   * 单击第几行的减号按钮, 删除这个参数
   * @param index 行号
   * @example  clickRemoveButton(1)
   */
  clickRemoveButton(
    index,
    iconSelector = 'aui-icon[icon="basic:minus_circle"]',
  ) {
    this.aloPatamsInput.$$(iconSelector).each((elem, rowindex) => {
      if (rowindex === index) {
        const button = new AlaudaButton(elem);
        button.click();
      }
    });
  }

  /**
   * 单击控件底部的button, 增加一个参数
   * @example click();
   */
  click(): promise.Promise<void> {
    // 找到要操作的按钮
    const button = new AlaudaButton(
      this.aloPatamsInput.$(this._addbuttonSelector),
    );
    // 单击按钮
    return button.click();
  }
}
