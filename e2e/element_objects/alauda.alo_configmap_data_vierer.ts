/**
 * alo-configmap-data-viewer 控件，
 * 配置字典详情页，数据部分有使用
 * Created by liuwei on 2018/11/7.
 */

import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AloConfigmapDataViewer extends AlaudaElementBase {
    private _aloConfigmapDataView: ElementFinder;

    /**
     * 控件 alo-configmap-data-viewer
     */
    constructor(aloConfigmapDataViewer: ElementFinder) {
        super();
        this._aloConfigmapDataView = aloConfigmapDataViewer;
    }

    /**
     * 获得 alo-configmap-data-viewer 控件
     */
    get aloConfigmapDataViewer(): ElementFinder {
        this.waitElementPresent(this._aloConfigmapDataView).then(isPresent => {
            if (!isPresent) {
                throw new Error('控件 alo-configmap-data-viewer 不存在');
            }
        });
        return this._aloConfigmapDataView;
    }

    /**
     * alo-configmap-data-viewer 控件包含左右两个部分，左侧是键，右侧包含键和值
     * 这个函数主要用来判断左侧是键是否存在
     */
    hasData(): promise.Promise<boolean> {
        return this._aloConfigmapDataView.$('.config-data-keys').isPresent();
    }

    /**
     * 判断 alo-configmap-data-viewer 是否存在
     */
    isPresent(): promise.Promise<boolean> {
        return this._aloConfigmapDataView.isPresent();
    }

    /**
     * 单击左侧键的名称，获得右侧键对应的值
     * @param data_key 左侧键的名称
     * @example click('dbname').then((value) => { console.log(value)})
     */
    click(data_key: string): promise.Promise<string> {
        const item = this.aloConfigmapDataViewer
            .$$('.config-data-keys__label')
            .filter(elem => {
                return elem
                    .$('.config-data-keys__label-key')
                    .getText()
                    .then(text => {
                        return text.trim() === data_key;
                    });
            })
            .first();

        this.waitElementPresent(item).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `在控件 alo-configmap-data-viewer 中，没有找到[${data_key}]`
                );
            }
        });

        item.click();
        // 等待active 的切换
        browser.sleep(1000);
        return this.aloConfigmapDataViewer
            .$('.config-data-values div[class*="isActive"]')
            .$('.config-data-section__content')
            .getText();
    }
}
