/**
 * 文本框控件
 * Created by liuwei on 2019/8/28.
 */

import { ElementFinder, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaLabel extends AlaudaElementBase {
    private _root: ElementFinder;

    constructor(lableElem: ElementFinder) {
        super();
        this._root = lableElem;
    }

    get root() {
        this.waitElementPresent(this._root);
        return this._root;
    }

    /**
     * 获取文本框的值
     */
    getText(): promise.Promise<string> {
        return this.root.getText();
    }

    /**
     * 控件是否出现
     */
    isPresent(): promise.Promise<boolean> {
        return this.root.isPresent();
    }
}
