/**
 * alo-array-form-table 下拉框列表控件
 * 负载均衡 添加规则部分，用这个控件
 * Created by shijingnan on 2019/7/11.
 */
import { $$, ElementFinder } from 'protractor';

import { ArrayFormTable } from './alauda.arrayFormTable';
import { AuiIcon } from './alauda.aui_icon';
import { AuiSwitch } from './alauda.auiSwitch';
import { AlaudaDropdown } from './alauda.dropdown';
import { AlaudaElement } from './alauda.element';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaRadioButton } from './alauda.radioButton';
import { ArrayRuleIndicatorForm } from './alauda.ruleIndicator';
import { AlaudaElementBase } from './element.base';

export class ArrayRuleForm extends AlaudaElementBase {
    private _ArrayRuleForm: ElementFinder;
    private _iconAdd_Selector: string;
    private _iconAddNone_Selector: string;
    private _iconRemove_Selector: string;

    /**
     * 构造函数
     * @param arrayRuleForm ElementFinder 类型，alo-array-form-table 页面元素
     */
    constructor(
        arrayRuleForm: ElementFinder,
        iconAdd_Selector: string = '.add-rule button',
        iconAddNone__Selector: string = '.add-rule--none a',
        iconRemove_Selector: string = '.aui-card__header .basic-trash'
    ) {
        super();
        this._ArrayRuleForm = arrayRuleForm;
        this._iconAdd_Selector = iconAdd_Selector;
        this._iconAddNone_Selector = iconAddNone__Selector;
        this._iconRemove_Selector = iconRemove_Selector;
    }

    /**
     * 获得alk-array-from-table 控件
     */
    get ArrayRuleForm(): ElementFinder {
        this.waitElementPresent(this._ArrayRuleForm);
        return this._ArrayRuleForm;
    }
    /**
     * 添加规则按钮
     */
    get iconAdd() {
        return new AuiIcon(this.ArrayRuleForm.$(this._iconAdd_Selector));
    }
    /**
     * 无规则时的添加规则按钮
     */
    get iconAddNone() {
        return new AuiIcon(this.ArrayRuleForm.$(this._iconAddNone_Selector));
    }

    /**
   * 填写表格
   * @param parameters
   * @example 规则: [{
                规则描述: '域名+URL',
                内部路由组: [[namespace_name, service_name1, '80', 100, true]],
                会话保持: '源地址哈希',
                规则: [
                    ['域名', ['domainupdate']],
                    ['URL', [['StartsWith', '/urlupdate']]],
                ],
                'URL 重写': true,
                '重写地址': '/redirect'
            }]
   */
    fill(parameters) {
        this.ArrayRuleForm.$$(this._iconRemove_Selector).each(elem => {
            const iconRemove = new AuiIcon(elem);
            iconRemove.click();
        });
        for (let i = 0; i < parameters.length; i++) {
            if (i === 0) {
                this.iconAddNone.click();
            } else {
                this.iconAdd.click();
            }
            for (const name in parameters[i]) {
                if (parameters[i].hasOwnProperty(name)) {
                    const ruleElement = new AlaudaElement(
                        `.aui-form-item__rule .rule:nth-child(${i +
                            2}) rc-load-balancer-rule-form .aui-form-item`,
                        `.aui-form-item__rule .rule:nth-child(${i +
                            2}) rc-load-balancer-rule-form .aui-form-item .aui-form-item__label`
                    );
                    let element;
                    switch (name) {
                        case '内部路由组':
                            element = new ArrayFormTable(
                                ruleElement.getElementByText(
                                    name,
                                    'rc-array-form-table'
                                ),
                                '.rc-array-form-table__bottom-control-buttons button'
                            );
                            element.fill(parameters[i][name]);
                            break;
                        case '会话保持':
                            element = new AlaudaRadioButton(
                                ruleElement.getElementByText(
                                    name,
                                    'aui-radio-group'
                                )
                            );
                            element.clickByName(parameters[i][name]);
                            break;
                        case 'URL 重写':
                        case '证书':
                            element = new AuiSwitch(
                                ruleElement.getElementByText(name, 'aui-switch')
                            );
                            if (parameters[i][name]) {
                                element.open();
                            } else {
                                element.close();
                            }
                            break;
                        case '规则':
                            element = new ArrayRuleIndicatorForm(
                                ruleElement.getElementByText(
                                    name,
                                    '.aui-form-item__content'
                                )
                            );
                            element.fill(parameters[i][name]);
                            break;
                        case '命名空间':
                        case '保密字典':
                            element = new AlaudaDropdown(
                                ruleElement.getElementByText(
                                    name,
                                    'aui-select'
                                ),
                                $$('.cdk-overlay-pane aui-tooltip .aui-option')
                            );
                            element.select(parameters[i][name]);
                            break;
                        default:
                            element = new AlaudaInputbox(
                                ruleElement.getElementByText(name, 'input')
                            );
                            element.input(parameters[i][name]);
                            break;
                    }
                }
            }
        }
    }
}
