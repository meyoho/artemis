/**
 * Created by liuwei on 2018/8/27.
 */

import { $, ElementFinder } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaElement extends AlaudaElementBase {
  protected _root: ElementFinder;
  protected _parent_css_selector: string;
  protected _left_css_selector: string;

  /**
   * 在父元素中，查询到所有的行，根据左侧文字过滤确定唯一行后，再查询该行下的控件
   * @param all_row_selector  能够找到所有的行css，eg. aui-form-item>div
   * @param left_label_css_selector 找到行后，在行的子元素里面找左侧label的css，eg. label
   * @param root 包含左右结构的父元素
   */
  constructor(
    all_row_selector = 'aui-form-field[class]',
    left_label_css_selector = 'label[class*=aui-form-field]',
    root: ElementFinder = $('html body'),
  ) {
    super();
    this._parent_css_selector = all_row_selector;
    this._left_css_selector = left_label_css_selector;
    this._root = root;
  }

  get root() {
    this.waitElementPresent(this._root);
    return this._root;
  }

  /**
   * 页面上根据左侧的文字，查找右侧控件， 如何右侧控件找不到，返回整行
   * @param leftLabelText 左侧文字
   * @param targetSelector 右侧控件css 选择器
   */
  getElementByText(
    leftLabelText: string,
    targetSelector = 'input',
  ): ElementFinder {
    this.waitElementPresent(
      this.root.$$(`${this._parent_css_selector}`).first(),
      5000,
    ).then(isPresent => {
      if (!isPresent) {
        throw new Error(
          `根据左侧的文字，查找右侧控件: 根据行选择器 ${this._parent_css_selector} 没有定位到行元素`,
        );
      }
    });

    this.waitElementPresent(
      this.root.$$(`${this._left_css_selector}`).first(),
      5000,
    ).then(isPresent => {
      if (!isPresent) {
        throw new Error(
          `根据左侧的文字，查找右侧控件: 根据左侧选择器 ${this._left_css_selector} 没有定位到左侧元素` +
            `\n根据行元素选择器 ${this._parent_css_selector}` +
            `\n根据左侧选择器 ${this._left_css_selector}`,
        );
      }
    });

    const parentRows = this.root.$$(this._parent_css_selector);

    // 根据左侧文字定位到所在的行元素
    const parentRow = parentRows
      .filter(row => {
        return row
          .$$(`${this._left_css_selector}`)
          .first()
          .getText()
          .then(text => {
            // console.log('页面值:' + text);
            // console.log('左侧值:' + leftLabelText);
            // const temp =
            //   text
            //     .replace('*\n', '')
            //     .replace('：', '')
            //     .trim() === leftLabelText;
            // console.log('相等么?:' + temp);
            // console.log('==================');
            return (
              text
                .replace('*\n', '')
                .replace('：', '')
                .trim() === leftLabelText
            );
          });
      })
      .first();

    this.waitElementPresent(parentRow, 5000).then(isPresent => {
      if (!isPresent) {
        parentRows.getText().then(text => {
          throw new Error(
            `根据左侧的文字 “${leftLabelText}” 没有过滤出行元素控件` +
              `\n所有行元素的内容是: ${text}`,
          );
        });
      }
    });

    // 根据targetSelector 定位到行下边的目标元素
    const taggetElem = parentRow.$$(targetSelector).first();
    this.waitElementPresent(taggetElem, 5000).then(isPresent => {
      if (!isPresent) {
        throw new Error(
          `根据左侧的文字 “${leftLabelText}” 没有定位到右侧目标控件 ${targetSelector}` +
            `\n行定位器: ${parentRow.locator()} ` +
            `\n左侧定位器:${this._left_css_selector}` +
            `\n右侧定位器: ${targetSelector}`,
        );
      }
    });

    return taggetElem;
  }
}
