/**
 * 文本控件
 * Created by zhangjiao on 2018/3/2.
 */

import { ElementFinder, element, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './element.base';

export class AlaudaText extends AlaudaElementBase {
  private text;

  constructor(selector) {
    super();
    this.text = selector;
  }
  get root(): ElementFinder {
    this.waitElementPresent(element(this.text));
    return element(this.text);
  }

  /**
   * 判断text是否显示
   */
  checkTextIsPresent() {
    CommonPage.waitElementPresent(element(this.text));
    return element(this.text).isPresent();
  }

  checkTextIsNotPresent() {
    CommonPage.waitElementNotPresent(element(this.text), 2000);
    return element(this.text).isPresent();
  }

  getTextElem() {
    return element(this.text);
  }
  /**
   * 获得文本
   */
  getText() {
    return element(this.text).getText();
  }

  getTextAll(index) {
    return element
      .all(this.text)
      .get(index)
      .getText();
  }

  getValue() {
    return element(this.text).getAttribute('value');
  }

  getTextAllMap() {
    return element.all(this.text).map(item => item.getText());
  }

  checkTextAllIsPresent(index): promise.Promise<boolean> {
    CommonPage.waitElementPresent(element.all(this.text).get(index));
    return element
      .all(this.text)
      .get(index)
      .isPresent();
  }
  click() {
    CommonPage.waitElementPresent(element(this.text));
    element(this.text).click();
  }
}
