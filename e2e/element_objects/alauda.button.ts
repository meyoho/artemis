/**
 * 按钮控件
 * Created by liuwei on 2018/8/13.
 */

import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaButton extends AlaudaElementBase {
  private _button: ElementFinder;

  constructor(button: ElementFinder) {
    super();
    this._button = button;
  }

  get button(): ElementFinder {
    return this._button;
  }

  /**
   * 判断按钮是否存在
   */
  isPresent(): promise.Promise<boolean> {
    this.waitElementPresent(this._button);
    return this._button.isPresent();
  }

  /**
   * 单击按钮
   */
  click(): promise.Promise<void> {
    this.isPresent().then(isPresent => {
      if (!isPresent) {
        throw new Error(`按钮 ${this._button.locator()} 没出现`);
      }
    });
    this.waitElementClickable(this.button);
    this.waitElementNotHasArtribute(this.button);
    return browser.sleep(1).then(() => {
      this.scrollToView(this.button).then(() => {
        this.button.click();
      });
      // this.button.click();
    });
  }

  /**
   * 获得按钮的文字
   */
  getText(): promise.Promise<string> {
    this.isPresent().then(isPresent => {
      if (!isPresent) {
        throw new Error(`按钮 ${this._button.locator()} 没出现`);
      }
    });
    return this.button.getText();
  }
}
