/**
 * 按钮控件
 * Created by liuwei on 2019/7/25.
 */

import { $, $$, ElementFinder, browser } from 'protractor';

import { AuiSearch } from '../alauda.auiSearch';
import { AlaudaElementBase } from '../element.base';
import { AlaudaButton } from '../alauda.button';

export class AlaudaClusterSelect extends AlaudaElementBase {
  private _root: ElementFinder;
  private _itemsCssSelector: string;
  private _searchCssSelector: string;

  constructor(
    root: ElementFinder,
    searchCssSelector = 'aui-tooltip aui-search',
    itemsCssSelector = 'aui-menu-item button',
  ) {
    super();
    this._root = root;
    this._searchCssSelector = searchCssSelector;
    this._itemsCssSelector = itemsCssSelector;
  }

  private get root(): ElementFinder {
    this.waitElementPresent(this._root);
    this.waitElementClickable(this._root, 10000);
    return this._root;
  }

  private get auiSearch() {
    return new AuiSearch($(this._searchCssSelector));
  }

  /**
   * 选择一个项目
   * @param clusterName 项目名称
   */
  async select(clusterName: string) {
    const clusterElem = $('.display-name>span');
    await this.waitElementPresent(clusterElem);
    // 断言集群是否切换成功

    await clusterElem.getText().then(async text => {
      if (text.trim() != clusterName.split('(')[0].trim()) {
        // 如果要选择的集群和已经选中的集群不一致，选择集群，否则忽略
        await this.waitElementPresent(this.root, 20000);
        await this.waitElementClickable(this.root);
        await new AlaudaButton(this.root).click();
        await this.waitElementPresent($('aui-tooltip aui-search'));
        await browser.sleep(1000);
        await this.auiSearch.search(clusterName.split('(')[0].trim());
        const project = $$(this._itemsCssSelector)
          .filter(item => {
            return item.getText().then(text => {
              return (
                text.split('(')[0].trim() === clusterName.split('(')[0].trim()
              );
            });
          })
          .first();
        await this.waitElementPresent(project);
        await project.isPresent().then(isPresent => {
          if (!isPresent) {
            $$(this._itemsCssSelector).filter(item => {
              return item.getText().then(text => {
                console.log('text: ' + text);
                console.log('name: ' + clusterName);
                return (
                  text.split('(')[0].trim() === clusterName.split('(')[0].trim()
                );
              });
            });
            throw new Error(`要选择的集群${clusterName},在下拉框中没有找到`);
          } else {
            new AlaudaButton(project).click();
            // 断言集群是否切换成功
            $('.display-name>span')
              .getText()
              .then(text => {
                if (text.trim() != clusterName.split('(')[0].trim()) {
                  throw new Error(`切换集群失败[${clusterName}]`);
                }
              });
          }
        });
      }
    });
  }
}
