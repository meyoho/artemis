/**
 * 按钮控件
 * Created by liuwei on 2019/7/25.
 */

import { $, $$, ElementFinder } from 'protractor';

import { AuiSearch } from '../alauda.auiSearch';
import { AlaudaElementBase } from '../element.base';

export class AlaudaNamespaceSelect extends AlaudaElementBase {
    private _root: ElementFinder;

    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    private get root(): ElementFinder {
        this.waitElementPresent(this._root);
        this.waitElementClickable(this._root, 3000);
        return this._root;
    }

    private get auiSearch() {
        return new AuiSearch($('aui-tooltip aui-search'));
    }

    /**
     * 选择一个项目
     * @param projectName 项目名称
     */
    select(clusterName: string, nsName: string) {
        const name = nsName.split(' ')[0].trim();
        this.root.click();
        this.auiSearch.search(name);
        const cluster = $$('.aui-menu-group .aui-menu-group__title')
            .filter(item => {
                return item.getText().then(text => {
                    return text === clusterName;
                });
            })
            .first();
        cluster.isPresent().then(isPresent => {
            if (!isPresent) {
                $$('aui-menu-item span:nth-child(1)').filter(item => {
                    return item.getText().then(text => {
                        console.log('text: ' + text);
                        console.log('name: ' + clusterName);
                        return text === clusterName;
                    });
                });
                throw new Error(
                    `要选择的集群${clusterName},在下拉框中没有找到`
                );
            } else {
                const ns = cluster
                    .$('.aui-menu-group .aui-menu-item--default span')
                    .filter(item => {
                        return item.getText().then(text => {
                            return text === name;
                        });
                    })
                    .first();

                ns.isPresent().then(isExist => {
                    if (isExist) {
                        ns.click();
                    } else {
                        throw new Error(
                            `要选择的命名空间${name},在下拉框中没有找到`
                        );
                    }
                });
            }
        });
    }
}
