/**
 * 切换用户视角的控件
 */
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class ViewSwitch extends AlaudaElementBase {
  private _root: ElementFinder;
  private _number;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
    this._number = 0;
  }

  private get aui_icon() {
    this.waitElementPresent(this._root.$('aui-icon')).then(isPresent => {
      if (!isPresent) {
        throw new Error('切换用户视图的按钮没有出现');
      }
    });
    this.waitElementClickable(this._root.$('aui-icon'));
    return this._root.$('aui-icon');
  }

  private get aui_icon_userView() {
    this.waitElementPresent(this._root.$('aui-icon[icon*="project"]'));
    return this._root.$('aui-icon[icon*="project"]');
  }

  private get aui_icon_adminView() {
    this.waitElementPresent(this._root.$('aui-icon[icon*="setting"]'));
    return this._root.$('aui-icon[icon*="setting"]');
  }

  /**
   * 切换业务视图
   */
  switchUserView() {
    this.aui_icon.getAttribute('icon').then(icon => {
      if (icon.includes('setting')) {
        // 如果在管理视图，切换
        this.aui_icon.click();
        this.aui_icon_userView.isPresent().then(isPresent => {
          if (!isPresent) {
            throw new Error('切换业务视图失败');
          }
        });
      }
    });
  }

  /**
   * 切换管理视图
   */
  switchAdminView() {
    this.aui_icon.getAttribute('icon').then(icon => {
      if (icon.includes('project')) {
        // 如果在业务视图，切换
        this.aui_icon.click();
        this.aui_icon_adminView.isPresent().then(isPresent => {
          if (!isPresent) {
            this._number = this._number + 1;
            if (this._number < 10) {
              this.switchAdminView();
            }

            //throw new Error('切换管理视图失败');
          }
        });
      }
    });
  }
}
