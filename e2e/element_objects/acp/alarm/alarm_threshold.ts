import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class AlarmThreshold extends AlaudaElementBase {
    private _root: ElementFinder;

    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get root() {
        this.waitElementPresent(this._root).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `告警创建的dialog页${this._root.locator()}没有出现`
                );
            }
        });
        return this._root;
    }

    input(data) {
        new AuiSelect(this.root.$('aui-select')).select(data[0]);
        new AlaudaInputbox(
            this.root.$('input[formcontrolname="threshold"]')
        ).input(data[1]);
    }
}
