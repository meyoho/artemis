import { AlaudaInputGroup } from '@e2e/element_objects/alauda.auiInput.group';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class AuthInfo extends AlaudaElementBase {
    private _root: ElementFinder;
    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get radiobutton() {
        return new AlaudaRadioButton(this._root.$('aui-radio-group'));
    }

    get username(): AlaudaInputGroup {
        return new AlaudaInputGroup(
            this._root.$('div aui-input-group:nth-child(1)')
        );
    }

    get secret() {
        return new AlaudaInputGroup(
            this._root.$('div aui-input-group:nth-child(2)')
        );
    }

    get port() {
        return new AlaudaInputGroup(
            this._root.$('div aui-input-group:nth-child(3)')
        );
    }

    getElementByText(text: string): any {
        switch (text) {
            case '类型':
                return this.radiobutton;
            case '用户名':
                return this.username;
            case '密钥地址':
                return this.secret;
            case '密码':
                return this.secret;
            case '端口':
                return this.port;
        }
    }

    input(textData) {
        for (const key in textData) {
            if (textData.hasOwnProperty(key)) {
                if (key === '类型') {
                    this.getElementByText(key).clickByName(textData[key]);
                } else {
                    this.getElementByText(key).input(textData[key]);
                }
            }
        }
    }
}
