import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { $, ElementFinder } from 'protractor';

export class LoadingBalancerCertificateForm extends AlaudaElementBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  get root() {
    this.waitElementPresent(this._root).then(isPresent => {
      if (!isPresent) {
        throw new Error(`规则控件${this._root.locator()}没有出现`);
      }
    });
    return this._root;
  }

  get alaudaElement() {
    return new AlaudaElement(
      'rc-load-balancer-certificate-form:not([hidden]) .aui-form-item',
      'rc-load-balancer-certificate-form:not([hidden]) .aui-form-item  .aui-form-item__label',
      this.root,
    );
  }

  /**
     * 填充数据
     * @param testData 测试数据
     * { 
         命名空间: 'ns',
         保密字典: 'secret'
     * },
     
     */
  input(testData) {
    for (const key in testData) {
      const aui_select = new AuiSelect(
        this.alaudaElement.getElementByText(key, 'aui-select'),
        $('.cdk-overlay-pane aui-tooltip'),
      );
      aui_select.select(testData[key]);
    }
  }
}
