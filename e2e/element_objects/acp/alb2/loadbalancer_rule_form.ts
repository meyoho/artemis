import { ArrayFormTable } from '@e2e/element_objects/alauda.arrayFormTable';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AuiSwitch } from '@e2e/element_objects/alauda.auiSwitch';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder, element, by } from 'protractor';

import { LoadingBalancerRule } from './loadbalancer_rule';

export class LoadingBalancerRuleForm extends AlaudaElementBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  get root() {
    this.waitElementPresent(this._root).then(isPresent => {
      if (!isPresent) {
        throw new Error(`规则控件${this._root.locator()}没有出现`);
      }
    });
    return this._root;
  }

  get priority() {
    return this.root.$('.priority span');
  }

  get upButton() {
    return this.root.$('.aui-card__header aui-icon[icon*="up"]');
  }

  get downButton() {
    return this.root.$('.aui-card__header aui-icon[icon*="down"]');
  }

  get deleteButton() {
    return this.root.$('.aui-card__header aui-icon[icon*="trash"]');
  }

  get alaudaElement() {
    return new AlaudaElement(
      'rc-load-balancer-rule-form aui-form-item:not([hidden]) .aui-form-item',
      'rc-load-balancer-rule-form aui-form-item:not([hidden]) .aui-form-item  .aui-form-item__label',
      this.root,
    );
  }
  _getElementByText(left: string, tagname = 'input'): ElementFinder {
    const xpath = `//rc-load-balancer-rule-form//aui-form-item[not(@hidden)]//label[@class="aui-form-item__label" and text()="${left}" ]/ancestor::aui-form-item[1]//${tagname}`;
    this.waitElementPresent(
      element(by.xpath(xpath)),
      //   `没有找到右侧控件${element(by.xpath(xpath)).locator()}`,
    );
    return element(by.xpath(xpath));
  }

  getElementByText(text): ElementFinder {
    switch (text) {
      case '规则描述':
      case '重写地址':
        return this._getElementByText(text, 'input');
      case '会话保持属性':
        return this._getElementByText(' 会话保持属性 ', 'input');
      case '证书':
        return this._getElementByText(text, 'aui-switch');
      case '命名空间':
      case '保密字典':
        return this._getElementByText(text, 'aui-select');
      case '内部路由组':
        return this._getElementByText(text, 'rc-array-form-table');

      case '规则':
        return this._getElementByText(
          text,
          'div[@class="aui-form-item__container"]',
        );
      case '会话保持':
        return this._getElementByText(text, 'aui-radio-group');
      case 'URL 重写':
        return this._getElementByText(text, 'aui-switch');
    }
  }

  enterValue(name, value) {
    this.root.getLocation().then(location => {
      this.scrollToWindow(location.x, location.y);
    });
    //this.scrollToBoWindowBottom();
    switch (name) {
      case '规则描述':
      case '会话保持属性':
      case '重写地址':
        new AlaudaInputbox(this.getElementByText(name)).input(value);
        break;
      case 'URL 重写':
        const auiSwitch = new AuiSwitch(this.getElementByText(name));
        auiSwitch.isDisplayed().then(isDisplayed => {
          if (isDisplayed) {
            if (value === 'true') {
              auiSwitch.open();
            } else {
              auiSwitch.close();
            }
          }
        });
        break;
      case '证书':
        // {'true': {命名空间: ns, 保密字典: secret}}
        const auiSwitch1 = new AuiSwitch(this.getElementByText(name));
        auiSwitch1.isDisplayed().then(isDisplayed => {
          if (isDisplayed) {
            for (const key in value) {
              if (key === 'true') {
                auiSwitch1.open();
                const certSelect = new AuiSelect(
                  this._getElementByText('证书', 'aui-select'),
                );
                certSelect.select(
                  value[key]['命名空间'],
                  value[key]['保密字典'],
                );
              } else {
                auiSwitch1.close();
              }
            }
          }
        });
        break;
      case '命名空间':
      case '保密字典':
        new AuiSelect(this.getElementByText(name)).select(value);
        break;

      case '内部路由组':
        new ArrayFormTable(
          this.getElementByText(name),
          'rc-array-form-table  .rc-array-form-table__bottom-control-buttons  button',
        ).fill(value);
        break;
      case '规则':
        new LoadingBalancerRule(this.getElementByText(name)).input(value);
        break;
      case '会话保持':
        new AlaudaRadioButton(this.getElementByText(name)).clickByName(value);
        break;
    }
  }

  /**
   * 填充数据
   * @param testData 测试数据
   */
  input(
    testData = {
      规则描述: 'description',
      证书: { false: { 命名空间: 'ns', 保密字典: 'secret' } },
      命名空间: 'abc',
      保密字典: 'abc',
      内部路由组: [['namespace_name', 'service_name2', '80', 100]],
      规则: [
        {
          Header: ['Cookie', 'Cookie test data'],
          Data: [{ Range: ['a', 'b'] }, { Equal: ['10'] }],
        },
      ],
      会话保持: '不会话保持',
      'URL 重写': 'false',
      重写地址: 'url',
    },
  ) {
    for (const key in testData) {
      this.scrollToBoWindowBottom();
      this.enterValue(key, testData[key]);
    }
  }
}
