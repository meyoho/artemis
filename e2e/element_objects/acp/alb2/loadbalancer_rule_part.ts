import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { $, ElementArrayFinder, ElementFinder } from 'protractor';

export class LoadingBalancerRulePart extends AlaudaElementBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  get root() {
    this.waitElementPresent(this._root).then(isPresent => {
      if (!isPresent) {
        throw new Error(`规则控件${this._root.locator()}没有出现`);
      }
    });
    return this._root;
  }

  get liList(): ElementArrayFinder {
    return this.root.$$('ul li .key-indicator');
  }

  get headerAuiSelect() {
    return new AuiSelect(
      this.root.$('.rule-form-header>aui-select'),
      $('.cdk-overlay-pane aui-tooltip'),
    );
  }

  get headerInput(): ElementFinder {
    return this.root.$(
      '.rule-form-header>input:not([disabled]):not([disabled])',
    );
  }

  get ruleAddButton(): AlaudaButton {
    return new AlaudaButton(this.root.$('li button'));
  }

  /**
   * 填充数据
   * @param testData 测试数据
   * { Header: [Cookie, 'Cookie test data'],
   *   Data: [ {Range: ['a', 'b']},
   *           {Equal: ['10']}
   *         ]
   * },
   * {
   * Header: ['域名'],
   * Data: [  { rule: '*.ext' },
   *          { '': 'full' }
   *       ]
   * }
   * 域名Data:key为前缀，value为域名
   */
  input(testData) {
    for (const key in testData) {
      this.scrollToBoWindowBottom();
      switch (key) {
        case 'Header':
          console.log(`输入规则的Header： ${testData['Header'][0]}`);
          this.headerAuiSelect.select(testData['Header'][0]);
          this.headerInput.isPresent().then(isPresent => {
            if (isPresent) {
              const inputBox = new AlaudaInputbox(this.headerInput);
              inputBox.input(testData['Header'][1]);
            }
          });
          break;
        case 'Data':
          this.liList.count().then(count => {
            const length = testData['Data'].length - count;
            for (let i = 0; i < length; i++) {
              this.ruleAddButton.click();
            }
            this.liList.each((elem, index) => {
              const auiSelect = new AuiSelect(
                elem.$('aui-select'),
                $('.cdk-overlay-pane aui-tooltip'),
              );
              // ruleData = {Range: ['a', 'b']}
              const ruleData = testData['Data'][index];
              for (const ruleTypeKey in ruleData) {
                console.log(
                  `输入规则 ${ruleTypeKey}：${ruleData[ruleTypeKey]} `,
                );
                if (testData['Header'][0] === '域名') {
                  auiSelect.select(ruleData[ruleTypeKey]);
                  elem
                    .$$('.key-indicator input:not([hidden]):not([disabled])')
                    .each(ruleElem => {
                      const ruleInput = new AlaudaInputbox(ruleElem);
                      ruleInput.input(ruleTypeKey);
                    });
                } else {
                  auiSelect.select(ruleTypeKey);
                  elem
                    .$$('.match-value input:not([hidden]):not([disabled])')
                    .each((ruleElem, indexData) => {
                      const ruleInput = new AlaudaInputbox(ruleElem);

                      ruleInput.input(ruleData[ruleTypeKey][indexData]);
                    });
                }
              }
            });
          });

          break;
      }
    }
  }
}
