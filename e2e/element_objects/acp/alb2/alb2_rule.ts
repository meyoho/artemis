import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

import { LoadingBalancerRuleForm } from './loadbalancer_rule_form';

export class Alb2Rule extends AlaudaElementBase {
    private _root: ElementFinder;
    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get root() {
        this.waitElementPresent(this._root).then(isPresent => {
            if (!isPresent) {
                throw new Error(`规则控件${this._root.locator()}没有出现`);
            }
        });
        return this._root;
    }

    get description(): ElementFinder {
        return this.root.$('.description-container');
    }

    get noRuleButton(): ElementFinder {
        return this.root.$('.add-rule--none a');
    }

    get rulePart(): ElementFinder {
        return this.root.$('.aui-form-item__content .rule-item-container');
    }

    get addRuleButton() {
        return this.root.$('.add-rule button');
    }

    /**
     * 填充数据
     * @param testData 测试数据
     * [
     * { Header: [Cookie, 'Cookie test data'],
     *   Data: [ {Range: ['a', 'b']},
     *           {Equal: ['10']}
     *         ]
     * },
     * { Header: [Cookie, 'Cookie test data'],
     *   Data: [ {Range: ['a', 'b']},
     *           {Equal: ['10']}
     *         ]
     * }
     * ]
     */
    // input(
    //     testData = [
    //         {
    //             规则描述: 'description',
    //             证书: { false: { 命名空间: 'ns', 保密字典: 'secret' } },
    //             命名空间: 'abc',
    //             保密字典: 'abc',
    //             内部路由组: [['namespace_name', 'service_name2', '80', 100]],
    //             规则: [
    //                 {
    //                     Header: ['Cookie', 'Cookie test data'],
    //                     Data: [{ Range: ['a', 'b'] }, { Equal: ['10'] }]
    //                 }
    //             ],
    //             会话保持: '不会话保持',
    //             'URL 重写': 'false',
    //             重写地址: 'url'
    //         }
    //     ]
    // ) {
    //     this.ruleList.count().then(number => {
    //         this.noRuleButton.isPresent().then(isPresent => {
    //             if (isPresent) {
    //                 if (testData.length > number) {
    //                     this.waitElementClickable(this.noRuleButton);
    //                     this.noRuleButton.click();
    //                 }
    //             }
    //         });
    //         const ruleForm = new LoadingBalancerRuleForm(this.ruleList.get(0));
    //         ruleForm.input(testData[0]);
    //         for (let i = 1; i < testData.length; i++) {
    //             this.waitElementClickable(this.addRuleButton);
    //             this.scrollToBoWindowBottom();
    //             this.addRuleButton.click();
    //             const moreRuleForm = new LoadingBalancerRuleForm(
    //                 this.ruleList.get(i)
    //             );
    //             moreRuleForm.input(testData[i]);
    //         }
    //     });
    // }
    input(
        testData = {
            规则描述: 'description',
            证书: { false: { 命名空间: 'ns', 保密字典: 'secret' } },
            命名空间: 'abc',
            保密字典: 'abc',
            内部路由组: [['namespace_name', 'service_name2', '80', 100]],
            规则: [
                {
                    Header: ['Cookie', 'Cookie test data'],
                    Data: [{ Range: ['a', 'b'] }, { Equal: ['10'] }]
                }
            ],
            会话保持: '不会话保持',
            'URL 重写': 'false',
            重写地址: 'url'
        }
    ) {
        const ruleForm = new LoadingBalancerRuleForm(this.rulePart);
        ruleForm.input(testData);
    }
}
