import { AuiIcon } from '@e2e/element_objects/alauda.aui_icon';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

import { LoadingBalancerRulePart } from './loadbalancer_rule_part';

export class LoadingBalancerRule extends AlaudaElementBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  get root() {
    this.waitElementPresent(this._root).then(isPresent => {
      if (!isPresent) {
        throw new Error(`规则控件${this._root.locator()}没有出现`);
      }
    });
    return this._root;
  }

  get ruleList() {
    return this.root.$$(
      '.rule-part-form__body rc-load-balancer-rule-part-form',
    );
  }

  /**
   * 添加规则指标按钮
   */
  get addRuleButton() {
    return new AlaudaButton(this.root.$('.add-rule-indicator span'));
  }

  /**
   * 填充数据
   * @param testData 测试数据
   * [
   * { Header: [Cookie, 'Cookie test data'],
   *   Data: [ {Range: ['a', 'b']},
   *           {Equal: ['10']}
   *         ]
   * },
   * { Header: [Cookie, 'Cookie test data'],
   *   Data: [ {Range: ['a', 'b']},
   *           {Equal: ['10']}
   *         ]
   * }
   * ]
   */
  input(testData) {
    this.root
      .$$('.rule-part-form__body>.remove-indicator  aui-icon')
      .each(elem => {
        const iconRemove = new AuiIcon(elem);
        iconRemove.click();
      });
    for (let i = 0; i < testData.length; i++) {
      this.addRuleButton.button.getLocation().then(localtion => {
        this.scrollToWindow(localtion.x, localtion.y);
      });
      this.waitElementClickable(this.addRuleButton.button);
      this.addRuleButton.click();

      const rulePart = new LoadingBalancerRulePart(this.ruleList.get(i));
      rulePart.input(testData[i]);
    }
  }
}
