/**
 * 切换用户视角的控件
 */
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { $$, ElementArrayFinder, ElementFinder } from 'protractor';

export class AccountMenu extends AlaudaElementBase {
    private _root: ElementFinder;
    private _accountIconCssSelector: string;
    private _menuItemsCssSelector: string;
    constructor(
        root: ElementFinder,
        accountNameleftIconCssSelector: string = 'aui-icon:nth-child(1)',
        menuItemsCssSelector: string = 'aui-menu-item button'
    ) {
        super();
        this._root = root;
        this._accountIconCssSelector = accountNameleftIconCssSelector;
        this._menuItemsCssSelector = menuItemsCssSelector;
    }

    get account(): ElementFinder {
        this.waitElementPresent(
            this._root.$(this._accountIconCssSelector)
        ).then(isPresent => {
            if (!isPresent) {
                throw new Error('页面右上角用户信息控件没有出现');
            }
        });
        this.waitElementClickable(this._root.$(this._accountIconCssSelector));
        return this._root.$(this._accountIconCssSelector);
    }

    get itemList(): ElementArrayFinder {
        return $$(this._menuItemsCssSelector);
    }

    /**
     * 选择一个项，单击
     * @param name 项目名称
     */
    select(name: string) {
        this._root.getAttribute('class').then(value => {
            if (!value.includes('isActive')) {
                this.account.click();
            }
            const item: ElementFinder = this.itemList
                .filter(elem => {
                    return elem.getText().then(text => {
                        return text.trim() === name;
                    });
                })
                .first();
            this.waitElementPresent(item);
            item.isPresent().then(isPresent => {
                if (!isPresent) {
                    this.itemList.filter(elem => {
                        return elem.getText().then(text => {
                            console.log('text:' + text);
                            console.log('name:' + name);
                            return text.trim() === name;
                        });
                    });
                    throw new Error(`页面右上角，用户菜单项${name}没有找到`);
                } else {
                    item.click();
                }
            });
        });
    }
}
