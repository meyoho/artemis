import { AuiSearch } from '@e2e/element_objects/alauda.auiSearch';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { $, ElementArrayFinder, ElementFinder, browser } from 'protractor';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class Resource extends AlaudaElementBase {
  private _root: ElementFinder;
  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  get auiSearch() {
    return new AuiSearch(this._root.$('.resource__nav__header aui-search'));
  }

  /**
   * 命名空间相关
   */
  get relatedWithNs(): ElementFinder {
    const elemWihNs = this._root.$(
      '.resource__nav__content .category-expansion-panel:nth-child(1) span',
    );
    this.waitElementPresent(elemWihNs);
    return elemWihNs;
  }

  /**
   * 命名空间相关
   */
  get relatedWithNsList(): ElementArrayFinder {
    this.waitElementPresent(this._root);
    return this._root.$$(
      '.resource__nav__content .category-list:nth-child(2) li',
    );
  }

  /**
   * 集群相关
   */
  get relatedWithCluster(): ElementFinder {
    const elem = this._root.$(
      '.resource__nav__content .category-expansion-panel:nth-child(3) span',
    );
    this.waitElementPresent(elem);
    return elem;
  }

  /**
   * 集群相关
   */
  get relatedWithClusterList(): ElementArrayFinder {
    this.waitElementPresent(this._root);
    return this._root.$$(
      '.resource__nav__content .category-list:nth-child(4) li',
    );
  }
  /**
   * 仅搜索左侧资源类型
   * @param resourceName 资源类型
   */
  onlysearch(resourceName: string) {
    this.auiSearch.enterSearch(resourceName);
    this.waitElementNotPresent($('.loading span'));
    //等待资源面板的数据发生变化
    browser.sleep(1000);
  }
  /**
   * 搜索左侧资源类型并点击
   * @param resourceName 资源类型
   */
  search(resourceName: string) {
    this.auiSearch.enterSearch(resourceName);
    this.waitElementNotPresent($('.loading span'));
    //等待资源面板的数据发生变化
    browser.sleep(1000);

    this._root
      .$$('li')
      .count()
      .then(count => {
        if (count > 0) {
          const elemLi = this._root
            .$$('li')
            .filter(elem => {
              return elem.getText().then(text => {
                return text === resourceName;
              });
            })
            .first();
          this.waitElementPresent(elemLi).then(isPresent => {
            if (isPresent) {
              new AlaudaButton(elemLi).click();
            }
          });
        }
      });

    return browser.sleep(1000);
  }
}
