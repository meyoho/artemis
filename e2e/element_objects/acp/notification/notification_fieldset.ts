import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

import { NotificationMethod } from './notification_method';

export class NotificationFieldSet extends AlaudaElementBase {
    private _root: ElementFinder;
    private _methodCssSelector: string;
    constructor(
        root: ElementFinder,
        methodCssSelector = 'div[class="notification-method"]'
    ) {
        super();
        this._root = root;
        this._methodCssSelector = methodCssSelector;
    }

    get root() {
        this.waitElementPresent(this._root).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `通知创建的dialog页，通知方式控件${this._root.locator()}没有出现`
                );
            }
        });
        return this._root;
    }

    get buttonAdd() {
        return new AlaudaButton(this.root.$('button'));
    }

    input(data) {
        this.root
            .$$(this._methodCssSelector)
            .count()
            .then(number => {
                data.forEach(() => {
                    if (number < data.length) {
                        this.buttonAdd.click();
                    }
                });
            });

        this._root.$$(this._methodCssSelector).each((methodRoot, index) => {
            const notificationMethod = new NotificationMethod(methodRoot);
            notificationMethod.input(data[index]);
        });
    }
}
