import { AlaudaAuiTagsInput } from '@e2e/element_objects/alauda.aui_tags_input';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class NotificationMethod extends AlaudaElementBase {
    private _root: ElementFinder;
    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get root() {
        this.waitElementPresent(this._root).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `通知创建的dialog页，通知方式控件${this._root.locator()}没有出现`
                );
            }
        });
        return this._root;
    }

    input(data) {
        const alaudaElement = new AlaudaElement(
            '.aui-form-item',
            'label',
            this._root
        );

        for (const key in data) {
            if (data.hasOwnProperty(key)) {
                switch (key) {
                    case '类型':
                        const typeElem = alaudaElement.getElementByText(
                            key,
                            'aui-select'
                        );
                        typeElem.isDisplayed().then(isDisplayed => {
                            if (isDisplayed) {
                                const dropDown = new AuiSelect(typeElem);
                                dropDown.select(data[key]);
                            }
                        });
                        break;
                    case '手机号码':
                    case '邮箱地址':
                        //AlaudaAuiTagsInput
                        const phoneElem = alaudaElement.getElementByText(
                            key,
                            'aui-tags-input'
                        );
                        phoneElem.isDisplayed().then(isDisplayed => {
                            if (isDisplayed) {
                                const tagInput = new AlaudaAuiTagsInput(
                                    phoneElem
                                );
                                data[key].forEach(phoneNum => {
                                    tagInput.input(phoneNum);
                                });
                            }
                        });
                        break;
                    case '备注':
                        //AlaudaAuiTagsInput
                        const itemElem = alaudaElement.getElementByText(
                            key,
                            'input'
                        );
                        itemElem.isDisplayed().then(isDisplayed => {
                            if (isDisplayed) {
                                const tagInput = new AlaudaInputbox(itemElem);
                                tagInput.input(data[key]);
                            }
                        });
                        break;
                    case '地址':
                    case '密钥':
                    case 'token':
                        //AlaudaAuiTagsInput
                        const itemElem1 = alaudaElement.getElementByText(
                            key,
                            'textarea'
                        );
                        itemElem1.isDisplayed().then(isDisplayed => {
                            if (isDisplayed) {
                                const tagInput = new AlaudaInputbox(itemElem1);
                                tagInput.input(data[key]);
                            }
                        });
                        break;
                }
            }
        }
    }
}
