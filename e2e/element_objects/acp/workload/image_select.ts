import { AuiDialog } from '@e2e/element_objects/alauda.aui_dialog';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaRadioButton } from '@e2e/element_objects/alauda.radioButton';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder, promise } from 'protractor';

export class ImageSelect extends AlaudaElementBase {
  private _auiDialog: ElementFinder;

  /**
   * 构造函数
   * @param auiDialog ElementFinder 类型，选择镜像仓库对话框,$('.cdk-global-overlay-wrapper aui-dialog')
   */
  constructor(auiDialog: ElementFinder) {
    super();
    this._auiDialog = auiDialog;
  }
  /**
   * 返回是否存在
   */
  get isPresent(): promise.Promise<boolean> {
    return this._auiDialog.isPresent();
  }
  /**
   * 获取选择镜像对话框
   */
  get imageDialog(): AuiDialog {
    this.waitElementPresent(this._auiDialog);
    return new AuiDialog(this._auiDialog);
  }

  get imageElement() {
    return new AlaudaElement('aui-dialog aui-form-item>div', 'label');
  }

  get methodRadio() {
    return new AlaudaRadioButton(
      this.imageElement.getElementByText('方式', 'aui-radio-group'),
      '.aui-radio-button__content',
    );
  }
  get addressInput() {
    return new AlaudaInputbox(
      this.imageElement.getElementByText('镜像地址', 'input'),
    );
  }
  get repoSelect() {
    return new AuiSelect(
      this.imageElement.getElementByText(
        '镜像仓库',
        'aui-select[formcontrolname=repository]',
      ),
    );
  }
  get tagSelect() {
    return new AuiSelect(
      this.imageElement.getElementByText(
        '镜像仓库',
        'aui-select[formcontrolname=tag]',
      ),
    );
  }

  /**
   * 选择镜像，用于创建应用、workload、任务等选择镜像的对话框
   * @param address 镜像地址,如果method是选择，相当于镜像仓库
   * @param method 输入 || 选择
   * @param tag 镜像仓库tag
   * @example enterImage('index.alauda.cn/alaudaorg/qaimages:helloworld')
   * @example enterImage('index.alauda.cn/alaudaorg/qaimages:helloworld','输入')
   * @example enterImage('10.0.128.96:31104/e2e-automation/helloworld','选择','latest')
   */
  enterImage(address, method = '输入', tag = '') {
    this.waitElementNotPresent(this.methodRadio.aui_radio_group, 3000);
    this.methodRadio.aui_radio_group.isPresent().then(isPresent => {
      if (isPresent) {
        this.methodRadio.clickByName(method);
        if (method === '输入') {
          this.addressInput.input(address);
        } else {
          this.repoSelect.select(address);
          this.tagSelect.select(tag);
        }
      } else {
        this.addressInput.input(address);
      }
    });
    return this.imageDialog.buttonConfirm.click();
  }
  /**
   * 选择完镜像点击取消按钮，用于创建应用、workload、任务等选择镜像的对话框
   * @param address 镜像地址,如果method是选择，相当于镜像仓库
   * @param method 输入 || 选择
   * @param tag 镜像仓库tag
   * @example enterImage('index.alauda.cn/alaudaorg/qaimages:helloworld')
   * @example enterImage('index.alauda.cn/alaudaorg/qaimages:helloworld','输入')
   * @example enterImage('10.0.128.96:31104/e2e-automation/helloworld','选择','latest')
   */
  enterImage_cancel(address, method = '输入', tag = '') {
    this.methodRadio.aui_radio_group.isPresent().then(isPresent => {
      if (isPresent) {
        this.methodRadio.clickByName(method);
        if (method === '输入') {
          this.addressInput.input(address);
        } else {
          this.repoSelect.select(address);
          this.tagSelect.select(tag);
        }
      } else {
        this.addressInput.input(address);
      }
    });
    this.imageDialog.buttonCancel.click();
  }
}
