/**
 * 时间选择控件
 */

import { ElementFinder, browser, promise, $$ } from 'protractor';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';

export class AlaudaTimePickr extends AlaudaElementBase {
  private _timepickr: ElementFinder;

  constructor(timepickr: ElementFinder) {
    super();
    this._timepickr = timepickr;
  }

  get timepickr(): ElementFinder {
    return this._timepickr;
  }

  /**
   * 判断按钮是否存在
   */
  isPresent(): promise.Promise<boolean> {
    this.waitElementPresent(this._timepickr);
    return this._timepickr.isPresent();
  }

  /**
   * 选择时间 例00:00
   */
  select(time: string): promise.Promise<void> {
    if (!time.includes(':')) {
      console.warn('时间格式不合法，例 00:00');
    } else {
      this.isPresent().then(isPresent => {
        if (!isPresent) {
          throw new Error(`按钮 ${this._timepickr.locator()} 没出现`);
        }
      });
      this.waitElementClickable(this.timepickr);
      this.timepickr.click();
      const hour_item = $$('.timepickr-hour .timepickr-item')
        .filter(elem => {
          return elem.getText().then(text => {
            return text.trim() === time.split(':')[0];
          });
        })
        .first();
      this.waitElementPresent(hour_item);
      hour_item.click();
      const minute_item = $$('.timepickr-minutes .timepickr-item')
        .filter(elem => {
          return elem.getText().then(text => {
            return text.trim() === time.split(':')[1];
          });
        })
        .first();
      this.waitElementPresent(minute_item);
      minute_item.click();
    }
    return browser.sleep(1);
  }
}
