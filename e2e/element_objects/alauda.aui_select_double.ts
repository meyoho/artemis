import { AlaudaElementBase } from './element.base';
import { ElementFinder, browser } from 'protractor';
import { AuiSelect } from './alauda.auiSelect';

export class AlaudaAuiSelectDouble extends AlaudaElementBase {
  private _root: ElementFinder;

  constructor(root: ElementFinder) {
    super();
    this._root = root;
  }

  select(data) {
    this._root.$$('aui-select').each((auiSelect, index) => {
      new AuiSelect(auiSelect).select(data[index]);
      browser.sleep(5);
    });
  }
}
