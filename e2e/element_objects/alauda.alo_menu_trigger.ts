/**
 * alo-menu-trigger 控件
 * 应用列表页，app 下的deployment 右侧的【3个点】 按钮， 单击后可选择更新操作
 * Created by liuwei on 2018/10/27.
 */
import { $, ElementFinder, browser, promise } from 'protractor';

import { AuiToolTip } from './alauda.auiToolTip';

export class AloMenuTigger {
    private _aloMenuTrigger: ElementFinder;
    private _auiTooltip: AuiToolTip;
    constructor(
        aloMenuTrigger: ElementFinder,
        auiTooltip: AuiToolTip = new AuiToolTip(
            $('.cdk-overlay-pane aui-tooltip')
        )
    ) {
        this._aloMenuTrigger = aloMenuTrigger;
        this._auiTooltip = auiTooltip;
    }

    /**
     * 选择一个操作
     * @param optionName 操作的名称，例如更新，删除等
     * @example select('更新')
     */
    select(optionName: string): promise.Promise<void> {
        this._aloMenuTrigger.click();
        this._auiTooltip.selectAui_menu_item(optionName);
        return browser.sleep(100);
    }
}
