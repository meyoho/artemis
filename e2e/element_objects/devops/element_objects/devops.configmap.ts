/**
 * 文本框控件
 * Created by liuwei on 2018/11/7.
 */

import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from '../../../element_objects/element.base';

export class DevopsConfigmapDataView extends AlaudaElementBase {
    private _aloConfigmapDataView: ElementFinder;

    /**
     * 控件 alo-configmap-data-viewer
     */
    constructor(aloConfigmapDataView: ElementFinder) {
        super();
        this._aloConfigmapDataView = aloConfigmapDataView;
    }

    /**
     * 根元素
     */
    get root(): ElementFinder {
        this.waitElementPresent(this._aloConfigmapDataView).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `控件 ${this._aloConfigmapDataView.locator()} 不存在`
                );
            }
        });
        return this._aloConfigmapDataView;
    }

    /**
     * 判断Inputbox是否显示
     */
    hasData(): promise.Promise<boolean> {
        return this._aloConfigmapDataView.$('.config-data-keys').isPresent();
    }

    /**
     * 判断Inputbox是否显示
     */
    hasPresent(): promise.Promise<boolean> {
        return this._aloConfigmapDataView.isPresent();
    }

    click(data_key: string): promise.Promise<string> {
        const item = this.root
            .$$('.config-data-keys__label')
            .filter(elem => {
                return elem
                    .$('.config-data-keys__label-key')
                    .getText()
                    .then(text => {
                        return text.trim() === data_key;
                    });
            })
            .first();

        this.waitElementPresent(item).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `在控件 alo-configmap-data-viewer 中，没有找到[${data_key}]`
                );
            }
        });

        item.click();
        // 等待active 的切换
        browser.sleep(1000);
        return this.root
            .$('.config-data-values div[class*="isActive"]')
            .$('.config-data-section__content')
            .getText()
            .then(text => {
                return text
                    .trim()
                    .replace('*\n', '')
                    .replace('\n*', '');
            });
    }
}
