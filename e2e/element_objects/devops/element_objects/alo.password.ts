import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder, promise } from 'protractor';

export class AloPassword extends AlaudaElementBase {
    private _root: ElementFinder;
    private _eye_cssSelector: string;

    constructor(
        root: ElementFinder,
        eye_cssSelector = 'aui-icon svg[class*="basic-eye_slash_s"]'
    ) {
        super();
        this._root = root;
        this._eye_cssSelector = eye_cssSelector;
    }

    get root() {
        this.waitElementPresent(this._root);
        return this._root;
    }

    getText(): promise.Promise<string> {
        const eyeButton = this.root.$(this._eye_cssSelector);

        return eyeButton.isPresent().then(isPresent => {
            if (isPresent) {
                eyeButton.click();
            }
            return this.root
                .$$('span span')
                .first()
                .getText();
        });
    }
}
