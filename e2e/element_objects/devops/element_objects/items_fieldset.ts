/**
 * aui-input-group 控件，左边一个input,右边一个单位
 * Created by liuwei on 2019/7/18.
 */
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { AlaudaInputbox } from '@e2e/element_objects/alauda.inputbox';
import { AlaudaElementBase } from '@e2e/element_objects/element.base';
import { ElementFinder } from 'protractor';

export class ItemFieldset extends AlaudaElementBase {
    private _root: ElementFinder;

    constructor(root: ElementFinder) {
        super();
        this._root = root;
    }

    get root() {
        this.waitElementPresent(this._root);
        return this._root;
    }

    get itemFieldset(): ElementFinder {
        return this._root;
    }

    get buttonAdd() {
        return new AlaudaButton(
            this._root.$('aui-icon[icon="basic:add_circle"]')
        );
    }

    add(testData) {
        this.root
            .$$('tr[rowtemplateindex="0"]')
            .count()
            .then(count => {
                const length = testData.length - count;
                for (let i = 0; i < length; i++) {
                    this.buttonAdd.button.isEnabled().then(isEnabled => {
                        if (isEnabled) {
                            this.buttonAdd.click();
                        }
                    });
                }
            });

        for (let i = 0; i < testData.length; i++) {
            const keyIndex = i + i;
            const valueIndex = i + i + 1;
            new AlaudaInputbox(
                this.root
                    .$$('aui-form-item')
                    .get(keyIndex)
                    .$('input')
            ).input(testData[i]['键']);
            new AlaudaInputbox(
                this.root
                    .$$('aui-form-item')
                    .get(valueIndex)
                    .$('textarea')
            ).input(testData[i]['值']);
        }
    }
}
