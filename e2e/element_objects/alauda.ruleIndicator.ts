/**
 * alo-array-form-table 下拉框列表控件
 * 负载均衡 添加规则指标 部分，用这个控件
 * Created by shijingnan on 2019/7/11.
 */
import { $, ElementFinder, browser } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiSelect } from './alauda.auiSelect';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';

export class ArrayRuleIndicatorForm extends AlaudaElementBase {
    private _ArrayRuleIndicatorForm: ElementFinder;
    private _iconAdd_Selector: string;
    private _iconRemove_Selector: string;

    /**
     * 构造函数
     * @param arrayRuleIndicatorForm ElementFinder 类型，规则指标页面元素
     */
    constructor(
        arrayRuleIndicatorForm: ElementFinder,
        iconAdd_Selector: string = '.add-rule-indicator  aui-icon',
        iconRemove_Selector: string = '.rule-part-form__body>.remove-indicator  aui-icon'
    ) {
        super();
        this._ArrayRuleIndicatorForm = arrayRuleIndicatorForm;
        this._iconAdd_Selector = iconAdd_Selector;
        this._iconRemove_Selector = iconRemove_Selector;
    }

    /**
     * 获得规则指标 控件
     */
    get ArrayRuleIndicatorForm(): ElementFinder {
        this.waitElementPresent(this._ArrayRuleIndicatorForm);
        return this._ArrayRuleIndicatorForm;
    }
    /**
     * 添加规则指标按钮
     */
    get iconAdd() {
        return new AuiIcon(
            this.ArrayRuleIndicatorForm.$(this._iconAdd_Selector)
        );
    }
    /**
     * 删除规则指标按钮
     */
    get iconRemove() {
        return new AuiIcon(
            this.ArrayRuleIndicatorForm.$(this._iconRemove_Selector)
        );
    }

    /**
   * 填写表格
   * @param parameters
   * @example 规则: [
                ['域名', ['a', 'b']],
                ['URL', [['RegEx', '^/a'], ['StartsWith', '/b']]],
                ['IP', [['Range', '10.10.0.1', '10.10.0.5'], ['Equal', '11.11.1.1']]],
                ['Header', 'header', [['Equal', 'header'], ['Range', 1, 3]]],
                ['Cookie', 'cookie', [['Equal', 'cookie']]],
                ['URL Param', 'param', [['Equal', 'param'], ['Range', 'a', 'z']]]
              ]
   */
    fill(parameters: Array<Array<any>>, start = 0) {
        this.ArrayRuleIndicatorForm.$$(this._iconRemove_Selector).each(elem => {
            const iconRemove = new AuiIcon(elem);
            iconRemove.click();
        });
        for (let i = start; i < parameters.length; i++) {
            this.iconAdd.click();
        }
        this.ArrayRuleIndicatorForm.$$(
            'rc-load-balancer-rule-part-form>form'
        ).each((elem, index) => {
            const ruleheader = elem.$('.rule-form-header');
            const rulevalue = elem.$(
                'rc-load-balancer-rule-values-form .rule-indicator'
            );
            const aui_select = ruleheader.$('aui-select');
            const type = parameters[index][0];
            this._fillForm(aui_select, type);
            let values;
            let keys;
            switch (type) {
                case '域名':
                case 'URL':
                case 'IP':
                    values = parameters[index][1];
                    for (let i = 0; i < values.length; i++) {
                        rulevalue.$('button').click();
                    }
                    break;
                case 'Header':
                case 'Cookie':
                case 'URL Param':
                    keys = parameters[index][1];
                    values = parameters[index][2];
                    for (let i = 0; i < values.length; i++) {
                        rulevalue.$('button').click();
                    }
                    break;
            }
            if (type === '域名') {
                rulevalue
                    .$$('input[formcontrolname="value"]')
                    .each((elm, index1) => {
                        this._fillForm(elm, values[index1]);
                    });
            }
            if (type === 'URL') {
                rulevalue.$$('li[ng-reflect-name]').each((elm, index1) => {
                    const condition = elm.$('aui-select');
                    this._fillForm(condition, values[index1][0]);
                    const value = elm.$('input[formcontrolname="value"]');
                    this._fillForm(value, values[index1][1]);
                });
            }
            if (type === 'IP') {
                rulevalue.$$('li[ng-reflect-name]').each((elm, index1) => {
                    const condition = elm.$('aui-select');
                    this._fillForm(condition, values[index1][0]);
                    if (values[index1][0] === 'Range') {
                        const startValue = elm.$(
                            'input[formcontrolname="startValue"]'
                        );
                        this._fillForm(startValue, values[index1][1]);
                        const end = elm.$('input[formcontrolname="endValue"]');
                        this._fillForm(end, values[index1][2]);
                    } else {
                        const value = elm.$('input[formcontrolname="value"]');
                        this._fillForm(value, values[index1][1]);
                    }
                });
            }
            if (
                type === 'Header' ||
                type === 'Cookie' ||
                type === 'URL Param'
            ) {
                const key = elem.$('input[formcontrolname="key"]');
                this._fillForm(key, keys);
                rulevalue.$$('li[ng-reflect-name]').each((elm, index1) => {
                    const condition = elm.$('aui-select');
                    this._fillForm(condition, values[index1][0]);
                    if (values[index1][0] === 'Range') {
                        const startValue = elm.$(
                            'input[formcontrolname="startValue"]'
                        );
                        this._fillForm(startValue, values[index1][1]);
                        const end = elm.$('input[formcontrolname="endValue"]');
                        this._fillForm(end, values[index1][2]);
                    } else {
                        const value = elm.$('input[formcontrolname="value"]');
                        this._fillForm(value, values[index1][1]);
                    }
                });
            }
        });
    }

    private _fillForm(elem: ElementFinder, parameters) {
        elem.getTagName().then(tagName => {
            this.waitElementPresent(elem);
            switch (tagName) {
                case 'input':
                    elem.getAttribute('readonly').then(readonly => {
                        if (!readonly) {
                            elem.click();
                            browser.sleep(100);
                            const inputbox = new AlaudaInputbox(elem);
                            elem.isEnabled().then(isEnabled => {
                                if (isEnabled) {
                                    inputbox.input(parameters);
                                }
                            });
                        }
                    });
                    break;
                case 'aui-select':
                    const auiSelect = new AuiSelect(
                        elem,
                        $('.cdk-overlay-pane aui-tooltip')
                    );

                    if (Array.isArray(parameters)) {
                        auiSelect.select(parameters[0], parameters[1]);
                    } else {
                        auiSelect.select(parameters);
                    }
                    break;
                default:
                    throw new Error('没有找到任何控件');
            }
        });
    }
}
