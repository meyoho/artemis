/**
 * 面包屑
 * Created by liuwei on 2018/3/15.
 */

import { $, ElementFinder, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './element.base';

export class AlaudaAuiBreadCrumb extends AlaudaElementBase {
    private _breadcrumb: ElementFinder;
    private _itemSelector: string;

    /**
     * 构造函数
     * @param breadcrumb 获得每个面包屑item的selector
     */
    constructor(
        breadcrumb: ElementFinder = $('div alo-breadcrumb'),
        itemCssSelector: string = 'ul li'
    ) {
        super();
        this._breadcrumb = breadcrumb;
        this._itemSelector = itemCssSelector;
    }

    /**
     * 页面的面包屑
     */
    get breadcrumb(): ElementFinder {
        this.waitElementPresent(this._breadcrumb, 5000).then(isPresent => {
            if (!isPresent) {
                throw new Error(`面包屑控件(${this._itemSelector})没有出现`);
            }
        });
        return this._breadcrumb;
    }

    /**
     * 获得面包屑的文本
     */
    getText(): promise.Promise<string> {
        CommonPage.waitElementPresent(this._breadcrumb);
        return this._breadcrumb.getText().then(res => {
            return res.replace(/\s/g, '');
        });
    }

    /**
     * 单击面包屑
     * @param name 面包屑上可单击的链接
     * @example click('应用')
     */
    click(name): promise.Promise<void> {
        return (
            this.breadcrumb
                //.$$('.aui-breadcrumb__item span:nth-child(1)')
                .$$(this._itemSelector)
                .filter(elem => {
                    return elem.getText().then(text => {
                        return text === name;
                    });
                })
                .first()
                .click()
        );
    }
}
