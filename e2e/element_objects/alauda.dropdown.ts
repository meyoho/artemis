/**
 * 下拉框列表控件
 * Created by liuwei on 2018/3/14.
 */
import {
  ElementArrayFinder,
  ElementFinder,
  browser,
  by,
  promise,
  protractor,
  $$,
} from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaText } from './alauda.text';
import { AlaudaElementBase } from './element.base';

export class AlaudaDropdown extends AlaudaElementBase {
  private _iconButton: ElementFinder;
  private _itemlist: ElementArrayFinder;
  private _inputTimeout: number;

  constructor(iconButton: ElementFinder, itemlist: ElementArrayFinder) {
    super();
    this._iconButton = iconButton;
    this._itemlist = itemlist;
    this._inputTimeout = 0;
  }

  get root(): ElementFinder {
    this.waitElementPresent(this._iconButton).then(isPresent => {
      if (!isPresent) {
        console.log(`下拉框${this._iconButton.locator()}没出现`);
      }
    });
    return this._iconButton;
  }

  /**
   * Aui-select 控件的文本框
   */
  get inputBox(): AlaudaInputbox {
    return new AlaudaInputbox(this.root.$('input'));
  }

  /**
   * 获取下拉框中文本框的值
   * @param valueCssSelector 文本框的值 的选择器
   */
  getValue(valueCssSelector = '.aui-select__label') {
    return this.waitElementPresent(this.root.$(valueCssSelector)).then(
      isPresent => {
        if (isPresent) {
          return this.root.$(valueCssSelector).getText();
        }
      },
    );
  }

  /**
   * dropdown 是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this._iconButton.isPresent();
  }

  /**
   * 单击下拉框， 选择值
   * @param selectvalue 从下拉框中要选择的值
   * @param childValue 鼠标悬浮 selectvalue 上之后要点击的元素的值
   */
  select(selectvalue, childValue: string = null): promise.Promise<void> {
    this.waitElementPresent(this._iconButton);
    CommonPage.waitElementClickable(this._iconButton);
    this._iconButton.click();
    browser.sleep(100);
    this.waitElementPresent(this._itemlist.first(), 3000).then(isPresent => {
      if (!isPresent) {
        this._iconButton.click();
        browser.sleep(100);
        const tooltip = new AlaudaText(
          by.css('aui-tooltip .aui-tooltip--bottom'),
        );
        this.waitElementPresent(tooltip.root, 1000).then(isPresent1 => {
          if (!isPresent1) {
            this._iconButton.click();
          }
        });
      }
    });

    const item = this._itemlist
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === selectvalue;
        });
      })
      .first();

    // 找到要选择的元素, 单击
    this.waitElementPresent(item, 10000).then(isPresent => {
      if (!isPresent) {
        throw new Error(`要选择的元素【${selectvalue}】在下拉框中没有找到`);
      }
    });
    if (childValue != null) {
      item.getText().then(text => {
        console.log(text);
      });

      browser
        .actions()
        .mouseMove(item)
        .mouseMove(
          $$('aui-tooltip>.aui-sub-dropdown aui-menu-item>button')
            .filter(citem => {
              return citem.getText().then(citem_text => {
                return citem_text.trim() === childValue;
              });
            })
            .first(),
        )
        .click()
        .perform();
    } else {
      this.waitElementClickable(item);
      item.click();
    }
    this._itemlist.count().then(count => {
      if (count > 0) {
        // 如果下拉框任然显示，单击icon按钮隐藏
        this._iconButton.click();
      }
    });
    return browser.sleep(1);
  }
  // 判断值不在下拉框中
  checkDataIsNotInDropdown(checkValue) {
    CommonPage.waitElementPresent(this._iconButton);
    const iconButton = this._iconButton;
    this._iconButton.click();
    browser.sleep(100);
    const itemlist = this._itemlist;
    const options = itemlist.filter(elem => {
      return elem.getText().then(text => {
        return text.trim() === checkValue;
      });
    });
    options.count().then(count => {
      if (count === 0) {
        CommonPage.waitElementNotPresent(options.first());
        return options.first().isPresent();
      } else {
        throw new Error(`在下拉框中不应改有这个值${checkValue}】`);
      }
    });
    iconButton.click();
    return browser.sleep(100);
  }
  /**
   * 判断值是否下拉框中
   * @param checkValue 值
   */
  checkDataExistInDropdown(checkValue): promise.Promise<boolean> {
    CommonPage.waitElementPresent(this._iconButton);
    this._iconButton.click();
    browser.sleep(100);
    return this._itemlist.getText().then(text => {
      this._iconButton.click();
      return text.includes(checkValue);
    });
  }
  /**
   * 获取下拉框的属性 用于判断下拉按钮是否可以点击
   * @param checkvalue 值
   * @param attr 属性
   */
  getDataAttr(checkvalue, attr = 'disabled') {
    this.waitElementPresent(this._iconButton);
    this._iconButton.click();
    this.waitElementPresent(this._itemlist.first(), 2000).then(isPresent => {
      if (!isPresent) {
        this._iconButton.click();
      }
    });
    browser.sleep(100);
    const item = this._itemlist
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === checkvalue;
        });
      })
      .first();

    // 找到要选择的元素, 单击
    this.waitElementPresent(item, 30000).then(isPresent => {
      if (!isPresent) {
        throw new Error(`要选择的元素【${checkvalue}】在下拉框中没有找到`);
      }
    });
    return item.getAttribute(attr);
  }
  private _wait(
    valueData: string,
    timeout = 10000,
    valueCssSelector = '.aui-select__label',
  ) {
    return browser.driver
      .wait(() => {
        return this.getValue(valueCssSelector).then(inputValue => {
          this._input(valueData);
          return inputValue === valueData;
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }

  /**
   * 递归: 文本框中输入值
   * @param inputValue 输入的值
   */
  input(inputValue, valueCssSelector = '.aui-select__label') {
    this.getValue(valueCssSelector).then(text => {
      console.log('输入前下拉框控件inputbox 的值: ' + text);
    });
    this._input(inputValue);
    this._wait(inputValue, 3000, valueCssSelector);

    this.getValue(valueCssSelector).then(text => {
      if (text === 'undefined' || !text || !/[^\s]/.test(text)) {
        console.log(`重试输入${text} ${this._inputTimeout++} 次`);
        if (this._inputTimeout > 10) {
          throw new Error(
            `在下拉框${this._iconButton.locator()} 中输入 ${inputValue} 超时了`,
          );
        }
        this.input(inputValue);
      } else {
        console.log('输入后下拉框控件inputbox 的值: ' + text);
      }
    });

    this._itemlist
      .first()
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          this.inputBox.inputbox.click();
        }
      });

    return browser.sleep(2);
  }

  private _input(inputValue: string) {
    this.inputBox.isReadonly().then(isReadonly => {
      if (!isReadonly) {
        this.inputBox.input(inputValue);
        browser.sleep(20);
        this.inputBox.inputbox.sendKeys(protractor.Key.ENTER);
        browser.sleep(20);
      } else {
        // console.log(
        //     '下拉框inputbox 是只读: ' + isReadonly + ' 单击下拉框'
        // );
        this.root.click();
      }
    });
  }
}
