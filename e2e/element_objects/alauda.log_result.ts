/**
 * Created by liuwei on 2019/8/15.
 */

import { ElementArrayFinder, ElementFinder, browser } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaLogResult extends AlaudaElementBase {
  private _root: ElementFinder;
  private _logcssSelector: string;

  constructor(logResult: ElementFinder, logcssSelector = '.alu-log-item') {
    super();
    this._root = logResult;
    this._logcssSelector = logcssSelector;
  }

  get root(): ElementFinder {
    this.waitElementPresent(this._root);
    return this._root;
  }

  /**
   * 获得所有logDashboard 里面所有 logs
   * @example
   * this.logs.getText().then(text => {
   *     console.log('日志面板下日志是:\n' + text);
   * })
   */
  get logs(): ElementArrayFinder {
    const loglist = this.root.$$(this._logcssSelector);
    this.waitElementPresent(loglist.first(), 30000).then(isPresent => {
      if (!isPresent) {
        loglist.getText().then(text => {
          console.log(text);
        });
        throw new Error('日志面板里面没有log');
      }
    });
    return loglist;
  }

  /**
   *日志面板是否包含日志
   * @param loglist 期望包含的日志 ['hehe.txt', 'server: envoy']
   * @example
   * const expectData = ['hehe.txt', 'server: envoy']
   * contain(expectData)
   *
   */
  async contain(loglist) {
    let isContain = true;
    await loglist.forEach(log => {
      try {
        this.waitElementPresent(this.logs.last());
        this.logs.getText().then(text => {
          isContain = isContain && String(text).includes(log);
        });
      } catch (ex) {
        browser.sleep(3000);
        this.waitElementPresent(this.logs.last());
        this.logs.getText().then(text => {
          isContain = isContain && String(text).includes(log);
        });
      }
    });
    return isContain;
  }
}
