/**
 * 文本框控件
 * Created by liuwei on 2018/8/13.
 */

import { ElementFinder, browser, promise, Key } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaInputbox extends AlaudaElementBase {
  private _inputBox: ElementFinder;

  constructor(inputBox: ElementFinder) {
    super();
    this._inputBox = inputBox;
  }

  /**
   * 获得文本框
   */
  get inputbox(): ElementFinder {
    this.waitElementPresent(this._inputBox).then(isPresent => {
      if (!isPresent) {
        throw new Error(`inputbox ${this._inputBox.locator()} 没出现`);
      }
    });
    return this._inputBox;
  }

  /**
   * 判断Inputbox是否显示
   */
  isPresent(): promise.Promise<boolean> {
    return this.inputbox.isPresent();
  }

  /**
   * 文本框是否是只读的
   */
  isReadonly() {
    return this.inputbox.getAttribute('readonly').then(readonly => {
      if (readonly !== null) {
        return readonly;
      } else {
        return false;
      }
    });
  }

  /**
   * 在文本框中输入一个值
   * @param inputValue 要输入的值
   */
  input(inputValue: string): promise.Promise<void> {
    return browser.sleep(1).then(() => {
      this.scrollToView(this.inputbox).then(() => {
        this.isReadonly().then(isReadonly => {
          if (!isReadonly) {
            if (String(inputValue).length > 0) {
              this.inputbox.clear();
              this.inputbox.sendKeys(inputValue);
            } else {
              this.clear();
            }
          }
        });
      });
    });
  }
  clear() {
    return browser.sleep(1).then(() => {
      this.scrollToView(this.inputbox).then(() => {
        this.isReadonly().then(isReadonly => {
          if (!isReadonly) {
            this.value.then(v => {
              const keys = [];
              for (let i = 0; i < v.length; i += 1) {
                keys[i] = Key.BACK_SPACE;
              }
              this.inputbox.sendKeys(...keys);
            });
          }
        });
      });
    });
  }
  /**
   * 获取文本框的值
   */
  getText(): promise.Promise<string> {
    return this.inputbox.getText();
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }
}
