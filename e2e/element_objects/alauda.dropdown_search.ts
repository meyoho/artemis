/**
 * 下拉框列表控件,带搜索框的，如项目下拉列表
 */
import {
    ElementArrayFinder,
    ElementFinder,
    browser,
    promise
} from 'protractor';

import { CommonPage } from '../utility/common.page';

import { AuiSearch } from './alauda.auiSearch';
import { AlaudaElementBase } from './element.base';

export class AlaudaDropdownWithSearch extends AlaudaElementBase {
    private _iconButton: ElementFinder;
    private _itemlist: ElementArrayFinder;
    private _searchBox: ElementFinder;

    constructor(
        iconButton: ElementFinder,
        itemlist: ElementArrayFinder,
        searchBox: ElementFinder
    ) {
        super();
        this._iconButton = iconButton;
        this._itemlist = itemlist;
        this._searchBox = searchBox;
    }

    /**
     * dropdown 是否存在
     */
    isPresent(): promise.Promise<boolean> {
        return this._iconButton.isPresent();
    }

    /**
     * 单击下拉框， 选择值
     * @param selectvalue 从下拉框中要选择的值
     */
    select(selectvalue): promise.Promise<void> {
        this.waitElementPresent(this._iconButton);
        this._iconButton.click();
        this.waitElementPresent(this._itemlist.first(), 2000).then(
            isPresent => {
                if (!isPresent) {
                    this._iconButton.click();
                }
            }
        );
        browser.sleep(100);

        const searchBox = new AuiSearch(this._searchBox);
        searchBox.onlySearch(selectvalue);
        browser.sleep(100);

        const item = this._itemlist
            .filter(elem => {
                return elem.getText().then(text => {
                    return text.trim() === selectvalue;
                });
            })
            .first();

        // 找到要选择的元素, 单击
        this.waitElementPresent(item, 30000).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `要选择的元素【${selectvalue}】在下拉框中没有找到`
                );
            }
        });
        item.click();

        this._itemlist.count().then(count => {
            if (count > 0) {
                // 如果下拉框任然显示，单击icon按钮隐藏
                this._iconButton.click();
            }
        });
        return browser.sleep(100);
    }
    // 判断值不在下拉框中
    checkDataIsNotInDropdown(checkValue) {
        CommonPage.waitElementPresent(this._iconButton);
        const iconButton = this._iconButton;
        this._iconButton.click();
        browser.sleep(100);
        const itemlist = this._itemlist;
        const options = itemlist.filter(elem => {
            return elem.getText().then(text => {
                return text.trim() === checkValue;
            });
        });
        options.count().then(count => {
            if (count === 0) {
                CommonPage.waitElementNotPresent(options.first());
                return options.first().isPresent();
            } else {
                throw new Error(`在下拉框中不应改有这个值${checkValue}】`);
            }
        });
        iconButton.click();
        return browser.sleep(100);
    }
    /**
     * 判断值是否下拉框中
     * @param checkValue 值
     */
    checkDataExistInDropdown(checkValue): promise.Promise<boolean> {
        CommonPage.waitElementPresent(this._iconButton);
        this._iconButton.click();
        browser.sleep(100);
        return this._itemlist.getText().then(text => {
            this._iconButton.click();
            return text.includes(checkValue);
        });
    }
    /**
     * 获取下拉框的属性 用于判断下拉按钮是否可以点击
     * @param checkvalue 值
     * @param attr 属性
     */
    getDataAttr(checkvalue, attr = 'ng-reflect-disabled') {
        this.waitElementPresent(this._iconButton);
        this._iconButton.click();
        this.waitElementPresent(this._itemlist.first(), 2000).then(
            isPresent => {
                if (!isPresent) {
                    this._iconButton.click();
                }
            }
        );
        browser.sleep(100);
        const item = this._itemlist
            .filter(elem => {
                return elem.getText().then(text => {
                    return text.trim() === checkvalue;
                });
            })
            .first();

        // 找到要选择的元素, 单击
        this.waitElementPresent(item, 30000).then(isPresent => {
            if (!isPresent) {
                throw new Error(
                    `要选择的元素【${checkvalue}】在下拉框中没有找到`
                );
            }
        });
        return item.getAttribute(attr);
    }
}
