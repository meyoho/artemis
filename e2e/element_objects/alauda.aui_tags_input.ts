/**
 * 文本框控件
 * Created by liuwei on 2018/4/9.
 */

import { ElementFinder, browser, promise, protractor } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaAuiTagsInput extends AlaudaElementBase {
  private _tagsInput: ElementFinder;

  constructor(tagsInput: ElementFinder) {
    super();
    this._tagsInput = tagsInput;
  }

  /**
   * 获得文本框
   */
  get inputbox(): ElementFinder {
    this.waitElementPresent(this._tagsInput);
    return this._tagsInput;
  }

  /**
   * 判断Inputbox是否显示
   */
  isPresent(): promise.Promise<boolean> {
    return this.inputbox.isPresent();
  }

  /**
   * 在文本框中输入一个值
   * @param inputValue 要输入的值
   */
  input(inputValue: string): promise.Promise<void> {
    this.inputbox.click();
    this.inputbox.$('input').sendKeys(inputValue);
    this.inputbox.$('input').sendKeys(protractor.Key.ENTER);
    return browser.sleep(100);
  }

  clear(): promise.Promise<void> {
    this.inputbox.click();
    browser.sleep(100);
    this.inputbox.clear();
    return browser.sleep(100);
  }
  clearTags(): promise.Promise<void> {
    this.inputbox.$$('aui-tag').each(elem => {
      elem.$('aui-icon').click();
    });
    return browser.sleep(1);
  }
  delete(): promise.Promise<void> {
    this.inputbox.click();
    browser.sleep(100);
    this.inputbox.$('input').sendKeys(protractor.Key.BACK_SPACE);
    return browser.sleep(100);
  }

  /**
   * 获取文本框的值
   */
  getText(): promise.Promise<string> {
    return this.inputbox.getText();
  }

  /**
   * 获取文本框的值
   */
  get value(): promise.Promise<string> {
    return this.inputbox.getAttribute('value');
  }
}
