/**
 * alaudaSwitch 列表控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AuiToolTip extends AlaudaElementBase {
  private _auiTooltip: ElementFinder;
  constructor(auiToolTip: ElementFinder) {
    super();
    this._auiTooltip = auiToolTip;
  }

  /**
   * 获得开关控件
   */
  get auiToolTip(): ElementFinder {
    this.waitElementPresent(this._auiTooltip);
    return this._auiTooltip;
  }

  isPresent(): promise.Promise<boolean> {
    return this._auiTooltip.$$('aui-option>div').isPresent();
  }

  get tagName(): promise.Promise<string> {
    // 找到要操做的item
    const item = this.auiToolTip.$(
      '.aui-option-container__content>*:first-child',
    );
    return this.waitElementPresent(item, 2000).then(isPresent => {
      if (isPresent) {
        return item.getTagName();
      }
      return null;
    });
  }
  get optionElements() {
    return this.auiToolTip.$$('.aui-option-container__content>*');
  }
  selectAui_menu_item(value: string): promise.Promise<void> {
    // 找到要操做的item
    const item = this.auiToolTip
      .$$('aui-menu-item button')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === value;
        });
      })
      .first();
    this.waitElementPresent(item);
    item.click();
    return browser.sleep(100);
  }

  /**
   * 获取可选择的下拉框的个数
   */
  aui_option_count(): promise.Promise<number> {
    // 找到要操做的item
    return this.auiToolTip.$$('aui-option div:not([hidden])').count();
  }

  /**
   * 打开开关
   */
  selectAui_option(value: string): promise.Promise<void> {
    // 找到要操做的item
    const item = this.auiToolTip
      .$$('aui-option .aui-option')
      .filter(elem => {
        return elem.getText().then(text => {
          // console.log('KKKKKKK1:' + text + '=======');
          // console.log('KKKKKKK2:' + value + '=======');
          return text.trim() === value;
        });
      })
      .first();
    this.waitElementPresent(item).then(isPresent => {
      if (!isPresent) {
        throw new Error(`在下拉框中没有找到元素【${value}】`);
      }
    });
    this.scrollToView(item).then(() => {
      item.click();
    });
    return browser.sleep(100);
  }

  selectAui_option_group(key: string, value: string): promise.Promise<boolean> {
    // 找到要操做的item
    const optionGroup = this.auiToolTip
      .$$('.aui-option-container__content aui-option-group')
      .filter(elem => {
        return elem
          .$('.aui-option-group__title')
          .getText()
          .then(text => {
            return text.trim() === key;
          });
      })
      .first();
    this.waitElementPresent(optionGroup);
    // 找到选择项，单击
    const item = optionGroup
      .$$('aui-option>div')
      .filter(elem => {
        return elem.getText().then(text => {
          return text.trim() === value;
        });
      })
      .first();
    item.click();
    browser.sleep(100);
    return item.isPresent();
  }

  get hasOption_group_Title(): promise.Promise<boolean> {
    // 找到要操做的item
    const groupTitle = this.auiToolTip
      .$('.aui-option-container__content aui-option-group')
      .$('.aui-option-group__title');
    return groupTitle.isPresent();
  }
  has_option_group_title(option_ele) {
    return option_ele.$('.aui-option-group__title').isPresent();
  }
  selectOption(key, value) {
    const option = this.optionElements
      .filter(ele => {
        return ele.getTagName().then(tagName => {
          switch (tagName) {
            case 'aui-option-group':
              return ele
                .$('.aui-option-group__title')
                .isPresent()
                .then(isPresent => {
                  if (isPresent) {
                    return ele
                      .$('.aui-option-group__title')
                      .getText()
                      .then(text => {
                        if (text.trim() === key) {
                          return ele
                            .$$('aui-option')
                            .filter(opt_ele => {
                              return opt_ele.getText().then(opt_text => {
                                return opt_text.trim() === value;
                              });
                            })
                            .count()
                            .then(ct => {
                              return ct > 0;
                            });
                        } else {
                          return false;
                        }
                      });
                  } else {
                    return ele
                      .$$('aui-option')
                      .filter(opt_ele => {
                        return opt_ele.getText().then(opt_text => {
                          return opt_text.trim() === value;
                        });
                      })
                      .count()
                      .then(ct => {
                        return ct > 0;
                      });
                  }
                });
            case 'aui-option':
              return ele.getText().then(text => {
                return text.trim() === key;
              });
            default:
              return false;
          }
        });
      })
      .first();
    return option.isPresent().then(ispresent => {
      if (ispresent) {
        option.getTagName().then(tagName => {
          switch (tagName) {
            case 'aui-option-group':
              this.has_option_group_title(option).then(isPresent => {
                if (isPresent) {
                  this.selectAui_option_group(key, value);
                } else {
                  this.selectAui_option(key);
                }
              });
              break;
            case 'aui-option':
              this.selectAui_option(key);
              break;
          }
        });
      }
    });
  }
}
