/**
 * 输入框两侧带加减号的输入框
 * Created by 李俊磊 on 2018/9/13.
 */
import { ElementFinder } from 'protractor';

import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';
export class AlaudaAuiNumberInput extends AlaudaElementBase {
  private _auiNumberInput: ElementFinder;
  private _minusSelector: string;
  private _plusSignSelector: string;
  constructor(
    auiNumberInput: ElementFinder,
    minusSelector = 'aui-icon[icon="minus"]',
    plusSignSelector = 'aui-icon[icon="plus"]',
  ) {
    super();
    this._auiNumberInput = auiNumberInput;
    this._minusSelector = minusSelector;
    this._plusSignSelector = plusSignSelector;
  }
  get root(): ElementFinder {
    this.waitElementPresent(this._auiNumberInput).then(IsPresent => {
      if (!IsPresent) {
        throw new Error(`AuiNumberInput元素未找到[${this._auiNumberInput}]`);
      }
    });
    return this._auiNumberInput;
  }
  get currentValue() {
    this.waitElementPresent(this.root.$('input')).then(IsPresent => {
      if (!IsPresent) {
        throw new Error(`Input元素未找到[${this.root.$('input')}]`);
      }
    });
    return this.root.$('input').getAttribute('value');
  }
  get plusButton() {
    return new AlaudaButton(this.root.$(this._plusSignSelector));
  }
  get minusButton() {
    return new AlaudaButton(this.root.$(this._minusSelector));
  }
  /**
   *
   * @param input_number 要输入的值 数字类型
   */
  input(input_number: number) {
    this.currentValue.then(value => {
      const num: number = parseInt(value, 10);
      if (input_number > num) {
        for (let i = 0; i < input_number - num; i++) {
          this.plusButton.click();
        }
      } else if (input_number < num) {
        for (let i = 0; i < num - input_number; i++) {
          this.minusButton.click();
        }
      }
    });
  }
}
