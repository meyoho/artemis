/**
 * alo-tags-label 控件,
 * 应用下的Deployment 的详情页的标签，注解控件用使用
 * Created by liuwei on 2018/10/18.
 */

import { $$, ElementFinder, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaTagsLabel extends AlaudaElementBase {
    private _root: ElementFinder;
    private _toolTipTagSelector: string;
    private _tagsSelector: string;
    private _moreSelector: string;

    constructor(
        aloTagsLabel: ElementFinder,
        tagsSelector: string = 'aui-tag span',
        moreSelector = 'aui-tag[auitooltiptype="info"]',
        toolTipTagsSelector = '.tooltip-tag aui-tag span'
    ) {
        super();
        this._root = aloTagsLabel;
        this._tagsSelector = tagsSelector;
        this._toolTipTagSelector = toolTipTagsSelector;
        this._moreSelector = moreSelector;
    }

    get root() {
        this.waitElementPresent(this._root);
        return this._root;
    }

    /**
     * 判断 alo-tags-label 是否存在
     */
    isPresent(): promise.Promise<boolean> {
        return this._root.isPresent();
    }

    /**
     * 获取 alo-tags-label 控件的值
     * @example getText()
     */
    getText(): promise.Promise<string> {
        // [3个点按钮]
        const tabButton = this.root.$(this._moreSelector);
        return tabButton.isPresent().then(ispresent => {
            if (ispresent) {
                // 如果 [3个点按钮] 存在单击获得所有值
                tabButton.click();
                const firstItem = $$(this._toolTipTagSelector).first();
                this.waitElementPresent(firstItem);
                const text = $$(this._toolTipTagSelector).getText();
                this.root
                    .$$(this._tagsSelector)
                    .first()
                    .click();
                return text;
            } else {
                // 如果 [3个点按钮] 不存在获得所有值
                return this.root.$$(this._tagsSelector).getText();
            }
        });
    }
}
