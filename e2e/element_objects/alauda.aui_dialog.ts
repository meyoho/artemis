/**
 * aui-dailog 控件
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';
import { AlaudaInputbox } from './alauda.inputbox';

export class AuiDialog extends AlaudaElementBase {
  private _root: ElementFinder;
  private _content: any;
  private _confirmSelector;
  private _cancelSelector;
  private _titleSelector;
  constructor(
    auiDialog: ElementFinder,
    contentSelector = '.aui-dialog__content',
    confirmSelector = '.aui-dialog__footer button[aui-button="primary"]',
    cancelSelector = '.aui-dialog__footer button[aui-button=""]',
    titleSelector = 'aui-dialog-header .aui-dialog__header-title',
  ) {
    super();
    this._root = auiDialog;
    this._content = this.auiDialog.$(contentSelector);
    this._confirmSelector = confirmSelector;
    this._cancelSelector = cancelSelector;
    this._titleSelector = titleSelector;
  }

  /**
   * 获得dialog控件
   */
  get auiDialog(): ElementFinder {
    this.waitElementPresent(this._root);
    return this._root;
  }

  isPresent() {
    return this._root.isPresent();
  }

  /**
   * Dialog 控件的标题
   */
  get title(): ElementFinder {
    return this.auiDialog.$(this._titleSelector);
  }

  get aui_icon(): AuiIcon {
    return new AuiIcon(this.auiDialog.$('aui-dialog-header aui-icon'));
  }

  /**
   * Dialog 控件的内容
   */
  get content(): any {
    return this._content;
  }
  set content(value) {
    this._content = value;
  }

  /**
   * 确定按钮
   */
  get buttonConfirm(): AlaudaButton {
    return new AlaudaButton(this.auiDialog.$(this._confirmSelector));
  }

  /**
   * 取消按钮
   */
  get buttonCancel(): AlaudaButton {
    return new AlaudaButton(this.auiDialog.$(this._cancelSelector));
  }

  /**
   * 单击确定按钮
   */
  clickConfirm(): promise.Promise<void> {
    this.buttonConfirm.click();
    this.waitElementNotPresent(this._root, 5000);
    return browser.sleep(1);
  }

  /**
   * 单击取消按钮
   */
  clickCancel(): promise.Promise<void> {
    this.buttonCancel.click();
    return browser.sleep(1);
  }
  /**
   * 单击关闭按钮
   */
  clickClose(): promise.Promise<void> {
    this.aui_icon.click();
    return browser.sleep(1);
  }
  input(name: string) {
    const input_box = new AlaudaInputbox(this.content.$('input'));
    input_box.input(name);
  }
}
