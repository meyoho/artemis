/**
 * 控件的基类
 * Created by liuwei on 2018/8/13.
 */
import { ElementFinder, browser, promise, protractor } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaElementBase {
  /**
   * 等待页面元素出现
   * @param elem 页面元素
   * @param timeout 超时时间, 默认10秒
   * @example waitElementPresent($('button'))
   */
  waitElementPresent(
    elem: ElementFinder,
    timeout = 10000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementPresent(elem, timeout);
  }

  /**
   * 等待页面元素不出现
   * @param elem 页面元素
   * @param timeout 超时时间, 默认20秒
   * @example waitElementNotPresent($('button'))
   */
  waitElementNotPresent(
    elem: ElementFinder,
    timeout = 10000,
  ): promise.Promise<boolean> {
    return CommonPage.waitElementNotPresent(elem, timeout);
  }

  /**
   * 等待页面元素 的 Attribute 消失
   * @param elem 页面元素
   * @param artribute Attribute 名称
   * @param timeout  超时时间，, 默认10秒
   */
  waitElementNotHasArtribute(
    elem: ElementFinder,
    artribute = 'disabled',
    timeout = 10000,
  ): promise.Promise<boolean> {
    return browser.driver
      .wait(() => {
        return elem.getAttribute(artribute).then(disabled => {
          return disabled === null;
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(
            `wait elem has not Artribute ${artribute}  error: ${err}`,
          );
          return false;
        },
      );
  }
  /**
   * 等待页面元素 的 Attribute 出现
   * @param elem 页面元素
   * @param artribute Attribute 名称
   * @param timeout  超时时间，, 默认10秒
   */
  waitElementHasArtribute(
    elem: ElementFinder,
    artribute = 'class',
    artribute_value = 'isActive',
    timeout = 10000,
  ): promise.Promise<boolean> {
    return browser.driver
      .wait(() => {
        return elem.getAttribute(artribute).then(attr_value => {
          return attr_value.includes(artribute_value);
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(
            `wait elem has not Artribute ${artribute}  error: ${err}`,
          );
          return false;
        },
      );
  }
  /**
   * 等待元素能单击
   * @param elem 页面元素
   * @param timeout 等待多久超时
   */
  waitElementClickable(elem: ElementFinder, timeout = 10000) {
    this.waitElementPresent(elem);
    const EC = protractor.ExpectedConditions;
    return browser.driver.wait(EC.elementToBeClickable(elem), timeout).then(
      () => true,
      err => {
        elem.getTagName().then(tagName => {
          console.error(err);
          throw new Error(`元素${tagName}, 不可单击`);
        });
        return false;
      },
    );
  }

  /**
   * 滑动滚动条到页面底部
   */
  scrollToBoWindowBottom() {
    browser.executeScript('window.scrollTo(0,document.body.scrollHeight)');
  }
  /**
   * 滚动到页面底部
   */
  scrollToBottom() {
    browser.executeScript(`(function () { 
            let y = document.body.scrollTop; 
            let step = 100; 
            window.scroll(0, y); 
            function f() { 
                if (y < document.body.scrollHeight) { 
                    y += step; 
                    window.scroll(0, y); 
                    setTimeout(f, 50); 
                }
                else { 
                    window.scroll(0, y); 
                    document.title += "scroll-done"; 
                } 
            } 
            setTimeout(f, 1000); 
        })(); `);
  }

  /**
   * 滑动滚动条到页面底部
   */
  scrollToWindow(x: number, y: number) {
    browser.executeScript(`window.scrollTo(${x},${y})`);
  }
  /**
   * 滚动到元素可见位置
   * @param ele 页面元素 ElementFinder 类型
   */
  scrollToView(ele: ElementFinder) {
    browser.executeScript('arguments[0].scrollIntoViewIfNeeded();', ele);
    return browser.sleep(1);
  }
}
