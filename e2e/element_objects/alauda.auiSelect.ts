/**
 * aui-select 下拉框列表控件
 * Created by liuwei on 2018/8/13.
 */
import {
  $,
  ElementFinder,
  browser,
  promise,
  protractor,
  Key,
} from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiToolTip } from './alauda.auiToolTip';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';

export class AuiSelect extends AlaudaElementBase {
  private _auiSelect: ElementFinder;
  private _auiToolTip: ElementFinder;
  private _inputTimeout: number;

  /**
   * 构造函数
   * @param auiSelect ElementFinder 类型，auiSelect 页面元素
   * @param auiToolTip ElementFinder 类型, auiToolTip 页面元素
   */
  constructor(
    auiSelect: ElementFinder,
    auiToolTip: ElementFinder = $('.cdk-overlay-pane aui-tooltip'),
  ) {
    super();
    this._auiSelect = auiSelect;
    this._auiToolTip = auiToolTip;
    this._inputTimeout = 0;
  }

  get root() {
    this.waitElementPresent(this._auiSelect);
    return this._auiSelect;
  }

  /**
   * Aui-select 控件的文本框
   */
  get inputBox(): AlaudaInputbox {
    return new AlaudaInputbox(this._auiSelect.$('input'));
  }

  /**
   * 判断AuiSelect是否显示
   */
  isPresent(): promise.Promise<boolean> {
    return this._auiSelect.isPresent();
  }

  /**
   * Aui-select 控件的下拉框
   */
  get auiToolTip(): AuiToolTip {
    return new AuiToolTip(this._auiToolTip);
  }

  /**
   * Aui-select 控件的aui-icon 控件
   */
  get auiIcon(): AuiIcon {
    this.waitElementPresent(this._auiSelect);
    return new AuiIcon(this._auiSelect.$('aui-icon'));
  }

  /**
   * 获得文本框中的输入值
   */
  get value(): promise.Promise<string> {
    return this._auiSelect.$('.aui-select__label').getText();
  }

  isEnable(): promise.Promise<boolean> {
    const divElem: ElementFinder = this.root.$(
      '[auitooltipposition="bottom start"]',
    );

    return divElem.isPresent().then(isPresent => {
      if (isPresent) {
        return divElem.getAttribute('class').then(classValue => {
          return !classValue.includes('isDisabled');
        });
      } else {
        // 如果找不到 [auitooltipposition="bottom start"], 默认返回true
        return browser.sleep(1).then(() => {
          return true;
        });
        // return this._auiSelect
        //     .getAttribute('class')
        //     .then(classValue => {
        //         return !classValue.includes('isDisabled');
        //     });
      }
    });
  }

  /**
   * 单击 icon, 从下拉框中选择一个item
   * @param text 要选择的item
   * @param itemListSelector string， 所有下拉项的css 选择器
   */
  select(key: string, value = ''): promise.Promise<void> {
    return this.isEnable().then(isEnable => {
      if (isEnable) {
        this.auiIcon.click();
        this.auiToolTip.isPresent().then(isPresent => {
          if (!isPresent) {
            this.auiIcon.click();
          }
        });
        this.inputBox.inputbox.getAttribute('readonly').then(readonly => {
          if (!readonly) {
            if (value === '') {
              this.inputBox.input(key);
            } else {
              this.inputBox.input(value);
            }

            this.auiToolTip.aui_option_count().then(count => {
              if (count > 1) {
                // 文本框输入值后，下拉框的选项仍然大于2个， 从下拉框中选择一个
                return this._selectItem(key, value);
              } else {
                // 否则按回车键，隐藏下拉框
                this.inputBox.inputbox.sendKeys(Key.ENTER);
                browser.sleep(100);
              }
            });
          } else {
            // 文本是只读，从下拉框中选择
            return this._selectItem(key, value);
          }
        });
      }
    });
  }

  private _selectItem(key: string, value: string): void | PromiseLike<void> {
    return this.auiToolTip.selectOption(key, value);
  }
  /** 
  private _selectItem(key: string, value: string): void | PromiseLike<void> {
    return this.auiToolTip.tagName.then(tagName => {
      switch (tagName) {
        case 'aui-option-group':
          this.auiToolTip.hasOption_group_Title.then(isPresent => {
            if (isPresent) {
              this.auiToolTip.selectAui_option_group(key, value);
            } else {
              this.auiToolTip.selectAui_option(key);
            }
          });
          break;
        case 'aui-option':
          this.auiToolTip.selectAui_option(key);
          break;
      }
    });
  }
*/
  private _wait(
    valueData: string,
    timeout = 10000,
    valueCssSelector = '.aui-select__label',
  ) {
    return browser.driver
      .wait(() => {
        return this.getValue(valueCssSelector).then(inputValue => {
          this._input(valueData);
          return inputValue === valueData;
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error [' + err + ']');
          return false;
        },
      );
  }

  /**
   * 获取下拉框中文本框的值
   * @param valueCssSelector 文本框的值 的选择器
   */
  getValue(valueCssSelector = '.aui-select__label') {
    return this.waitElementPresent(this.root.$(valueCssSelector)).then(
      isPresent => {
        if (isPresent) {
          return this.root.$(valueCssSelector).getText();
        }
      },
    );
  }

  /**
   * 递归：文本框中输入值
   * @param inputValue 输入的值
   */
  input(inputValue, valueCssSelector = '.aui-select__label') {
    this.getValue(valueCssSelector).then(text => {
      console.log('输入前下拉框控件inputbox 的值: ' + text);
    });
    this._input(inputValue);
    this._wait(inputValue, 3000, valueCssSelector);

    this.getValue(valueCssSelector).then(text => {
      if (this._inputTimeout++ > 5) {
        throw new Error(`在 ${this._auiSelect.locator()} 输入值 ${text} 失败`);
      }
      if (text === 'undefined' || !text || !/[^\s]/.test(text)) {
        this.input(inputValue);
      } else {
        console.log('输入后下拉框控件inputbox 的值: ' + text);
      }
    });

    return browser.sleep(2);
  }

  private _input(inputValue: string) {
    this.inputBox.isReadonly().then(isReadonly => {
      if (!isReadonly) {
        this.inputBox.input(inputValue);
        browser.sleep(20);
        this.inputBox.inputbox.sendKeys(protractor.Key.ENTER);
        browser.sleep(20);
      } else {
        // console.log(
        //     '下拉框inputbox 是只读: ' + isReadonly + ' 单击下拉框'
        // );
        this.root.click();
      }
    });
  }
  input_select(input_value, select_value) {
    this.root.click();
    this.inputBox.input(input_value);
    browser.sleep(20);
    this.auiToolTip.selectAui_option(select_value);
  }
}
