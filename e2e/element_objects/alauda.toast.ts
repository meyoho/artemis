/**
 * toast 控件
 * 创建成功，的提示有使用这个控件
 * Created by liuwei on 2018/4/10.
 */

import { $, ElementFinder, promise } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaToast {
  private _toast_selector: ElementFinder;

  constructor(
    selector: ElementFinder = $('.aui-message .aui-message__content'),
  ) {
    this._toast_selector = selector;
  }

  get message(): ElementFinder {
    return this._toast_selector;
  }

  /**
   * 获取toast控件的message文本值, 并等待toast 控件消失
   */
  getMessage(timeout = 30000): promise.Promise<string> {
    CommonPage.waitElementPresent(this.message, timeout);
    return this.message.isPresent().then(isPresent => {
      if (isPresent) {
        const text = this.message.getText();
        CommonPage.waitElementNotPresent(this.message);
        return text;
      } else {
        console.log('没有找到toast 控件');
      }
    });
  }

  /**
   * 等待toast 消失
   */
  waitDisappear() {
    CommonPage.waitElementNotPresent(this.message);
  }
}
