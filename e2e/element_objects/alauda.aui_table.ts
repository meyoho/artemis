/**
 * Created by liuwei on 2018/8/13.
 */
import {
  // $,
  ElementArrayFinder,
  ElementFinder,
  browser,
  promise,
} from 'protractor';
import { isArray } from 'util';

import { CommonMethod } from '../utility/common.method';
import { CommonPage } from '../utility/common.page';

import { AlaudaElementBase } from './element.base';

interface HeaderCell {
  index: number;
  name: string;
}

export class AlaudaAuiTable extends AlaudaElementBase {
  private _auiTable: ElementFinder;
  private _headerCellSelector: string;
  private _cellSelector: string;
  private _rowSelector: string;

  constructor(
    auiTable: ElementFinder,
    headerCellSelector: string = 'aui-table-header-cell',
    bodyCellSelector: string = 'aui-table-cell',
    rowCellSelector: string = 'aui-table-row',
  ) {
    super();
    this._auiTable = auiTable;
    this._headerCellSelector = headerCellSelector;
    this._cellSelector = bodyCellSelector;
    this._rowSelector = rowCellSelector;
  }

  get auiTable() {
    this.waitElementPresent(this._auiTable);
    // this.waitElementNotPresent($('.aui-icon-spinner'), 10000);
    return this._auiTable;
  }

  /**
   * 根据列名获得表格共几列，{name}列在第几列
   *
   * @parameter {name} 列名， string 类型
   */
  getColumeIndexByColumeName(name) {
    const headerCell = this.auiTable
      .$$(this._headerCellSelector)
      .map((elem, index) => {
        return {
          index: index,
          name: elem.getText(),
        };
      });
    // 遍历表头，返回{name}列在第几列，共几列
    return headerCell.then(headerCellList => {
      let columeIndex = 0;
      headerCellList.forEach((item: HeaderCell) => {
        if (item.name === name) {
          columeIndex = item.index;
          return;
        }
      });
      return {
        columeCount: headerCellList.length,
        columeIndex: columeIndex,
      };
    });
  }

  /**
   * 根据列名获得表格共几列，{name}列在第几列
   *
   * @parameter {name} 列名， string 类型
   */
  _getColumeIndexByColumeName(name) {
    const headerCell = this.auiTable
      .$$(this._headerCellSelector)
      .map((elem, index) => {
        return {
          index: index,
          name: elem.getText(),
        };
      });
    // 遍历表头，返回{name}列在第几列，共几列
    return headerCell.then(headerCellList => {
      let [columeIndex] = [0];
      headerCellList.forEach((item: HeaderCell) => {
        if (item.name === name) {
          columeIndex = item.index;
          return;
        }
      });
      return {
        columeCount: headerCellList.length,
        columeIndex: columeIndex,
      };
    });
  }

  /**
   * 根据列名获得该列的所有值
   *
   * @parameter {name} 表格的列名， string 类型
   */
  getColumeTextByName(name): promise.Promise<string> {
    return this._getColumeIndexByColumeName(name).then(table => {
      const colume: ElementArrayFinder = this.auiTable
        .$$(this._cellSelector)
        .filter((elem, index) => {
          return elem.getText().then(() => {
            return index % table.columeCount === table.columeIndex;
          });
        });
      return colume.getText();
    });
  }

  /**
   * 获得表格的表头所有列名
   *
   */
  getHeaderText(): promise.Promise<string> {
    return this.auiTable.$$(this._headerCellSelector).getText();
  }

  /**
   * 根据列名获得表头的单元格元素
   *
   * @parameter {name} 表格头的单元格显示的内容文本， string 类型
   */
  private _getHeaderCellByName(name): ElementFinder {
    return this.auiTable
      .$$(this._headerCellSelector)
      .filter(elem => {
        return elem.getText().then(text => {
          return text === name;
        });
      })
      .first();
  }

  /**
   * 单击表头，等待表格数据加载完毕
   *
   * @parameter {name} 表格头的单元格显示的内容文本， string 类型
   */
  clickHeaderByName(name): promise.Promise<void> {
    this._getHeaderCellByName(name).click();
    return browser.sleep(100);
  }

  /**
   * 根据关键字获得一行
   * @param keys 数组列型，根据keys里面的元素可以唯一确定一行
   * @return {ElementFinder}
   * @example getRow([secret_1,namespace2241_1,'Secret']).element(by.css('.aui-table__cell button')).click()
   */
  getRow(keys): ElementFinder {
    if (!isArray(keys)) {
      throw new Error(`参数 keys(${keys}) 不是数组类型的`);
    }
    const rowlist = this.auiTable.$$(this._rowSelector);

    // 根据关键字找到唯一行
    const row = rowlist
      .filter(elem => {
        return elem.getText().then(text => {
          return keys.every((key: string) => text.includes(key));
        });
      })
      .first();
    this.waitElementPresent(row).then(isPresent => {
      if (!isPresent) {
        throw new Error(`在表格中，没有找到${keys}`);
      } else {
        console.log('get row success');
      }
    });
    return row;
  }

  /**
   * 根据关键字获得所有行
   * @param keys 数组列型，根据keys里面的元素可以唯一确定一行
   * @return {ElementArrayFinder}
   */
  getRows(keys): ElementArrayFinder {
    if (!isArray(keys)) {
      throw new Error(`参数 keys(${keys}) 不是数组类型的`);
    }
    const rowlist = this.auiTable.$$(this._rowSelector);

    // 根据关键字找到唯一行
    const row = rowlist.filter(elem => {
      return elem.getText().then(text => {
        return keys.every((key: string) => text.includes(key));
      });
    });
    this.waitElementPresent(row.first()).then(isPresent => {
      if (!isPresent) {
        throw new Error(`在表格中，没有找到${keys}`);
      } else {
        console.log('get row success');
      }
    });
    return row;
  }

  /**
   * 判断关键字是否在列表中
   * @param keys 数组列型，根据keys里面的元素可以唯一确定一行
   * @return promise.Promise<boolean>
   */
  hasRow(keys): promise.Promise<boolean> {
    if (!isArray(keys)) {
      throw new Error(`参数 keys(${keys}) 不是数组类型的`);
    }
    const rowlist = this.auiTable.$$(this._rowSelector);

    // 根据关键字找到唯一行
    const row = rowlist
      .filter(elem => {
        return elem.getText().then(text => {
          return keys.every((key: string) => text.includes(key));
        });
      })
      .first();
    return this.waitElementPresent(row).then(isPresent => {
      if (!isPresent) {
        console.log(`在表格中，没有找到${keys}`);
        return false;
      } else {
        console.log(`在表格中，找到${keys}`);
        return true;
      }
    });
  }

  /**
   * 获取第几行，行号从0开始
   * @param index 行号
   */
  getRowByIndex(index: number): ElementFinder {
    const rowlist = this.auiTable.$$(this._rowSelector);
    return rowlist.get(index);
  }

  getRowCell(row_index, cell_index): ElementFinder {
    return this.getRowByIndex(row_index)
      .$$(this._cellSelector)
      .get(cell_index);
  }

  /**
   * 根据列名和关键字获得一个单元格
   *
   * @parameter {name} 列名
   * @parameter {keys} 数组列型，根据keys里面的元素可以唯一确定一行
   *
   * @example
   *
   * getCell('名称', [namespace1, 'Namespace']).then(function (cell) {
      expect(cell.element(by.css('.aui-table__cell a')).getText()).toBe(namespace1);
    });

   * @return 返回 {keys} 找到的唯一行，所在{name} 列的单元格
   */
  getCell(name, keys): promise.Promise<ElementFinder> {
    const body_cell_selector = this._cellSelector;
    // 根据关键字找到唯一行
    const row = this.getRow(keys);
    // 根据列名，返回第几列单元格
    return this.getColumeIndexByColumeName(name).then(colume => {
      return row.$$(body_cell_selector).get(colume.columeIndex);
    });
  }

  /**
   * 获取table 有多少行
   *
   *
   * @example getRowCount().then(function(rowCount) {
      console.log('Row count is : ' + rowCount)
    })

   * @return 返回表格有多少行
   */
  getRowCount(): promise.Promise<number> {
    const rowlist = this.auiTable.$$(this._rowSelector);
    return rowlist.count().then(rowCount => {
      return rowCount;
    });
  }

  /**
   * 返回多少列
   * @return {wdpromise.Promise<number>}
   */
  getColumeCount(): promise.Promise<number> {
    return this.auiTable.$$(this._headerCellSelector).count();
  }

  /**
   * 仅适用于列表的某个列的显示文本的生序排序的验证
   *
   * @parameter {columeName} 列名
   *
   * @example verifyAscending('名称')
   * @return void
   */
  verifyAscending(columeName) {
    const resourceTable = this.getColumeTextByName(columeName);
    resourceTable.then((columeTextlist: any) => {
      // 复制一个新数组
      const expect1: any = columeTextlist.slice();
      expect(expect1).toEqual(columeTextlist.sort());
    });
  }

  /**
   * 仅适用于列表的某个列的显示文本的降序排序的验证
   *
   * @parameter {columeName} 列名
   *
   * @example verifyAscending('名称')
   * @return void
   */
  verifyDescending(columeName) {
    const resourceTable = this.getColumeTextByName(columeName);
    resourceTable.then((columeTextlist: any) => {
      // 复制一个新数组
      const expect1 = columeTextlist.slice();
      expect(expect1).toEqual(columeTextlist.sort(CommonMethod.stringDown));
    });
  }

  waitRowCountChangeto(rowCount) {
    CommonPage.waitRowcountTextChangeTo(
      this.auiTable.$$(this._rowSelector),
      rowCount,
    );
    browser.sleep(1000);
  }

  /**
   * 获得整个表格的文本内容
   */
  getText(): promise.Promise<string> {
    return this.auiTable.getText();
  }
}
