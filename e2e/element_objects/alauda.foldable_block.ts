/**
 * alo-foldable-block 按钮控件,
 * 在创建应用的页面，计算组件-高级，容器-高级有使用
 * Created by liuwei on 2018/9/14.
 */

import { ElementFinder, promise } from 'protractor';

import { AlaudaButton } from './alauda.button';
import { AlaudaElementBase } from './element.base';

export class FoldableBlock extends AlaudaElementBase {
  private _foldableBlock;
  private _hiddenSelector;

  constructor(
    foldableBlock: ElementFinder,
    hiddenSelector = 'aui-icon svg[class*="down"]',
  ) {
    super();
    this._foldableBlock = foldableBlock;
    this._hiddenSelector = hiddenSelector;
  }

  get root(): ElementFinder {
    this.waitElementPresent(this._foldableBlock);
    return this._foldableBlock;
  }

  /**
   * 判断 alo-foldable-block 控件是否存在
   */
  isPresent(): promise.Promise<boolean> {
    return this.root.isPresent();
  }

  /**
   * 展开
   * @example expand()
   */
  expand() {
    this.root
      .$(this._hiddenSelector)
      .isPresent()
      .then(isPresent => {
        if (isPresent) {
          this._click();
        }
      });
  }
  /**
   * 折叠
   * @example fold()
   */
  fold() {
    this.root
      .$(this._hiddenSelector)
      .isPresent()
      .then(isPresent => {
        if (!isPresent) {
          this._click();
        }
      });
  }

  /**
   * 单击按钮
   */
  private _click(): promise.Promise<void> {
    // this.scrollToView(this.root);
    const button = new AlaudaButton(this.root);
    return button.click();
  }
}
