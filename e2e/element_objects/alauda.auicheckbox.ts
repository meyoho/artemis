/**
 * created by lrz on 2018/12/14.
 */
import { $, ElementFinder, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AlaudaAuiCheckbox extends AlaudaElementBase {
  private _checkbox: ElementFinder;

  /**
   * 构造函数
   * @param checkbox 复选框
   */
  constructor(checkbox: ElementFinder = $('.aui-checkbox')) {
    super();
    this._checkbox = checkbox;
  }

  /**
   * 判断复选框是否存在
   */
  isPrsent(): promise.Promise<boolean> {
    return this._checkbox.isPresent();
  }

  /**
   * 是否选中
   */
  isChecked(): promise.Promise<boolean> {
    return this.checkbox.$('div[class*=isChecked]').isPresent();
  }

  /**
   * 复选框
   */
  get checkbox(): ElementFinder {
    this.waitElementPresent(this._checkbox);
    return this._checkbox;
  }

  /**
   * 选中复选框
   */
  check() {
    this.isChecked().then(ispresent => {
      if (!ispresent) {
        this.waitElementClickable(this.checkbox.$('i'));
        this.checkbox.$('i').click();
      }
    });
  }

  /**
   * 取消选中复选框
   */
  uncheck() {
    this.isChecked().then(ispresent => {
      if (ispresent) {
        this.waitElementClickable(this.checkbox.$('i'));
        this.checkbox.$('i').click();
      }
    });
  }
}
