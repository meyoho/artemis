/**
 * alo-array-form-table 下拉框列表控件
 * 创建应用页面，计算组件-高级 部分，组件标签用到这个控件了
 * Created by liuwei on 2018/8/13.
 */
import { $, $$, ElementFinder, browser, promise } from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AlaudaInputGroup } from './alauda.auiInput.group';
import { AuiMultiSelect } from './alauda.auiMultiSelect';
import { AuiSelect } from './alauda.auiSelect';
import { AuiSwitch } from './alauda.auiSwitch';
import { AlaudaButton } from './alauda.button';
import { AlaudaInputbox } from './alauda.inputbox';
import { AlaudaElementBase } from './element.base';
import { AlaudaTimePickr } from './acp/workload/alauda.timepickr';

export class ArrayFormTable extends AlaudaElementBase {
  private _alkArrayFormTable: ElementFinder;
  private _buttonAdd_Selector: string;
  private _buttonOther_Selector: string;
  private _tr_Selector: string;

  /**
   * 构造函数
   * @param alkArrayFormTable ElementFinder 类型，alo-array-form-table 页面元素
   */
  constructor(
    alkArrayFormTable: ElementFinder,
    buttonAdd_Selector = '.alo-form-table__bottom-control-buttons button:nth-child(1)',
    buttonOther_Selector = '.alo-form-table__bottom-control-buttons button:nth-child(2)',
    tr_Selector = 'tr[rowtemplateindex="0"]',
  ) {
    super();
    this._alkArrayFormTable = alkArrayFormTable;
    this._buttonAdd_Selector = buttonAdd_Selector;
    this._buttonOther_Selector = buttonOther_Selector;
    this._tr_Selector = tr_Selector;
  }

  /**
   * 获得alk-array-from-table 控件
   */
  get alkArrayFormTable(): ElementFinder {
    this.waitElementPresent(this._alkArrayFormTable);
    return this._alkArrayFormTable;
  }

  /**
   * 获得表头的文字
   */
  get headerText(): promise.Promise<string> {
    return this.alkArrayFormTable.$$('th').getText();
  }

  /**
   * 添加按钮
   */
  get buttonAdd() {
    return new AlaudaButton(this.alkArrayFormTable.$(this._buttonAdd_Selector));
  }

  get buttonOther() {
    return new AlaudaButton(
      this.alkArrayFormTable.$(this._buttonOther_Selector),
    );
  }

  /**
   * 填写表格
   * @param parameters
   * @example fill([['key1', 'value1'], ['key2', 'value2']]);
   */
  fill(parameters: Array<Array<any>>) {
    this.alkArrayFormTable
      .$$(this._tr_Selector)
      .count()
      .then(count => {
        const length = parameters.length - count;
        for (let i = 0; i < length; i++) {
          this.buttonAdd.button.isEnabled().then(isEnabled => {
            if (isEnabled) {
              this.buttonAdd.click();
            }
          });
        }
      });
    this.alkArrayFormTable.$$(this._tr_Selector).each((elem, index) => {
      elem.$$('td>*:first-child').each((tdElem, index1) => {
        if (parameters[index]) {
          elem = this.alkArrayFormTable.$$(this._tr_Selector).get(index);
          tdElem = elem.$$('td>*:first-child').get(index1);
          tdElem.getTagName().then(tagName => {
            elem
              .$$('td')
              .get(index1)
              .getAttribute('hidden')
              .then(ishidden => {
                if (ishidden !== 'true') {
                  switch (tagName) {
                    case 'div':
                      tdElem.getAttribute('class').then(value => {
                        if (value !== 'empty-block') {
                          const aui_select = tdElem.$('aui-select');
                          const aui_input = tdElem.$('input');
                          const aui_textarea = tdElem.$('textarea');

                          aui_select.isPresent().then(isPresent => {
                            if (isPresent) {
                              this._fillForm(
                                aui_select,
                                parameters[index][index1],
                              );
                            } else {
                              let isSametd = true;
                              aui_input.isPresent().then(isPresent1 => {
                                isSametd = isSametd && isPresent1;
                                if (isPresent1) {
                                  aui_input.isEnabled().then(isEnabled => {
                                    if (isEnabled) {
                                      this._fillForm(
                                        aui_input,
                                        parameters[index][index1],
                                      );
                                    }
                                  });
                                }
                              });

                              aui_textarea.isPresent().then(isPresent1 => {
                                isSametd = isSametd && isPresent1;
                                if (isPresent1) {
                                  aui_textarea.isEnabled().then(isEnabled => {
                                    if (isEnabled) {
                                      if (isSametd) {
                                        this._fillForm(
                                          aui_textarea,
                                          parameters[index][index1 + 1],
                                        );
                                      } else {
                                        this._fillForm(
                                          aui_textarea,
                                          parameters[index][index1],
                                        );
                                      }
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                      break;
                    case 'alu-notification-receiver-form-item':
                      tdElem
                        .$$(
                          'aui-form-item[class="object-item"] .aui-form-item__content>*:first-child',
                        )
                        .each((item, itemIndex) => {
                          if (itemIndex === 2) {
                            new AlaudaInputbox(
                              $$(
                                'alu-notification-receiver-form-item>div>aui-form-item:nth-child(3) input',
                              ).get(index),
                            ).input(parameters[index][itemIndex]);
                          } else {
                            this._fillForm(item, parameters[index][itemIndex]);
                          }
                        });

                      break;
                    case 'rc-hpa-cron-trigger-form':
                      tdElem
                        .$('aui-multi-select')
                        .isPresent()
                        .then(ispresent => {
                          if (ispresent) {
                            const week = parameters[index][index1][0];
                            const multiSelect = new AuiMultiSelect(
                              tdElem.$('aui-multi-select'),
                              $('.cdk-overlay-pane aui-tooltip'),
                            );
                            multiSelect.clearTags();
                            week.split(',').forEach(day => {
                              multiSelect.input(day);
                            });
                            const time = parameters[index][index1][1];
                            const timePickr = new AlaudaTimePickr(
                              tdElem.$('rc-timepickr'),
                            );
                            timePickr.select(time);
                          } else {
                            new AlaudaInputbox(tdElem.$('input')).input(
                              parameters[index][index1],
                            );
                          }
                        });
                      break;
                    default: {
                      this._fillForm(tdElem, parameters[index][index1]);
                    }
                  }
                }
              });
          });
        }
      });
    });
  }

  /**
   * 单击控件底部的button
   * @param buttonInnerText button上的显示文字
   */
  click(
    buttonInnerText = '添加',
    button_father_css_seletor = '.alo-form-table__bottom-control-buttons',
  ): promise.Promise<void> {
    // 找到要操作的按钮
    const bottonButtons = this.alkArrayFormTable
      .$(button_father_css_seletor)
      .$$('button');
    const button = bottonButtons
      .filter(elem => {
        return elem.getText().then(text => {
          return text === buttonInnerText;
        });
      })
      .first();
    // 等待按钮出现
    this.waitElementPresent(button).then(isPresent => {
      if (!isPresent) {
        console.log(`要单击的button元素【${buttonInnerText}】没出现`);
      }
    });
    // 单击按钮
    this.scrollToView(button).then(() => {
      button.click();
    });
    return browser.sleep(100);
  }

  private _fillForm(elem: ElementFinder, parameters) {
    elem.getTagName().then(tagName => {
      this.waitElementPresent(elem);
      let multiSelect: AuiMultiSelect;
      let auiIcon: AuiIcon;
      let inputbox_key: AlaudaInputbox;
      let auiInputGroup: AlaudaInputGroup;
      let button: AlaudaButton;
      let auiSelect: AuiSelect;

      switch (tagName) {
        case 'input':
          elem.getAttribute('readonly').then(readonly => {
            if (!readonly) {
              elem.click();
              browser.sleep(100);
              const inputbox = new AlaudaInputbox(elem);
              elem.isEnabled().then(isEnabled => {
                if (isEnabled) {
                  inputbox.input(String(parameters));
                }
              });
            }
          });
          break;
        case 'textarea':
          elem.getAttribute('readonly').then(readonly => {
            if (!readonly) {
              elem.isEnabled().then(isEnabled => {
                if (isEnabled) {
                  new AlaudaInputbox(elem).input(parameters);
                }
              });
            }
          });
          break;
        case 'aui-select':
          auiSelect = new AuiSelect(elem, $('.cdk-overlay-pane aui-tooltip'));
          if (Array.isArray(parameters)) {
            auiSelect.select(parameters[0], parameters[1]);
          } else {
            if (parameters !== '') {
              auiSelect.select(parameters);
            }
          }

          break;
        case 'aui-multi-select':
          multiSelect = new AuiMultiSelect(
            elem,
            $('.cdk-overlay-pane aui-tooltip'),
          );
          if (Array.isArray(parameters)) {
            parameters.forEach(item => {
              multiSelect.select(item);
            });
          } else {
            multiSelect.select(parameters);
          }
          break;
        case 'aui-icon':
          auiIcon = new AuiIcon(elem);
          if (String(parameters).toLowerCase() === 'true') {
            auiIcon.click();
          }
          break;
        case 'div':
          inputbox_key = new AlaudaInputbox(
            elem.$('.aui-form-item__content input'),
          );
          inputbox_key.isPresent().then(isPresent => {
            if (isPresent) {
              inputbox_key.input(parameters[0]);
            }

            const inputbox_value = new AlaudaInputbox(
              elem.$('.aui-form-item__content textarea'),
            );
            inputbox_value.isPresent().then(isPresent1 => {
              if (isPresent1) {
                inputbox_value.input(parameters[1]);
              }
            });
          });
          break;
        case 'aui-input-group':
          auiInputGroup = new AlaudaInputGroup(elem);
          auiInputGroup.input(parameters);
          break;
        case 'span':
          break;
        case 'button':
          button = new AlaudaButton(elem);
          if (String(parameters).toLowerCase() === 'true') {
            button.click();
          }
          break;
        case 'aui-switch':
          const auiswitch = new AuiSwitch(elem);
          if (String(parameters).toLowerCase() === 'true') {
            auiswitch.open();
          }
          break;
        default: {
          console.log('tagname: ' + tagName);
          throw new Error('没有找到任何控件');
        }
      }
    });
  }
}
