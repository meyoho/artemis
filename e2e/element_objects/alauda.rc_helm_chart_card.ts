import { AlaudaElementBase } from './element.base';
import { ElementFinder } from 'protractor';
import { AlaudaButton } from './alauda.button';

export class HelmChartCard extends AlaudaElementBase {
  private root: ElementFinder;
  private img_selector: string;
  private name_selector: string;
  private describe_selector: string;
  private create_app_button_selector: string;
  constructor(
    helm_chart_card: ElementFinder,
    img_selector = '.chart-card__img',
    name_selector = '.chart-card__name',
    describe_selector = '.chart-card__desc',
    create_app_button_selector = 'acl-disabled-container',
  ) {
    super();
    this.root = helm_chart_card;
    this.img_selector = img_selector;
    this.name_selector = name_selector;
    this.describe_selector = describe_selector;
    this.create_app_button_selector = create_app_button_selector;
  }
  get helm_chart_card() {
    return this.root;
  }
  get image() {
    return this.root.$(this.img_selector);
  }
  get name() {
    return this.root.$(this.name_selector).getText();
  }
  get describetion() {
    return this.root.$(this.describe_selector).getText();
  }
  clickAddApp() {
    const button = new AlaudaButton(
      this.root.$(this.create_app_button_selector),
    );
    return button.click();
  }
}
