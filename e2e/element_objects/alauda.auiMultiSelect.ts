/**
 * aui-select 下拉框列表控件
 * 域名管理，添加域名页面，绑定项目用到了这个控件
 * Created by liuwei on 2018/8/13.
 */
import {
  ElementArrayFinder,
  ElementFinder,
  promise,
  protractor,
  browser,
} from 'protractor';

import { AuiIcon } from './alauda.aui_icon';
import { AuiToolTip } from './alauda.auiToolTip';
import { AlaudaElementBase } from './element.base';

export class AuiMultiSelect extends AlaudaElementBase {
  private _auiSelect: ElementFinder;
  private _auiToolTip: ElementFinder;

  /**
   * 构造函数
   * @param auiSelect ElementFinder 类型，auiSelect 页面元素
   * @param auiToolTip ElementFinder 类型, auiToolTip 页面元素
   */
  constructor(auiSelect: ElementFinder, auiToolTip: ElementFinder) {
    super();
    this._auiSelect = auiSelect;
    this._auiToolTip = auiToolTip;
  }

  get root(): ElementFinder {
    this.waitElementPresent(this._auiSelect);
    return this._auiSelect;
  }

  /**
   * 在文本框中输入一个值
   * @param inputValue 要输入的值
   */
  input(inputValue: string): promise.Promise<void> {
    this.root.click();
    this.root.$('input').sendKeys(inputValue);
    this.root.$('input').sendKeys(protractor.Key.ENTER);
    this.root.click();
    return browser.sleep(100);
  }

  /**
   * Aui-select 控件的文本框
   */
  get auiTags(): ElementArrayFinder {
    return this._auiSelect.$$('aui-multi-select aui-tag');
  }

  /**
   * Aui-select 控件的aui-icon 控件
   */
  get auiIcon(): AuiIcon {
    return new AuiIcon(this._auiSelect.$('.aui-multi-select__suffix aui-icon'));
  }

  /**
   * Aui-select 控件的下拉框
   */
  get auiToolTip(): AuiToolTip {
    return new AuiToolTip(this._auiToolTip);
  }

  /**
   * 获得文本框中的输入值
   */
  get value(): promise.Promise<string> {
    return this.auiTags.getText();
  }

  /**
   * 单击 icon, 从下拉框中选择一个item
   * @param text 要选择的item
   * @param itemListSelector string， 所有下拉项的css 选择器
   */
  select(key: string, value = ''): promise.Promise<void> {
    const inputValue: ElementFinder = this.auiTags
      .filter(elem => {
        return elem
          .$('span')
          .getText()
          .then(text => {
            return text === key || text === value;
          });
      })
      .first();
    return inputValue.isPresent().then(isPresent => {
      if (!isPresent) {
        this.auiIcon.click();
        return this.auiToolTip.tagName.then(tagName => {
          switch (tagName) {
            case 'aui-option-group':
              this.auiToolTip.selectAui_option_group(key, value);
              this.auiIcon.click();
              break;
            case 'aui-option':
              this.auiToolTip.selectAui_option(key);
              this.auiToolTip.isPresent().then(isPresent1 => {
                if (isPresent1) {
                  this.auiIcon.click();
                }
              });
              break;
          }
        });
      }
    });
  }

  /**
   * 单击 icon, 从下拉框中选择一个item
   * @param text 要选择的item
   * @param itemListSelector string， 所有下拉项的css 选择器
   */
  unselect(key: string, value = ''): promise.Promise<void> {
    const inputValue: ElementFinder = this.auiTags
      .filter(elem => {
        return elem
          .$('span')
          .getText()
          .then(text => {
            return text === key || text === value;
          });
      })
      .first();
    return inputValue.isPresent().then(isPresent => {
      if (isPresent) {
        inputValue.$('aui-icon').click();
      }
    });
  }
  clearTags(): promise.Promise<void> {
    this.auiTags.each(elem => {
      elem.$('aui-icon').click();
    });
    return browser.sleep(1);
  }
}
