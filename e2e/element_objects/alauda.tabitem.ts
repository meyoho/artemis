/**
 * 详情页的tab标签控件
 * Created by alauda on 2018/3/2.
 */

import { browser, element } from 'protractor';

import { CommonPage } from '../utility/common.page';

export class AlaudaTabItem {
    private tabItem;
    private tabActive;
    private tabClick;

    constructor(tabItem, tabActive, tabClick) {
        this.tabItem = tabItem;
        this.tabActive = tabActive;
        this.tabClick = tabClick;
    }

    /**
     * tabitem验证
     */
    getTabItem() {
        return element.all(this.tabItem);
    }
    getActiveText() {
        CommonPage.waitElementPresent(element(this.tabActive));
        return element(this.tabActive).getText();
    }
    click() {
        CommonPage.waitElementPresent(element(this.tabClick));
        element(this.tabClick).click();
        return browser.sleep(500);
    }

    checkTabActiveIsPresent() {
        CommonPage.waitElementPresent(element(this.tabActive));
        return element(this.tabActive).isPresent();
    }

    checkTabClickIsPresent() {
        CommonPage.waitElementPresent(element(this.tabClick));
        return element(this.tabClick).isPresent();
    }
}
