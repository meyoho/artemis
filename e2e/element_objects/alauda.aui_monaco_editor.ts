/**
 * aui-monaco-editor 控件
 * 为微服务环境安装的页面用到了这个控件
 * Created by liuwei on 2018/8/30.
 */
import { ElementFinder, promise } from 'protractor';

import { AlaudaElementBase } from './element.base';

export class AuiMonacoEditor extends AlaudaElementBase {
    private _auiMonacoEditor: ElementFinder;
    constructor(auiMonacoEditor: ElementFinder) {
        super();
        this._auiMonacoEditor = auiMonacoEditor;
    }

    /**
     * 获得aui-monaco-editor控件
     */
    get auiMonacoEditor(): ElementFinder {
        this.waitElementPresent(this._auiMonacoEditor);
        return this._auiMonacoEditor;
    }

    /**
     * 获得控件的文本内容
     */
    getText(): promise.Promise<string> {
        return this.auiMonacoEditor.$$('.mtk1').getText();
    }
}
