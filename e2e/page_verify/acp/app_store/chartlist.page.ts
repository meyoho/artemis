import { ChartListPage } from '@e2e/page_objects/acp/app_store/chart.list.page';

export class ChartListVerify extends ChartListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toEqual(expectValue[key]);
          break;
        case '帮助信息':
          this.alert_content_text.then(text => {
            expect(text).toContain(expectValue[key]);
          });
          break;
        case '数量':
          this.get_chart_cards_count().then(ct => {
            expect(ct).toBe(expectValue[key]);
          });
          break;
        case '当前chart源':
          break;
        case '模板':
          const tpls = expectValue[key] as Array<Record<string, any>>;
          tpls.forEach(tpl => {
            this.get_chart_card_by_name(tpl['名称']).then(chart => {
              if (chart === null) {
                console.error(`未找到chart ${tpl['名称']}`);
                expect(chart).not.toBe(null);
              } else {
                for (const k in tpl) {
                  switch (k) {
                    case '名称':
                      chart.name.then(text => {
                        expect(text).toBe(tpl[k]);
                      });
                      break;
                    case '注释':
                      chart.describetion.then(text => {
                        expect(text).toBe(tpl[k]);
                      });
                      break;
                    default:
                      break;
                  }
                }
              }
            });
          });
          break;
      }
    }
  }
}
