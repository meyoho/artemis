import { AppListPage } from '@e2e/page_objects/acp/app_store/app.list.page';

export class TplAppListVerify extends AppListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '提示信息':
          expect(this.alert_content_text).toBe(expectValue[key]);
          break;
        case '表头':
          expect(this.tpl_app_table.getHeaderText()).toEqual(expectValue[key]);
          break;
        case '数量':
          expect(this.tpl_app_table.getRowCount()).toBe(expectValue[key]);
          break;
      }
    }
  }
}
