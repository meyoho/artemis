import { AppDetailPage } from '@e2e/page_objects/acp/app_store/app.detail.page';

export class TplAppDetailVerify extends AppDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toBe(expectValue[key]);
          break;
        case '名称':
          expect(this.name).toContain(expectValue[key]);
          break;
        case '资源列表名称':
          expect(this.rs_list_name).toBe(expectValue[key]);
          break;
        case '资源列表表头':
          expect(this.rs_list_table.getHeaderText()).toEqual(expectValue[key]);
          break;
        case '资源':
          const rss = expectValue[key] as Array<Array<string>>;
          rss.forEach(rs => {
            this.rs_list_table.hasRow(rs).then(has => {
              expect(has).toBe(true);
            });
          });
          break;
        default:
          this.getElementByText(key, '.field-set-item__value')
            .getText()
            .then(text => {
              expect(text).toContain(expectValue[key]);
            });
          break;
      }
    }
  }
}
