import { ChartDetailPage } from '@e2e/page_objects/acp/app_store/chart.detail.page';

export class ChartDetailVerify extends ChartDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toEqual(expectValue[key]);
          break;
        case '名称':
          this.name.then(text => {
            expect(text).toContain(expectValue[key]);
          });

          break;
        default:
          this.getElementByText(key, '.field-set-item__value')
            .getText()
            .then(text => {
              expect(text).toContain(expectValue[key]);
            });
          break;
      }
    }
  }
}
