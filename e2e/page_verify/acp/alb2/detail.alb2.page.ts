import { DetaiAlb2Page } from '@e2e/page_objects/acp/alb2/detail.alb2.page';

export class DetaiAlb2PageVerify extends DetaiAlb2Page {
  verify(expectValue: { [key: string]: string | { [key: string]: string } }) {
    for (const key in expectValue) {
      switch (key) {
        // 验证基础信息
        case '名称':
          expect(this.name).toContain(expectValue[key]);
          break;
        case '所属集群':
        case '类型':
          expect(this.getElementByText(key).getText()).toContain(
            expectValue[key],
          );
          break;
        case '分配项目':
          expect(this.getElementByText('项目').getText()).toContain(
            expectValue[key],
          );
          break;
        case '侦听器':
          this.getElementByText(key)
            .getRow([expectValue[key]['端口']])
            .getText()
            .then(text => {
              const dicExpect = expectValue[key] as { [key: string]: string };
              for (const keyExpect in dicExpect) {
                //找到名称所在的行，获得该行的文字，验证包含期望值
                expect(text.replace(/\s/g, '')).toContain(dicExpect[keyExpect]);
              }
            });
          break;
        case '删除':
          this.getElementByText('侦听器')
            .getColumeTextByName('端口')
            .then(text => {
              expect(text).not.toContain(expectValue[key]);
            });
          break;

        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
      }
    }
  }
}
