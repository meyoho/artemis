import { Alb2ListPage } from '@e2e/page_objects/acp/alb2/list.alb2.page';

export class Alb2ListPageVerify extends Alb2ListPage {
  verify(expectValue: {
    [key: string]: string | number | { [key: string]: string };
  }) {
    for (const key in expectValue) {
      switch (key) {
        case '检索':
          this.table.getRowCount().then(count => {
            expect(count).toBe(expectValue[key] as number);
          });
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '创建':
          this.table
            .getRow([expectValue[key]['名称']])
            .getText()
            .then(text => {
              const dicExpect = expectValue[key] as {
                [key: string]: string;
              };
              for (const keyExpect in dicExpect) {
                //找到名称所在的行，获得该行的文字，验证包含期望值
                expect(text.replace(/\s/g, '')).toContain(dicExpect[keyExpect]);
              }
            });
          break;
      }
    }
  }
}
