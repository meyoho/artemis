import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { DetaiFrontendPage } from '@e2e/page_objects/acp/alb2/detail.frontend.page';
import { $, $$, browser, promise } from 'protractor';

export class DetaiFrontendPageVerify extends DetaiFrontendPage {
  verify(expectValue: {
    [key: string]: string | string[] | number | { [key: string]: string };
  }) {
    for (const key in expectValue) {
      switch (key) {
        // 验证基本信息
        case '名称':
        case '所属集群':
          break;
        case '协议':
        case '默认内部路由':
          expect(this.getElementByText(key).getText()).toContain(
            expectValue[key],
          );
          break;
        case '默认内部路由不包含':
          expect(this.getElementByText('默认内部路由').getText()).not.toContain(
            expectValue[key],
          );
          break;
        case '规则':
          const table: AuiTableComponent = this.getElementByText(key);
          let text: promise.Promise<string>;
          if (expectValue[key][0] === '-') {
            text = table.getText();
          } else {
            text = table.getRows([expectValue[key][0]]).getText();
          }
          text.then(text => {
            const expectData = new Array(text.toString().split('\n')).sort();
            let allExpectData = expectData[0];
            table.getCell('规则', [expectValue[key][0]]).then(ruleele => {
              ruleele
                .$('button[aui-button="primary"]')
                .isPresent()
                .then(isPresent => {
                  if (isPresent) {
                    ruleele.$('button[aui-button="primary"]').click();
                    $('.cdk-overlay-pane aui-tooltip')
                      .getText()
                      .then(tooltiptxt => {
                        const tooltipData = new Array(
                          tooltiptxt.split('\n'),
                        ).sort();
                        allExpectData = expectData[0].concat(tooltipData[0]);
                        for (const actualData of expectValue[key] as string[]) {
                          expect(allExpectData).toContain(actualData);
                          // expect(
                          //   allExpectData.includes(actualData),
                          // ).toBeTruthy();
                        }
                      });
                  } else {
                    for (const actualData of expectValue[key] as string[]) {
                      // expect(allExpectData.includes(actualData)).toBeTruthy();
                      expect(allExpectData).toContain(actualData);
                    }
                  }
                });
            });
          });

          break;
        case '删除规则':
          browser.sleep(1000);
          this.waitElementPresent(
            $('.detail-port-list>div'),
            '端口详情页，规则表格没出现',
          ).then(isPresent => {
            if (isPresent) {
              const table1: AuiTableComponent = this.getElementByText('规则');

              table1.getText().then(text => {
                const expectData = new Array(text.split('\n')).sort();

                for (const actualData of expectValue[key] as string[]) {
                  expect(expectData[0].includes(actualData)).toBeFalsy();
                }
              });
            } else {
              throw new Error('端口详情页，规则表格没出现');
            }
          });

          break;
        case '不能删除规则':
          const option = new AlaudaDropdown(
            this.getElementByText('规则')
              .getRow([expectValue[key]])
              .$('button[aui-button="text"]'),
            $$('.cdk-overlay-pane aui-tooltip aui-menu-item button'),
          );
          option.getDataAttr('删除规则').then(text => {
            expect(text).toBe('true');
          });
          option.getDataAttr('更新规则').then(text => {
            expect(text).toBe(null);
          });
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
      }
    }
  }
}
