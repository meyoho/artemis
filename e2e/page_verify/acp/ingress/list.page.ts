import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { IngressListPage } from '@e2e/page_objects/acp/ingress/list.page';

export class IngressListPageVerify extends IngressListPage {
  verify(expectValue: {
    [key: string]: string | number | Map<string, string>;
  }) {
    for (const key in expectValue) {
      switch (key) {
        case '数量':
          const value = expectValue[key] as number;
          if (value === 0) {
            expect(this.noData.isPresent()).toBeTruthy();
          } else {
            this.ingressNameTable.getRowCount().then(count => {
              expect(count).toBe(value);
            });
            // });
          }
          break;
        case '规则':
          const auiTableRule = new AlaudaAuiTable(
            this.ingressNameTable.auiTable,
          );
          for (const [ingressName, detail] of (expectValue[key] as Map<
            string,
            string
          >).entries()) {
            auiTableRule
              .getRow([ingressName])
              .getText()
              .then(text => {
                expect(text).toContain(detail);
              });
          }

          break;
      }
    }
  }
}
