import { DetailIngressPage } from '@e2e/page_objects/acp/ingress/detail.page';

export class DetailIngressPageVerify extends DetailIngressPage {
  verify(expectValue: {
    [key: string]:
      | string
      | string[]
      | { [key: string]: string }
      | { [key: string]: string | number }[];
  }) {
    for (const key in expectValue) {
      switch (key) {
        case '名称':
          this.name.then(name => {
            expect(name.trim()).toContain(expectValue[key] as string);
          });
          break;
        case '面包屑':
          this.breadcrumb.getText().then(text => {
            expect(text.trim()).toBe(expectValue[key] as string);
          });
          break;
        case '规则':
          (expectValue[key] as { [key: string]: string | number }[]).forEach(
            rule => {
              this.rules
                .getRow([rule['地址']])
                .getText()
                .then(text => {
                  console.log(text);
                  expect(text).toContain(rule['内部路由'] as string);
                  expect(text).toContain(rule['服务端口'] as string);
                });
            },
          );

          break;
        case 'HTTPS证书':
          //[{ 保密字典: secret_name1, 域名: domain_name_ext }]
          (expectValue[key] as { [key: string]: string | number }[]).forEach(
            cert => {
              this.tls
                .getRow([cert['保密字典']])
                .getText()
                .then(text => {
                  expect(text).toContain(cert['域名'] as string);
                });
            },
          );
          break;
        case 'HTTPS证书无数据':
          expect(this.notls.isPresent()).toBeTruthy();
          break;
        case 'YAML':
          // 验证YAML正确
          const yamlvalue = this.alaudaCodeEdit.getYamlValue();

          (expectValue[key] as string[]).forEach(item => {
            expect(yamlvalue).toContain(item);
          });
          break;
        case '重复校验':
          this.auiDialog.title.getText().then(text => {
            expect(text).toContain('访问地址配置重复');
          });
          this.auiDialog.content.getText().then(text => {
            expect(text).toContain(expectValue[key] as string);
          });
          this.auiDialog.clickConfirm();
          break;
      }
    }
  }
}
