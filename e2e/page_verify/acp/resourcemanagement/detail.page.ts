import { DetailResourcePage } from '@e2e/page_objects/acp/resource_management/detail.page';
import { browser } from 'protractor';

export class DetailPageVeriy extends DetailResourcePage {
  verify(expectValue) {
    return browser.sleep(1).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '详情YAML':
            this.alaudaCodeEdit.getYamlValue().then(stringyaml => {
              expect(stringyaml).toContain(expectValue[key]);
            });
            this.closeButton.click();
            browser.sleep(1000);
            break;
        }
      }
    });
  }
}
