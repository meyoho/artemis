import { ListPage } from '@e2e/page_objects/acp/resource_management/list.page';
import { browser } from 'protractor';

export class ListPageVerify extends ListPage {
  verify(expectValue) {
    browser.sleep(1).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '命名空间相关':
            expect(this.resource.relatedWithNs.getText()).toBe(
              expectValue[key],
            );
            break;
          case '资源数量':
            if (expectValue[key] === 0) {
              expect(this.noData.isPresent()).toBeTruthy();
            } else {
              expect(this.ResourceNameTable.getRowCount()).toBe(
                expectValue[key],
              );
            }

            break;
          // 验证面包屑 信息
          case '集群相关':
            expect(this.resource.relatedWithCluster.getText()).toBe(
              expectValue[key],
            );
            break;
          case '标签':
            this.ResourceNameTable.getCell('标签', [expectValue[key]]).then(
              elem => {
                elem.getText().then(text => {
                  expect(text).toBe('update: update');
                });
              },
            );
            break;
          case '命名空间标签':
            // this.searchResource(expectValue[key]);
            this.ResourceNameTable.getCell('标签', [expectValue[key]]).then(
              elem => {
                elem.getText().then(text => {
                  expect(text).toContain('a: a');
                });
              },
            );
            break;
        }
      }
    });
  }
}
