import { AlaudaDropdown } from '@e2e/element_objects/alauda.dropdown';
import { DomainListPage } from '@e2e/page_objects/acp/domain/list.page';
import { $, $$ } from 'protractor';

export class DomainListPageVerify extends DomainListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '全域名不分配集群':
                        for (const domainName in expectValue[key]) {
                            if (expectValue[key].hasOwnProperty(domainName)) {
                                this.domainNameTable
                                    .getRow([domainName])
                                    .getText()
                                    .then(text => {
                                        expect(
                                            text
                                                .replace(/\s/g, '')
                                                .replace(/\n/g, '')
                                        ).toBe(expectValue[key][domainName]);
                                    });
                            }
                        }

                        break;
                    case '泛域名分配集群':
                        for (const domainName in expectValue[key]) {
                            if (expectValue[key].hasOwnProperty(domainName)) {
                                this.domainNameTable
                                    .getRow([domainName])
                                    .getText()
                                    .then(text => {
                                        expect(
                                            text
                                                .replace(/\s/g, '')
                                                .replace(/\n/g, '')
                                        ).toBe(expectValue[key][domainName]);
                                    });
                            }
                        }

                        break;
                    case '全域名分配集群':
                        for (const domainName in expectValue[key]) {
                            if (expectValue[key].hasOwnProperty(domainName)) {
                                this.domainNameTable
                                    .getRow([domainName])
                                    .getText()
                                    .then(text => {
                                        expect(
                                            text
                                                .replace(/\s/g, '')
                                                .replace(/\n/g, '')
                                        ).toBe(expectValue[key][domainName]);
                                    });
                            }
                        }

                        break;
                    case '泛域名分配集群':
                        for (const domainName in expectValue[key]) {
                            if (expectValue[key].hasOwnProperty(domainName)) {
                                this.domainNameTable
                                    .getRow([domainName])
                                    .getText()
                                    .then(text => {
                                        expect(
                                            text
                                                .replace(/\s/g, '')
                                                .replace(/\n/g, '')
                                        ).toBe(expectValue[key][domainName]);
                                    });
                            }
                        }
                        break;
                    case '删除域名':
                        expect(this.domainNameTable.getText()).not.toContain(
                            expectValue[key]
                        );

                        break;
                }
            }
        }
    }

    checkingress(
        domain_name,
        namespace,
        expect_result,
        projectName = this.projectName
    ) {
        this.enterUserView(namespace, projectName);
        this.clickLeftNavByText('访问规则');
        this.getButtonByText('创建访问规则').click();
        const domainlist: AlaudaDropdown = new AlaudaDropdown(
            $('.aui-form-item__container aui-select:nth-child(2)'),
            $$('.cdk-overlay-pane aui-tooltip aui-option')
        );
        expect(domainlist.checkDataExistInDropdown(domain_name)).toBe(
            expect_result
        );
    }
}
