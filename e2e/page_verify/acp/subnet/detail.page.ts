import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { DetailPage } from '@e2e/page_objects/acp/subnet/detail.page';
import { $, browser } from 'protractor';

export class DetailPageVerify extends DetailPage {
  verify(expectValue) {
    return browser.sleep(1).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '所属集群':
          case '网段':
          case '已用 IP 数':
          case '可用 IP 数':
          case '网关类型':
          case '外出流量 NAT':
          case '子网隔离':
            // this.clickTab('详情信息');
            expect(this.getElementByText(key).getText()).toContain(
              expectValue[key],
            );
            break;
          case '保留 IP':
          case '白名单':
          case '命名空间':
            this.getElementByText(key)
              .getText()
              .then(text => {
                expectValue[key].forEach(i => {
                  expect(text).toContain(i);
                });
              });
            break;
          case '已用 IP':
            this.clickTab('已用 IP');
            if (expectValue[key] === 0) {
              this.waitElementPresent(
                $('.empty-placeholder'),
                '检索无结果控件没有出现',
              );
              expect($('.empty-placeholder').isPresent()).toBeTruthy();
            } else {
              const table = new AlaudaAuiTable(
                this.IpTable.auiTable.$('aui-table'),
              );
              table.getRowCount().then(rowCount => {
                expect(this.IpTable.getRowCount()).toBe(
                  rowCount + expectValue[key],
                );
              });
            }
            break;
          case '过滤IP':
            if (expectValue[key] === 0) {
              this.waitElementPresent(
                $('.empty-placeholder'),
                '检索无结果控件没有出现',
              );
              expect($('.empty-placeholder').isPresent()).toBeTruthy();
            } else {
              expect(this.IpTable.getRowCount()).toBe(expectValue[key]);
            }
            break;
          case '容器组':
            expect(this.IpTable.getColumeTextByName('容器组')).toContain(
              expectValue[key],
            );
            break;
          // 验证面包屑 信息
          case '面包屑':
            expect(this.breadcrumb.getText()).toContain(expectValue[key]);
            break;
        }
      }
    });
  }
}
