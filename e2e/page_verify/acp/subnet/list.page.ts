import { ListPage } from '@e2e/page_objects/acp/subnet/list.page';
import { $, browser } from 'protractor';

export class ListPageVerify extends ListPage {
  verify(expectValue) {
    return browser.sleep(1).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '检索':
            if (expectValue[key] === 0) {
              this.waitElementPresent(
                $('.empty-placeholder'),
                '检索无结果控件没有出现',
              );
              expect($('.empty-placeholder').isPresent()).toBeTruthy();
            } else {
              expect(this.SubnetTable.getRowCount()).toBe(expectValue[key]);
            }
            break;
          case '创建':
            this.SubnetTable.getRow([expectValue[key]['名称']])
              .getText()
              .then(text => {
                const dicExpect = expectValue[key];
                for (const keyExpect in dicExpect) {
                  //找到名称所在的行，获得该行的文字，验证包含期望值
                  expect(text).toContain(dicExpect[keyExpect]);
                }
              });

            break;
          // 验证面包屑 信息
          case '面包屑':
            expect(this.breadcrumb.getText()).toContain(expectValue[key]);
            break;
        }
      }
    });
  }
}
