import { ListPage } from '@e2e/page_objects/acp/chart_repo/chart_repo.list.page';

export class ListPageVerify extends ListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '面包屑':
                        expect(this.breadcrumb.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                    case '表头':
                        expect(this.chartRepoTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.chartRepoTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                    case '数据':
                        expect(
                            this.chartRepoTable.hasRow(expectValue[key]['keys'])
                        ).toBe(expectValue[key]['exists']);
                        break;
                }
            }
        }
    }
}
