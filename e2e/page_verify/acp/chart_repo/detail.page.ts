import { DetailPage } from '@e2e/page_objects/acp/chart_repo/chart_repo.detail.page';

export class DetailPageVerify extends DetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '面包屑':
          expect(this.breadcrumb.getText()).toBe(expectValue[key]);
          break;
        case '详情信息':
          const detail: Record<string, any> = expectValue[key];
          for (const k in detail) {
            switch (k) {
              case '标题':
                expect(this.detail_title).toContain(detail[k]);
                break;
              case '同步状态':
                expect(this.sysc_status).toContain(detail[k]);
                break;
              default:
                expect(
                  this.detail_card
                    .getElementByText(k, '.field-set-item__value')
                    .getText(),
                ).toContain(detail[k]);
                break;
            }
          }
          break;
        case '模板仓库':
          const repos: Record<string, any> = expectValue[key];
          for (const k in repos) {
            switch (k) {
              case '标题':
                expect(this.repos_title).toContain(repos[k]);
                break;
            }
          }
          break;
      }
    }
  }
}
