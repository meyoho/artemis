import { CreatePage } from '@e2e/page_objects/acp/chart_repo/chart_repo.create.page';

export class CreatePageVerify extends CreatePage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title).toContain(expectValue[key]);
                        break;
                }
            }
        }
    }
}
