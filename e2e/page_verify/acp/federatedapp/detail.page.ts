import { DetailPage } from '@e2e/page_objects/acp/federatedapp/detail.page';

export class DetailVerify extends DetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '名称':
          expect(this.name.getText()).toBe(expectValue[key]);
          break;
        case '应用状态':
          this.appStatus.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '工作负载':
          this.workloadTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '内部路由':
          this.serviceTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '配置':
          this.configTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '差异化':
          this.overrideTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '无差异化':
          expect(this.noOverride.getText()).toContain(key);
          break;
        case '无资源':
          expect(this.noResource.getText()).toContain(key);
          break;
      }
    }
  }
}
