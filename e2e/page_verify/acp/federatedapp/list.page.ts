import { ListPage } from '@e2e/page_objects/acp/federatedapp/list.page';

export class ListVerify extends ListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '数量':
          this.FedAppTable.getRowCount().then(count => {
            expect(count).toBe(expectValue[key] as number);
          });
          break;
        case '标题':
          this.breadcrumb.getText().then(text => {
            expect(text).toContain(expectValue[key] as string);
          });
          break;
        case 'Header':
          this.FedAppTable.getHeaderText().then(text => {
            expect(text).toEqual(expectValue[key] as string);
          });
          break;
        case '无联邦应用':
          expect(this.noData.getText()).toContain(key);
          break;
        case '创建':
          this.FedAppTable.getRow([expectValue[key]['名称']])
            .getText()
            .then(text => {
              const dicExpect = expectValue[key] as {
                [key: string]: string | string[];
              };
              for (const keyExpect in dicExpect) {
                //找到名称所在的行，获得该行的文字，验证包含期望值
                if (Array.isArray(dicExpect[keyExpect])) {
                  (dicExpect[keyExpect] as string[]).forEach(tmp => {
                    expect(text.replace(/\s/g, '')).toContain(tmp);
                  });
                } else
                  expect(text.replace(/\s/g, '')).toContain(dicExpect[
                    keyExpect
                  ] as string);
              }
            });
          break;
      }
    }
  }
}
