import { PodDetailPage } from '@e2e/page_objects/acp/pods/pods.detail.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { browser, ElementFinder } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';

export class PodDetailVerifyPage extends PodDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '面包屑':
          this.breadcrumb.getText().then(text => {
            expect(text.trim()).toContain(expectValue[key]);
          });
          break;
        case '名称':
          this.podName.then(podname => {
            expect(podname.trim()).toContain(expectValue[key]);
          });
          break;
        case '容器组标签':
        case '凭据':
          this.showLabels(key).then(labels => {
            expectValue[key].forEach(v_text => {
              expect(labels).toContain(v_text);
            });
          });
          break;
        case '主机选择器':
          this.nodeSelector.getText().then(text => {
            expectValue[key].forEach(v_text => {
              expect(text).toContain(v_text);
            });
          });
          break;
        case '存储卷':
        case 'Pod 亲和':
          const v_data = expectValue[key] as Record<
            string,
            string[] | Array<Array<string>>
          >;
          for (const k in v_data) {
            switch (k) {
              case '表头':
                this.getElementByText(key)
                  .getHeaderText()
                  .then(texts => {
                    expect(texts).toEqual(v_data[k]);
                  });
                break;
              case '存在数据':
                v_data[k].forEach(row_keys => {
                  this.getElementByText(key)
                    .hasRow(row_keys)
                    .then(has => {
                      expect(has).toBe(true);
                    });
                });
                break;
            }
          }
          break;
        case '状态分析':
          const v_status = expectValue[key] as Record<string, any>;
          for (const v_status_key in v_status) {
            switch (v_status_key) {
              case '表头':
                this.status_info_table()
                  .getHeaderText()
                  .then(htext => {
                    expect(htext).toEqual(v_status[v_status_key]);
                  });
                break;
              case '数据':
                v_status[v_status_key].forEach(line => {
                  this.status_info_table()
                    .hasRow(line)
                    .then(has_row => {
                      expect(has_row).toBe(true);
                    });
                });
                break;
            }
          }

          break;
        case '容器':
          browser.sleep(1).then(() => {
            const v_containers_data = expectValue[key] as Record<
              string,
              Record<string, any>
            >;
            for (const container_name in v_containers_data) {
              const v_container_data = v_containers_data[container_name];
              this.containers.clickTab(container_name).then(() => {
                for (const v_container_key in v_container_data) {
                  switch (v_container_key) {
                    case '已挂载存储卷':
                    case '端口':
                      const v_container_data_1 = v_container_data[
                        v_container_key
                      ] as Record<string, any>;
                      for (const v_container_1_key in v_container_data_1) {
                        const table = this.containers.getElementByText(
                          v_container_key,
                        ) as AuiTableComponent;
                        if (v_container_1_key === '表头') {
                          table.getHeaderText().then(texts => {
                            expect(texts).toEqual(
                              v_container_data_1[v_container_1_key],
                            );
                          });
                        } else if (v_container_1_key === '数据') {
                          v_container_data_1[v_container_1_key].forEach(row => {
                            table.hasRow(row).then(has => {
                              expect(has).toBe(true);
                            });
                          });
                        }
                      }
                      break;
                    case '存活性健康检查':
                    case '可用性健康检查':
                      const v_container_data_2 = v_container_data[
                        v_container_key
                      ] as Record<string, string>;
                      const helth_check = this.containers.getElementByText(
                        v_container_key,
                      ) as AlaudaElement;
                      for (const helth_check_key in v_container_data_2) {
                        helth_check
                          .getElementByText(
                            helth_check_key,
                            '.healthcheck-table-cell-content',
                          )
                          .getText()
                          .then(text => {
                            expect(text.trim()).toBe(
                              v_container_data_2[helth_check_key],
                            );
                          });
                      }
                      break;
                    case 'EXEC':
                    case '日志':
                      break;
                    case '启动命令':
                      const ele = this.containers.getElementByText(
                        v_container_key,
                      ) as ElementFinder;

                      ele.getText().then(text => {
                        expect(text.trim().replace(/\s/g, '')).toBe(
                          v_container_data[v_container_key] as string,
                        );
                      });
                      break;
                    default:
                      const df = this.containers.getElementByText(
                        v_container_key,
                      ) as ElementFinder;

                      df.getText().then(text => {
                        expect(text.trim().replace(/\s/g, '')).toBe(
                          v_container_data[v_container_key] as string,
                        );
                      });
                      break;
                  }
                }
              });
            }
          });

          break;
        case 'YAML':
          this.Yaml.then(strpodyaml => {
            this.jsonContain(
              expectValue[key],
              CommonMethod.parseYaml(strpodyaml),
              true,
            );
          });
          break;
        case '配置':
          this.clickTab('配置').then(() => {
            for (const container_name in expectValue[key]) {
              for (const k in expectValue[key][container_name]) {
                for (const vk in expectValue[key][container_name]) {
                  if (vk === '更新按钮') {
                    this.getConfigCard(k, container_name)
                      .$('.aui-card__header')
                      .getText()
                      .then(text => {
                        if (expectValue[key][container_name][vk] === '无') {
                          expect(text).not.toContain('更新');
                        }
                      });
                  } else if (vk === '表头') {
                    this.getConfigTable(k, container_name)
                      .getHeaderText()
                      .then(texts => {
                        expect(texts).toEqual(
                          expectValue[key][container_name][vk],
                        );
                      });
                  } else if (vk === '数据') {
                    expectValue[key][container_name][vk].forEach(row => {
                      this.getConfigTable(k, container_name)
                        .hasRow(row)
                        .then(has => {
                          expect(has).toBe(true);
                        });
                    });
                  }
                }
              }
            }
          });
          break;
        case '日志':
          this.log.then(log => {
            expectValue[key].forEach(text => {
              expect(log).toContain(text);
            });
          });
          break;
        case '事件':
          break;
        default:
          this.getElementByText(key)
            .getText()
            .then(text => {
              expect(text.trim()).toContain(expectValue[key]);
            });
          break;
      }
    }
  }
}
