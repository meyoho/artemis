import { PodListPage } from '@e2e/page_objects/acp/pods/pods.list.page';

export class PodListVerifyPage extends PodListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '面包屑':
          this.breadcrumb.getText().then(text => {
            expect(text.trim()).toContain(expectValue[key]);
          });
          break;
        case '提示信息':
          this.alertInfo.getText().then(text => {
            expect(text).toBe(expectValue[key]);
          });
          break;
        case '表头':
          this.podListTable.getHeaderText().then(texts => {
            expect(texts).toEqual(expectValue[key]);
          });
          break;
        case '存在Pod':
        case '不存在Pod':
          const v_pods_data = expectValue[key] as Array<string>;
          v_pods_data.forEach(podName => {
            if (key === '存在Pod') {
              this.searchPod(podName, 1);
            } else {
              this.searchPod(podName, 0);
            }
          });
          break;
        case '删除确认信息':
          this.confirmDialog.title.getText().then(text => {
            expect(text.trim()).toBe(expectValue[key]);
          });
          this.confirmDialog.clickCancel();
          break;
      }
    }
  }
}
