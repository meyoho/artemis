import { DetailServicePage } from '@e2e/page_objects/acp/service/detail.page';
import { CommonMethod } from '@e2e/utility/common.method';

export class DetailServicePageVerify extends DetailServicePage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '名称':
          this.name.then(name => {
            expect(name.trim()).toContain(expectValue[key]);
          });
          break;
        case '来源':
        case '会话保持':
        case '外网访问':
          expect(this.getElementByText(key).getText()).toBe(expectValue[key]);
          break;
        case '标签':
        case '注解':
          this.getElementByText(key)
            .getText()
            .then(text => {
              const list = text.split('\n');
              expectValue[key].forEach(item => {
                expect(list).toContain(item);
              });
            });
          break;
        case '不包含标签':
          this.getElementByText('标签')
            .getText()
            .then(text => {
              const list = text.split('\n');
              expectValue[key].forEach(item => {
                expect(list).not.toContain(item);
              });
            });
          break;
        case '不包含注解':
          this.getElementByText('注解')
            .getText()
            .then(text => {
              const list = text.split('\n');
              expectValue[key].forEach(item => {
                expect(list).not.toContain(item);
              });
            });
          break;
        case '虚拟 IP-clusterip/nodeport':
          this.getElementByText('虚拟 IP')
            .getText()
            .then(text => {
              expect(text !== 'None').toEqual(expectValue[key]);
            });
          break;
        case '虚拟 IP-headless':
          this.getElementByText('虚拟 IP')
            .getText()
            .then(text => {
              expect(text === 'None').toEqual(expectValue[key]);
            });
          break;
        case '主机端口-clusterip/headless':
          this.ports.getColumeTextByName('主机端口').then(text => {
            expect(text.includes('-')).toEqual(expectValue[key]);
          });
          break;
        case '主机端口-nodeport':
          this.ports.getColumeTextByName('主机端口').then(text => {
            expect(!text.includes('-')).toEqual(expectValue[key]);
          });
          break;

        case '端口':
          this.ports.auiTable.getText().then(text => {
            const list = text.split('\n');
            expectValue[key].forEach(item => {
              expect(list).toContain(item);
            });
          });
          break;
        case '选择器':
          this.selectors.auiTable.getText().then(text => {
            const list = text.split('\n');
            expectValue[key].forEach(item => {
              expect(list).toContain(item);
            });
          });
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '容器组':
          this.clickTab('容器组');
          this.podListTable.getColumeTextByName('名称').then(names => {
            console.log(`内部路由容器组${names}`);
            if (expectValue[key].length === 0) {
              expect(names.length).toBe(0);
            } else {
              expectValue[key].then(values => {
                console.log(`组件容器组${values}`);
                expect(String(values)).toContain(names);
              });
            }
          });
          break;
        case 'YAML':
          this.clickTab('YAML');
          this.alaudaCodeEdit.getYamlValue().then(yml => {
            const yamlContent = CommonMethod.parseYaml(yml);
            this.jsonContain(expectValue[key], yamlContent, true);
          });
          break;
      }
    }
  }
}
