import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { ServiceListPage } from '@e2e/page_objects/acp/service/list.page';
import { $ } from 'protractor';

export class ServiceListPageVerify extends ServiceListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '检索':
                        if (expectValue[key] === 0) {
                            this.waitElementPresent(
                                $('.empty-placeholder'),
                                '检索无结果控件没有出现'
                            );
                            expect(
                                $('.empty-placeholder').isPresent()
                            ).toBeTruthy();
                        } else {
                            const table = new AlaudaAuiTable(
                                this.serviceNameTable.auiTable.$('aui-table')
                            );
                            table.getRowCount().then(rowCount => {
                                expect(
                                    this.serviceNameTable.getRowCount()
                                ).toBe(rowCount + expectValue[key]);
                            });
                        }

                        break;
                    case '创建':
                        this.serviceNameTable
                            .getRow([expectValue[key]['名称']])
                            .getText()
                            .then(text => {
                                const dicExpect = expectValue[key];
                                for (const keyExpect in dicExpect) {
                                    if (dicExpect.hasOwnProperty(keyExpect)) {
                                        //找到名称所在的行，获得该行的文字，验证包含期望值
                                        expect(text).toContain(
                                            dicExpect[keyExpect]
                                        );
                                    }
                                }
                            });

                        break;
                    // 验证面包屑 信息
                    case '面包屑':
                        expect(this.breadcrumb.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
