import { PSPDetailPage } from '@e2e/page_objects/acp/psp/psp.detail.page';
import { ElementFinder } from 'protractor';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';

export class AuiSwitch {
  private;
}
export class PSPDetailVerifyPage extends PSPDetailPage {
  verify(data: Record<string, any>) {
    for (const section in data) {
      switch (section) {
        case '基本策略':
          for (const key in data[section]) {
            switch (key) {
              //   case '默认允许':
              //   case '默认禁止':
              //     const ele_1 = this.getElementByText(
              //       section,
              //       key,
              //     ) as ElementFinder;
              //     ele_1.getAttribute('class').then(attr => {
              //       expect(attr.includes('isChecked')).toBe(data[section][key]);
              //     });
              //     break;
              default:
                const ele_2 = this.getElementByText(
                  section,
                  key,
                ) as ElementFinder;
                ele_2.getAttribute('class').then(attr => {
                  expect(attr.includes('isChecked')).toBe(data[section][key]);
                });
                break;
            }
          }
          break;
        case '主机路径策略（allowedHostPaths）':
        case '主机端口策略（hostPorts）':
        case '用户运行策略（runAsUser）':
          for (const key in data[section]) {
            switch (key) {
              case '范围':
                this.getElementByText(section, key)
                  .getText()
                  .then(text => {
                    expect(text.trim()).toBe(data[section][key]);
                  });
                break;
              case '主机路径范围':
              case '主机端口范围':
              case '用户 UID 范围':
                const ele_4 = this.getElementByText(
                  section,
                  key,
                ) as AlaudaAuiTable;
                ele_4.getText().then(text => {
                  const data_4 = data[section][key] as Array<string>;
                  data_4.forEach(v => {
                    expect(text).toContain(v);
                  });
                });
                break;
            }
          }
          break;
        case '帮助信息':
          this.HelpInfo.then(text => {
            expect(text.trim()).toBe(data[section]);
          });
          break;
        case '删除提示':
          this.deletePSP('不删除').then(() => {
            this.confirmDialog.title.getText().then(text => {
              expect(text.trim()).toBe(data[section]);
            });
          });
          this.confirmDialog.clickCancel();
          break;
      }
    }
  }
}
