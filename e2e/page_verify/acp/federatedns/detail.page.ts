import { DetailPage } from '@e2e/page_objects/platform/federatedns/detail.page';

export class DetailVerify extends DetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '名称':
          expect(this.name.getText()).toContain(expectValue[key]);
          break;
        case '显示名称':
        case '所属联邦集群':
          expect(this.getElementByText(key).getText()).toContain(
            expectValue[key],
          );
          break;
        case '资源配额':
          expectValue[key].forEach(data => {
            for (const indicator in data) {
              if (indicator !== '集群') {
                expect(this.getQuotaValue(indicator)).toContain(
                  data[indicator],
                );
              } else {
                this.changeRegion(data[indicator]);
              }
            }
          });
          break;
        case '容器限额':
          expectValue[key].forEach(data => {
            for (const indicator in data) {
              if (indicator !== '集群') {
                for (const name in data[indicator]) {
                  expect(this.getLimitRangeValue(indicator, name)).toBe(
                    data[indicator][name],
                  );
                }
              } else {
                this.changeRegion(data[indicator]);
              }
            }
          });
          break;
      }
    }
  }
}
