import { DeploymentDetailVerify } from '../deployment/detail.page';
import { $$, $, browser, ElementFinder } from 'protractor';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';

export class TappDetailVerify extends DeploymentDetailVerify {
  sverify(expectValue: {
    [key: string]:
      | string
      | string[]
      | number
      | { [key: string]: string | number };
  }) {
    for (const key in expectValue) {
      switch (key) {
        case '更新策略':
        case '镜像':
          const ele_up_image = this.getElementByText(key) as ElementFinder;
          ele_up_image.getText().then(text => {
            expect(text).toBe(expectValue[key] as string);
          });
          break;
        case '启动命令':
        case '参数':
          const arg_ele = this.getElementByText(key) as ElementFinder;
          arg_ele
            .$('.rc-text-list__trigger')
            .isPresent()
            .then(isPresent => {
              if (isPresent) {
                arg_ele.$('.rc-text-list__trigger').click();
                $$('aui-tooltip .rc-text-list__item')
                  .getText()
                  .then(text => {
                    (expectValue[key] as string[]).forEach(value => {
                      expect(text).toContain(value);
                    });
                  });
              } else {
                (expectValue[key] as string[]).forEach(value => {
                  expect(arg_ele.getText()).toContain(value);
                });
              }
            });
          break;
        case '状态':
          this.waitStatus(expectValue[key]);
          const ele_s = this.getElementByText(key) as ElementFinder;
          ele_s.getText().then(text => {
            expect(text.replace(/\s/g, '')).toContain(expectValue[
              key
            ] as string);
          });
          break;
        case '实例数':
          const ele_rp = this.getElementByText(key) as ElementFinder;
          ele_rp.getText().then(text => {
            expect(text.replace(/\s/g, '')).toContain(expectValue[
              key
            ] as string);
          });
          break;
        case '容器组标签':
        case '主机选择器':
          const ele_label_ns = this.getElementByText(key) as AlaudaTagsLabel;
          ele_label_ns.getText().then(text => {
            const arr_text = Array.from(text);
            if ((expectValue[key] as string) === '-') {
              expect(arr_text).toEqual([]);
            } else {
              (expectValue[key] as string).split(';').forEach(value => {
                expect(
                  arr_text.map(v => {
                    return String(v).replace(/\s/g, '');
                  }),
                ).toContain(value);
              });
            }
          });
          break;
        case '资源限制':
          const ele_r = this.getElementByText(key) as ElementFinder;
          ele_r.getText().then(text => {
            expect(text.replace(/\s/g, '')).toBe(expectValue[key] as string);
          });
          break;
        case '环境变量':
        case '日志文件':
        case '排除日志文件':
          this.alaudaEnvTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '无配置引用':
          this.alaudaConfigTable.getRowCount().then(count => {
            expect(count).toBe(0);
          });
          break;
        case '配置引用':
          this.alaudaConfigTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '可用性健康检查':
          this.healthCheck.expand();
          this.healthCheckElement('可用性').scrollToView(
            this.healthCheckElement('可用性').root,
          );
          for (const k in expectValue[key] as {
            [key: string]: string | number;
          }) {
            this.healthCheckElement('可用性')
              .getElementByText(k, '.healthcheck-table-cell-content')
              .getText()
              .then(text => {
                expect(text).toContain(expectValue[key][k]);
              });
          }
          break;
        case '存活性健康检查':
          this.healthCheck.expand();
          this.healthCheckElement('存活性').scrollToView(
            this.healthCheckElement('存活性').root,
          );
          for (const k in expectValue[key] as {
            [key: string]: string | number;
          }) {
            this.healthCheckElement('存活性')
              .getElementByText(k, '.healthcheck-table-cell-content')
              .getText()
              .then(text => {
                expect(text).toContain(expectValue[key][k]);
              });
          }
          break;
        case 'Pod 亲和':
          const ele_affi = this.getElementByText(key) as ElementFinder;
          ele_affi.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '日志':
          this.clickTab('日志');
          browser.sleep(1000);
          $('rc-pods-log .view-line span span')
            .getText()
            .then(text => {
              expect(text).toContain(expectValue[key] as string);
            });
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '自动扩缩容':
          this.getElementByText(key)
            .getText()
            .then(text => {
              expect(text.replace(/\s/g, '')).toContain(expectValue[
                key
              ] as string);
            });
          break;
      }
    }
  }
}
