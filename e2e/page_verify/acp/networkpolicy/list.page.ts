import { PolicyListPage } from '@e2e/page_objects/acp/networkpolicy/list.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { $ } from 'protractor';
import { AuiIcon } from '@e2e/element_objects/alauda.aui_icon';

export class PolicyListPageVerify extends PolicyListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '数量':
          const value = expectValue[key] as number;
          if (value === 0) {
            expect(this.noData.isPresent()).toBeTruthy();
          } else {
            this.policyNameTable.getRowCount().then(count => {
              expect(count).toBe(value);
            });
            // });
          }
          break;
        case 'YAML':
          this.waitElementPresent(
            $('ng-monaco-editor>.ng-monaco-editor-container'),
            '等待yaml内容加载失败',
            10000,
          ).then(ispresent => {
            if (ispresent) {
              new AlaudaYamlEditor().getYamlValue().then(yaml_text => {
                const yaml_obj = CommonMethod.parseYaml(yaml_text);
                this.jsonContain(expectValue[key], yaml_obj, true);
              });
              new AuiIcon($('.aui-dialog__header-close aui-icon')).click();
            } else {
              throw new Error('Yaml 内容加载失败');
            }
          });
          break;
      }
    }
  }
}
