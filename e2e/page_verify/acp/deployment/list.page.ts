import { DeploymentListPage } from '@e2e/page_objects/acp/deployment/list.page';

export class DeploymentListVerify extends DeploymentListPage {
  verify(expectValue: {
    [key: string]:
      | string
      | string[]
      | number
      | { [key: string]: string | number };
  }) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          this.breadcrumb.getText().then(text => {
            expect(text).toEqual(expectValue[key] as string);
          });
          break;
        case 'Header':
          this.WorkloadTable.getHeaderText().then(text => {
            expect(text).toEqual(expectValue[key] as string);
          });
          break;
        case '数量':
          this.refreshList();
          this.WorkloadTable.getRowCount().then(count => {
            expect(count).toBe(expectValue[key] as number);
          });
          break;
        case '创建':
          this.WorkloadTable.getRow([expectValue[key]['名称']])
            .getText()
            .then(text => {
              const dicExpect = expectValue[key] as { [key: string]: string };
              for (const keyExpect in dicExpect) {
                //找到名称所在的行，获得该行的文字，验证包含期望值
                expect(text.replace(/\s/g, '')).toContain(dicExpect[keyExpect]);
              }
            });
          break;
        case '空表格文字':
          this.tableFooterText.then(text => {
            expect(text.trim()).toBe(expectValue[key] as string);
          });
          break;
        case '不存在行':
          const nline_data = expectValue[key] as Array<string>;
          nline_data.forEach(l => {
            this.WorkloadTable.hasRow([l]).then(has => {
              expect(has).toBe(false);
            });
          });
          break;
        case '存在行':
          const iline_data = expectValue[key] as Array<string>;
          iline_data.forEach(l => {
            this.WorkloadTable.hasRow([l]).then(has => {
              expect(has).toBe(true);
            });
          });
          break;
      }
    }
  }
}
