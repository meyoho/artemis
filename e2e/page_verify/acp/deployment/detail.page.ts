import { DetailDeploymentPage } from '@e2e/page_objects/acp/deployment/detail.page';
import { $$, $, browser, ElementFinder } from 'protractor';
import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { AlaudaYamlEditor } from '@e2e/element_objects/alauda.yamleditor';
import { CommonMethod } from '@e2e/utility/common.method';
import { AuiSelect } from '@e2e/element_objects/alauda.auiSelect';
import { PodDetailPage } from '@e2e/page_objects/acp/pods/pods.detail.page';

export class DeploymentDetailVerify extends DetailDeploymentPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '容器':
          expectValue[key].forEach(data => {
            this.clickTab(data['容器名称']);
            this.verify(data);
          });
          break;
        case '节点异常策略':
        case '更新策略':
        case '镜像':
          const ele_up_image = this.getElementByText(key) as ElementFinder;
          ele_up_image.getText().then(text => {
            expect(text).toBe(expectValue[key] as string);
          });
          break;
        case '启动命令':
        case '参数':
          const arg_ele = this.getElementByText(key) as ElementFinder;
          arg_ele
            .$('.rc-text-list__trigger')
            .isPresent()
            .then(isPresent => {
              if (isPresent) {
                arg_ele.$('.rc-text-list__trigger').click();
                $$('aui-tooltip .rc-text-list__item')
                  .getText()
                  .then(text => {
                    (expectValue[key] as string[]).forEach(value => {
                      expect(text).toContain(value);
                    });
                  });
              } else {
                (expectValue[key] as string[]).forEach(value => {
                  expect(arg_ele.getText()).toContain(value);
                });
              }
            });
          break;
        case '状态':
          this.waitStatus(expectValue[key]);
          const ele_s = this.getElementByText(key) as ElementFinder;
          ele_s.getText().then(text => {
            expect(text.replace(/\s/g, '')).toContain(expectValue[
              key
            ] as string);
          });
          break;
        case '实例数':
          const ele_rp = this.getElementByText(key) as ElementFinder;
          ele_rp.getText().then(text => {
            expect(text.replace(/\s/g, '')).toContain(expectValue[
              key
            ] as string);
          });
          break;
        case '标签':
        case '主机选择器':
        case '凭据':
          const ele_label_ns = this.getElementByText(key) as AlaudaTagsLabel;
          ele_label_ns.getText().then(text => {
            const arr_text = Array.from(text);
            if ((expectValue[key] as string) === '-') {
              expect(arr_text).toEqual([]);
            } else {
              (expectValue[key] as string).split(';').forEach(value => {
                expect(
                  arr_text.map(v => {
                    return String(v).replace(/\s/g, '');
                  }),
                ).toContain(value);
              });
            }
          });
          break;
        case '资源限制':
          const ele_r = this.getElementByText(key) as ElementFinder;
          ele_r.getText().then(text => {
            expect(text.replace(/\s/g, '')).toBe(
              expectValue[key].replace('CPU:', '').replace('内存:', ''),
            );
          });
          break;
        case '环境变量':
          this.alaudaEnvTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '无配置引用':
          this.alaudaConfigTable.getRowCount().then(count => {
            expect(count).toBe(0);
          });
          break;
        case '配置引用':
          this.alaudaConfigTable.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '可用性健康检查':
          this.healthCheck.expand();
          this.healthCheckElement('可用性').scrollToView(
            this.healthCheckElement('可用性').root,
          );
          for (const k in expectValue[key] as {
            [key: string]: string | number;
          }) {
            this.healthCheckElement('可用性')
              .getElementByText(k, '.healthcheck-table-cell-content')
              .getText()
              .then(text => {
                expect(text).toContain(expectValue[key][k]);
              });
          }
          break;
        case '存活性健康检查':
          this.healthCheck.expand();
          this.healthCheckElement('存活性').scrollToView(
            this.healthCheckElement('存活性').root,
          );
          for (const k in expectValue[key] as {
            [key: string]: string | number;
          }) {
            this.healthCheckElement('存活性')
              .getElementByText(k, '.healthcheck-table-cell-content')
              .getText()
              .then(text => {
                expect(text).toContain(expectValue[key][k]);
              });
          }
          break;
        case '存储卷':
        case '已挂载存储卷':
        case 'Pod 亲和':
        case '端口':
        case '日志文件':
          const ele_affi = this.getElementByText(key) as ElementFinder;
          ele_affi.getText().then(text => {
            (expectValue[key] as string[]).forEach(value => {
              expect(text).toContain(value);
            });
          });
          break;
        case '验证容器日志':
          this.clickTab('日志');
          expectValue[key].forEach(data => {
            new AuiSelect(
              $('.log-source__toolbar aui-select:nth-child(4)'),
            ).select(data['容器']);
            browser.sleep(1000);
            $('rc-pods-log .view-lines')
              .getText()
              .then(text => {
                data['日志'].forEach(log => {
                  expect(text).toContain(log);
                });
              });
          });
          break;
        case '日志':
          this.clickTab('日志');
          browser.sleep(1000);
          $('rc-pods-log .view-lines')
            .getText()
            .then(text => {
              expect(text).toContain(expectValue[key] as string);
            });
          break;
        case '日志容器组':
          this.clickTab('日志');
          this.waitElementPresent(
            $('.log-source__toolbar aui-select:nth-child(4)'),
            '等待日志容器组下拉列表框出现失败',
          );
          $('.log-source__toolbar aui-select:nth-child(2)')
            .getText()
            .then(text => {
              expect(text.trim()).toBe(expectValue[key] as string);
            });
          break;
        case '日志容器':
          this.clickTab('日志');
          this.waitElementPresent(
            $('.log-source__toolbar aui-select:nth-child(4)'),
            '等待日志容器下拉列表框出现失败',
          );
          $('.log-source__toolbar aui-select:nth-child(4)')
            .getText()
            .then(text => {
              expect(text.trim()).toBe(expectValue[key] as string);
            });
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '自动扩缩容':
          this.getElementByText(key)
            .getText()
            .then(text => {
              expect(text.replace(/\s/g, '')).toContain(expectValue[
                key
              ] as string);
            });
          break;
        case 'YAML':
          this.clickTab('YAML');
          this.waitElementPresent(
            $('ng-monaco-editor>.ng-monaco-editor-container'),
            '等待yaml内容加载失败',
            10000,
          ).then(ispresent => {
            if (ispresent) {
              new AlaudaYamlEditor().getYamlValue().then(yaml_text => {
                const yaml_obj = CommonMethod.parseYaml(yaml_text);
                this.jsonContain(expectValue[key], yaml_obj, true);
              });
            } else {
              throw new Error('Yaml 内容加载失败');
            }
          });
          break;
        case '容器组列表':
          this.clickTab('容器组');
          const pod_list_v_data = expectValue[key] as Record<
            string,
            | Array<string>
            | number
            | string
            | Record<string, Array<string> | string>
          >;
          for (const pk in pod_list_v_data) {
            switch (pk) {
              case '表头':
                this.podListTable.getHeaderText().then(header_text => {
                  expect(header_text).toEqual(pod_list_v_data[pk] as Array<
                    string
                  >);
                });
                break;
              case '操作项':
                browser.sleep(1).then(() => {
                  const ops = pod_list_v_data[pk] as Record<
                    string,
                    Array<string> | string
                  >;
                  const ops_vdata = pod_list_v_data[pk]['verify_data'] as Array<
                    string
                  >;
                  ops_vdata.forEach(data => {
                    this.podDropDownList(ops['podName'] as string)
                      .checkDataExistInDropdown(data)
                      .then(isin => {
                        expect(isin).toBe(true);
                      });
                  });
                });
                break;
              case 'YAML':
                browser.sleep(1).then(() => {
                  const pods = pod_list_v_data[pk] as Record<string, any>;
                  for (const podname in pods) {
                    this.getPodYaml(podname).then(strpodyaml => {
                      this.jsonContain(
                        pods[podname],
                        CommonMethod.parseYaml(strpodyaml),
                        true,
                      );
                    });
                  }
                });
                new PodDetailPage().clickTab('详细信息').then(() => {
                  new PodDetailPage().toWorkloadDetailPage();
                });
                break;
              case '存在':
                this.podListTable
                  .hasRow([pod_list_v_data[pk] as string])
                  .then(has => {
                    expect(has).toBe(true);
                  });
                break;
              case '不存在':
                this.podListTable
                  .hasRow([pod_list_v_data[pk] as string])
                  .then(has => {
                    expect(has).toBe(false);
                  });
                break;
              case '数量':
                this.podListTable.getRowCount().then(rc => {
                  expect(rc).toBe(pod_list_v_data[pk] as number);
                });
                break;
            }
          }
          break;
      }
    }
  }
}
