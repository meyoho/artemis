import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AppDetailPage } from '@e2e/page_objects/acp/appcore/app.detail.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';
import { $ } from 'protractor';

export class AppDetailVerify extends AppDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '名称':
          expect(this.name.getText()).toContain(expectValue[key]);
          break;
        case '应用状态':
          this.appStatus(expectValue[key]['app名称'], expectValue[key]['状态'])
            .getText()
            .then(text => {
              expect(text).toContain(expectValue[key]['状态']);
            });
          break;
        case '联邦化标记':
          $('.basic-federation_cluster')
            .isPresent()
            .then(isPresent => {
              expect(isPresent).toBe(expectValue[key]);
            });
          break;
        case '按钮不可点击':
          expectValue[key].forEach(k => {
            expect(
              this.getBtn(expectValue['名称'], k).getAttribute('disabled'),
            ).toBeTruthy();
          });
          break;
        case 'StatefulSet':
        case 'Deployment':
        case 'DaemonSet':
          const deploy = new AlaudaElement(
            'rc-field-set-item',
            '.field-set-item__label>label',
            this.getCardByHeader(key),
          );
          const d = expectValue[key];
          for (const k in d) {
            switch (k) {
              case '名称':
                const ret = this.getCardByHeader(key)
                  .$$('.resource__header__front')
                  .filter(ele => {
                    return ele.getText().then(text => {
                      if (text.includes(d[k])) {
                        return true;
                      }
                    });
                  });
                ret.count().then(count => {
                  expect(count).toBe(1);
                });

                break;
              case '状态':
                this.getResourceStatus(key, d[k]['pod名称'])
                  .getText()
                  .then(text => {
                    expect(text).toContain(d[k]['pod状态']);
                  });
                break;
              case '容器数量':
                this.getCardByHeader(key)
                  .$$('.resource__content')
                  .count()
                  .then(ct => {
                    expect(ct).toBe(d[k]);
                  });
                break;

              case '容器信息':
                const c_verifys = d[k] as Array<Record<string, any>>;
                const pod_info = this.getCardByHeader(key);
                c_verifys.forEach(c_verify => {
                  const container = new AlaudaElement(
                    'rc-field-set-item',
                    '.field-set-item__label>label',
                    pod_info
                      .$$('.resource__content')
                      .filter(ele => {
                        return new AlaudaElement(
                          'rc-field-set-item',
                          '.field-set-item__label>label',
                          ele,
                        )
                          .getElementByText('容器', '.field-set-item__value')
                          .getText()
                          .then(c_name => {
                            return c_name.trim() === c_verify['容器'];
                          });
                      })
                      .first(),
                  );
                  for (const container_verify_key in c_verify) {
                    container
                      .getElementByText(
                        container_verify_key,
                        '.field-set-item__value',
                      )
                      .getText()
                      .then(txt => {
                        expect(c_verify[container_verify_key]).toBe(
                          txt.trim().replace(/\n/g, ''),
                        );
                      });
                  }
                });
                break;
              case '实例数量':
                this.getResourceWorkload(key, d[k]['pod名称'])
                  .getText()
                  .then(text => {
                    console.log(text);
                    expect(text).toContain(d[k]['pod数量']);
                  });
                break;
              case '资源限制':
                expect(
                  deploy
                    .getElementByText(k, '.field-set-item__value')
                    .getText()
                    .then(text => {
                      return text.replace(/\n/g, '');
                    }),
                ).toBe(d[k].replace('CPU: ', '').replace('内存: ', ''));
                break;
              default:
                expect(
                  deploy
                    .getElementByText(k, '.field-set-item__value')
                    .getText()
                    .then(text => {
                      return text.replace(/\n/g, '');
                    }),
                ).toBe(d[k]);
            }
          }
          break;
        case '容器组标签':
          this.showLabels().then(text => {
            expect(text).toContain(expectValue[key]['标签']);
          });
          break;
        case '其他资源':
          expectValue[key].forEach(rs => {
            this.otherResourceTable.hasRow(rs).then(hasRow => {
              expect(hasRow).toBe(true);
            });
          });
          break;
        case '其他资源不存在':
          expectValue[key].forEach(rs => {
            this.otherResourceTable.hasRow(rs).then(hasRow => {
              expect(hasRow).toBe(false);
            });
          });
          break;
        case '历史版本':
          new AlaudaButton(this.getTab('历史版本')).click().then(() => {
            for (const k in expectValue[key]) {
              switch (k) {
                case '数量':
                  this.historyListTable.getRowCount().then(row_count => {
                    expect(row_count).toBe(expectValue[key][k]);
                  });
                  break;
                case '注释':
                  this.historyListTable
                    .hasRow([expectValue[key][k]])
                    .then(hasRow => {
                      expect(hasRow).toBe(true);
                    });
                  break;
                case '回滚注释':
                  this.historyListTable
                    .getRowCell(0, 3)
                    .getText()
                    .then(text => {
                      expect(text.trim()).toContain(expectValue[key][k]);
                    });
                  break;
                case '表头':
                  this.historyListTable.getHeaderText().then(text => {
                    expect(text).toEqual(expectValue[key][k]);
                  });
                  break;
                case '变更内容':
                  new AlaudaButton(
                    this.historyListTable
                      .getRowByIndex(0)
                      .$('aui-table-cell>aui-icon'),
                  )
                    .click()
                    .then(() => {
                      this.historyListTable
                        .getRowByIndex(0)
                        .$('aui-table-cell>a')
                        .getText()
                        .then(text => {
                          console.log(text);
                          const c_version = Number.parseInt(
                            text
                              .trim()
                              .split(/\(|\)/i)[1]
                              .split(/\./i)[1],
                          );
                          const verify_infos = expectValue[key][k] as Array<
                            string
                          >;
                          verify_infos.forEach(value => {
                            this.historyListTable
                              .getRowByIndex(0)
                              .$('aui-table-cell:last-child')
                              .getText()
                              .then(text => {
                                expect(text).toContain(
                                  value
                                    .replace('{new}', `v.${c_version}`)
                                    .replace('{old}', `v.${c_version - 1}`),
                                );
                              });
                          });
                        });
                    });
                  break;
              }
            }
          });
          break;
        case 'YAML':
          this.Yaml.then(yaml => {
            yaml.getYamlValue().then(yaml_text => {
              String(yaml_text)
                .split('---\n')
                .forEach((yml_string, index) => {
                  const yaml_obj = CommonMethod.parseYaml(yml_string);
                  this.jsonContain(expectValue[key][index], yaml_obj, true);
                });
            });
          });
          break;
        case 'YAML内容不包含':
          this.Yaml.then(yaml => {
            yaml.getYamlValue().then(yaml_text => {
              const yaml_conent = String(yaml_text);
              const verify_values = expectValue[key] as Array<string>;
              verify_values.forEach(verify_value => {
                expect(yaml_conent).not.toContain(verify_value);
              });
            });
          });
          break;
        default:
          expect(
            this.getElementByText(key, '.field-set-item__value')
              .getText()
              .then(text => {
                return text.replace(/\n/g, '');
              }),
          ).toBe(expectValue[key]);
          break;
      }
    }
  }
}
