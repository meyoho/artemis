import { AppListPage } from '@e2e/page_objects/acp/appcore/app.list.page';

export class AppListVerify extends AppListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toEqual(expectValue[key]);
          break;
        case 'Header':
          expect(this.appTable.getHeaderText()).toEqual(expectValue[key]);
          break;
        case '数量':
          expect(this.appTable.getRowCount()).toBe(expectValue[key]);
          break;
        case '选择镜像弹窗存在':
          this.image_selector.isPresent.then(isPresent => {
            expect(isPresent).toBe(expectValue[key]);
          });
          break;
      }
    }
  }
}
