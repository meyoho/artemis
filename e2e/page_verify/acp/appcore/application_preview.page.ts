import { PreviewPage } from '@e2e/page_objects/acp/appcore/app.preview.page';
import { isString } from 'util';
import { browser } from 'protractor';

export class AppPreviewVerify extends PreviewPage {
  verify(expectValue) {
    return browser.sleep(1).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '标题':
            expect(this.breadcrumb.getText()).toContain(expectValue[key]);
            break;
          case '名称':
            this.getElementByText(key, '.aui-form-item__content')
              .getText()
              .then(text => {
                expect(text).toContain(expectValue[key]);
              });
            break;
          case '资源':
            browser.sleep(1).then(() => {
              const rss = expectValue[key] as Array<
                Record<string, string | Record<string, any>>
              >;
              rss.forEach(rs => {
                const rscard = this.get_resource_card(
                  rs['资源类型'] as string,
                  rs['名称'] as string,
                );
                for (const rk in rs) {
                  if (isString(rs[rk])) {
                    rscard.get_value_by_key(rk).then(text => {
                      expect(text.trim()).toBe(rs[rk] as string);
                    });
                  } else if (typeof rs[rk] === 'object') {
                    rscard.get_table(rk).then(rsTable => {
                      for (const tk in rs[rk] as Record<string, any>) {
                        switch (tk) {
                          case '表头':
                            rsTable.getHeaderText().then(text => {
                              expect(text).toEqual(rs[rk][tk]);
                            });
                            break;
                          default:
                            const table_lines = rs[rk][tk] as Array<
                              Array<string>
                            >;
                            table_lines.forEach(line => {
                              rsTable.hasRow(line).then(has => {
                                expect(has).toBe(true);
                              });
                            });
                            break;
                        }
                      }
                    });
                  }
                }
              });
            });
            break;
          case '不存在资源':
            browser.sleep(1).then(() => {
              const nrss = expectValue[key] as Array<
                Record<string, string | Record<string, any>>
              >;
              nrss.forEach(rs => {
                this.get_resource_card(
                  rs['资源类型'] as string,
                  rs['名称'] as string,
                ).isPresent.then(ispresent => {
                  expect(ispresent).toBe(false);
                });
              });
            });
            break;
          case 'YAML':
            break;
          default:
            this.getElementByText(key)
              .getText()
              .then(text => {
                expect(text.replace(/\n/g, '')).toBe(expectValue[key]);
              });
            break;
        }
      }
    });
  }
}
