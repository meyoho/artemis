import { AppCreatePage } from '@e2e/page_objects/acp/appcore/app.create.page';
import { isString } from 'util';
import { browser } from 'protractor';

export class AppCreateVerifyPage extends AppCreatePage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '应用':
          break;
        case '计算组件':
          const expectComputeValue = expectValue[key] as Record<any, any>;
          for (const compute_key in expectComputeValue) {
            switch (compute_key) {
              case '名称':
                break;
              case '实例数量':
                break;
              case '部署模式':
                break;
              case '组件 - 高级':
                break;
              case '容器组':
                const expectContainerValue = expectComputeValue[
                  compute_key
                ] as Record<any, any>;
                for (const container_key in expectContainerValue) {
                  switch (container_key) {
                    case '名称':
                      break;
                    case '镜像地址':
                      break;
                    case '资源限制':
                      break;
                    case '启动命令':
                      break;
                    case '参数':
                      break;
                    case '日志文件':
                      break;
                    case '排除日志文件':
                      break;
                    case '端口':
                      break;
                    case '健康检查':
                      break;
                    case '存储卷挂载':
                      break;
                    case '环境变量':
                      break;
                    case '配置引用':
                      break;
                    case '存储卷':
                      break;
                    case '容器组标签':
                      break;
                    case '主机选择器':
                      break;
                    case '亲和性':
                      break;
                    case 'Host 模式':
                      break;
                    case '凭据':
                      break;
                    case '固定 IP':
                      break;
                    case 'galaxy':
                      break;
                    case '':
                      break;
                  }
                }
            }
          }
          break;
        case '访问规则':
        case '内部路由':
          browser.sleep(1).then(() => {
            const service_ingress_verify = expectValue[key] as Record<
              string,
              any
            >;
            const service_ingress_card = this.get_resource_card(
              key,
              service_ingress_verify['名称'],
            );
            service_ingress_card.isPresent.then(ispresent => {
              if (ispresent) {
                for (const k in service_ingress_verify) {
                  if (isString(service_ingress_verify[k])) {
                    service_ingress_card.get_value_by_key(k).then(text => {
                      expect(text).toBe(service_ingress_verify[k]);
                    });
                  } else {
                    const table_verifys = service_ingress_verify[k] as Record<
                      string,
                      Array<Array<string> | string>
                    >;
                    for (const table_verify in table_verifys) {
                      if (table_verify === '表头') {
                        service_ingress_card.get_table(k).then(table => {
                          table.getHeaderText().then(header_text => {
                            expect(header_text).toEqual(table_verifys[
                              table_verify
                            ] as Array<string>);
                          });
                        });
                      } else {
                        const table_rows = table_verifys[table_verify] as Array<
                          Array<string>
                        >;
                        table_rows.forEach(table_row => {
                          service_ingress_card.get_table(k).then(table => {
                            table.hasRow(table_row).then(has_row => {
                              expect(has_row).toBe(true);
                            });
                          });
                        });
                      }
                    }
                  }
                }
              } else {
                expect(ispresent).toBe(true);
              }
            });
          });
          break;
        case '不存在访问规则':
        case '不存在内部路由':
          browser.sleep(1).then(() => {
            const n_service_ingress_verify = expectValue[key] as Record<
              string,
              any
            >;
            const n_service_ingress_card = this.get_resource_card(
              key.slice(3),
              n_service_ingress_verify['名称'],
            );
            n_service_ingress_card.isPresent.then(ispresent => {
              expect(ispresent).toBe(false);
            });
          });
          break;
        default:
          break;
      }
    }
  }
}
