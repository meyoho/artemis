/**
 * Created by zongyao on 2019/8/30.
 */
import { SecretDetailPage } from '@e2e/page_objects/devops/config-secret/secret_detail.page';

export class SecretDetailVerify extends SecretDetailPage {
    verify(expectData) {
        for (const key in expectData) {
            if (expectData.hasOwnProperty(key)) {
                switch (key) {
                    case '数据':
                        expectData[key].forEach(dataItem => {
                            expect(this.data.click(dataItem['键'])).toBe(
                                dataItem['值']
                            );
                        });
                        break;
                    default:
                        this.getElementByText(key)
                            .getText()
                            .then(text => {
                                console.log(`${key}:` + text);
                            });
                        expect(this.getElementByText(key).getText()).toBe(
                            expectData[key]
                        );
                }
            }
        }
    }
}
