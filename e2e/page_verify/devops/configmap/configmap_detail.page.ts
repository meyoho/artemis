/**
 * Created by zongyao on 2019/8/30.
 */
import { AlaudaLabel } from '@e2e/element_objects/alauda.label';
import { ConfigmapDetailPage } from '@e2e/page_objects/devops/configmap/configmap_detail.page';

export class ConfigmapDetailVerify extends ConfigmapDetailPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                    case '名称':
                        this.waitElementPresent(
                            this.name,
                            'configmap 详情页, 名称控件没出现'
                        );
                        expect(this.name.getText()).toContain(expectValue[key]);
                        break;
                    case '显示名称':
                        const label: AlaudaLabel = this.getElementByText(key);
                        expect(label.getText()).toBe(expectValue[key]);
                        break;
                    case '数据':
                        expectValue[key].forEach(dataItem => {
                            expect(this.data.click(dataItem['键'])).toBe(
                                dataItem['值']
                            );
                        });
                        break;
                }
            }
        }
    }
}
