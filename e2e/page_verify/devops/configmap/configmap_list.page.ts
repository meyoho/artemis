import { ConfigmapListPage } from '@e2e/page_objects/devops/configmap/configmap_list.page';

export class ConfigmapListVerify extends ConfigmapListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case 'Header':
                        expect(this.configmapTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.configmapTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
