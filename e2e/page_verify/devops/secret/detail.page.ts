/**
 * Created by weiliu on 2020/02/05.
 */
import { browser } from 'protractor';
import { SecretPage } from '@e2e/page_objects/devops/secret/secret.page';

export class SecretPageDetailVerify extends SecretPage {
  verify(expectValue) {
    browser.sleep(100).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '名称':
            expect(this.detailPage_Content.getTitleText()).toBe(
              expectValue[key],
            );
            break;
          case '显示名称':
            expect(
              this.detailPage_Content.getElementByText(key).getText(),
            ).toBe(expectValue[key]);
            break;
          case '使用域':
            expect(
              this.detailPage_Content.getElementByText(key).getText(),
            ).toBe(expectValue[key]);
            break;
          case '类型':
            expect(
              this.detailPage_Content.getElementByText(key).getText(),
            ).toBe(expectValue[key]);
            break;
        }
      }
    });
  }
}
