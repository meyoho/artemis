/**
 * Created by weiliu on 2020/02/05.
 */
import { browser } from 'protractor';
import { DevopsToolDetailPage } from '@e2e/page_objects/devops_new/devops-tools/detail.page';

export class DevopsToolDetailVerify extends DevopsToolDetailPage {
  verify(expectValue) {
    browser.sleep(100).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '类型':
            expect(this.getElementByText(key).getText()).toBe(
              `类型 ${expectValue[key]}`,
            );
            break;
          case '访问地址':
          case 'API 地址':
            expect(this.getElementByText(key).getText()).toBe(expectValue[key]);
            break;
          case '名称':
          case '项目名称':
          case '凭据':
            const rowElem = this.bindTable.getRow([
              expectValue['名称'],
              expectValue['项目名称'],
            ]);
            this.waitElementPresent(rowElem, '没有找到绑定的项目');

            rowElem.getText().then(text => {
              expect(text.includes(expectValue[key])).toBeTruthy();
            });
            break;
        }
      }
    });
  }

  verifyProjectBind(expectValue) {
    browser.sleep(100).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '集成名称':
          case '凭据':
          case '仓库类型':
          case '描述':
          case '认证方式':
            expect(this.getElementByText(key).getText()).toBe(expectValue[key]);
            break;
        }
      }
    });
  }

  verifyHarborBind(expectValue) {
    browser.sleep(100).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '集成名称：':
          case '凭据：':
          case '绑定时间：':
          case '描述：':
            expect(this.getElementByText_harbor(key).getText()).toBe(
              expectValue[key],
            );
            break;
          case '仓库类型：':
            expect(this.getElementByText_harbor(key).getText()).toBe(
              `仓库类型： ${expectValue[key]}`,
            );
            break;
        }
      }
    });
  }
}
