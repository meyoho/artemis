/**
 * Created by weiliu on 2020/02/05.
 */
import { PipelineDetailPage } from '@e2e/page_objects/devops_new/pipeline/detail.page';
import { browser } from 'protractor';

export class PipelineDetailVerify extends PipelineDetailPage {
  verify(expectValue) {
    browser.sleep(100).then(() => {
      for (const key in expectValue) {
        switch (key) {
          case '名称':
          case '显示名称':
          case '创建方式':
          case 'Jenkins':
            expect(this.getElementByText(key).getText()).toBe(expectValue[key]);
            break;
          case '执行方式':
            expect(this.getElementByText(key).getText()).toContain(
              expectValue[key],
            );
            break;
          case 'yamlvalue':
            (expectValue[key] as Array<string>).forEach(data => {
              this.codeEditor.getYamlValue().then(yamlValue => {
                expect(yamlValue).toContain(data);
              });
            });
            break;
        }
      }
    });
  }
}
