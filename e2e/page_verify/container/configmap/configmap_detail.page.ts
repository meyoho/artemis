import { ConfigmapDetailPage } from '@e2e/page_objects/acp/container/conifgmap/configmap_detail.page';
import { CommonMethod } from '@e2e/utility/common.method';

export class ConfigmapDetailVerify extends ConfigmapDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.title.getText()).toContain(expectValue[key]);
          break;
        case '名称':
          this.waitElementPresent(
            this.name,
            'configmap 详情页, 名称控件没出现',
          );
          expect(this.name.getText()).toContain(expectValue[key]);
          break;
        case '描述':
          expect(this.getElementByText(key).getText()).toBe(expectValue[key]);
          break;
        case '配置项':
          expectValue[key].forEach((dataItem: Record<string, any>) => {
            if ('值' in dataItem) {
              expect(this.data.get_value(dataItem['键'])).toBe(dataItem['值']);
            } else {
              this.data.hasKey(dataItem['键']).then(hasKey => {
                expect(hasKey).toBe(false);
              });
            }
          });
          break;
        case '计算组件数量':
          expect(this.workloads.getRowCount()).toBe(expectValue[key]);
          break;
        case '计算组件':
          expectValue[key].forEach(element => {
            expect(
              this.workloads.getCell(element[1], element[0]).then(ele => {
                return ele.getText();
              }),
            ).toBe(element[2]);
          });
          break;
        case 'JSON':
          this.yamlContent.then(yaml_content => {
            const yml = CommonMethod.parseYaml(yaml_content);
            this.jsonContain(expectValue[key], yml, true);
          });
      }
    }
  }
}
