import { JobDetailPage } from '@e2e/page_objects/acp/container/jobs/job_detail.page';

export class JobDetailVerify extends JobDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          this.breadcrumb.getText().then(text => {
            expect(text).toContain(expectValue[key]);
          });
          break;
        case '名称':
          this.name.getText().then(text => {
            expect(text).toContain(expectValue[key]);
          });
          break;
        case '任务状态':
          this.getElementByText(key)
            .getText()
            .then(text => {
              const status = text.replace(/\n/g, '');
              if (status === '未知状态') {
                console.error('定时任务执行异常，pod无法创建');
              } else {
                expect(status).toBe(expectValue[key]);
              }
            });
          break;
        case '日志':
          const log_v = expectValue[key] as Array<string>;
          log_v.forEach(v => {
            this.podsLogElement()
              .getText()
              .then(text => {
                expect(text).toContain(v);
              });
          });
          break;
        case '弹窗':
          for (const k in expectValue[key]) {
            if (k === '标题') {
              this.auiDialog.title.getText().then(text => {
                expect(text).toBe(expectValue[key][k]);
              });
            } else if (k === '内容') {
              this.auiDialog.content.getText().then(text => {
                expect(text).toBe(expectValue[key][k]);
              });
            }
          }
          break;
        default:
          this.getElementByText(key)
            .getText()
            .then(text => {
              expect(text.replace(/\n/g, '')).toBe(expectValue[key]);
            });
          break;
      }
    }
  }
}
