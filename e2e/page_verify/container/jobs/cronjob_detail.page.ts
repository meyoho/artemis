import { CronJobDetail } from '@e2e/page_objects/acp/container/jobs/cronjob_detail.page';
import { CommonMethod } from '@e2e/utility/common.method';

export class CronjobDetailVerify extends CronJobDetail {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '名称':
          expect(this.name.getText()).toContain(expectValue[key]);
          break;
        case 'YAML':
          this.clickTab('YAML');
          this.yamlContent.getYamlValue().then(yml => {
            const v_data: Array<Array<any>> = expectValue[key];
            const yamlContent = CommonMethod.parseYaml(yml);
            v_data.forEach(kd => {
              let v = yamlContent;
              kd[0].forEach(k => {
                v = v[k];
              });
              expect(v).toBe(kd[1]);
            });
          });
          break;
        case 'JSON':
          this.clickTab('YAML');
          this.yamlContent.getYamlValue().then(yml => {
            const yamlContent = CommonMethod.parseYaml(yml);
            this.jsonContain(expectValue[key], yamlContent, true);
          });
          break;
        case '弹窗':
          for (const k in expectValue[key]) {
            if (k === '标题') {
              this.auiDialog.title.getText().then(text => {
                expect(text).toBe(expectValue[key][k]);
              });
            } else if (k === '内容') {
              this.auiDialog.content.getText().then(text => {
                expect(text).toBe(expectValue[key][k]);
              });
            }
          }
          break;
        default:
          expect(
            this.getElementByText(key)
              .getText()
              .then(text => {
                return text.replace(/\n/g, '');
              }),
          ).toBe(expectValue[key]);
          break;
      }
    }
  }
}
