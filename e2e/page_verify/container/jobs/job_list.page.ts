import { JobList } from '@e2e/page_objects/acp/container/jobs/job_list.page';

export class JobListVerify extends JobList {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          this.breadcrumb.getText().then(text => {
            expect(text).toEqual(expectValue[key]);
          });
          break;
        case 'Header':
          this.jobtable.getHeaderText().then(text => {
            expect(text).toEqual(expectValue[key]);
          });
          break;
        case '数量':
          this.jobtable.getRowCount().then(rc => {
            expect(rc).toBe(expectValue[key]);
          });
          break;
        case '不存在':
          this.jobtable.hasRow([expectValue[key]]).then(has => {
            expect(has).toBe(false);
          });
      }
    }
  }
}
