import { CronjobList } from '@e2e/page_objects/acp/container/jobs/cronjob_list.page';

export class CronjobListVerify extends CronjobList {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          this.breadcrumb.getText().then(text => {
            expect(text).toEqual(expectValue[key]);
          });
          break;
        case 'Header':
          this.cronjobtable.getHeaderText().then(text => {
            expect(text).toEqual(expectValue[key]);
          });
          break;
        case '数量':
          this.cronjobtable.getRowCount().then(rc => {
            expect(rc).toBe(expectValue[key]);
          });
          break;
        case '存在':
          this.cronjobtable.hasRow([expectValue[key]]).then(has => {
            expect(has).toBe(true);
          });
      }
    }
  }
}
