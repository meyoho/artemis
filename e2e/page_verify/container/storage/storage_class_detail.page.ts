import { ScDetailPage } from '@e2e/page_objects/acp/container/storage/storage_class_detail.page';

export class ScCreatePageVerify extends ScDetailPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title).toContain(expectValue[key]);
                        break;
                    case '名称':
                        expect(this.name).toBe(expectValue[key]);
                        break;
                    case '类型':
                        expect(this.getDetail(key)).toBe(expectValue[key]);
                        break;
                }
            }
        }
    }
}
