import { PvDetailPage } from '@e2e/page_objects/acp/container/storage/pv_detail.page';

export class PvDetailVerify extends PvDetailPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                    case '名称':
                        expect(this.name.getText()).toContain(expectValue[key]);
                        break;
                    default:
                        expect(this.getElementByText(key).getText()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
