import { PVList } from '@e2e/page_objects/acp/container/storage/pv_list.page';

export class PvListVerify extends PVList {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '数量':
                        expect(this.pvTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
