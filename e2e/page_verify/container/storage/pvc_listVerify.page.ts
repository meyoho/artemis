import { PvcList } from '@e2e/page_objects/acp/container/storage/pvc_list.page';

export class PvcListVerify extends PvcList {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case 'Header':
                        expect(this.pvTable.getHeaderText()).toBe(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.pvTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
