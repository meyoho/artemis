import { SCList } from '@e2e/page_objects/acp/container/storage/storage_class_list.page';
import { ElementFinder } from 'protractor';

export class SCListVerify extends SCList {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case 'header':
                        expect(this.scTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '名称':
                        this.scTable
                            .getCell(key, expectValue[key])
                            .then(elem => {
                                expect(elem.getText()).toContain(
                                    expectValue[key]
                                );
                            });

                        break;
                    case '类型':
                        expect(
                            this.scTable.getRow(expectValue['名称']).getText()
                        ).toContain(expectValue[key]);
                        break;
                    case '默认':
                        const cell: ElementFinder = this.scTable.getRow(
                            expectValue['名称']
                        );
                        if (expectValue[key] === '非默认') {
                            expect(cell.getText()).not.toContain(
                                expectValue[key]
                            );
                        } else {
                            expect(cell.getText()).toContain(expectValue[key]);
                        }
                        break;
                    case '删除':
                        expect(this.scTable.getText()).not.toContain(
                            expectValue[key]
                        );
                }
            }
        }
    }
}
