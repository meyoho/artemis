import { PvcDetailPage } from '@e2e/page_objects/acp/container/storage/pvc_detail.page';
import { CommonMethod } from '@e2e/utility/common.method';
import { $ } from 'protractor';
export class PvcDetailVerify extends PvcDetailPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                    case '名称':
                        expect(this.name.getText()).toContain(expectValue[key]);
                        break;
                    case '容器组':
                        expect($('aui-card .empty-placeholder').getText()).toBe(
                            expectValue[key]
                        );
                        break;
                    case 'yaml':
                        this.yaml.getYamlValue().then(yaml => {
                            expect(
                                CommonMethod.parseYaml(yaml).spec.resources
                                    .requests.storage
                            ).toBe(expectValue[key]);
                        });
                        break;
                    default:
                        expect(this.getElementByText(key).getText()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
