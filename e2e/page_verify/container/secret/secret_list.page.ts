import { SecretListPage } from '@e2e/page_objects/acp/container/secret/secret_list.page';

export class SecretListVerify extends SecretListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case 'Header':
                        expect(this.secretTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.secretTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                }
            }
        }
    }
}
