import { SecretDetailPage } from '@e2e/page_objects/acp/container/secret/secret_detail.page';

export class SecretDetailVerify extends SecretDetailPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.title.getText()).toContain(
                            expectValue[key]
                        );
                        break;
                    case '名称':
                        expect(this.name.getText()).toContain(expectValue[key]);
                        break;
                    case '描述':
                        expect(this.getElementByText(key).getText()).toBe(
                            expectValue[key]
                        );
                        break;
                    case '类型':
                        expect(this.getElementByText(key).getText()).toBe(
                            expectValue[key]
                        );
                        break;
                    case '数据':
                        expectValue[key].forEach(dataItem => {
                            expect(this.data.get_value(dataItem['键'])).toBe(
                                dataItem['值']
                            );
                        });

                        break;
                }
            }
        }
    }
}
