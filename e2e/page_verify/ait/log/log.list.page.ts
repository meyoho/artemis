import { LogListPage } from '@e2e/page_objects/ait/log/loglist.page';

export class LogListPageVerify extends LogListPage {
  async verify(expectValue: { [key: string]: string[] | number }) {
    for (const key in expectValue) {
      switch (key) {
        // 验证jie 信息
        case '日志':
          expect(this.logResult.contain(expectValue[key])).toBeTruthy();
          break;
        case '时间范围':
          this.timeRange.auiIcon.click();
          const listItemText = await this.timeRange.auiToolTip.auiToolTip
            .$$('aui-option>div')
            .getText()
            .then(text => {
              return text;
            });
          (expectValue[key] as string[]).forEach(item => {
            expect(listItemText).toContain(item);
          });
          this.timeRange.auiIcon.click();
          break;
        case '柱状图':
          this.logEvent.highchartsSeries.count().then(count => {
            expect(count).toBe(31);
          });

          let isGreaterThan1 = false;
          await this.logEvent.highchartsSeries.each(series => {
            series.getAttribute('height').then(value => {
              isGreaterThan1 = isGreaterThan1 || Number(value) > 1;
            });
          });

          expect(isGreaterThan1).toBeTruthy();
          break;
        case '日志个数':
          this.logEvent.logCount.getText().then(text => {
            const count = Number(
              text
                .toLowerCase()
                .replace(' logs', '')
                .trim(),
            );
            expect(count).toBeGreaterThanOrEqual(1);
          });
          break;
      }
    }
  }
}
