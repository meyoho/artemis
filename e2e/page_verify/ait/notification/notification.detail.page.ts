import { NotificationDetailPage } from '@e2e/page_objects/ait/notification/notificationdetail.page';
export class NotificationDetailPageVerify extends NotificationDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      // console.log('key::: ' + key);
      switch (key) {
        case '基本信息':
        case '服务器配置':
          for (const itemKey in expectValue[key]) {
            const itemInfo = expectValue[key][itemKey];
            switch (itemKey) {
              case '创建时间':
              case '更新时间':
                expect(this.getElementByText(itemKey).getText()).not.toBe(
                  itemInfo,
                );
                break;
              case '名称':
                expect(this.getNotificationTitle().getText()).toBe(itemInfo);
                break;
              default:
                expect(this.getElementByText(itemKey).getText()).toBe(itemInfo);
                break;
            }
          }
          break;
        case '通知方式1':
        case '通知方式2':
          const index = String(key).replace('通知方式', '');
          for (const itemKey in expectValue[key]) {
            const itemInfo = expectValue[key][itemKey];
            switch (itemKey) {
              case '通知对象':
                expect(
                  this.getNotificationReceiver(index, itemKey).getText(),
                ).toBe(`${itemInfo}`);
                break;
              default:
                expect(this.getNotificationText(index, itemKey).getText()).toBe(
                  `${itemKey}：${itemInfo}`,
                );
                break;
            }
          }
          break;
      }
    }
  }
}
