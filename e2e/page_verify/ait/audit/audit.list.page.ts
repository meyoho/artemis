import { AuditListPage } from '@e2e/page_objects/ait/audit/auditlist.page';

export class AuditListPageVerify extends AuditListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      if (expectValue.hasOwnProperty(key)) {
        switch (key) {
          case '请求URI':
            expect(this.auditTable.getRowByIndex(1).getText()).toContain(
              expectValue[key],
            );
            break;
        }
      }
    }
  }
}
