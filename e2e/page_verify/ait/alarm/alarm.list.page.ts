import { AlarmListPage } from '@e2e/page_objects/ait/alarm/alarmlist.page';

export class AlarmListPageVerify extends AlarmListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      if (expectValue.hasOwnProperty(key)) {
        switch (key) {
          case '检索数量':
            expect(this.alarmTable.getRowCount()).toBe(expectValue[key]);
            break;
          case '无数据':
            expect(this.noData.isPresent()).toBe(expectValue[key]);
            break;
        }
      }
    }
  }
}
