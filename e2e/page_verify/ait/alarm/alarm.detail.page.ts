import { AlarmDetailPage } from '@e2e/page_objects/ait/alarm/alarmdetail.page';
import { AlaudaText } from '@e2e/element_objects/alauda.text';
import { by } from 'protractor';

export class AlarmDetailPageVerify extends AlarmDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '基本信息':
          const basicInfo = expectValue[key];
          for (const infoKey in basicInfo) {
            const value = basicInfo[infoKey];
            expect(this.getElementByText(infoKey).getText()).toBe(value);
          }
          break;
        case '告警规则':
          const alarmRule = expectValue[key];
          alarmRule.forEach((itemList, trnum) => {
            trnum = trnum + 1;
            itemList.forEach((text, tdnum) => {
              tdnum = tdnum + 1;
              const rule = new AlaudaText(
                by.xpath(
                  `//div[text() = " 告警规则 "]/..//aui-table-row[${trnum}]//aui-table-cell[${tdnum}]`,
                ),
              );
              expect(rule.getText()).toBe(text);
            });
          });
          break;
        case '操作指令':
          const alarmAction = expectValue[key];
          alarmAction.forEach(itemList => {
            itemList.forEach(item => {
              if (Array.isArray(item)) {
                item.forEach(element => {
                  expect(this.getElementByText(key).getText()).toContain(
                    element,
                  );
                });
              } else {
                expect(this.getElementByText(key).getText()).toContain(item);
              }
            });
          });
          break;
      }
    }
  }
}
