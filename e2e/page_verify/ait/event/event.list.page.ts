import { EventListPage } from '@e2e/page_objects/ait/event/eventlist.page';

export class EventListPageVerify extends EventListPage {
  async verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '时间范围':
          this.timeRange.auiIcon.click();
          const listItemText = await this.timeRange.auiToolTip.auiToolTip
            .$$('aui-option>div')
            .getText()
            .then(text => {
              return text;
            });
          await expectValue[key].forEach(item => {
            expect(listItemText).toContain(item);
          });
          this.timeRange.auiIcon.click();
          break;
        case '事件':
          this.auiTable.getText().then(text => {
            console.log(text.split('事件详情').length);
            const list = text
              .replace(/\s/g, '')
              .replace(/\n/g, '')
              .split('事件详情');
            let isDeployment = true;
            for (let i = 0; i < list.length - 1; i++) {
              isDeployment = isDeployment && list[i].includes('Deployment');
            }
            expect(isDeployment).toBeTruthy();
            expect(text).toContain(expectValue[key][0]);
            expect(text).toContain(expectValue[key][1]);
          });
          break;
      }
    }
  }
}
