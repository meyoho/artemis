import { AsmPageBase } from '@e2e/page_objects/asm/asm.page.base';
import { PageBase } from '@e2e/page_objects/page.base';
import { CommonKubectl } from '@e2e/utility/common.kubectl';
import { CommonMethod } from '@e2e/utility/common.method';

export class YamlVerify extends PageBase {
  verifyYaml(type, resource_name, namespace, expectJson) {
    const asmPage = new AsmPageBase();
    const yaml = CommonKubectl.execKubectlCommand(
      `kubectl get ${type} ${resource_name} -n ${namespace} -o yaml`,
      asmPage.clusterName,
    );

    const json = CommonMethod.parseYaml(yaml);
    this.jsonContain(expectJson, json, true);
  }
  isDelete(type, resource_name, namespace) {
    const asmPage = new AsmPageBase();
    const isDelete = CommonKubectl.execKubectlCommand(
      `kubectl get ${type} ${resource_name} -n ${namespace} -o yaml`,
      asmPage.clusterName,
    ).includes('NotFound');
    return isDelete;
  }
}
