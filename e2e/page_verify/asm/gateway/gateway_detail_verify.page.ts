import { GatawayDetailPage } from '@e2e/page_objects/asm/gateway/gataway_detail.page';
import { AlaudaAuiTable } from '@e2e/element_objects/alauda.aui_table';
import { $, browser, promise } from 'protractor';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class GatewayDetailVerify extends GatawayDetailPage {
  /**
   * 验证网关基本信息
   */
  verifyGatewayInfo(expectData) {
    for (const key in expectData) {
      expect((this.getElementByText(key) as AlaudaAuiTable).getText()).toBe(
        expectData[key],
      );
    }
  }

  /**
   * 验证网关路由信息
   */
  verifyGatewayRouteInfo(expectData): promise.Promise<void> {
    const area = new AlaudaElement(
      '.baseInfo .alo-detail .alo-detail__field',
      '.baseInfo .alo-detail .alo-detail__field label',
    );
    area.scrollToBoWindowBottom();
    for (const key in expectData) {
      switch (key) {
        case '条件规则':
          const conditionRule = new AuiTableComponent(
            $('alo-route-detail .condition'),
            'aui-table',
          );

          const ruleDetailBtn = new AlaudaButton(
            $('alo-route-detail .condition .switchBtn'),
          );
          ruleDetailBtn.scrollToBoWindowBottom();
          ruleDetailBtn.click().then(() => {
            expect(conditionRule.getColumeTextByName('条件类别')).toEqual(
              expectData[key][0],
            );
            expect(conditionRule.getColumeTextByName('匹配方式')).toEqual(
              expectData[key][1],
            );
            expect(conditionRule.getColumeTextByName('Key')).toEqual(
              expectData[key][2],
            );
            expect(conditionRule.getColumeTextByName('Value')).toEqual(
              expectData[key][3],
            );
          });

          break;
        case '权重规则':
          const weightRule = new AuiTableComponent(
            $('alo-route-detail .weights'),
            'aui-table',
          );
          expect(weightRule.getColumeTextByName('工作负载')).toEqual(
            expectData[key][0],
          );
          expect(weightRule.getColumeTextByName('版本')).toEqual(
            expectData[key][1],
          );
          expect(weightRule.getColumeTextByName('权重')).toEqual(
            expectData[key][2],
          );
          break;
        default:
          expect(area.getElementByText(key, 'span').getText()).toBe(
            expectData[key],
          );
          break;
      }
    }
    return browser.sleep(1);
  }
}
