import { GatewayListPage } from '@e2e/page_objects/asm/gateway/gataway_list.page';
import { browser } from 'protractor';

export class GatewayListVerify extends GatewayListPage {
  /**
   * 验证列表有几条数据
   * @param num 列表的数量
   * @param name 要搜索的名字
   */
  verifyNum(num, name = '') {
    if (name === '') {
      expect(this.gatewayTable.getRowCount()).toBe(num);
    } else {
      this.gatewayTable.searchByResourceName(name, num);
      expect(this.gatewayTable.getRowCount()).toBe(num);
    }
    return browser.sleep(1);
  }
}
