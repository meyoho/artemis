import { AsmProjectListPage } from '@e2e/page_objects/asm/admin_view_project/asm_project_list.page';
import { browser } from 'protractor';

export class AsmProjectListVerifyPage extends AsmProjectListPage {
  verifyListInfo(expectData, name) {
    for (const key in expectData) {
      switch (key) {
        case '名称':
          this.projectTable.getCell(key, name).then(elem => {
            expect(elem.$('a').getText()).toEqual(expectData[key]);
          });
          break;
        case '关联集群':
          this.projectTable.getCell(key, name).then(elem => {
            expect(elem.$('div').getText()).toEqual(expectData[key]);
          });
          break;
      }
    }
    return browser.sleep(1);
  }
}
