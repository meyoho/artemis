import { AsmProjectDetailPage } from '@e2e/page_objects/asm/admin_view_project/asm_project_detail.page';

export class AsmProjectDetailVerifyPage extends AsmProjectDetailPage {
  /**
   * 验证基本信息
   */
  verifyBasicDetailInfo(expectData) {
    for (const key in expectData) {
      expect(this.getElementByText(key, 'span').getText()).toBe(
        expectData[key],
      );
      break;
    }
  }

  /**
   * 验证命名空间列表
   */
  verifyNamespaceInfo(namespaceExpectData) {
    for (const key in namespaceExpectData) {
      switch (namespaceExpectData[key]) {
        case '名称':
          this.namespaceTable.getCell(key, name).then(elem => {
            expect(elem.$('span').getText()).toEqual(namespaceExpectData[key]);
          });
          break;
        case '关联集群':
          this.namespaceTable.getCell(key, name).then(elem => {
            expect(elem.$('div').getText()).toEqual(namespaceExpectData[key]);
          });
          break;
      }
    }
  }
}
