import { MicroServiceListPage } from '@e2e/page_objects/asm/servicelist/microservicelist.page';
import { browser } from 'protractor';

export class MicroServiceListPageVerify extends MicroServiceListPage {
  /**
   * 验证列表有几条数据
   * @param num 列表的数量
   * @param name 要搜索的名字
   */
  verifyNum(num, name: string = '') {
    if (name === '') {
      expect(this.serviceListTable.getRowCount()).toBe(num);
    } else {
      this.serviceListTable.searchByResourceName(name, num);
      expect(this.serviceListTable.getRowCount()).toBe(num);
    }
    return browser.sleep(1);
  }

  verifyParam(expectData, name) {
    for (const key in expectData) {
      if (expectData.hasOwnProperty(key)) {
        switch (key) {
          case '名称':
            this.serviceListTable.getCell(key, name).then(elem => {
              expect(elem.$('a').getText()).toEqual(expectData[key]);
            });
            break;
          case '工作负载':
            this.serviceListTable.getCell(key, name).then(elem => {
              expect(elem.$('p').getText()).toEqual(expectData[key]);
            });
            break;
        }
      }
    }
    return browser.sleep(1);
  }
}
