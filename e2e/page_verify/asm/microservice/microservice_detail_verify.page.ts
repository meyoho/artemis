import { MicroServiceDetailPage } from '@e2e/page_objects/asm/servicelist/microservice_detail.page';
import { browser, promise, $ } from 'protractor';
import { AuiTableComponent } from '@e2e/component/aui_table.component';
import { AlaudaElement } from '@e2e/element_objects/alauda.element';
import { AlaudaButton } from '@e2e/element_objects/alauda.button';

export class MicroDetailPageVerify extends MicroServiceDetailPage {
  /**
   * 验证服务入口列表数量
   */
  verifyServiceEntryNum(num) {
    browser.sleep(700);
    expect(this.serviceEntryTable.getRowCount()).toBe(num);
  }

  /**
   * 验证服务入口列表参数信息
   */
  verifyServiceEntryInfo(expectData) {
    browser.sleep(700);
    for (const key in expectData) {
      switch (key) {
        case '服务入口':
          expect(this.serviceEntryTable.getRowByIndex(0).getText()).toEqual(
            expectData[key],
          );
      }
    }
  }

  /**
   * 验证策略列表数量
   */
  verifyStrategyNum(num) {
    browser.sleep(700);
    expect(this.strategyTable.getRowCount()).toBe(num);
    return browser.sleep(1);
  }

  /**
   * 验证策略列表信息
   */
  verifyStrategyInfo(expectData) {
    for (const key in expectData) {
      switch (key) {
        case '策略类型':
          this.waitElementPresent(
            this.strategyTable.getRowByIndex(0),
            '策略列表没数据',
          );
          expect(this.strategyTable.getColumeTextByName('策略类型')).toEqual(
            expectData[key],
          );
          break;
        case '策略详情':
          expect(this.strategyTable.getColumeTextByName('策略详情')).toEqual(
            expectData[key],
          );
          break;
      }
    }
    return browser.sleep(1);
  }

  /**
   * 验证白名单列表数量
   */
  verifyWhiteListNum(num): promise.Promise<void> {
    browser.sleep(700).then(() => {
      expect(this.whiteListTable.getRowCount()).toBe(num);
    });
    return browser.sleep(1);
  }

  /**
   * 验证白名单列表信息
   */
  verifyWhiteListInfo(expectData): promise.Promise<void> {
    for (const key in expectData) {
      switch (key) {
        case '微服务':
          this.waitElementPresent(
            this.whiteListTable.getRowByIndex(0),
            '访问控制列表没数据',
          );
          expect(this.whiteListTable.getColumeTextByName(key)).toEqual(
            expectData[key],
          );
          break;
        case '版本':
          expect(this.whiteListTable.getColumeTextByName(key)).toEqual(
            expectData[key],
          );
          break;
      }
    }
    return browser.sleep(1);
  }

  /**
   * 验证路由信息
   */
  verifyRouteInfo(expectData): promise.Promise<void> {
    const area = new AlaudaElement(
      '.baseInfo .alo-detail .alo-detail__field',
      '.baseInfo .alo-detail .alo-detail__field label',
    );
    area.scrollToBoWindowBottom();
    for (const key in expectData) {
      switch (key) {
        case '条件规则':
          const conditionRule = new AuiTableComponent(
            $('alo-route-detail .condition'),
            'aui-table',
          );

          const ruleDetailBtn = new AlaudaButton(
            $('alo-route-detail .condition .switchBtn'),
          );
          ruleDetailBtn.scrollToBoWindowBottom();
          ruleDetailBtn.click().then(() => {
            expect(conditionRule.getColumeTextByName('条件类别')).toEqual(
              expectData[key][0],
            );
            expect(conditionRule.getColumeTextByName('匹配方式')).toEqual(
              expectData[key][1],
            );
            expect(conditionRule.getColumeTextByName('Key')).toEqual(
              expectData[key][2],
            );
            expect(conditionRule.getColumeTextByName('Value')).toEqual(
              expectData[key][3],
            );
          });

          break;
        case '权重规则':
          const weightRule = new AuiTableComponent(
            $('alo-route-detail .weights'),
            'aui-table',
          );
          expect(weightRule.getColumeTextByName('工作负载')).toEqual(
            expectData[key][0],
          );
          expect(weightRule.getColumeTextByName('版本')).toEqual(
            expectData[key][1],
          );
          expect(weightRule.getColumeTextByName('权重')).toEqual(
            expectData[key][2],
          );
          break;
        default:
          expect(area.getElementByText(key, 'span').getText()).toBe(
            expectData[key],
          );
          break;
      }
    }
    return browser.sleep(1);
  }
}
