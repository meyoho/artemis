import { ServiceMeshDetailPage } from '@e2e/page_objects/asm/admin_view_service_mesh/service_mesh_detail.page';

export class VerifyServiceMeshDetailPage extends ServiceMeshDetailPage {
  verifyDetailInfo(expectData) {
    for (const key in expectData) {
      expect(this.getElementByText(key, 'span').getText()).toBe(
        expectData[key],
      );
      break;
    }
  }
}
