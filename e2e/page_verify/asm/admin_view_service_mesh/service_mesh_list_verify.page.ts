import { ServiceMeshListPage } from '@e2e/page_objects/asm/admin_view_service_mesh/service_mesh_list.page';

export class VerifyServiceMeshListPage extends ServiceMeshListPage {
  verifyListInfo(expectData, name) {
    for (const key in expectData) {
      switch (key) {
        case '名称':
          this.serviceMeshTable.getCell(key, name).then(elem => {
            expect(elem.$('a').getText()).toEqual(expectData[key]);
          });
          break;
        case '集群':
          this.serviceMeshTable.getCell(key, name).then(elem => {
            expect(elem.getText()).toEqual(expectData[key]);
          });
          break;
        case '状态':
          this.serviceMeshTable.getCell(key, name).then(elem => {
            expect(elem.$('div').getText()).toEqual(expectData[key]);
          });
          break;
      }
    }
  }
}
