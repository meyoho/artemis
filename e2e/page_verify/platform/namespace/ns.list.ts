import { NamespaceListPage } from '@e2e/page_objects/platform/namespace/list.page';

export class NslistVerify extends NamespaceListPage {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    case '标题':
                        expect(this.breadcrumb.getText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case 'Header':
                        expect(this.namespaceNameTable.getHeaderText()).toEqual(
                            expectValue[key]
                        );
                        break;
                    case '数量':
                        expect(this.namespaceNameTable.getRowCount()).toBe(
                            expectValue[key]
                        );
                        break;
                    case '状态':
                        expect(this.getNsStatus(expectValue[key]['名称'])).toBe(
                            expectValue[key]['状态']
                        );
                }
            }
        }
    }
}
