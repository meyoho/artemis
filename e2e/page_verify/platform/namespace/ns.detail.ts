import { AlaudaTagsLabel } from '@e2e/element_objects/alauda.alo_tags_label';
import { DetailNamespacePage } from '@e2e/page_objects/platform/namespace/detail.page';
import { ImportNamespacePage } from '@e2e/page_objects/platform/namespace/import.page';

export class NsDetailVerify extends DetailNamespacePage {
  get nsImportPage(): ImportNamespacePage {
    return new ImportNamespacePage();
  }
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '标题':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        case '基本信息':
          for (const k in expectValue[key]) {
            switch (k) {
              case '命名空间名称':
                expect(this.name.getText()).toContain(expectValue[key][k]);
                break;
              default:
                expect(
                  this.getElementByText(k)
                    .getText()
                    .then(text => {
                      return text.replace(/\n/g, '');
                    }),
                ).toContain(expectValue[key][k]);
            }
          }

          break;
        case '资源配额':
          for (const k in expectValue[key]) {
            expect(this.getResourceQuotaValue(k)).toBe(expectValue[key][k]);
          }
          break;
        case '容器限额':
          for (const k in expectValue[key]) {
            for (const i in expectValue[key][k]) {
              expect(this.getLimitRangeValue(k, i)).toBe(
                expectValue[key][k][i],
              );
            }
          }
          break;
        case '标签':
        case '注解':
          // const testDataAnn = [
          //     ['resource.alauda.io/status', 'Initializing'],
          //     ['add', 'updateannotation']
          // ];
          const tags: AlaudaTagsLabel = new AlaudaTagsLabel(
            this.getElementByText(key),
            '.plain-container__label',
            '.full-content-toggle',
            '.cdk-overlay-pane .plain-container__label',
          );

          expectValue[key].forEach(dataArray => {
            expect(tags.getText()).toContain(
              `${dataArray[0]}: ${dataArray[1]}`,
            );
          });

          break;
        case '不包含标签':
          const nolabels: AlaudaTagsLabel = new AlaudaTagsLabel(
            this.getElementByText('标签'),
            '.plain-container__label',
            '.full-content-toggle',
            '.cdk-overlay-pane .plain-container__label',
          );

          expectValue[key].forEach(dataArray => {
            expect(nolabels.getText()).not.toContain(
              `${dataArray[0]}: ${dataArray[1]}`,
            );
          });

          break;
        case '不包含注解':
          const notags: AlaudaTagsLabel = new AlaudaTagsLabel(
            this.getElementByText('注解'),
            '.plain-container__label',
            '.full-content-toggle',
            '.cdk-overlay-pane .plain-container__label',
          );

          expectValue[key].forEach(dataArray => {
            expect(notags.getText()).not.toContain(
              `${dataArray[0]}: ${dataArray[1]}`,
            );
          });

          break;
        case '验证命名空间不存在':
          this.nsImportPage
            .verify_ns(
              expectValue[key]['所属集群'],
              expectValue[key]['命名空间'],
            )
            .then(bool => {
              expect(bool).toBeFalsy();
            });
          break;
        case '验证命名空间存在':
          this.nsImportPage
            .verify_ns(
              expectValue[key]['所属集群'],
              expectValue[key]['命名空间'],
            )
            .then(bool => {
              expect(bool).toBeTruthy();
            });
          break;
        default:
          expect(
            this.getElementByText(key)
              .getText()
              .then(text => {
                return text.replace(/\n/g, '');
              }),
          ).toBe(expectValue[key]);
          break;
      }
    }
  }
}
