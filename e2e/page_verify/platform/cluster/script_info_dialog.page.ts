import { ScriptInfoDialog } from '@e2e/page_objects/acp/cluster/script_info_dialog.page';

export class ScriptInfoDialogVerify extends ScriptInfoDialog {
    verify(expectValue) {
        for (const key in expectValue) {
            if (expectValue.hasOwnProperty(key)) {
                switch (key) {
                    // 验证configDialog 信息
                    case '脚本':
                        this.getElementByText(key)
                            .getText()
                            .then(text => {
                                for (const itemKey in expectValue[key]) {
                                    if (expectValue[key].hasOwnProperty(key)) {
                                        const item = expectValue[key][itemKey];
                                        expect(text).toContain(item);
                                    }
                                }
                            });
                        this.close();

                        break;
                }
            }
        }
    }
}
