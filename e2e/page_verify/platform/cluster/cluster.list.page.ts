import { ClusterListPage } from '@e2e/page_objects/platform/cluster/cluster.list.page';

export class ClusterListPageVerify extends ClusterListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '检索数量':
          expect(this.clusterTable.getRowCount()).toBe(expectValue[key]);
          break;
        case '无数据':
          expect(this.noData.isPresent()).toBe(expectValue[key]);
          break;
      }
    }
  }
}
