import { CreateClusterPage } from '@e2e/page_objects/platform/cluster/cluster.create.page';

export class CreateClusterPageVerify extends CreateClusterPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        // 验证configDialog 信息
        case '脚本信息':
          this.shellText.getText().then(text => {
            for (const itemKey in expectValue[key]) {
              if (expectValue[key].hasOwnProperty(key)) {
                const item = expectValue[key][itemKey];
                expect(text).toContain(item);
              }
            }
          });

          break;
      }
    }
  }
}
