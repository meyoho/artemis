import { ClusterDetailPage } from '@e2e/page_objects/platform/cluster/cluster.detail.page';

export class DetailClusterPageVerify extends ClusterDetailPage {
  verify(expectValue: { [key: string]: string }) {
    for (const key in expectValue) {
      switch (key) {
        case '超售比':
          this.getElementByText(key)
            .getText()
            .then(text => {
              expect(text.replace(/\s/g, '')).toContain(expectValue[key]);
            });
          break;
      }
    }
  }
}
