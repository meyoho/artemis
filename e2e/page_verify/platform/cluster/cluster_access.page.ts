import { AccessClusterPage } from '@e2e/page_objects/acp/cluster/cluster_access.page';
import { browser } from 'protractor';

export class AccessClusterPageVerify extends AccessClusterPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        // 验证configDialog 信息
        case '标题':
          expect(this.confirmDialog.title.getText()).toContain(
            expectValue[key],
          );
          break;
        case '内容':
          expect(this.confirmDialog.content.getText()).toContain(
            expectValue[key],
          );
          break;
        // 验证面包屑 信息
        case '面包屑':
          expect(this.breadcrumb.getText()).toContain(expectValue[key]);
          break;
        // 验证警示信息
        case '警示信息':
          this.alertMessage.getText().then(text => {
            for (const itemkey in expectValue[key]) {
              const item = expectValue[key][itemkey];
              expect(text).toContain(item);
            }
          });
          break;

        case '必填项':
          this.waitElementPresent(
            this.itemsRequired.first(),
            '必填项提示没有出现',
          );
          browser.sleep(1000);
          expect(this.itemsRequired.count()).toBe(expectValue[key]);

          expect(this.itemsRequired.count()).toBe(
            this.itemsRequiredTooltip.count(),
          );

          break;
      }
    }
  }
}
