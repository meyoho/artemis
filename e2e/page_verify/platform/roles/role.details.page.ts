import { RoleDetailPage } from '@e2e/page_objects/platform/roles/role.details.page';

export class RoleDetailPageVerify extends RoleDetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '基本信息':
          const basicInfo = expectValue[key];

          for (const basinInfoKey in basicInfo) {
            const basicIntem = basicInfo[basinInfoKey];
            const itemElem = this.alaudaElement.getElementByText(
              basinInfoKey,
              'span',
            );
            this.waitElementPresent(itemElem, '');
            expect(itemElem.getText()).toContain(basicIntem);
          }

          break;
      }
    }
  }
}
