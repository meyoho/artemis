import { RoleListPage } from '@e2e/page_objects/platform/roles/role.list.page';

export class RoleListPageVerify extends RoleListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      const rowElem = this.roleTable.getRow([key]);
      rowElem.getText().then(rowText => {
        for (const rowKey in expectValue[key]) {
          const cellText = expectValue[key][rowKey];
          expect(rowText.includes(cellText)).toBeTruthy();
        }
      });
    }
  }
}
