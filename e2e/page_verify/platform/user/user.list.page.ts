import { UserListPage } from '@e2e/page_objects/platform/user/user.list.page';

export class UserListPageVerify extends UserListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '状态':
          this.usersTable
            .getCell('状态', [expectValue[key]])
            .then(function(elem) {
              expect(elem.getText()).toContain(expectValue[key]);
            });
          break;
        case '来源':
          this.usersTable
            .getCell('来源', [expectValue[key]])
            .then(function(elem) {
              expect(elem.getText()).toContain(expectValue[key]);
            });
          break;
      }
    }
  }
}
