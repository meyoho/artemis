import { LicenseManageListPage } from '@e2e/page_objects/platform/licensemanage/license_list.page';

export class LicenseListVerifyPage extends LicenseManageListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '面包屑':
          this.breadcrumb.getText().then(text => {
            expect(text).toBe(expectValue[key]);
          });
          break;
        case '提示信息':
          this.alarmInfo.then(text => {
            expect(text).toContain(expectValue[key]);
          });
          break;
        case '表头':
          this.licenseTable.getHeaderText().then(texts => {
            expect(texts).toEqual(expectValue[key]);
          });
          break;
        case '存在数据':
        case '不存在数据':
          expectValue[key].forEach(v => {
            this.licenseTable.hasRow(v).then(has => {
              if (key == '存在数据') {
                expect(has).toBe(true);
              } else if (key == '不存在数据') {
                expect(has).toBe(false);
              }
            });
          });
          break;
        case '存在导入License弹窗':
          this.licenseImportPage.isPresent.then(ispresent => {
            expect(ispresent).toEqual(expectValue[key]);
          });
          break;
      }
    }
  }
}
