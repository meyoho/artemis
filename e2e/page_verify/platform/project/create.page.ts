/**
 * Created by zhangjiao on 2018/7/5.
 */

import { CreatePage } from '@e2e/page_objects/platform/project/create.page';
import { browser } from 'protractor';

export class CreatePageVerify extends CreatePage {
  verify(expectValue: Map<string, string>) {
    for (const key in expectValue) {
      switch (key) {
        case '项目创建成功':
          this.toast.getMessage().then(text => {
            expect(text).toEqual('项目创建成功');
          });

          expect(browser.getCurrentUrl()).toContain(expectValue[key]);
          console.log('key:' + key);
          console.log('value:' + expectValue[key]);
          break;
      }
    }
  }
}
