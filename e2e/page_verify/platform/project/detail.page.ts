import { DetailPage } from '@e2e/page_objects/platform/project/detail.page';
import { ElementFinder } from 'protractor';

export class DetailPageVerify extends DetailPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '基本信息':
          const basicInfo = expectValue[key];

          for (const basinInfoKey in basicInfo) {
            const basicIntem = basicInfo[basinInfoKey];
            const itemElem = this.alaudaElement.getElementByText(
              basinInfoKey,
              'span',
            );
            this.waitElementPresent(itemElem, '');
            expect(itemElem.getText()).toBe(basicIntem);
          }
          break;
        case '项目配额':
          const projectQuotas = expectValue[key];
          projectQuotas.forEach(
            (itemList: Map<string, string>, clusterName) => {
              for (const itemkey in itemList) {
                const cpu: Array<ElementFinder> = this.getQuotaElementByText(
                  clusterName,
                  itemkey,
                );
                cpu[0].getText().then(text => {
                  expect(text).toBe(itemList[itemkey][0]);
                });
                cpu[1].getText().then(text => {
                  expect(text).toBe(itemList[itemkey][1]);
                });
              }
            },
          );
          break;
      }
    }
  }
}
