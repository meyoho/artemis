import { ListPage } from '@e2e/page_objects/platform/project/list.page';

export class ListPageVerify extends ListPage {
  verify(expectValue) {
    for (const key in expectValue) {
      switch (key) {
        case '名称':
          this.projectsTable.getCell('名称', [expectValue[key]]).then(data => {
            data.getText().then(text => {
              expect(text).toBe(
                expectValue[key] + `\n${expectValue['显示名称']}`,
              );
            });
          });
          break;
        case '状态':
          this.projectsTable
            .getCell('状态', [expectValue['名称']])
            .then(data => {
              data.getText().then(text => {
                expect(text).toBe(expectValue[key]);
              });
            });
          break;
        case '集群(CPU/内存/存储)':
          this.projectsTable
            .getCell('集群(CPU/内存/存储)', [expectValue['名称']])
            .then(data => {
              data.getText().then(text => {
                expect(text).toBe(expectValue[key]);
              });
            });
          break;
        case '创建时间':
          this.projectsTable
            .getCell('创建时间', [expectValue['名称']])
            .then(cellElem => {
              cellElem.getText().then(text => {
                expect(text).not.toBe(expectValue[key]);
              });
            });
          break;
      }
    }
  }
}
