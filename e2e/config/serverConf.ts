/**
 * 管理配置文件
 * Created by liuwei on 2018/3/2.
 */

interface Config {
  // 测试数据相关参数
  BRANCH_NAME: string;
  TESTDATAPATH: string;

  // ladap 相关参数
  LDAP_ISREADY: string;
  LDAPLOGIN_URL: string;
  LDAPLOGIN_USER: string;
  LDAPLOGIN_PASSWORD: string;
  // LDAP 注册好的账户
  LDAP_USER: string;
  LDAP_PASSWORD: string;

  BASE_URL: string;
  ADMIN_USER: string;
  ADMIN_PASSWORD: string;
  TESTIMAGE: string;

  // label base Domain
  LABELBASEDOMAIN: string;

  //kubectl config 相关参数
  GLOBAL_MASTER_IP: string;
  GLOBAL_NAMESPCE: string;
  USER_TOKEN: string;

  BUSINESS_MASTER_IP: string;
  REGIONNAME: string;
  SECONDREGIONNAME: string;
  NFS_SERVER: string;

  IS_PUBLIC: string;

  // ASM相关
  SERVICE_MESH_NAME: string;

  // 子网相关
  CALICOREGIONNAME: string;
  OVNREGIONNAME: string;
  MACVLANREGIONNAME: string;

  // 联邦相关
  FEDERATIONREGIONNAME: string;

  // Jenkins 相关参数
  JENKINS_NAME: string;
  JENKINS_HOST: string;
  JENKINS_USER: string;
  JENKINS_PWD: string;

  // Nexus 相关参数
  NEXUS_URL: string;
  NEXUS_NAME: string;
  NEXUS_USER: string;
  NEXUS_PWD: string;

  // Maven 相关参数
  MAVEN_NAME: string;

  // Github 相关参数
  GITHUB_USER: string;
  GITHUB_TOKEN1: string;

  // 企业版Gitlab 相关参数
  GITLAB_NAME: string;
  GITLAB_URL: string;
  GITLAB_USER: string;
  GITLAB_TOKEN: string;
  // 要用到的Gitlab 中的相关代码仓库
  GITLAB_TMP_PRIVATE: string;

  GITLAB_WORKLOAD: string;
  GITLAB_BRANCH: string;
  GITLAB_PUBLIC: string;
  GITLAB_PRIVATE: string;

  // DockerRegistry 相关参数
  REGISTRY_ISREADY: string; // registry ready状态是部署好了registry，并且push了镜像 auto-e2erepo/hello-go:
  REGISTRY_NAME: string;
  REGISTRY_HOST: string;
  REGISTRY_REPO: string;

  // Harbor 相关参数
  HARBOR_NAME: string;
  HARBOR_HTTP: string;
  HARBOR_HOST: string;
  HARBOR_USER: string;
  HARBOR_PWD: string;
  HARBOR_PROJECT: string;
  HARBOR_REPO: string;
  HARBOR_TAG: string;

  // Sonar 相关参数
  SONAR_ISREADY: string;
  SONAR_NAME: string;
  SONAR_HOST: string;
  SONAR_USER: string;
  SONAR_PWD: string;

  // 模板仓库相关参数
  HELM_CHART_REPO_URI: string;
  SVN_CHART_REPO_URI: string;
  GIT_CHART_REPO_URI: string;

  // 通知相关
  SERVER_HOST: string;
  SERVER_PORT: string;
  EMAILSENDER_NAME1: string;
  EMAILSENDER_PWD1: string;
  EMAILSENDER_NAME2: string;
  EMAILSENDER_PWD2: string;
  //devops 通知
  DEVOPS_EMAIL_NAME: string;

  //代理
  PROXY: string;
}

let config: Config;

const env = process.env.ENV;
switch (env) {
  case 'int':
    config = {
      // 测试数据相关参数
      BRANCH_NAME: `${process.env.BRANCH_NAME}`,
      TESTDATAPATH: process.cwd() + '/e2e/test_data/',

      // LDAP 相关参数
      LDAP_ISREADY: 'true',
      LDAPLOGIN_URL: 'http://ldapadmin.qa.alauda.cn/',
      LDAPLOGIN_USER: 'cn=admin,dc=alauda,dc=com',
      LDAPLOGIN_PASSWORD: 'admin',

      // LDAP 注册好的账户
      LDAP_USER: 'jiaozhang@alauda.io',
      LDAP_PASSWORD: '123456',

      BASE_URL: 'https://newint.alauda.cn',
      ADMIN_USER: 'admin@cpaas.io',
      ADMIN_PASSWORD: '6MkuTYi8HqyGnZugX2F4',
      TESTIMAGE: 'index.alauda.cn/alaudaorg/qaimages:helloworld',

      //kubectl config 相关参数
      GLOBAL_MASTER_IP: '192.168.16.41',
      GLOBAL_NAMESPCE: 'cpaas-system',
      LABELBASEDOMAIN: 'cpaas.io',
      USER_TOKEN:
        'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLWxkem5tIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIzMGYyMDAwNC02MmQxLTExZWEtODI1OC01MjU0MDBjM2U0MDEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.YMTe744f47IbstBAJ9QFtZmazYhJ4PKm1-SqUuZCIvwRQ7T0XGJRSBKEvdyn__-RnXcxtHxYyXm0lPHini72nte-gA8SrEEynWnoHj4FwWxbMU4cUCZwGRb2RPfjiaAv3BdnAG0Xx2nVy6xVHTgxF7BWcDuXxKu0exUhSn0MK-3bC3xSJybbaFa6TjXtpX7769kkUfL8xNEIywrRd50WAqpUnQD-XVsuk6AKFhxqP-mGX7GLI5-oTa2ylox0VZ0n3FBMNRsZX79ddFEgk30n0Tkxd6uvMKrgTpsvbuKACrS_lJHm3mc13GkrmusmDbRmm74iJpRR1be-vagWOLU1qQ',

      BUSINESS_MASTER_IP: '192.168.17.14', // 如果不配置，使用内网连busines 集群
      REGIONNAME: 'ovn',
      SECONDREGIONNAME: 'global',
      CALICOREGIONNAME: 'calico',
      OVNREGIONNAME: 'ovn',
      MACVLANREGIONNAME: 'ovn',
      FEDERATIONREGIONNAME: 'ovn',
      NFS_SERVER: '',

      IS_PUBLIC: `${process.env.IS_PUBLIC || 'true'}`,

      // ASM 相关参数
      SERVICE_MESH_NAME: `${process.env.SERVICE_MESH_NAME || 'ovn'}`,

      // Jenkins 相关参数
      JENKINS_NAME: `${process.env.JENKINS_NAME || 'jenkins-ovn'}`,
      JENKINS_HOST: `${process.env.JENKINS_HOST ||
        'http://192.168.17.14:32001'}`,
      JENKINS_USER: `${process.env.JENKINS_USER || 'admin'}`,
      JENKINS_PWD: `${process.env.JENKINS_PWD ||
        '11c2b0c5311088ce77060867af0b57cac1'}`,

      // Github 相关参数
      GITHUB_USER: `${process.env.GITHUB_USER || 'jiaozhang1'}`,
      GITHUB_TOKEN1: `${process.env.GITHUB_TOKEN1 ||
        '4cafb81c5f21b2465dda29782f8fd1b0d452df66'}`,

      // 企业版Gitlab 相关参数
      GITLAB_NAME: `${process.env.GITLAB_NAME || 'gitlab-staging'}`,
      GITLAB_URL: `${process.env.GITLAB_URL || 'http://10.0.128.241:31101'}`,
      GITLAB_USER: `${process.env.GITLAB_USER || 'root'}`,
      GITLAB_TOKEN: `${process.env.GITLAB_TOKEN || 'tzrgg4d1zzPxAfpVgzET'}`,
      // 要用到的Gitlab 中的相关代码仓库
      GITLAB_TMP_PRIVATE: `${process.env.GITLAB_TMP_PRIVATE ||
        'template-private'}`,
      GITLAB_WORKLOAD: `${process.env.GITLAB_WORKLOAD || 'workload'}`,
      GITLAB_BRANCH: `${process.env.GITLAB_BRANCH || 'test/v1.1'}`,
      GITLAB_PUBLIC: `${process.env.GITLAB_PUBLIC || 'devops-ui-public'}`,
      GITLAB_PRIVATE: `${process.env.GITLAB_PRIVATE || 'devops-ui-private'}`,

      // DockerRegistry 相关参数
      REGISTRY_ISREADY: `${process.env.REGISTRY_ISREADY || 'true'}`, // registry ready状态是部署好了registry，并且push了镜像 auto-e2erepo/hello-go:
      REGISTRY_NAME: `${process.env.REGISTRY_NAME || 'docker-registry'}`,
      REGISTRY_HOST: `${process.env.REGISTRY_HOST ||
        'http://10.0.128.241:32677'}`,
      REGISTRY_REPO: `${process.env.REGISTRY_REPO || 'auto-e2erepo'}`,
      // Nexus 相关参数
      NEXUS_URL: `${process.env.NEXUS_URL || 'http://10.0.128.241:32010'}`,
      NEXUS_NAME: `${process.env.NEXUS_NAME || 'nexus'}`,
      NEXUS_USER: `${process.env.NEXUS_USER || 'admin'}`,
      NEXUS_PWD: `${process.env.NEXUS_PWD || 'admin'}`,

      // Maven 相关参数
      MAVEN_NAME: `${process.env.MAVEN_NAME || 'maven-public'}`,
      // Harbor 相关参数
      HARBOR_NAME: `${process.env.HARBOR_NAME || 'prod-harbor'}`,
      HARBOR_HTTP: `${process.env.HARBOR_HTTP || 'http://'}`,
      HARBOR_HOST: `${process.env.HARBOR_HOST || '10.0.128.241:31104'}`,
      HARBOR_USER: `${process.env.HARBOR_USER || 'admin'}`,
      HARBOR_PWD: `${process.env.HARBOR_PWD || 'Harbor12345'}`,
      HARBOR_PROJECT: `${process.env.HARBOR_PROJECT || 'e2e-automation'}`,
      HARBOR_REPO: 'helloworld',
      HARBOR_TAG: `${process.env.HARBOR_TAG || 'latest'}`,

      // Sonar 相关参数c
      SONAR_ISREADY: `${process.env.SONAR_ISREADY || 'true'}`,
      SONAR_NAME: `${process.env.SONAR_NAME || 'sonarqube'}`,
      SONAR_HOST: `${process.env.SONAR_HOST || 'http://10.0.128.241:31342'}`,
      SONAR_USER: `${process.env.SONAR_USER || 'admin'}`,
      SONAR_PWD: `${process.env.SONAR_PWD ||
        'd40f4a76180d33521506935f8d3c715702a9d905'}`,

      // 模板仓库相关参数
      HELM_CHART_REPO_URI: `${process.env.HELM_CHART_REPO_URI ||
        'http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-release-new.ace-default.cloud.myalauda.cn/'}`,
      SVN_CHART_REPO_URI: `${process.env.SVN_CHART_REPO_URI ||
        'https://svn.apache.org/repos/asf/xml/xang/trunk'}`,
      GIT_CHART_REPO_URI: `${process.env.GIT_CHART_REPO_URI ||
        'https://github.com/alauda/captain-test-charts'}`,

      // devops 通知相关
      DEVOPS_EMAIL_NAME: `${process.env.DEVOPS_EMAIL_NAME ||
        'uiauto-devops-email1'}`,

      // 通知相关
      SERVER_HOST: `${process.env.SERVER_HOST || 'smtp.163.com'}`,
      SERVER_PORT: `${process.env.SERVER_PORT || '25'}`,
      EMAILSENDER_NAME1: `${process.env.EMAILSENDER_NAME1 ||
        'jiaozhang555@163.com'}`,
      EMAILSENDER_PWD1: `${process.env.EMAILSENDER_PWD1 || '123qwe'}`,
      EMAILSENDER_NAME2: `${process.env.EMAILSENDER_NAME2 ||
        'uiauto555@163.com'}`,
      EMAILSENDER_PWD2: `${process.env.EMAILSENDER_PWD2 || '123qwe'}`,

      PROXY: process.env.BROWSER_PROXY
        ? (function(): string {
            const proxy = process.env.BROWSER_PROXY.split(/;|=/).map(item =>
              item.trim(),
            );
            if (
              proxy[proxy.length - 2].startsWith('http') ||
              proxy[proxy.length - 2].startsWith('ssl')
            ) {
              return `http://${proxy[proxy.length - 1]}`;
            } else {
              return null;
            }
          })()
        : null,
    };
    break;
  case 'staging':
    config = {
      // 测试数据相关参数
      BRANCH_NAME: `${process.env.BRANCH_NAME}`,
      TESTDATAPATH: process.cwd() + '/e2e/test_data/',

      // LDAP 相关参数
      LDAP_ISREADY: 'true',
      LDAPLOGIN_URL: 'http://ldapadmin.qa.alauda.cn/',
      LDAPLOGIN_USER: 'cn=admin,dc=alauda,dc=com',
      LDAPLOGIN_PASSWORD: 'admin',

      // LDAP 注册好的账户
      LDAP_USER: `${process.env.LDAP_USER || 'jiaozhang@alauda.io'}`,
      LDAP_PASSWORD: `${process.env.LDAP_PASSWORD || '123456'}`,

      BASE_URL: `${process.env.BASE_URL || 'https://10.0.129.100'}`,
      ADMIN_USER: `${process.env.ADMIN_USER || 'admin@alauda.io'}`,
      ADMIN_PASSWORD: `${process.env.ADMIN_PASSWORD || 'password'}`,
      TESTIMAGE: `${process.env.TESTIMAGE ||
        'index.alauda.cn/alaudaorg/qaimages:helloworld'}`,

      //kubectl config 相关参数
      GLOBAL_MASTER_IP: `${process.env.GLOBAL_MASTER_IP || '94.191.76.27'}`,
      GLOBAL_NAMESPCE: `${process.env.GLOBAL_NAMESPCE || 'alauda-system'}`,
      LABELBASEDOMAIN: `${process.env.LABELBASEDOMAIN || 'alauda.io'}`,
      USER_TOKEN: `${process.env.USER_TOKEN ||
        'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLTh0dHFsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJhNTZmZDY0Yi05M2M2LTExZTktOGFhNC01MjU0MDBkNjMyNjUiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.BnxD9q905PubwE0BNutmFx2Peoa-qtaOhGqAVuAKvT4IA4vgPbXoWPIDRzpVP573xe84y1rHyEsPOMwlmjTYi0xUmW3HgyIHeoodLtGxtlje1WtbHuHS_ZnWtU26aENipAlzfJllEIcKJspWJS53vNm6xjW3VkJoLuPA45kU12-mmoQGxY-p4SeFF1MQsJX2F9cjmjR27KQ--BCKXJ9QQsI6ARnj11QzKKaJY71x89J0_nBk7aVBFQMbXOvDJ9_a3eTQQAgU4X-m35c3HcrRYW0mPSafGYr4V75KvDQfJXqHEGL_zt4t1Zw7q7s7eqMgvENV-6p5EPxwY6SV3563bg'}`,

      BUSINESS_MASTER_IP: `${process.env.BUSINESS_MASTER_IP ||
        '129.28.187.165'}`, // 如果不配置，使用内网连busines 集群
      REGIONNAME: `${process.env.REGIONNAME || 'high'}`,
      SECONDREGIONNAME: `${process.env.SECONDREGIONNAME || 'global'}`,
      CALICOREGIONNAME: `${process.env.CALICOREGIONNAME || 'cls-w5xrsf62'}`,
      OVNREGIONNAME: `${process.env.OVNREGIONNAME || 'ovn'}`,
      MACVLANREGIONNAME: `${process.env.MACVLANREGIONNAME || 'ovn'}`,
      FEDERATIONREGIONNAME: `${process.env.FEDERATIONREGIONNAME || 'mirror'}`,
      NFS_SERVER: `${process.env.NFS_SERVER || ''}`,

      IS_PUBLIC: `${process.env.IS_PUBLIC || 'true'}`,

      // ASM 相关参数
      SERVICE_MESH_NAME: `${process.env.SERVICE_MESH_NAME || 'newhigh'}`,

      // Jenkins 相关参数
      JENKINS_NAME: `${process.env.JENKINS_NAME || 'local-ares-jenkins'}`,
      JENKINS_HOST: `${process.env.JENKINS_HOST ||
        'http://10.0.128.241:32001'}`,
      JENKINS_USER: `${process.env.JENKINS_USER || 'admin'}`,
      JENKINS_PWD: `${process.env.JENKINS_PWD ||
        '116a236e2354969f00a83fa26c5e5db565'}`,

      // Github 相关参数
      GITHUB_USER: `${process.env.GITHUB_USER || 'jiaozhang1'}`,
      GITHUB_TOKEN1: `${process.env.GITHUB_TOKEN1 ||
        '4cafb81c5f21b2465dda29782f8fd1b0d452df66'}`,

      // 企业版Gitlab 相关参数
      GITLAB_NAME: `${process.env.GITLAB_NAME || 'gitlab-staging'}`,
      GITLAB_URL: `${process.env.GITLAB_URL || 'http://10.0.128.241:31101'}`,
      GITLAB_USER: `${process.env.GITLAB_USER || 'root'}`,
      GITLAB_TOKEN: `${process.env.GITLAB_TOKEN || 'tzrgg4d1zzPxAfpVgzET'}`,
      // 要用到的Gitlab 中的相关代码仓库
      GITLAB_TMP_PRIVATE: `${process.env.GITLAB_TMP_PRIVATE ||
        'template-private'}`,
      GITLAB_WORKLOAD: `${process.env.GITLAB_WORKLOAD || 'workload'}`,
      GITLAB_BRANCH: `${process.env.GITLAB_BRANCH || 'test/v1.1'}`,
      GITLAB_PUBLIC: `${process.env.GITLAB_PUBLIC || 'devops-ui-public'}`,
      GITLAB_PRIVATE: `${process.env.GITLAB_PRIVATE || 'devops-ui-private'}`,

      // DockerRegistry 相关参数
      REGISTRY_ISREADY: `${process.env.REGISTRY_ISREADY || 'true'}`, // registry ready状态是部署好了registry，并且push了镜像 auto-e2erepo/hello-go:
      REGISTRY_NAME: `${process.env.REGISTRY_NAME || 'docker-registry'}`,
      REGISTRY_HOST: `${process.env.REGISTRY_HOST ||
        'http://10.0.128.241:32677'}`,
      REGISTRY_REPO: `${process.env.REGISTRY_REPO || 'auto-e2erepo'}`,
      // Nexus 相关参数
      NEXUS_URL: `${process.env.NEXUS_URL || 'http://10.0.128.241:32010'}`,
      NEXUS_NAME: `${process.env.NEXUS_NAME || 'nexus'}`,
      NEXUS_USER: `${process.env.NEXUS_USER || 'admin'}`,
      NEXUS_PWD: `${process.env.NEXUS_PWD || 'admin'}`,

      // Maven 相关参数
      MAVEN_NAME: `${process.env.MAVEN_NAME || 'maven-public'}`,
      // Harbor 相关参数
      HARBOR_NAME: `${process.env.HARBOR_NAME || 'harbor-staging'}`,
      HARBOR_HTTP: `${process.env.HARBOR_HTTP || 'http://'}`,
      HARBOR_HOST: `${process.env.HARBOR_HOST || '10.0.128.241:31104'}`,
      HARBOR_USER: `${process.env.HARBOR_USER || 'admin'}`,
      HARBOR_PWD: `${process.env.HARBOR_PWD || 'Harbor12345'}`,
      HARBOR_PROJECT: `${process.env.HARBOR_PROJECT || 'e2e-automation'}`,
      HARBOR_REPO: `${process.env.HARBOR_REPO || 'helloworld'}`,
      HARBOR_TAG: `${process.env.HARBOR_TAG || 'latest'}`,

      // Sonar 相关参数
      SONAR_ISREADY: `${process.env.SONAR_ISREADY || 'true'}`,
      SONAR_NAME: `${process.env.SONAR_NAME || 'sonarqube'}`,
      SONAR_HOST: `${process.env.SONAR_HOST || 'http://10.0.128.241:31342'}`,
      SONAR_USER: `${process.env.SONAR_USER || 'admin'}`,
      SONAR_PWD: `${process.env.SONAR_PWD ||
        'd40f4a76180d33521506935f8d3c715702a9d905'}`,

      // 模板仓库相关参数
      HELM_CHART_REPO_URI: `${process.env.HELM_CHART_REPO_URI ||
        'http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-release-new.ace-default.cloud.myalauda.cn/'}`,
      SVN_CHART_REPO_URI: `${process.env.SVN_CHART_REPO_URI ||
        'https://svn.apache.org/repos/asf/xml/xang/trunk'}`,
      GIT_CHART_REPO_URI: `${process.env.GIT_CHART_REPO_URI ||
        'https://github.com/alauda/captain-test-charts'}`,

      // devops 通知相关
      DEVOPS_EMAIL_NAME: `${process.env.DEVOPS_EMAIL_NAME ||
        'uiauto-devops-email1'}`,

      // 通知相关
      SERVER_HOST: `${process.env.SERVER_HOST || 'smtp.163.com'}`,
      SERVER_PORT: `${process.env.SERVER_PORT || '25'}`,
      EMAILSENDER_NAME1: `${process.env.EMAILSENDER_NAME1 ||
        'jiaozhang555@163.com'}`,
      EMAILSENDER_PWD1: `${process.env.EMAILSENDER_PWD1 || '123qwe'}`,
      EMAILSENDER_NAME2: `${process.env.EMAILSENDER_NAME2 ||
        'uiauto555@163.com'}`,
      EMAILSENDER_PWD2: `${process.env.EMAILSENDER_PWD2 || '123qwe'}`,
      PROXY: process.env.BROWSER_PROXY
        ? (function(): string {
            const proxy = process.env.BROWSER_PROXY.split(/;|=/).map(item =>
              item.trim(),
            );
            if (
              proxy[proxy.length - 2].startsWith('http') ||
              proxy[proxy.length - 2].startsWith('ssl')
            ) {
              return `http://${proxy[proxy.length - 1]}`;
            } else {
              return null;
            }
          })()
        : null,
    };
    break;
  default:
    config = {
      // 测试数据相关参数
      BRANCH_NAME: `${process.env.BRANCH_NAME || 'local'}`,
      TESTDATAPATH: process.cwd() + '/e2e/test_data/',

      // LDAP 相关参数
      LDAP_ISREADY: `${process.env.LDAP_ISREADY || 'true'}`,
      LDAPLOGIN_URL:
        `${process.env.LDAPLOGIN_URL}` || 'http://ldapadmin.qa.alauda.cn/',
      LDAPLOGIN_USER: `${process.env.LDAPLOGIN_USER ||
        'cn=admin,dc=alauda,dc=com'}`,
      LDAPLOGIN_PASSWORD: `${process.env.LDAPLOGIN_PASSWORD || 'admin'}`,

      // LDAP 注册好的账户
      LDAP_USER: `${process.env.LDAP_USER || 'jiaozhang@alauda.io'}`,
      LDAP_PASSWORD: `${process.env.LDAP_PASSWORD || '123456'}`,

      BASE_URL: `${process.env.BASE_URL}`,
      ADMIN_USER: `${process.env.ADMIN_USER || 'admin@cpaas.io'}`,
      ADMIN_PASSWORD: `${process.env.ADMIN_PASSWORD || 'password'}`,
      TESTIMAGE: `${process.env.TESTIMAGE ||
        'index.alauda.cn/alaudaorg/qaimages:helloworld'}`,

      //kubectl config 相关参数
      GLOBAL_MASTER_IP: `${process.env.GLOBAL_MASTER_IP || '118.24.219.139'}`,
      GLOBAL_NAMESPCE: `${process.env.GLOBAL_NAMESPCE || 'alauda-system'}`,
      LABELBASEDOMAIN: `${process.env.LABELBASEDOMAIN}`,
      USER_TOKEN: `${process.env.USER_TOKEN || 'false'}`,

      BUSINESS_MASTER_IP: `${process.env.BUSINESS_MASTER_IP || 'false'}`, // 如果不配置，使用内网连busines 集群
      REGIONNAME: `${process.env.REGIONNAME || 'high'}`,
      SECONDREGIONNAME: `${process.env.SECONDREGIONNAME}`,
      CALICOREGIONNAME: `${process.env.CALICOREGIONNAME}`,
      OVNREGIONNAME: `${process.env.OVNREGIONNAME}`,
      MACVLANREGIONNAME: `${process.env.MACVLANREGIONNAME}`,
      FEDERATIONREGIONNAME: `${process.env.FEDERATIONREGIONNAME}`,
      NFS_SERVER: `${process.env.NFS_SERVER || '192.168.0.1'}`,

      IS_PUBLIC: `${process.env.IS_PUBLIC || 'false'}`,

      // ASM 相关参数
      SERVICE_MESH_NAME: `${process.env.SERVICE_MESH_NAME || 'ovn'}`,

      // Jenkins 相关参数
      JENKINS_NAME: `${process.env.JENKINS_NAME || 'local-ares-jenkins'}`,
      JENKINS_HOST: `${process.env.JENKINS_HOST ||
        'http://10.0.128.241:32001'}`,
      JENKINS_USER: `${process.env.JENKINS_USER || 'admin'}`,
      JENKINS_PWD: `${process.env.JENKINS_PWD ||
        '116a236e2354969f00a83fa26c5e5db565'}`,

      // Github 相关参数
      GITHUB_USER: `${process.env.GITHUB_USER || 'jiaozhang1'}`,
      GITHUB_TOKEN1: `${process.env.GITHUB_TOKEN1 ||
        '4cafb81c5f21b2465dda29782f8fd1b0d452df66'}`,

      // 企业版Gitlab 相关参数
      GITLAB_NAME: `${process.env.GITLAB_NAME || 'gitlab-staging'}`,
      GITLAB_URL: `${process.env.GITLAB_URL || 'http://10.0.128.241:31101'}`,
      GITLAB_USER: `${process.env.GITLAB_USER || 'root'}`,
      GITLAB_TOKEN: `${process.env.GITLAB_TOKEN || 'tzrgg4d1zzPxAfpVgzET'}`,
      // 要用到的Gitlab 中的相关代码仓库
      GITLAB_TMP_PRIVATE: `${process.env.GITLAB_TMP_PRIVATE ||
        'template-private'}`,
      GITLAB_WORKLOAD: `${process.env.GITLAB_WORKLOAD || 'workload'}`,
      GITLAB_BRANCH: `${process.env.GITLAB_BRANCH || 'test/v1.1'}`,
      GITLAB_PUBLIC: `${process.env.GITLAB_PUBLIC || 'devops-ui-public'}`,
      GITLAB_PRIVATE: `${process.env.GITLAB_PRIVATE || 'devops-ui-private'}`,

      // DockerRegistry 相关参数
      REGISTRY_ISREADY: `${process.env.REGISTRY_ISREADY || 'true'}`, // registry ready状态是部署好了registry，并且push了镜像 auto-e2erepo/hello-go:
      REGISTRY_NAME: `${process.env.REGISTRY_NAME || 'docker-registry'}`,
      REGISTRY_HOST: `${process.env.REGISTRY_HOST ||
        'http://10.0.128.241:32677'}`,
      REGISTRY_REPO: `${process.env.REGISTRY_REPO || 'auto-e2erepo'}`,

      // Nexus 相关参数
      NEXUS_URL: `${process.env.NEXUS_URL || 'http://10.0.128.241:32010'}`,
      NEXUS_NAME: `${process.env.NEXUS_NAME || 'nexus'}`,
      NEXUS_USER: `${process.env.NEXUS_USER || 'admin'}`,
      NEXUS_PWD: `${process.env.NEXUS_PWD || 'admin'}`,

      // Maven 相关参数
      MAVEN_NAME: `${process.env.MAVEN_NAME || 'maven-public'}`,

      // Harbor 相关参数
      HARBOR_NAME: `${process.env.HARBOR_NAME || 'harbor-staging'}`,
      HARBOR_HTTP: `${process.env.HARBOR_HTTP || 'http://'}`,
      HARBOR_HOST: `${process.env.HARBOR_HOST || '10.0.128.241:31104'}`,
      HARBOR_USER: `${process.env.HARBOR_USER || 'admin'}`,
      HARBOR_PWD: `${process.env.HARBOR_PWD || 'Harbor12345'}`,
      HARBOR_PROJECT: `${process.env.HARBOR_PROJECT || 'e2e-automation'}`,
      HARBOR_REPO: `${process.env.HARBOR_REPO || 'helloworld'}`,
      HARBOR_TAG: `${process.env.HARBOR_TAG || 'latest'}`,

      // Sonar 相关参数
      SONAR_ISREADY: `${process.env.SONAR_ISREADY || 'true'}`,
      SONAR_NAME: `${process.env.SONAR_NAME || 'sonarqube'}`,
      SONAR_HOST: `${process.env.SONAR_HOST || 'http://10.0.128.241:31342'}`,
      SONAR_USER: `${process.env.SONAR_USER || 'admin'}`,
      SONAR_PWD: `${process.env.SONAR_PWD ||
        'd40f4a76180d33521506935f8d3c715702a9d905'}`,
      // 模板仓库相关参数
      HELM_CHART_REPO_URI: `${process.env.HELM_CHART_REPO_URI ||
        'http://7DO9QoLtDx1g:2wb4E1iydRj7@alauda-chart-stable-new.ace-default.cloud.myalauda.cn/'}`,
      SVN_CHART_REPO_URI: `${process.env.SVN_CHART_REPO_URI ||
        'https://svn.apache.org/repos/asf/xml/xang/trunk'}`,
      GIT_CHART_REPO_URI: `${process.env.GIT_CHART_REPO_URI ||
        'https://github.com/alauda/captain-test-charts'}`,

      // devops 通知相关
      DEVOPS_EMAIL_NAME: `${process.env.DEVOPS_EMAIL_NAME ||
        'uiauto-devops-email1'}`,

      // 通知相关
      SERVER_HOST: `${process.env.SERVER_HOST || 'smtp.163.com'}`,
      SERVER_PORT: `${process.env.SERVER_PORT || '25'}`,
      EMAILSENDER_NAME1: `${process.env.EMAILSENDER_NAME1 ||
        'jiaozhang555@163.com'}`,
      EMAILSENDER_PWD1: `${process.env.EMAILSENDER_PWD1 || '123qwe'}`,
      EMAILSENDER_NAME2: `${process.env.EMAILSENDER_NAME2 ||
        'uiauto555@163.com'}`,
      EMAILSENDER_PWD2: `${process.env.EMAILSENDER_PWD2 || '123qwe'}`,
      PROXY: process.env.BROWSER_PROXY
        ? (function(): string {
            const proxy = process.env.BROWSER_PROXY.split(/;|=/).map(item =>
              item.trim(),
            );
            if (
              proxy[proxy.length - 2].startsWith('http') ||
              proxy[proxy.length - 2].startsWith('ssl')
            ) {
              return `http://${proxy[proxy.length - 1]}`;
            } else {
              return null;
            }
          })()
        : null,
    };
}

export const ServerConf = config;
