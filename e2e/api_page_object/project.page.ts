import { CommonHttp } from '@e2e/utility/common.http';
import { CommonMethod } from '@e2e/utility/common.method';

export class ApiProjectPage {
  private _url: string;
  private _templateFile: string;
  constructor() {
    this._url = `/apis/auth.alauda.io/v1/projects/`;
    this._templateFile = 'alauda.project.template.yaml';
  }
  /**
   * 删除项目
   * @param name 项目名称
   */
  async delete(name: string) {
    await CommonHttp.delete(`${this._url}${name}`).then(response => {
      switch (response.status) {
        case 404:
          console.log(`删除项目${name} ${response.statusText}`);
          break;
        case 200:
          console.log(`删除项目${name} ${response.statusText}`);
          break;
        default:
          console.log(response);
          break;
      }
    });
  }

  /**
   * 创建项目
   * @param data 创建项目参数
   */
  async create(
    data = {
      PROJECT_NAME: 'project_name',
      PROJECT_DESCRIPTION: 'UI TEST Project',
      PROJECT_DISPLAYNAME: 'UI TEST Project',
      CLUSTERS: [],
    },
  ) {
    const yaml = CommonMethod.readTemplateFile(this._templateFile, data);
    await CommonHttp.post(this._url, CommonMethod.parseYaml(yaml)).then(
      response => {
        switch (response.status) {
          case 201:
            console.log(`创建项目${data.PROJECT_NAME} ${response.statusText}`);
            break;
          default:
            console.log(response);
            break;
        }
      },
    );
  }

  /**
   * 查询项目
   * @param name 项目名称
   */
  async search(name: string) {
    await CommonHttp.get(`${this._url}${name}`);
  }
}
