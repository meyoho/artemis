import { ApiProjectPage } from './project.page';

export class ApiPage {
  static get project() {
    return new ApiProjectPage();
  }
}
