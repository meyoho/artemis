/**
 * Created by liuwei on 2018/2/28.
 */
import { atob } from 'atob';
import { execSync } from 'child_process';
import { readFileSync, writeFileSync, existsSync } from 'fs';

import { ServerConf } from '../config/serverConf';

export class CommonMethod {
  static isClearTestData = true;
  /**
   * 比较两个字符串变量的大小
   * stringObject < target 返回 1
   * stringObject > target 返回 -1
   *
   * @parameter {stringObject} string 变量
   * @parameter {target} string 量
   */
  static stringDown(stringObject, target) {
    return stringObject.localeCompare(target) < 0 ? 1 : -1;
  }

  /**
   * 读取YAML（{fileName}）文件，返回文件内容
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {parameter} 键值对格式，键写在YAML文件中 例如： { '$NAME': 20, '$PASSWORD': 30 }
   *
   * @example  readyamlfile('../test_data/configmap.yaml', {'$NAME':'liuweiconfigmap'});
   * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
   *
   */
  static readyamlfile(fileName, parameter: { [key: string]: string } = {}) {
    let data = readFileSync(ServerConf.TESTDATAPATH + fileName, 'utf8');
    for (const key of Object.keys(parameter)) {
      while (data.includes(key)) {
        data = data.replace(key, parameter[key]);
      }
      data = data.replace(key, parameter[key]);
    }
    return data;
  }
  /**
   * 读取模板（{fileName}）文件，返回文件内容
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {parameter} 键值对格式，键写在模板文件中 例如： { 'NAME': 20, 'PASSWORD': 30 }
   *
   * @example  readyamlfile('../test_data/configmap.yaml', {'NAME':'liuweiconfigmap'});
   * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
   *
   */
  static readTemplateFile(fileName, parameter: Record<string, any> = {}) {
    const template = require('art-template');

    template.defaults.imports.base64Encode = function(str: string) {
      const base64 = require('js-base64').Base64;
      return base64.encode(str);
    };
    template.defaults.imports.lower = function(str: string) {
      return str.toLowerCase();
    };
    template.defaults.imports.upper = function(str: string) {
      return str.toUpperCase();
    };
    template.defaults.imports.getEnv = function(str, default_value) {
      return process.env[str] ? process.env[str] : default_value;
    };
    template.defaults.imports.indent = function(str: string, size: number) {
      return (
        str
          .split('\n')
          .map((line, index) => {
            let blank = '';
            if (index > 0) {
              for (let i = 0; i < size; i++) {
                blank = blank + ' ';
              }
            }
            return blank + line;
          })
          .reduce((content, line) => {
            return content + '\n' + line;
          }) + '\n'
      );
    };
    console.log(template(ServerConf.TESTDATAPATH + fileName, parameter));
    return template(ServerConf.TESTDATAPATH + fileName, parameter);
  }
  /**
   * 将 {value} 值写入{fileName} 文件，返回文件的完整名
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {value} string 类型, 要写入 {fileName 的文件内容
   *
   * @returns  string 类型，返回文件的完整路径
   * @example writeyamlfile('temp.yaml', 'yaml 格式的文件字符串内容')
   */
  static writeyamlfile(fileName, value) {
    writeFileSync(ServerConf.TESTDATAPATH + 'temp/' + fileName, value);
    return ServerConf.TESTDATAPATH + 'temp/' + fileName;
  }

  /**
   * 命令行执行任意一个命令
   *
   * @parameter {cmd} string 类型, 命令， 例如： kubectl get service -n liuweinamespace
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example execCommand('kubectl get configmap');
   */
  static execCommand(cmd): string {
    try {
      return String(execSync(cmd));
    } catch (ex) {
      return ex.toString();
    }
  }
  /**
   * 通过python 登陆获取token
   * @parameter user 用户名
   * @parameter password 密码
   * @parameter auth 登陆方式 支持ldap和local
   * @returns token
   */
  static loginGetToken(
    user = ServerConf.ADMIN_USER,
    password = ServerConf.ADMIN_PASSWORD,
    auth = 'local',
  ) {
    const cmd = `python3 ./scripts/loginGetToken.py --base_url ${
      ServerConf.BASE_URL
    } --user ${user} --password ${password} --auth=${auth} ${
      ServerConf.PROXY ? `--proxy ${ServerConf.PROXY}` : ''
    }`;
    console.log(cmd);
    const token = CommonMethod.execCommand(cmd);
    if (token.includes('Error')) {
      return '';
    } else {
      return token.replace(/(\r\n)|\n$/, '');
    }
  }
  /**
   * 程序等待 milliSeconds
   *
   * @parameter {milliSeconds} int 类型, 命令.
   *
   * @returns  string 类型，命令执行后在屏幕上输出的内容
   * @example sleep(1000);
   */
  static sleep(milliSeconds) {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + milliSeconds) {
      if (milliSeconds % 10000 === 0) {
        console.log('');
      }
    }
  }

  /**
   * 解析yaml
   * @param yamlValue, string 类型，从yaml读出的值
   * @return {any}
   */
  static parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  /**
   * 将yaml 对象保存到文件中
   * @param filename 保存的文件名
   * @param yaml yaml 对象
   */
  static dumpYaml(filename: string, yaml: any) {
    const jsyaml = require('js-yaml');
    try {
      writeFileSync(filename, jsyaml.dump(yaml), 'utf8');
    } catch (e) {
      console.log(e);
    }
  }

  /**
   * 生成测试数据
   */
  static random_generate_testData(prefix = '') {
    // CommonMethod.clearTestData();
    CommonMethod.sleep(1);
    const date = new Date();
    const temp =
      date.getFullYear() +
      date.getMonth() +
      date.getDay() +
      date.getHours() +
      date.getMinutes() +
      date.getSeconds() +
      date.getMilliseconds();

    return 'artemis-auto-' + prefix + String(temp);
  }

  /**
   * 清理由于异常中断引起的脏数据
   */
  static clearTestData() {
    if (CommonMethod.isClearTestData) {
      const namespacelist = CommonMethod.parseYaml(
        CommonMethod.execCommand('kubectl get namespaces -o yaml'),
      );
      for (const iterator of namespacelist.items) {
        const name = iterator.metadata.name;
        if (name.includes('diablo-auto-')) {
          const patt1 = new RegExp('^[0-9]*$');
          if (patt1.test(name.replace('diablo-auto-', ''))) {
            console.log(
              CommonMethod.execCommand(`kubectl delete namespace ${name}`),
            );
          }
        }
      }
    }
    CommonMethod.isClearTestData = false;
  }

  /**
   * base64 解码
   * @param base64Str base64 字符串
   */
  static base64Decode(base64Str: string): string {
    return atob(base64Str);
  }

  static base64Encode(str: string): string {
    const base64 = require('js-base64').Base64;
    return base64.encode(str);
  }

  /**
   * 判断文件是否存在
   * @param filename 文件名
   */
  static isExistFile(filename: string): boolean {
    return existsSync(`${ServerConf.TESTDATAPATH}temp/${filename}`);
  }
}
