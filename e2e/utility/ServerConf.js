class ServerConf {
  constructor() {
    switch (process.env.ENV) {
      case 'int':
        this.TESTIMAGE = 'index.alauda.cn/alaudaorg/qaimages:helloworld';
        this.GLOBAL_MASTER_IP = '192.168.16.41';
        this.GLOBAL_NAMESPCE = 'cpaas-system';
        this.LABELBASEDOMAIN = 'cpaas.io';
        this.USER_TOKEN =
          'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLWxkem5tIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIzMGYyMDAwNC02MmQxLTExZWEtODI1OC01MjU0MDBjM2U0MDEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.YMTe744f47IbstBAJ9QFtZmazYhJ4PKm1-SqUuZCIvwRQ7T0XGJRSBKEvdyn__-RnXcxtHxYyXm0lPHini72nte-gA8SrEEynWnoHj4FwWxbMU4cUCZwGRb2RPfjiaAv3BdnAG0Xx2nVy6xVHTgxF7BWcDuXxKu0exUhSn0MK-3bC3xSJybbaFa6TjXtpX7769kkUfL8xNEIywrRd50WAqpUnQD-XVsuk6AKFhxqP-mGX7GLI5-oTa2ylox0VZ0n3FBMNRsZX79ddFEgk30n0Tkxd6uvMKrgTpsvbuKACrS_lJHm3mc13GkrmusmDbRmm74iJpRR1be-vagWOLU1qQ';
        this.BUSINESS_MASTER_IP = '192.168.17.14'; // 如果不配置，使用内网连busines 集群
        this.REGIONNAME = 'ovn';
        this.CALICOREGIONNAME = 'calico';
        this.CALICO_IP = '192.168.17.1';
        this.OVNREGIONNAME = 'ovn';
        this.OVN_IP = '192.168.17.14';
        this.JENKINS_NAME = 'jenkins-ovn';
        this.JENKINS_HOST = 'http://192.168.17.14:32001';
        this.JENKINS_USER = 'admin';
        this.JENKINS_PWD = '1c2b0c5311088ce77060867af0b57cac1';
        this.GITLAB_NAME = 'gitlab-staging';
        this.GITLAB_URL = 'http://10.0.128.241:31101';
        this.GITLAB_USER = 'root';
        this.GITLAB_TOKEN = 'tzrgg4d1zzPxAfpVgzET';
        this.NEXUS_URL = 'http://10.0.128.241:32010';
        this.NEXUS_NAME = 'nexus';
        this.NEXUS_USER = 'admin';
        this.NEXUS_PWD = 'admin';
        this.MAVEN_NAME = 'maven-public';
        this.GITLAB_WORKLOAD = 'workload';
        this.HARBOR_NAME = 'prod-harbor';
        this.HARBOR_HTTP = 'https://';
        this.HARBOR_HOST = '10.0.128.241:31104';
        this.HARBOR_USER = 'admin';
        this.HARBOR_PWD = 'Harbor12345';
        this.SERVER_HOST = 'smtp.163.com';
        this.SERVER_PORT = '25';
        this.EMAILSENDER_NAME2 = 'uiauto555@163.com';
        this.EMAILSENDER_PWD2 = '123qwe';
        this.ADMIN_USER = 'admin@cpaas.io';
        this.DEVOPS_EMAIL_NAME = 'uiauto-devops-email1';
        break;
      case 'staging':
        this.TESTIMAGE = 'index.alauda.cn/alaudaorg/qaimages:helloworld';
        this.GLOBAL_MASTER_IP = '94.191.76.27';
        this.GLOBAL_NAMESPCE = 'alauda-system';
        this.LABELBASEDOMAIN = 'alauda.io';
        this.USER_TOKEN =
          'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLTh0dHFsIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJhNTZmZDY0Yi05M2M2LTExZTktOGFhNC01MjU0MDBkNjMyNjUiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.BnxD9q905PubwE0BNutmFx2Peoa-qtaOhGqAVuAKvT4IA4vgPbXoWPIDRzpVP573xe84y1rHyEsPOMwlmjTYi0xUmW3HgyIHeoodLtGxtlje1WtbHuHS_ZnWtU26aENipAlzfJllEIcKJspWJS53vNm6xjW3VkJoLuPA45kU12-mmoQGxY-p4SeFF1MQsJX2F9cjmjR27KQ--BCKXJ9QQsI6ARnj11QzKKaJY71x89J0_nBk7aVBFQMbXOvDJ9_a3eTQQAgU4X-m35c3HcrRYW0mPSafGYr4V75KvDQfJXqHEGL_zt4t1Zw7q7s7eqMgvENV-6p5EPxwY6SV3563bg';
        this.BUSINESS_MASTER_IP = '129.28.187.165'; // 如果不配置，使用内网连busines 集群
        this.REGIONNAME = 'high';
        this.CALICOREGIONNAME = 'cls-w5xrsf62';
        this.CALICO_IP = '94.191.86.189';
        this.OVNREGIONNAME = 'ovn';
        this.OVN_IP = '129.28.166.59';
        this.JENKINS_NAME = 'local-ares-jenkins';
        this.JENKINS_HOST = 'http://10.0.128.241:32001';
        this.JENKINS_USER = 'admin';
        this.JENKINS_PWD = '116a236e2354969f00a83fa26c5e5db565';
        this.GITLAB_NAME = 'gitlab-staging';
        this.GITLAB_URL = 'http://10.0.128.241:31101';
        this.GITLAB_USER = 'root';
        this.GITLAB_TOKEN = 'tzrgg4d1zzPxAfpVgzET';
        this.NEXUS_URL = 'http://10.0.128.241:32010';
        this.NEXUS_NAME = 'nexus';
        this.NEXUS_USER = 'admin';
        this.NEXUS_PWD = 'admin';
        this.MAVEN_NAME = 'maven-public';
        this.GITLAB_WORKLOAD = 'workload';
        this.HARBOR_NAME = 'harbor-staging';
        this.HARBOR_HTTP = 'http://';
        this.HARBOR_HOST = '10.0.128.241:31104';
        this.HARBOR_USER = 'admin';
        this.HARBOR_PWD = 'Harbor12345';
        this.SERVER_HOST = 'smtp.163.com';
        this.SERVER_PORT = '25';
        this.EMAILSENDER_NAME2 = 'uiauto555@163.com';
        this.EMAILSENDER_PWD2 = '123qwe';
        this.ADMIN_USER = 'admin@alauda.io';
        this.DEVOPS_EMAIL_NAME = 'uiauto-devops-email1';
        break;
      default:
        this.TESTIMAGE = process.env.TESTIMAGE;
        this.GLOBAL_MASTER_IP = process.env.GLOBAL_MASTER_IP;
        this.GLOBAL_NAMESPCE = process.env.GLOBAL_NAMESPCE;
        this.LABELBASEDOMAIN = process.env.LABELBASEDOMAIN;
        this.USER_TOKEN = process.env.USER_TOKEN;
        this.BUSINESS_MASTER_IP = process.env.BUSINESS_MASTER_IP;
        this.REGIONNAME = process.env.REGIONNAME;
        this.CALICOREGIONNAME = process.env.CALICOREGIONNAME;
        this.CALICO_IP = process.env.CALICO_IP;
        this.OVNREGIONNAME = process.env.OVNREGIONNAME;
        this.OVN_IP = process.env.OVN_IP;

        this.JENKINS_NAME = process.env.JENKINS_NAME;
        this.JENKINS_HOST = process.env.JENKINS_HOST;
        this.JENKINS_USER = process.env.JENKINS_USER;
        this.JENKINS_PWD = process.env.JENKINS_PWD;
        this.GITLAB_NAME = process.env.GITLAB_NAME;
        this.GITLAB_URL = process.env.GITLAB_URL;
        this.GITLAB_USER = process.env.GITLAB_USER;
        this.GITLAB_TOKEN = process.env.GITLAB_TOKEN;
        this.NEXUS_URL = process.env.NEXUS_URL;
        this.NEXUS_NAME = process.env.NEXUS_NAME;
        this.NEXUS_USER = process.env.NEXUS_USER;
        this.NEXUS_PWD = process.env.NEXUS_PWD;
        this.MAVEN_NAME = process.env.MAVEN_NAME;
        this.GITLAB_WORKLOAD = process.env.GITLAB_WORKLOAD;
        this.HARBOR_NAME = process.env.HARBOR_NAME;
        this.HARBOR_HTTP = process.env.HARBOR_HTTP;
        this.HARBOR_HOST = process.env.HARBOR_HOST;
        this.HARBOR_USER = process.env.HARBOR_USER;
        this.HARBOR_PWD = process.env.HARBOR_PWD;
        this.SERVER_HOST = process.env.SERVER_HOST;
        this.SERVER_PORT = process.env.SERVER_PORT;
        this.EMAILSENDER_NAME2 = process.env.EMAILSENDER_NAME2;
        this.EMAILSENDER_PWD2 = process.env.EMAILSENDER_PWD2;
        this.ADMIN_USER = process.env.ADMIN_USER;
        break;
    }
  }
}
module.exports = {
  // 通用
  ServerConf: new ServerConf(),
};
