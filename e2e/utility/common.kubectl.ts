/**
 * 主要封装一些kubectl 的操作
 * Created by liuwei on 2018/3/2.
 */

import { CommonMethod } from './common.method';
import { existsSync, unlinkSync } from 'fs';

export class CommonKubectl {
  private static _yamlFileName;

  /**
   * global 集群的名称
   */
  static get globalClusterName(): string {
    return 'global';
  }

  static get yamlFileName() {
    return CommonKubectl._yamlFileName;
  }

  static set yamlFileName(value) {
    CommonKubectl._yamlFileName = value;
  }

  /**
   * 根据YAML 模版创建资源
   * @param filename YALM 模版文件名称
   * @param parameter YAML模版里要替换的的参数， eg. {'${NAME}': project_name1, '${PROJECT_NAME}': project_name1}
   * @param testscriptfilename YAML模版替换变量后保存在testdata/tmp 文件夹下面的YAML 文件，可以用来debug
   * @param clusterName 集群名称，指定在那个集群创建资源，不传clusterName 默认在global 集群上创建资源
   * @returns YAML模版替换变量后保存在testdata/tmp 文件名称，可以用来删除资源
   */
  static createResource(
    filename,
    parameter,
    testscriptfilename,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML
    const yaml = CommonMethod.readyamlfile(filename, parameter);
    // console.log(yaml);
    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    const tempfilename = CommonMethod.writeyamlfile(
      `${testscriptfilename}.yaml`,
      yaml,
    );

    try {
      // 执行创建命令
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl apply -f ${tempfilename} --validate=false`,
          clusterName,
        ),
      );
    } catch (ex) {
      console.log(ex.message);
    }

    return tempfilename;
  }
  /**
   * 根据art-template YAML 模版创建资源
   * @param filename art-template YALM 模版文件名称
   * @param parameter YAML模版里要替换的的参数， eg. {'proname': project_name1, 'ns_name': ns_name1}
   * @param testscriptfilename YAML模版替换变量后保存在testdata/tmp 文件夹下面的YAML 文件，可以用来debug
   * @param clusterName 集群名称，指定在那个集群创建资源，不传clusterName 默认在global 集群上创建资源
   * @returns YAML模版替换变量后保存在testdata/tmp 文件名称，可以用来删除资源
   */
  static createResourceByTemplate(
    filename,
    parameter,
    testscriptfilename,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML

    const yaml = CommonMethod.readTemplateFile(filename, parameter);
    // console.log(yaml);
    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    const tempfilename = CommonMethod.writeyamlfile(
      `${testscriptfilename}.yaml`,
      yaml,
    );

    try {
      // 执行创建命令
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl create -f ${tempfilename} --validate=false`,
          clusterName,
        ),
      );
    } catch (ex) {
      console.log(ex.message);
    }

    return tempfilename;
  }
  /**
   * 根据art-template YAML 模版删除资源
   * @param filename art-template YALM 模版文件名称
   * @param parameter YAML模版里要替换的的参数， eg. {'proname': project_name1, 'ns_name': ns_name1}
   * @param testscriptfilename YAML模版替换变量后保存在testdata/tmp 文件夹下面的YAML 文件，可以用来debug
   * @param clusterName 集群名称，指定在那个集群创建资源，不传clusterName 默认在global 集群上创建资源
   * @returns YAML模版替换变量后保存在testdata/tmp 文件名称，可以用来删除资源
   */
  static deleteResourceByTemplate(
    filename,
    parameter,
    testscriptfilename,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML

    const yaml = CommonMethod.readTemplateFile(filename, parameter);
    // console.log(yaml);
    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    const tempfilename = CommonMethod.writeyamlfile(
      `${testscriptfilename}.yaml`,
      yaml,
    );

    try {
      // 执行创建命令
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl delete -f ${tempfilename}`,
          clusterName,
        ),
      );
    } catch (ex) {
      console.log(ex.message);
    }

    return tempfilename;
  }
  /**
   * 根据YAML 文件删除资源
   * @param yamlFileName yaml 文件，kubectl delete -f 需要
   * @param clusterName 指定在那个集群上删除资源， 不传clusterName 默认在global 集群上删除资源
   */
  static deleteResourceByYmal(
    yamlFileName = CommonKubectl.yamlFileName,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    try {
      return CommonKubectl.execKubectlCommand(
        `kubectl delete -f ${yamlFileName}`,
        clusterName,
      );
    } catch (ex) {
      console.log(ex.message);
      return ex.message;
    }
  }

  /**
   * 根据资源类型，名称，删除一个资源
   * @param kind 资源类型
   * @param name 资源名称
   * @param clusterName 指定在那个集群删除资源，不传clusterName 值默认在global 集群删除资源
   * @example  deleteResource('configmap'， 'qqqqqq')
   */
  static deleteResource(
    kind,
    name,
    clusterName: string = CommonKubectl.globalClusterName,
    namespace = '',
  ): string {
    try {
      if (namespace !== '') {
        return CommonKubectl.execKubectlCommand(
          `kubectl delete ${kind} ${name} -n ${namespace}  --timeout=5s`,
          clusterName,
        );
      } else {
        return CommonKubectl.execKubectlCommand(
          `kubectl delete ${kind} ${name}  --timeout=5s`,
          clusterName,
        );
      }
    } catch (ex) {
      console.log(ex.message);
      return ex.message;
    }
  }

  /**
   * 执行一个通用的 kubectl 命令
   * @param cmd kubectl 命令
   * @param clusterName 指定在那个集群上执行命令， 不传clusterName 值默认在global 集群上执行
   * @example execKubectlCommand('kubectl get node')
   */
  static execKubectlCommand(
    cmd: string,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    if (existsSync('/root/.kube/config.lock')) {
      unlinkSync('/root/.kube/config.lock');
    }
    if (cmd.startsWith('kubectl')) {
      //cmd = `kubectl --context ${clusterName} ${cmd.substring(7, cmd.length)}`;
      cmd = cmd.replace(/kubectl/gi, `kubectl --context ${clusterName}`);
    }
    console.log(cmd);
    return CommonMethod.execCommand(cmd);
  }

  /**
   * 获取资源，并且返回 yaml 解析后的结果
   * @param kind 资源类型
   * @param namespace 命名空间
   * @param name 资源名称
   * @param clusterName 指定在那个集群上执行命令
   */
  static getResourceParseYaml(
    kind: string,
    namespace: string,
    name: string,
    clusterName: string = CommonKubectl.globalClusterName,
  ) {
    return CommonMethod.parseYaml(
      CommonKubectl.execKubectlCommand(
        `kubectl get ${kind} -n ${namespace} ${name} -o yaml`,
        clusterName,
      ),
    );
  }

  /**
   * devops 那边用的，判断项目时候绑定jenkins, harbor, gitlab,
   * 资源的host 的值如果等于传入的值，证明已经集成成功了，否则返回false, case 里面需要集成jenkins, harbor, gitlab
   * @param kind 资源类型
   * @param host jenkins, harbor, gitlab 的值， eg, http://62.234.104.184:31101
   */
  static getServiceNameByHost(
    kind: string,
    host: string,
    clusterName: string = CommonKubectl.globalClusterName,
  ): string {
    //切换集群

    // 执行kubectl 命令
    const items = CommonMethod.parseYaml(
      CommonKubectl.execKubectlCommand(
        `kubectl get ${kind} -o yaml`,
        clusterName,
      ),
    ).items;
    for (const item of items) {
      if (item.spec.http.host === host) {
        return item.metadata.name;
      }
      //   if (item.hasOwnProperty('spec')) {
      //     if (item.spec.hasOwnProperty('http')) {
      //       if (item.spec.http.hasOwnProperty('host')) {
      //         if (item.spec.http.host === host) {
      //           return item.metadata.name;
      //         }
      //       }
      //     }
      //   }
    }
    return 'false';
  }

  /**
   * devops 那边用的，判断镜像仓库是否同步成功
   * @param kind 资源类型
   * @param namespace 命名空间
   * @param name 资源名称
   * @param clusterName 在那个集群上执行
   */
  private static _isReadyRepositorySync(
    kind: string,
    namespace: string,
    name: string,
    clusterName: string = CommonKubectl.globalClusterName,
  ) {
    try {
      const repo = CommonMethod.parseYaml(
        CommonKubectl.execKubectlCommand(
          `kubectl get ${kind} -n ${namespace} ${name} -o yaml`,
          clusterName,
        ),
      );
      if (repo.status.phase !== 'Ready') {
        // console.log('Repository status is ' + repo.status.phase);
        return false;
      }
      return true;
    } catch {
      return false;
    }
  }

  /**
   * devops 那边用的，等待资源处于Ready 状态
   * @param kind 资源类型
   * @param namespace 命名空间
   * @param name 资源名称
   * @param timeout 超时时间
   * @param clusterName 在那个集群上执行
   */
  static waitRepository(
    kind: string,
    namespace: string,
    name: string,
    timeout = 120,
    clusterName: string = CommonKubectl.globalClusterName,
  ) {
    let isReady = this._isReadyRepositorySync(kind, namespace, name);
    let loop = 1;
    while (!isReady) {
      CommonMethod.sleep(5000);
      isReady = this._isReadyRepositorySync(kind, namespace, name, clusterName);
      if (loop++ > timeout) {
        break;
      }
    }
    return isReady;
  }
}
