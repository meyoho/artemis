// const { browser, $, element, by } = require('protractor');
const CommonKubectl = require('./kubectl.common').KubectlCommon;

class TestData {
  parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  /**
   * 根据类型资源清理资源
   * @param {string} 资源类型名称
   */
  clearTestData(resourceType, timeout, isNamespaced) {
    // 获取所有资源
    let command = `kubectl get ${resourceType} -o yaml`;
    if (isNamespaced) {
      command += ' --all-namespaces';
    }
    try {
      const namespacelist = this.parseYaml(
        CommonKubectl.execKubectlCommand(command, 'global'),
      );

      for (const iterator of namespacelist.items) {
        const name = iterator.metadata.name;
        const creationTimestamp = iterator.metadata.creationTimestamp.getTime();
        let namespace = '';
        if (isNamespaced) {
          namespace = iterator.metadata.namespace;
        }
        let timeDiff = parseInt(new Date().getTime() - creationTimestamp);
        // 获得资源存在了多长时间
        timeDiff = parseInt(timeDiff / (1000 * 60));

        if (
          name.includes('pr-') ||
          name.includes('platform-local-') ||
          name.includes('mastere2eproject') ||
          name.includes('acp-local') ||
          name.includes('locale2eproject-') ||
          name.includes('artemis-auto-') ||
          name.includes('artemis-') ||
          name.includes('e2enamespace')
        ) {
          try {
            if (timeDiff > timeout) {
              // 如果超过了3小时， 删除
              console.log(
                `脏数据 ${resourceType}【${iterator.metadata.name}】 存在超过了90分钟，自动删除了`,
              );
              let command = `kubectl delete ${resourceType} ${name}`;
              if (isNamespaced) {
                command += ` -n ${namespace}`;
              }
              CommonKubectl.execKubectlCommand(command, 'global');
            }
          } catch (error) {
            console.error('删除数据发生错误:' + error);
          }
        }
      }
    } catch {
      console.log(`查询 ${resourceType} 失败了`);
      // console.error(CommonKubectl.execKubectlCommand(command, 'global'));
    }
  }

  clear() {
    // this.clearTestData('projects.auth.alauda.io', 120, false);
    // this.clearTestData('namespaces', 120, false);
    // this.clearTestData('daemonset', 120, false);
    // this.clearTestData('deployment', 120, false);
    // this.clearTestData('CodeRepoBinding', 120, true);
    // this.clearTestData('JenkinsBinding', 120, true);
    // this.clearTestData('ImageRegistryBinding', 120, true);
    // this.clearTestData('Domain', 120, false);
    // this.clearTestData('secret', 120, true);
    // 资源不够，不支持并行跑流水线
    // this.clearTestData('MicroservicesEnvironment', 1, false);
  }
}
module.exports = {
  // 通用
  TestData: new TestData(),
};
