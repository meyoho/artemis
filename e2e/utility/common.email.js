//const { execSync } = require('child_process');
// const fs = require('fs');
// let join = require('path').join;
//const nodemailer = require('nodemailer');

const axios = require('axios');

function warningRobotSend(message, mentioned_list) {
  const data = {
    msgtype: 'text',
    text: { content: message, mentioned_mobile_list: mentioned_list },
  };
  const webhookUrl =
    'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=e81b5624-d29b-431d-8a44-e7945479c565';

  // const params = {
  //   access_token: process.env.DING_DING_ACCESS_TOKEN,
  // };
  // axios.post(dingdingUrl, payload, { params }).then(console.log);
  return axios.post(webhookUrl, data);
}
// class Email {
//   get testReportPath() {
//     return process.cwd() + '/e2e-reports/';
//   }

//   execSync(cmd) {
//     return execSync(cmd);
//   }

//   send(emailTitle) {
//     console.log('======> Email 发送了');
//     const resultList = JSON.parse(
//       fs.readFileSync(this.testReportPath + 'totalResult.txt', 'utf-8'),
//     );
//     let emailBody = `<table border="1" bordercolor="#000000" cellspacing="0" cellpadding="2" style="border-collapse:collapse;">
//       <tr style="background-color: #24a7e3; color:white;font-size:14">
//       <td>name</td>
//       <td>count</td>
//       <td>success</td>
//       <td>error</td>
//       <td>caseError</td>
//       <td>avgTime</td>
//       <td>minTime</td>
//       <td>maxTime</td></tr>`;
//     resultList.forEach(result => {
//       emailBody =
//         emailBody +
//         `
//         <tr style="font-size:12">
//         <td>${result.name}</td>
//         <td>${result.count}</td>
//         <td>${result.success}</td>
//         <td>${result.error}</td>
//         <td>${result.caseError}</td>
//         <td>${result.avgTime}</td>
//         <td>${result.minTime}</td>
//         <td>${result.maxTime}</td></tr>`;
//     });
//     emailBody = emailBody + '</table>';

//     fs.writeFileSync(
//       process.cwd() + '/e2e-reports/totalReport.html',
//       `<html><body>${emailBody}</body></html>`,
//     );

//     this.execSync(`tar -zcvf testReport.tar.gz e2e-reports`);

//     console.log(
//       '\n\n*********************Email parameter *******************************',
//     );
//     console.log(`*             EMAIL_HOST: ${process.env.EMAIL_HOST}`);
//     console.log(`*             EMAIL_PORT: ${process.env.EMAIL_PORT}`);
//     console.log(`*             EMAIL_USER: ${process.env.EMAIL_USER}`);
//     console.log(`*         EMAIL_PASSWORD: ${process.env.EMAIL_PASSWORD}`);
//     console.log(`*             EMAIL_FROM: ${process.env.EMAIL_FROM}`);
//     console.log(`*               EMAIL_TO: ${process.env.EMAIL_TO}`);
//     console.log(
//       '************************************************************************\n\n',
//     );

//     const transporter = nodemailer.createTransport({
//       host: process.env.EMAIL_HOST, // 'smtpdm.aliyun.com',
//       port: process.env.EMAIL_PORT,
//       secure: true,
//       auth: {
//         user: process.env.EMAIL_USER, // 'info@alauda.cn',
//         pass: process.env.EMAIL_PASSWORD, // 'MATHilde123',
//       },
//       logger: true,
//       debug: true,
//     });

//     const mailOptions = {
//       from: process.env.EMAIL_FROM, // 'info@alauda.cn', // sender address
//       to: process.env.EMAIL_TO, // list of receivers
//       subject: emailTitle, // Subject line

//       html: emailBody,
//       attachments: [
//         {
//           path: process.cwd() + '/testReport.tar.gz',
//         },
//       ],
//     };

//     return transporter.sendMail(mailOptions, function(error, info) {
//       if (error) {
//         return console.log(error);
//       }
//       console.log('Mail sent successfully : ' + info.response);
//     });
//   }
// }

// const email = new Email();
// email.send('发行版稳定性测试报告');

module.exports = {
  warningRobot: warningRobotSend,
};
