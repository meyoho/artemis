const { execSync } = require('child_process');
const fs = require('fs');
const path = require('path');

const ServerConf = require('./ServerConf').ServerConf;

class KubectlCommon {
  get testdataPath() {
    return process.cwd() + '/e2e/test_data';
  }

  constructor() {
    this.GLOBAL_MASTER_IP = ServerConf.GLOBAL_MASTER_IP;
    this.GLOBAL_NAMESPCE = ServerConf.GLOBAL_NAMESPCE;
    this.USER_TOKEN = ServerConf.USER_TOKEN;
    this.BUSINESS_MASTER_IP = ServerConf.BUSINESS_MASTER_IP;
    this.REGIONNAME = ServerConf.REGIONNAME;

    // this.switchGlobal();
    // this.initKubectlContext(this.REGIONNAME, this.BUSINESS_MASTER_IP);
  }

  parseYaml(yamlValue) {
    const yaml = require('js-yaml');
    try {
      return yaml.safeLoad(yamlValue);
    } catch (error) {
      return error;
    }
  }

  readyamlfile(fileName, parameter) {
    let data = fs
      .readFileSync(path.join(this.testdataPath, fileName), 'utf8')
      .toString();
    for (const key of Object.keys(parameter)) {
      while (data.includes(key)) {
        data = data.replace(key, parameter[key]);
      }
      data = data.replace(key, parameter[key]);
    }
    return data;
  }
  /**
   * 读取模板（{fileName}）文件，返回文件内容
   *
   * @parameter {fileName} string 类型, 文件名称
   * @parameter {parameter} 键值对格式，键写在模板文件中 例如： { 'NAME': 20, 'PASSWORD': 30 }
   *
   * @example  readyamlfile('../test_data/configmap.yaml', {'NAME':'liuweiconfigmap'});
   * @returns  string 类型，返回YAML 文件内容，该内容包含的所有键值被对应的值替换
   *
   */
  readTemplateFile(fileName, parameter) {
    const template = require('art-template');
    template.defaults.imports.base64Encode = function(str) {
      const base64 = require('js-base64').Base64;
      return base64.encode(str);
    };
    template.defaults.imports.upper = function(str) {
      return str.toUpperCase();
    };
    template.defaults.imports.lower = function(str) {
      return str.toLowerCase();
    };
    template.defaults.imports.getEnv = function(str, default_value) {
      return process.env[str] ? process.env[str] : default_value;
    };
    template.defaults.imports.indent = function(str, size) {
      return (
        str
          .split('\n')
          .map((line, index) => {
            let blank = '';
            if (index > 0) {
              for (let i = 0; i < size; i++) {
                blank = blank + ' ';
              }
            }
            return blank + line;
          })
          .reduce((content, line) => {
            return content + '\n' + line;
          }) + '\n'
      );
    };
    console.log(template(path.join(this.testdataPath, fileName), parameter));
    return template(path.join(this.testdataPath, fileName), parameter);
  }
  createResource(filename, parameter, testscriptfilename, clusterName) {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML
    const yaml = this.readyamlfile(filename, parameter);
    // console.log(yaml);
    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    fs.writeFileSync(
      `${this.testdataPath}/temp/${testscriptfilename}.yaml`,
      yaml,
    );

    try {
      // 执行创建命令
      console.log(
        this.execKubectlCommand(
          `kubectl apply -f ${this.testdataPath}/temp/${testscriptfilename}.yaml --validate=false`,
          clusterName,
        ),
      );
    } catch (ex) {
      console.log(ex.message);
    }
  }
  createResourceByTemplate(
    filename,
    parameter,
    testscriptfilename,
    clusterName,
  ) {
    // 根据YAML 文件的模版生成一个字符串格式的 YAML

    const yaml = this.readTemplateFile(filename, parameter);
    // console.log(yaml);
    // 将生成的生成一个字符串格式的 YAML 写入到临时文件
    fs.writeFileSync(
      `${this.testdataPath}/temp/${testscriptfilename}.yaml`,
      yaml,
    );

    try {
      // 执行创建命令
      console.log(
        this.execKubectlCommand(
          `kubectl apply -f ${this.testdataPath}/temp/${testscriptfilename}.yaml --validate=false`,
          clusterName,
        ),
      );
    } catch (ex) {
      console.log(ex.message);
    }
  }
  /**
   * 切换kubectl context
   * @param {*} name context 命名
   * @param {*} serverAddress api Server 地址
   * @param {*} toke 访问集群token
   */
  switchCluster(name, serverAddress, toke) {
    execSync(`rm -f  $HOME/.kube/config.lock`);
    execSync(`./scripts/kubectlConfig.sh ${name} ${serverAddress} ${toke}`);
    try {
      execSync('kubectl get ns default');
    } catch (ex) {
      console.log(`切换集群 ${serverAddress} 失败，kubectl 命令无法使用`);
      console.log(ex.message);
    }
  }

  /**
   * 切换context 到global 集群
   */
  switchGlobal() {
    execSync(`rm -f  $HOME/.kube/config.lock`);
    this.switchCluster(
      'global',
      `https://${this.GLOBAL_MASTER_IP}:6443`,
      this.USER_TOKEN,
    );
  }

  /**
   * 初始化 kubectl context
   * @param {*} clusterName 集群名称
   * @param {*} apiServer_ip 集群对应 api server IP
   */
  initKubectlContext(clusterName, apiServer_ip) {
    // 获取访问业务集群的保密字典
    // console.log(
    //   `kubectl --context global get clusters.clusterregistry.k8s.io -n ${this.GLOBAL_NAMESPCE} ${clusterName} -o yaml`,
    // );
    const clusterSecret = this.parseYaml(
      execSync(
        `kubectl --context global get clusters.clusterregistry.k8s.io -n ${this.GLOBAL_NAMESPCE} ${clusterName} -o yaml`,
      ),
    );

    // 获取访问集群的serverAddress
    let serverAddress;
    if (apiServer_ip === undefined || apiServer_ip === 'false') {
      serverAddress =
        clusterSecret.spec.kubernetesApiEndpoints.serverEndpoints[0]
          .serverAddress;
    } else {
      serverAddress = `https://${apiServer_ip}:6443`;
    }

    // 获取访问集群的Token
    const token = execSync(
      `kubectl --context global get secret -n ${this.GLOBAL_NAMESPCE} ${clusterSecret.spec.authInfo.controller.name} -o=custom-columns='':.data.token | base64 --decode`,
    );
    // console.log(
    //   `kubectl --context global get secret -n ${this.GLOBAL_NAMESPCE} ${clusterSecret.spec.authInfo.controller.name} -o yaml | grep token | awk '{print $2}' | base64 --decode`,
    // );

    try {
      console.log(
        `\n==============初始化 ${clusterName} 集群 kubectl Context=========`,
      );
      console.log(`serverAddress: ${serverAddress}`);
      console.log(`token: ${token}`);
      console.log(`=======================================================\n`);
      this.switchCluster(clusterName, serverAddress, token);
    } catch (ex) {
      console.log('ERROR: ' + ex.message);
    }
  }

  execKubectlCommand(cmd, clusterName) {
    try {
      cmd = cmd.replace(/kubectl/gi, `kubectl --context ${clusterName}`);
      return execSync(cmd).toString();
    } catch (ex) {
      console.error(`${cmd} 执行失败`);
      return ex.message;
    }
  }

  /**
   * devops 那边用的，判断项目时候绑定jenkins, harbor, gitlab,
   * 资源的host 的值如果等于传入的值，证明已经集成成功了，否则返回false, case 里面需要集成jenkins, harbor, gitlab
   * @param kind 资源类型
   * @param host jenkins, harbor, gitlab 的值， eg, http://62.234.104.184:31101
   */
  // getServiceNameByHost(kind, host, clusterName) {
  //     // 执行kubectl 命令
  //     const items = this.parseYaml(
  //         this.execKubectlCommand(`kubectl get ${kind} -o yaml`, clusterName)
  //     ).items;
  //     items.forEach(item => {
  //         if (item.hasOwnProperty('spec')) {
  //             if (item.spec.hasOwnProperty('http')) {
  //                 if (item.spec.http.hasOwnProperty('host')) {
  //                     if (item.spec.http.host === host) {
  //                         return item.metadata.name;
  //                     }
  //                 }
  //             }
  //         }
  //     });
  //     return 'false';
  // }
}
module.exports = {
  // 通用
  KubectlCommon: new KubectlCommon(),
};
