import axios from 'axios';
//const HttpsProxyAgent = require('https-proxy-agent');
import { ServerConf } from '../config/serverConf';
import { CommonMethod } from '@e2e/utility/common.method';
import { LoginPage } from '@e2e/page_objects/login.page';

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
axios.defaults.baseURL = ServerConf.BASE_URL;
axios.defaults.timeout = 5000;
// const [host, port, username, password] = [
//   '139.186.17.154',
//   '52975',
//   'alauda',
//   'Ah#U4TSwnjERBU@E1KZN8',
// ];
// axios.defaults.httpsAgent = new HttpsProxyAgent(
//   `http://${username}:${password}@${host}:${port}/`,
// );

// http request 拦截器
axios.interceptors.request.use(
  config => {
    const loginPage = new LoginPage();
    if (!loginPage.hasToken()) {
      const token = CommonMethod.loginGetToken(
        ServerConf.ADMIN_USER,
        ServerConf.ADMIN_PASSWORD,
      );
      CommonMethod.writeyamlfile('token', token);
    }
    config.headers.Authorization = `Bearer ${CommonMethod.readyamlfile(
      'temp/token',
    )}`;

    config.headers.Authorization = `Bearer ${CommonMethod.readyamlfile(
      'temp/token',
    )}`;

    if (config.method === 'patch') {
      config.headers['content-type'] = 'application/merge-patch+json';
    }
    return config;
  },
  err => {
    return Promise.reject(err);
  },
);

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error && error.response.status) {
      switch (error.response.status) {
        case 400:
          console.log('Status Code 400 : 错误请求');
          return error.response;
        case 401:
          console.log('Status Code 401 : 未授权，请重新登录');
          return error.response;
        case 403:
          console.log('Status Code 403 : 拒绝访问');
          return error.response;
        case 404:
          console.log('Status Code 404 : 请求错误,未找到该资源');
          return error.response;
        case 405:
          console.log('Status Code 405 : 请求方法未允许');
          return error.response;
        case 408:
          console.log('Status Code 408 : 请求超时');
          return error.response;
        case 500:
          console.log('Status Code 500: 服务器端出错');
          return error.response;
        case 501:
          console.log('Status Code 501 : 网络未实现');
          return error.response;
        case 502:
          console.log('Status Code 502 : 网络错误');
          return error.response;
        case 503:
          console.log('Status Code 503 : 服务不可用');
          return error.response;
        case 504:
          console.log('Status Code 504 : 网络超时');
          return error.response;
        case 505:
          console.log('Status Code 505 : http版本不支持该请求');
          return error.response;
        default:
          console.log(
            `Status Code ${error.response.status} : ${error.response.statusText}`,
          );
          return error.response;
      }
    }
  },
);

/**
 * 发送 get 请求
 * @param url 请求 url
 * @param params 参数
 */
export async function axiosGet(url, params = {}) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, {
        params: params,
      })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}

/**
 * 发送 delete 请求
 * @param url 请求 url
 * @param params 参数
 */
export function axiosDelete(url, params = {}) {
  return new Promise((resolve, reject) => {
    axios
      .delete(url, {
        params: params,
      })
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}

/**
 * 发送 Post 请求
 * @param url 请求 url
 * @param data 参数
 */
export function axiosPost(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, data)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}

/**
 * 发送 Put 请求
 * @param url 请求 url
 * @param data 参数
 */
export async function axiosPut(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios
      .put(url, data)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}

/**
 * 发送 Patch 请求
 * @param url 请求 url
 * @param data 参数
 */
export async function axiosPatch(url, data = {}) {
  return new Promise((resolve, reject) => {
    axios
      .patch(url, data)
      .then(response => {
        resolve(response);
      })
      .catch(err => {
        reject(err);
      });
  });
}

export default axios;
