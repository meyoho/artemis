/**
 * Created by liuwei on 2018/2/28.
 */

import {
  axiosGet,
  axiosDelete,
  axiosPost,
  axiosPut,
  axiosPatch,
} from './alauda.axios';

export class CommonHttp {
  /**
   * 发送 get 请求
   * @param url 请求URL
   * @param params 参数
   */
  static async get(url, params = {}): Promise<any> {
    const resonse = await axiosGet(url, params);
    return resonse;
  }

  /**
   * 发送 delete 请求
   * @param url 请求URL
   * @param params 参数
   */
  static async delete(url, params = {}): Promise<any> {
    return await axiosDelete(url, params);
  }

  /**
   * 发送 post 请求
   * @param url 请求URL
   * @param params 参数
   */
  static async post(url, data = {}): Promise<any> {
    return await axiosPost(url, data);
  }

  /**
   * 发送 put 请求
   * @param url 请求URL
   * @param params 参数
   */
  static async put(url, data = {}): Promise<any> {
    return await axiosPut(url, data);
  }

  /**
   * 发送 patch 请求
   * @param url 请求URL
   * @param params 参数
   */
  static async patch(url, data = {}): Promise<any> {
    return await axiosPatch(url, data);
  }
}
