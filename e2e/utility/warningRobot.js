const fs = require('fs');
const path = require('path');
const xml2js = require('xml2js');
const unique = require('array-unique');
const warningRobot = require('./common.email').warningRobot;

function retrievalFailedCase() {
  /**
   * Since the ( afterLaunch ) called after prtractor finished the execution
   * the results XML file should have the header added from the reporter call.
   * and we add this to close the list here
   */
  const file = resultsFilePath();
  if (!fs.existsSync(file)) {
    /* Result file is not created, because tests passed, consider as success */
    return false;
  }

  fs.appendFileSync(file, '</list>');

  let failedTestList;
  const data = fs.readFileSync(file, 'utf8');
  const parser = new xml2js.Parser();
  parser.parseString(data, function(err, result) {
    if (err) {
      console.log(err);
    }
    failedTestList = result;
  });

  if (
    failedTestList.list.failedTest &&
    failedTestList.list.failedTest.length !== 0
  ) {
    const list = [];
    failedTestList.list.failedTest.forEach(function(failTest) {
      list.push(failTest.spec.toString());
    });

    const fixedSpecList = fixSpecs(unique(list.sort()).join(','));

    return String(fixedSpecList);
  }

  return false;
}

function fixSpecs(specList) {
  const list = specList.split(',');
  list.forEach(function(data, key) {
    if (data.indexOf('__protractor_internal_afterEach_setup_spec.js') > 0) {
      list.splice(key, 1);
    }
  });
  return list.join(',');
}

function resultsFilePath() {
  const resultsDir = path.resolve(process.cwd(), 'protractorFailuresReport');
  let fileName = 'protractorTestErrors';

  if (!fs.existsSync(resultsDir)) {
    fs.mkdirSync(resultsDir);
  }

  fileName = fileName + '-' + 3;

  return resultsDir + '/' + fileName + '.xml';
}

// module.exports = {
//   retrievalFailedCase: retrievalFailedCase,
// };

let failedCase = retrievalFailedCase();

if (
  process.env.BUILD_URL !== undefined &&
  failedCase !== false &&
  (process.env.BUILD_URL.includes('master') ||
    process.env.BUILD_URL.includes('automation-ui-test'))
) {
  failedCase =
    String(failedCase).replace(
      new RegExp('/go/src/alauda.io/artemis/', 'g'),
      '',
    ) +
    '\n\n 注: ' +
    process.env.BUILD_URL;

  const mentioned_list = [];

  if (failedCase.includes('e2e/tests/ait')) {
    mentioned_list.push('13436500533');
  }
  if (failedCase.includes('e2e/tests/platform')) {
    mentioned_list.push('13436500533');
  }
  if (failedCase.includes('e2e/tests/devops')) {
    mentioned_list.push('13851618795');
    mentioned_list.push('15337081725');
  }
  if (failedCase.includes('e2e/tests/acp')) {
    mentioned_list.push('15201476124');
    mentioned_list.push('15210895066');
  }
  if (failedCase.includes('e2e/tests/asm')) {
    mentioned_list.push('15810027671');
  }
  mentioned_list.push('13520466801');

  warningRobot(
    '第二次重试失败的case:\n' + failedCase.replace(new RegExp(',', 'g'), '\n'),
    mentioned_list,
  );
}
