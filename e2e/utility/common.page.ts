/**
 * Created by liuwei on 2018/2/14.
 */
import { ElementFinder, browser, by, element, protractor } from 'protractor';

export class CommonPage {
  /**
   * wait an element to present on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementPresent(elem: ElementFinder, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => isPresent);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(`wait ${elem.locator()} Present error [${err}]`);
          return false;
        },
      );
  }

  /**
   * wait an element to disappear on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotPresent(elem: ElementFinder, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.isPresent().then(isPresent => !isPresent);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(`wait ${elem.locator()} not Present error [${err}]`);
          return false;
        },
      );
  }

  /**
   * wait an element to display on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementDisplay(elem, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.isDisplayed().then(isDisplayed => isDisplayed);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(`wait ${elem.locator()} displayed error [${err}]`);
          return false;
        },
      );
  }

  /**
   * wait an element to hidden on the page
   *
   * @parameter {elem} which the element of a page for waiting
   * @parameter {timeout} Wait for how many millisecond of timeout
   */
  static waitElementNotDisplay(elem, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.isDisplayed().then(isDisplayed => !isDisplayed);
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn(`wait ${elem.locator()} not displayed error [${err}]`);
          return false;
        },
      );
  }

  static get _leftNavItem() {
    return {
      命名空间相关资源: '/others,namespaced',
      集群相关资源: '/others,clustered',
    };
  }

  /**
   * 等待元素能单击
   * @param elem 页面元素
   * @param timeout 等待多久超时
   */
  static waitElementClickable(elem: ElementFinder, timeout = 20000) {
    const EC = protractor.ExpectedConditions;
    return browser.driver.wait(EC.elementToBeClickable(elem), timeout).then(
      () => true,
      err => {
        elem.getTagName().then(tagName => {
          console.error(err);
          throw new Error(`元素${tagName}, 不可单击`);
        });
        return false;
      },
    );
  }

  /**
   * 单击页面的左导航
   *
   * @parameter {text} 左导航显示的文字，
   * @example clickLeftNavByText('命名空间相关资源');
   */
  static clickLeftNavByText(text) {
    const child_xpath = `//div[contains(@class,"aui-nav-item-li")]//div[@class="aui-platform-nav__label"  and normalize-space(text()) ='${text}']`;
    const parent_xpath =
      child_xpath + `/ancestor::div[contains(@class,"aui-nav-item-li")]`;

    const parent_elem = element.all(by.xpath(parent_xpath)).first();
    let child_elem: ElementFinder;
    return element
      .all(by.xpath(child_xpath))
      .count()
      .then(count => {
        if (count === 1) {
          child_elem = element.all(by.xpath(child_xpath)).first();
        } else {
          child_elem = element.all(by.xpath(child_xpath)).last();
        }

        // 等待左导航加载
        CommonPage.waitElementPresent(child_elem, 10000).then(isPresent => {
          if (!isPresent) {
            throw new Error(`根据xpath: ${child_xpath} 没有找到左导航`);
          }
        });

        child_elem.isDisplayed().then(isDisplayed => {
          if (isDisplayed) {
            // 如果左导航显示了，单击
            CommonPage.waitElementClickable(child_elem);
            child_elem.click();
          } else {
            // 如果没显示，找到父菜单，单击
            CommonPage.waitElementClickable(parent_elem);
            parent_elem.click().then(() => {
              CommonPage.waitElementClickable(child_elem);
              child_elem.click();
            });
          }
        });
        // 等待page Progressbar 元素不显示
        // return CommonPage.waitProgressbarNotDisplay();
        return browser.sleep(1);
      });
  }

  /**
   * 等待元素的文本变成期望的文本
   * @param elem 页面元素
   * @param expectText 期望文本
   * @param timeout 超时时间
   */
  static waitElementTextChangeTo(elem, expectText, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.getText().then(text => {
          return text.trim() === String(expectText).trim();
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }

  static waitRowcountTextChangeTo(elem, expectCount, timeout = 20000) {
    return browser.driver
      .wait(() => {
        return elem.count().then(function(count) {
          return count === expectCount;
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }

  static waitStateChangeTo(elem, expectText, timeout = 1200000) {
    return browser.driver
      .wait(() => {
        return elem.getText().then(text => {
          if (text.trim() === '执行失败') {
            throw new Error(`流水线执行失败`);
          }
          return text.trim() === String(expectText).trim();
        });
      }, timeout)
      .then(
        () => true,
        err => {
          console.warn('wait error: [' + err + ']');
          return false;
        },
      );
  }
}
