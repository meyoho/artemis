const CommonKubectl = require('./kubectl.common').KubectlCommon;
const ServerConf = require('./ServerConf').ServerConf;

class PrepareTestData {
  get testdataPath() {
    return process.cwd() + '/e2e/test_data';
  }

  isNotExist(clusterName, cmd) {
    return (
      CommonKubectl.execKubectlCommand(cmd, clusterName).indexOf('NotFound') !==
      -1
    );
  }
  sleep(ms) {
    const startTime = new Date().getTime();
    while (new Date().getTime() < startTime + ms) {
      if (ms % 10000 === 0) {
        console.log('');
      }
    }
  }
  /**
   * 业务集群的名称，值必须是真实的业务集群的名称，
   * 会根据这个值查询访问该集群的token
   */
  get buinessClusterName() {
    return ServerConf.REGIONNAME;
  }

  get globalClusterName() {
    // 执行kubectl 命令时, 判断clustername === 'global', 就自动切换到global 集群，
    // 否则切换到页面集群， 值必须是 'global'
    return 'global';
  }

  /**
   * 创建一个项目
   * @param projectName 项目名称
   */
  createProject(projectName) {
    this.createProjectByTemplate(projectName);
  }
  /**
   * 创建项目
   * @param {*} projectName 项目名称
   * @param {*} cluster 集群名称 或集群列表
   */
  createProjectByTemplate(
    projectName,
    cluster = this.buinessClusterName,
    type = '',
    fedcluster = '',
  ) {
    console.log(`在集群${cluster}上, 创建项目 ${projectName}`);
    const data = {
      PROJECT_NAME: projectName,
      PROJECT_DESCRIPTION: 'UI TEST Project',
      PROJECT_DISPLAYNAME: 'UI TEST Project',
      CLUSTERS: [],
      type: type,
    };
    if (Array.isArray(cluster)) {
      cluster.forEach(v => {
        data.CLUSTERS.push({
          CLUSTER_NAME: v,
        });
      });
    } else {
      data.CLUSTERS.push({
        CLUSTER_NAME: cluster,
      });
    }
    if (fedcluster != '') {
      data.FED_CLUSTERS = fedcluster;
    }
    if (this.isNotExist('global', `kubectl get project ${projectName}`)) {
      // 如果项目不存在，创建项目, 创建项目是在global 集群上创建的
      CommonKubectl.createResourceByTemplate(
        'alauda.project.template.yaml',
        data,
        'qa-project' + String(new Date().getMilliseconds()),
        this.globalClusterName,
      );

      // 等待项目创建成功
      let timeout = 0;
      while (
        this.isNotExist(
          this.globalClusterName,
          `kubectl get project ${projectName}`,
        )
      ) {
        this.sleep(1000);
        if (timeout++ > 120) {
          throw new Error(`创建项目 ${projectName} 超时了`);
        }
      }
    } else {
      console.log(`项目 ${projectName} 已经存在了`);
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl get project ${projectName}`,
          this.globalClusterName,
        ),
      );
    }

    console.log(`==================================================\n`);
  }
  /**
   * 在项目下，创建一个命名空间
   * @param projectName 项目名称
   * @param namespaceName 命名空间名称
   */
  createNamespace(
    projectName,
    namespaceName,
    regionName = this.buinessClusterName,
  ) {
    console.log(
      `在项目${projectName} 下，创建命名空间${namespaceName}(所属集群:${regionName})`,
    );
    if (
      this.isNotExist(
        regionName,
        `kubectl get ns ${projectName}-${namespaceName}`,
      )
    ) {
      CommonKubectl.createResourceByTemplate(
        'alauda.namespace.template.yaml',
        {
          cluster: regionName,
          project: projectName,
          ns_name: namespaceName,
          display_name: 'UI TEST NS',
        },
        'qa-namespace' + String(new Date().getMilliseconds()),
        regionName,
      );

      // 等待项目创建成功
      let timeout = 0;
      while (
        this.isNotExist(
          regionName,
          `kubectl get ns ${projectName}-${namespaceName}`,
        )
      ) {
        this.sleep(1000);
        if (timeout++ > 120) {
          throw new Error(
            `创建命名空间 ${projectName}-${namespaceName} 超时了`,
          );
        }
      }
    }
    console.log(`==================================================\n`);
  }

  enabledAsm(namespaceName) {
    console.log(`命名空间 ${namespaceName} 开启 Service Mesh`);
    CommonKubectl.execKubectlCommand(
      `kubectl label ns ${namespaceName} "${ServerConf.LABELBASEDOMAIN}/serviceMesh"=enabled --overwrite`,
      this.buinessClusterName,
    );

    CommonKubectl.execKubectlCommand(
      `kubectl label ns ${namespaceName} "istio-injection"=enabled --overwrite`,
      this.buinessClusterName,
    );
  }

  /**
   * 创建一个asm使用的应用
   * @param {*} name
   * @param {*} namespaceName
   */
  createAsmApp(name, namespaceName) {
    if (
      this.isNotExist(
        this.buinessClusterName,
        `kubectl get deploy ${name} -n ${namespaceName}`,
      )
    ) {
      console.log(`在命名空间 ${namespaceName} 创建应用 ${name}`);
      CommonKubectl.createResource(
        'alauda.asm.service.yaml',
        {
          '${namespace_name}': namespaceName,
          '${label}': ServerConf.LABELBASEDOMAIN,
          '${app_name}': name,
        },
        'qa-asmapp' + String(new Date().getMilliseconds()),
        this.buinessClusterName,
      );
    } else {
      console.log(`deployment ${name} 已经存在了`);
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl get deploy ${name} -n ${namespaceName}`,
          this.buinessClusterName,
        ),
      );
    }
  }

  /**
   * 删除project
   * @param project_name project 名称
   * @example deleteProject('asm-demo')
   */
  deleteProject(project_name) {
    try {
      // 获得项目绑定的所有集群
      CommonKubectl.execKubectlCommand(
        `kubectl delete project ${project_name}`,
        this.globalClusterName,
      );
    } catch (e) {
      console.log(e.message);
    }
  }

  _hasIntegrate(type, host) {
    const toollists = CommonKubectl.parseYaml(
      CommonKubectl.execKubectlCommand(
        `kubectl get ${type}  -o yaml`,
        this.globalClusterName,
      ),
    ).items;

    let isIntegrate = false;
    toollists.forEach(elem => {
      if (elem.spec.http.host === host) {
        console.log(
          `【${elem.metadata.name}】 已经使用 【${host}】 集成了 ${type}`,
        );
        isIntegrate = true;
      }
    });
    return isIntegrate;
  }

  _getIntegrateName(type, host) {
    const codeRepoServiceList = CommonKubectl.parseYaml(
      CommonKubectl.execKubectlCommand(
        `kubectl get ${type}  -o yaml`,
        this.globalClusterName,
      ),
    ).items;

    let name;

    codeRepoServiceList.forEach(elem => {
      if (elem.spec.http.host === host) {
        name = elem.metadata.name;
      }
    });

    return name;
  }

  _getIntegrateUID(type, host) {
    const nexus = CommonKubectl.parseYaml(
      CommonKubectl.execKubectlCommand(
        `kubectl get ${type}  -o yaml`,
        this.globalClusterName,
      ),
    ).items;

    let uid;

    nexus.forEach(elem => {
      if (elem.spec.http.host === host) {
        uid = elem.metadata.uid;
      }
    });

    return uid;
  }

  /**
   * 集成一个Jenkins
   */
  integrateJenkins() {
    console.log(`集成Jenkins ${ServerConf.JENKINS_HOST}`);
    if (!this._hasIntegrate('Jenkins', ServerConf.JENKINS_HOST)) {
      CommonKubectl.createResourceByTemplate(
        'alauda.jenkins.yaml',
        {
          JENKINS_NAME: ServerConf.JENKINS_NAME,
          JENKINS_HOST: ServerConf.JENKINS_HOST,
        },
        'qa-jenkins' + String(new Date().getMilliseconds()),
        this.globalClusterName,
      );
      console.log(`==================================================\n`);
      return ServerConf.JENKINS_NAME;
    } else {
      console.log(`==================================================\n`);
      return this._getIntegrateName('Jenkins', ServerConf.JENKINS_HOST);
    }
  }

  /***
   * 如果没有使用 ServerConf.GITLAB_URL 集成过GitLab，集成一个Gitlab,
   * 返回集成的名称
   */
  integrateGitlab() {
    console.log(`集成GitLab ${ServerConf.GITLAB_URL}`);
    if (!this._hasIntegrate('CodeRepoService', ServerConf.GITLAB_URL)) {
      CommonKubectl.createResource(
        'alauda.codereposervice.yaml',
        {
          '${NAME}': ServerConf.GITLAB_NAME,
          '${CODEREPO_NAME}': ServerConf.GITLAB_NAME,
          '${CODEREPO_HOST}': ServerConf.GITLAB_URL,
          '${TYPE}': 'Gitlab',
          '${PUBLIC}': 'false',
        },
        'qa-gitlab' + String(new Date().getMilliseconds()),
        this.globalClusterName,
      );
      console.log(`==================================================\n`);
      return ServerConf.GITLAB_NAME;
      // return [
      //   ServerConf.GITLAB_NAME,
      //   this._getIntegrateUID('CodeRepoService', ServerConf.GITLAB_URL),
      // ];
    } else {
      console.log(`==================================================\n`);
      return this._getIntegrateName('CodeRepoService', ServerConf.GITLAB_URL);
      // return [
      //   this._getIntegrateName('CodeRepoService', ServerConf.GITLAB_URL),
      //   this._getIntegrateUID('CodeRepoService', ServerConf.GITLAB_URL),
      // ];
    }
  }

  /**
   * 集成一个Nexus
   */
  integrateNexus() {
    console.log(`集成 Nexus ${ServerConf.NEXUS_URL}`);
    if (!this._hasIntegrate('artifactregistrymanager', ServerConf.NEXUS_URL)) {
      CommonKubectl.createResourceByTemplate(
        'devops.nexus.yaml',
        {
          label_base_domain: ServerConf.LABELBASEDOMAIN,
          artifact_registry_manager_name: ServerConf.NEXUS_NAME,
          artifact_registry_manager_host: ServerConf.NEXUS_URL,
          artifact_registry_manager_secret_name: 'uiauto-nexussecret-global',
          type: 'nexus',
        },
        'qa-nexus' + String(new Date().getMilliseconds()),
        this.globalClusterName,
      );
      console.log(`==================================================\n`);
      return [
        ServerConf.NEXUS_NAME,
        this._getIntegrateUID('artifactregistrymanager', ServerConf.NEXUS_URL),
      ];
    } else {
      console.log(`==================================================\n`);
      return [
        this._getIntegrateName('artifactregistrymanager', ServerConf.NEXUS_URL),
        this._getIntegrateUID('artifactregistrymanager', ServerConf.NEXUS_URL),
      ];
    }
  }

  /**
   * 新建Maven仓库
   */
  createMavenRepo(host, name, uid) {
    CommonKubectl.createResourceByTemplate(
      'devops.maven.yaml',
      {
        nexus_host: host,
        nexus_name: name,
        nexus_uid: uid,

        artifact_registry_name: ServerConf.MAVEN_NAME,
        artifact_registry_manager_secret_name: 'uiauto-nexussecret-global',
      },
      'qa-maven' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
    return ServerConf.MAVEN_NAME;

    // if (!this._hasIntegrate('artifactregistry', ServerConf.NEXUS_URL)) {
    //   CommonKubectl.createResourceByTemplate(
    //     'alauda.artifactregistry.yaml',
    //     {
    //       label_base_domain: ServerConf.LABELBASEDOMAIN,
    //       artifact_registry_name: ServerConf.MAVEN_NAME,
    //       artifact_registry_manager_secret_name: 'uiauto-nexussecret-global',
    //     },
    //     'qa-maven' + String(new Date().getMilliseconds()),
    //     this.globalClusterName,
    //   );
    //   return ServerConf.MAVEN_NAME;
    // } else {
    //   return this._getIntegrateName('artifactregistry', ServerConf.NEXUS_URL);
    // }
  }

  /**
   * 集成一个Harbor
   */
  integrateHarbor() {
    console.log(
      `集成 Harbor ${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
    );
    if (
      !this._hasIntegrate(
        'ImageRegistry',
        `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
      )
    ) {
      CommonKubectl.createResourceByTemplate(
        'alauda.imageregistry.yaml',
        {
          NAME: ServerConf.HARBOR_NAME,
          REGISTRY_NAME: ServerConf.HARBOR_NAME,
          REGISTRY_HOST: `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
          REGISTRY_TYPE: 'Harbor',
        },
        'qa-harbor' + String(new Date().getMilliseconds()),
        this.globalClusterName,
      );
      console.log(`==================================================\n`);
      return ServerConf.HARBOR_NAME;
    } else {
      console.log(`==================================================\n`);
      return this._getIntegrateName(
        'ImageRegistry',
        `${ServerConf.HARBOR_HTTP}${ServerConf.HARBOR_HOST}`,
      );
    }
  }
  /**
   * 创建JenkinsSecret
   * @param secretName 凭据名称
   */
  createJenkinsSecret(secretName, secretNs) {
    console.log(`创建Jenkins 凭据: ${secretName}`);
    const secret_data = {
      name: secretName,
      namespace: this.getCredentialsNs(secretNs),
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: ServerConf.JENKINS_USER,
        password: ServerConf.JENKINS_PWD,
      },
    };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      secret_data,
      'jenkins-secret' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  /**
   * 创建NexusSecret
   * @param secretName 凭据名称
   */
  createNexusSecret(secretName, namespace) {
    const secret_data = {
      name: secretName,
      namespace: this.getCredentialsNs(namespace),
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: ServerConf.NEXUS_USER,
        password: ServerConf.NEXUS_USER,
      },
    };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      secret_data,
      'nexus-secret' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  /**
   * 创建GitlabSecret
   * @param secretName 凭据名称
   */
  createGitlabSecret(secretName, namespace) {
    const secret_data = {
      name: secretName,
      namespace: this.getCredentialsNs(namespace),
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: ServerConf.GITLAB_USER,
        password: ServerConf.GITLAB_TOKEN,
      },
    };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      secret_data,
      'gitlab-secret' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  /**
   * 创建HarborSecret
   * @param secretName 凭据名称
   */
  createHarborSecret(secretName, namespace) {
    const secret_data = {
      name: secretName,
      namespace: this.getCredentialsNs(namespace),
      datatype: 'kubernetes.io/basic-auth',
      datas: {
        username: ServerConf.HARBOR_USER,
        password: ServerConf.HARBOR_PWD,
      },
    };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      secret_data,
      'harbor-secret' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  /**
   * 创建镜像服务类型的Secret
   * @param secretName 凭据名称
   */
  createImageSecret(secretName, namespace) {
    const auth = Buffer.from(
      `${ServerConf.HARBOR_USER}:${ServerConf.HARBOR_PWD}`,
    ).toString('base64');
    const secret_image_data = {
      name: secretName,
      namespace: namespace,
      datatype: 'kubernetes.io/dockerconfigjson',
      datas: {
        '.dockerconfigjson': JSON.stringify({
          auths: {
            [ServerConf.HARBOR_HOST]: {
              username: ServerConf.HARBOR_USER,
              email: 'a@b.com',
              password: ServerConf.HARBOR_PWD,
              auth: auth,
            },
          },
        }),
      },
    };
    // with yinshuxun help

    // const secret_image_data = {
    //   name: secretName,
    //   namespace: namespace,
    //   datatype: 'kubernetes.io/dockerconfigjson',
    //   datas: {
    //     '.dockerconfigjson':
    //       '{"auths":{"10.0.128.241:31104":{"username":"admin","email":"a@b.com","password":"Harbor12345","auth":"YWRtaW46SGFyYm9yMTIzNDU="}}}',
    //   },
    // };
    CommonKubectl.createResourceByTemplate(
      'alauda.secret_tpl.yaml',
      secret_image_data,
      'image-secret' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }

  /**
   * 为项目绑定Jenkins
   * @param bindName Jenkins绑定名称
   */
  createJenkinsBinding(
    jenkinsName,
    bindName,
    projectName,
    secretName,
    secretNs,
  ) {
    console.log(`为项目${projectName} 绑定jenkins ${jenkinsName}`);
    CommonKubectl.createResourceByTemplate(
      'alauda.jenkinsbinding.yaml',
      {
        JENKINSBIND_NAME: bindName,
        JENKINS_NAME: jenkinsName,
        PROJECT_NAME: projectName,
        JENKINSSECRET_NAME: secretName,
        SECRET_NS: this.getCredentialsNs(secretNs),
      },
      'qa-jenkinsbinding' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
    console.log(`==================================================\n`);
  }

  /**
   * 获得 'global-credentials'
   */
  getCredentialsNs(ns) {
    if (ns === 'global-credentials') {
      return ServerConf.GLOBAL_NAMESPCE === 'alauda-system'
        ? 'global-credentials'
        : `${ServerConf.GLOBAL_NAMESPCE}-global-credentials`;
    }
    return ns;
  }

  /**
   * 为项目绑定Maven
   * @param bindName Maven绑定名称
   */
  createMavenBinding(mavenName, bindName, projectName, secretName, ns) {
    console.log(`\n项目${projectName} 绑定Maven`);
    CommonKubectl.createResourceByTemplate(
      'devops.maven.bind.yaml',
      {
        LABELBASEDOMAIN: ServerConf.LABELBASEDOMAIN,
        maven_name: mavenName,
        artifact_registry_binding_name: bindName,
        project_name: projectName,
        secret_namespace: this.getCredentialsNs(ns),
        artifact_registry_binding_secret_name: secretName,
      },
      'qa-mavenbinding' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
    console.log(`==================================================\n`);
  }
  /**
   * 为项目绑定Gitlab
   * @param bindName GItlab绑定名称
   */
  createGitlabBinding(
    codeServiceName,
    bindName,
    projectName,
    secretName,
    secretNs,
  ) {
    console.log(`${projectName} 项目绑定 GitLab(${codeServiceName})`);
    CommonKubectl.createResourceByTemplate(
      'devops.coderepobinding.yaml',
      {
        CODESERVICEBIND_NAME: bindName,
        NAMESPACE: projectName,
        CODESERVICE_NAME: codeServiceName,
        CODESERVICE_TYPE: 'gitlab',
        CODESERVICE_SECRET: secretName,
        SECRET_NS: this.getCredentialsNs(secretNs),
        ACCOUNT_NAME: ServerConf.GITLAB_USER,
      },
      'qa-coderepobinding' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
    console.log(`==================================================\n`);
  }
  /**
   * 为项目绑定Harbor
   * @param bindName Harbor绑定名称
   */
  createHarborBinding(harborname, bindName, projectName, secretName, secretNs) {
    console.log(`${projectName} 项目绑定 Harbor(${harborname})`);
    CommonKubectl.createResource(
      'alauda.imageregistry-bind.yaml',
      {
        '${NAME}': bindName,
        '${REGISTRY_NAME}': harborname,
        '${REGISTRYBIND_NAME}': bindName,
        '${NAMESPACE}': projectName,
        '${HARBOR_SECRET}': secretName,
        '${SECRET_NS}': this.getCredentialsNs(secretNs),
        '${REGISTRY_TYPE}': 'Harbor',
      },
      'qa-harborbinding' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
    console.log(`==================================================\n`);
  }
  /**
   * 为项目创建应用
   */
  createDevopsApp(namespace, appname) {
    CommonKubectl.createResourceByTemplate(
      'devops.deployments.yaml',
      {
        APP_NAME: appname,
        //label: ServerConf.LABELBASEDOMAIN,
        PROJECT_NAME: namespace,
        IMAGE: ServerConf.TESTIMAGE,
        CONTAINER_NAME: 'devops-app',
      },
      'qa-app' + String(new Date().getMilliseconds()),
      this.buinessClusterName,
    );
  }

  // -------------下面是通知相关资源
  // 部署默认的短信服务器，部署短信发送者，部署Webhook发送者
  createDefaultSender() {
    CommonKubectl.createResourceByTemplate(
      'alauda.defaultsender.yaml',
      {
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  // 创建通知服务器
  createEmailServer(name = 'uiauto-email163-server') {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationserver.yaml',
      {
        NAME: name,
        HOST: ServerConf.SERVER_HOST,
        PORT: ServerConf.SERVER_PORT,
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  //创建邮箱的通知对象和发送人，邮箱的通知模板
  createEmailReceiver(
    name = 'test-alauda-io-uiauto',
    destination = 'test@alauda.io',
  ) {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationreceiver.yaml',
      {
        DISPLAYNAME: 'UIAUTO邮箱',
        TYPE: 'email',
        NAME: name,
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        DESTINATION: destination,
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  createEmailSenderSecret(server = 'uiauto-email163-server') {
    let name = ServerConf.EMAILSENDER_NAME2.replace('@', '-');
    name = name.replace('.', '-');
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationsendersecret.yaml',
      {
        USERNAME: Buffer.from(ServerConf.EMAILSENDER_NAME2).toString('base64'),
        PASSWORD: Buffer.from(ServerConf.EMAILSENDER_PWD2).toString('base64'),
        NAME: name,
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        SERVER: server,
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  createEmailtemplate() {
    CommonKubectl.createResource(
      'alauda.notificationemailtemplate.yaml',
      {
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${CREATOR}': ServerConf.ADMIN_USER,
        '${NAME}': 'uiauto-emailtemplate',
        '${DISPLAYNAME}': 'UIAUTO-Email的模板',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  //创建短信的通知对象，短信的通知模板
  createSmsReceiver() {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationreceiver.yaml',
      {
        DISPLAYNAME: 'UIAUTO短信',
        TYPE: 'sms',
        NAME: '13436500533-uiauto',
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        DESTINATION: '13436500533',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  createSmstemplate() {
    CommonKubectl.createResource(
      'alauda.notificationsmstemplate.yaml',
      {
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${CREATOR}': ServerConf.ADMIN_USER,
        '${NAME}': 'uiauto-smstemplate',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  //使用yaml创建webhook的通知对象，通知模板
  createWebhookReceiver() {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationreceiver.yaml',
      {
        DISPLAYNAME: 'UIAUTOWebhook',
        TYPE: 'webhook',
        NAME: 'http---webhook123-com-uiauto',
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        DESTINATION: 'http://webhook123.com',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  createWebhooktemplate() {
    CommonKubectl.createResource(
      'alauda.notificationwebhooktemplate.yaml',
      {
        '${label}': ServerConf.LABELBASEDOMAIN,
        '${CREATOR}': ServerConf.ADMIN_USER,
        '${NAME}': 'uiauto-othertemplate',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  //创建钉钉的通知对象
  createDingtalkReceiver() {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationreceiver.yaml',
      {
        DISPLAYNAME: 'UIAUTO-钉钉',
        TYPE: 'dingtalk',
        NAME: 'http---dingtalk-com-uiauto',
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        DESTINATION: 'http://dingtalk.com',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  //创建企业微信的通知对象
  createWechatReceiver() {
    CommonKubectl.createResourceByTemplate(
      'alauda.notificationreceiver.yaml',
      {
        DISPLAYNAME: 'UIAUTO-企业微信',
        TYPE: 'wechat',
        NAME: 'http---wechat-com-uiauto',
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        DESTINATION: 'http://wechat.com',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }
  // 给stable chartrepo 绑定所有项目
  bindingAllProjectToChartRepo() {
    const ret = CommonKubectl.execKubectlCommand(
      `kubectl label chartrepos.app.alauda.io -n ${ServerConf.GLOBAL_NAMESPCE} stable ${ServerConf.LABELBASEDOMAIN}/project=ALL_ALL --overwrite=true`,
      this.globalClusterName,
    );
    console.log(ret);
  }

  createNotificationEmail(
    name = 'uiauto-notification-email',
    receiver = 'test-alauda-io-uiauto',
    sender = 'jiaozhang555-163-com',
    template = 'uiauto-emailtemplate',
  ) {
    CommonKubectl.createResourceByTemplate(
      'alauda.notification.email.yaml',
      {
        NAME: name,
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        RECEIVER: receiver,
        SENDER: sender,
        TEMPLATE: template,
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }

  createNotificationSms() {
    CommonKubectl.createResourceByTemplate(
      'alauda.notification.sms.yaml',
      {
        NAME: 'uiauto-notification-sms',
        NAMESPACE: `${ServerConf.GLOBAL_NAMESPCE}`,
        RECEIVER: '13800138000-uiauto',
        SENDER: 'default-sms-sender',
        TEMPLATE: 'uiauto-smstemplate',
      },
      'qa-ait' + String(new Date().getMilliseconds()),
      this.globalClusterName,
    );
  }

  /**
   * 为基础设施创建应用+(Deployment, DaemonSet, StatefulSet)，测试监控以及创建告警
   */
  createAitApp(appName, namespace, projectname, kind) {
    if (
      this.isNotExist(
        this.buinessClusterName,
        `kubectl get application ${appName} -n ${namespace}`,
      )
    ) {
      console.log(`在命名空间 ${namespace} 创建应用 ${appName}`);
      CommonKubectl.createResourceByTemplate(
        'alauda.application.tpl.yaml',
        {
          app_name: appName,
          ns_name: namespace,
          pro_name: projectname,
          image: ServerConf.TESTIMAGE,
          kind: kind,
        },
        'qa-application' + String(new Date().getMilliseconds()),
        this.buinessClusterName,
      );
    } else {
      console.log(`application ${appName} 已经存在了`);
      console.log(
        CommonKubectl.execKubectlCommand(
          `kubectl get application ${appName} -n ${namespace}`,
          this.buinessClusterName,
        ),
      );
    }
  }
}

module.exports = {
  // 通用
  prepareTestData: new PrepareTestData(),
};
