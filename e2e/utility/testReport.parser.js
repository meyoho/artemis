const fs = require('fs');
const join = require('path').join;
const parseString = require('xml2js').parseString;
const nodemailer = require('nodemailer');

class TestReportParser {
  get testdataPath() {
    return process.cwd() + '/e2e-reports';
  }

  sendMail(emailTitle, emailBody) {
    if (process.env.finish) {
      console.log('======> Email 发送了');
      const transporter = nodemailer.createTransport({
        host: 'smtpdm.aliyun.com',
        port: '465',
        secure: true,
        auth: {
          user: 'info@alauda.cn',
          pass: 'MATHilde123',
        },
        logger: true,
        debug: true,
      });

      const mailOptions = {
        from: 'info@alauda.cn', // sender address
        to: 'weiliu@alauda.io', // list of receivers
        subject: emailTitle, // Subject line

        html: emailBody,
        attachments: [
          {
            path: process.cwd() + '/e2e-reports/report.html',
          },
        ],
      };

      return transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          return console.log(error);
        }
        console.log('Mail sent successfully : ' + info.response);
      });
    }
  }

  /**
   * 保存测试结果
   * @param {string} reportResult
   */
  saveReport(reportResult) {
    const testReport = this.testdataPath + '/totalResult.txt';
    const tempResult = [];
    if (fs.existsSync(testReport)) {
      reportResult.forEach(element => {
        const resultList = JSON.parse(fs.readFileSync(testReport, 'utf-8'));
        const testcaseResult = resultList.find(value => {
          return value.name === element.name;
        });

        testcaseResult.count++;

        if (element.success === true) {
          testcaseResult.success++;
        } else {
          testcaseResult.error++;
        }
        testcaseResult.caseError = testcaseResult.error / testcaseResult.count;
        testcaseResult.avgTime =
          (parseFloat(testcaseResult.count) *
            parseFloat(testcaseResult.avgTime) +
            parseFloat(element.time)) /
          (parseFloat(testcaseResult.count) + 1);

        testcaseResult.minTime =
          parseFloat(testcaseResult.minTime) < parseFloat(element.time)
            ? parseFloat(testcaseResult.minTime)
            : parseFloat(element.time);
        testcaseResult.maxTime =
          parseFloat(testcaseResult.maxTime) > parseFloat(element.time)
            ? parseFloat(testcaseResult.maxTime)
            : parseFloat(element.time);
        tempResult.push(testcaseResult);
      });
      fs.writeFileSync(testReport, JSON.stringify(tempResult));
    } else {
      reportResult.forEach(element => {
        const dic = {};
        dic['name'] = element.name;
        dic['count'] = 1;
        dic['success'] = element.success ? 1 : 0;
        dic['error'] = element.success ? 0 : 1;
        dic['caseError'] = dic['error'] / dic['count'];
        dic['avgTime'] = element.time;
        dic['minTime'] = element.time;
        dic['maxTime'] = element.time;
        tempResult.push(dic);
      });
      fs.writeFileSync(testReport, JSON.stringify(tempResult));
    }
  }

  /**
   * 分析测试结果
   */
  parse() {
    console.log('分析测试结果');
    const reportResult = [];
    try {
      const reports = this.findSync(this.testdataPath);
      reports.forEach(report => {
        if (String(report).includes('.xml')) {
          const data = fs.readFileSync(report, 'utf-8');
          parseString(data, function(err, result) {
            result.testsuites.testsuite.forEach(testsuite => {
              testsuite.testcase.forEach(testcase => {
                const dic = {};
                dic['name'] = testcase.$.name;
                const success = Object.keys(testcase).length === 1;
                dic['success'] = success;
                dic['time'] = testcase.$.time;
                reportResult.push(dic);
              });
            });
          });
        }
      });
      this.saveReport(reportResult);
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  /**
   * 获得startPath 路径下所有的文件名
   * @param {string} startPath
   */
  findSync(startPath) {
    const result = [];
    function finder(path) {
      const files = fs.readdirSync(path);
      files.forEach(val => {
        const fPath = join(path, val);
        const stats = fs.statSync(fPath);
        if (stats.isDirectory()) finder(fPath);
        if (stats.isFile()) result.push(fPath);
      });
    }
    finder(startPath);
    return result;
  }
}

module.exports = {
  // 通用
  Parser: new TestReportParser(),
};
