#!/bin/sh

#set -o pipefail
#set -o nounset

PLUGIN_NAME="${PLUGIN_NAME:-artemis}"
RESULTS_DIR="${RESULTS_DIR:-/tmp/results}"

# run tests
./scripts/e2e.sh
cp -r e2e-reports/* ${RESULTS_DIR}
cp -r protractorFailuresReport ${RESULTS_DIR}
# cp test.log ${RESULTS_DIR}
# cat test.log

# Gather results into one file
cd ${RESULTS_DIR}
tar -czf ${PLUGIN_NAME}.tar.gz *

# Let the sonobuoy worker know the job is done
echo -n "${RESULTS_DIR}/${PLUGIN_NAME}.tar.gz" >"${RESULTS_DIR}/done"

sleep 120