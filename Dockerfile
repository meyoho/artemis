FROM harbor-b.alauda.cn/alaudaorg/builder-nodejs:10-stretch-chrome-v2.11-7-gda72849

MAINTAINER qa@alauda.io

ENV PROJECT_PATH=/go/src/alauda.io/artemis
ENV LANG=en_US.UTF-8
RUN apt-get update -y && \
  apt-get install ttf-wqy-microhei ttf-wqy-zenhei xfonts-wqy -y && \
  apt-get install -y locales && \
  grep -Po '^[^#].+' /etc/locale.gen|xargs -i  sed -i -e 's/^'{}'/# '{}'/g' /etc/locale.gen && \
  sed -i -e "s/# $LANG.*/$LANG UTF-8/" /etc/locale.gen && \
  dpkg-reconfigure --frontend=noninteractive locales && \
  update-locale LANG=$LANG && \
  apt-get install -y vim openssl libssl-dev python3-pip
RUN mkdir -p ${PROJECT_PATH}
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
  && chmod +x ./kubectl && mv kubectl /usr/local/sbin
WORKDIR ${PROJECT_PATH}
COPY ./ ${PROJECT_PATH}
RUN chmod +x ./scripts/kubectlConfig.sh \
  && chmod +x ./scripts/add_volumemonut.sh \
  && chmod +x ./scripts/e2e.sh \
  && yarn install \
  && python3.5 -m pip install testlink-api \
  && python3.5 -m pip install requests \
  && python3.5 -m pip install urllib3 \
  && python3.5 -m pip install bs4 \
  && python3.5 -m pip install lxml 

RUN ./node_modules/webdriver-manager/bin/webdriver-manager update --versions.chrome 2.44

CMD [ "./scripts/e2e.sh"]
