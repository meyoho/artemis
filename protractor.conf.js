// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const tsconfig = require('./tsconfig.json');
const { SpecReporter } = require('jasmine-spec-reporter');
const { JUnitXmlReporter } = require('jasmine-reporters');
const { retry } = require('protractor-retry');
const HtmlReporter = require('protractor-beautiful-reporter');
const Recycler = require('./e2e/utility/clear.testdata');
const failFast = require('protractor-fail-fast');

const config = {
  plugins: [failFast.init()],
  specs: [],
  suites: {
    full_test: ['./e2e/tests/**/*-spec.ts'],
    diablo: ['./e2e/tests/devops/**/l0.*.devops-spec.ts'],
    icarus: ['./e2e/tests/acp/**/l0.*.acp-spec.ts'],
    mephisto: ['./e2e/tests/asm/**/l0*.asm-spec.ts'],
    underlord: [
      './e2e/tests/platform/**/l0*.platform-spec.ts',
      './e2e/tests/platform/**/l0*.acp-spec.ts',
    ],
    bat: ['./e2e/tests/**/l0*-spec.ts'],
    acp: ['./e2e/tests/acp/**/l0.*.acp-spec.ts'],
    acp_app: ['./e2e/tests/acp/acp_containerplatform/appcore/*.acp-spec.ts'],
    acp_network: [
      './e2e/tests/acp/acp_containerplatform/network/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/subnet/*.acp-spec.ts',
    ],
    acp_workload: [
      './e2e/tests/acp/acp_containerplatform/tapp/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/workload/*.acp-spec.ts',
    ],
    acp_other: [
      './e2e/tests/acp/acp_containerplatform/acp_storage/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/app_store/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/chart_repo/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/configs/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/jobs/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/pods/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/ratio/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/federatedapp/*.acp-spec.ts',
      './e2e/tests/platform/resource_management/*.acp-spec.ts',
      './e2e/tests/platform/namespace/*.acp-spec.ts',
      './e2e/tests/platform/federatedns/*.acp-spec.ts',
      './e2e/tests/platform/licensemanage/*.acp-spec.ts',
    ],
    acp_infra: ['./e2e/tests/acp/cluster/l0.*.acp-spec.ts'],
    acp_containerplatform: [
      './e2e/tests/acp/acp_containerplatform/**/*.acp-spec.ts',
      './e2e/tests/platform/**/*.acp-spec.ts',
    ],
    acp_operation: ['./e2e/tests/acp/acp_operation/**/l0.*.acp-spec.ts'],

    devops_ui: ['./e2e/tests/devops/**/l0.*.devops-spec.ts'],
    devops_api: ['./e2e/tests/devops/**/*.devops-spec.ts'],
    devops_pipeline: ['./e2e/tests/devops/pipeline/*.*.devops-spec.ts'],
    devops_project: ['./e2e/tests/devops/project/*.*.devops-spec.ts'],
    devops_tools: ['./e2e/tests/devops/devops-tools/*.*.devops-spec.ts'],
    devops_highcluster: [
      './e2e/tests/devops/pipeline/*.*.business.devops-spec.ts',
    ],

    asm_ui: ['./e2e/tests/asm/**/*.asm-spec.ts'],
    asm_api: ['./e2e/tests/asm/**/*.asm-spec.ts'],

    platform_ui: ['./e2e/tests/platform/**/l0.*.platform-spec.ts'],
    platform_api: ['./e2e/tests/platform/**/*.platform-spec.ts'],

    amp: ['./e2e/tests/amp/**/*.amp-spec.ts'],

    ait_ui: ['./e2e/tests/ait/**/l0.*.ait-spec.ts'],
    ait_api: ['./e2e/tests/ait/**/*.ait-spec.ts'],

    acp1: [
      './e2e/tests/acp/acp_containerplatform/appcore/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/network/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/subnet/*.acp-spec.ts',
    ],
    acp2: [
      './e2e/tests/acp/acp_containerplatform/tapp/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/workload/*.acp-spec.ts',
    ],
    acp3: [
      './e2e/tests/ait/**/*.ait-spec.ts',
      './e2e/tests/platform/**/*.platform-spec.ts',
      './e2e/tests/acp/acp_containerplatform/acp_storage/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/app_store/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/chart_repo/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/configs/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/jobs/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/ratio/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/resource_management/*.acp-spec.ts',
      './e2e/tests/acp/acp_containerplatform/federatedapp/*.acp-spec.ts',
      './e2e/tests/platform/**/*.acp-spec.ts',
    ],
    devops_asm: [
      './e2e/tests/devops/**/*.devops-spec.ts',
      './e2e/tests/asm/**/*.asm-spec.ts',
    ],
  },
  capabilities: {
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 1,
    acceptInsecureCerts: true,
  },
  allScriptsTimeout: 11000,
  directConnect: true,
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 1800000,
  },

  onPrepare() {
    require('ts-node').register({
      project: './tsconfig.json',
    });
    require('tsconfig-paths').register({
      project: './tsconfig.json',
      baseUrl: './e2e',
      paths: tsconfig.compilerOptions.paths,
    });

    const jasmineEnv = jasmine.getEnv();

    jasmineEnv.addReporter(
      new SpecReporter({ spec: { displayStacktrace: true } }),
    );
    jasmineEnv.addReporter(
      new JUnitXmlReporter({
        savePath: './e2e-reports',
        consolidateAll: true,
      }),
    );
    jasmineEnv.addReporter(
      new HtmlReporter({
        baseDirectory: './e2e-reports',
        screenshotsSubfolder: 'images',
        takeScreenShotsOnlyForFailedSpecs: true,
        docTitle: 'Acp2.x测试报告',
      }).getJasmine2Reporter(),
    );

    retry.onPrepare();

    browser.waitForAngularEnabled(false);
  },

  beforeLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch {
      console.warn('清理数据失败');
    }
  },

  onCleanUp(results) {
    retry.onCleanUp(results);
  },

  afterLaunch() {
    //清理资源
    try {
      Recycler.TestData.clear();
    } catch {
      console.warn('清理数据失败');
    }
    failFast.clean();
    return retry.afterLaunch(2);
  },
};

const proxy = process.env.BROWSER_PROXY;
if (proxy) {
  config.capabilities.proxy = proxy
    .split(';')
    .map(pair => pair.split('=').map(item => item.trim()))
    .reduce((acc, curr) => {
      const key = curr[0];
      const value = key === 'socksVersion' ? parseInt(curr[1]) : curr[1];
      return { ...acc, [key]: value };
    }, {});
}

module.exports = {
  config,
};
