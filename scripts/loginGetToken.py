import requests
import urllib3
import re
import argparse
import base64
urllib3.disable_warnings()


def arg_par():
    parser = argparse.ArgumentParser(description='created by cloud . 2017-12-16')
    parser.add_argument("--base_url", metavar="base url", default="", help="平台访问地址")
    parser.add_argument("--user", metavar="user email", default="admin@alauda.io", help="用户邮箱")
    parser.add_argument("--password", metavar="user password", default="password", help="密码")
    parser.add_argument("--auth", metavar="auth", default="local", help="登陆方式支持local和ldap")
    parser.add_argument("--proxy", metavar="proxy", default=None, help="代理支持http和https代理")
    #"http://alauda:Ah%23U4TSwnjERBU%40E1KZN8@139.186.17.154:52975"
    return vars(parser.parse_args())


def login(host, username, password, proto="https", auth="local", http_proxy=None):
    if http_proxy:
        proxy = {
            "http": http_proxy,
            "https": http_proxy
        }
    else:
        proxy = None
    headers = {
        'Referer': '{proto}://{host}/console-acp'.format(proto=proto, host=host)
    }
    r1 = requests.get("{proto}://{host}/console-acp/api/v1/token/login".format(proto=proto, host=host), headers=headers, proxies=proxy, timeout=10, verify=False)
    auth_url = r1.json().get("auth_url")
    state = r1.json().get("state")
    r2 = requests.get(auth_url, headers=headers, proxies=proxy, timeout=10, verify=False)
    req = re.findall('/dex/auth/{auth}\?req=(\w{1,})'.replace('{auth}', auth), r2.text)[0]
    r3 = requests.post("https://{host}/dex/auth/{auth}?req={req}".format(auth=auth, proto=proto, host=host,req=req), params={"req": req, "login": username, "encrypt": str(base64.b64encode(password.encode('utf-8')), "utf8")}, headers=headers, proxies=proxy, timeout=10, verify=False)
    code = re.findall(r'code=(\w{1,})', r3.history[1].text)[0]
    r4 = requests.get("{proto}://{host}/console-acp/api/v1/token/callback".format(proto=proto, host=host), params={"code": code, "state": state}, headers=headers, proxies=proxy, timeout=10, verify=False)
    return r4.json().get("id_token")


if __name__ == "__main__":
    args = arg_par()
    url = urllib3.util.parse_url(args['base_url'])
    print(login(host=url.host, username=args['user'], password=args['password'], proto=url.scheme, auth=args['auth'], http_proxy=args['proxy']))