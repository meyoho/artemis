#!/bin/sh
rm -f ./test_data/temp/*.yaml
rm -rf ./e2e-reports/*
rm -rf ./protractorFailuresReport

if [ $# == 0 ]; then
    ./node_modules/.bin/protractor ./protractor.headless.conf.js
else
    ./node_modules/.bin/protractor ./protractor.headless.conf.js --suite $1
fi