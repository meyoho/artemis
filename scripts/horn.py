#coding:utf-8
#!/bin/python3.5
import os
import re
import csv
import ssl
import json
import codecs
import requests
import testlink
import http.client
import xmlrpc.client
from bs4 import BeautifulSoup


class ProxiedTransport(xmlrpc.client.Transport):
    def set_proxy(self, proto, host, port=None, headers=None):
        self.proto = proto
        self.proxy = host, port
        self.proxy_headers = headers
        self.contex = ssl._create_unverified_context()

    def make_connection(self, host):
        connection = http.client.HTTPSConnection(*self.proxy, context=self.contex)
        connection.set_tunnel(host)
        self._connection = host, connection
        return connection


class ResultData(dict):
    def setitem(self, key, value):
        if key in self:
            if self[key] is True:
                pass
            else:
                self[key] = value
        else:
            self[key] = value


def send_metrics_to_prometheus(report):
    data = {
        "job": project_name.replace(' ', ''),               # Job Name，同pushgateway里的job name
        "metric_name": "ui_automation",                    # 指标名称，用来在grafana面板上配置使用
        "description": "UI 自动化测试失败统计",              # 指标说明
        "labels": ["vteam"],                               # 指标的标签列表
        "samples": [{
            "value": 1,                                    # 指标的值
            "vteam": "<vteam>",                            # 指标对应的标签1的值
        }]
    }
    vteam_agg = {}
    failed_files = scan_files(os.path.abspath(os.path.curdir)+'/e2e-reports/protractorFailuresReport', '^protractorTestErrors-3.xml$')
    failed_report = map(lambda x: re.split(r'\|[^\|]{1,}$', x)[0], filter(lambda ret: not report[ret], report.keys()))
    if failed_files:
        with codecs.open(failed_files[0], 'r', 'utf-8') as fp:
            failed_xml = BeautifulSoup(fp, 'lxml')
            case_files = filter(lambda case_file_name: case_file_name.endswith('spec.ts'), failed_xml.list.spec.string.split(',')) if failed_xml.list.spec.string else ''
            for case_name in failed_report:
                for case_file in case_files:
                    vteam_name = re.findall('(\w{1,})-spec.ts', case_file)[0]
                    with codecs.open(case_file, 'r', 'utf-8') as case_fp:
                        case_content = case_fp.read()
                        # print(case_content)
                        if case_name in case_content:
                            if vteam_name in vteam_agg:
                                vteam_agg[vteam_name] = vteam_agg[vteam_name] + 1
                            else:
                                vteam_agg[vteam_name] = 1
    samples = []
    for vteam in vteam_agg:
        samples.append({"value": vteam_agg[vteam], 'vteam': vteam})
    try:
        data["samples"] = samples
        rsp = requests.post(push_gateway, data=json.dumps([data]))
        if rsp.status_code == 200:
            if rsp.json()["result"] == "OK":
                print("send prometheus metrics success.")
            else:
                print("send prometheus metrics fail. %s : %s" % (rsp.status_code, rsp.text))
        else:
            print("send prometheus metrics fail %s:%s" % (rsp.status_code, rsp.text))
    except Exception as e:
        print("send report to prometheus fail. %s" % e)


def init_config():
    global testlink_uri
    global testlink_key
    global project_name
    global testplan_name
    global testbuild_name
    global http_proxy
    global branch
    global case_type
    global push_gateway
    testlink_uri = os.getenv("TESTLINK_URI", "testlink.alauda.cn")
    testlink_key = os.getenv("TESTLINK_KEY", "4fd38fc86d8810ce5442b43c8d21b87a")
    push_gateway = os.getenv("PUSH_GATE_WAY")
    case_type = os.getenv("CASE_TYPE")
    if os.getenv("ENV") == "staging":
        project_name = os.getenv("PROJECT_NAME", "ACP 2.0 UI")
        testplan_name = os.getenv("TESTPLAN_NAME", "master-regression-test")
        testbuild_name = os.getenv("TESTBUILD_NAME", "master-regression-test")
    else:
        project_name = os.getenv("PROJECT_NAME")
        testplan_name = os.getenv("TESTPLAN_NAME")
        testbuild_name = os.getenv("TESTBUILD_NAME")

    def trans_proxy(data):
        proto = re.findall(r'^(\w+)=', data)[0] if re.findall(r'^(\w+)=', data) else ''
        ip = re.findall(r'\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b', data)
        ip = ip[0] if ip else None
        port = re.findall(r':(\d+)$', data)[0] if re.findall(r':(\d+)$', data) else None
        if proto.startswith('http'):
            return 'http', ip, port
        elif proto.startswith('ssl'):
            return 'https', ip, port

    if os.getenv("BROWSER_PROXY"):
        proxys = list(map(trans_proxy, filter(lambda x: x.startswith('http') or x.startswith('ssl'), os.getenv("BROWSER_PROXY").split(';'))))
        http_proxy = proxys[0] if proxys else None
    else:
        http_proxy = None
    branch = os.getenv("BRANCH_NAME")


def get_json(file_name):
    print("start read %s" % file_name)
    with codecs.open(file_name, 'r', encoding="utf-8") as _jp:
        return json.load(_jp)


def csv_writer(file_name, data, fieldnames):
    with codecs.open(file_name, 'w', encoding='utf-8') as fp:
        writer = csv.DictWriter(fp, fieldnames=fieldnames)
        writer.writeheader()
        for d in data:
            writer.writerow(d)


def scan_files(directory, match=None):
    print("start scan directory: %s" % directory)
    files_list = []
    for root, sub_dirs, files in os.walk(directory):
        for special_file in files:
            if match:
                if re.match(match, special_file):
                    files_list.append(os.path.join(root, special_file))
            else:
                files_list.append(os.path.join(root, special_file))
    return files_list


def create_api():
    if http_proxy and http_proxy[1] and http_proxy[2]:
        proxy = ProxiedTransport()
        proxy.set_proxy(proto=http_proxy[0], host=http_proxy[1], port=int(http_proxy[2]))
    else:
        proxy = None
    return testlink.TestlinkAPIClient('http://%s/lib/api/xmlrpc/v1/xmlrpc.php' % testlink_uri, testlink_key, transport=proxy)


def get_report():
    report = ResultData()
    for file_name in filter(lambda f_n: 'combined.json' not in f_n, scan_files(os.path.abspath(os.path.curdir)+'/e2e-reports', '.{1,}\.json$')):
        content = get_json(file_name)
        if isinstance(content, dict):
            if "description" in content and "passed" in content and "pending" in content:
                if content["pending"]:
                    print(content["description"])
                    report.setitem(content["description"], "xit")
                else:
                    report.setitem(content["description"], content["passed"])
        else:
            print("Parse json file failed <%s>" % file_name)

    return report


def report_result(api, report):
    if project_name and testplan_name and testbuild_name:
        project_id = api.getProjectIDByName(project_name)
        case_prefix = list(filter(lambda project: project["name"] == project_name, api.getProjects()))[0]["prefix"]
        plan = list(filter(lambda pl: pl["name"] == testplan_name, api.getProjectTestPlans(project_id)))[0]
        # platforms = api.getTestPlanPlatforms(int(plan[id]))
        platform_id = '0'
        ret = []
        for case_name in filter(lambda cn: report[cn] != 'xit', report):
            if re.match("^%s-[1-9]+.+$" % case_prefix, case_name):
                full_external_ids = set(re.findall("%s-[0-9]+" % case_prefix, case_name))
                if full_external_ids:
                    for full_external_id in full_external_ids:
                        s = 'p' if report[case_name] else 'f'
                        try:
                            api.reportTCResult(testplanid=plan["id"], status=s, testcaseexternalid=full_external_id, platformid=platform_id, buildname=testbuild_name)
                        except Exception as e:
                            ret.append({"case_name": case_name, "passed": report[case_name]})
                            print('修改case状态失败', e)
            csv_writer(os.path.abspath(os.path.curdir)+'/e2e-reports/failed_report.csv', ret, ["case_name", "passed"])


def change_case_type(api, report):
    case_prefix = list(filter(lambda project: project["name"] == project_name, api.getProjects()))[0]["prefix"]
    for case_name in report:
        if re.match("^%s-[1-9]+.+$" % case_prefix, case_name):
            full_external_ids = set(re.findall("%s-[0-9]+" % case_prefix, case_name))
            for full_external_id in full_external_ids:
                try:
                    if report[case_name] == 'xit':
                        print("【%s】更新用例执行方式为手动" % full_external_id)
                        api.updateTestCase(testcaseexternalid=full_external_id, executiontype=1)
                    else:
                        print("【%s】更新用例执行方式为自动" % full_external_id)
                        api.updateTestCase(testcaseexternalid=full_external_id, executiontype=2)
                except Exception as e:
                    print("update test case execution type fail: %s" % e)


if __name__ == "__main__":
    init_config()
    api = create_api()
    report = get_report()
    if branch == "master":
        change_case_type(api, report)
        report_result(api, report)
        if os.getenv('ENV') == 'staging' and push_gateway:
            send_metrics_to_prometheus(report)
    else:
        report_result(api, report)