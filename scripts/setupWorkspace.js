const { prepareTestData } = require('../e2e/utility/prepare.testdata');
const KubectlCommon = require('../e2e/utility/kubectl.common').KubectlCommon;
const ServerConf = require('../e2e/utility/ServerConf').ServerConf;

setup();

function setup() {
  initKubectlContext();
  switch (process.env.CASE_TYPE) {
    case 'acp':
    case 'acp_infra':
    case 'acp_containerplatform':
    case 'acp_operation':
    case 'acp_app':
    case 'acp_network':
    case 'acp_other':
    case 'acp_workload':
    case 'icarus':
    case 'acp1':
    case 'acp2':
      setupAcpEnv();
      break;
    case 'underlord':
    case 'platform_ui':
    case 'platform_api':
    case 'acp3':
      setupAcpEnv();
      setupAitEnv();
      break;
    case 'asm_ui':
    case 'mephisto':
    case 'asm_api':
      setupAsmEnv();
      break;
    case 'devops_ui':
    case 'devops_api':
    case 'diablo_clone':
    case 'devops_pipeline':
    case 'devops_highcluster':
      setupDevOpsEnv();
      break;
    case 'ait_ui':
    case 'ait_api':
      setupAitEnv();
      break;
    case 'devops_asm':
      setupDevOpsEnv();
      setupAsmEnv();
      break;
    default:
      setupAllEnv();
  }
}

function initKubectlContext() {
  console.log('初始化 global 集群 kubectl Context\n');
  KubectlCommon.switchGlobal();

  KubectlCommon.initKubectlContext(
    ServerConf.REGIONNAME,
    ServerConf.BUSINESS_MASTER_IP,
  );
}

function setupAllEnv() {
  setupAcpEnv();
  setupAsmEnv();
  setupDevOpsEnv();
  setupAitEnv();
}

function setupAcpEnv() {
  const region_list = [ServerConf.REGIONNAME];
  if (ServerConf.CALICOREGIONNAME) {
    KubectlCommon.initKubectlContext(
      ServerConf.CALICOREGIONNAME,
      ServerConf.CALICO_IP,
    );
    region_list.join(ServerConf.CALICOREGIONNAME);
  }
  if (ServerConf.OVNREGIONNAME) {
    KubectlCommon.initKubectlContext(
      ServerConf.OVNREGIONNAME,
      ServerConf.OVN_IP,
    );
    region_list.join(ServerConf.OVNREGIONNAME);
  }
  prepareTestData.createProjectByTemplate('uiauto-acp', region_list);
  prepareTestData.createNamespace('uiauto-acp', 'ns1');
  prepareTestData.createNamespace('uiauto-acp', 'ns2');
  if (ServerConf.CALICOREGIONNAME) {
    prepareTestData.createNamespace(
      'uiauto-acp',
      'ns3',
      ServerConf.CALICOREGIONNAME,
    );
  }
  if (ServerConf.OVNREGIONNAME) {
    prepareTestData.createNamespace(
      'uiauto-acp',
      'ns4',
      ServerConf.OVNREGIONNAME,
    );
  }
  prepareTestData.createProjectByTemplate('uiauto-acp2');
  prepareTestData.createNamespace('uiauto-acp2', 'ns1');
  prepareTestData.createNamespace('uiauto-acp2', 'ns2');
  prepareTestData.bindingAllProjectToChartRepo();
}

function setupDevOpsEnv() {
  // 集成一个Jenkins,Gitlab,Harbor
  const jenkinsName = prepareTestData.integrateJenkins();
  const codeService = prepareTestData.integrateGitlab();
  const harborName = prepareTestData.integrateHarbor();
  const nexusNameUid = prepareTestData.integrateNexus();

  // 创建邮件服务器
  prepareTestData.createEmailServer('uiauto-devops-es1');
  //创建邮件通知对象
  prepareTestData.createEmailReceiver('uiauto-devops-er1');
  //创建邮件发送人
  prepareTestData.createEmailSenderSecret('uiauto-devops-es1');
  //创建邮件通知
  prepareTestData.createNotificationEmail(
    'uiauto-devops-email1',
    'uiauto-devops-er1',
    'uiauto-devops-es1',
    'notificationtemplate-devops-default',
  );

  // 项目1，只绑定了Jenkins(JenkinsSecretGlobal),创建属于项目1的私有凭据：gitlabSecret和harborImageSecret
  prepareTestData.createProject('uiauto-devops1');
  prepareTestData.createNamespace('uiauto-devops1', 'ns1');
  // 项目2，绑定Jenkins(JenkinsSecretGlobal), 使用GitlabSecretGlobal和HarborSecretGlobal绑定gitlab和Harbor
  prepareTestData.createProject('uiauto-devops2');
  prepareTestData.createNamespace('uiauto-devops2', 'ns1');
  // 项目3，绑定Jenkins(JenkinsSecretPrivate), 使用GitlabSecretPrivate和HarborSecretPrivate绑定gitlab和Harbor
  prepareTestData.createProject('uiauto-devops3');
  prepareTestData.createNamespace('uiauto-devops3', 'ns1');
  // 项目4，主要测试项目下的工具绑定
  prepareTestData.createProject('uiauto-devops4');

  // 创建全局凭据
  prepareTestData.createJenkinsSecret(
    'uiauto-jenkinssecret-global',
    'global-credentials',
  );

  prepareTestData.createGitlabSecret(
    'uiauto-gitlabsecret-global',
    'global-credentials',
  );
  prepareTestData.createHarborSecret(
    'uiauto-harborsecret-global',
    'global-credentials',
  );
  // 集成 Nexus 时候需要凭据
  prepareTestData.createNexusSecret(
    'uiauto-nexussecret-global',
    'global-credentials',
  );

  // 项目1下使用的资源，(为项目1创建应用)

  prepareTestData.createGitlabSecret(
    'uiauto-gitlabsecret-private1',
    'uiauto-devops1',
  );
  prepareTestData.createImageSecret(
    'uiauto-harborsecret-private1',
    'uiauto-devops1',
  );
  prepareTestData.createJenkinsBinding(
    jenkinsName,
    'uiauto-jenkinsbind1',
    'uiauto-devops1',
    'uiauto-jenkinssecret-global',
    'global-credentials',
  );
  prepareTestData.createHarborBinding(
    harborName,
    'uiauto-harborbind1',
    'uiauto-devops1',
    'uiauto-harborsecret-global',
    'global-credentials',
  );
  prepareTestData.createDevopsApp('uiauto-devops1-ns1', 'devops-app1');

  // 项目2下使用的资源：(为项目2绑定Jenkins,代码仓库和绑定镜像仓库,maven，使用global-credentials凭据)

  const mavenName = prepareTestData.createMavenRepo(
    ServerConf.NEXUS_URL,
    nexusNameUid[0],
    nexusNameUid[1],
  );
  prepareTestData.createMavenBinding(
    mavenName,
    'uiauto-mavenbind2',
    'uiauto-devops2',
    'uiauto-nexussecret-global',
    'global-credentials',
  );
  prepareTestData.createJenkinsBinding(
    jenkinsName,
    'uiauto-jenkinsbind2',
    'uiauto-devops2',
    'uiauto-jenkinssecret-global',
    'global-credentials',
  );
  prepareTestData.createGitlabBinding(
    codeService,
    'uiauto-gitlabbind2',
    'uiauto-devops2',
    'uiauto-gitlabsecret-global',
    'global-credentials',
  );
  prepareTestData.createHarborBinding(
    harborName,
    'uiauto-harborbind2',
    'uiauto-devops2',
    'uiauto-harborsecret-global',
    'global-credentials',
  );
  prepareTestData.createDevopsApp('uiauto-devops2-ns1', 'devops-app2');

  // 项目3下使用的资源：(为项目3绑定Jenkins,代码仓库和绑定镜像仓库，使用private的凭据)
  prepareTestData.createJenkinsSecret(
    'uiauto-jenkinssecret-private3',
    'uiauto-devops3',
  );
  prepareTestData.createGitlabSecret(
    'uiauto-gitlabsecret-private3',
    'uiauto-devops3',
  );
  prepareTestData.createHarborSecret(
    'uiauto-harborsecret-private3',
    'uiauto-devops3',
  );
  prepareTestData.createJenkinsBinding(
    jenkinsName,
    'uiauto-jenkinsbind3',
    'uiauto-devops3',
    'uiauto-jenkinssecret-private3',
    'uiauto-devops3',
  );
  prepareTestData.createGitlabBinding(
    codeService,
    'uiauto-gitlabbind3',
    'uiauto-devops3',
    'uiauto-gitlabsecret-private3',
    'uiauto-devops3',
  );
  prepareTestData.createHarborBinding(
    harborName,
    'uiauto-harborbind3',
    'uiauto-devops3',
    'uiauto-harborsecret-private3',
    'uiauto-devops3',
  );

  // 项目4下使用的资源：(为项目4创建gilabsecret,harborsecret,githubsecret)
  prepareTestData.createJenkinsSecret(
    'uiauto-jenkinssecret-private4',
    'uiauto-devops4',
  );
  prepareTestData.createGitlabSecret(
    'uiauto-gitlabsecret-private4',
    'uiauto-devops4',
  );
  prepareTestData.createHarborSecret(
    'uiauto-harborsecret-private4',
    'uiauto-devops4',
  );
}

function setupAsmEnv() {
  prepareTestData.createProject('uiauto-asm');
  prepareTestData.createProject('uiauto-asm-pagination');
  prepareTestData.createNamespace('uiauto-asm-pagination', 'ns');
  prepareTestData.createNamespace('uiauto-asm', 'ns1');
  prepareTestData.createNamespace('uiauto-asm', 'ns2');

  prepareTestData.enabledAsm('uiauto-asm-ns1');
  prepareTestData.enabledAsm('uiauto-asm-pagination-ns');

  prepareTestData.createAsmApp('asm-app1', 'uiauto-asm-ns1');
  prepareTestData.createAsmApp('asm-app2', 'uiauto-asm-ns1');
}

function setupAitEnv() {
  prepareTestData.createDefaultSender();
  prepareTestData.createEmailServer();
  prepareTestData.createEmailReceiver();
  prepareTestData.createEmailSenderSecret();
  prepareTestData.createEmailtemplate();
  prepareTestData.createSmsReceiver();
  prepareTestData.createSmstemplate();
  prepareTestData.createWebhookReceiver();
  prepareTestData.createWebhooktemplate();
  prepareTestData.createDingtalkReceiver();
  prepareTestData.createWechatReceiver();
  prepareTestData.createNotificationEmail();
  prepareTestData.createNotificationSms();
  prepareTestData.createAitApp(
    'auto-ait-appdeploy1',
    'uiauto-acp-ns1',
    'uiauto-acp',
    'Deployment',
  );
  prepareTestData.createAitApp(
    'auto-ait-appstatefulset1',
    'uiauto-acp-ns1',
    'uiauto-acp',
    'StatefulSet',
  );
  prepareTestData.createAitApp(
    'auto-ait-appdaemonset1',
    'uiauto-acp-ns1',
    'uiauto-acp',
    'DaemonSet',
  );
}
