#!/bin/bash
if [ "$1" == "" ]; then
    echo "write context name..."
    exit 1
fi

kubectl config set-cluster $1 --server=$2 --insecure-skip-tls-verify=true
kubectl config set-credentials $1 --token=$3
kubectl config set-context $1 --cluster=$1 --user=$1
kubectl config use-context $1
