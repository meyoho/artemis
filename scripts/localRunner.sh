rm -f ./e2e/test_data/temp/*.yaml
rm -f ./e2e/test_data/temp/token
rm -rf ./e2e-reports
rm -rf ./protractorFailuresReport

if [ -a ./custom.env ]; then
  source ./custom.env
else
  export BROWSER_PROXY="proxyType=manual;sslProxy=139.186.2.80:37491"
  export ENV=staging
  export BRANCH_NAME=local
  export INIT_WORKSPACE=true
fi

if [ $INIT_WORKSPACE ]; then
  if [ $INIT_WORKSPACE = "true" ]; then
    rm -f ./e2e/test_data/temp/token
    node ./scripts/setupWorkspace.js
  fi
else
  rm -f ./e2e/test_data/temp/token
  node ./scripts/setupWorkspace.js
fi

./node_modules/.bin/protractor $*