#!/bin/bash

# run shell cmd, continue when error
exec_cmd(){
  eval '$1' || echo -e "\033[33m [WARN] $2 \033[0m"
}

# run shell cmd, exit when error
error_exit(){
  eval '$1'
  CODE=$?
  if [ $CODE -gt 0 ]; then
    echo -e "\033[31m [ERROR] '$2' \033[0m"
    exit $CODE
  fi
}

error_exit 'node ./scripts/setupWorkspace.js' 'setupWorkspace Error, please check ENV'

CODE=999

# run tests
if [ $CASE_TYPE == "BAT" ]; then
  yarn runner bat
  CODE=$?
else
  yarn runner $CASE_TYPE
  CODE=$?
fi

exec_cmd 'node ./e2e/utility/warningRobot.js' 'Send weixin message error'
exec_cmd 'python3.5 ./scripts/horn.py' 'Update testlink case error'

if [ -n "${REPORTS_DIR}" ];then 
    if [ ! -d "${REPORTS_DIR}" ];then
        exec_cmd "rm -rf ${REPORTS_DIR}" 'Delete Report DIR error'
        exec_cmd "mkdir -p ${REPORTS_DIR}" 'Create Report DIR error'
    fi
    dir_name=${REPORTS_DIR}/${CASE_TYPE}-$(date "+%Y-%m-%d-%H-%M")
    exec_cmd "mkdir -p ${dir_name}" 'Create Report DIR error'
    exec_cmd "cp -r ./e2e-reports/* ${dir_name}" 'Copy Report Result error'
    if [ -d "./protractorFailuresReport" ];then
       exec_cmd "mv ./protractorFailuresReport ${dir_name}" 'Move Error Report Result error'
    fi
fi

if [ $CODE -gt 0 ]; then
    echo -e "\033[31m [ERROR] 'UI TESST Error, exit code is $CODE' \033[0m"
    exit $CODE
else
    echo -e "\033[32m [INFO] 'UI TESST Success, exit code is $CODE' \033[0m"
    exit $CODE
fi
