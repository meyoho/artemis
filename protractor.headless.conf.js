const { config: baseConfig } = require('./protractor.conf');

const config = {
  ...baseConfig,
  capabilities: {
    ...baseConfig.capabilities,
    browserName: 'chrome',
    shardTestFiles: true,
    maxInstances: 4,
    chromeOptions: {
      args: [
        '--headless',
        '--disable-gpu',
        '--window-size=2920,3640',
        '--ignore-certificate-errors',
        '--no-sandbox',
      ],
    },
    acceptInsecureCerts: true,
  },
};

module.exports = {
  config,
};
